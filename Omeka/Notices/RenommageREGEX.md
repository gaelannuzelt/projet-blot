### REGEX dans libreOffice

- Rechercher
  - `^(.*\\)?(?:$|(.+?)(?:(\.[^.]*$)|$))`
- Remplacer 
  - `$1zzz$3`


```sh
$1 = "path/"
$2 = "nom"
$3 = ".extension"
```

### Rechercher `zzz` et remplacer par `A2` (identifiant)

- Formule
  - `=`