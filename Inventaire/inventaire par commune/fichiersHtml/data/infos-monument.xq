for $i in //*:section[@class="monument"]
let $nom := string-join(
  for $z in $i/h3//text()
  return
  $z, ' ')

let $commune := for $a in $i//*:span[@class="commune"]
return <commune>{$a/text()}</commune>

let $lieu := for $b in $i//*:span[@class="lieu"]
return <lieu>{$b/text()}</lieu>

let $type := for $c in $i//*:span[@class="type"]
return <type>{$c/text()}</type>

let $date := for $d in $i//*:span[@class="date"]
return <date>{$d/text()}</date>

let $inventeur := for $e in $i//*:span[@class="inventeur"]
return <inventeur>{$e/text()}</inventeur>

let $rapport := for $f in $i//*:span[@class="rapport"]
return <rapport>{$f/text()}</rapport>

let $hal := for $g in $i//*:span[@class="hal"]
return <hal>{$g/text()}</hal>

let $sujet := for $h in $i//*:span[@class="sujet"]
return <sujet>{$h/text()}</sujet>

let $lien_monument := for $j in $i//*:span[@class="lien_monument"]
return <lien_monument>{$j/text()}</lien_monument>

let $sub := for $k in $i//*:span[@class="sub"]
return <sub>{$k/text()}</sub>

let $monument_détruit := for $l in $i//*:span[@class="monument_détruit"]
return <monument_détruit>{$l/text()}</monument_détruit>

let $description := for $m in $i
return <descritption>{$i}</descritption>

return <notice><nom>{$nom}</nom>{$commune}{$lieu}{$type}{$date}{$inventeur}{$rapport}{$hal}{$sujet}{$lien_monument}{$sub}{$monument_détruit}{$description}</notice>
