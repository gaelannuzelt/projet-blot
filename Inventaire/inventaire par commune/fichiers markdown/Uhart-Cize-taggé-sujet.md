# Uhart-Cize

### [Uhart-Cize]{.spatial} - [Phagalepoa]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 885m.

Ce tumulus est situé sur sol plat au sommet d'une petite éminence qui domine, à l'Ouest, la « voie romaine », à environ 400m au Sud du refuge d'Orisson.

Description

Tumulus circulaire mesurant 8m de diamètre et environ 0,90m de haut. Il n'y a pas d'éléments pierreux visibles ; on note une légère dépression à son sommet.

Il est entouré d'une légère dépression circulaire, de 1,60m de diamètre en moyenne, qui pourrait correspondre au volume de terre recueillie pour son élaboration.

Historique

Monument découvert par [F. Meyrat]{.creator} en [décembre 2017]{.date}.

### [Uhart-Cize]{.spatial} - [Othatzen borda]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 922m.

Il est situé à environ 800m plus au Sud que *Phagalepoa - Tumulus*, sur la même ligne de croupes orientées Sud-Nord, et toujours à l'Ouest de la « voie romaine ». On le voit à l'extrémité Nord d'une petite éminence, juste avant sa rupture de pente vers le Nord.

Description

Une légère surélévation du terrain au sommet de cette éminence semble bien individualiser un relief tumulaire circulaire de 6m de diamètre et 0,50m de haut. [Monument cependant douteux]{.douteux}.

Historique

A été découvert par [F. Meyrat]{.creator} en [décembre 2017]{.date}.
