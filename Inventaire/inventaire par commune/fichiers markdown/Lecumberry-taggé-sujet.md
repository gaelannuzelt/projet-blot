# Lecumberry

### [Lecumberry]{.spatial} - [Apatessaro]{.coverage} 1 ter - [Cromlech]{.type}

Localisation

Carte 1346 Est Saint-Jean-Pied-de-Port.

Altitude : 1135m.

[Nous]{.creator} avons déjà publié la plus grande partie de cette nécropole en 1972 [àblotNouveauxVestigesMegalithiques1972a p.74], et 1986 [@blotNecropoleProtohistoriqueApatessaro1986 p. 89]. On compte ainsi les *cromlechs 1*, *1 bis*, *2*, *6*, et *7* ; les *tumulus 3*, *4*, *5*, et *8*.

Nous avons effectué des fouilles de sauvetage sur les *cromlechs 1* et *1 bis*, [@blotCromlechsApatessaro1bis1986, p.92], et sur les *tumulus n°4*, [@blotTumulusApatessaroIV1988 p.1], *n°5* [@blotTumulusApatessaroCompte1988 p.177], et sur le *n°6* [@blotTumulusApatesaroVI1994 p.47].

On peut voir le *cromlech 1 ter* à 3m à l'Ouest du *n°1 bis*.

Description

Six petites pierres, au ras du sol, délimitent un cercle de 4m de diamètre.

Historique

Monument découvert en [juin 1983]{.date}.

### [Lecumberry]{.spatial} - [Apatessaro]{.coverage} 8 - [Tumulus]{.type}

Localisation

Altitude : 1200m.

Il est (difficilement) visible en bordure de la piste qui monte à Okabé, mais à quelques mètres au Sud ; isolé dans un ensemble pierreux, il se détache cependant sur un petit replat gazonné, à 3m au Sud-Est d'un gros amas de blocs de poudingue.

Description

Tumulus pierreux de faible hauteur (0,15m) et légèrement ovale (3,50m x 3m), constitué de blocs pierreux de modeste volume (d'un demi à un pavé).

Historique

Monument découvert en [juin 1983]{.date}.

### [Lecumberry]{.spatial} - [Artxilondo]{.coverage} - [Dolmen]{.type}

Localisation

Carte 1346 Est Saint-Jean-Pied-de-Port.

Altitude : 880m.

Il est érigé sur un petit promontoire à 80m au Sud-Ouest de la jonction d'Iraunabako-erreka avec Toscato-erreka ; il est donc sur la rive gauche du rio Artxilondo qu'il domine directement, et à 600m à l'Ouest Sud-Ouest des cabanes d'Artxilondo.

Description

Important tumulus de galets de [quartzite]{.subject} roulés, mesurant 11m de diamètre et 1,40m de haut. Au centre, la [chambre funéraire]{.subject} est orientée Nord-Ouest Sud-Est et mesure 3m de long, 1,20m de large, et 0,90m de profondeur ; elle est bien délimitée au Nord-Est par une grande dalle de [grés]{.subject} poudingue de 2,80m de long, 0,90m de haut et 0,30m d'épaisseur. Sur le versant Est du tumulus, immédiatement à côté de la chambre gît la dalle de [couverture]{.subject} : belle dalle de [grés]{.subject} poudingue, grossièrement rectangulaire mesurant 2,86m de long, 1,80m de large et 0,30m d'épaisseur. Ses 4 bords portent des traces d'épannelage.

Historique

Dolmen découvert en [octobre 1976]{.date}. Il a fait l'objet d'un sondage diagnostic en 2000 par J. Blot et D. Ebrard [@ebrardDolmenArtxilondoLecumberry2000].

### [Lecumberry]{.spatial} - [Artxilondo]{.coverage} - [tertres d'habitat]{.type}

Localisation

Altitude : 921m.

Tout d'abord, 6 tertres, découvert en 1974, sont échelonnés sur la rive gauche d'un petit ruisseau né au niveau du cayolar de Néquecharré (ou Négousaro), et érigés au niveau des 2 anciennes cabanes d'Artxilondo. - Ils ont depuis été remaniés ou détruits par de nouvelles constructions. Ont ainsi disparu les *tertres n°2*, *4* et *5*. Il ne semble rester que les *n°6*, *1* et *3*.

Description

- *Tertre n°1*, de 11m de diamètre et 0,60m de haut.

- *Tertre n°2* : à 7m à l'Ouest du précédent. Mesure10m de diamètre et 0,60m de haut.

- *Tertre n°3* : à 2m à l'Ouest du précédent. Mesure 13m de diamètre et 0,70m de haut.

- *Tertre n°4* : tangent au précédent et de mêmes dimensions.

- *Tertre n°5* : tangent au précédent, en partie amputé de son extrémité Sud par le passage de la piste venue des vieilles cabanes, mesure10m de diamètre et 0,40m de haut environ.

- *Tertre n°6* : à 1m à l'Ouest du précédent - lui aussi amputé par la piste -- situé à gauche du chemin qui mène aux 2 récents cayolar, et avant ce dernier ; mesure 6m et 0,40m de haut environ.

- Le *tertre n°7* est situé en bordure du chemin qui contourne les deux récents cayolars, au Nord de ceux-ci, et donne vers la pente d'un ruisseau.

- Le *tertre n°8* : est situé en dehors de la clôture des barbelés, à 9m au Sud-Est du n°1 ; mesure 6m x 7m et 0,50m de haut semble-t-il.

Historique

Ces tertres ont été trouvés par [Blot J.]{.creator} en [1974]{.date}. Sauf les 7 et 8 derniers tertres ont été trouvés pat Blot J. en [février 2014]{.date}.

### [Lecumberry]{.spatial} - [Artxilondo]{.coverage} 4 - [Tumulus]{.type}

Localisation

Carte 1346 Est Saint-Jean-Pied-de-Port.

Altitude : 908m.

Nous avons déjà décrit [@blotNouveauxVestigesMegalithiques1972a, p.72], dans cette région située à l'Ouest du mont Okabé, 3 cromlechs 1, 2, 3.

Par rapport aux cabanes d'Atxilondo, le tumulus 4 est situé à 4m de l'angle Nord Nord-Est du cayolar situé à droite de la route qui se rend aux cabanes précitées, au premier virage une fois franchi le pont sur Iraunabako-erreka.

Description

Tumulus de 4,50m de diamètre et 0,30m de haut ; 4 pierres sont visibles à sa périphérie sans que l'on puisse parler d'un tumulus-cromlech.

Historique

Monument découvert en [juillet 1974]{.date}.

### [Lecumberry]{.spatial} - [Artxilondo]{.coverage} 5 - [Tumulus]{.type}

Localisation

Il est situé à 8m à l'Ouest Nord-Ouest de *Artxilondo 4 - Tumulus*.

Description

Tumulus terreux d'un diamètre de 4m et 0,40m de haut ; une vingtaine de pierres apparaissent en désordre à sa surface. Il se pourrait qu'il y ait un péristalithe, mais en grande partie recouvert de terre et de végétation.

Historique

Monument découvert en [juillet 1974]{.date}.

### [Lecumberry]{.spatial} - [Artxilondo]{.coverage} 6 - [Tumulus]{.type}

Localisation

Altitude : 908m.

Il se trouve à 4m à l'Ouest du Tumulus-cromlech 5 ou à environ 25m à l'Ouest du petit cayolar.

Description

Tumulus de 8m de diamètre et 0,30m environ ; quelques rares pierres apparaissent par endroits.

Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

### [Lecumberry]{.spatial} - [Bassabero]{.coverage} 1 - [Tumulus]{.type}

Localisation

Carte 1346 Est Saint-Jean-Pied-de-Port.

Altitude : 1217m.

Il est érigé au point culminant de la montée qui domine Artxilondo au Nord, éminence située entre Apatessaroko-erreka et Toscato-erreka.

Description

Tumulus pierreux de 3m de diamètre et 0,40m de haut, formé de blocs de pierre amoncelés de la taille d'un pavé (à 150m au Nord-Est dans un petit ensellement, un léger relief pourrait évoquer un tumulus herbeux de 6m de diamètre 0,40m de haut).

Historique

Monument découvert en [octobre 1980]{.date}.

### [Lecumberry]{.spatial} - [Bassabero]{.coverage} 2 - [Cromlech]{.type}

Localisation

Carte 1346 Est Saint-Jean-Pied-de-Port.

Altitude : 1040m.

Cromlech situé au flanc de cette éminence incluse entre les 2 ruisseaux Apatessaroko-erreka et Toscato-erreka. Il est construit sur un terrain en légère pente qui domine à 20m à gauche le chemin qui mène à Neguecharre, ceci environ 500m après qu'il ait franchi le petit ravin d'Apatessaro.

Description

Cercle de 4m de diamètre, délimité par 15 pierres bien visibles (0,10m à 0,20m de haut). Au centre est visible, au ras du sol, une pierre de [grés]{.subject} local.

Historique

Monument découvert en [1979]{.date}.

### [Lecumberry]{.spatial} - [Bassabero]{.coverage} 3 - [Tumulus]{.type}

Localisation

Altitude : 1247m.

Situé à environ 1m au Nord d'un pointement rocheux.

Description

Tertre légèrement ovalaire, érigé sur terrain plat, de 8m de long et 6m environ de large, et 0,40m de haut, constitué de pierres concassées, de la taille d'une cerise ou d'un abricot.

Historique

Tertre découvert par [Meyrat F.]{.creator} en [août 2013]{.date}.

### [Lecumberry]{.spatial} - [Bassabero]{.coverage} 4 - [Tumulus]{.type}

Localisation

Il est à 10m au Nord-Ouest de *Bassabero 3 - Tumulus*.

Description

De même forme ovalaire et de dimensions semblables que *Bassabero 3 - Tumulus*, mais un peu plus haut (0,50m), il est constitué du même cailloutis.

Historique

Tumulus découvert par [Meyrat F.]{.creator} en [août 2013]{.date}.

### [Lecumberry]{.spatial} - [Bassahémela]{.coverage} - [Tumulus-cromlech]{.type} ([?]{.douteux})

Localisation

Altitude : 537m.

Situé à une quarantaine de mètres à l'Ouest Nord-Ouest du pointement rocheux.

Description

Petit tumulus terreux, de 4m de diamètre et 0,30m de haut ; il semble que 6 pierres environ puissent délimiter un péritalthe.

Historique

Monument découvert par [Meyrat F.]{.creator} en [avril 2014]{.date}.

### [Lecumberry]{.spatial} - [Bassahémela]{.coverage} - [Tumulus-cromlech]{.type} ([?]{.douteux})

Localisation

Altitude : 537m.

Situé à une quarantaine de mètres à l'Ouest Nord-Ouest du pointement rocheux.

Description

Petit tumulus terreux, de 4m de diamètre et 0,30m de haut ; il semble que 6 pierres environ puissent délimiter un péritalthe.

Historique

Monument découvert par [Meyrat F.]{.creator} en [avril 2014]{.date}.

### [Lecumberry]{.spatial} - [Bassahémela]{.coverage} - [Tumulus]{.type}

Localisation

Carte 1346 Est Saint-Jean-Pied-de-Port.

Altitude : 540m.

Il est situé sur un petit replat à environ 40m au Sud Sud-Est du point culminant ainsi dénommé.

Description

Tumulus mixte de 4m de diamètre et 0,60m de haut ; de très nombreuses pierres sont visibles.

Historique

Monument découvert en [mai 1972]{.date}.

### [Lecumberry]{.spatial} - [Egurgi]{.coverage}- [Tertres d'habitat]{.type}

Localisation

Altitude : 830m.

Ces 13 tertres sont échelonnés de part et d'autre de la route qui se rend à la frontière en passant par les anciennes installations piscicoles d'Egurgi.

Description

On note :

- le *Tertre n°1*, à droite (à l'Ouest) de la route, il mesure 10m de diamètre et 0,50m de haut 

- Le *Tertre n°2* est tangent au Sud-Est et mesure 12m de diamètre et 0,70m de haut. 

- Le *Tertre n°3* est de l'autre côté de la route et a été coupé en son milieu ; il mesure 22m de diamètre et 1m de haut 

- Le *Tertre n°4* est à 7m à l'Est Nord-Est du précédent, plus sur la hauteur ; il mesure 22m de diamètre et 1m de haut. 

- Le *Tertre n°5* est à l'Est du précédent, et mesure 6m de diamètre et 0,80m de haut. 

- Les *Tertres 6, 7, 8* sont tangents les uns aux autres, échelonnés suivant un axe Ouest-Est, et de taille bien inférieure : ils mesurent entre 2 et 3 m de diamètre 

- Il en est de même pour le groupe de *Tertres 9, 10, 11, 12, 13* qui sont un peu au Sud des précédents, et ont sensiblement les mêmes dimensions.

Historique

Les tertres 1,2,3,4 ont été trouvés par [Blot J.]{.creator} en [1970]{.date} et les autres par [Meyrat F.]{.creator} en [août 2013]{.date}.

### [Lecumberry]{.spatial} - [Etxaaté Bizkarra]{.coverage} 3 - [Tumulus]{.type}

Localisation

À 40m au Nord-Est du *Tumulus 2*.

Altitude : 1199m

Description

Ce tumulus, au sommet d'un petit relief naturel, mesure 3,90m de diamètre, et 0,70m de haut. Il est constitué de terre et de nombreuses pierres tant à sa surface - particulièrement dans son secteur Sud - qu'à sa périphérie, sans qu'on puisse parler réellement d'un péristalithe.

Historique

Tumulus découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

### [Lecumberry]{.spatial} - [Etxaaté Bizkarra]{.coverage} 4 - [Tumulus-cromlech]{.type}

Localisation

Altitude : 1198m.

Un peu en contrebas et au Sud-Est du *Tumulus 2*.

Description

Tumulus érigé sur un terrain en légère pente vers l'Ouest, mesurant 3,60m de diamètre, 0,40m de haut ; on note une dépression centrale de 2,20m de diamètre, dont la périphérie est bordée par une dizaine de pierres plus ou moins apparentes.

Historique

Tumulus découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

### [Lecumberry]{.spatial} - [Etxaaté Bizkarra]{.coverage} - [Tertre d'habitat]{.type}

On note à 2m à l'Est de *Etxaate Bizkarra 4 - Tumulus-cromlech*, un relief qui paraît pouvoir être rattaché à celui d'un tertre d'habitat mesurant 7m de diamètre, ainsi que 3m à son sommet plat et 0,50m de haut.

Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

### [Lecumberry]{.spatial} - [Haltza]{.coverage} - [Tertre d'habitat]{.type}

Localisation

Altitude : 782m.

Il est situé à quelques mètres au Sud-Est de la route qui monte vers St-Sauveur, sur le replat d'où se détache la bretelle qui descend vers le ravin de Gastaneguiko erreka.

Description

Tumulus de terre circulaire de 8m de diamètre environ, asymétrique (de 1m dans sa plus grande hauteur).

Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

### [Lecumberry]{.spatial} - [Harribilibila]{.coverage} Nord - [Tertres d'habitat]{.type}

Localisation

Altitude : 770m.

Les 2 tertres se trouvent à l'extrémité Nord Nord-Ouest du haut plateau d'Harribilibila (lieu-dit : Zalgarazareteitzaleta), sur le versant Nord d'une doline.

Description

- *TH 1* : le plus au Nord-Est des deux - Relief très discret mais cependant visible ; diamètre : 5m à 6m.

- *TH 2* se trouve à 15m au Sud-Est du précédent, de relief encore plus discret ; dimensions semblables.

Historique

Tertres découverts par [Blot J.]{.creator} en [avril 2013]{.date}.

### [Lecumberry]{.spatial} - [Hurlastère]{.coverage} - [Tertres d'habitat]{.type}

Localisation

Altitude : 510m.

Ces deux tertres sont situés au pied de la colline de ce nom qui porte la route se rendant vers les chalets d'Irati. Au niveau du col d'Haltza se détache, à droite, une piste qui descend vers le ravin où coule un ruisseau dénommé Harrigorrizolako erreka. Au dernier virage de la piste arrivée en bas de sa course, se détachent ces deux magnifiques éminences, que, jusqu'à preuve du contraire nous appellerons tertres d'habitat, leurs caractéristiques y répondant, semble-t-il, mais... sur un mode « majeur » !

Description

- *Tertre n°1* : Le premier dans le virage, sur la droite, dans une prairie en pente douce vers l'Ouest.

    Tertre ovale gazonné, ne présentant aucune trace de structure particulière. Il mesure 25m x 23m et d'une hauteur de plusieurs mètres difficilement appréciable... (3m environ).

- *Tertre n°2* : situé à environ 45m au Nord Nord-Ouest du précédent, toujours sur un terrain en pente vers l'Ouest. Tertre ovale gazonné.

### [Lecumberry]{.spatial} - [Irati Soro]{.coverage} 5 - [Cromlech]{.type}

Localisation

Altitude : 910m.

Difficile à voir, ce cromlech incomplet est situé en contre-bas et à une quinzaine de mètres à l'Est de la route qui vient de Sourcay, sur un terrain en pente légère.

Description

Cercle de 6m de diamètre, limité par 5 pierres, essentiellement groupées dans le secteur Ouest.

Historique

Cercle peut-être brièvement cité par [JM de Barandiaran]{.creator} en [1953]{.date} [@barandiaranHombrePrehistoricoPais1953p 248] et précisé par [Blot J.]{.creator} en [2014]{.date}.

### [Lecumberry]{.spatial} - [Irauko lepoa]{.coverage} 2 - [Cromlech]{.type}

Localisation

Carte 1346 Est Saint-Jean-Pied-de-Port.

317,228 - 1789,730.

Altitude : 1008.

Il est à 100m au Nord Nord-Ouest du *Cromlech n°1* et à 35m à l'Ouest Nord-Ouest de la piste qui part de la route asphaltée et descend dans la vallée d'Iraunabako-erreka. Le *Cromlech n°1* a été publié [@blotNouveauxVestigesMegalithiques1972b p. 68] ; il a été rasé par un bulldozer en 1973.

Description

Cercle de 5,50m de diamètre délimité par 9 blocs de poudingue qui s'élèvent de 0,20m à 0,30m au-dessus du sol, certains atteignent même 0,70m de long en secteur Ouest.

Historique

Monument découvert en [septembre 1972]{.date}.

### [Lecumberry]{.spatial} - [Iraunabako erreka]{.coverage} 1 - [Cromlech]{.type}

Localisation

Carte 1346 Est Saint-Jean-Pied-de-Port.

Altitude : 966m.

Il est situé à 250m au Sud Sud-Ouest du col d'Irau, à une confluence de ruisseaux.

Description

Cercle de 6,30m de diamètre ; on compte environ 65 pierres réparties, semble-t-il en 2 ou 3 cercles concentriques, plus ou moins bien individualisés suivant les endroits. Près du centre, 2 légères dépressions évoquent la possibilité de fouilles anciennes.

Historique

Monument vu en [septembre 1968]{.date} ; il nous avait été signalé par [P. Boucher]{.creator}.

### [Lecumberry]{.spatial} - [Iraunabako erreka]{.coverage} 2 - [Tumulus]{.type}

Localisation

À 15m au Nord de *Iraunabako-erreka 1 - Cromlech*.

Description

Tumulus mixte, de terre et de pierres, légèrement ovale, à grand axe Est-Ouest. (5m x 2,50m) et atteignant 0,30m de haut.

Historique

Monument découvert en [septembre 1968]{.date}.

### [Lecumberry]{.spatial} - [Iraunabako erreka]{.coverage} 3 - [Tumulus]{.type}

Localisation

Il est à 150m au Sud Sud-Ouest de *Iraunabako-erreka 1 - Cromlech* et sur la rive droite d'Iraunabako-erreka.

Description

Tumulus mixte de 0,50m de haut, légèrement ovale à grand axe Nord-Est Sud-Ouest, (5,20m x 4,40m) et à sommet légèrement aplani au centre duquel on remarque une dépression évoquant une fouille ancienne. Il est constitué de blocs de la taille d'un pavé.

Historique

Monument découvert en [1968]{.date}.

### [Lecumberry]{.spatial} - [Iraunabako erreka]{.coverage} 4 - [Tumulus]{.type}

Localisation

Il est à 80m au Sud-Ouest de *Iraunabako-erreka 3 - Tumulus*.

Description

Tumulus mixte, de pierres et de terre, circulaire, arrondi en dôme, mesurant 4,50m de diamètre et 0,50m de haut.

Historique

Monument découvert en [septembre 1968]{.date}. Nous avons pratiqué une fouille de sauvetage en 1988. [@blotTumulusIrauIV1992 p.167]. La datation obtenue (2560 - 2057 BC.) nous indique qu'il s'agit du plus ancien monument à incinération connu en Pays Basque.

### [Lecumberry]{.spatial} - [Iraunabako erreka]{.coverage} 5 - [Cromlech]{.type}

Localisation

Carte 1346 Est Saint-Jean-Pied-de-Port.

Altitude : 938m.

Il est sur la rive droite du ruisseau, à 4m de la berge et à une centaine de mètres environ au Nord du défilé rocheux, au niveau de l'extrémité Est de la crête Exaaté-Bizkarra.

Description

Cercle de 5m de diamètre, délimité par 11 petits blocs de [grés]{.subject} poudingue, dépassant la surface du sol d'environ 0,10m à 0,30m ; au Nord, on note une dalle couchée vers l'extérieur, mesurant 1m x 0,60m.

Historique

Monument découvert en [septembre 1968]{.date}.

### [Lecumberry]{.spatial} - [Iraukolepoa]{.coverage} 3 - [Tumulus]{.type}

Localisation

Altitude : 1008m.

Il est à 1m au Nord-Est de la route et à 2 m à l'Est du petit parking goudronné.

Description

Petit tumulus terreux de 5m de diamètre et quelques centimètres de haut. On note une légère dépression centrale de 1m de diamètre environ.

Historique

Tumulus découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

### [Lecumberry]{.spatial} - [Iraukolepoa]{.coverage} 4 - [Tumulus-cromlech]{.type}

Localisation

Il est à une quinzaine de mètres au Sud de *Iraukolepoa 3 - Tumulus*.

Description

Tumulus mixte de pierres et de terre de 5m de diamètre environ et 0,40m de haut, avec légère dépression centrale de 1,30m de diamètre. Les pierres périphériques sont particulièrement fournies et visibles dans le secteur Ouest.

Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

### [Lecumberry]{.spatial} - [Iraukolepoa]{.coverage} 5 - [Cromlech]{.type}

Localisation

Il est situé à 14m à l'Est de *Iraukolepoa 4 - Tumulus-cromlech*.

Altitude : 1008m

Description

Petit cercle de 3,50m de diamètre, délimité par 4 pierres, peu en relief, dont une centrale.

Historique

Cercle découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

### [Lecumberry]{.spatial} - [Iraukolepoa]{.coverage} 6 - [Cromlech]{.type}

Localisation

Altitude : 970m.

Situé à mi-pente, à gauche (à l'Est) de la piste qui descend du col.

Description

Six pierres au ras du sol délimitent un cercle très net de 2,20m de diamètre.

Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

### [Lecumberry]{.spatial} - [Iraukolepoa]{.coverage} 7 - [Cromlech]{.type}

Localisation

Altitude : 995m.

À 20m au Sud-Est et de la piste qui descend du col, et donc sur terrain en légère pente vers le Sud-Ouest.

Description

7 pierres délimitent un demi-cercle de 7m de diamètre ; la plus grande présente des traces nettes d'épannelage. Dans la moitié Nord gît une pierre, la plus importante, qui mesure 1,50m de long, 0,40m de large et 0,20m d'épaisseur.

Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

### [Lecumberry]{.spatial} - [Iraukolepoa]{.coverage} 8 - [Tumulus]{.type}

Localisation

À 10m au Nord-Ouest du petit parking goudronné du col d'Irau.

Description

Tumulus de terre et de pierres coupé à son tiers par la route. (Il en resterait les 2/3) - Il mesure 11m de diamètre et 0,80m de haut.

Historique

Tumulus découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

### [Lecumberry]{.spatial} - [Iraunabako erreka]{.coverage} 6 - [Dolmen]{.type}

Localisation

Altitude : 965m.

Il est situé sur la rive gauche du rio.

Description

Tumulus de 3,50m de diamètre, mixte (terre et pierres) et 0,40m de haut. Au centre, la [chambre funéraire]{.subject}, orientée Est-Ouest, mesure 1,10m de long, et 0,35m de large. Elle est parfaitement délimitée par 4 dalles qui affleurent la surface du sol : La dalle Sud mesure 0,95m de long et 0,20m de large ; La dalle Nord mesure 0,80m de long, 0,30m de large ; la dalle Ouest : 0,39m de long et 0,12m de large ; la dalle Est : 0,72m de long et 0,13m de large.

Sur le versant Sud-Ouest du tumulus gît la pierre de [couverture]{.subject} mesurant 0,90m de long, 0,40m de large et 0,20m d'épaisseur. Ce monument a fait l'objet d'une fouille clandestine relativement récente.

Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

### [Lecumberry]{.spatial} - [Iraunabako erreka]{.coverage} 7 - [Tumulus]{.type}

Localisation

À 15m au Sud-Ouest de *Iraunabako erreka 6 - Dolmen*.

Description

Tumulus pierreux de 5,50m de diamètre et 0,30 de haut.

Historique

Tumulus découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

### [Lecumberry]{.spatial} - [Iraunabako erreka]{.coverage} 8 - [Cromlech]{.type}

Localisation

À 6m au Sud de *Iraunabako erreka 7 - Tumulus*.

Description

De nombreuses grosses pierres enfouies dans le sol délimitent les ¾ d'un cercle de 3,80m de diamètre...

Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

### [Lecumberry]{.spatial} - [Iraunabako erreka]{.coverage} 9 - [Tertre d'habitat]{.type}

Localisation

À 30m au Sud Sud-Ouest de *Iraunabako-erreka 5 - Cromlech*.

Altitude : 938m

Description

Tertre ovalaire, asymétrique de 11m de long, 6m de large et 1,10m de haut dans sa partie la plus élevée.

Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

### [Lecumberry]{.spatial} - [Iraunabako erreka]{.coverage} 10 - [Tertre d'habitat]{.type}

Localisation

Il est à 80m au Sud Sud-Ouest de *Iraunabako-erreka 5 - Cromlech*, au pied de la colline d'Harluze.

Description

Tertre ovalaire asymétrique, mesurant 11m de long, 6m de large et 1,10m de haut.

Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

### [Lecumberry]{.spatial} - [Iraunabako erreka]{.coverage} 11 - [Tumulus-cromlech]{.type}

Localisation

À 20m au Nord de *Iraunabako-erreka 5 - Cromlech*.

Description

Petit tumulus circulaire, discret sous la végétation, mesurant 2,10m de diamètre et limité par 8 pierres périphériques.

Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

### [Lecumberry]{.spatial} - [Iraunabako erreka]{.coverage} 12 - [Tertre d'habitat]{.type}

Localisation

Altitude : 938m.

Il est à 150m au Nord-Ouest de *Iraunabako-erreka 5 - Cromlech*, le long de la piste pastorale.

Description

Tertre ovale de 10m de long, 3m de large et 0,80m de haut.

Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

### [Lecumberry]{.spatial} - [Iraunabako erreka]{.coverage} 13 - [Cromlech]{.type}

Localisation

Altitude : 976m.

Il est situé à la confluence de petits rius qui vont constituer la naissance du rio Iraty, immédiatement en contre bas et au Sud du col d'Irau.

Description

Petit cercle discret, de 3,10m de diamètre, délimité par 8 pierres au ras du sol.

Historique

Monument découvert par [Blot J.]{.creator} en [mai 2016]{.date}.

À noter qu'à une centaine de mètres plus au Nord, là où le vallon se rétrécit, on peut voir une structure très semblable à *T2*, en gros galets, mais de forme imprécise (effet de la nature ou remaniement d'une structure anthropique par l'effet des eaux par exemple ?).

### [Lecumberry]{.spatial} - [Iraubazter vallon]{.coverage} - [Tertre d'habitat]{.type}

Localisation

Altitude : 940m.

Situé près de la confluence du riu Iraubazter avec le rio Irati, en bordure des terres inondées, marécageuses.

Description

Tertre terreux asymétrique de forme classique, mesurant 4m de diamètre et 0,40m de haut.

Historique

Monument découvert par [Blot J.]{.creator} en [juillet 2016]{.date}.

### [Lecumberry]{.spatial} - [Iraubazter vallon]{.coverage} 1 - [Tumulus]{.type}

Localisation

Altitude : 941m.

Il est à 16m au Nord-Ouest de *Iraubazter vallon - Tertre d'habitat*.

Description

Tumulus terreux très net, circulaire en forme de galette, de 4m de diamètre et 0,40m de haut, présentant une légère excavation en son centre (fouille ancienne ?). Quelques pierres sont visibles en surface.

Historique

Tumulus découvert par [Blot J.]{.creator} en [juillet 2016]{.date}.

### [Lecumberry]{.spatial} - [Iraubazter vallon]{.coverage} 2 - [Tumulus]{.type}

Localisation

Altitude : 941m.

Il est à 4m à l'Ouest Nord-Ouest de *Iraubazter vallon 1 - Tumulus*.

Description

Tumulus en forme de galette aplati, de 7m de diamètre et 0,40m de haut, présentant une excavation centrale de 4m de diamètre environ et 0,30m de profondeur. De nombreuses pierres sont visibles en surface.

Historique

Tumulus découvert par [Blot J.]{.creator} en [juillet 2016]{.date}.

### [Lecumberry]{.spatial} - [Iraubazter vallon]{.coverage} 3 - [Tumulus]{.type}

Localisation

Il est situé à 8m au Nord-Ouest de *Iraubazter vallon 2 - Tumulus*.

Description

Petit tumulus circulaire, de forme légèrement conique, de 2,50m de diamètre et 0,40m de haut ; de nombreuses pierres apparaissent en surface.

Historique

Tumulus découvert par [Blot J.]{.creator} en [juillet 2016]{.date}.

### [Lecumberry]{.spatial} - [Jatsélépoa]{.coverage} - [Camp protohistorique]{.type} ([?]{.douteux})

Celui-ci domine au Nord-Est le col de ce nom et, au Nord-Ouest, la ferme Ondarneborda.

Altitude : 463m.

Description

Cet ensemble se répartit en un premier espace situé au plus haut, de forme ovale, entourée d'une levée de terre ayant pu supporter des palissades, auquel on accède directement par le col, et, au Nord-Est, d'une étendue quasi horizontale, où se distinguent nettement des levées de terre ayant pu faire partie d'un système de « pré-défense ».

Historique

Cet ensemble découvert par [Meyrat F.]{.creator} en [novembre 2013]{.date}.

### [Lecumberry]{.spatial} - [Négousaro]{.coverage} - [Tertres d'habitat]{.type} ([?]{.douteux})

Localisation

Altitude : 1040m.

Les 6 tertres sont à flanc de colline, sur terrain très en pente, à une quarantaine de mètres en dessous de l'ancien cayolar de J.P. Lorran [@blotArtzainakBergersBasques1989], et à 40m au-dessus et au Sud du ruisseau en contre bas.

Description

Quatre tertres bien visibles, légèrement ovales, se succèdent, sur une ligne horizontale orientée Sud-Est Nord-Ouest ; leurs dimensions varient de deux à trois mètres de large pour 4m de long et une hauteur de 0,50m en moyenne. À 4m au-dessus du *tertre n°3*, on note un relief discret mais bien visible, reste du tertre n°5, de dimensions semblables ; enfin, au-dessous et à 4m au Nord du *tertre n°4*, on voit les restes du *tertre n°6* (6m de long, 4m de large, 0,20m de haut). Monuments cependant tous très douteux.

Historique

Tertres trouvés par [Blot J.]{.creator} et [Meyrat F.]{.creator} en [août 2013]{.date}.

### [Lecumberry]{.spatial} - [Toscako erreka]{.coverage} - [Tertres d'habitat]{.type}

Localisation

Altitude : 900m.

Les 14 tertres d'habitat sont situés au Sud et en contre-bas de la route qui se rend à Néquécharré (ou Négousaro), un peu avant le virage qui correspond à son franchissement du ruisseau Toscako erreka. On les trouve répartis selon un axe Est-Ouest, le long d'anciennes pistes pastorales dont une se dirige vers les ruines d'un ancien cayolar, près du ruisseau., c'est à partir de ce dernier (coordonnées indiquées ci-dessus) que nous ferons la description des tertres :

Description

Ils sont tous de forme ovale et érigés sur un terrain en pente vers le Sud.

- *Tertre n°1* : situé à 8m au Nord-Ouest des ruines du cayolar. Mesure 13m x 10m et 1m de haut.

- *Tertre n°2* : situé à 75m environ au Nord-Ouest du précédent. Mesure 14m x 8m et 0,50m de haut.

- *Tertre n°3* : situé à 50m à l'Ouest du n°1 et à 35m au Sud du n°2 (découvert par [Meyrat F.]{.creator} en [2014]{.date})

- *Tertre n°4* : situé à 7m au Sud du n°9 et 20m à l'Est du n°1. Mesure 14m x 9m et 1m de haut.

- *Tertre n°5* : situé à 25m au Sud du n°6 et à 7m au Nord du n°4. Mesure 10m x 8m et 0,50m de haut.

- *Tertre n°6* : Situé à environ 20m au Sud du n°9 et 25m au Nord du n°5. Mesure 14m x 9m et 1,50m de haut.

- *Tertre n°7* : Situé à 10m environ à l'Ouest du n°6 et 80m à l'Est du n°2. Mesure 14m x 7m et 0,80m de haut.

- *Tertre n°8* : Situé à 10m à l'Est du n°6. Mesure 10m x 8m et 0,80m de haut.

- *Tertre n°9* : situé à 27m à l'Est du n°10 et 20m au Nord du 6. Mesure 14m x 9m et 0,50m de haut.

- *Tertre n°10* : Situé à 75m au Nord-Est du n°2 et 27m à l'Ouest du n°9. Mesure 8m x 7m et 0,30m de haut.

- *Tertre n°11* : Situé à 30m à l'Est du n°9 ; mesure 13m x 7m et 0,30m de haut.

- *Tertre n°12* : à une trentaine de mètres sous le tracé de la route qui mène à Négousaro et au Nord du 9. Tertre peu marqué.

- *Tertre n°13* : à 20m à l'Est du n°12. - Tertre peu marqué.

- *Tertre n°14* : à une quarantaine de mètres à l'Est du n°13, relief mieux marqué que les 2 précédents ; de nombreuses pierres y sont visibles.

Historique

Tertres découverts par [Bot J.]{.creator} en [1974]{.date}.

## [Lecumberry]{.spatial} - La nécropole d'[Okabé]{.coverage}

La région d'Okabé a fait essentiellement l'objet de 2 publications de P. Gombault [@gombaultTumulusEnceintesFuneraires1914, p. 65] et [@gombaultProposCromlechsOkabe1935, p.391], décrivant les cromlechs de 1 à 17 ; puis nous-mêmes [@blotNouveauxVestigesMegalithiques1972a, p.58] publions les monuments de 18 à 26. En 1978, nous pratiquons une fouille de sauvetage sur le cromlech n° 6 [@blotCromlechOkabeBasse1978, p. 1]. Datation obtenue : 767-216 BC.

Les 6 monument décrits ci-après complètent cet inventaire.

### [Lecumberry]{.spatial} - [Okabé]{.coverage} 27 - [Tumulus]{.type}

Localisation

Carte 1346 Est Saint-Jean-Pied-de-Port.

Altitude : 1340m.

Le *Tumulus 27* est situé à 40m à l'Est Sud-Est du n°11.

Description

Petit amas bien circonscrit d'un diamètre de 2m et 0,30m de haut, formé d'environ une dizaine de pierres blanchâtres, de la taille d'un gros pavé.

Historique

Monument découvert en [octobre 1978]{.date}.

### [Lecumberry]{.spatial} - [Okabé]{.coverage} 28 - [Tumulus-cromlech]{.type}

Localisation

Il est tangent au Nord Nord-Ouest du n°24.

Description

Tumulus terreux de 4,20m de diamètre et 0,30m de haut, délimité par 6 pierres ; une pierre est visible au centre.

Historique

Monument découvert en [octobre 1978]{.date}.

### [Lecumberry]{.spatial} - [Okabé]{.coverage} 29 - [Tumulus]{.type}

Localisation

Il est à 4m au Nord du n°24.

Description

Petit tumulus terreux de 2,20 de diamètre et 0,20 de haut ; 2 pierres sont visibles à sa périphérie en secteur Nord-Ouest.

Historique

Monument découvert en [octobre 1978]{.date}.

### [Lecumberry]{.spatial} - [Okabé]{.coverage} 30 - [Cromlech]{.type}

Localisation

Il est à 12m au Nord-Ouest du n°1.

Description

7 pierres nettement visibles sont disposées en arc de cercle qui représente en réalité la moitié Nord d'un cercle de 5m de diamètre.

Historique

Monument découvert en [octobre 1978]{.date}.

### [Lecumberry]{.spatial} - [Okabé]{.coverage} 31 - [Cromlech]{.type}

Localisation

On le trouve sur un petit replat à 100m au Sud Sud-Est du n°1.

Description

Cercle de 3,50m de diamètre délimité par 7 pierres, au ras du sol, mais bien visibles. Edifié sur un terrain en légère pente vers le Nord-Est.

Historique

Monument découvert en [octobre 1978]{.date}.

### [Lecumberry]{.spatial} - [Okabé]{.coverage} 32 - [Tumulus]{.type}

Localisation

Il est à 10m au Nord-Ouest de *Okabé 31 - Cromlech*.

Description

Petit amas pierreux circulaire, de 2m de diamètre et 0,30m de haut, en partie dissimulé sous la bruyère naine.

Historique

Monument découvert en [mai 1979]{.date}.

### [Lecumberry]{.spatial} - [Okabé]{.coverage} 33 - [Tumulus]{.type}

Localisation

Altitude : 1382m.

Il est à 6m environ au Nord-Ouest de *Okabé 13 - Cromlech*.

Description

Tumulus très peu marqué en hauteur, mesurant 5,30m de diamètre, constitué de nombreuses pierres de faible volume émergeant de la surface du sol.

Historique

Tumulus découvert par [Blot J.]{.creator} en [août 2013]{.date}.
