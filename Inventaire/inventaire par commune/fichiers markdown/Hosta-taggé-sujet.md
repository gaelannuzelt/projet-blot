# Hosta

### [Hosta]{.spatial} - [Belchou]{.coverage} 1 - [Cromlech]{.type}

Localisation

Carte 1446 Ouest Ordiarp.

Altitude : 828m.

On le trouve en bordure Sud Sud-Est de la grande cuvette d'un haut plateau situé Sud Sud-Est au pied du mont Belchou.

Description

Cercle de 4,60m de diamètre formé de 14 blocs de [grès]{.subject} blanc, de la taille d'un à 2 ou même 3 pavés. Cinq pierres apparaissent au centre du cercle.

Historique

Monument découvert en [août 1978]{.date}.

### [Hosta]{.spatial} - [Belchou]{.coverage} 2 - [Tumulus]{.type}

Localisation

Il est à 80m au Nord-Ouest de *Belchou 1 - Cromlech*.

Description

Tumulus mixte de terre et de blocs de [grès]{.subject} blanc très abondant ; la périphérie est très nettement délimitée, sans qu'on puisse parler cependant de péristalithe.

À 10m du Nord-Est de la bordure de la cuvette, on remarque à une quarantaine de mètres au Nord-Est du n°2, une série de *12 tertres d'habitat*, allongés selon un axe Nord-Ouest Sud-Est, avec parfois à leur sommet des pierres disposées en rectangle ou en ovale.

Historique

Monument découvert en [août 1978]{.date}.

### [Hosta]{.spatial} - [Belchou]{.coverage} 3 - [Tumulus]{.type}

Localisation

À 150m au Nord Nord-Ouest de *Belchou 2 - Tumulus*.

Description

Il se présente sous la forme d'une jolie galette de pierres blanches, légèrement tumulaire.

Historique

Monument découvert en [août 1978]{.date}.

### [Hosta]{.spatial} - [Belchou]{.coverage} 4 - [Tumulus]{.type}

Localisation

Sur un replat à flanc de montagne, à 30m au Nord Nord-Ouest de *Belchou 3 - Tumulus*.

Description

Belle petite galette, légèrement surélevée, de pierres blanches, mesurant 3m de diamètre.

Historique

Monument découvert en [août 1978]{.date}.

### [Hosta]{.spatial} - [Belchou]{.coverage} 5 - [Tumulus]{.type}

Localisation

À 100m au Nord Nord-Est de *Belchou 4 - Tumulus*, à gauche de la piste qui monte de la plaine du Belchou, et quasi tangente à elle.

Description

Belle galette de 5m de diamètre et constituée de blocs de [grès]{.subject} de la taille d'un pavé.

Historique

Monument découvert en [août 1978]{.date}.

### [Hosta]{.spatial} - [Belchou]{.coverage} 6 - [Tumulus]{.type}

Localisation

À 40m au Nord Nord-Ouest de *Belchou 5 - Tumulus*, au-dessus de lui sur la pente herbeuse, à gauche de la piste qui monte de la plaine.

Description

Tumulus pierreux en forme de galette, de 4m de diamètre et de quelques centimètres de hauteur.

Historique

Monument découvert en [août 1978]{.date}.

### [Hosta]{.spatial} - [Belchou]{.coverage} 7 - [Cromlech]{.type}

Localisation

Il est situé à 100m à l'Est Nord-Est de *Belchou 1 - Cromlech*, en bordure du relèvement de la cuvette, comme *Belchou 8 - Cromlech* et *Belchou - Monolithe*.

Description

Cercle de pierres de 5m de diamètre, délimité par 22 pierres ; il est érigé sur un sol très légèrement incliné vers le Nord Nord-Ouest.

Historique

Monument découvert en [août 1978]{.date}.

### [Hosta]{.spatial} - [Belchou]{.coverage} 8 - [Cromlech]{.type}

Localisation

À 100m au Nord-Est de *Belchou 7 - Cromlech*, à 830m d'altitude.

Description

Petit cercle de 3,60m de diamètre, délimité par une quinzaine de pierres au ras du sol ; il présente un aspect très légèrement tumulaire.

Historique

Monument découvert en [août 1978]{.date}. *Belchou 9 - Cromlech*, situé à 150m plus à l'Est Nord-Est se trouve sur la commune de [Saint-Just-Ibarre]{.spatial}.

### [Hosta]{.spatial} - [Belchou]{.coverage} - [Monolithe]{.type}

Localisation

Altitude : 970m.

Il repose sur le sol, à droite, dans l'avant-dernier virage de la route qui mène à Lahondoko Olha.

Description

Gros bloc de [schiste]{.subject} gris local, de forme parallélépipédique, allongé selon un axe Est Nord-Est Ouest Sud-Ouest. L'extrémité Est est arrondie, le bord Nord, rectiligne, le bord Sud, un peu plus irrégulier ; on peut noter des traces d'épannelage au sommet et sur le bord Sud. Il mesure 2,77m de long, 1,37m à sa base Ouest et 0,70m au sommet ; son épaisseur varie de 0,50m à 0,63m.

Historique

Monolithe découvert par [F. Meyrat]{.creator} en [août 2011]{.date}.

### [Hosta]{.spatial} - [Belchou]{.coverage} 11 - [Tumulus]{.type}

Localisation

Altitude : 828m.

À la limite Sud-Ouest du haut plateau, au tiers de sa longueur.

Description

Modeste tumulus en forme de galette de faible relief, de 3,50m de diamètre recouverte de nombreux petits blocs de [calcaire]{.subject}.

Historique

Tumulus découvert par [Blot J.]{.creator} en [août 2011]{.date}.

### [Hosta]{.spatial} - [Belchou]{.coverage} 12 - [Cromlech]{.type}

Localisation

Altitude : 828m.

À quelques mètres à l'Est de *Belchou 11 - Tumulus*.

Description

Huit pierres délimitent un cercle de 4,40m de diamètre ; la partie Sud-Ouest en est moins fournie, et on en note 4 dans la région centrale.

Historique

Monument découvert par [Blot J.]{.creator} en [août 2011]{.date}.

### [Hosta]{.spatial} - [Belchou]{.coverage} 13 - [Cromlech]{.type}

Localisation

Altitude : 828m.

Situé à quelques mètres à l'Est de *Belchou 12 - Cromlech*, et sur la ligne intercommunale entre Hosta et Saint-Just-Ibarre.

Description

On ne voit qu'un demi-cercle (de 4,60m de diamètre) de 4 pierres bien visibles dans la moitié Nord. Au Sud, hors du cercle, un bloc de [calcaire]{.subject} de volume plus important attire le regard.

Historique

Monument découvert par [Blot J.]{.creator} en [août 2011]{.date}.

### [Hosta]{.spatial} - [Belchou]{.coverage} 15 - [Cromlech]{.type}

Localisation

Altitude : 830m.

Situé à quelques dizaines de mètres à l'Ouest Nord-Ouest du *groupe de TH* au Sud-Ouest du haut plateau.

Description

Petit cercle de 3m de diamètre bien délimité par 21 pierres très nettes.

Historique

Monument découvert par [F. Meyrat]{.creator} en [août 2011]{.date}.

### [Hosta]{.spatial} - [Belchou]{.coverage} 16 - [Cromlech]{.type}

Localisation

Altitude : 836m.

Il se trouve à 40m au Nord-Ouest du *tumulus-cromlech T2*.

Description

Petit cromlech surélevé de 3m de diamètre, bien délimité par ce qui pourrait être une double couronne de pierres concentriques.

Historique

Monument découvert par [Blot J.]{.creator} en [août 2011]{.date}.

### [Hosta]{.spatial} - [Belchou]{.coverage} 1 (groupe Sud-Ouest - tertres du haut) - [Tertres d'habitats]{.type}

Localisation

Altitude : 840m.

Situé à 40m à l'Ouest Nord-Ouest de *Belchou 16 - Cromlech* à l'extrémité Nord-Ouest du haut plateau, il domine l'ensemble de ce dernier où l'on distingue 2 grands groupes de tertres d'habitat (un au Sud-Ouest, 17 TH et un au Nord-Est, 25 TH)

Description

Tertre de terre ovale, asymétrique parce que construit sur terrain en pente. Il mesure 10m de long et 5m de large ; sa hauteur est de 1m au Nord-Ouest et de 2m au Sud-Est.

On peut distinguer les vestiges d'un autre tertre à 15m au Nord-Ouest, mesurant environ 9m de long.

Historique

Découvert par [Blot J.]{.creator} en [août 2011]{.date}.

### [Hosta]{.spatial} - [Othamunho]{.coverage} - [Dolmen]{.type}

Localisation

Carte 1446 Ouest Ordiarp.

Altitude : 710m.

Description

Il n'y a pas de tumulus visible. La [chambre funéraire]{.subject}, orientée Sud-Ouest Nord-Est, qui mesure 1,80m de long et 0,50m de large, apparaît délimitée seulement par ses 4 dalles : au Sud-Est, une grande dalle de 1,75m de long et 0,33m de haut ; au Nord-Ouest, deux dalles plus petites et au Nord-Est une seule dalle de 0,55m de long. Il n'y a pas de couvercle visible.

Historique

Dolmen découvert en [septembre 1970]{.date}.

### [Hosta]{.spatial} - [Nethé]{.coverage} Nord 1 - [Tumulus]{.type}

Localisation

Altitude : 555m.

Description

Petit tumulus pierreux légèrement oblong, à grand axe Nord-Sud, de 2,80m de diamètre environ et 0,30m de haut.

La pousse d'un arbre (aubépine) à sa périphérie Nord a bouleversé la disposition des pierres (petits blocs de [calcaire]{.subject} blanc), qui ne paraissent cependant pas avoir été disposés dans un ordre particulier ; il semblerait qu'on puisse cependant par endroits distinguer une ébauche de péristalithe...

Historique

Tumulus découvert en [1974]{.date} par [Blot J.]{.creator} Ce tumulus, ainsi que le suivant avait été simplement cité dans le [@blotNouveauxVestigesProtohistoriques1975, p.122]

### [Hosta]{.spatial} - [Nethé]{.coverage} Nord 2 - [Tumulus]{.type}

Localisation

Altitude : 555m.

Il est situé à 4m au Nord-Ouest de *Nethe Nord 1 - Tumulus*. Un abreuvoir récent a été construit à 15m au Sud de ce tumulus.

Description

Petit tumulus pierreux, légèrement oblong, à grand axe orienté Nord-Est Sud-Ouest, de 3,40m de diamètre environ et 0,40m de haut ; une aubépine a poussé en périphérie Sud-Ouest, perturbant la disposition des pierres (petits blocs de calcaire blanc). Il est difficile d'évoquer un péristhalite (?).

Historique

Découvert par [Blot J.]{.creator} en [1974]{.date} et simplement cité dans [@blotNouveauxVestigesProtohistoriques1975, p.122]

### [Hosta]{.spatial} - [Nethé]{.coverage} Nord 3 - [Tumulus]{.type}

Altitude : 763m.

Il est situé à 40m au Nord de la borne géodésique du sommet de Néthé.

Description

Petit tumulus pierreux, très net, de forme oblongue, à grand axe Nord-Est Sud-Ouest, de 2,80m de diamètre et 0,30m de haut. Les blocs de [calcaire]{.subject} blanc ne semblent avoir été disposés dans un ordre particulier.

Historique

Tumulus découvert par [Meyrat F.]{.creator} en [décembre 2015]{.date}.

### [Hosta]{.spatial} - [Nethé]{.coverage} Sud - groupe Nord

Sous la dénomination « Néthé Sud », nous avions simplement cité dans [@blotNouveauxVestigesProtohistoriques1975, p.122] un ensemble de 7 tumulus et 1 tertre d'habitat, découverts par nous en 1974, au niveau du col séparant le mont Néthé au Nord du mont Iramunho au Sud. Ces monuments ont été revisités par F. Meyrat, qui a découvert un nouvel ensemble de 4 tumulus plus un tertre d'habitat, que nous appellerons le *Groupe Sud*, les précédents, déjà découverts par [nous]{.creator} en [1974]{.date}, formant le *Groupe Nord* de l'ensemble dénommé *Nethé Sud*.

Cet ensemble de 8 [tumulus]{.type} et 1 [tertre d'habitat]{.type} est situé à environ 200m au Sud Sud-Est de la ferme *Bordaxarria*, dans une prairie ; les 8 tumulus se succèdent en bordure d'une haie.

- *Tumulus 1* :

    Altitude : 545m.

    Il est situé à une vingtaine de mètres à l'Ouest de la haie. Tumulus mixte, de terre et de pierres, de forme légèrement ovale, mesurant 8m x 7m et 0,50m de haut.

- *Tumulus 2* :

    Il est à 2m au Sud-Est du précédent, et à 20m à l'Ouest de la haie. Tumulus mixte de terre et de pierres, mesurant 7m de diamètre et 0,40m de haut.

- *Tumulus 3* :

    Il est à 4m à l'Est Nord-Est du précédent, et à 8m seulement à l'Ouest de la haie. Tumulus mixte mesurant 8m de diamètre et 0,50m de haut.

- *Tumulus 4* :

    Il est à 4m au Sud-Est du précédent et à 15m environ à l'Ouest de la haie. Tumulus mixte, de forme ovale mesurant 9m x 6m et 0,30m de haut.

- *Tumulus 5* :

    Il est à 5m au du précédent et à 7 m à l'Ouest de la haie, tumulus circulaire mixte de 5m de diamètre et 0,40m de haut.

- *Tumulus 6* :

    Tangent au Sud-Est au précédent, mixte, circulaire, mesure 6m de diamètre et 0,60m de haut.

- *Tumulus 7* :

    Situé à 7m au Sud Sud-Est du précédent et à 8m à l'Ouest de la haie. Tumulus mixte de terre et pierres de forme ovale, mesurant 11m x 8m et 0,40m de haut.

- *Tumulus 8* :

    Situé à 8m au Sud Sud-Est du précédent et à 8m à l'Ouest de la haie.

    Tumulus mixte de forme ovale, mesurant 11m x 8m et 0,50m de haut.

- *Bordaxarria Nord - tertre d'habitat*

    Altitude : 547m.

    Il est situé à une cinquantaine de mètres à l'Ouest Sud-Ouest du précédent, en surélévation.

    Tertre asymétrique, mesurant 16m x 11m et 0,70m de haut.

### [Hosta]{.spatial} - [Nethé]{.coverage} Sud - groupe Sud

Sous la dénomination « Néthé Sud », nous avions simplement cité dans [@blotNouveauxVestigesProtohistoriques1975, p.122] un ensemble de 7 tumulus et 1 tertre d'habitat, découverts par nous en 1974, au niveau du col séparant le mont Néthé au Nord du mont Iramunho au Sud. Ces monuments ont été revisités par [F. Meyrat]{.creator}, qui a découvert un nouvel ensemble de 4 tumulus plus un tertre d'habitat, que nous appellerons le *Groupe Sud*, les précédents, déjà découverts par nous en 1974, formant le *Groupe Nord* de l'ensemble dénommé NETHE SUD.

Cet ensemble de 4 [tumulus]{.type} et 1 [tertre d'habitat]{.type}, se trouve au Sud-Est du précédent ensemble (groupe Nord), de l'autre côté d'une haie, dans un terrain en jachère à la végétation abondante.

- *Tumulus 10*

    Altitude : 555m.

    Tumulus situé tout contre la clôture du terrain. Tumulus mixte, circulaire de 12m de diamètre et 0,40m de haut.

- *Tumulus 11*

    Situé à 4m au Nord du précédent, tumulus mixte, circulaire de 12m de diamètre et 0,40m de haut.

- *Tumulus 12*

    Situé à 10m au Nord-Ouest du précédent, tumulus mixte, circulaire de 12m de diamètre et 0,40m de haut.

- *Tumulus 13*

    Situé à 6m au Nord-Est du précédent ; tumulus mixte, ovale, mesurant 12m x 10m et 0,50m de haut.

- *Bordaxarria Sud - Tertre d'habitat*

    Tertre asymétrique situé à environ 200m à l'Est Sud-Est du *tumulus 10*.

    Altitude : 542m.

    Ce tertre mesure 13m x 11m et 0,90m de haut.
