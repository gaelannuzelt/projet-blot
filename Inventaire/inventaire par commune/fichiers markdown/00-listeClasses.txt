
 notices de monuments

---------------------------------------
###############  Notes  ###############
---------------------------------------

{.spatial}
{.coverage}
{.type}
{.creator}
{.date}
{.isReferencedBy}
{.subject}
{.douteux}

-??----??----??----??----??----??----??

[]{.references} = lien monument blot ?

---------------------------------------
SUJETS
([?]{.douteux})
[dolmen]{.subject}
[chambre funéraire]{.subject}
[couverture]{.subject}
[ciste]{.subject}
[dolmen]{.subject}
[cairn]{.subject}

[grès]{.subject}
[calcaire]{.subject}
[schiste]{.subject}
[quartzite]{.subject}
[triasique]{.subject}

---------------------------------------

[Urrugne]{.spatial} - 

---------------------------------------

`(ref)` --> refs non-bibtex 

- Aldude-taggé.md : FAIT
- Banca : 2
- Bidarray
- Biriatou
- Garindein
- Itxassou
- Larrau

---------------------------------------
pour Julien : 

---------------------------------------
_Collections_

Aldude 
    ## Les cromlechs d'Arguibel

Lecumberry :
    ## [Lecumberry] - La nécropole d'[Okabé]

Osses :
    ## Horza : les cromlechs d'Horza

Ostabat :
    ## Gagneko Ordokia : les tertres pierreux

Saint-Etienne-de-Baigorri :
    ## Les monuments du secteur de l'antenne relai d'Urxilo
    ## Les monuments du secteur des Cabanes
    ## Les monuments du secteur du Saroi
    ## Les monuments du secteur des Poteaux
Sare :
    ## Les cromlechs de Gastenbakarre
    ## Les 3 dolmens de Gaztenbakarreko erreka
    ## Cromlechs et tumulus de Kondendiagako lepoa

---------------------------------------
[...] --> texte à vérifier

- Ostabat

---------------------------------------

`##### sous-monument`
OU
    `#### sous-monument` (et `##### Localisation`, description, historique (voir css `h4` --> `h5`))
Finalement : 
    `- *Sous-monument*`

---------------------------------------

<monument>
</monument>

-------------------

<article>
</article>

---------------------------------------
```bash
find . -name "*.md" | while read i; do pandoc -o ../Sorties/Unique.md --self-contained -s --toc "$i"; done
```

```bash
pandoc --wrap=none -o Sortie/final.md *.md
```

pandoc

---------------------------------------
### liste des clés bibtex

#### fil du texte

blotArcheologieMontagneBasque1993a
blotSouleSesVestiges1979
blotSouleSesVestiges1979
blotMonolithesPaysBasque1983
blotInventaireMonumentsProtohistoriques2011
blotInventaireMonumentsProtohistoriques2011
blotInventaireMonumentsProtohistoriques2011
barandiaranProspecionesExcavacionesPrehistoricas1962
barandiaranHombrePrehistoricoPais1953
barandiaranCronicaPrehistoriaBaigorri1949
blotTumuluscromlechZahoII1989
berdoyValleeBaretousDepuis1990
blotSouleSesVestiges1979
blotSouleSesVestiges1979
blotInventaireMonumentsProtohistoriques2009
blotNouveauxVestigesMegalithiques1972b
blotVestigesProtohistoriquesVoie1978
blotNouveauxVestigesMegalithiques1971
blotNouveauxVestigesMegalithiques1971
barandiaranContribucionEstudioCromlechs1949
veyrinCromlechsPierresLevees1935
blotNouveauxVestigesMegalithiques1971
blotNouveauxVestigesMegalithiques1971
barandiaranHombrePrehistoricoPais1953
blotInventaireMonumentsProtohistoriques2013
ebrard50AnsArcheologie2013
blotSouleSesVestiges1979
blotSouleSesVestiges1979
blotSouleSesVestiges1979
blotNouveauxVestigesMegalithiques1972b
blotNouveauxVestigesMegalithiques1972b
blotNouveauxVestigesMegalithiques1972b
blotNouveauxVestigesMegalithiques1973a
barandiaranHombrePrehistoricoPais1953a
blotNouveauxVestigesMegalithiques1972b
blotTumuluscromlechMehatzeMehatze1983
blotNouveauxVestigesMegalithiques1972b
blotNouveauxVestigesMegalithiques1972b
blotNouveauxVestigesMegalithiques1972b
blotNouveauxVestigesMegalithiques1972b
blotSouleSesVestiges1979
blotMonolithesPaysBasque1983
blotNouveauxVestigesMegalithiques1972a
barandiaranContribucionEstudioCromlechs1949
chauchatSeptDolmensNouveaux1966
blotNouveauxVestigesMegalithiques1972
blotTumuluscromlechUgatzePic1974
blotNouveauxVestigesProtohistoriques1975
blotSouleSesVestiges1979
blotNouveauxVestigesMegalithiques1973a
blotNouveauxVestigesProtohistoriques1975
blotNouveauxVestigesProtohistoriques1975
blotNouveauxVestigesProtohistoriques1975
blotNouveauxVestigesProtohistoriques1975
blotInventaireMonumentsProtohistoriques2009
blotInventaireMonumentsProtohistoriques2009
blotNouveauxVestigesMegalithiques1973
blotCromlechMeatseItxassou1970
chauchatNecropolePrehistoriqueCol1977
blotCromlechUniteMeatse1979
blotCromlechUniteMeatse1979
blotCromlechMeatse121996a
blotRapportSondagesArcheologiques1995
blotBaratzCerclePierres2002
blotRapportSondagesArcheologiques1996
blotCromlechMeatseCommune1997
blotSondagesArcheologiquesAu1998
blotContributionEtudeCercles1995
blotMessageArchitecturesProtohistoriques2003
barandiaranContribucionEstudioCromlechs1949
blotNouveauxVestigesMegalithiques1972c
blotInventaireMonumentsProtohistoriques2013d
blotNouveauxVestigesMegalithiques1972b
blotNouveauxVestigesMegalithiques1972b
blotTumulusBixustiaZuhamendi1976
blotTumulusBixustiaZuhamendi1976
blotTumulusBixustiaZuhamendi1976
blotSouleSesVestiges1979
blotSouleSesVestiges1979
blotSouleSesVestiges1979
blotCromlechUniteMeatse1979
blotSouleSesVestiges1979
barandiaranProspecionesExcavacionesPrehistoricas1962
blotSouleSesVestiges1979
blotSouleSesVestiges1979
blotSouleSesVestiges1979
boucherNotesProspectionMegalithique1968
blotTumuluscromlechMillagateIV1990
blotTumuluscromlechMillagateCompte1991
barandiaranHombrePrehistoricoPais1953
blotSouleSesVestiges1979
blotNouveauxVestigesMegalithiques1972b
blotNouveauxVestigesMegalithiques1972b
blotNecropoleProtohistoriqueApatessaro1986
blotCromlechsApatessaro1bis1986
blotTumulusApatessaroIV1988
blotTumulusApatessaroCompte1988
blotTumulusApatesaroVI1994
ebrardDolmenArtxilondoLecumberry2000
blotNouveauxVestigesMegalithiques1972a
barandiaranHombrePrehistoricoPais1953
blotNouveauxVestigesMegalithiques1972b
blotTumulusIrauIV1992
blotArtzainakBergersBasques1989
gombaultTumulusEnceintesFuneraires1914
gombaultProposCromlechsOkabe1935
blotNouveauxVestigesMegalithiques1972a
blotCromlechOkabeBasse1978
blotMonolithesPaysBasque1983
blotNouveauxVestigesProtohistoriques1975
blotNouveauxVestigesMegalithiques1972a
barandiaranCronicaPrehistoria1952
rocqEtudePeuplementPays1935
blotSouleSesVestiges1979
blotNouveauxVestigesProtohistoriques1975
barandiaranProspecionesExcavacionesPrehistoricas1962
blotNouveauxVestigesMegalithiques1972c
blotInventaireMonumentsProtohistoriques2014
blotInventaireMonumentsProtohistoriques2015
barandiaranContribucionEstudioCromlechs1949
barandiaranProspecionesExcavacionesPrehistoricas1962
blotNouveauxVestigesMegalithiques1972c
blotInventaireMonumentsProtohistoriques2009
blotNouveauxVestigesProtohistoriques1975
blotNouveauxVestigesProtohistoriques1975
blotNouveauxVestigesProtohistoriques1975
blotSouleSesVestiges1979
blotInventaireMonumentsProtohistoriques2018
blotNouveauxVestigesMegalithiques1972a
blotInventaireMonumentsProtohistoriques2009
barandiaranCronicaPrehistoriaBaigorri1949
blotNouveauxVestigesMegalithiques1972b
blotNouveauxVestigesMegalithiques1972b
blotNouveauxVestigesMegalithiques1973
blotInventaireMonumentsProtohistoriques2009
blotNouveauxVestigesMegalithiques1972c
blotNouveauxVestigesMegalithiques1972c
blotNouveauxVestigesMegalithiques1972c
blotNouveauxVestigesMegalithiques1972c
blotNouveauxVestigesMegalithiques1972c
blotInventaireMonumentsProtohistoriques2009
blotNouveauxVestigesMegalithiques1972b
blotVestigesProtohistoriquesVoie1978
boucherNotesProspectionMegalithique1968
blotEtatConnaissancesArcheologiques1978
blotNouveauxVestigesMegalithiques1972b
blotInventaireMonumentsProtohistoriques2012
blotNouveauxVestigesMegalithiques1972b
blotVestigesProtohistoriquesVoie1978
blotInventaireMonumentsProtohistoriques2018
blotVestigesProtohistoriquesVoie1978
blotVestigesProtohistoriquesVoie1978
blotTumulusBixustiaZuhamendi1976
blotInventaireMonumentsProtohistoriques2011
blotTumulusBixustiaZuhamendi1976
barandiaranCatalogueStationsPrehistoriques1946
blotNouveauxVestigesMegalithiques1971
blotNouveauxVestigesMegalithiques1971
blotNouveauxVestigesMegalithiques1971
barandiaranContribucionEstudioCromlechs1949
barandiaranHombrePrehistoricoPais1953
blotInventaireMonumentsProtohistoriques2009
barandiaranCronicaPrehistoriaPirineo1951
barandiaranHombrePrehistoricoPais1953
blotMonolithesPaysBasque1983
blotMonolithesPaysBasque1983
blotInventaireMonumentsProtohistoriques2013
blotInventaireMonumentsProtohistoriques2011
blotNouveauxVestigesMegalithiques1971
barandiaranCatalogueStationsPrehistoriques1946
chauchatSeptDolmensNouveaux1966
blotMonolitheFaagueTable2009
blotInventaireMonumentsProtohistoriques2010
blotInventaireMonumentsProtohistoriques2011
barandiaranCronicaPrehistoriaPirineo1951a
blotNouveauxVestigesMegalithiques1971
blotTumulusBixustiaZuhamendi1976
blotSouleSesVestiges1979
blotNouveauxVestigesProtohistoriques1975
blotNouveauxVestigesProtohistoriques1975
blotInventaireMonumentsProtohistoriques2009
barandiaranCronicaPrehistoriaAlduides1949
blotNouveauxVestigesMegalithiques1972b
blotInventaireMonumentsProtohistoriques2009
blotNouveauxVestigesMegalithiques1972b
blotInventaireMonumentsProtohistoriques2009
blotInventaireMonumentsProtohistoriques2009
blotInventaireMonumentsProtohistoriques2010
chauchatSeptDolmensNouveaux1966
chauchatSeptDolmensNouveaux1966
blotInventaireMonumentsProtohistoriques2009
chauchatSeptDolmensNouveaux1966
barandiaranHombrePrehistoricoPais1953
barandiaranProspecionesExcavacionesPrehistoricas1962
blotInventaireMonumentsProtohistoriques2010
blotNouveauxVestigesMegalithiques1972c
chauchatSeptDolmensNouveaux1966
chauchatSeptDolmensNouveaux1966
chauchatSeptDolmensNouveaux1966

#### triées

- barandiaranCatalogueStationsPrehistoriques1946
  barandiaranCatalogueStationsPrehistoriques1946

- barandiaranContribucionEstudioCromlechs1949
  barandiaranContribucionEstudioCromlechs1949
  barandiaranContribucionEstudioCromlechs1949
  barandiaranContribucionEstudioCromlechs1949
  barandiaranContribucionEstudioCromlechs1949

- barandiaranCronicaPrehistoria1952

- barandiaranCronicaPrehistoriaAlduides1949

- barandiaranCronicaPrehistoriaBaigorri1949
  barandiaranCronicaPrehistoriaBaigorri1949

- barandiaranCronicaPrehistoriaPirineo1951

- barandiaranCronicaPrehistoriaPirineo1951a

- barandiaranHombrePrehistoricoPais1953
  barandiaranHombrePrehistoricoPais1953
  barandiaranHombrePrehistoricoPais1953
  barandiaranHombrePrehistoricoPais1953
  barandiaranHombrePrehistoricoPais1953
  barandiaranHombrePrehistoricoPais1953
  barandiaranHombrePrehistoricoPais1953
  barandiaranHombrePrehistoricoPais1953

- barandiaranProspecionesExcavacionesPrehistoricas1962
  barandiaranProspecionesExcavacionesPrehistoricas1962
  barandiaranProspecionesExcavacionesPrehistoricas1962
  barandiaranProspecionesExcavacionesPrehistoricas1962
  barandiaranProspecionesExcavacionesPrehistoricas1962

- berdoyValleeBaretousDepuis1990

- blotArcheologieMontagneBasque1993a

- blotArtzainakBergersBasques1989

- blotBaratzCerclePierres2002

- blotContributionEtudeCercles1995

- blotCromlechMeatse121996a

- blotCromlechMeatseCommune1997

- blotCromlechMeatseItxassou1970

- blotCromlechOkabeBasse1978

- blotCromlechUniteMeatse1979
  blotCromlechUniteMeatse1979
  blotCromlechUniteMeatse1979

- blotCromlechsApatessaro1bis1986

- blotEtatConnaissancesArcheologiques1978

- blotInventaireMonumentsProtohistoriques2009
  blotInventaireMonumentsProtohistoriques2009
  blotInventaireMonumentsProtohistoriques2009
  blotInventaireMonumentsProtohistoriques2009
  blotInventaireMonumentsProtohistoriques2009
  blotInventaireMonumentsProtohistoriques2009
  blotInventaireMonumentsProtohistoriques2009
  blotInventaireMonumentsProtohistoriques2009
  blotInventaireMonumentsProtohistoriques2009
  blotInventaireMonumentsProtohistoriques2009
  blotInventaireMonumentsProtohistoriques2009
  blotInventaireMonumentsProtohistoriques2009
  blotInventaireMonumentsProtohistoriques2009

- blotInventaireMonumentsProtohistoriques2010
  blotInventaireMonumentsProtohistoriques2010
  blotInventaireMonumentsProtohistoriques2010

- blotInventaireMonumentsProtohistoriques2011
  blotInventaireMonumentsProtohistoriques2011
  blotInventaireMonumentsProtohistoriques2011
  blotInventaireMonumentsProtohistoriques2011
  blotInventaireMonumentsProtohistoriques2011
  blotInventaireMonumentsProtohistoriques2011

- blotInventaireMonumentsProtohistoriques2012
  
- blotInventaireMonumentsProtohistoriques2013
  blotInventaireMonumentsProtohistoriques2013
  blotInventaireMonumentsProtohistoriques2013

- blotInventaireMonumentsProtohistoriques2014

- blotInventaireMonumentsProtohistoriques2015

- blotInventaireMonumentsProtohistoriques2018
  blotInventaireMonumentsProtohistoriques2018

- blotMessageArchitecturesProtohistoriques2003

- blotMonolitheFaagueTable2009

- blotMonolithesPaysBasque1983
  blotMonolithesPaysBasque1983
  blotMonolithesPaysBasque1983
  blotMonolithesPaysBasque1983
  blotMonolithesPaysBasque1983

- blotNecropoleProtohistoriqueApatessaro1986

- blotNouveauxVestigesMegalithiques1971
  blotNouveauxVestigesMegalithiques1971
  blotNouveauxVestigesMegalithiques1971
  blotNouveauxVestigesMegalithiques1971
  blotNouveauxVestigesMegalithiques1971
  blotNouveauxVestigesMegalithiques1971
  blotNouveauxVestigesMegalithiques1971
  blotNouveauxVestigesMegalithiques1971
  blotNouveauxVestigesMegalithiques1971

blotNouveauxVestigesMegalithiques1972

blotNouveauxVestigesMegalithiques1972a
blotNouveauxVestigesMegalithiques1972a
blotNouveauxVestigesMegalithiques1972a
blotNouveauxVestigesMegalithiques1972a
blotNouveauxVestigesMegalithiques1972a

blotNouveauxVestigesMegalithiques1972b
blotNouveauxVestigesMegalithiques1972b
blotNouveauxVestigesMegalithiques1972b
blotNouveauxVestigesMegalithiques1972b
blotNouveauxVestigesMegalithiques1972b
blotNouveauxVestigesMegalithiques1972b
blotNouveauxVestigesMegalithiques1972b
blotNouveauxVestigesMegalithiques1972b
blotNouveauxVestigesMegalithiques1972b
blotNouveauxVestigesMegalithiques1972b
blotNouveauxVestigesMegalithiques1972b
blotNouveauxVestigesMegalithiques1972b
blotNouveauxVestigesMegalithiques1972b
blotNouveauxVestigesMegalithiques1972b
blotNouveauxVestigesMegalithiques1972b
blotNouveauxVestigesMegalithiques1972b
blotNouveauxVestigesMegalithiques1972b
blotNouveauxVestigesMegalithiques1972b
blotNouveauxVestigesMegalithiques1972b
blotNouveauxVestigesMegalithiques1972b
blotNouveauxVestigesMegalithiques1972b

blotNouveauxVestigesMegalithiques1972c
blotNouveauxVestigesMegalithiques1972c
blotNouveauxVestigesMegalithiques1972c
blotNouveauxVestigesMegalithiques1972c
blotNouveauxVestigesMegalithiques1972c
blotNouveauxVestigesMegalithiques1972c
blotNouveauxVestigesMegalithiques1972c
blotNouveauxVestigesMegalithiques1972c
blotNouveauxVestigesMegalithiques1972c

- blotNouveauxVestigesMegalithiques1973
  blotNouveauxVestigesMegalithiques1973

- blotNouveauxVestigesMegalithiques1973a
  blotNouveauxVestigesMegalithiques1973a

- blotNouveauxVestigesProtohistoriques1975
  blotNouveauxVestigesProtohistoriques1975
  blotNouveauxVestigesProtohistoriques1975
  blotNouveauxVestigesProtohistoriques1975
  blotNouveauxVestigesProtohistoriques1975
  blotNouveauxVestigesProtohistoriques1975
  blotNouveauxVestigesProtohistoriques1975
  blotNouveauxVestigesProtohistoriques1975
  blotNouveauxVestigesProtohistoriques1975
  blotNouveauxVestigesProtohistoriques1975
  blotNouveauxVestigesProtohistoriques1975
  blotNouveauxVestigesProtohistoriques1975

- blotRapportSondagesArcheologiques1995

- blotRapportSondagesArcheologiques1996

- blotSondagesArcheologiquesAu1998

- blotSouleSesVestiges1979
  blotSouleSesVestiges1979
  blotSouleSesVestiges1979
  blotSouleSesVestiges1979
  blotSouleSesVestiges1979
  blotSouleSesVestiges1979
  blotSouleSesVestiges1979
  blotSouleSesVestiges1979
  blotSouleSesVestiges1979
  blotSouleSesVestiges1979
  blotSouleSesVestiges1979
  blotSouleSesVestiges1979
  blotSouleSesVestiges1979
  blotSouleSesVestiges1979
  blotSouleSesVestiges1979
  blotSouleSesVestiges1979
  blotSouleSesVestiges1979
  blotSouleSesVestiges1979
  blotSouleSesVestiges1979
  blotSouleSesVestiges1979

- blotTumulusApatesaroVI1994

- blotTumulusApatessaroCompte1988

- blotTumulusApatessaroIV1988

- blotTumulusBixustiaZuhamendi1976
  blotTumulusBixustiaZuhamendi1976
  blotTumulusBixustiaZuhamendi1976
  blotTumulusBixustiaZuhamendi1976
  blotTumulusBixustiaZuhamendi1976
  blotTumulusBixustiaZuhamendi1976

- blotTumulusIrauIV1992

- blotTumuluscromlechMehatzeMehatze1983

- blotTumuluscromlechMillagateCompte1991

- blotTumuluscromlechMillagateIV1990

- blotTumuluscromlechUgatzePic1974

- blotTumuluscromlechZahoII1989

- blotVestigesProtohistoriquesVoie1978
  blotVestigesProtohistoriquesVoie1978
  blotVestigesProtohistoriquesVoie1978
  blotVestigesProtohistoriquesVoie1978
  blotVestigesProtohistoriquesVoie1978

- boucherNotesProspectionMegalithique1968
  boucherNotesProspectionMegalithique1968

- chauchatNecropolePrehistoriqueCol1977

- chauchatSeptDolmensNouveaux1966
  chauchatSeptDolmensNouveaux1966
  chauchatSeptDolmensNouveaux1966
  chauchatSeptDolmensNouveaux1966
  chauchatSeptDolmensNouveaux1966
  chauchatSeptDolmensNouveaux1966
  chauchatSeptDolmensNouveaux1966
  chauchatSeptDolmensNouveaux1966

- ebrard50AnsArcheologie2013

- ebrardDolmenArtxilondoLecumberry2000

- gombaultProposCromlechsOkabe1935

- gombaultTumulusEnceintesFuneraires1914

- rocqEtudePeuplementPays1935

- veyrinCromlechsPierresLevees1935

---------------------------



