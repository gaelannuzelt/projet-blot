# Garindein

### [Garindein]{.spatial} - [Tibarenne]{.coverage} Ouest 3 - [Tumulus]{.type}

Localisation

Altitude : 474m.

Tumulus situé tout en bordure et au Sud de la piste pastorale de crête.

Description

Petit tumulus circulaire de 3,80m de diamètre et environ 0,40m de haut. On distingue environ 7 blocs de [calcaire]{.subject} blanc de dimensions assez importantes (0,42m x 0,27m x 0,8m), accumulés au centre, mais qui pourraient y avoir été rapportés ultérieurement à l'édification du tumulus originel.

À propos des autres tumulus de Tibarenne Ouest (T1 et T2), nous voudrions ajouter ces récentes constatations. Depuis l'époque de notre première prospection (ref)(Blot J. 1974) et de notre publication de ces monuments dans [@blotNouveauxVestigesProtohistoriques1975 p.123], ces lieux ont été transformés en prairies artificielles occasionnant d'importants dégâts au niveau des monuments. C'est ainsi que le tumulus :

- *Tibarenne Ouest T1* mesurant 13m de diamètre et 0,80m de haut a été complètement arasé ; il n'en reste rien.

- Le tumulus *Tibarenne Ouest T2* : 
    
    Altitude : 459m ; 
    
    A lui aussi été en grande partie arasé ; toutefois on distingue encore un léger relief de 16m à 18m de diamètre et 0,20m à 0,30m de haut au centre duquel on peut voir les restes de la chambre funéraire. Celle-ci devait mesurer 1,15m de long et 0,55m de large, orientée Nord-Sud, et on en voit encore deux dalles jointives délimitant sa paroi Ouest (la plus grande mesurant 0,58m de long et 0,11m d'épaisseur) et une autre dalle au Sud (0,14m x 0,21m).

Historique

Tumulus découvert par [J. Blot]{.creator} en [avril 2016]{.date}.
