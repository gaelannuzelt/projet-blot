# Iholdy

### [Iholdy]{.spatial} - [Haraneko Ithurria]{.coverage} - [Dolmen]{.type}

Localisation

Altitude : 415m.

Situé à l'extrémité de la petite colline dont le sommet pointe à 453m, on le voit à 15m à l'Ouest du chemin de crête et tangent au Sud, à une clôture qui empiète en partie sur lui.

Description

Ce volumineux [tumulus]{.subject} circulaire de 8m de diamètre et 0,60m de haut est constitué de pierraille de la taille d'un pavé ou plus ; ces éléments sont éparpillés sans ordre apparent. On note une légère dépression centrale de 1,50m de diamètre, probablement trace d'une ancienne fouille clandestine et, sur le flanc Sud Sud-Est du tumulus, une dalle inclinée mesurant 0,57m x 0,50m qui pourrait être un reliquat de la chambre funéraire démolie.

Les habitants de la région n'ont pas mémoire d'un cayolar ou d'une bergerie à cet endroit.

Historique

Monument découvert par [F. Meyrat]{.creator} en [novembre 2014]{.date}.

### [Iholdy]{.spatial} - [Hocha Handia]{.coverage} 2 - [Tumulus]{.type} ([?]{.douteux})

Localisation

Altitude : 554m.

Il se trouve sur le flanc Sud-Ouest du mont Hocha Handia, à l'amorce de la montée, au Nord de la cote 549.

Description

Tumulus de faible relief, que nous rattacherions à la catégorie des *tertres pierreux* fréquents dans ces collines.

Historique

Tumulus découvert par le [groupe Ilharriak]{.creator} en [février 2013]{.date}.

### [Iholdy]{.spatial} - [Hocha Handia]{.coverage} 3 - [Tumulus]{.type} ([?]{.douteux})

Localisation

Altitude : 550m.

À peu de distance de *Hocha Handia 2 - Tumulus (?)*, très discret, *tertre pierreux* ou origine naturelle ?

Historique

Tumulus découvert par le [groupe Ilharriak]{.creator} en [février 2013]{.date}.

### [Iholdy]{.spatial} - [Hocha Handia]{.coverage} 3 - [Tumulus]{.type}

Localisation

Altitude : 550m

Description

Ce petit tumulus pierreux est du même type que ceux de cette région du Lantabat, au sens vaste du terme ; il mesure 3,5m de diamètre et 0,20m de haut.

### [Iholdy]{.spatial} - [Hocha Handia]{.coverage} 4 - [Tumulus]{.type}

Localisation

Altitude : 553 m.

Situé à 4m au Sud de la piste de crête, et sur une rupture de pente, de sorte que le tumulus à tendance à s'effondrer de ce côté.

Description

Tumulus mixte, avec de nombreuses pierres apparentes, il mesure 5,50m de diamètre et environ 0,50 de haut.

Historique

Tumulus découvert par [F. Meyrat]{.creator} en [novembre 2014]{.date}.

### [Iholdy]{.spatial} - [Hocha Handia]{.coverage} 5 - [Tumulus]{.type}

Localisation

Contigu à *Hocha Handia 4 - Tumulus*, au Nord-Est.

Description

De dimension inférieure au précédent, difficile à préciser environ 2m et 0,15m de haut.

Historique

Tumulus découvert par [F. Meyrat]{.creator} en [novembre 2014]{.date}.

### [Iholdy]{.spatial} - [Hocha Handia]{.coverage} 6 - [Tumulus]{.type}

Localisation

Altitude : 554m.

Ces tumulus *Hocha Handia n°6* à *10* sont situés environ 200m à l'Est, sur un vaste plateau qui précède la montée au sommet d'Hocha Handia. Ils sont alignés, en deux groupes de 3, le long ou au milieu de deux filons rocheux de calcaire blanc, parallèles, à fleur de terre et, pour le premier, (*T6*) à environ 25m au Sud Sud-Ouest du chemin de crête.

Description

Tumulus circulaire mixte (terre et petits blocs de calcaire blanc éparpillés visibles en surface) de 3,70m de diamètre et de très faible hauteur (0,30m environ).

Historique

Tumulus découvert par [F. Meyrat]{.creator} en [novembre 2014]{.date}.

### [Iholdy]{.spatial} - [Hocha Handia]{.coverage} 7 - [Tumulus]{.type}

Localisation

À 5m à l'Est Sud-Est de *Hocha Handia 6 - Tumulus*.

Ces tumulus *Hocha Handia n°6* à *10* sont situés environ 200m à l'Est, sur un vaste plateau qui précède la montée au sommet d'Hocha Handia. Ils sont alignés, en deux groupes de 3, le long ou au milieu de deux filons rocheux de calcaire blanc, parallèles, à fleur de terre et, pour le premier, (*T6*) à environ 25m au Sud Sud-Ouest du chemin de crête.

Description

Petit tumulus mixte de 2,50m de diamètre et 0,15m de haut.

Historique

Tumulus découvert par [F. Meyrat]{.creator} en [novembre 2014]{.date}.

### [Iholdy]{.spatial} - [Hocha Handia]{.coverage} 8 - [Tumulus]{.type}

Localisation

Altitude : 556m.

Situé à 18m au Sud Sud-Est de *Hocha Handia 7 - Tumulus*.

Ces tumulus *Hocha Handia n°6* à *10* sont situés environ 200m à l'Est, sur un vaste plateau qui précède la montée au sommet d'Hocha Handia. Ils sont alignés, en deux groupes de 3, le long ou au milieu de deux filons rocheux de calcaire blanc, parallèles, à fleur de terre et, pour le premier, (*T6*) à environ 25m au Sud Sud-Ouest du chemin de crête.

Description

Très semblable aux précédents (1,70m de diamètre et 0,15m de haut). [Monument douteux]{.douteux}.

Historique

Tumulus découvert par [F. Meyrat]{.creator} en [novembre 2014]{.date}.

### [Iholdy]{.spatial} - [Hocha Handia]{.coverage} 9 - [Tumulus]{.type}

Localisation

Altitude : 557m.

Il est situé à 37m au Nord-Est de *Hocha Handia 8 - Tumulus*.

Ces tumulus *Hocha Handia n°6* à *10* sont situés environ 200m à l'Est, sur un vaste plateau qui précède la montée au sommet d'Hocha Handia. Ils sont alignés, en deux groupes de 3, le long ou au milieu de deux filons rocheux de calcaire blanc, parallèles, à fleur de terre et, pour le premier, (*T6*) à environ 25m au Sud Sud-Ouest du chemin de crête.

Description

Petit tumulus pierreux, semblables aux précédents, mesurant 3,10m de diamètre, 0,15m de haut.

Historique

Tumulus découvert par [F. Meyrat]{.creator} en [février 2015]{.date}.

### [Iholdy]{.spatial} - [Hocha Handia]{.coverage} 10 - [Tumulus]{.type}

Localisation

Altitude : 557m.

À 14m au Nord-Ouest de *Hocha Handia 9 - Tumulus*.

Ces tumulus *Hocha Handia n°6* à *10* sont situés environ 200m à l'Est, sur un vaste plateau qui précède la montée au sommet d'Hocha Handia. Ils sont alignés, en deux groupes de 3, le long ou au milieu de deux filons rocheux de calcaire blanc, parallèles, à fleur de terre et, pour le premier, (*T6*) à environ 25m au Sud Sud-Ouest du chemin de crête.

Description

Même type de tumulus mixte, mesurant 2,70m de diamètre et 0,10m de haut.

Historique

Tumulus découvert par [F. Meyrat]{.creator} en [février 2015]{.date}.

### [Iholdy]{.spatial} - [Hocha Handia]{.coverage} 11 - [Tumulus]{.type}

Localisation

Altitude : 557m.

Il est à 9m au Nord-Ouest de Hocha Handia 10 - Tumulus

Ces tumulus *Hocha Handia n°6* à *10* sont situés environ 200m à l'Est, sur un vaste plateau qui précède la montée au sommet d'Hocha Handia. Ils sont alignés, en deux groupes de 3, le long ou au milieu de deux filons rocheux de calcaire blanc, parallèles, à fleur de terre et, pour le premier, (*T6*) à environ 25m au Sud Sud-Ouest du chemin de crête.

Description

Petit tumulus pierreux mixte de 2,20m de diamètre et 0,10m de haut.

Historique

Tumulus découvert par [F. Meyrat]{.creator} en [février 2015]{.date}.

### [Iholdy]{.spatial} - [Laparzale]{.coverage} Nord 1 et 2 - [tertres pierreux]{.type}

Localisation

Sur la partie Nord du plateau de Laparzale, à 150m environ à l'Est de la route, vers l'extrémité Nord du plateau.

Altitude : 347m.

Description

Deux tertres pierreux tangents : le plus au Sud mesure 6m de diamètre, le second tangent au Nord, 7m ; hauteur approximative des deux : 0,30m.

Ces tertres pierreux à l'aspect assez particulier, dans cette région du Lantabat, ne sont pas des tertres d'habitat, pourraient être de tas d'épierrage et même éventuellement des tumulus à vocation funéraire, au moins pour certains.

Historique

Tertres déjà vus en [1975]{.date} et retrouvés par [J. Blot]{.creator} en 2013.

### [Iholdy]{.spatial} - [Laparzale]{.coverage} Nord 3 - [tertres pierreux]{.type}

Localisation

Altitude : 347m.

Il est à environ 60m au Nord Nord-Est de *Laparzale 1 et 2 Nord - tertres pierreux*

Description

Tertre pierreux de 6m de diamètre et 0,40m de haut.

Historique

Tertre retrouvé en [2013]{.date} par [J. Blot]{.creator}.

### [Iholdy]{.spatial} - [Mendibile]{.coverage} haut - [demi-cercle]{.type}

Localisation

Altitude : 457m.

Il est situé à l'extrémité Est du sommet de cette colline.

Description

Demi-cercle délimité par une trentaine de pierres, au ras du sol, apparaissant dans le secteur Sud du cercle.

Historique

Monument découvert par [Balere]{.creator} en [mars 2013]{.date}.

### [Iholdy]{.spatial} - [Mendibile]{.coverage} Nord 1 - [tertre pierreux]{.type}

Localisation

Sur le plateau qui s'étend au Nord de la colline de ce nom. Nous y avions décrits 6 tertres pierreux en 1975 [@blotNouveauxVestigesProtohistoriques1975 p. 117]. Lors de visite en 2013 nous n'en n'avons retrouvé que 3. (La moitié Ouest du plateau ayant été transformée en prairie artificielle, le reste étant envahi par une végétation très dense...).

Altitude : 310m.

Description

Tertre pierreux érigé sur un terrain en légère pente vers le Nord, mesurant 7m de diamètre et 0,40m de haut.

Historique

Tertre retrouvé par [J. Blot]{.creator} en [2013]{.date}.

### [Iholdy]{.spatial} - [Mendibile]{.coverage} Nord 2 - [tertre pierreux]{.type}

Localisation

Situé à une centaine de mètres à l'Est Nord-Est de *Mendibile 1 Nord - tertre pierreux*.

Altitude : 309m.

Description

Tertre pierreux, érigé sur terrain en très légère pente vers le Nord, mesurant 5m et 0,40m de haut.

Historique

Tertre retrouvé par [J. Blot]{.creator} en [2013]{.date}.

### [Iholdy]{.spatial} - [Mendibile]{.coverage} Nord 3 - [tertre pierreux]{.type}

Localisation

Situé à environ 80m au Nord-Est de *Mendibile 2 Nord - tertre pierreux*.

Altitude : 308m.

Description

Tertre pierreux de 8m de diamètre et 0,45m de haut.
