# Macaye

### [Macaye]{.spatial} - [Bordaleku]{.coverage} - [Tertre d'habitat]{.type}

Nous rappelons ici que 3 tertres (et un monolithe) ont été publiés déjà été publiés par nous [@blotMonolithesPaysBasque1983, p.11]. Les 6 tertres ci-après décrits sont moins visibles, mais à proximité immédiate des précédents.

Pour mémoire rappelons que :

- *TH n°1* : est à 21m au Nord du monolithe ; Mesure 10m de diamètre et 1,30m de haut.

- *TH n°2* : à 25m au Nord du précédent ; mesure 9m de diamètre et 1,80m de haut.

- *TH n°3* : à 7m au Nord du précédent ; Tertre de forme oblongue mesure 15m x10m et près de 2,80 de haut - On note à sa surface aplanie, quelques pierres disposées en U qui pourraient avoir délimité l'ancien habitat érigé sur ce tertre.

Les six tertres suivants ont été trouvés en [mai 2013]{.date} par [F. Meyrat]{.creator} :

- *Tertre n°4* : Situé à 20m au Nord Nord-Ouest du précédent. Tertre (?) discret de 5m de diamètre, 0,40m de haut.

- *Tertre n°5* : Situé à 5m au Nord Nord-Est du précédent. Lui aussi tertre (?) discret. Mesure 6m de diamètre et 0,30m de haut.

- *Tertre n°6* : Situé au Sud-Est et en contre-bas du précédent, sur la pente ; mesure 6m de diamètre et 1m de haut.

- *Tertre n°7* : situé au Nord Nord-Est du précédent, de l'autre côté de la piste et près d'un rocher et sur terrain en pente. Mesure7m de diamètre et 1m de haut.

- *Tertre n°8* : à 17m au Sud-Est du précédent. Longé par la piste qui passe à son sommet. Mesure 9m x 7m et 1m de haut.

- *Tertre n°9* : à 6m au Sud-Est du précédent et à 8m du ruisseau que franchit la piste. Mesure 6m de diamètre et 1m de haut.

### [Macaye]{.spatial} - [Harguibele]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 573m.

Il est situé à l'Est du mont Ursuya, tangent, au Sud, à la piste de crête qui s'y rend.

Description

Tumulus circulaire de 6m de diamètre mais de faible hauteur (0,15m à 0,25m de haut). Environ 10 pierres apparaissent à sa surface, sans que l'on puisse parler de péristalithe.

Historique

Monument trouvé par [F. Meyrat]{.creator} en [juin 2014]{.date}.

### [Macaye]{.spatial} - [Hiriguibel]{.coverage} - [Cromlech]{.type}

Localisation

Altitude : 595m.

Situé à 150m environ au Nord-Ouest du Tumulus Hiriguibel, à 5m au Nord de la piste de crête qui monte au sommet de l'Ursuya et à 15m au Sud d'une autre piste s'y rendant aussi.

Description

On note une quinzaine de pierres affleurant plus ou moins en demi-cercle la surface du sol. Elles sont visibles dans la moitié Nord d'un cercle de 4,30m de diamètre, matérialisé par un léger relief en forme de bourrelet.

Historique

Cromlech découvert par [P. Badiola]{.creator} en [janvier 2016]{.date}.

### [Macaye]{.spatial} - [Larrondoa]{.coverage} - [Tumulus]{.type}

Localisation

Carte 1345 Ouest Cambo-les-Bains.

Altitude : 315m.

Il est situé à l'Ouest de la piste pastorale de crête qui longe le sommet d'une croupe étendue au Nord Nord-Ouest du col Sendalepoa (au Nord de Louhossoa). Il est situé, à la cote 315, à 100m à l'Ouest Sud-Ouest de la ferme Larrondoa.

Description

Tumulus mixte de pierres et terre, de 16m de diamètre et 0,80m de haut.

Historique

Monument découvert en [mai 1973]{.date}. Il a depuis été rasé par les labours ; on pouvait voir après le premier passage de la charrue, la pierraille étalée sur une large surface circulaire, mélangée, au centre, avec des fragments de charbons de bois.

### [Macaye]{.spatial} - [Mendizabale]{.coverage} Sud - [Tumulus]{.type}

Localisation

Altitude : 860m

Description

Tumulus (douteux ?) de terre, de 5m à 6m de diamètre et 0,80m de haut. Pourrait être un mouvement naturel du terrain.

Historique

Monument trouvé par [Blot J.]{.creator} en [février 2013]{.date}.

### [Macaye]{.spatial} - [Ursuya]{.coverage} 8 - [Tumulus]{.type}

Localisation

Altitude : 575m.

Ce petit tumulus est situé tout au sommet d'une petite éminence qui domine au Nord-Ouest un petit col, lui-même au Nord-Ouest du mont Ursuya.

Description

Petit tumulus en forme de galette aplatie de 2,30m de diamètre et quelques centimètres de haut. Une vingtaine de petites pierres blanche ([calcaire]{.subject}) apparaissent à la surface du sol.

Historique

Monument découvert par [Blot J.]{.creator} en [janvier 2013]{.date}.
