# Biriatou

### [Biriatou]{.spatial} - [Faalégui]{.coverage} - [Dolmen]{.type} ([?]{.douteux})

Localisation

Carte IGN 1245 OT Hendaye Saint-Jean-de-Luz.

Altitude : 340m.

On atteint cette structure en montant directement depuis le col de Pittare au sommet de Faalégui, en suivant le flanc Est de ce mont ; on doit atteindre et longer une barre rocheuse, tout près de laquelle elle se situe.

Cette structure nous paraît particulièrement intéressante, dans la mesure où elle illustre bien les confusions possibles entre architectures d'origine anthropique et artefact naturel.

On note, à proximité immédiate d'un important amas rocheux, faisant partie de la barre ci-dessus évoquée, une structure assez évocatrice d'un dolmen. Cependant, seul le sol sur lequel elle repose est plat, alors que le terrain dans sa totalité est en pente accentuée, du Sud vers le Nord.

Description

Elle se compose d'un gros bloc rocheux (ou table), reposant à 0,80m de haut, sur deux « montants » presque verticaux qui paraissent délimiter une [chambre funéraire]{.subject} de 1,10m de profondeur, ouverte à l'Est. Le fond de cette chambre est fermé par une petite murette en pierres sèches. Enfin il existe à la périphérie Nord-Ouest comme une ébauche d'un petit amoncellement de pierraille pouvant évoquer les «restes» d'un tumulus pierreux. Les dimensions du bloc de couverture sont de 2,20m dans son axe Nord-Sud ; de 1,80m dans l'axe Est-Ouest, et une épaisseur maximum de 1,14m. Les deux montants sont légèrement inclinés, comme le reste des éléments rocheux du côté Nord.

En fait, il semble bien que la « table » de couverture soit un bloc rocheux ([gneiss]{.subject} probable) qui s'est détaché du filon d'origine, bien visible immédiatement au Sud, à la suite d'un phénomène naturel (érosion, gel...). Ce bloc a ensuite naturellement glissé, en suivant la pente naturelle, sur les éléments rocheux déjà en place, qui jouent maintenant le rôle de « montants ». Il paraît aussi évident que cette structure a pu, ensuite, être aménagée pour en faire un abri, tout comme l'a été le (vrai) *dolmen de Generalen Tomba* dans les Aldudes.

Historique

Cette très curieuse structure nous a été signalée par [P. Badiola]{.creator} en [août 2009]{.date}.

### [Biriatou]{.spatial} - [Faalégui]{.coverage} - [Tertre d'habitat]{.type}

Localisation

Altitude : 415m.

Il est aménagé sur un terrain en pente vers le Nord-Est, à une quinzaine de mètres à l'Est d'un très petit riu, et à 70m au Nord-Ouest d'un enclos à bétail.

Description

Ce tertre asymétrique, au sommet très aplati, (plate-forme de tir ?), est de forme ovale, sa périphérie Nord-Est étant la plus pentue. Il mesure 9m dans son axe Nord-Ouest Sud-Est et 10m dans l'axe de la pente (Nord-Est Sud-Ouest). Sa hauteur, peu marquée est de 0,60m.

Historique

A été trouvé en [mai 2011]{.date} par le [groupe Hilharriak]{.creator}.

### [Biriatou]{.spatial} - [Faalégui]{.coverage} - [Tumulus]{.type}

Localisation

Carte 1245 Ouest

Altitude : 496m.

Ce tumulus est érigé très exactement au sommet de la colline qui domine au Nord le col de Pittare, dit aussi col des Poiriers.

Description

Tumulus pierreux imposant de 16m de diamètre et 2m de haut, formé de blocs de [grès]{.subject} de la taille d'un pavé, ou plus volumineux ; ils sont plus visibles dans la moitié Sud-Ouest du tertre. La périphérie est bien soulignée par de nombreux blocs, sans que l'on puisse cependant parler de « péristalithe ».

Historique

Tumulus découvert en [mars 1997]{.date}.

### [Biriatou]{.spatial} - [Lizarlan]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 115m.

Il se trouve sur la rive gauche de la route qui se détache de celle qui monte à Biriatou, et longe la Bidassoa en direction de la maison Lizarlan. Il est érigé sur un petit plateau qui domine la route de 2m environ, à une vingtaine de mètres au Sud-Ouest de celle-ci et tout de suite après un virage très marqué vers la gauche.

Description

Tumulus pierreux en forme de galette aplatie, érigé sur un terrain en légère pente vers le Nord-Ouest. Il mesure 8m de diamètre, 0,30m à 0,40m de haut, et 3 arbres poussent à sa surface ; au centre et en partie recouverte par les racines d'un arbre, apparaît, semble-t-il une structure circulaire faite de pierres. Il semblerait aussi que quelques pierres, plus volumineuses que les autres, délimitent un péristalithe.

Historique

Monument découvert par [I. Txintxuretta]{.creator} en [octobre 2010]{.date}.

### [Biriatou]{.spatial} - [Lumaberde]{.coverage} - [Dolmen]{.type}

Localisation

Altitude : 214m.

Ce dolmen (ou coffre dolménique) est situé dans un ensellement à 40m à l'Ouest du sommet principal de la colline, à une dizaine de mètres à l'Est d'un second sommet moins marqué et avant que le terrain ne redescende. Il est très difficile à trouver lorsque la végétation occupe totalement l'espace. Il est tangent au Sud du sentier d'accès, fort peu marqué par les rares passages humains ou animaux.

Description

Ce monument est d'interprétation peu aisée ; il vient d'être dégagé à nouveau en février 2016 par Meyrat F. pour étude. On note un tumulus mixte de 4,20m de diamètre et de très faible hauteur (0,15 à 0,20 m), où apparaissent de nombreux blocs ou dalles de [grès]{.subject} local, parmi lesquelles 5 ou 6 paraissant artificiellement disposées, délimitant une [chambre funéraire]{.subject} mesurant environ 2m de long, 0,50m de large et à grand axe orienté Nord-Ouest Sud-Est. Etant érigé sur la ligne de crête, le tumulus est semble-t-il l'objet d'un soutirage qui s'effectue à la fois vers le Nord et surtout vers le Sud, ce qui donne une chambre décentrée vers le Nord.

Il semble bien aussi qu'il existe un péristalithe constitué d'une quinzaine de blocs de [grès]{.subject} local plus ou moins volumineux.

Historique

Site découvert par [Cl. Chauchat]{.creator} et [J.L. Tobie]{.creator}, non décrit mais simplement cité en [1966]{.date}{.creator}, dans le (ref)(Bulletin du Musée Basque n°33).

### [Biriatou]{.spatial} - [Mandale]{.coverage} - [Tumulus]{.type}

Localisation

Carte 1245 Ouest.

Altitude : 480m.

Tumulus pratiquement tangent à la frontière, et à 100m à l'Ouest de la BF 573.

Description

Tumulus essentiellement pierreux, mais recouvert de terre ; bien visible, il présente un diamètre est de 10m et une hauteur d'environ 1m.

Historique

Tumulus découvert en [mai 1989]{.date} ([J. Blot]{.creator}).

### [Biriatou]{.spatial} - [Mandale]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 547m.

Il se situe à une quinzaine de mètres à l'Ouest de la piste qui se détache, vers le Sud, du GR 10, après que celui-ci ait contourné la colline de Mandale, en venant d'Ibardin. À l'Est de ce GR 10, et en symétrique se voit une autre surélévation de terrain correspondant à un affleurement rocheux naturel ; un peu plus haut, toujours à l'Est, se trouve la « Redoute de la Baïonnette ».

Description

Tumulus circulaire, à sommet aplati, recouvert d'une abondante végétation, (ajoncs), comme tout le site. Il mesure environ 0,80m de haut, et 12m de diamètre. On ne distingue pas de péristalithe, ni de pierres à sa surface.

Historique

Monument découvert par [Blot J.]{.creator} en [juin 1968]{.date}.

### [Biriatou]{.spatial} - [Col d'Osin]{.coverage} - [Tumulus]{.type}

Localisation

Altitude :

Il est situé à environ 25 mètres au Nord-Est de la piste (l'ancien GR 10 qui se rend à Biriatou) en partant du col d'Osin.

Description

Petit tumulus pierreux, recouvert d'herbe, situé au Nord-Ouest du dolmen d'Osin ; il mesure 3,50m de diamètre et 0,30m de haut.

Historique

Monument découvert par [J. Blot]{.creator} en [avril 2010]{.date}.

### [Biriatou]{.spatial} - [Osin]{.coverage} - [Monolithe]{.type}

Localisation

Altitude : 371m.

Cette belle dalle gît en bordure Sud de la piste pastorale qui se rend vers Biriatou, après sa bifurcation d'avec celle qui monte vers le Xoldokogagna.

Description

Belle dalle de [grès]{.subject} local, de forme parallélépipédique, mesurant 2,02m de long, et 0,42m d'épaisseur en moyenne. Sa largeur est de 0,90m à sa base et de 0,77m, avant son rétrécissement à l'Est. Il semblerait qu'elle présente des traces d'épannelage à son sommet Est et à son bord Nord.

Historique

Dalle trouvée par [F. Meyrat]{.creator} en [février 2016]{.date}.

### [Biriatou]{.spatial} - [Osin]{.coverage} 2 - [Tumulus]{.type}

Localisation

Altitude : 372m.

Description

Petit tumulus circulaire, mixte, de terre et de quelques pierres, mesurant 5m de diamètre et 0,20m de haut.

Historique

Tumulus découvert par [F. Meyrat]{.creator} en [février 2016]{.date}.

### [Biriatou]{.spatial} - [Osin]{.coverage} 3 - [Tumulus]{.type}

Localisation

Altitude : 375m.

Il est situé à 7m à l'Est de *Osin 2 - Tumulus* et à 5m au Nord Nord-Ouest de *Osin 4 - Tumulus*.

Description

Petit tumulus mixte de terre et de pierres circulaire, mesurant 4,70m de diamètre et 0,20m de haut.

Historique

Tumulus découvert par [F. Meyrat]{.creator} en [février 2016]{.date}.

### [Biriatou]{.spatial} - [Osin]{.coverage} 4 - [Tumulus]{.type}

Localisation

Altitude : 372m.

Il est situé à 5m au Sud Sud-Est de *Osin 3 - Tumulus*.

Description

Petit tumulus circulaire mixte mesurant 4,70m de diamètre et 0,20m de haut.

Historique

Tumulus découvert par [F. Meyrat]{.creator} en [février 2016]{.date}.

### [Biriatou]{.spatial} - [Osingozelaya]{.coverage} - [Pierre plantée]{.type}

Localisation

Altitude : 386m.

Il est situé près du sommet du mont Osingocelaya qui domine au Nord les deux pistes pastorales se rendant au Xoldokogagna et à Biriatou.

Description

Pierre plantée verticalement, de forme triangulaire à base inférieure, mesurant 1,70m de haut, 0,80m d'épaisseur en moyenne et 1,30m de large à sa base ; elle est légèrement inclinée vers le Sud et vers le Nord-Est.

Historique

Pierre trouvée par [F. Meyrat]{.creator} en [février 2016]{.date}.

### [Biriatou]{.spatial} - [Osinkozelaya]{.coverage} - [Tumulus]{.type}

Localisation

Au flanc Sud-Est du mont Osingozelaya qui domine, à l'Ouest, le Col des Poiriers dit aussi de Pittare, riche de ses 6 tumulus-cromlechs. Il se trouve sur une petite éminence, à 340m d'altitude, et à 20m environ au Sud-Ouest de la piste.

Altitude : 340m.

Description

Tumulus pierreux, de 10m de diamètre, fait d'un amoncellement de petites dalles de [grès]{.subject} rose. Sa hauteur est difficile à apprécier, entre 30 et 50 centimètres. On note une importante excavation dans le secteur Sud-Est, comme si le monument avait fait l'objet d'une fouille clandestine.

Historique

Monument découvert en [mai 2009]{.date} par [J. Blot]{.creator}.

### [Biriatou]{.spatial} - [Rocher des Perdrix]{.coverage} - [Pierre couchée]{.type} et/ou [tumulus]{.type} (??)

Localisation

Altitude : 238m et à environ 600m au Sud-Ouest du Rocher des Perdrix, à la jonction des tracés de l'actuel GR et de l'ancien.

Description

On peut voir un assez volumineux bloc de [grès]{.subject}, de forme triangulaire, presque isocèle, mesurant 1,52m à sa base ; il semble présenter des traces de régularisation à sa base Est et repose sur l'extrémité Est de ce qui pourrait être un tumulus de forme ovale, de 5,50m x 4m et 0,30m de haut, à la surface duquel apparaissent une vingtaines de pierres.

Phénomène naturel dû à la solifluxion, ou « muga » intentionnellement disposé là ? Très douteux...

Historique

Monument découvert par [Meyrat F.]{.creator} en [janvier 2017]{.date}.

### [Biriatou]{.spatial} - [Rocher des Perdrix]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 350m.

Il est situé quasiment à l'extrémité d'une petite croupe, au Nord-Ouest des ruines d'une bergerie, et à une vingtaine de mètres à l'Est du rocher le plus proéminent.

Description

Tumulus parfaitement circulaire de 6m de diamètre, et 0,30m de haut, constitué d'une multitude de petits fragments de dalles et de terre ; au centre se voit une dépression de 2,30m de diamètre (vestige d'une fouille ancienne ?).

Historique

Monument découvert par [J. Blot]{.creator} en [avril 2010]{.date}.

### [Biriatou]{.spatial} - [Xoldokogaina]{.coverage} - [Cromlech]{.type}

Localisation

Altitude : 460m.

Il est tangent à la piste qui monte au Xoldokogagna, sur la gauche en montant.

Description

Edifié sur un terrain en légère pente ; une vingtaine de pierres délimitent un cercle de 2,70m de diamètre légèrement surélevé en son centre, où se trouve une dalle plantée.

Historique

Monument découvert par [J. Blot]{.creator} en [avril 2010]{.date}.

### [Biriatou]{.spatial} - [Xoldokocelai]{.coverage} - [Dalle plantée]{.type}

Localisation

Altitude : 370m.

À une quinzaine de mètres au Sud-Ouest de la piste pastorale.

Description

On voit une pierre plantée de 1,60m de long à sa base et 0,75m de haut, épaisse de 0,11m. Il semblerait que l'on puisse deviner à sa périphérie les vestiges d'un tumulus qui aurait eu environ 2m à 3m de diamètre... S'agit-il des restes d'un dolmen ?

Historique

Dalle découverte par [J. Blot]{.creator} en [avril 2010]{.date}.

### [Biriatou]{.spatial} - [Xoldokocelai]{.coverage} 2 - [Dalle plantée]{.type} ([?]{.douteux})

Localisation

Altitude : 430m.

Erigée sur un terrain en pente vers l'Ouest, en pleine fougeraie.

Description

Cette dalle plantée verticalement mesure 1,08m de haut, 0,77m de large et 0,15m d'épaisseur ; elle est légèrement inclinée vers l'Ouest.

Historique

Dalle découverte par [Badiola P.]{.creator} en [mai 2012]{.date}.

### [Biriatou]{.spatial} - [Xoldokocelai]{.coverage} 4 - [Dolmen]{.type} ([?]{.douteux})

Localisation

Altitude : 370m.

Monument situé à environ 80m au Nord-Est de la piste, à la rupture de pente.

Description

On note une grande dalle de [grès]{.subject} gris horizontale, rectangulaire, mesurant 1,35m de long et 1m de large et 0,24m d'épaisseur, orientée Nord-Est Sud-Ouest.

Elle repose à son bord Nord-Ouest, sur une longue dalle verticalement enfouie dans le sol, mesurant 1,10m de long ; une autre pierre apparaît sous l'extrémité Est de la dalle horizontale.

Historique

Monument (douteux ?) découvert par [J. Blot]{.creator} en [avril 2010]{.date}.

### [Biriatou]{.spatial} - [Xoldokocelai]{.coverage} 5 - [Dolmen]{.type}

Localisation

Altitude : 390m.

Il est situé à environ 150 mètres plus loin vers le Sud-Est, à quelques mètres au Sud-Ouest de la piste pastorale.

Description

On remarque un important tumulus circulaire fait de terre et de pierres, d'environ 9 mètres de diamètre et 1m de haut, présentant une excavation centrale de 4m de diamètre. On ne distingue pas de dalles délimitant une [chambre funéraire]{.subject}, sauf, au bord Nord-Ouest du tumulus : une grande dalle, enfouie dans le sol, ainsi que deux autres, dont la longueur totale atteint 1,83m et qui pourraient représenter ce qui reste du chevet de cette [chambre funéraire]{.subject}. Il ne serait pas étonnant, dans le contexte de cette montagne, que les autres dalles de la chambre aient pu été récupérées par des carriers.

Historique

Monument découvert par [J. Blot]{.creator} en [avril 2010]{.date}.

### [Biriatou]{.spatial} - [Xoldokocelai]{.coverage} 6 - [Dolmen]{.type}

Localisation

Il se voit à très courte distance au Nord-Ouest de *Xoldokocelai 5 - Dolmen*.

Description

Il se présente sous la forme d'un tumulus circulaire de terre et de pierres d'environ 0,80m de haut au centre duquel apparaissent 3 dalles pouvant délimiter une [chambre funéraire]{.subject} de 1,40m de large et de longueur indéfinie, orientée Ouest-Est.

La dalle Ouest mesure 0,35m à sa base et 0,50m de haut ; celle du Sud mesure 1,05m de long et 0,35m de large.

Historique

Monument découvert par [J. Blot]{.creator} en [avril 2010]{.date}.

### [Biriatou]{.spatial} - [Xoldokocelai]{.coverage} 7 - [Dolmen]{.type} ([?]{.douteux})

Localisation

Altitude : 400m.

Situé un peu en hauteur et au Sud de *Xoldokocelai 6 - Dolmen*.

Description

On note un tumulus érigé sur un terrain assez en pente, mesurant 7 à 8 mètres de diamètre et 0,80m dans sa plus grande hauteur, au sommet duquel apparaissent deux dalles verticales, dont l'une mesure 0,58m à la base et 0,55m de haut.

Historique

Monument (douteux ?), découvert par [J. Blot]{.creator} en [avril 2010]{.date}.

### [Biriatou]{.spatial} - [Xoldokocelai]{.coverage} 8 - [Dolmen]{.type} ([?]{.douteux})

Localisation

Altitude : 444m.

Ce monument, tout comme *Xoldokocelai 9 à 13 - Dolmen*, est caché dans la très abondante végétation qui recouvre tout le flanc Nord du mont Xoldokogaina. Pour le trouver, il faut partir de la borne 486 au sommet du plateau et prendre la direction Nord ; les monuments sont répartis le long de la ligne de séparation entre les communes de Biriatou et d'Urrugne.

Description

[chambre funéraire]{.subject} en grande partie cachée par la végétation, constituée d'une dizaine de dalles, orientée Ouest-Est et mesurant environ 3m de long et 1,70m de large. On compte 1 dalle à l'Est, 4 dalles au Sud, et 4 autres dalles au Nord ; elles ne paraissent pas jointives. Il n'y a pas de tumulus visible.

Historique

Monument découvert par [J. Blot]{.creator} en [avril 2010]{.date}.

### [Biriatou]{.spatial} - [Xoldokocelai]{.coverage} 9 - [Dolmen]{.type}

Localisation

Il est à 7mètres au Sud de *Xoldokocelai 8 - Dolmen ([?]{.douteux})*.

Description

Une [chambre funéraire]{.subject} de 1,83m de long et 1,50m de large, orientée Nord-Ouest Sud-Est, est délimitée par un ensemble de 8 dalles ; la paroi Nord-Ouest est formée par 2 dalles de 0,91m et 0,73m de long ; la paroi Sud-Ouest par 4 dalles et la paroi Sud-Est par 2 autres dalles de 0,46m et 1,02m de long. Seule la paroi Nord-Est ne possède pas de dalles actuellement visibles. Cette chambre est érigée sur un terrain en légère pente vers le Nord-Est, et ne possède aucun tumulus.

Historique

Monument découvert par [J. Blot]{.creator} en [avril 2010]{.date}.

### [Biriatou]{.spatial} - [Xoldokocelai]{.coverage} 10 - [Dolmen]{.type}

Localisation

Altitude : 440m.

On le trouve à 25 mètres à l'Est de *Xoldokocelai 8 - Dolmen (?)*.

Description

Une grande [chambre funéraire]{.subject} de 3,20m de long et 2,55m de large est orientée Nord Nord-Est-Sud Sud-Ouest. Là encore la végétation rend la lecture du monument difficile ; toutefois, la paroi Sud-Ouest est formée par une grande dalle de 0,85m de long et 0,45m de large ; la paroi Sud-Est est délimitée par 2 dalles dont une de 1,10m de long (en deux fragments) et l'autre de 1,05m de long et 0,24m d'épaisseur. Enfin la paroi Nord-Ouest est marquée, semble-t-il, par 4 dalles ; aucune ne se voit au Nord-Est. Le monument est érigé sur un terrain en légère pente vers le Nord-Est. ; il n'y a pas de tumulus visible.

Historique

Monument découvert par [F. Meyrat]{.creator} en [avril 2010]{.date}.

### [Biriatou]{.spatial} - [Xoldokocelai]{.coverage} 12 - [Dolmen]{.type}

Localisation

Il est à une quinzaine de mètres au Nord de *Xoldokocelhay 11 bis - Dolmen*.

Description

Très belle [chambre funéraire]{.subject} de plus de 2 mètres de long et 1,22m de large, orientée selon un axe Ouest-Est.

La paroi Sud est formée par une grande dalle de [grès]{.subject} gris, plantée dans le sol et inclinée vers l'extérieur, mesurant 1,91m à sa base, 0,72m de haut, 2,08m de long à son sommet et 0,12m d'épaisseur. La paroi Nord est représentée par une autre belle dalle de 1,74m à sa base, 0,53m de haut et 0,16m d'épaisseur. Il semble bien que les 3 grands fragments de dalles visibles dans la chambre, correspondent à la dalle de couverture brisée ; le plus à l'Est mesure 0,96m de long et 0,58m de large, le fragment central : 0,96m de long et 0,68m de large. Pas de tumulus visible, et terrain en légère pente vers l'Est.

Historique

Monument découvert par [F. Meyrat]{.creator} en [avril 2010]{.date}.

### [Biriatou]{.spatial} - [Xoldokocelai]{.coverage} 13 - [Dolmen]{.type}

Localisation

Il est tangent à l'Est au *Xoldokocelai 12 - Dolmen*.

Description

[chambre funéraire]{.subject} de 1,76m de long et 1,38m de large, orientée selon un axe Ouest-Est. La végétation est importante mais permet de distinguer une paroi Sud formée de 4 dalles, et une paroi Nord formée de 4 autres éléments. À l'Ouest se dresse une dalle de 0,97m de long à sa base, 0,60m de haut et 0,11m d'épaisseur. Pas de tumulus visible et terrain en légère pente vers l'Est. Une vue d'ensemble de ces deux monuments, de ces deux architectures dans le prolongement l'une de l'autre, nous suggère qu'il puisse s'agir non pas de deux monuments contigus mais d'un seul dolmen à deux chambres, d'un total de près de 4m de long, du même type que ce que nous décrivons aussi pour *Xoldokocelai 3 - Dolmen*.

Historique

Monument découvert par [F. Meyrat]{.creator} en [avril 2010]{.date}.

### [Biriatou]{.spatial} - [Xoldokocelai]{.coverage} 16 - [Dolmen]{.type}

Localisation

Altitude : 440m.

Il est situé à environ 13m à l'Ouest de la piste qui monte du Rocher des Perdrix.

Description

Tumulus surtout pierreux, érigé sur un terrain en légère pente, de 3 à 4m de diamètre et 0,30m de haut, la périphérie étant délimitée par une douzaine de pierres. Au centre se voit une [chambre funéraire]{.subject}, orientée Nord-Sud, de 1,70m x 0,50m délimitée 4 éléments, au ras du sol. À l'Ouest une dalle de 0,36m de long et 0,9m d'épaisseur ; à l'Est, 2 dalles, l'une de 0,32m de long, l'autre de 0,16m ; enfin au Sud, un fragment de dalles marque ce qui reste de la paroi initiale.

Historique

Monument découvert par [F. Meyrat]{.creator} en [avril 2012]{.date}.

### [Biriatou]{.spatial} - [Xoldokogaina]{.coverage} - [Monolithe]{.type}

Localisation

Altitude : 480m.

Il est à 60m au Nord de la borne géodésique et et à 50m à l'Ouest de la piste de crète.

Description

Cette dalle de [grès]{.subject} local en forme de pain de sucre, gît sur un terrain en légère pente. Elle mesure 3,10m de long, 0,90m de large au maximum, et 0,12m d'épaisseur.

Tout son bord Sud-Est présente des traces d'épannelage.

Historique

Monolithe découvert par [Badiola P.]{.creator} en [mai 2012]{.date}.

### [Biriatou]{.spatial} - [Xoldokocelai]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 371m.

On le trouve à une quinzaine de mètres à droite de la piste qui monte vers le sommet du Xoldokogaina, au niveau du replat d'où se détache la piste qui se rend vers les dolmens de Xoldokocelai. Il est sur une petite surélévation de terrain.

Description

Tumulus pierreux de 4,20m de diamètre et 0,20m de haut. On distingue assez nettement dans le secteur Sud 5 pierres qui forment une ébauche de péristalithe.

Historique

Monument découvert par [J. Blot]{.creator} en [avril 2010]{.date}.

### [Biriatou]{.spatial} - [Xoldokocelai]{.coverage} 4 - [Tumulus]{.type} ([?]{.douteux})

Localisation

Altitude : 450m.

La piste passe à 30m à l'Ouest et *Xoldokocelay 16 - Dolmen* est à 15m au Nord-Ouest. Erigé sur terrain en pente légère vers le Nord.

Description

Tumulus de terre et quelques pierres de 4m de diamètre et 0,40m de haut maximum. On note une dalle importante plantée dans le sol au Nord, de 1,50m de long, 0,32m de haut et 0,19m d'épaisseur. Cette dalle aurait pu bloquer une coulée de solifluxion.

Historique

Tumulus découvert par [F. Meyrat]{.creator} en [mai 2012]{.date}.

### [Biriatou]{.spatial} - [Xoldokocelai]{.coverage} - [Tumulus-cromlech]{.type}

Localisation

Altitude : 415m.

Situé à une vingtaine de mètres à droite de la piste qui monte au sommet de Xoldokocelai, sur un petit replat avec vue sur Hendaye et les Trois Couronnes.

Description

Tumulus de très faible hauteur, (0,20m environ) essentiellement formé d'une quarantaine de petites dalles ou fragments de dalles, plus ou moins enfouies dans le sol, et particulièrement nombreuses dans la moitié Sud du monument ; le centre en est peu fourni.

Historique

Monument découvert par [Blot C.]{.creator} en [décembre 2011]{.date}.
