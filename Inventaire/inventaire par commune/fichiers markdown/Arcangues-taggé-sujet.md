# Arcangues

### [Arcangues]{.spatial} - [Dornarieta]{.coverage} - [Tumulus]{.type}

Localisation

Carte : 1244 Est Espelette.

Altitude : 83m.

Il est situé à gauche de la route qui de Saint-Pée mène à Arcangues, un peu avant le lieu-dit Dornarieta, au sommet d'une petite éminence, cote 83, qui domine un virage important de la route.

Description

Important tumulus terreux de 12m de diamètre et 1,20m de haut, érigé sur un sol en très légère pente vers l'Est.

Historique

Monument découvert en [octobre 1971]{.date}.
