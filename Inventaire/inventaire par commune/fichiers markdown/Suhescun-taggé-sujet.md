# Suhescun

### [Suhescun]{.spatial} - [Elhortegia]{.coverage} 1 - [Tumulus]{.type}

Localisation

Altitude : 388m.

Tumulus situé à 150m environ au Nord-Ouest du Col des Palombières, sur le premier replat avant la rude ascension de la pente suivante. Il est situé en terrain très légèrement en pente vers le Sud-Est, à 7m au Sud-Ouest de la piste ascendante et à 50m environ au Nord de celle qui traverse d'Est en Ouest ce même replat.

Description

Tumulus terreux circulaire de 4,40m de diamètre et 0,15m de haut, à la surface duquel apparaissent de nombreuses pierres de grés blanc, laissant entrevoir la possibilité d'un péristalithe, surtout dans sa moitié Sud.

Historique

Monument signalé par [nous]{.creator} [@blotTumulusBixustiaZuhamendi1976 p.117] et revu en [avril 2015]{.date}. Nous avions signalé dans cette publication deux autres tumulus «disposés en triangle» séparés d'une vingtaine de mètres ; après contrôle sur le terrain, nous apportons le rectificatif suivant : les deux autres éléments se trouvent alignés au Sud de *T1*, mais leur qualité de « tumulus » semble douteuse. Néanmoins nous en dirons quelques mots.

- *Elhortegia T2 (?)* : situé à 17m au Sud du tumulus précédent, il se présente sous la forme d'un relief ovale, de terre et de pierraille, à grand axe Nord-Sud, mesurant 11m de long, 6 de large et 0,40m de haut.

- *Elhortegia T3 (?)* : situé à 5m au Sud du précédent et à 10m au Nord de la piste Est-Ouest déjà signalée. Constitué de terre et de pierraille, il affecte lui aussi une forme ovale, oblongue, mesure 12m de long, 5m de large et 0,40m de haut.

Devons-nous les considérer comme des tas d'épierrage (peu probable à nos yeux) ou comme des « tumulus » s'étant éboulés dans le sens de la pente ?

### [Suhescun]{.spatial} - [Harribeltza]{.coverage} 2 - [Tumulus]{.type} ([?]{.douteux})

Localisation

Un monument dénommé *T1* a été découvert par le groupe Hilharriak en février 2013, mais nous n'avons rien vu à l'endroit signalé.

Il se trouve vers l'extrémité Ouest du grand plateau inclus dans le périmètre du camp protohistorique d'Harribeltza.

Description

Tumulus oblongue, érigé sur terrain plat, constitué de terre et de pierraille, à grand axe Nord-Sud ; il mesure 6,60m de long et 5m de large et environ 0,80m de haut. Monument légèrement douteux.

Historique

Monument découvert par le [groupe Hilharriak]{.creator} en [février 2013]{.date}.

### [Suhescun]{.spatial} - [Harribeltza]{.coverage} 3 - [Tumulus]{.type}

Localisation

Il est à 2m au Nord Nord-Ouest de *Harribeltza 2 - Tumulus (?)*.

Description

Petit tumulus de terre et de pierraille, bien circulaire, mesurant 2,40m de diamètre et 0,35m à 0,40m de haut.

Historique

Tumulus découvert par [J. Blot]{.creator} en [octobre 2014]{.date}.

### [Suhescun]{.spatial} - [Hirur Haïtzeta lepoa]{.coverage} - [Cromlech]{.type} ([?]{.douteux})

Localisation

Altitude : 412m.

Au sommet d'un petit replat dominant au Nord le col d'Hirur Haizeta, à proximité immédiate de postes de tir à la palombe.

Description

Cercle de 4m de diamètre, délimité par une vingtaine de petites pierres de la taille d'un poing, dont le sommet affleure à peine la surface du sol. Une très légère dépression occupe la totalité du cercle. Monument douteux ?

Historique

Monument découvert par [J. Blot]{.creator} en [avril 2015]{.date}.

### [Suhescun]{.spatial} - [Hocheko Ithurria]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 419m.

Il se voit à droite de la route qui de Suhescun mène au col d'Istixaretta au niveau du réservoir d'eau dénommé « Hocheko Ithurria » : là se détache une bretelle qui mène au col « Haltzeko lepoa ». Le tumulus se trouve à 20m au Sud de la bifurcation.

Description

Tumulus très visible de 5m de diamètre et 0,40m de haut ; érigé sur un terrain en très légère pente vers le Nord, il est constitué de terre et de pierraille.

Historique

Monument découvert par [J. Blot]{.creator} en [octobre 2014]{.date}.

### [Suhescun]{.spatial} - [Istilxarreta]{.coverage} - [Tumulus]{.type} ([?]{.douteux})

Localisation

Altitude : 540m.

Sur un replat au milieu de la ligne de crête qui monte du col d'Istilxaretta vers Hocha Handia.

Description

Elle est rendue très difficile par l'abondance de la végétation (ronces etc.) qui dissimule les détails. Néanmoins il semble qu'on puisse « deviner » un tumulus de terre et de pierres de 7m de diamètre environ, avec une dépression centrale (excavation ?). Il ne semble pas s'agir d'un mouvement naturel du terrain. Il pourrait exister un deuxième élément, sensiblement de mêmes dimensions, tangent au Sud-Ouest, mais lui aussi dissimulé par la végétation... Comme d'habitude, nous préférons signaler un élément douteux que d'en omettre un qui pourrait éventuellement s'avérer valable ultérieurement.

Historique

Monument découvert par [J. Blot]{.creator} en [octobre 2014]{.date}.

### [Suhescun]{.spatial} - [Sen Julian]{.coverage} - [Tertre d'habitat]{.type}

Localisation

Altitude : 411m.

Il est érigé au flanc Nord-Ouest de la colline de Sen Julian, à une dizaine de mètres à l'Est de la naissance d'un petit riu et une centaine de mètres d'un petit bosquet de chênes.

Description

Tertre asymétrique sur terrain en pente vers le Nord Nord-Est, de 10m de large, d'une douzaine de long, et 1m de haut environ.

Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2015]{.date}.

### [Suhescun]{.spatial} - [Urdamendi Lepoa]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 383m.

Il est situé à 10m au Sud du chemin de crête.

Description

Tumulus circulaire mixte, de terre et de pierres de 8m de diamètre et 0,40m de haut, fortement dissimulé dans les broussailles.

Historique

Tumulus découvert par [F. Meyrat]{.creator} en [novembre 2014]{.date}.
