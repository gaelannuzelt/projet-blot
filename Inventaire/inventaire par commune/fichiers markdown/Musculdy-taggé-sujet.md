# Musculdy

### [Musculdy]{.spatial} - [Saint-Antoine]{.coverage} - [Tumulus]{.type}

Nous avions cité dans le [@blotNouveauxVestigesProtohistoriques1975, p.122], 8 tumulus mixtes, de terre et de pierres, situés à environ 900 mètres au Nord de la chapelle Saint-Antoine, au milieu d'un vaste plateau, en bordure de la piste pastorale de crête. Leurs dimensions varient de 2m à 5m pour une hauteur moyenne de 0,40m. Nous en décrirons ici plus précisément 4, les plus visibles actuellement ([F. Meyrat]{.creator} - [décembre 2015]{.date}).

Localisation

- *T1* : Altitude : 587m.

- *T2* : Altitude : 587m.

- *T3* : Altitude : 587m.

- *T4* : Altitude : 587m.

Description

- *T1* : tumulus mixte légèrement oblongue, de 5 x 4m et 0,20m de haut ; situé à 6m au Nord Nord-Est de la piste.

- *T2* : Tumulus de 6 x 4 m et 0,30m de haut. Situé à 5m au Nord Nord-Est de la piste et à 19m au Sud Sud-Ouest de T1.

- *T3* : Tumulus oblongue de 6 x 4m et 0,30m de haut. Situé à 20m au Nord Nord-Ouest de la piste et à 25m au Nord Nord-Ouest de T2.

- *T4* : Tumulus oblongue de 7 x 4m et 0,40m de haut. Situé à 70m au Sud Sud-Ouest de T2, et tangent au bord Nord-Ouest de la piste.

Historique

Tumuli vus et publiés en [1975]{.date} par [Blot J.]{.creator} et revisités en 2015 par F. Meyrat.
