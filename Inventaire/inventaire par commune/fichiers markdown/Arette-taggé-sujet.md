# Arette

### [Arette]{.spatial} - [La Borne frontière]{.coverage} - [Faux dolmen]{.type}

Localisation

Altitude : 1756m.

Cette construction est située à gauche de la route quand on se rend en Espagne, à une soixantaine de mètres avant la BF 262 où se renouvellent chaque année le 13 juillet les Faceries entre Roncal et Barétous. 

Elle est située très en hauteur par rapport à la route, à une distance d'environ cinquante mètres.

Description

Ce monument a été publié - en tant que dolmen - dans [@berdoyValleeBaretousDepuis1990 p. 97-114] par [Anne Berdoy]{.creator} et [Claude Blanc]{.creator}.

Nous publions ici ces quelques lignes à titre rectificatif, car nous ne pensons absolument pas qu'il puisse s'agir d'un dolmen.

Cette construction est érigée sur *un terrain très en pente* vers l'Ouest ; il s'agit ici plus d'un abri, *ouvert plein Ouest*, que d'un [dolmen]{.subject}. 
Constitué de [calcaire]{.subject} local, *il prend appui sur la colline où il est érigé, et s'enfouit dedans*. 
Il est délimité par 4 dalles : 3 dalles latérales et une de [couverture]{.subject}.

Tout d'abord, une dalle latérale verticale, côté Nord, mesurant 2,10m de long, 1,10m de haut dans sa moitié arrière, et 0,24m d'épaisseur. 
*Cette dalle semble bien être en place, d'origine, et avoir été utilisée telle que*.

Une autre dalle verticale, disposée de main d'homme, ferme cet abri au Sud. Elle mesure 2,05 m de long, 1,19m de haut et 0,27m d'épaisseur.

Le fond de l'abri, à l'Est est délimité par une dalle rectangulaire verticale de 1,54m de long et 1,14m de haut *qui prend appui sur le terrain en arrière de lui*. 
Il est recouvert par une dalle horizontale mesurant 1,75m dans le sens Ouest-Est, et 1,75m dans le sens Nord-Sud, et 0,12m d'épaisseur qui s'appuie sur les 3 dalles précédentes. 
On note enfin une dalle horizontale disposée à l'avant de la construction, c.à.d. à l'Ouest ; elle mesure 1,63m dans le sens Nord-Sud, et 1,18m dans le sens Ouest-Est, et a une épaisseur de 0,10m. Nous en faisons plus un seuil de l'abri qu'une dalle de « fermeture » d'un [dolmen]{.subject}.

**Discussion** : Un [dolmen]{.subject}, en Pays Basque (et ailleurs), est érigé sur un terrain plat, la base de ses montants est enfoncée dans le sol de manière égale, l'ouverture de la chambre funéraire se fait à l'Est, ou dans une direction où l'Est prédomine, c.à.d. vers le soleil levant. 
Comme on le voit ici, aucun de ces critères n'est respecté. 
Enfin, nous avons l'exemple très proche de deux constructions similaires, construites à proximité du vrai dolmen de Caque. 
Nous renvoyons le lecteur à notre publication dans [@blotSouleSesVestiges1979 p. 42-43].

Altitude : 1726m.

Les deux constructions, autrefois appelées *[dolmen]{.subject} de Caque 2* et *3*, sont situées à une vingtaine de mètres à l'Est du vrai dolmen de Caque. 
Comme la construction ici décrite, elles utilisent le terrain environnant, le complétant pour en faire un abri. 
Pierre Boucher, au vu de leurs structures, en faisait des abris à porcs, ceci étant confirmé par le nom béarnais de ces deux constructions : *curtel*, signifiant porcherie.

### [Arette]{.spatial} - [Larranche]{.coverage} 1 - [Tumulus]{.type}

Nous décrirons sous ce nom un tumulus identifié en 1978 et publié en 1979 sous le nom de *tumulus de Garbas* [@blotSouleSesVestiges1979 p. 38]. 
Avec la précision des GPS actuels, ce monument se trouve être en réalité dans la commune d'Arette, et plus précisément au lieu-dit *Larranche*.

Localisation

Altitude : 898m.

Un peu avant le col de Garbas, se trouve un petit col que nous appellerons *col de Larranche* ; à ce niveau, et à 6m environ au Nord la piste, une légère éminence domine celle-ci, sur laquelle est construit le monument.

Description

Tumulus terreux d'environ 8m de diamètre et 0,80m de haut. 
On note une dépression qui ampute en partie son quart Sud-Ouest. 
Tumulus ou tertre d'habitat ? L'hésitation est permise.

Historique

Tumulus découvert par [Blot J.]{.creator} en [juin 1978]{.date} et revisité en juillet 2016.

### [Arette]{.spatial} - [Larranche]{.coverage} 2 - [Tumulus]{.type}

Localisation

Altitude : 904m.

En empruntant au col la piste de gauche, horizontale, on distingue à environ 150m au Nord de *Larranche 1 - Tumulus* et à environ 4m au Nord-Est de la piste, un petit tumulus qui la surplombe légèrement.

Description

Petit tumulus terreux en forme de galette aplatie, mesurant 2,80m de diamètre et 0,25m de haut ; on distingue à sa périphérie deux pierres profondément enfouies dans son quart Sud-Est ; éléments d'un péristalithe ?

Historique

Tumulus découvert par [Blot J.]{.creator} en [juillet 2016]{.date}.

### [Arette]{.spatial} - [Soum de Soudet]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 1491m.

Il est situé vers l'extrémité Sud-Ouest de cette longue croupe allongée suivant un axe Est-Ouest qui domine, au Nord, la route reliant la station de la Pierre St Martin à Lanne, au moment de la bifurcation vers Arette. 
Il est situé juste avant la rupture de pente, au Nord de la piste de crête, et tangent à celle-ci, à une dizaine de mètres d'un poteau indicateur renversé portant l'inscription : "Soum de Soudet 2250m". 
On a un panorama exceptionnellement vaste sur tout l'horizon au Sud-Ouest.

Description

Tumulus terreux circulaire bien visible, de 12m de diamètre et 0,50m de haut environ, en forme de galette aplatie.

Historique

Monument découvert par [Blot J.]{.creator} en [août 2017]{.date}.
