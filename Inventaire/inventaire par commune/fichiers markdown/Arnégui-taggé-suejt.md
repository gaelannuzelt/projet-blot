# Arnégui

### [Arnégui]{.spatial} - [Elhursaro]{.coverage} 1 - [Cromlech]{.type}

Localisation

Altitude : 1125m.

Ce cromlech fait partie d'un ensemble de 5 cromlechs et un tumulus érigés sur un sol plat, au milieu d'un vaste plateau en pente douce vers l'Ouest et lui-même situé à l'Ouest de la voie romaine (au niveau du col d'Elhursaro.).

Description

Il est le plus à l'Ouest du groupe. 
Neuf petits blocs de calcaire, au ras du sol et de dimensions très modestes, délimitent un cercle de 3,30m de diamètre; seule une pierre située au Sud est un peu plus visible (0,80m x 0,50m).

Disons de suite que la modestie et l'architecture en général fort négligée de ces 5 monuments, nous évoquent fortement les cromlechs de Sohandy, dont les exemplaires fouillés par nous ont été datés en période historique, bien que relevant d'une tradition protohistorique.

Historique

Monument découvert par [F. Meyrat]{.creator} en [septembre 2018]{.date}.

### [Arnégui]{.spatial} - [Elhursaro]{.coverage} 2 - [Cromlech]{.type}

Localisation

Il est situé à 2m au Nord-Nord-Ouest de *Elhursaro 1 - Cromlech*.

Description

Une dizaine de petits blocs de [calcaire]{.subject}, au ras du sol, délimitent un cercle de 3,70m de diamètre. 
Avant un dégagement sommaire, ils étaient très peu visibles ; notons toutefois une pierre au Nord-Ouest, mesurant 0,56m de long et 0,28m de large ainsi qu'une autre, à l'Est mesurant 0,64m de long, 0,10m de large et 0,17m de haut.

Historique

Monument découvert par [F. Meyrat]{.creator} en [septembre 2018]{.date}.

### [Arnégui]{.spatial} - [Elhursaro]{.coverage} 3 - [Cromlech]{.type} ([?]{.douteux})

Localisation

Il est situé à 9,30 m au Sud-Ouest de *Elhursaro 2 - Cromlech*.

Description

Cinq pierres [calcaire]{.subject} délimitent un cercle de 3,30m de diamètre. 
Trois sont de très faibles dimensions dans la moitié Est, et les deux autres, dans la moitié Ouest sont plus importantes et très proches de *Elhursaro 4 - Cromlech*. : elles peuvent faire partie de son architecture, ou être tangentes à ce cercle, ce qui nous paraît le plus probable. 
Ce monument, dans son ensemble, nous paraît cependant douteux; seules des fouilles trancheraient la question.

Historique

Monument découvert par [F. Meyrat]{.creator} en [septembre 2018]{.date}.

### [Arnégui]{.spatial} - [Elhursaro]{.coverage} 4 - [Cromlech]{.type}

Localisation

Ce cercle est tangent au Sud-Est de *Elhursaro 3 - Cromlech*.

Description

On compte 7 blocs de [calcaire]{.subject}, délimitant un cercle de 3,40m de diamètre. 
Dans la moitié Sud-Est, comme nous l'avons signalé, les blocs sont tangents à 2 témoins du *cromlech n°3*, mais, peut-être ces deux-là font-ils aussi partie du *cercle n°4* ?

Historique

Monument découvert par [F. Meyrat]{.creator} en [septembre 2018]{.date}.

### [Arnégui]{.spatial} - [Elhursaro]{.coverage} 5 - [Cromlech]{.type} ([?]{.douteux})

Localisation

Il est tangent, au Nord-Ouest, de *Elhursaro 3 - Cromlech*.

Description

Cercle de 3,70m de diamètre, délimité par 5 petits blocs de [calcaire]{.subject}, dont 2 font partie du péristalithe du *cromlech n°3*, au Nord-Ouest. 
La discrétion des témoins et l'irrégularité du cercle nous font qualifier ce dernier de douteux. 
En fait seules des fouilles pourraient, là encore - comme pour le n°3 - témoigner de l'authenticité de ce monument.

Historique

Monument découvert par [F. Meyrat]{.creator} en [septembre 2018]{.date}.

### [Arnégui]{.spatial} - [Elhursaro]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 1117m.

Il est situé à une dizaine de mètres au Nord-Ouest de *Elhursaro 4 - Cromlech*.

Description

Tumulus en terre, circulaire, en forme de galette aplatie, mesurant 4m de diamètre et 0,10m de haut. 
Pas de pierres visibles.

Historique

Monument découvert par [F. Meyrat]{.creator} en [septembre 2018]{.date}.

### [Arnégui]{.spatial} - [Héganzo]{.coverage} 1 - [Cromlech]{.type}

Localisation

Carte IGN 1346 Ouest Saint-Etienne-de-Baïgorri.

Altitude : 1111m.

Ce monument se trouve sur l'éminence qui domine au Sud-Ouest le col d'Héganzo et la route en lacet qui y passe.

Description

Sur un terrain en légère pente vers le Sud-Ouest, vingt-cinq pierres, au ras du sol, délimitent un cercle de 4,50m de diamètre, à l'intérieur légèrement surélevé (0,30m environ). 
On peut distinguer aussi de nombreuses pierres à l'intérieur.

Historique

Monument découvert par [A. Martinez Manteca]{.creator} en [2001]{.date}.

### [Arnégui]{.spatial} - [Héganzo]{.coverage} 2 - [Cromlech]{.type}

Localisation

À 3m environ au Sud-Ouest de *Héganzo 1 - Cromlech*.

Description

Six pierres au ras du sol délimitent un cercle de 2m de diamètre; au centre sont visibles 4 petits blocs de schiste.

Historique

Monument découvert par [A. Martinez Manteca]{.creator} en [2009]{.date}.

### [Arnégui]{.spatial} - [Héganzo]{.coverage} 3 - [Cromlech]{.type}

Localisation

À environ 2 m au Nord-Est de *Héganzo 1 - Cromlech*.

Description

Quelques petits blocs de [schiste]{.subject} au ras du sol semblent délimiter un cercle de 3,50m de diamètre. 
Monument douteux.

Historique

Monument découvert par [Blot. J.]{.creator} en [octobre 2009]{.date}.

### [Arnégui]{.spatial} - [Héganzo]{.coverage} - [Tumulus]{.type} (ou [Tertre]{.type})

Localisation

Altitude : 1075 m.

Il est situé au Nord-Ouest de *Héganzo 3 - Cromlech*, sur un replat, à 1075 mètres d'altitude.

Description

Tumulus circulaire de 11m de diamètre et 0,70m de haut, constitué, semble-t-il, plus par du cailloutis que de la terre. 
Ce fait, ainsi que sa situation sur un sol plat, serait plus en faveur d'un tumulus que d'un tertre d'habitat.

Historique

Monument découvert par [A. Martinez Manteca]{.creator} en [2001]{.date}.

### [Arnégui]{.spatial} - [Legarre]{.coverage} 1 - [Tertre d'habitat]{.type}

Localisation

Altitude : 980m.

À l'extrémité Nord-Ouest d'un petit replat dominant, au Sud-Ouest la bretelle issue de la *Voie Romaine* et descendant vers Arnégui.

Description

Tertre de terre ovale d'une vingtaine de mètres de long et 9m de large, orienté selon un axe Nord-Est Sud-Ouest, de 1m de haut à son versant Sud-Est et 5m à 6m sur le versant opposé.

Historique

Tertre découvert par [Blot J.]{.creator} en [août 2011]{.date}.

### [Arnégui]{.spatial} - [Legarre]{.coverage} 2 - [Tertre d'habitat]{.type}

Localisation

Altitude : 790m.

Situé à quelques mètres au Sud-Ouest d'un virage en épingle à cheveu de la bretelle issue de la *Voie Romaine* et descendant vers Arnégui.

Description

Tertre de terre ovale, d'une vingtaine de mètres de long et cinq de large, et de 2m de haut à son versant Est.

Historique

Tertre découvert par [Blot J.]{.creator} en [août 2011]{.date}.

### [Arnégui]{.spatial} - [Legarre]{.coverage} - [Tumulus]{.type}

Localisation

Carte 1346 Ouest Saint-Etienne-de-Baïgorri.

Altitude : 737m.

Il est situé sur un replat à l'Est Sud-Ouest du pic Beillurti et à une quinzaine de mètres au Nord de la route goudronnée qui relie la *Voie Romaine* à Arnégui.

Description

Tumulus de terre à sommet aplati de 7m de diamètre et 0,40m de haut.

Historique

Tumulus découvert en [août 1977]{.date}.

### [Arnégui]{.spatial} - [Legarre]{.coverage} 1 - [Tumulus]{.type}

Localisation

Altitude : 740m.

Il est situé sur un replat à l'Est Sud-Ouest du pic Beillurti et à une trentaine de mètres au Nord de la route goudronnée qui relie la *Voie Romaine* à Arnégui.

Description

Ce tumulus circulaire, que nous avions découvert en 1977, constitué de terre présentait un sommet aplati et mesurait 7m de diamètre et 0,40m de haut. 
Lors de notre passage en septembre 2018, nous n'avons pu que constater sa quasi totale destruction due à l'érection d'un pylône électrique sur son emplacement.

Historique

Tumulus découvert en [août 1977]{.date} par [J. Blot]{.creator} et publié dans [@blotInventaireMonumentsProtohistoriques2009].

### [Arnégui]{.spatial} - [Legarre]{.coverage} 2 - [Tumulus]{.type}

Localisation

Altitude : 760m.

Il est situé sur la droite du chemin qui mène au sommet de la colline qui domine, à l'Ouest de *Legarre 1 - Tumulus* ; il est tangent à une clôture de barbelés.

Description

Tumulus en terre, plutôt de forme ovalaire à grand axe Ouest Nord-Ouest - Est Sud-Est, mesurant 6m x 3m et 0,40m de haut.

Historique

Tumulus découvert par [F. Meyrat]{.creator} en [octobre 2018]{.date}.

### [Arnégui]{.spatial} - [Négoucharo]{.coverage} 1 - [Cromlech]{.type}

Nous avons déjà décrit dans ce site, 3 tumulus [@blotNouveauxVestigesMegalithiques1972b p.84] et 1 tumulus-cromlech [@blotVestigesProtohistoriquesVoie1978 p.72]. 
Trois nouveaux monuments sont à décrire.

Localisation

Altitude : 1000m.

Le monument se trouve sur une petite éminence qui domine, à l'Est, la route qui a longé le pic de Beillurti, au moment où s'en détache une bretelle destinée à un cayolar voisin.

Description

Une vingtaine de pierres, parfaitement visibles au ras du sol, délimitent un cercle de 3,80m de diamètre.

Historique

Monument découvert par [A. Martinez Manteca]{.creator} en [2001]{.date}.

### [Arnégui]{.spatial} - [Négoucharo]{.coverage} 4 - [Tumulus]{.type}

Localisation

Situé à 25 mètres au Nord-Est de *Négoucharo 1 - Cromlech*.

Description

Tumulus constitué de terre et de nombreuses pierres, mesurant 9m de diamètre et 0,85m de haut, édifié sur terrain plat.

Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2009]{.date}.

### [Arnégui]{.spatial} - [Négoucharo]{.coverage} 5 - [Tumulus]{.type} ([?]{.douteux})

Localisation

Situé à 15m au Nord-Est de *Negoucharo 4 - Tumulus*.

Description

Tumulus de 4m de diamètre et 0,40m de haut, constitué de terre et de pierres. 
Monument douteux.

Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2009]{.date}.

### [Arnégui]{.spatial} - [Urdanazburu]{.coverage} N 4 - [Tumulus]{.type}

Localisation

Altitude : 1190m.

Ce monument est à environ 70m à l'Ouest de la route, érigé sur un terrain plat.

Description

Il se présente comme un tumulus pierreux de 3m de diamètre et 0,45m de haut environ ; les pierres sont bien enfoncées dans la structure mais en désordre et on note une certaine irrégularité dans le secteur Nord-Ouest, comme si un éboulement s'était produit, rompant la circularité du tumulus.

Historique

Monument découvert par [Blot J.]{.creator} en [septembre 2019]{.date}.
