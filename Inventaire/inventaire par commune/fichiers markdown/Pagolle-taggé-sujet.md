# Pagolle

### [Pagolle]{.spatial} - [Mehalzu]{.coverage} 1 - [Tumulus]{.type}

Localisation

Altitude : 499m.

Ce tumulus se trouve sur un terrain en très légère pente vers le Nord-Ouest, à environ 5m à l'Est de la clôture en fil barbelé matérialisant la limite entre Pagolle et Juxue.

Description

Tumulus de 5m de diamètre et 0,30m de haut environ ; une vingtaine de petites dalles de calcaire blanc sont visibles en surface, dont six pourraient matérialiser une [ciste]{.subject} centrale.

Historique

Tumulus découvert par [Blot J.]{.creator} en [octobre 2015]{.date}.

### [Pagolle]{.spatial} - [Mehalzu]{.coverage} 2 - [Tumulus]{.type}

Localisation

Altitude : 612m.

Tertre érigé sur un beau replat après une rude montée et avant d'aborder le sommet de Mèhalzu ; il est à 5m à l'Est de la clôture marquant la limite entre Pagolle et Juxue.

Description

Tertre mixte, terreux et pierreux, à la surface duquel apparaissent de nombreux blocs de [calcaire]{.subject}, de volumes variables, dont une dalle, située au Sud, dont la périphérie visible est épannelée ; elle mesure 0,89m de long et 0,25m de haut.

Historique

Tumulus découvert par [Blot J.]{.creator} en [octobre 2015]{.date}.

### [Pagolle]{.spatial} - [Oxaraniako borda]{.coverage} Est (ou [Mehalzu]{.coverage} ) - [Tumulus]{.type}

Ces 9 tumulus sont situés sur un terrain dans l'ensemble en légère pente vers le Nord.

Historique

Ces 9 tumulus ont été découverts en [avril 1974]{.date} par [Blot J.]{.creator} et cités en 1975, dans le [@blotNouveauxVestigesProtohistoriques1975, p.121], dans la rubrique *Tertres de Méhalzu*). Signalons, à cette occasion que l'ensemble des tumulus du site *Daraturagagne* [@blotNouveauxVestigesProtohistoriques1975, p.122], a été rasé par la création de nouvelles prairies.

- *Tumulus n°1* - Altitude : 472m. Proche d'un poste de tir à la palombe. Tumulus mixte de terre et de pierres, circulaire, mesurant 9m de diamètre et 0,60m de haut

- *Tumulus n°2* - Altitude : 469m. Il est situé à 10m au Nord Nord-Est du n° 1. Tumulus mixte mesurant 11m x 8m et 0,40m de haut, à grand axe Nord-Sud.

- *Tumulus n°3* - Altitude : 476m. Tumulus mixte situé à 12m au Sud du n°1. Il mesure 9m x 7m et 0,40m de haut.

- *Tumulus n°4* - Altitude : 478m. Tumulus mixte situé à 5m au Sud Sud-Est du n°3. Il mesure 7m x 6m et 0,40m de haut.

- *Tumulus n°5* - Altitude : 471m. Tumulus mixte circulaire, situé à 12m au Nord-Ouest du n°2, mesurant 8m de diamètre et 0,30m de haut.

- *Tumulus n°6* - Altitude : 471m. Tumulus mixte, situé à 3m au Nord-Ouest du n°5 ; il mesure 9m x 7m et 0,40m de haut, à grand axe Nord-Sud.

- *Tumulus n°7* - Altitude : 467m. Tumulus situé à 16m au Nord-Ouest du n° 6. Il mesure 12m x 6m et 0,30m de haut., à grand axe Nord-Sud.

- *Tumulus n°8* - Altitude : 462m. Tumulus mixte situé à 20m au Nord-Est du n°7. Il mesure 8m x 7m et 0,40m de haut.

- *Tumulus n°9* - Altitude : 467m. Tumulus mixte situé à 70m à l'Est Sud-Est du n°8 : il mesure 9m x 8m et 0,60m de haut.
