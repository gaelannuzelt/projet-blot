# Lacarry

### [Lacarry]{.spatial} - [Idagorria bas]{.coverage} Ouest - [Tertres d'habitat]{.type}

Localisation

Altitude : 965m.

Le plus net et le plus haut situé se voit en contre-bas de la route, (avant le virage vers Urrutchantzé), et les 3 autres sont à une vingtaine de mètres plus bas, au Sud-Est, en bordure du ruisseau.

Description

Le tertre le plus haut situé, de forme circulaire, asymétrique, mesure une quinzaine de mètres de diamètre et 1m de haut. Les 3 autres se sont effondrés au cours des temps, mais sont encore visibles.

Historique

Tertre découvert par [Blot J.]{.creator} en [2012]{.date}.

### [Lacarry]{.spatial} - [Olhaberria]{.coverage} Ouest 1 - [Tertre d'habitat]{.type}

Localisation

Altitude : 980m.

Tertre situé entre le premier et le second relief de la crête de *Bostmendiette*.

Description

Tertre de terre circulaire, de 15m de diamètre et 3m de haut, asymétrique à grande pente Sud.

Historique

Tertre découvert pat [Blot J.]{.creator} en [1979]{.date} mais seulement cité, non décrit, sous la rubrique *Bostmendiette*.

### [Lacarry]{.spatial} - [Olhaberria]{.coverage} Centre 1 - [Tertre d'habitat]{.type}

Localisation

Altitude : 980m.

Situé entre le deuxième et le troisième relief de la crête de *Bosmendiette*.

Description

Très semblable à *Olhaberria Ouest 1 - Tertre d'habitat* : tertre circulaire de terre de 15m de diamètre, et 2m à 3m de haut, asymétrique à grande pente Sud.

Historique

Tertre découvert par [Blot J.]{.creator} en [1979]{.date}, seulement cité sous la rubrique *Bostmendiette*.

### [Lacarry]{.spatial} - [Olhaberria]{.coverage} Est - [Tertres d'habitat]{.type}

Localisation

Altitude : 1000m.

La dizaine de tertres d'habitat est située au petit col où se trouvent les ruines du cayolar Olhaberria.

Description

Une dizaine de tertre, très proches les uns des autres, sont construits autour d'excavations (ou de [dolines]{.subject} ?). Ils sont circulaires, mesurent une dizaine de mètres de diamètre et un à deux de haut environ.

Historique

Tertres découverts par [Blot J.]{.creator} en [1979]{.date}, et seulement cités à la rubrique *Bostmendiette*.

### [Lacarry]{.spatial} - [Ugatze]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 1154m.

Il est situé sur une éminence qui domine le col d'Ugatze à l'Est.

Description

Tumulus de terre de 3,50m de diamètre et 0,30m de haut, partiellement amputé de son quart Nord par le passage de la piste de crête. On note, sur la coupe faite au Bulldozer 2 pierres à l'extrémité Ouest de la coupe, qui pourraient appartenir à un péristalithe...

Historique

Monument découvert en [mai 1989]{.date} par [Luis Millan]{.creator}.

### [Lacarry]{.spatial} - [Ugatze Lepoa]{.coverage} - [Cromlech]{.type} ou [Tumulus]{.type} ([?]{.douteux})

Localisation

Altitude : 1140m.

Il est en plein col, à 20m à l'Est du virage de la piste.

Description

Environ 13 pierres, au ras du sol délimitent un cercle de 3m de diamètre ; le secteur Nord en est démuni. Monument douteux.

Historique

Monument découvert par [Blot J.]{.creator} en [mai 2012]{.date}.
