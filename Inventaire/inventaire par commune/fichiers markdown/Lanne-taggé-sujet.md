# Lanne

### [Lanne]{.spatial} - [Ayhautce]{.coverage} - [Cromlech]{.type}

Localisation

Altitude : 1227m.

Il est situé sur un replat à une trentaine de mètres au Nord de la route, au niveau d'un virage bien marqué.

Description

Nous avons déjà publié ce monument [@blotNouveauxVestigesMegalithiques1972b] mais il a, depuis, fait l'objet d'une fouille clandestine dont on voit la trace sous la forme d'une excavation centrale de 2m de large environ et 0,40m de profondeur.

On peut encore voir un cercle de 4,20m à 4,30m de diamètre formé d'une dizaine de pierres encore visibles, sur les 17 que nous avions notées en 1971. Peut-être certaines sont-elles enfouies sous la mousse... mais d'autres semblent bien avoir été éjectées du cercle. À noter que les 3 tertres d'habitat décrits en 1972 et dont le premier est à 20m au Nord du cromlech, n'ont subi aucune détérioration.

Historique

Monument découvert par [Blot J.]{.creator} en [1971]{.date}, et revisité en juillet 2016.

### [Lanne]{.spatial} - [Ayhautce]{.coverage} 4 et 5 Ouest - [Tertres d'habitat]{.type}

Nous devons signaler et ajouter un quatrième tertre d'habitat, aux 3 précédemment cités par nous en 1972 [@blotNouveauxVestigesMegalithiques1972b]. Il est le plus au Nord. Ils sont tous quatre situés à quelques mètres au Nord du précédent cromlech. Par ailleurs il existe un cinquième tertre d'habitat, *TH Ouest*, situé à environ 100m plus à l'Ouest, de l'autre côté d'un petit ruisseau.

### [Lanne]{.spatial} - [Coutchet de Planté]{.coverage} - [Tertres d'habitat]{.type} ([?]{.douteux})

Localisation :

Altitude : 1452m.

On note sur ce vaste pâturage de nombreuses petites éminences que l'on pourrait prendre pour de petits tertres d'habitats, ce que nous ne pensons pas (surtout si on les compare avec leurs voisins, les tertres de la Cabane de la Serre... juste en contre-bas de la route). Nous avons trouvé les semblables à Ste Engrace (Col de la Taillade). [Blot J.]{.creator}, [juillet 2016]{.date}.

### [Lanne]{.spatial} - [Couyalarou]{.coverage} - [Tertres d'habitat]{.type}

Localisation

Ces 3 tertres d'habitat sont situés à environ quatre cent mètres au-dessus et à l'Ouest Sud-Ouest des ruines de la cabane de Couyalarou, dont les vestiges des murs en pierres sont encore visibles.

Description

- *TH 1* - Altitude : 1344m.

    Tertre de terre recouvert d'une végétation abondante, de forme ovale (12m x 10m et 0,60m de haut).

- *TH 2* - Altitude : 1313m.

    Situé à 8m à l'Est du précédent. Tertre de forme ovale recouvert de végétation, mesurant 10m x 8m et 0,60m de hauteur.

- *TH 3* - Altitude : 1311m.

    Situé à 6m à l'Est Sud-Est du précédent.

Historique

Tertres découverts par [Blot J.]{.creator} en [1978]{.date} et jamais publiés. Ils ont été revisités par [Meyrat F.]{.creator} en [août 2017]{.date}.

### [Lanne]{.spatial} - [Hournères]{.coverage} (col de) - [Cromlech]{.type}

Localisation

Altitude : 1475m.

Erigé dans le col lui-même, sur terrain plat.

Description

Cercle de 2m de diamètre délimité par 7 pierres de très faible hauteur ; il n'y a pas de tumulus mais un très faible bourrelet périphérique ; une légère dépression est visible en secteur Sud-Est. Deux témoins se détachent par leurs dimensions : en secteur Sud-Est, une pierre au ras du sol, mesurant 0,75m x 0,50m ; en secteur Sud-Ouest, une autre de 0,30m de haut et 0,60m x 0,50m.

Historique

Cercle découvert par [Meyrat F.]{.creator} en [décembre 2016]{.date}.

### [Lanne]{.spatial} - [Hournères]{.coverage} (ravin) - [Tertres d'habitat]{.type}

Localisation

Altitude : 1319m.

Les 2 tertres sont situés, sur la rive Ouest du ruisseau donnant naissance à celui empruntant le ravin de Hournères ; terrain en légère pente vers le Sud.

Description

- *Tertre n°1* : le plus à l'Est, à une vingtaine de mètres à l'Ouest du ruisseau ; il mesure 5m x 5m et 0,60m de haut.

- *Tertre n°2* : situé à 30m à l'Ouest du précédent, il mesure 6m x 5m et 0,30m de haut.

Historique

Monuments découverts par [Meyrat F.]{.creator} en [décembre 2016]{.date}.

### [Lanne]{.spatial} - [Isiburie]{.coverage} Nord - [Tertres d'habitat]{.type}

Localisation

Altitude : 1324m.

Cet ensemble de 3 tertres et plus est situé sur un terrain en légère pente vers l'Est.

Description

- *Tertre n°1* : il est situé à 30m environ au Nord des ruines bien visibles d'une borde située en bordure Est de la ligne de crête ; à peu près circulaire de 9m de diamètre, il mesure 0,80m de haut.

- *Tertre n°2* : situé à 3m au Nord du précédent, mesure 9m x 8m et environ 1m de haut.

- *Tertre n°3* : situé à 25m environ au Nord Nord-Ouest du précédent, mesure 8m x 7m et 0,60m de haut.

- *Autres tertres* : la végétation a rendu difficile le comptage de ces tertres situés à l'Ouest Nord-Ouest, une quarantaine de mètres plus bas sur la pente.

Historique

Tertres découverts par [Meyrat F.]{.creator} en [décembre 2016]{.date}.

### [Lanne]{.spatial} - [Isiburie]{.coverage} Sud - [Tertres d'habitat]{.type}

Localisation

Altitude : 1343m.

Situés sur la crête qui sépare les deux ruisseaux donnant naissance à l'Arrec d'Isiburie ; les 4 tertres sont peu visibles (faible hauteur) et érigés sur un terrain en légère pente vers le Sud-Est.

Description

- *Tertre n°1* : mesure 6m x 4m et 0,30m de haut.

- *Tertre n°2* : il est quasi tangent au Sud du précédent ; mesure 6m x 5m et 0,40m de haut.

- *Tertre n°3* : Situé à 10m à l'Est du précédent ; mesure 6m x 6m et 0,20m de haut.

- *Tertre n°4* : situé à 3m au Sud du précédent ; il mesure 6m x 6m et 0,20m de haut. Il est à une trentaine de mètres au Nord-Ouest de l'Arrec (ruisseau).

Historique

Tertres découverts par [Meyrat F.]{.creator} en [décembre 2016]{.date}.

### [Lanne]{.spatial} - [Lacurde (col de)]{.coverage} - [Cromlech]{.type}

Localisation

Altitude : 1336m.

Il est sur terrain plat, au niveau même du col, au pied du mont Iguntze.

Description

Sur les trois éléments repérés, nous dirons qu'un seul cercle de pierres nous paraît le plus plausible, les deux autres paraissant plus être des éléments naturels que des artefacts.

- *Cercle n°1* : de 1,10m de diamètre, délimité par une dizaine de pierres de petit calibre, ne dépassant que de peu le sol ; l'une d'entre elles, plus importante est située au Nord et mesure 0,29m x 0,28m et 0,20m de haut. Au centre apparaissent 4 autres petites pierres.

- Le *Cercle (?) n°2* : situé à 4m à l'Est du précédent, mesurerait 1,10m de diamètre, délimité par 5 pierres (en demi-cercle).

- Le *Cercle (?) n°3* est à 4m au Nord-Est du n°1 et à 24m au Nord du n°2 ; il serait délimité par 4 pierres, dont une de 0,70m x 0,68m et 0,40m de haut, contrastant avec les 3 autres peu visibles.

Historique

Éléments découverts par [Meyrat F.]{.creator} en [octobre 2016]{.date}.

### [Lanne]{.spatial} - [Lacurde (col de)]{.coverage} - [Tertres d'habitat]{.type} ([?]{.douteux})

Localisation

Altitude: 1338m.

Ces trois probables tertres d'habitat sont situés dans le secteur Nord-Est du col de Lacurde, à une trentaine de mètres environ au Nord-Est de *Lacurde (col de) - Cromlech*.

Description

- *TH 1* : il est situé à 2m à l'Est des ruines d'une borde, dont seuls subsistent les fondations. Tertre de terre allongé de faible hauteur (0,40m), mesurant 15m x 6m.

- *TH 2* : situé à 2m à l'Ouest Sud-Ouest des mêmes ruines ; tertre de très faible hauteur (0,15m), mesurant 7m x 4m.

- *TH 3* : Situé à 20m au Nord-Ouest de *TH 1 et à 9m au Nord de *TH 2* ; il mesure 0,30m de haut et 10m x 9m.

Historique

Ces monuments, douteux car difficiles à identifier étant donnée leur faible hauteur, ont été trouvés par [Meyrat F.]{.creator} en [Novembre 2016]{.date}.

### [Lanne]{.spatial} - [Pâturages de Montory]{.coverage} - [Tertres d'habitats]{.type}

Localisation

Altitude : 1097m.

L'ensemble de ces tertres (plus de 20) se trouve réparti en deux groupes distants d'une cinquantaine de mètres sur un vaste pâturage en pente douce vers le Nord, et sont entourés de part et d'autre par deux petits ruisseaux de montagne.

Description

- *Le Groupe Nord* comporte au moins une quinzaine de tertres terreux de forme ovale, mesurant en moyenne 10m x 4m et 0,60m de haut.

- *Le Groupe Sud*, riche de treize tertres ou plus (la végétation abondante rend difficile leur repérage exact), ont, pour beaucoup les mêmes dimensions que ceux du groupe sud ; toutefois certains peuvent atteindre 10m x 6m et 0,60m de haut, ou 10m x 8m et 0,80m de haut et même 15m x 4m et 0,60m de haut ; enfin l'un d'eux, le plus au Sud, atteint 22m x 8m et 0,40m de haut.

Historique

Ces tertres ont été identifiés par [J. Blot]{.creator} en [1978]{.date}, mais n'ont jamais été publiés, les documents ayant été égarés ; ils ont été revisités en [octobre 2016]{.date} par [F. Meyrat]{.creator}.

### [Lanne]{.spatial} - [Sulatcé]{.coverage} - [Cromlech]{.type}

Localisation

Altitude : 1342m.

Il est situé sur un replat qui domine un virage « en épingle à cheveu » de la route.

Description

Il semble bien qu'on puisse distinguer un cercle de 4,50m de diamètre, délimité par environ 6 pierres. Ce très probable monument a été remanié, (quand ?, par qui ?, pourquoi ?) de sorte que certaines pierres de la périphérie ont pu être enlevées, et que la [ciste]{.subject} centrale a été en partie mise au jour...

Historique

Monument découvert en [juillet 2016]{.date} par [Blot J.]{.creator}.
