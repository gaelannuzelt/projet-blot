# Gamarthe

### [Gamarthe]{.spatial} - [Eheta]{.coverage} Nord 1 - [Structure rectangulaire]{.type} ([?]{.douteux})

Localisation

Altitude : 531m.

Situé à l'extrémité Nord d'un replat qui s'étend lui-même au Nord de la colline d'Eheta, à 3m environ au Sud-Est d'un important filon rocheux.

Description

Structure rectangulaire, constituée de petits blocs de [calcaire]{.subject} blanc, plus ou moins de la taille d'un pavé, posés ou peu enfouis dans le sol, disposés de façon assez lâche sur 3 faces et délimitant ainsi un rectangle ouvert au Sud. Les « murs » Est et Ouest mesurent 2,80m de long et plus ou moins 0,30m de large ; le mur Nord mesure 1,90m de long et 0,50m de large en moyenne. Il s'agit fort probablement des vestiges très discrets d'un habitat sommaire érigé à cet endroit... mais quand ? ...

Historique

Découvert par [Meyrat F.]{.creator} en [avril 2016]{.date}.

### [Gamarthe]{.spatial} - [Eheta]{.coverage} Nord 2 - [Structure circulaire]{.type} ([?]{.douteux})

Localisation

Tangente au Sud-Ouest du précédent, à l'extrémité Sud du « mur » Ouest de la structure rectangulaire.

Description : Petite structure circulaire constituée d'une vingtaine de petits blocs de [calcaire]{.subject} blanc plus ou moins enfoncés dans le sol ; « monument » très douteux.

Historique

Découvert par [Meyrat F.]{.creator} en [avril 2016]{.date}.

### [Gamarthe]{.spatial} - [Eheta]{.coverage} Nord 3 - [Structure tumulaire]{.type} ([?]{.douteux})

Localisation

Il est situé à 2m au Nord de la structure rectangulaire.

Description

Tumulus pierreux circulaire de 1,40m de diamètre et 0,20m de haut. ; nombreux petits blocs de [calcaire]{.subject} blanc en surface et en périphérie, évoquant un petit tumulus cromlech... monument douteux.

Historique

Découvert par [Meyrat F.]{.creator} en [avril 2016]{.date}.

### [Gamarthe]{.spatial} - [Orgamendy]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 494m.

Situé dans un col au Sud de la colline Orgamendy, et du chemin qui y monte, sur une crête de terrain séparant deux versants vers l'Est et vers l'Ouest.

Description

Tumulus terreux, de 9m à 10m de diamètre, (difficile à apprécier, car de faible hauteur : 0,30m environ) et présentant une légère dépression sur sa périphérie Ouest ; pas de pierres visibles.

Historique

Tumulus découvert par [Meyrat F.]{.creator} en [avril 2016]{.date}.
