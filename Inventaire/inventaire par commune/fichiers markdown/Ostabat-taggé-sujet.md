# Ostabat

### [Ostabat]{.spatial} - [Lindungneko lepoa]{.coverage} - [Tumulus]{.type}

Localisation

Carte 1345 Est Iholdy.

Altitude : 350m.

Nous avons publié [@blotNouveauxVestigesProtohistoriques1975, p. 109], toute une série de monuments couronnant les croupes du Lantabat. Nous y ajoutons ce dernier, situé entre les tertres d'Ipharlaze et ceux de Lindugnekolepoa.

Il est situé à 100m à l'Est du col de ce nom qu'il domine, sur un léger replat tangent à la bordure relevée d'un petit fossé délimitant lui-même un pâturage enclos de barbelés.

Description

Tumulus terreux de 14m de diamètre et 0,50m de haut.

Historique

Monument découvert en [août 1971]{.date}.

### [Ostabat]{.spatial} - [Ipharlatze]{.coverage} 1 - [Tumulus]{.type}

Localisation

Altitude : 328m.

Situé au niveau du col à environ une quarantaine de mètres au Sud-Ouest de la route.

Description

Tumulus circulaire de terre et de terre de 14m à 15m de diamètre, et 0,80m de haut.

Historique

Monument découvert par [Blot J.]{.creator} en [novembre 1975]{.date}.

### [Ostabat]{.spatial} - [Ipharlatze]{.coverage} 2 - [Tumulus]{.type}

Localisation

Situé à une vingtaine de mètres au Sud de *Ipharlatze 1 - Tumulus*.

Description

Tumulus circulaire de 12m de diamètre et 0,80m de haut, de terre et de pierres.

Historique

Monument découvert par [Blot J.]{.creator} en [novembre 1975]{.date}.

## [Ostabat]{.spatial} - [Gagneko Ordokia]{.coverage} : les [tertres pierreux]{.type}

Nous décrirons ici un ensemble de tertres pierreux dont certains ont été découverts par [J. Blot]{.creator} en [1975]{.date}, d'autres par le [groupe Hilarriak]{.creator} en [mars 2013]{.date}, les autres enfin par J. Blot en [avril 2013]{.date}.

Commune d'Ostabat et de [Lantabat]{.spatial}. Nous les citons tous ici pour faciliter la localisation de ces tertres les uns par rapport aux autres - Toutefois **les tertres n°14, 15, 16 et 19 sont dans la commune de Lantabat**.


### [Ostabat]{.spatial} - [Gagneko Ordokia]{.coverage} 1 - [Tertre pierreux]{.type}

Localisation

Altitude : 401m.

Il est situé à environ 75m au Nord Nord-Est du *grand Tumulus*.

Description

Tertre pierreux de 4m de diamètre et quelques centimètres de haut.

Historique

Monument découvert par [J. Blot]{.creator}.

### [Ostabat]{.spatial} - [Gagneko Ordokia]{.coverage} 2 - [Tertre pierreux]{.type}

Localisation

Situé à 1m à l'Ouest Nord-Ouest de *Gagneko Ordokia 1 - Tertre pierreux*.

Description

Tertre pierreux mesurant 6m de diamètre ; faible hauteur.

Historique

Monument découvert par [J. Blot]{.creator}.

### [Ostabat]{.spatial} - [Gagneko Ordokia]{.coverage} 3 - [Tertre pierreux]{.type}

Localisation

Situé à 15m à l'Ouest de *Gagneko Ordokia 2 - Tertre pierreux*.

Description

Tertre pierreux de 3m de diamètre - Faible hauteur.

Historique

Monument découvert par [J. Blot]{.creator}.

### [Ostabat]{.spatial} - [Gagneko Ordokia]{.coverage} 4 - [Tertre pierreux]{.type}

Localisation

Situé à 6m au Nord Nord-Ouest de *Gagneko Ordokia 3 - Tertre pierreux*.

Description

Tertre pierreux de 3m de diamètre et de faible hauteur.

Historique

Monument découvert par [J. Blot]{.creator}.

### [Ostabat]{.spatial} - [Gagneko Ordokia]{.coverage} 5 - [Tertre pierreux]{.type}

Localisation

Tangent à l'Ouest de *Gagneko Ordokia 4 - Tertre pierreux*.

Altitude : 401m.

Description

Tertre pierreux de 5m de faible hauteur.

Historique

Monument découvert par [J. Blot]{.creator}.

### [Ostabat]{.spatial} - [Gagneko Ordokia]{.coverage} 6 - [Tertre pierreux]{.type}

Localisation

Altitude : 401m.

Il est situé à 33m à l'Est de *Gagneko Ordokia 1 - Tertre pierreux*.

Description

Tertre de pierres bien visibles, de 6,50m de diamètre et 0,30m de hauteur.

Historique

Monument découvert par le [groupe Hilarriak]{.creator}.

### [Ostabat]{.spatial} - [Gagneko Ordokia]{.coverage} 7 - [Tertre pierreux]{.type}

Localisation

Situé à 4m à l'Est de *Gagneko Ordokia 6 - Tertre pierreux*.

Description

Mesure 4m de diamètre et de faible hauteur.

Historique

Monument découvert par [J. Blot]{.creator}.

### [Ostabat]{.spatial} - [Gagneko Ordokia]{.coverage} 8 - [Tertre pierreux]{.type}

Localisation

Situé à 100m environ au Nord de *Gagneko Ordokia 1 - Tertre pierreux*.

Altitude : 401m.

Historique

Monument découvert par le [groupe Hilarriak].

### [Ostabat]{.spatial} - [Gagneko Ordokia]{.coverage} 9 - [Tertre pierreux]{.type}

Localisation

Situé à 20m au Nord de *Gagneko Ordokia 8 - Tertre pierreux*.

Description

Mesure 5m de diamètre ; de faible hauteur.

Historique

Monument découvert par le [groupe Hilarriak]{.creator}.

### [Ostabat]{.spatial} - [Gagneko Ordokia]{.coverage} 10 - [Tertre pierreux]{.type}

Localisation

Situé à 2m au Sud de *Gagneko Ordokia 8 - Tertre pierreux*.

Description

Mesure 3m de diamètre et de faible hauteur.

Historique

Monument découvert par [J. Blot].

### [Ostabat]{.spatial} - [Gagneko Ordokia]{.coverage} 11 - [Tertre pierreux]{.type}

Localisation

Situé à 2m au Sud de *Gagneko Ordokia 10 - Tertre pierreux*.

Description

Mesure 3m de diamètre ; de faible hauteur.

Historique

Monument découvert par [J. Blot]{.creator}.

### [Ostabat]{.spatial} - [Gagneko Ordokia]{.coverage} 12 - [Tertre pierreux]{.type}

Localisation

Altitude : 401m.

Il est situé à plusieurs dizaines de mètres au Nord de *Gagneko Ordokia 9 - Tertre pierreux*.

Description

Mesure 5,50m de diamètre et 0,40m de haut.

Historique

Monument découvert par [J. Blot]{.creator}.

### [Ostabat]{.spatial} - [Gagneko Ordokia]{.coverage} 13 - [Tertre pierreux]{.type}

Localisation

Situé à 15m à l'Ouest de *Gagneko Ordokia 12 - Tertre pierreux*.

Description

Mesure 4,90m de diamètre et 0,40m de haut.

Historique

Monument découvert par [J. Blot]{.creator}.

### [Ostabat]{.spatial} - [Gagneko Ordokia]{.coverage} 14 - [Tertre pierreux]{.type}

Localisation

Altitude : 397m.

Il est situé à 50m au Nord de *Gagneko Ordokia 13 - Tertre pierreux*.

Description

Tertre pierreux très visible, étalé sur terrain en pente vers le Nord-Ouest ; mesure 6m de diamètre.

Historique

Monument découvert par [J. Blot]{.creator}.

### [Ostabat]{.spatial} - [Gagneko Ordokia]{.coverage} 15 - [Tertre pierreux]{.type}

Localisation

Situé à 11m à l'Ouest de *Gagneko Ordokia 14 - Tertre pierreux*.

Description

Mesure 4,90m de diamètre et 0,40m de haut.

Historique

Monument découvert par [J. Blot]{.creator}.

### [Ostabat]{.spatial} - [Gagneko Ordokia]{.coverage} 16 - [Tertre pierreux]{.type}

Localisation

Altitude : 396m.

Situé à 13m au Nord Nord-Ouest du *n°15*.

Description

Mesure 3,50m de diamètre et 0,40m de haut.

Historique

Monument découvert par [J. Blot]{.creator}.

### [Ostabat]{.spatial} - [Gagneko Ordokia]{.coverage} 17 - [Tertre pierreux]{.type}

Localisation

Situé à 3m au Sud-Est de la piste.

Description

Tertre pierreux de 2,40m de diamètre et de faible hauteur.

Historique

Monument découvert par [J. Blot]{.creator}.

### [Ostabat]{.spatial} - [Gagneko Ordokia]{.coverage} 18 - [Tertre pierreux]{.type}

Localisation

Situé à 1m à l'Ouest de *Gagneko Ordokia 17 - Tertre pierreux*.

Description

Mesure 2m de diamètre, très faible hauteur.

Historique

Monument découvert par [J. Blot]{.creator}.

### [Ostabat]{.spatial} - [Gagneko Ordokia]{.coverage} 19 - [Tertre pierreux]{.type}

Localisation

Altitude : 400m.

Il est tangent à la piste, au Nord.

Description

Mesure 4,30m de diamètre et 0,30m de haut.

Historique

Monument découvert par [J. Blot]{.creator} en [2013]{.date}.
