# Uhart-Mixe

### [Uhart-Mixe]{.spatial} - [Beneditenia]{.coverage} - [Dolmen]{.type} (Faux dolmen) ([?]{.douteux})
Localisation

Altitude : 110m.

Situé au point où le sentier de GR 65 se détache de la petite route qui mène à Orsanco, ce « monument » est constitué de 3 dalles de schiste plantées en forme de pseudo-dolmen, sans tumulus visible.

Historique

Monument signalé par [P. Velche]{.creator} en [juin 2011]{.date} ; après enquête, il s'avère que ces dalles ont été plantées en 1996 par le curé en charge des villages avoisinants, de même que le pseudo menhir érigé derrière la chapelle de Soyarce.

### [Uhart-Mixe]{.spatial} - [Soyarce]{.coverage} 3 - [Tumulus]{.type}

Localisation

Altitude : 284m.

Il est situé à 20m au Nord-Ouest de *Soyarce 3 - Tumulus*.

[@blotNouveauxVestigesProtohistoriques1975], tangent à la berge Sud du chemin de Soyarce, et immédiatement à l'Ouest d'un abreuvoir en ciment.

Description

Tumulus de terre et de pierres de 9m à 10m de diamètre environ et 0,40m de haut.

Historique

Monument découvert par [Blot J.]{.creator} en [mars 1976]{.date} ; non publié depuis.

### [Uhart-Mixe]{.spatial} - [Soyarce]{.coverage} 4 - [Tumulus]{.type} ([?]{.douteux})

Localisation

Altitude : 276m.

Situé à quelques dizaines de mètres au Nord-Ouest du chemin qui descend de Soyarce vers « Gibraltar ».

Description

Tumulus érigé sur un terrain en légère pente vers le Sud-Est, de 6m à 7m de diamètre, et de 0,30m à 0,40m de haut, formé de terre et de nombreuses pierres. Sa hauteur irrégulière, le volume des pierres nous feraient penser à un possible (?) tas d'épierrage.

Historique

Monument trouvé par le [groupe Hilharriak]{.creator} en [décembre 2011]{.date}.

### [Uhart-Mixe]{.spatial} - [Soyarce]{.coverage} 5 - [Tumulus]{.type}

Localisation

Altitude : 285m.

Il est situé à 20m au Sud-Est de *Soyarce 3 - Tumulus* [@blotNouveauxVestigesProtohistoriques1975].

Description

Tumulus pierreux très net mais de très faible hauteur, étalé en forme de galette circulaire de 4,60m de diamètre.

Historique

Monument découvert par [Blot J.]{.creator} en [janvier 2012]{.date}.

### [Uhart-Mixe]{.spatial} - [Soyarce]{.coverage} 6 - [Tumulus pierreux]{.type} ([?]{.douteux})

Localisation

Altitude : 270m.

Il se trouve tangent au bord Est du chemin qui vient de la chapelle de Soyarce. Sur un terrain en légère pente vers le Sud, il mesure 7m à 8m de diamètre, 0,30m de haut, constitué de nombreuses petites dalles du matériau local (marne).

Historique

Monument trouvé par [Blot J.]{.creator} en [janvier 2012]{.date}.
