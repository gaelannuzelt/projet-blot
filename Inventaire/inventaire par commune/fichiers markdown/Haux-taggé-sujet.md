# Haux

### [Haux]{.spatial} - [Apolotzegagna]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 703m.

Ce tumulus se trouve à l'extrémité Est de la crête sommitale du sommet du mont Apoltzegagna, avant la rupture de pente.

Description

Tumulus de terre et de pierres de 5m à 6m de diamètre et 0,40m à 0,50m de haut.

Historique

[Nous]{.creator} avions décrit ce tumulus en [1979]{.date} mais sans en donner la localisation exacte, d'où la rectification actuelle.

### [Haux]{.spatial} - [Haux (Bois de)]{.coverage} - [Tumulus]{.type} ([?]{.douteux})

Localisation

Altitude : 1080m.

Situé sur un vaste promontoire herbeux dominant le col d'Ordabure ; il se trouve à l'extrémité Ouest de celui-ci, à la rupture de pente.

Description

Tumulus en forme de galette aplatie circulaire, mesurant 4,50m de diamètre et 0,15m à 0,20m de haut ; il est constitué de terre et des fragments de [calcaire]{.subject} concassé sont visibles en surface, en particulier dans la région centrale. Tumulus très douteux, une simple lentille de solifluxion étant aussi fort probable...

Historique

Tumulus découvert par [Meyrat F.]{.creator} en [mai 2017]{.date}.

### [Haux]{.spatial} - [Hilague]{.coverage} (col de) - [Tertre d'habitat]{.type}

Localisation

Altitude : 1275m.

On peut le voir à environ 80m à l'Est Sud-Est du relais téléphonique érigé au débouché de la piste à ce col ; il domine cette piste, étant construit sur une petite hauteur à l'Est de celle-ci.

Description

Tertre classique, asymétrique, allongé selon un axe Est-Ouest mesurant 6m et l'axe Nord-Sud étant de 5m.

Historique

Tertre découvert par [J. Blot]{.creator} en [octobre 2016]{.date}.

### [Haux]{.spatial} - [Iguntze]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 1381m.

Situé au sommet d'Iguntze et en descendant le versant Est de cette montagne vers le col de Lacurde (en suivant la ligne de crête), on peut distinguer, sur un léger replat, un probable tumulus.

Description

Tumulus terreux en forme de galette circulaire très aplatie, de 8m de diamètre et 0,10m à 0,15m de haut environ ; la piste passe en son milieu. Pas d'éléments pierreux visibles.

Historique

Tumulus découvert par [F. Meyrat]{.creator} en [novembre 2016]{.date}.

### [Haux]{.spatial} - [Losco]{.coverage} (col de) - [Tumulus]{.type}

Localisation

Altitude : 1062m.

Ce tumulus se trouve au col de Losco, à une quinzaine de mètres à l'Ouest de la piste qu'il domine et qui amorce à cet endroit un virage vers le Nord.

Description

Tumulus peu visible, circulaire, terreux (quelques rares petites pierres sont visibles) ; il mesure 8m de diamètre, 0,20m environ de haut, et présente une légère dépression centrale.

Historique

Tumulus découvert par [J. Blot]{.creator} en [octobre 2016]{.date}.
