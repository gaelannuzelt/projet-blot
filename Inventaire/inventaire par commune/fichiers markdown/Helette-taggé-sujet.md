# Helette

### [Helette]{.spatial} - [Erregelu]{.coverage} 1 - [Tumulus-cromlech]{.type}

Localisation

Altitude : 485m.

Sur un replat au flanc Nord du mont Baigoura, à 600m environ à l'Est de la route asphaltée qui monte aux antennes. Situé vers l'extrémité Est du plateau, à une vingtaine de mètres au Nord de la piste pastorale.

Description

Tumulus de terre d'environ 18m de diamètre et 0,40m de haut. Sa périphérie est délimitée par une dizaine de pierres plus ou moins visibles ; il semblerait même qu'on puisse deviner deux autres cercles concentriques, l'un de 10m de diamètre, l'autre de 6m.

Historique

Monument découvert par [I. Txintxuretta]{.creator} en [janvier 2015]{.date}.

### [Helette]{.spatial} - [Erregelu]{.coverage} 2 - [Cromlech]{.type}

Localisation

Altitude : 486m.

Sur un replat au flanc Nord du mont Baigoura, à 600m environ à l'Est de la route asphaltée qui monte aux antennes. Il est situé à une quinzaine de mètres au Nord-Ouest de *Erregelu 1 - Tumulus-cromlech*.

Description

Cercle de 4m de diamètre, délimité par une dizaine de pierres au ras du sol, mais bien visibles.

Historique

Monument découvert par [I. Txintxuretta]{.creator} en [janvier 2015]{.date}.

### [Helette]{.spatial} - [Erregelu]{.coverage} 3 - [Cromlech]{.type} ([?]{.douteux})

Localisation

Sur un replat au flanc Nord du mont Baigoura, à 600m environ à l'Est de la route asphaltée qui monte aux antennes. Monument que nous n'avons pas bien identifié, qui serait à 4m à l'Est Nord-Est de *Erregelu 2 - Cromlech*.

Description

Cercle de 4m délimité par 5 pierres.

Historique

Monument découvert par [I. Txintxuretta]{.creator} en [janvier 2015]{.date}.

### [Helette]{.spatial} - [Erregelu]{.coverage} 3 bis - [Cromlech]{.type} ([?]{.douteux})

Localisation

Sur un replat au flanc Nord du mont Baigoura, à 600m environ à l'Est de la route asphaltée qui monte aux antennes. Situé à 4m à l'Est Nord-Est de *Erregelu 3 - Cromlech (?)*.

Description

Monument tout aussi douteux que le précédent. Mesure 4m de diamètre et délimité par 8 pierres environ.

Historique

Monument découvert par [F. Meyrat]{.creator} en [février 2015]{.date}.

### [Helette]{.spatial} - [Erregelu]{.coverage} 4 - [Cromlech]{.type}

Localisation

Altitude : 485m.

Sur un replat au flanc Nord du mont Baigoura, à 600m environ à l'Est de la route asphaltée qui monte aux antennes.

Description

Cercle de 4m de diamètre, délimité par 6 pierres à la surface aplanie, bien visibles.

Historique

Monument découvert par [I. Txintxuretta]{.creator} en [janvier 2015]{.date}.

### [Helette]{.spatial} - [Erregelu]{.coverage} 5 - [Cromlech]{.type} ([?]{.douteux})

Localisation

Altitude : 490m.

Sur un replat au flanc Nord du mont Baigoura, à 600m environ à l'Est de la route asphaltée qui monte aux antennes. Situé à une quarantaine de mètres à l'Ouest Sud-Ouest de *Erregelu 4 - Cromlech*.

Description

Monument peu facile à voir, douteux ! Cercle de 9m de diamètre, délimité par 3 ou 4 pierres.

Historique

Monument découvert par [I. Txintxuretta]{.creator} en [janvier 2015]{.date}.

### [Helette]{.spatial} - [Erregelu]{.coverage} 6 - [Cromlech]{.type}

Localisation

Sur un replat au flanc Nord du mont Baigoura, à 600m environ à l'Est de la route asphaltée qui monte aux antennes. Situé à 5m à l'Ouest Nord-Ouest de *Erregelu 5 - Cromlech (?)*.

Description

Cercle mesurant 5m de diamètre et délimité par environ 7 pierres.

Historique

Monument découvert par [I. Txintxuretta]{.creator} en [janvier 2015]{.date}.

### [Helette]{.spatial} - [Erregelu]{.coverage} 7 - [Tumulus]{.type} ([?]{.douteux})

Localisation

Altitude : 488m.

Sur un replat au flanc Nord du mont Baigoura, à 600m environ à l'Est de la route asphaltée qui monte aux antennes. Il est à 35m à l'Ouest de *Erregelu 1 - Tumulus cromlech*.

Description

Tumulus (douteux ?) de terre de 10m de diamètre environ et 0,30m de haut, à la surface duquel apparaissent quelques pierres sans ordre apparent.

Historique

Monument trouvé par [F. Meyrat]{.creator} en [février 2015]{.date}.

### [Helette]{.spatial} - [Erregelu]{.coverage} 8 - [Tertre d'habitat]{.type}

Localisation

Altitude : 498m.

Sur un replat au flanc Nord du mont Baigoura, à 600m environ à l'Est de la route asphaltée qui monte aux antennes. Il est situé sur un terrain en légère pente vers le Sud, à 18m à l'Est d'un petit ru, et à 12m au Nord Nord-Ouest de la piste pastorale.

Description

Tertre asymétrique mesurant 7m de long et environ 6m de large pour 0,30m de haut.

Historique

Monument trouvé par [F. Meyrat]{.creator} en [février 2015]{.date}.

### [Helette]{.spatial} - [Etxolalde]{.coverage} - [Monolithe]{.type}

Localisation

Altitude : 488m

Ce monolithe est situé au versant nord du mont Baigoura, sur un léger replat, à une cinquantaine de mètres au sud du sentier de découverte du Baigoura, et à une centaine de mètres au Nord-Ouest d'une petite borde dénommée Etxolalde. À noter enfin une tourbière et un point d'eau à environ 150m au Nord-Ouest du monolithe.

Description

Volumineux bloc de [grés]{.subject} blanc en forme de pain de sucre, couché sur le sol et orienté NE-SO, comme la plupart des grand monolithes d'Iparralde.

Il mesure 4,10m de long, 2,40m dans sa plus grande largeur, 1,42m de large avant le rétrécissement du sommet, et 0,90m à sa base. Son épaisseur est variable : O,50m au sommet ; côté Nord-Ouest : en son milieu elle atteint 0,90m d'épaisseur et 1,90m vers sa base. Le côté Sud-Ouest n'ayant pas été suffisamment dégagé en profondeur, nous n'avons pas d'estimations. À sa base, dans sa partie dégagée, l'épaisseur atteint 1,30m.

Son poids peut être estimé aux alentours de 16 tonnes (B. Auriol).
Enfin ce bloc de [grés]{.subject} présente quelques traces d'épannelage à son sommet, régularisant la symétrie de celui-ci, la nature ayant spontanément fourni le reste de la forme souhaitée.

Historique

Ce monolithe, profondément enfoui et dans la végétation et dans le sol, a été fortuitement découvert par [Joanes Laco]{.creator} lors d'une partie de chasse en [octobre 2019]{.date}. À notre demande et pour confirmation et étude, le monument a été plus largement dégagé ensuite.

### [Helette]{.spatial} - [Garralda]{.coverage} 3 - [Tumulus]{.type}

Localisation

Carte 1345 Est Iholdy.

Altitude : 330m.

Nous avons décrit [@blotNouveauxVestigesMegalithiques1973a p.195], un dolmen, un tumulus-cromlech (*n°1*) et un tumulus simple (*n°2*). Le tumulus n°3 est situé à 2m à l'Ouest du *n°2*.

Description

Tumulus en terre de 7m de diamètre et 0,40m de haut.

Historique

Monument découvert en [juin 1975]{.date}.
