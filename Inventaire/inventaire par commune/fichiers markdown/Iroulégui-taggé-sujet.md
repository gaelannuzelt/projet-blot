# Iroulégui

### [Iroulégui]{.spatial} - [Artxuita]{.coverage} - [Ciste]{.type} ([?]{.douteux})

Localisation

Altitude :412m.

Ce monument est situé sur un petit plateau habituellement encombré de végétation (fougères et ajoncs) à environ une quarantaine de mètres au Nord-Ouest du dolmen d'Artxuita qui est bien visible, à une quarantaine de mètres à l'Ouest de la route qui mène au sommet du Jarra.

Description

Monument douteux, visible uniquement après écobuage. Quelques dalles, au ras du sol, délimitent ce qui pourrait être une chambre funéraire mesurant environ 2m de long et 1,30m de large, orientée Nord-Est Sud-Ouest. Il existe 2 dalles principales : la dalle **a**, à l'Ouest, mesurant 1,30m de long et 0,41m de large, éversée vers l'intérieur ; la dalle **b**, à l'Est, mesurant 1,30m de long et 0,50 de large, elle aussi éversée vers l'intérieur. Deux petites dalles plantées dans le sol (**c** = 0,40m de long et **d** = 0,26m de long) complètent la limite de la chambre à l'Ouest. Trois autres : (**e** = 0,10m, **f** =0,30m et **g** = 0,37m) formeraient la limite Nord-Est de cette chambre. À noter la présence d'un important bloc pierreux au centre (**h**) d'environ 0,80m de long...

Dans le contexte pierreux environnant il est très difficile d'évoquer l'existence d'un éventuel péristalithe...

Historique

Ciste découvert par [I. Txintxuretta]{.creator} en [mars 2016]{.date}.

### [Iroulégui]{.spatial} - [Jarra (Erramonénéa)]{.coverage} - [Dolmen]{.type} ([?]{.douteux})

Localisation

Altitude : 355m.

Ce faux dolmen est situé dans une prairie, à 140m au Sud Sud-Est de la maison Erramonénéa.

Description

Ses différents éléments constitutifs, en [grés]{.subject} rose, peuvent faire croire aux restes bien conservés d'un dolmen, à la [dolmen]{.subject} ouverte à l'Est, et dont le montant Nord, vertical, mesurerait 1,50m de long et 1,10m de haut ainsi que 0,50m d'épaisseur ; la dalle de chevet, verticale, située à l'Ouest Nord-Ouest mesurerait 0,70m de haut et 0,45m d'épaisseur. Divers éléments rocheux, au Sud du montant Nord, et au Nord-Ouest de cet ensemble pourraient faire croire à l'autre montant de la chambre funéraire et à un reste de la dalle de couverture.

Il ne s'agit en fait que d'un seul bloc rocheux éclaté probablement par le gel, dont aucun des éléments n'est véritablement enfoncé dans le sol. Même le propriétaire des lieux pense qu'il s'agit d'un dolmen...

### [Iroulégui]{.spatial} - [Jarra (Erramonénéa)]{.coverage} - [Monolithe]{.type} ([?]{.douteux})

Localisation

Il est situé à 150m au Nord Nord-Est de la maison Erramonénéa, au milieu d'un bois de châtaignier, et à 50m au Nord Nord-Ouest d'une petite bergerie située au bord d'une prairie entourée d'un muret de pierres sèches.

Description

Ce bloc rocheux mesure 1,30m à 1,40m de haut, 1m de large à la base et 0,80m d'épaisseur. Si sa situation, isolé au sommet d'une légère éminence naturelle, laisse à penser à qu'il n'est pas venu là naturellement, sa forme, ses dimensions et le fait qu'il ne soit que posé sur le sol ne nous incite pas à le classer dans les « monolithes » au sens où nous l'entendons, à savoir comme ancienne borne pastorale...

Historique

Ce bloc rocheux nous avait été signalé en [1996]{.date} par [I. Gaztelu]{.creator} comme pouvant être un monolithe.

### [Iroulégui]{.spatial} - [Jarra (Erramonénéa)]{.coverage} - [Monolithe]{.type} ([?]{.douteux})

Localisation

À 50m au Sud Sud-Ouest de la maison Erramonénéa (en lisière d'un bois de châtaignier).

Description

Pierre couchée au sol, rectangulaire, mesurant 2,40m de long, 1m de large et 0,35m d'épaisseur en moyenne. Une base plus large et plus épaisse (1,20m de large et 0,70m d'épaisseur) contraste avec l'autre extrémité, de dimensions plus modestes (0,50m de large et 0,13m d'épaisseur), arrondie, plus fine, qui semble avoir été épannelée. Cette pierre était couchée dans le champ, et les engins agricoles butaient souvent dessus, de sorte que le propriétaire l'a fait extraire et transportée à quelques mètres au Nord de son emplacement initial. Cette dalle pourrait *peut-être* (avec les réserves d'usage) avoir été une borne pastorale, quand elle était érigée, si elle l'a été.
