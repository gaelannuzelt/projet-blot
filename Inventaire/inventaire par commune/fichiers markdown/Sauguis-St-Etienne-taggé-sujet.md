# Sauguis-Saint-Etienne

### [Sauguis-Saint-Etienne]{.spatial} - [Zera]{.coverage} 6 - [Tumulus]{.type}

Localisation

Altitude : 640m.

Il se trouve sur un replat à mi-pente du flanc Nord du mont Zera, dominant le col de Saraxague. Il est situé au milieu de deux pistes pastorales qui passent à 2m de lui.

Description

Tumulus terreux érigé sur sol plat, mesurant 10m de diamètre et 0,70m de haut.

Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.
