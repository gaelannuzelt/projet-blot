# Montory

### [Montory]{.spatial} - [Erretzu]{.coverage} 1 - [Tumulus]{.type} ([?]{.douteux})

Localisation

Altitude : 693m.

Il est situé à l'extrémité Est Sud-Est d'un camp protohistorique.

Description

Situé en un point dominant de la crête, ce « tumulus » ovale, se présente sous la forme d'un amoncellement de bloc ou de dallettes de [grés]{.subject}, reposant, au Sud-Ouest sur un socle rocheux de grosses dalles naturellement en place. Les dimensions avoisinent 12m x 10m et 2m de haut environ. Depuis quelques années un petit cairn a été érigé sur son sommet.

Il est à noter que l'amoncellement de dallettes de [grés]{.subject} se poursuit vers le Nord, comme suite à un éboulement (?) sur 8m à 10m. Au vu de cet ensemble, on ne peut manquer d'évoquer la possibilité d'un rapport (mais lequel ?), de ce «tumulus» avec un stockage de pierres lié aux extractions de « lauzes » qui se faisaient au voisinage (d'où probablement le nom de « col de la LOSERE »). Nous resterons donc très prudent quant à l'époque et à la finalité de ce « tumulus ».

Historique

Tumulus découvert par [J. Blot]{.creator} en [1978]{.date} et publié dans [@blotSouleSesVestiges1979, p.22].

### [Montory]{.spatial} - [Erretzu]{.coverage} 2 - [Tumulus]{.type} ([?]{.douteux})

Localisation

Altitude : 692m.

Il est à 32m au Nord de *Erretzu 1 - Tumulus (?)*, à l'extrémité d'un filon pierreux naturel ; cependant son aspect et ses dimensions évoquent un possible artefact.

Description

Ce tumulus circulaire en forme de galette, mesure 3,80m de diamètre, 0,40m de haut ; il est constitué de terre et de pierres dont de nombreuses apparaissent en surface, sans qu'on puisse y distinguer un péristalithe évident.

Historique

Tumulus découvert par [F. Meyrat]{.creator} en [janvier 2016]{.date}.

### [Montory]{.spatial} - [Ourgaray]{.coverage} Est - [Cromlech]{.type}

Localisation

Altitude : 463m.

Il est à environ une cinquantaine de mètres au Nord Nord-Est de la cote 465.

Descriptrion

Sur un sol plat, au centre de la ligne de crête, cinq pierres délimitent un cromlech de 3m de diamètre ; la plus visible, au Nord, mesure 0,76m de long, 0,35m de large et 0,20m de haut.

Historique

Monument découvert par [F. Meyrat]{.creator} en [octobre 2015]{.date}.

### [Montory]{.spatial} - [Ourgaray]{.coverage} Ouest - [Tumulus]{.type}

Localisation

Altitude : 494m.

Tumulus visible au centre du plateau ; la piste pastorale est tangente à l'Est Sud-Est. Un piquet balise du sentier de randonnée, est planté dans le secteur Est, le plus saillant du tumulus.

Description

Tumulus terreux bien visible, de 4,20m de diamètre et 0,20m de haut, présentant une légère dépression centrale. Deux petites pierres sont visibles en surface à proximité immédiate du piquet.

Historique

Monument découvert par [F. Meyrat]{.creator} en [octobre 2015]{.date}.
