# Sainte-Engrâce

### [Sainte-Engrâce]{.spatial} - [Anhaou]{.coverage} (groupe Nord-Est) - [Tertres d'habitat]{.type} 

Localisation

Altitude : 1266m.

Cinq tertres situés à 15m au Nord-Est du virage de la route qui passe elle aussi au Nord-Est du cayolar de ce nom.

Description

De ces 5 tertres, un seul est très visible, mesurant 6m x 6m et 0,50m de haut, les autres ont des dimensions plus réduites par l'érosion, mais sont cependant bien identifiables.

Rappelons ici les 7 tertres très nets déjà décrits [@blotSouleSesVestiges1979, p.33]

Historique

Tertres découverts par [Blot J.]{.creator} en [octobre 2013]{.date}.

### [Sainte-Engrâce]{.spatial} - [Erainze]{.coverage} - [Tertres d'habitat]{.type}

Localisation

Altitude : 1431m.

Ces 6 tertres sont répartis en deux groupes, accolés aux parois rocheuses qui rétrécissent le passage, au fond de ce vaste cirque où sont construits les cayolars d'Erainze. On y accède par une piste qui part du col d'Erainze, au Sud.

On distingue un **Groupe Est**, constitué de 2 tertres et un **Groupe Ouest**, de 4 autres tertres, ces deux groupes étant répartis de part et d'autre du rétrécissement déjà signalé, par où passe la piste qui mène, bien plus bas, vers Ste Engrace, par les gorges d'Ehujarro.

Description

- **Groupe Est**

    - *Tertre 1* : Celui-ci est (comme les autres) adossé à la paroi rocheuse, au Nord. Il mesure 7m de diamètre et 1,70m de haut. À son sommet, aplati, large de 2,50m, on peut distinguer une couronne de quinze pierres environ, qui ont pu servir de contention à l'habitat provisoire construit le temps de l'estive. De nombreuses dolines à proximité assurent, avec le ruisseau, l'approvisionnement en eau.

    - *Tertre 2* : Quasi tangent, à l'Est, au précédent. Lui aussi adossé à la paroi rocheuse. Il mesure 8m de diamètre, et 1,70m de haut. Comme pour le précédent, on peut compter 8 pierres à son sommet.

- **Groupe Ouest**

    - *Tertre 3* : Altitude : 1431m. Lui aussi adossé à la paroi rocheuse, ce tertre mesure 10m de diamètre et 0,50m de haut ; sommet là encore couronné de quelques pierres.

    - *Tertre 4* : Situé à 1m à l'Est du précédent. Adossé à la paroi rocheuse, il mesure 6m de diamètre et 0,50m de haut.

    - *Tertre 5* : situé à 5m à l'Est du précédent, adossé à la paroi rocheuse, il mesure 9m de diamètre et 0,80m de haut ; de nombreuses pierres sont visibles à son sommet.

    - *Tertre 6* : Situé tangent au précédent, à l'Est, il est de taille beaucoup plus modeste, mesurant 4m de diamètre et 0,40m de haut.

Historique

Le *Tertre n°1* a été découvert par [A. Martinez]{.creator} en [septembre 2017]{.date}, le *n°2* par [Blot J.]{.creator} en fin [septembre 2017]{.date} et les 4 autres par [Meyrat F.]{.creator} à la même date.

### [Sainte-Engrâce]{.spatial} - [Erainze]{.coverage} - ([?]{.douteux})

Localisation

Altitude : 1440m.

Situé à quelques dizaines de mètres à l'Est de *Erainze - Tertres d'habitat* (TH 1 et 2), on note ce vestige d'un habitat (non datable), constitué d'une vingtaine de blocs de calcaire blanc, émergeant du sol, et disposés suivant un plan rectangulaire ; il mesure 2,60m de long et 1,40m de large, et son ouverture est orienté vers le Sud-Ouest. Vestige identifié par [Blot Colette]{.creator} en [septembre 2017]{.date}.

### [Sainte-Engrâce]{.spatial} - [Heguitchudia]{.coverage} - [Tertres d'habitat]{.type}

Localisation

Altitude : 1139m.

*TH2* : Altitude : 1109m.

Cet ensemble de 7 tertres est situé sur la ligne de croupes qui borde la rive gauche des gorges d'Holzarté.

Description

- *TH1* : Ce tertre - qui fait partie d'un groupe de 7 au total - est isolé, en hauteur dans le pâturage, à une cinquantaine de mètres de la piste qui passe en contre-bas au Nord-Ouest, et à 17m au Sud-Ouest des ruines du cayolar d'Héguitchudia. Tertre terreux asymétrique, érigé sur terrain en pente vers le Nord. Il mesure 8m de diamètre et 0,90m de haut environ.

- *TH2* : Il est situé, avec les autres, à une cinquantaine de mètres à l'Est et en contre-bas du précédent, sur un terrain très en pente vers le Nord-Est, à proximité de la lisière des bois et d'un petit sentier. Il mesure 12m x 8m et 0,80m de haut.

- *TH3* : Altitude : 1105m. Il est situé à 45m au Sud-Est du précédent, et mesure 12m x 8m et 0,50m de haut.

- *TH4* : Altitude : 1104m. Situé à 16m à l'Est de *TH3*, il est moins visible ; il mesure12m x 6m et 0,40m de haut.

- *TH5* et *TH6* : Altitude : 1107m. Ces deux tertres sont tangents, peu visibles, et à quelques mètres à l'Ouest Nord-Ouest de *TH4*. À eux deux ils mesurent au total 10m x 8m et 0,40m de haut, n'étant individualisés que par un discret rétrécissement.

- *TH7* : Altitude : 1101m. Situé à 10m au Nord de *TH5* et *TH6*, il mesure 10m x 6m et 0,60m de haut.

Historique

Les *TH1*, *TH2, *TH3*, *TH4*, ont été découverts en [septembre 2017] par [A. Martinez]{.creator}, les *TH5*, *TH6* et *TH7* par [Meyrat F.]{.creator} à la fin de ce même mois.

### [Sainte-Engrâce]{.spatial} - [Huchtia]{.coverage} - [Tertre d'habitat]{.type} ou [Tumulus]{.type}

Localisation

Altitude : 823m.

Il est à une quinzaine de mètres au Nord-Est d'un chemin pastoral.

Description

Tumulus terreux circulaire de 10m de diamètre, de 0,40m de haut, érigé sur un sol très légèrement en pente vers le Nord-Est.

La question peut se poser de savoir s'il s'agit d'un tertre d'habitat très érodé, ou d'un tumulus... La différence entre le diamètre et la hauteur et sa parfaite circularité nous inciterait à penser peut-être à un tumulus...

Historique

Monument découvert par [Blot J.]{.creator} en [novembre 2015]{.date}, ainsi que tous les autres monuments d'Huchtia.

### [Sainte-Engrâce]{.spatial} - [Huchtia]{.coverage} Sud haut - [Tertres d'habitat]{.type}

- Hucthia Sud haut

    - Localisation

        Altitude : 821m.

        Situé à une quarantaine de mètres au Nord-Ouest du monument précédent et tangent au Nord-Est du chemin pastoral.

    - Description

        Tertre terreux asymétrique érigé sur terrain en légère pente vers le Nord-Est, mesurant 5m de diamètre et 0,40m de haut. On en distingue semble-t-il un autre à 10m au Sud-Est.

- Huchtia Sud bas

    Situé à une trentaine de mètres au Nord-Ouest du précédent, il mesure 6m de diamètre et 0,40m de haut.

### [Sainte-Engrâce]{.spatial} - [Huchtia]{.coverage} Nord - [Tertres d'habitat]{.type}

- *Huchtia Nord haut*

    - Localisation

        Altitude : 819m.

        Il est tangent au Nord-Est du chemin pastoral.

    - Description

        Tertre terreux asymétrique mesurant 8m de diamètre et 0,40m de haut.

- *Huchtia Nord bas*

    Tertre situé à une vingtaine de mètres au Nord-Est du précédent, il en a sensiblement les mêmes dimensions.

### [Sainte-Engrâce]{.spatial} - [Huchtia]{.coverage} (groupe central) - [Tertres d'habitat]{.type} 

Localisation

Altitude : 815m.

Cet ensemble de 8 petits tertres se trouve à une soixantaine de mètres plus bas et à l'Est du terre Huchtia Nord haut. Leur finalité n'est pas évidente...

### [Sainte-Engrâce]{.spatial} - [Huchtia]{.coverage} Est 1 - [Tertre d'habitat]{.type}

Localisation

Altitude : 306m.

Situé à une quarantaine de mètres à l'Est du groupe précédent, ce tertre est bien visible (entre deux arbres) du fait de ses grandes dimensions (15m de diamètre et 0,80m de haut).

### [Sainte-Engrâce]{.spatial} - [Hustarte]{.coverage} - ([?]{.douteux})

Localisation

Altitude : 1462m.

Ce monument est situé, en terrain plat, sur le flanc Est d'un haut plateau qui s'étend au Nord du sommet dit Heylé Gagne.

Description

Dix-sept pierres, de la taille d'un ou deux gros pavés, délimitent un rectangle mesurant 3,20m de long et 2,60m de large, à grand axe orienté Nord-Ouest Sud-Est ; il s'agit très probablement des restes d'un habitat pastoral, d'époque indéterminée très semblable à celui que nous avons signalé à Erainze (commune de Sainte-Engrâce) [@blotInventaireMonumentsProtohistoriques2018].

Historique

Vestige découvert par [P. Velche]{.creator} en [septembre 2017]{.date}.

### [Sainte-Engrâce]{.spatial} - [Izeytochiloa]{.coverage} - [Tertres d'habitat]{.type}

Localisation

Altitude : 1183m.

Ces trois tertres se trouvent à une cinquantaine de mètres à l'Ouest Nord-Ouest du cayolar du même nom, et à une vingtaine de mètres à l'Ouest de la route, en bordure d'un petit riu qui naît à ce niveau.

Description

- Le premier tertre, terreux et asymétrique, est le plus près de la route et sous le départ d'une piste herbeuse ; il mesure 8m de diamètre et 2m de haut. 

- Le second tertre est à 8m au Nord-Ouest du premier. Il mesure 8m de diamètre et 1,50m de haut ; le troisième est à 3m à l'Est du précédent et mesure 8m de diamètre et 1,80m de haut.

Historique

Tertres trouvés par [Blot J.]{.creator} en [novembre 2015]{.date}.

### [Sainte-Engrâce]{.spatial} - [Izeytochiloa]{.coverage} - [Plate-forme]{.type} ([?]{.douteux})

Localisation

Altitude : 1193m.

Ce vestige se trouve à une soixantaine de mètres à l'Ouest des tertres précédents et de la route.

Description

Aménagement du terrain, (en partie creusé aux dépens de la pente voisine) en une vaste plate-forme de 25m de long selon sur son axe Nord-Sud et de 6m de large (axe Est-Ouest).

Finalité difficile à préciser : plate-forme pour fabrication du charbon de bois ? poste de tir pour artillerie ? pour habitat pastoral ?

Historique

Découvert par [Blot J.]{.creator} en [novembre 2015]{.date}.

### [Sainte-Engrâce]{.spatial} - [Négumendi]{.coverage} - [Tertres d'habitat]{.type}

Localisation

Altitude : 1280m.

Une fois laissée la piste qui mène au pluviomètre, aller vers l'extrémité Ouest du plateau de ce nom, (au moment où il amorce une descente).

Description

On peut voir en divers endroits des reliefs dont 4 au moins nous paraissent des vestiges de Tertres d'habitat, en particulier celui auprès d'un arbre : il mesure 5m de diamètre, 0,40m de haut et présente une légère excavation en son centre.

Historique

Tertres trouvés par [Blot J.]{.creator} en [octobre 2013]{.date}.

### [Sainte-Engrâce]{.spatial} - [Orkhatzolhatzé]{.coverage} - [Grand tertre]{.type}

Localisation

Altitude : 1270m.

Ce tertre borde à l'Est une piste pastorale qui court au flanc Est du mont Negumendi, dominant la fontaine d'Orkhatzolhatzé.

Description

Grand tertre de terre, de forme ovale, à grand axe Nord-Ouest Sud-Est, mesurant 25m de long, 8m de large et 2,50m de haut environ. La finalité de ce tertre (ou Tumulus ?) n'est pas évidente.

Historique

Tertre découvert par [Blot J.]{.creator} en [octobre 2014]{.date}.

### [Sainte-Engrâce]{.spatial} - [Orkhatzolhatzé]{.coverage} - [Tertres d'habitat]{.type}

Localisation

Altitude : 1260m.

Sur la piste (élargie) qui mène au plateau d'Orkhatzolhatze sous-jacent.

Description

Deux tertres circulaires très nets bordent la piste pastorale, espacés d'une dizaine de mètres, chacun mesurant 5m à 6m de diamètre et 0,45m de haut environ.

Historique

Tertres découverts par [Blot J.]{.creator} en [octobre 2013]{.date}.

### [Sainte-Engrâce]{.spatial} - [Pénalargue]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 1473m.

Il est situé au sommet de la colline de ce nom.

Description

Nous avions publié succinctement ce tumulus en 1973 [@blotNouveauxVestigesMegalithiques1972a]. Depuis il a subi des dégradations importantes (fouilles clandestines probables). Il se présentait sous la forme d'un tumulus terreux circulaire en forme de galette aplatie, mesurant 13m de diamètre et 0,80m de haut, avec quelques rares pierres visibles en surface. Il a été, depuis, profondément remanié, et l'on peut distinguer deux traces d'excavation, l'une au Sud-Est, l'autre au Nord-Ouest, toutes deux atteignant le centre du monument.

Historique

Monument découvert par [Blot J.]{.creator} en [1971]{.date} et revisité en [juillet 2016]{.date}.

### [Sainte-Engrâce]{.spatial} - [Taillade (Col de la)]{.coverage} - [Tertres]{.type} ou [Tumuli]{.type} ([?]{.douteux})

Localisation

Altitude : 1393m.

On peut noter sur le plateau situé au Sud-Ouest de la route, en bordure de celle-ci, une dizaine de petites éminences de terre faites de main d'homme, *sur terrain plat*, échelonnées selon un axe Sud-Ouest Nord-Est ; elles mesurent environ 6m de diamètre, 0,30m à 0,40m de haut, et sont espacées de 2 à 3m en moyenne. Une autre est visible à quelques mètres à l'Ouest d'une cabane de construction récente au Sud de l'alignement ci-dessus décrit. Il est difficile de les dénommer tertres ou tumulus, au sens que nous donnons habituellement à ces termes. Elles sont très semblables à ce que nous avons signalé à Coutchet-de-Planté (Lanne).

On en retrouve aussi un grand nombre, côté Nord-Est de la route, *sur terrain en pente*, et à plusieurs niveaux. Banquettes de tir ?

Historique

Monuments découverts par [Blot J.]{.creator} en [juillet 2016]{.date}.

### [Sainte-Engrâce]{.spatial} - [Sohotholhatzé]{.coverage} (groupe du bas) - [Tertres d'habitat]{.type} 

Localisation

En contre-bas de la route qui rejoint le cayolar de Sohotolhatzé à celui d'Anhaou.

Altitude : 1269m.

Description

Un groupe de 4 Tertres d'habitat répartis par 2 groupes de 2 tertres sur les bords des petits rius qui sillonnent la pente à cet endroit. Dimensions moyennes : 4m à 5m de diamètre et 0,60m de haut.

Historique

Tertres découverts par [Blot J.]{.creator} en [octobre 2013]{.date}.

### [Sainte-Engrâce]{.spatial} - [Soum de Léche]{.coverage} - [Dolmen]{.type} ([?]{.douteux})

Localisation

Altitude : 1680m.

On y accède en suivant le GR 10 qui part du col de la Pierre Saint-Martin pour descendre à Sainte-Engrâce. On le suit sur environ 1800m, et à environ 1800m, sur la gauche et on voit la structure à une cinquantaine de mètres en contre-bas de la piste.

Description

Quatre dalles de [grés]{.subject} local, friable, plus ou moins dressées et une autre au sol pourraient délimiter une [chambre funéraire]{.subject} de plus de 3m de long et 0,70m de large, orientée Est-Ouest, le tout sur un terrain en forte pente vers le Nord...

La paroi Sud de cette chambre serait formée par 2 dalles : une dalle principale la plus à l'Ouest, inclinée vers le Nord, mesurant 2,05m à sa base et 1,58m de haut avec une épaisseur moyenne de 0,36m. Elle est séparée de 0,36m de la seconde dalle de cette paroi Sud, qui elle, est inclinée vers le Sud et mesure 1,36 m à sa base, 1,10m de haut et 0,21m d'épaisseur en moyenne. La paroi Nord est matérialisée par une seule dalle, inclinée vers le Nord, mesurant 1,10m à la base, 0,75m de haut et 0,36m d'épaisseur environ ; toutefois, une deuxième dalle, qui gît à environ 2m au Nord, pourrait représenter le montant manquant, normalement situé exactement à l'Est de la précédente, et qui aurait glissé après désinsertion. Elle mesure 1,44m dans son plus grand axe Est-Ouest, 1,37m dans l'axe Nord-Sud, et son épaisseur est de 0,25m en moyenne.

Plus au Nord, dans la pente, à 32m de ces éléments, gît une dalle, mesurant 2,20m dans son grand axe Sud-0uest Nord-Est, 1,36m dans l'axe Nord-Ouest Sud-Est et 0,35m d'épaisseur. Cet élément pourrait, dans l'optique d'un dolmen, être considéré comme la dalle de couverture ayant glissé jusque-là. Toutefois, compte tenu de la très forte pente du terrain, du contexte local où se voient d'autres dalles mises à jour par l'érosion, nous considérons cette hypothèse « dolménique » comme assez peu probable...

Historique

Structure découverte par [P. Velche]{.creator} en [Octobre 2016]{.date}.
