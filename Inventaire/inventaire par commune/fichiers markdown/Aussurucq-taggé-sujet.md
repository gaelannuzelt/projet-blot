# Aussurucq

### [Aussurucq]{.spatial} - [Elsarreko Ordoki]{.coverage} - [Tertres]{.type} et [tumulus-cromlech]{.type}

Le haut-plateau d'Elsarreko Ordoki, (polgé) qui s'étend à l'Est du pic Zabozé, recèle de nombreux vestiges dont 18 ont été identifiés au cours des temps par [Blot J.]{.creator}, [Ebrard D.]{.creator}, et [D. Sarramagnan]{.creator}. On a ainsi pu distinguer : un tumulus-cromlech, 11 tertres pierreux, une dalle « mégalithique » (n°8) déplacée et ensevelie en 1995, un gîte à petits rognons de silex,( n°9) deux meules de charbonnier, (n°10 et 11), un cayolar en ruine, (n°15). Nous citerons et localiserons ici tertres et tumulus-cromlech.

- *Tertre pierreux 1*
    
    Altitude : 735m.

    Mitoyen du cayolar Pagolen Olha. Diamètre approximatif : 9m x 9m, 1m de haut. 
    
    (inventeur : D. Ebrard, [1972]{.date}).

- *Tertre pierreux 2*

    Altitude : 735m.

    Tertre pierreux, mesurant 10m de diamètre, 0,90m de haut, avec dépression centrale de 0,80m de profondeur.

    (inventeur : Blot J., [1979]{.date})

- *Tumulus-cromlech 3*

    Altitude: 730m.

    Il mesure environ 6m de diamètre et 0,30m de haut. Un péristalithe d'environ 6 pierres est visible à la périphérie du tumulus.

    (inventeur : Blot J., 1979)

- *Tertre pierreux 4*

    Altitude : 740m. A été remanié superficiellement en 1995 et fouillé par D. Ebrard en 1996.

    Mesure 6m de diamètre et 0,20m de haut.

    (Inventeur : Blot J., 1979)

- *Tertre pierreux 5*

    Altitude : 740m.

    Remanié par les engins en 1995 ; mesure 9m de diamètre et 0,80m de haut. TH ?

    (Inventeur : D. Ebrard, [1993]{.date}).

- *Tertre pierreux 6*

    Tangent au précédent au Sud. Remanié par engins en 1993. Mêmes dimensions que le précédent.

    (inventeur : D. Ebrard 1993).

- *Tertre (?) n°7*

    Altitude : 730m.

    (inventeurs : D. Ebrard et Sarramagnan, [1996]{.date})

- *Tertre pierreux 12*

    Altitude : 740m.

    Remanié en 1995 - mesure 6,50m de diamètre et 0,20m de haut. Fouilllé en 1996.

    (inventeurs : D. Ebrard et Sarramagnan, 1996)

- *Tertre pierreux 14*

    Altitude : 740m.

    Arasé en 1995.

    (Inventeurs : D. Ebrard et Sarramagnan, 1996).

- *Tertre 16*

    Altitude : 750m.

    Arasé en 1995.

    (inventeurs : D. Ebrard et Sarramagnan, 1996).

- *Tertre 17 *

    Altitude : 740m.

    Défriché en 1995. Mesure 6m de diamètre et 0,20m de haut.

    (inventeurs : D. Ebrard et Sarramagnan, 1996).

- *Tertre 18 *

    Altitude : 750m.
    
    Tout en bordure de la piste routière ; Mesure 8m de diamètre et 1 m de haut.

    Défriché en 1995.

    (Inventeur : Blot J., 1979).

Historique

On trouvera les détails dans [@ebrard50AnsArcheologie2013, p.285] de la bibliographie des monuments.

### [Aussurucq]{.spatial} - [Ilhasteria]{.coverage} 2 - [Tumulus]{.type}

Localisation

Carte 1446 Ouest Ordiarp.

Altitude : 1140m.

Il est situé à 200m au Nord-Est du sommet de la colline Ilhastéria (au Nord-Est de la fontaine d'Ahuski), et à 400m au Sud-Est du cayolar Nabolégui, en bordure de crête ; une vue magnifique sur la chaîne de montagnes s'étend devant lui, à l'Est.

Nous avons décrit le *n°1* en 1979 [@blotSouleSesVestiges1979, p.7].

Ce tumulus n°2 est situé à 1m à l'Ouest Sud-Ouest du *n°1*.

Description

Tumulus mixte, de terre et de pierres [calcaires]{.subject} blanches, mesurant 5m de diamètre et 0,40m de haut, et bien que sa périphérie en soit un peu irrégulière, il évoque parfaitement, en plus petit, la facture de son grand voisin.

Historique

Tumulus découvert en [octobre 1971]{.date}. Nous avons demandé au professeur Jean Haristchelhar la signification du mot *Ilhasteria* ; sans que ce terme puisse être traduit mot pour mot, il nous a répondu que cela évoquait « l'idée de mort rapide ». Cela correspondrait fort bien à ce site connu pour la fréquence avec laquelle frappe la foudre ; par ailleurs, la difficulté à identifier ces monuments faits d'amoncellement de pierraille dans un contexte lui-même riche en roches, nous conforte dans l'idée que nous avions déjà suggérée pour les tumulus *Ilhareko-lepoa 1* et *2*, à savoir que la dénomination du lieu a été donnée à l'époque de la construction de ces monuments funéraires, et non depuis.

### [Aussurucq]{.spatial} - [Ilhasteria]{.coverage} 3 - [Tumulus]{.type}

Localisation

Altitude : 1140m.

Il est à 4m à l'Ouest du *tumulus n°1* déjà décrit par nous [@blotSouleSesVestiges1979, p.7].

Description

Petit tumulus circulaire en forme de galette, de 4m de diamètre et 0,40m de haut, formé de terre et de nombreuses pierres locales ; il semble qu'on puisse distinguer un péristalithe.

Historique

Ce tumulus a été aperçu en [1971]{.date} par [Blot J.]{.creator} et confirmé ici.

### [Aussurucq]{.spatial} - [Ilhasteria]{.coverage} - [Cromlech]{.type}

Description

Très petit cercle de pierres de 0,90m de diamètre délimité par une douzaine d'éléments au ras du sol ; à l'intérieur, 6 autres éléments sont visibles.

Historique

Monument découvert par [Blot J.]{.creator} en [mai 2013]{.date}.

### [Aussurucq]{.spatial} - [Ithé]{.coverage} 6 - [Tumulus]{.type}

Localisation

Altitude : 700m.

Il est situé à 60m environ à l'Ouest de la route Aussurucq - Mendive.

Description

Tumulus pierreux, formé de petits et moyens blocs de [calcaire]{.subject} blanc, mesurant environ 10m de diamètre et 0,50m de haut.

Historique

Monument découvert par [Blot J.]{.creator} en [avril 2013]{.date}.

### [Aussurucq]{.spatial} - [Léchareguibela]{.coverage} - [Tertre d'habitat]{.type}

Localisation

Altitude : 906m.

Il est au bout du chemin aménagé pour atteindre le cayolar de ce nom, et à 10m au Nord-Est de ce dernier.

Description

Tertre de terre et de pierraille de forme ovale à grand axe Sud-Est Nord-Ouest, mesurant 9m x 6m, et présentant une dépression centrale. Erigé sur terrain en pente vers le Nord-Ouest.

Historique

Tertre découvert par [Blot J.]{.creator} en [avril 2013]{.date}.

### [Aussurucq]{.spatial} - [Ohaté]{.coverage} 2 - [Tertre d'habitat]{.type}

Localisation

Altitude : 869m.

Il est à une vingtaine de mètres en contre-bas des ruines du cayolar Ohaté, lequel est tangent au *TH n°1* (et à un second, évoqué, mais peu probable) que nous avons déjà publié en 1979 [@blotSouleSesVestiges1979 p. 10]. En bordure de piste.

Description

Tertre de terre et de pierrailles, dissymétrique en hauteur (maximum 1m de haut) et mesurant 6,50m de diamètre environ.

Historique

Tertre découvert par [Blot J.]{.creator} en [avril 2013]{.date}.

### [Aussurucq]{.spatial} - [Olhatzezarre]{.coverage} (groupe Ouest) - [Tertres d'habitat]{.type} 

Localisation

Altitude : 1000m.

Description

- *TH 1* : Au fond d'une grande doline on note un grand tertre de terre circulaire, (le plus au Sud). D'une dizaine de mètres de diamètre, avec dépression centrale ouverte vers l'Ouest.

- Le *TH 2* lui est tangent au Nord, de moindre dimension, et sans dépression centrale.

- Le *TH 3* est plus au Nord, sur les bords de la doline, et de moindre dimensions.

Historique

Monuments vus par [Blot J.]{.creator} en [1971]{.date}, et succinctement décrits [@blotSouleSesVestiges1979 p.7].

### [Aussurucq]{.spatial} - [Olhatzezarre]{.coverage} (groupe Est) - [Tertres d'habitat]{.type} 

Localisation

Altitude : 1000m.

Les 7 tertres d'habitat sont à une cinquantaine de mètres à l'Est du groupe précédent.

Description

7 TH sont répartis sur une pente douce inclinée vers l'Est. Les dimensions en sont variables, mais atteignent souvent 7m à 9m de diamètre et 0,80m de haut. Le *TH 2* est à 15m au Sud du *TH 1* (qui est le plus au Nord). Le *TH 3* est à 7m au Sud du n°2. Le *TH 4* est à 6m à l'Ouest du n°3. Le *TH 5* est à 6m au Sud-Est du n° 4. Le *TH 6* est à 20m au Sud du n°5. Le *TH 7* est à 4m à l'Est du n° 6.

On notera enfin un dernier *TH (Sud) effondré* dans une petite doline, à l'extrémité Sud du plateau, à l'amorce de la piste qui redescend vers le cayolar Olhatzezarre. (Blot J. avril 2013).

Historique

TH vus par [Blot J.]{.creator} en [1971]{.date}, et succinctement décrits [@blotSouleSesVestiges1979 p.7].
