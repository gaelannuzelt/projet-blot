# Aldude

### [Aldude]{.spatial} - [Arguibel]{.coverage} (ou Harguibel) - [Dolmen]{.type}

Situation

Carte 1346 Ouest Saint-Etienne-de-Baïgorry.

Altitude : 952m.

Il est situé au Sud du mont Arguibel et à 12m au Sud-Est de la BF 126.

Description

Il n'y a pas de tumulus visible. On note, émergeant du sol une dalle horizontale, disposée à plat, grossièrement rectangulaire, orientée Nord-Ouest Sud-Est, mesurant 1,70m de long et 0,90m de large, qui pourrait être la table (ou couvercle). Au Nord, une deuxième dalle est visible, (dalle de « chevet » ?) qui n'est pas perpendiculaire à la première, mais orientée Est-Ouest ; elle mesure 1m de long, 0,30m de haut et 0,40m d'épaisseur. L'ensemble évoque un coffre dolménique peut-être encore vierge.

Historique

Dolmen découvert en [août 1976]{.date}.

### [Aldude]{.spatial} - [Arguibel]{.coverage} - [Tertre cayolar]{.type} ([?]{.douteux})

Enfin, à mi-pente, sur le côté Sud du groupe de cromlechs, on note un relief de terre, sur terrain incliné, avec une structure en grosses pierres au centre, évoquant un [tertre d'habitat]{.type} sur lequel aurait été ensuite érigé un abri (cayolar ?) très primitif.

### [Aldude]{.spatial} - [Arguibel]{.coverage} 6 - [Tertre d'habitat]{.type}

Localisation

Carte 1346 Ouest Saint-Etienne-de-Baïgorry.

Altitude : 930m.

Il est situé dans le col, à 30m à l'Est du monolithe d'Arguibel et domine une pente abrupte vers l'Est.

Description

Tumulus terreux de 9m de diamètre et 0,80m de haut, présentant une dépression centrale, résultat probable d'une fouille ancienne. Un éboulement de la pente a amputé le secteur Est de ce monument.

Historique

Monument découvert en [août 1976]{.date}.

### [Aldude]{.spatial} - [Belaun]{.coverage} Ouest n°1 - [Tumulus-cromlech]{.type} ([?]{.douteux})

Localisation

Altitude : 810m.

Sur un replat au flanc du mont Eyharce, dominant le col de Belaun.

Description

On note un léger relief circulaire, de 5,50m de diamètre et de 0,30m de haut, délimité par 8 pierres de calibre variés ; deux d'entre elles en secteur Nord, semblent avoir été arrachées à l'extérieur du cercle, et mesurent respectivement 0,50m et 0,70m dans leur plus grand axe ; monument douteux.

Historique

Monument découvert par [F. Meyrat]{.creator} en [avril 2010]{.date}.

### [Aldude]{.spatial} - [Belaun]{.coverage} Ouest n°2 - [Tumulus-cromlech]{.type} ([?]{.douteux})

Localisation

À 25 mètres à l'Est de *Belaun ouest n°1 - Tumulus-cromlech (?)*.

Description

Structure ovalaire de 5,70m de long, suivant un axe Est-Ouest, et 3,70m de large, mesurant 0,30m à 0,40m de haut, constituée de pierres de calibre variés, surtout visibles dans la périphérie Sud. Monument douteux.

Historique

Monument découvert par [J. Blot]{.creator} en [avril 2010]{.date}.

### [Aldude]{.spatial} - [Belaun]{.coverage} Est - [Tumulus]{.type} ([?]{.douteux})

Localisation

Altitude : 760m.

Description

Tumulus de 3,60m de diamètre, et 0,40m de haut environ, constitué de petits blocs calcaires mobiles. Monument douteux.

Historique 

Monument découvert par [F. Meyrat]{.creator} en [avril 2010]{.date}.

### [Aldude]{.spatial} - [Berdaritz]{.coverage} - [Monolithe]{.type}

Localisation

Carte 1346 Ouest St-Etienne-de-Baïgorri.

Altitude : 690m.

Ce monolithe est situé au niveau des premiers mètres du flanc Nord-Ouest du mont Urrixka, couché sur un sol en légère pente ; il est à 60m au Nord Nord-Est de la BF 117. Comme le monolithe d'Arguibel, auquel il ressemble beaucoup [@blotMonolithesPaysBasque1983 p.23], il est à proximité (à 50m au Nord) d'une source. Il lui ressemble encore non seulement par sa position dans un col, mais aussi par son riche environnement archéologique. Enfin il est important de souligner qu'il n'y a, dans un vaste rayon de plusieurs dizaines de mètres, aucune pierre, aucun bloc rocheux d'éboulis sur cette pente. Cette grande dalle est d'autant plus remarquable dans sa solitude...

Description

Il s'agit d'une grande dalle polygonale de [grés]{.subject} [triasique]{.subject}, à sommet triangulaire, mesurant près de 5m dans son grand axe Sud-Est Nord-Ouest ; son sommet a été détaché, très probablement par l'action du gel. L'épaisseur moyenne est de 0,20m à 0,30m. L'extrémité Nord-Ouest est beaucoup plus mince que le reste de la dalle, et on remarque des traces d'épannelage (flêches), sur le bord arrondi, au Nord-Est, et sur le bord Sud-Est du sommet.

Enfin il existe un bloc rocheux massif enfoui dans le sol à proximité du sommet de ce monolithe, qui paraît « en place » et ne présente, semble-t-il, aucun rapport avec lui ; il est en particulier beaucoup plus épais.

Historique

Monument découvert en [janvier 2004]{.date}.

### [Aldude]{.spatial} - [Berdaritz]{.coverage} n°1 - [Cromlech]{.type} ([?]{.douteux})

Localisation

Altitude : 735m.

Il est à environ 80m à l'Est Nord-Est de la BF 118.

Description

On distingue un cercle d'environ 5,50m de diamètre, érigé sur un terrain en légère pente vers le Nord-Est. Il est matérialisé par un bourrelet de terrain contenant de nombreuses pierres, ceci étant particulièrement bien visible dans les secteurs Nord-Est et Sud. Le centre est le siège d'une dépression peu profonde. Monument douteux.

Historique

Monument trouvé en [2002]{.date} par [J. Capbodevilla]{.creator} et [I. Zabala]{.creator}.

### [Aldude]{.spatial} - [Berdaritz]{.coverage} n°2 - [Cromlech]{.type} ([?]{.douteux})

Localisation

À 30 mètres à l'Ouest Sud-Ouest, du précédent.

Description

Une dizaine de pierres au ras du sol délimitent un cercle de 2,60m de diamètre contenant une pierre centrale. Monument douteux.

Historique 

Monument trouvé par [J. Blot]{.creator} en [avril 2010]{.date}.

### [Aldude]{.spatial} - [Berdaritz]{.coverage} n°3 - [Cromlech]{.type} ([?]{.douteux})

Localisation

Altitude : 732m.

Situé à 60m au Sud de la BF 118 et à 200m environ à l'Ouest du *Cromlech C2* [@blotInventaireMonumentsProtohistoriques2011 p. 3-4].

Description

Sur l'étendue plate et sans pierres apparentes de la lande environnante se détachent 5 pierres disposées en cercle sur un bourrelet de terrain circulaire de 4m de diamètre, entourant une légère dépression dans laquelle 3 autres blocs pierreux apparaissent.

Monument douteux, (T ?), comme les 2 autres cromlechs déjà décrits [@blotInventaireMonumentsProtohistoriques2011].

Historique

Monument découvert par [F. Meyrat]{.creator} en [mars 2014]{.date}.

### [Aldude]{.spatial} - [Berdaritz]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 685m.

Il est situé à 56 mètre à l'Ouest de la BF 117.

Description

Ce monument, probable, dont il ne reste que la moitié, a en effet été amputé par la passage des engins agricoles utilisés pour mettre en valeur la prairie à au Nord de la clôture de barbelés qui passe au milieu de ce monument. Tumulus de terre semble-t-il, de 6m de diamètre et 0,45m de haut.

Historique

Monument découvert par [J. Blot]{.creator} en [avril 2010]{.date}.

### [Aldude]{.spatial} - [Eyharzeko lepoa]{.coverage} - [Dalle plantée]{.type} ([?]{.douteux})

Localisation

Située à 20 mètres au Sud de la BF 125.

Description

Dalle verticale de [grés]{.subject} rose, orientée Sud Sud-Est, de 1m de long, 0,46m de haut et 0,23m d'épaisseur. Son bord supérieur présente des traces très évidentes d'épannelage. Pas de traces de tumulus ; s'agit-t-il d'un vestige dolmenique ?

Historique

Dalle découverte par [J. Blot]{.creator} en [avril 2010]{.date}.

### [Aldude]{.spatial} - [Eyharcé]{.coverage} (col de) - [Dalle]{.type} ou [Dolmen]{.type} ([?]{.douteux})

Localisation

Altitude : 850m.

Elle est située à 25m au Sud de la BF 125 et à 30m au Nord de la terminaison de la route sur le col.

Description 

Dalle verticale de grés rose, orientée Sud Sud-Est, de 1m de long, 0,46m de haut et 0,23m d'épaisseur. La totalité de son bord supérieur présente des traces très évidentes d'épannelage. On ne note pas la présence d'autres dalles mais il semble qu'on puisse distinguer un léger relief tumulaire, circulaire, de 8m de diamètre environ et 0,15 à 0,20m de haut, au centre duquel apparaît la dalle. Nous tenons pour très probable que cet ensemble soit un vestige de coffre dolménique assez semblable au monument *Eyharcé (col de) - Ciste.

Historique 

Dalle revue en mai 2018 mais déjà repérée par nous ([J. Blot]{.creator}), en [avril 2010]{.date} et publiée en 2011 sous la rubrique « cas particulier » : [@blotInventaireMonumentsProtohistoriques2011 p. 5].

### [Aldude]{.spatial} - [Eyharcé]{.coverage} (col de) - [Ciste]{.type}

Localisation

Altitude : 851m.

Monument situé à 50m à l'Ouest Sud-Ouest de la BF 125 ; il est, semble-t-il, tangent à la ligne frontière.

Description 

Monument très peu visible, au ras du sol. On distingue tout d'abord 2 dalles parallèles au ras du sol, délimitant la chambre funéraire orientée pratiquement plein Est. La dalle Nord mesure 0,85m de long et entre 0,05m et 0,10m d'épaisseur ; la dalle Sud mesure 1,04m de long, et entre 0,2m et 0,5m d'épaisseur. Ces deux dalles paraissent enfoncées bien verticalement dans le sol ; il n'y en a pas d'autres visibles à l'Est ou à l'Ouest, ni de dalle de couverture.

On note quelques rares pierres (7 au total), elles aussi au ras du sol, en forme de galets, situées à proximité de la chambre funéraire et qui pourraient faire partie d'un tumulus mixte de terre et de pierres, ou d'un éventuel péristalithe. On notera particulièrement 2 pierres au Sud, distantes de 2,20m environ de la [chambre funéraire]{.subject}, une autre à l'Est, distante de 2,23m de la [chambre funéraire]{.subject} et une dernière à l'Ouest, distante de 2,15m ; ce qui pourrait faire envisager (dans le cas d'un vestige de péristalithe), un tumulus de 5m de diamètre environ, dimensions parfaitement compatible avec ce type de monument.

Historique 

Monument découvert par [J. Blot]{.creator} en [mai 2018]{.date}.

### [Aldude]{.spatial} - [Eyharcé]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 777m.

Il est situé à une dizaine de mètres à l'Est Nord-Est (à droite) de la route qui monte vers le col d'Eyharcé, et bien visible de celle-ci ; on est à 400m avant le dernier virage en épingle à cheveux qui la mène ensuite en direct au col d'Eyharcé. De l'autre côté de la route, monte, sur la gauche, une piste pastorale qui se rend directement au col d'Eyharcé.

Description

Tumulus terreux uniquement, en forme de galette aplatie, mesurant 6,20m de diamètre et 0,30m de haut ; on note au centre une légère dépression. Sa périphérie est aussi entourée d'une légère dépression, sans doute en rapport avec l'extraction des terres nécessaires à la confection de ce tumulus.

Historique

Tumulus découvert par [F. Meyrat]{.creator} en [juillet 2018]{.date}.

## [Aldude]{.spatial} - Les [cromlechs]{.type} d'[Arguibel]{.coverage}

Localisation

Carte 1346 Ouest Saint-Etienne-de-Baïgorri.

Altitude : 960m.

J.M. de Barandiaran [@barandiaranProspecionesExcavacionesPrehistoricas1962, p.13; @barandiaranHombrePrehistoricoPais1953, p.249] avait décrit sur une petite éminence à environ 130m au Nord-Ouest de la BF 126 et dominant au Nord-Ouest la ligne frontière deux cromlechs (communes d'Aldude et Baztan). Un premier cercle de 6m de diamètre, constitué de 11 pierres bien visibles (la plus haute mesure 0,90m de haut), et un second, à l'Est du précédent, de 2,20m de diamètre, entouré de 8 pierres. N'ayant pas retrouvé sur le terrain ces données, nous proposons la description suivante.

### [Aldude]{.spatial} - [Arguibel]{.coverage} 1 - [Cromlech]{.type}

Le plus à l'Est, mesure 6m de diamètre, est entouré de 15 pierres ; il pourrait correspondre au *n°1* de [J.M. de Barandiaran]{.creator}.

### [Aldude]{.spatial} - [Arguibel]{.coverage} 2 - [Cromlech]{.type}

Est tangent au Nord-Ouest du précédent.

Quatorze pierres délimitent un cercle de 6m de diamètre.

Historique

Monument découvert en [octobre 1972]{.date}.

### [Aldude]{.spatial} - [Arguibel]{.coverage} 3 - [Cromlech]{.type}

Existe à l'intérieur du n°2, tangent à lui en secteur Nord-Ouest. Il mesure 2m de diamètre, et est entouré de 10 pierres environ. Cette présence, très nette ici, d'un second cercle à l'intérieur d'un premier, est tout à fait exceptionnelle, pour ne pas dire unique, à notre connaissance.

Historique

Monument découvert en [août 1976]{.date}.

### [Aldude]{.spatial} - [Arguibel]{.coverage} 4 - [Cromlech]{.type}

Il est tangent au Nord-Ouest du n°2.

Une quinzaine de pierres délimitent un cercle de 2,50m de diamètre. Il est érigé sur un terrain en légère pente vers l'Ouest.

Historique

Monument découvert en [octobre 1972]{.date}.

### [Aldude]{.spatial} - [Arguibel]{.coverage} 5 - [Cromlech]{.type}

Monument modeste et fort dégradé, visible au Nord du n°1, et tangent à lui. Il mesure 3m de diamètre, et il serait délimité par 4 à 5 pierres.

### [Aldude]{.spatial} - [Arguibel]{.coverage} 6 - [Cromlech]{.type}

Localisation

Au flanc Nord de l'ensemble d'Arguibel, sur terrain incliné.

Altitude : 960m.

Description

Une douzaine (une quinzaine ?) de pierres de faibles dimensions paraissent délimiter une stucture circulaire de 3m de diamètre environ.

Historique

Monument découvert par [Blot J.]{.creator} en [mai 2013]{.date}.

### [Aldude]{.spatial} - [Arguibel]{.coverage} 7 - [Cromlech]{.type}

Localisation

Au flanc Nord Nord-Ouest de l'ensemble d'Arguibel, sur terrain incliné. Mêmes coordonnées que le groupe.

Description

7 à 8 pierres délimiteraient une structure circulaire de 2m environ de diamètre.

Historique

Monument découvert par [Blot J.]{.creator} en [mai 2013]{.date}.

### [Aldude]{.spatial} - [Arguibel]{.coverage} 8 - [Cromlech]{.type}

Localisation

Au flanc Nord du groupe, entre C6 et C7, sur terrain incliné.

Description

Une dizaine de pierres semblent délimiter une structure circulaire de 3m de diamètre environ.

Historique 

Monument découvert par [Blot J.]{.creator} en [mai 2013]{.date}.

### [Aldude]{.spatial} - [Arguibel]{.coverage} 9 - [Cromlech]{.type}

Localisation 

Altitude : 930m.

Il est situé à 20m au Sud Sud-Ouest de la BF 126 ; la ligne frontière est matérialisée par une barrière barbelée qui partage le monument selon un axe orienté sensiblement Nord-Sud, de telle sorte que les ¾ de ce dernier sont en territoire du Baztan, et le reste dans les Aldudes.

Description

On peut en estimer le diamètre à 8m environ, bien que le seul quart Nord-Ouest du monument soit visible, matérialisé par un arc de cercle de 5 pierres ; l'une d'elles est un véritable petit monolithe couché au sol, orienté plein Est, de 2,20m de long et 1,15m dans sa partie la plus large, et 0,20m d'épaisseur visible. Les 3 pierres au centre du monument font-elles partie d'une ciste ?

Historique

Monuments découverts en [août 1976]{.date}.

### [Aldude]{.spatial} - Les monuments de [Zaho]{.coverage}

Un ensemble de 4 monuments et un [monolithe]{.type}, localisés de part et d'autres de la frontière a été décrit par [J.M. de Barandiaran]{.creator} [@barandiaranCronicaPrehistoriaBaigorri1949 p.74]. Nous en donnons ici, très brièvement, les principales caractéristiques qui diffèrent cependant sensiblement de la description princeps, y ajoutant 4 nouveaux monuments.

- *Zaho 1 - [Tumulus-cromlech]{.type}*

    Altitude : 997m.

    Mesure 10m de diamètre et possède une trentaine de pierres périphériques ; il est à 20m au Nord-Ouest de la ligne frontière matérialisée par des barbelés, en territoire du Baztan.

- *Zaho 2 - Tumulus-cromlech*

    Il est situé à 20m à l'Est Sud-Est du premier, dans le territoire des Aldudes, et mesure 9m de diamètre avec 26 pierres périphériques visibles avant la fouille de sauvetage que nous avons effectué en août 1983 [@blotTumuluscromlechZahoII1989 p.49] ; la datation obtenue est la suivante : (Gif 6343) : mesure d'âge (BP) : 2640+-90, soit en date calibrée : 995-497 (BC).

- *Zaho 3 - Tumulus-cromlech*

    Il est tangent au Sud du précédent, et mesure 13m de diamètre et possède une trentaine de pierres périphériques. Il est à environ 120m au Nord-Est de la BF 128.

- *Zaho 4 - [Tumulus]{.type}*

    (Appelé dolmen par J.M. de Barandiaran), est situé à 100m au Sud du n°3 et à 20m au Sud-Est du monolithe ; il mesure 16m de diamètre et présente une importante excavation centrale.

Le monolithe de [grés]{.subject} rose, couché au sol, à grand axe orienté Sud-Ouest Nord-Est, mesure 4,90m de long, 1,30m de large et 0,50m d'épaisseur en moyenne.

Nous ajoutons aux monuments ci-dessus un groupe de **4 nouveaux**, découverts en [août 1974]{.date} ; nous décrivons en premier le :

- *Zaho 5 - Tumulus*

    - Localisation

        Altitude : 1000m.

        Le n°5 est tangent à l'Ouest du n°4.

    - Description

        Tumulus mixte de 6m de diamètre et 0,30m de haut; on distingue bien 7 pierres à la périphérie - mais sans que l'on puisse parler de péristalithe - ainsi que 3 pierres centrales.

- *Zaho 6 - Tumulus*

    - Localisation

        Il est tangent à l'Ouest du n°5, et à 12m au Sud-Est du monolithe de Zaho.

    - Description

        Tumulus de terre, de 5m de diamètre et 0,30m de haut ; on voit une pierre en secteur Nord-Ouest de sa périphérie.

- *Zaho 7 - Tumulus*

    Aldudes (Baztan).

    - Localisation

        Il est situé à 12m au Nord-Ouest de la BF 128.

    - Description

        Tumulus de terre de 7m de diamètre et 0,30m de haut, avec une pierre périphérique visible en secteur Nord-Ouest.

- *Zaho 8 - Tumulus-cromlech*

    - Localisation

        Sur un petit replat qui jalonne la descente vers le Sud-Ouest en venant du monolithe. Ce monument est à 150m de la BF. 128 et à 20m à l'Est de la ligne frontière.

    - Description

        Tumulus de 4m de diamètre et 0,40m de haut, délimité par 8 pierres, au ras du sol ; on distingue une très légère dépression centrale.


### [Aldude]{.spatial} - [Urrixka]{.coverage} 1 bis - [Dolmen]{.type} ([?]{.douteux})

Localisation

À 80 mètres environ à l'Est de *Urrixka 2 - Dolmen*, plus en hauteur sur le flanc de la montagne.

Description

On peut voir un amas de pierraille circulaire, mais relativement peu fourni, d'environ 10m de diamètre pouvant évoquer un tumulus pierreux dolménique assez semblable à ceux des monuments de cette montagne ; au centre, deux dalles importantes, horizontales et en parties enfouies dans le sol, renforcent cette similitude. Toutefois le terrain en pente et la présence en amont d'un filon rocheux naturel délité posent des problèmes...

Historique

Monument découvert par [Blot. J.]{.creator} en [juin 2010]{.date}. (Serait inscrit au Patrimonio de Navarra, en 2007, selon L. Millan)

### [Aldude]{.spatial} - [Urrixka]{.coverage} 2 - [Dolmen]{.type}

Localisation

Carte 1346 Ouest Saint-Etienne-de-Baïgorri.

Altitude : 870m.

Il est situé au flanc Sud-Est du mont Urrixka ; rappelons que J.M. de Barandiaran avait décrit le dolmen n°1 au flanc Sud-Ouest de ce mont, un peu au-dessus du col de Berdaritz, à 680m d'altitude, et à quelques mètres de ce n°1, un peu plus en altitude et à l'Est, le dolmen dit : *Jeneralen tomba*.

*Urrixka 2* est tout à fait en bordure et à droite du chemin qui gravit cette colline, sur un replat très faiblement incliné vers le Sud-Est.

Description

Tumulus pierreux de 8m à 9m de diamètre et 0,40m de haut, constitué de gros blocs amoncelés à sec. Au centre une dépression signale la chambre funéraire qui devait être, à l'origine, quadrangulaire, allongée, ouverte à l'Est Sud-Est. Il n'en reste qu'un montant de 1,65m de long et 0,45m de haut, incliné légèrement vers le Sud ; sur ce support s'appuient deux fragments de la table du [dolmen]{.subject}, brisée en 3 parties, dont une repose librement sur le côté Ouest du tumulus.

Historique

Dolmen découvert en [octobre 1986]{.date}.

### [Aldude]{.spatial} - [Urrixka]{.coverage} 4 - [Dolmen]{.type}

Localisation

Altitude : 830m.

Pour le trouver, le plus simple est de partir de *Urrixka 2 - Dolmen*. Pour atteindre ce dernier, suivre, à partir du col de Berdaritz, la piste qui monte vers le sommet d'Urrixka, puis prendre, à environ 750m d'altitude, un bel embranchement vers la droite, qui après 800m de parcours en montée douce, arrive sur un replat, dans un sous-bois. *Urrixka 2 - Dolmen* est immédiatement visible sur la droite, érigé sur un sol en légère pente vers l'Est. On note à ce niveau, une bifurcation de la piste, dont une branche redescend vers l'Est. Parcourir environ 70m jusqu'au niveau d'un éboulis de gros rocs remaniés au bulldozer ; elle se dirige vers le Nord-Est sur une centaine de mètres : on arrive à un début de pente vers le Nord, là où se trouve le dolmen.

Description

Vaste tumulus pierreux (en grés triasique, comme tous les monuments de cette montagne) de 10m de diamètre et 0,50m de haut, érigé sur un terrain en légère pente vers le Nord. Au centre, trois vastes fragments de dalle paraissent pouvoir être considérés comme les vestiges de la dalle de couverture.

Ils mesurent respectivement : 1,07m de long, 1,36m de large et 0,38m d'épaisseur. Le fragment voisin, à l'Est, mesure 1,52m de long, 0,84m de large et 0,33m d'épaisseur ; enfin le troisième fragment, situé contre le précédent et à son extrémité Nord, mesure 0,81m de long, 0,70m de large et 0,24m d'épaisseur.

Trois autres dalles semblent pouvoir être rattachées aux montants de la [chambre funéraire]{.subject} qui est orientée Est-Ouest. La première dalle, située à l'extrémité Sud-Ouest du fragment de dalle de couverture ci-dessus décrit en premier, est verticalement enfoncée dans le sol selon un axe Est-Ouest ; elle mesure 1,43m de long, 0,13m d'épaisseur et environ 0,30m de haut. On peut distinguer, au Nord de celle-ci, une deuxième dalle, quasiment couchée sur le sol et qui paraît bien en avoir été arrachée ; elle mesure 1,30m de long à sa base, 0,60m au sommet et 0,20m d'épaisseur. Son orientation et ses dimensions nous la font interpréter comme un des montants Nord de la chambre funéraire. Il en est de même pour la dalle suivante, située dans le prolongement et à l'Est de la précédente, (et comme elle probablement arrachée du sol), mesurant 1,27m de long, 0,95m de large et 0,18m d'épaisseur.

Historique

Monument découvert par [F. Meyrat]{.creator} en [mai 2010]{.date}.

### [Aldude]{.spatial} - [Urrixka]{.coverage} 3 - [Tumulus-cromlech]{.type} ([?]{.douteux})

Localisation

Carte 1346 Ouest St Etienne-de-Baïgorri.

Ce monument est situé sur un éperon rocheux à sommet plat, qui se dresse à 70m environ à l'Ouest de *Urrixka 2 - Dolmen*.

Description 

Petite structure difficile à classer, circulaire, de 1m à 1,10m de diamètre, constituée de petites dallettes, au nombre de 12 à 13, certaines disposées un peu en écaille de poisson, se superposant en partie, et formant une couronne enserrant 3 autres petites dalles disposées à plat. Cette structure, édifiée à l'extrémité de l'éperon rocheux, dominant le magnifique panorama de la vallée des Aldudes entourée de ses montagnes, mais exposée en plein vent, évoque plus une structure funéraire que les vestiges d'un foyer de campeurs, par exemple. Monument douteux.

Historique

Monument découvert en [juin 2003]{.date}.

### [Aldude]{.spatial} - [Zarkindégui]{.coverage} - [Pierre couchée]{.type} ([?]{.douteux})

Description

Il s'agit de la BF 122 gisant au sol ; pierre grossièrement épannelée, à sommet arrondi, pourrait être une « borne antique » réutilisée ultérieurement. Elle mesure 1,27m de long, 0,75m à sa base et 0,38m d'épaisseur en moyenne. Une croix est gravée au-dessus du nombre 122.

Historique

Monument découvert par [Blot J.]{.creator} en [juin 2010]{.date}.

### [Aldude]{.spatial} - [Zarkindegui]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 760m.

À environ une centaine de mètres à l'Ouest de la BF 119.

Description

Tumulus de 8,50m de diamètre, et de faible hauteur, érigé sur un terrain en légère pente vers le Nord, il est constitué d'un amoncellement de petite pierraille en grés ; on note une dépression centrale de faible profondeur.

Historique

Monument découvert par [J. Blot]{.creator} en [avril 2010]{.date}.
