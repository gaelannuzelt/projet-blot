# Jaxu

### [Jaxu]{.spatial} - [Ihitzgorri]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 555m.

Il se trouve à l'extrémité Ouest de la croupe dénommée Nabahandi ou Ihitgorri.

Description

Tumulus de pierres de 3m de diamètre érigé sur un filon rocheux. Sa hauteur varie entre une vingtaine de centimètres et une quarantaine, dans sa partie Nord où l'on a récemment rajouté des éléments d'épierrage, sur la structure d'origine. Il semble bien que l'on puisse distinguer un péristalithe dans sa moitié Nord.

Historique

Monument découvert en [avril 2012]{.date} par [P. Velche]{.creator}.

### [Jaxu]{.spatial} - [Ihitzgorri]{.coverage} - [Monolithe]{.type}

Localisation

Altitude : 503m.

Ce monolithe git à 0,50m au Sud Sud-Est de la piste qui parcourt la crête d'Ihitzgorri, et se trouve être la seule pierre notable des environs.

Description

Dalle de [grés]{.subject} local, allongée sur le sol suivant un axe Ouest Sud-Ouest Est Nord-Est, mesurant plus de 3m de long, 1,10m de large, de forme triangulaire à sommet Ouest. Cette dalle présente des traces d'épannelage tout le long deux ses deux bords.

Historique

Monolithe découvert par [P. Velche]{.creator} en [avril 2012]{.date}.

### [Jaxu]{.spatial} - [Ihitzgorri]{.coverage} 2 - [Tumulus]{.type}

Localisation

Altitude : 545m.

Il est situé à environ 140m à l'Est du tumulus *T 1* décrit dans le *Tome 5* de notre *Inventaire* [@blotInventaireMonumentsProtohistoriques2013d, p.13].

Description

Tumulus pierreux plutôt ovale de 6m x 5m et 0,30m de haut. Paraît s'appuyer en partie sur un filon rocheux naturel ?

Historique

Tumulus découvert par [F. Meyrat]{.creator} en [août 2015]{.date}.

### [Jaxu]{.spatial} - [Ihitzgorri]{.coverage} 3 - [Tumulus]{.type}

Localisation

Altitude : 500m.

Il est situé dans un petit col immédiatement au Nord-Est et au pied de la colline Ihizgorri.

Description

Tumulus pierreux circulaire de 5m de diamètre et 0,30m de haut.

Historique

Tumulus découvert par [F. Meyrat]{.creator} en [août 2015]{.date}.

### [Jaxu]{.spatial} - [Nabahandi]{.coverage} - [Dalle taillée]{.type}, [dolmen]{.type} ([?]{.douteux})

Localisation

Altitude : 507m.

Dalle située vers l'extrémité Nord-Est de la petite colline du même nom.

Elle est située à une trentaine de mètres au Sud-Est de *Ihitzgorri - Monolithe*, et à une cinquantaine de mètres à l'Est du *Col de Ihizgorri 3 - Tumulus*.

Description

Dalle de [grés]{.subject} [triasique]{.subject}, plantée dans le sol, selon un axe Est Nord-Est Ouest Sud-Ouest (et fortement inclinée vers le Nord). Elle mesure 0,90m de long à sa base, 0,50m dans sa plus grande hauteur, et son épaisseur est en moyenne de 0,15m. Tout son pourtour est épannelé.

Dans son prolongement au Nord-Est, on note à une distance de 0,60m, une première pierre, et à 0,55m de celle-ci une seconde, toutes deux en grès rose, alors que tout l'environnement est formé de blocs et filons de calcaire blanc.

Il ne semble pas y avoir de tumulus autour de cette dalle dont la finalité pose problème : borne ? vestige d'un petit coffre dolménique démoli ?

Historique

Dalle découverte par [Blot J.]{.creator} en [octobre 2015]{.date}.

### [Jaxu]{.spatial} - [Sen Julian]{.coverage} - [Cromlech]{.type} ([?]{.douteux})

Localisation

Altitude : 479m.

Tout au sommet d'une petite colline, se dresse un calvaire un peu décentré à l'Est Sud-Est par rapport au centre d'un monument formé de deux cercles concentriques.

Description

Il semble que l'on puisse discerner 2 cercles concentriques constitués de pierres affleurant à peine à la surface du sol : le plus interne, de 4m de diamètre, posséderait une dizaine de pierres ; le plus externe, de 8m de diamètre est délimité par 15 pierres bien visibles au Nord Nord-Est.

Historique

Monument très douteux découvert par [F. Meyrat]{.creator} en [août 2015]{.date}.
