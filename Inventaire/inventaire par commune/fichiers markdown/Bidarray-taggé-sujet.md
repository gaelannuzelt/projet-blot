# Bidarray

### [Bidarray]{.spatial} - [Artzamendi]{.coverage} - [Dolmen]{.type}

Localisation

Altitude : 680m.

Il faut prendre la piste qui relie la BF 81 à Mendittipi puis à la borde Mendigaîna sur le Plateau Vert. Le dolmen se situe à environ 80m au-dessus et au Nord-Ouest d'un cayolar en ruine situé sur cette piste, à peu près entre Mendittipi et Mendigaina.

Description

Important tumulus pierreux d'environ 11m de diamètre et 0,80m de haut, constitué de blocs de [grès]{.subject} et de gros fragments de dalles. Au centre se voit la [chambre funéraire]{.subject}, d'environ 2m de long, et 1,30m de large, profonde de 0,80m et orientée Sud-Est Nord-Ouest. Elle est délimitée, au Sud-Ouest par une grande dalle verticale de 1,27m de long, et 0,14m d'épaisseur. Au Nord-Est deux dalles verticales, accolées l'une contre l'autre, forment la paroi opposée, mesurant 0,85m de long et 0,75m de haut. La paroi Sud-Est, est elle aussi formée de deux dalles, mais dans le prolongement l'une de l'autre et très inclinées vers l'intérieur ; l'une mesure 0,62m de long, et 0,11m d'épaisseur, l'autre 0,66m de long. Enfin, la paroi Nord-Ouest est elle aussi constituée de deux dalles qui furent accolées l'une contre l'autre, mais dont la plus interne a basculé en dedans ; dimensions : la dalle interne : 0,97m de long, 0,90 de haut et 0,08m d'épaisseur, l'externe : 0,65m de long, 0,72m de haut et 0,20m d'épaisseur.

Au flanc Sud-Est du tumulus, se voient trois volumineux fragments de dalle qui sont en réalité la table dolménique, basculée sur le côté et fracturée (gel ? ; action de l'homme ? ) ; elle devait mesurer environ 2m de long, autant dans sa plus grande largeur et 0,25m d'épaisseur.

Historique

Monument découvert par mon ami [F. Meyrat]{.creator} en [mars 2010]{.date}.

### [Bidarray]{.spatial} - [Bourounzukoborda]{.coverage} - [Tertres d'habitat]{.type}

Localisation

Une dizaine de tertres (et même plus semble-t-il), se répartissent sur un plateau en légère pente vers le Nord Nord-Ouest, au pied des crêtes d'Iparla, très en contre-bas de la BF 88, entre la borde Bourounzukoborda à l'Est et deux cayolars près d'un pointement rocheux à l'Ouest. Deux petits ruisseaux encadrent à l'Est et à l'Ouest cet ensemble de tertres. Enfin deux pistes pastorales parallèles parcourent l'ensemble d'Est en Ouest ; les tertres se répartissent le long de ces pistes.

Description

- *Tertre n°1* : De forme ovale et asymétrique, il mesure 10m x 6m et 0,70m de haut.

- *Tertre n°2* : situé à 18m au Nord-Est du précédent, il mesure 8m x 6m et 0,60m de haut.

- *Tertre n°3* : juste au Sud de la piste, à 6m au Nord-Est du précédent ; Mêmes dimensions que le précédent.

- *Tertre n°4* : situé à 10m à l'Ouest Nord-Ouest du précédent, et de la première piste. Mesure 11m x 8m et 0,80m de haut.

- *Tertre n°5* : situé au Nord-Ouest de la deuxième piste et à 14m à l'Ouest Nord-Ouest du précédent. Mesure 9m x 8m et 0,50m de haut.

- *Tertre n°6* : situé à 2m à l'Ouest Sud-Ouest du précédent ; mesure 8m x 7m et 0,60m de haut.

- *Tertre n°7* : situé à 10m à l'Ouest Sud-Ouest du précédent ; mesure 10m x 6m et 0,40m de haut.

- *Tertre n°8* : situé au Sud de la piste et à 10m au Sud du précédent. Mesure 10m x 8m et 1m de haut.

- *Tertre n°9* : situé à nouveau au Nord de la piste et à 25m à l'Ouest Nord-Ouest du tertre n°7 : mesure 8m x 6m et 0,50m de haut.

- *Tertre n°10* : il est traversé par la piste pastorale (qui se rend à Bourounzukoborda), et est à 2m au Sud-Ouest du précédent. Il mesure 9m x 6m et 0,60m de haut.

### [Bidarray]{.spatial} - [Iparla]{.coverage} 1 - [Tumulus-cromlech]{.type}

Localisation

Carte 1345 Ouest Cambo-les-Bains.

Altitude : 830m.

Nous avons décrit en (ref)(Blot J.1983 b, p. 13) un monolithe dalle (Altitude 740m), érigé en bordure de la piste (actuel GR 10) qui, venant de Bidarray, escalade les premiers contreforts Nord-Est des crêtes d'Iparla, et arrive en terrain plat.

On trouve un ensemble de 6 monuments répartis sur un replat herbeux dominant, au Nord, le GR 10 d'une soixantaine de mètres environ. Le monument n°1 est le plus important et le plus visible de cette belle nécropole.

Description

Tumulus de 6m de diamètre et 0,50m de haut, entouré de 14 pierres bien visibles, pouvant atteindre, au Nord par exemple, 0,75m de haut et 0,70m de large.

Historique

Monument découvert en [mai 1972]{.date}.

### [Bidarray]{.spatial} - [Iparla]{.coverage} 2 - [Tumulus-cromlech]{.type}

Localisation

À 7m à l'Ouest Sud-Ouest de *Iparla 1 - Tumulus-cromlech*.

Description

Tumulus aplati de 10m de diamètre et 0,80m de haut, délimité par 14 pierres périphériques de 0,30m à 0,40m de haut en moyenne.

Historique

Monument découvert en [mai 1972]{.date}.

### [Bidarray]{.spatial} - [Iparla]{.coverage} 3 - [Tumulus]{.type}

Localisation

À 3m à l'Ouest Nord-Ouest de *Iparla 2 - Tumulus-cromlech*.

Description

Tumulus de 4m de diamètre et 0,40m de haut ; une vingtaine de pierres apparaissent en désordre à la surface de ce monument, mais il n'y a pas de péristalithe évident.

Historique

Monument découvert en [mai 1972]{.date}.

### [Bidarray]{.spatial} - [Iparla]{.coverage} 4 - [Cromlech]{.type}

Localisation

Il est à 6m à l'Ouest Sud-Ouest de *Iparla 3 - Tumulus* et à 10m à l'Ouest Sud-Ouest de *Iparla 2 - Tumulus-cromlech*.

Description

Cercle de 5m de diamètre, très légèrement surélevé, délimité par 5 petites dallettes disposées de façon radiale, perpendiculaires au tracé du cercle, ce qui est exceptionnel en Pays Basque ; nous avons cependant revu ce type d'architecture dans nos fouilles au col de Méatsé.

Historique

Monument découvert en [mai 1972]{.date}.

### [Bidarray]{.spatial} - [Iparla]{.coverage} 5 - [Tumulus-cromlech]{.type}

Localisation

Il est à 4m au Nord Nord-Ouest de *Iparla 4 - Cromlech*.

Description

Tumulus de 5m de diamètre et 0,30m de haut. Trois pierres bien visibles à sa périphérie, en secteur Sud, permettent de penser qu'il y a bien un péristalithe. On note une pierre centrale.

Historique

Monument découvert en [mai 1972]{.date}.

### [Bidarray]{.spatial} - [Iparla]{.coverage} 6 - [Cromlech]{.type}

Localisation

Il est à 7m au Nord-Est de *Iparla 5 - Tumulus-cromlech*.

Description

Cercle de 4,60m de diamètre délimité par 4 pierres bien visibles dans le secteur Sud ; il existe aussi une pierre centrale.

Historique

Monument découvert en [mai 1972]{.date}.

### [Bidarray]{.spatial} - [Iparla]{.coverage} 7 - [Tumulus-cromlech]{.type}

Localisation

Même carte.

Altitude : 812m.

Monument isolé, situé sur un vaste plateau à 500m environ au Sud Sud-Ouest de *Iparla 6 - Cromlech*, à quelques mètres à l'Ouest de la piste pastorale reprise par le GR 10.

Description

Tumulus de 8m de diamètre et 0,50m de haut. Six dalles bien visibles, particulièrement au Nord et au Sud marquent la périphérie du monument. Au centre, une dépression allongée à grand axe Est-Ouest, réniforme, reste probablement la trace d'une fouille ancienne.

Historique

Monument découvert en [septembre 1973]{.date}.

### [Bidarray]{.spatial} - [Iparla]{.coverage} 8 - [Tumulus-cromlech]{.type}

Localisation

Même carte.

Altitude : 650m.

Il est situé sur un replat à environ 150m à l'Est Nord-Est et au-dessus de la bergerie Belzaouzk et sur la gauche de la piste qui monte du petit col qui sépare cette bergerie de Bourouzunekoborda.

Description

Tumulus de terre, circulaire, à sommet aplati en galette, mesurant 9,50m de diamètre et 0,60m de haut. Sa périphérie est marquée par 6 pierres : 4 en secteur Sud-Ouest et 2 en secteur Nord-Est.

Historique

Monument découvert en [octobre 1974]{.date}.

### [Bidarray]{.spatial} - [Lacho]{.coverage} - [Dolmen]{.type}

Localisation

Altitude : 550m

Il se trouve à environ 120 mètres à l'Ouest et en contre-bas de *Lacho 1 - Monolithe*.

Description

La [chambre funéraire]{.subject} de ce monument, orientée Nord-Sud paraît longue de 0,90m à 1m et large en moyenne de 0,75m. Elle est délimitée par 5 dalles de [grès]{.subject} rose, d'environ 6 à 8 centimètres d'épaisseur dont les sommets dépassent de peu la surface du sol. La dalle Nord mesure 0,69m de long et 0,15m de haut ; la dalle Ouest mesure 0,80m de long et la paroi Est est formée de deux dalles, l'une de 0, 36m de long et 0,12 de haut, l'autre de 0,46m de long et 0,13m de haut. Cette chambre est au centre d'un tumulus mixte de terre et de pierres dont certaines apparaissent dans le quadrant Nord-Est ainsi qu'à l'Ouest. À la périphérie Ouest de ce tumulus, faiblement marqué, gît, une dalle de [grès]{.subject} bien visible de forme trapézoïdale, de 2,50m de long et 1,67m de large, à grand axe Nord-Ouest Sud-Est, qui ne semble pas être du tout en rapport avec ce monument.

Historique

Monument découvert par [F. Meyrat]{.creator} en [décembre 2010]{.date}.

### [Bidarray]{.spatial} - [Lacho]{.coverage} 1 - [Monolithe]{.type}

Localisation

Altitude : 580m.

Il se trouve à une quinzaine de mètres au Sud du sentier qui monte vers col de Lacho en venant des bergeries Turchilen et Larhanton.

On peut voir un éperon rocheux à une centaine de mètres à l'Est et un autre à environ 150 mètres à l'Ouest en contre-bas.

Description

Cette dalle de [grès]{.subject} rose gît sur un sol légèrement incliné vers l'Ouest, et présente une forme grossièrement parallélépipédique à sommet orienté vers le Sud-Ouest. Elle mesure 3,30m dans son plus grand axe, 2,05 mètres à sa base, qui est épaisse de 0,42m alors que le sommet n'est que de 0,16m d'épaisseur. Il est intéressant de noter que la forme pointue du sommet résulte d'un épannelage de la totalité de la base Ouest, et d'un autre plus discret au Sud. On retrouve ces traces de régularisation sur le côté Nord, et surtout à l'Est qui présente une très importante encoche, ayant sans doute dépassé l'attente du « tailleur de pierre ».

Historique

Ce monolithe a été trouvé en [décembre 2010]{.date} par [F. Meyrat]{.creator}.

### [Bidarray]{.spatial} - [Lacho]{.coverage} 1 - [Pierre couchée]{.type} ([?]{.douteux})

Localisation

Altitude : 520m.

Elle gît tout au bord Nord du sentier - qui la contourne nettement - reliant le col de Lacho à Bidarray, quand on se dirige vers l'Est. Le terrain est en pente vers le Nord, ainsi que la pierre. On ne note pas de signes d'éboulis dans les environs immédiats.

Description

Ce bloc de [grès]{.subject}, de forme grossièrement parallélépipédique mesure environ 2,50m de long et 1,50m de large, à grand axe orienté Nord-Sud et présente une épaisseur décroissante (0,40m au sud et 0,20m à 0,10m au Nord). Le bord Nord semble résulter d'une cassure, alors qu'un segment du bord Est, à sa jonction avec le bord Nord, présenterait peut-être des traces d'épannelage.

Historique

Pierre trouvée par [F. Meyrat]{.creator} en [janvier 2011]{.date}.

### [Bidarray]{.spatial} - [Lacho]{.coverage} 2 - [Pierre couchée]{.type} ([?]{.douteux})

Localisation

Elle est située dans une pente assez marquée, à 150m au-dessous et au Sud-Ouest du piton rocheux (à la cote 536), et à environ 400m au Nord-Est de la BF 86.

Altitude : 470m.

Description

Gros bloc de [grès]{.subject} en forme de pain de sucre, à grand axe Est-Ouest et à pointe Ouest. Il mesure 4,80m de long, 2,10m à sa base, 0,60m à la pointe, et possède une épaisseur qui varie de 0,75m à 0,95m. Les éventuelles traces d'épannelage, par exemple à la base, sont fort discrètes.

Historique

Pierre trouvée par [F. Meyrat]{.creator} en [décembre 2010]{.date}.

### [Bidarray]{.spatial} - [Lacho]{.coverage} - [Tumulus]{.type}

Localisation

Il se trouve à 7 mètres à l'Est de la [chambre funéraire]{.subject} de *Lacho - Dolmen*.

Description

Petit tumulus pierreux circulaire de 2,40m de diamètre et 0,30m de haut, constitué de petits blocs de [grès]{.subject} et de fragments de dalles, disposés de façon concentrique. Monument très semblable au *tumulus de Buluntza*, à celui d'*Apatessarro 8*, à celui d'*Erreta* ou de *Caminarte*.

Historique

Monument découvert par [F. Meyrat]{.creator} en [décembre 2010]{.date}.

### [Bidarray]{.spatial} - [Lacho]{.coverage} - [Tumulus-cromlech]{.type}

Localisation

À 3 mètres à l'Ouest Sud-Ouest de *Lacho - Dolmen*.

Description

On note un tumulus pierreux d'environ 4m de diamètre et 0,50m de haut, constitué de terre et de nombreux fragments de dalles ; il semble que la périphérie Sud-Ouest de ce tumulus soit assez nettement délimitée par une dizaine de pierres disposées en arc de cercle. Au centre (après un léger dégagement de la couverture herbeuse) on peut voir une petite [ciste]{.subject} rectangulaire, formée de 4 dalles, orientée Nord-Ouest Sud-Est et dont les dimensions sont de 0,65m de long et 0,40m de large.

Historique

Monument découvert par [F. Meyrat]{.creator} en [décembre 2010]{.date}.

### [Bidarray]{.spatial} - [Méatsékobizkarra]{.coverage} - [Monolithe]{.type}

Localisation

Il est situé à 80m au Sud Sud-Ouest du très beau cromlech entourant la Borne Frontière n°81 (qui l'a massacré), et se trouve en bordure d'un chemin relativement récent, creusé au bulldozer. Il semble donc que sa position actuelle ne soit pas l'ancienne, mais elle ne doit en être éloignée que de quelques mètres au maximum.

Altitude : 715m.

On notera, là encore, la présence d'un point d'eau tout proche, à une dizaine de mètres au Nord. Il est par ailleurs intéressant de constater que ce monolithe ainsi que celui déjà décrit par nous sous le nom de *monolithe de l'Artzamendi*, se trouvent tous deux à proximité d'une borne frontière (la borne 82 pour ce dernier) et il n'est pas exclu que ces pierres soient les ancêtres directs de ces deux bornes frontières ; on note par exemple que le monolithe ici décrit possède deux repères bien visibles, sous forme de deux petites croix profondément gravées sur sa face supérieure. On connaît des monolithes, possédant eux aussi des inscriptions « modernes », qui jouent encore le rôle de borne frontière comme à Eyharcé ou à Gorospil.

Description

Il se présente sous la forme d'un imposant bloc de [grès]{.subject} rose couché sur le sol, de forme grossièrement triangulaire, orienté Nord-Ouest Sud-Est. Long de 3m, il présente une épaisseur variable : la base, qui mesure 1,96m de large est épaisse de 0,20m ; la partie médiane, de 1,57m de largeur est épaisse de 0,55m, et le sommet de 1,22m de large, ne mesure que 0,28m d'épaisseur. On ne note pas de traces d'épannelage, mais, sur sa face supérieure, on note deux croix gravées de ½ centimètre de profondeur mesurant 0,16m x 0,16m, situées à 1,02m de la base, et symétriquement par rapport à la ligne médiane.

Historique

Ce monolithe nous a été signalé par [Francis Meyrat]{.creator} en [juin 2008]{.date}.

### [Bidarray]{.spatial} - [Mendittipiko lepoa]{.coverage} - [Cromlech]{.type}

Localisation

Altitude : 732m.

Il se trouve à environ 1 mètre au Sud du *dolmen de Zelai*, en plein col, et à une vingtaine de mètres à l'Ouest et en bas de la butte sur laquelle est érigé *Mendittipi - Tumulus-cromlech*.

Description

Cercle de 4,30m de diamètre, délimité par une dizaine de pierres au ras du sol, plus nombreuses dans la moitié Ouest.

Historique

Monument découvert par [J. Blot]{.creator} en [Mars 2010]{.date}.

### [Bidarray]{.spatial} - [Mendittipiko lepoa]{.coverage} 2 - [Cromlech]{.type}

Localisation

Altitude : 730m.

Il est à 1 mètre au Sud-Ouest du *dolmen Zelai*.

Description

Seules 4 pierres sont visibles, dont une, à l'Ouest, mesure 0,77m de long et 0,20m de haut ; elles délimitent un cercle de 4,30m de diamètre.

Historique

Monument découvert par [Blot J.]{.creator} en [novembre 2011]{.date}.

### [Bidarray]{.spatial} - [Olhatia]{.coverage} - [Dalle plantée]{.type} ([?]{.douteux})

Localisation

Cette dalle était située sur un replat situé à droite de la route qui se termine en cul-de-sac (lieu-dit Olhatia) avant de se transformer en piste charretière pour se rendre au Plateau Vert en passant au flanc Sud-Ouest du mont Gakoeta.

Altitude : 330m.

Description

À cet endroit, était plantée selon un axe Nord-Sud, une belle dalle de [grès]{.subject} rose, à sommet arrondi résultant d'un épannelage évident. Elle mesurait 1,10m de haut, 0,80m de largeur à sa base et 0,40m d'épaisseur au maximum. Deux autres pierres, pratiquement tangentes, l'accompagnaient, l'une au Nord, (0,40m de large et 0,50m de haut), l'autre au Sud, (0,35m de large et 0,20m de haut), sans qu'il soit vraiment possible de parler de [chambre funéraire]{.subject}.

Borne pastorale ? Vestige dolménique ?

Historique

Dalle découverte par [J. Blot]{.creator} en [octobre 1971]{.date}, et détruite depuis.

### [Bidarray]{.spatial} - [Plateau Vert]{.coverage} 1 - [Dolmen]{.type}

Localisation

Carte 1345 Ouest Cambo-les-Bains.

Altitude : 570m.

Dolmen situé à l'extrémité Sud du Plateau Vert, et à une vingtaine de mètres au Nord du chemin qui monte de Bidarray, un peu avant qu'il n'arrive en terrain plat. Il est à 100m à l'Ouest d'une borde en partie ruinée, située dans un petit bosquet de hêtres.

Description

Tumulus terreux d'environ 8m de diamètre et 0,40m de haut, plus marqué dans sa partie Nord, le terrain étant légèrement en pente vers le Sud.

La [chambre funéraire]{.subject} centrale, orientée Nord Nord-Est Sud Sud-Ouest, est bien conservée et mesure 2,70m de long, 2m dans sa partie la plus large, au Nord, et 0,70m de profondeur. Elle est délimitée, au Nord, par une dalle de 2m de long ; à l'Ouest par une dalle de 2,20m de long, 0,70m de haut et 0,30m d'épaisseur. La paroi est se compose de 2 dalles dans le prolongement l'une de l'autre, inclinées vers l'intérieur : la plus grande, au nord, de 1m de long, est doublée à sa face externe par 2 dalles plus petites. De la paroi Sud, il ne reste que 2 dalles brisées et en partie enfouies qui ont glissé un peu à l'extérieur de la [chambre funéraire]{.subject}. Enfin à 1m au Sud-ouest gît une grande dalle de 1,70m de long, 1,20m de large, et 0,30m d'épaisseur, qui pourrait être un élément d'un montant latéral, ou plus probablement de la [couverture]{.subject} (ou table).

Historique

Dolmen découvert en [octobre 1972]{.date}.

### [Bidarray]{.spatial} - [Plateau Vert]{.coverage} 2 - [Dolmen]{.type}

Localisation

Carte 1345 Ouest Cambo-les-Bains.

Altitude : 490m.

Dolmen très difficile à repérer ; situé à l'extrémité Nord du Plateau Vert, à environ 150m à l'Ouest Nord-Ouest d'une bergerie érigée près d'un pointement rocheux.

Description

Il n'y a pas de tumulus visible. La [chambre funéraire]{.subject} mesure 2,30m de long, 0,80m de large, et 0,50m de haut ; elle est délimitée par 8 dalles en partie brisées. La paroi Ouest est formée par 3 dalles alignées selon un axe Nord-Sud ; la plus grande, au Nord, mesure 1m de long à sa base et 0,40m de haut, les 2 autres ne font que 0,50m de long et 0,30m de haut. La paroi Est se compose de 3 dalles alignées selon un axe Nord Nord-Est, Sud Sud-Ouest, de longueur croissante, de 0,40m pour la plus petite au Nord, à 1m de long pour celle du Sud ; 2 autres petites dalles, au ras du sol, complètent cette paroi. Il n'y a pas de couvercle visible.

Historique

Dolmen découvert en [mars 1971]{.date}.

### [Bidarray]{.spatial} - [Plateau Vert]{.coverage} 4 - [Cromlech]{.type}

Localisation

Carte 1345 Ouest Cambo-les-Bains.

Altitude : 600m.

Nous avons déjà décrit en 1972, (ref)(Blot J.,1972, p.22) près d'une borde, un tumulus (dénommé à tort « dolmen » ; il s'agit en fait du *tumulus n°3 du Plateau Vert*), au flanc Nord-Ouest du Plateau Vert.

Le cromlech n°4 est situé sur la partie la plus élevée de ce Plateau Vert, à 8m au Nord d'une petite mare toujours bien visible.

Description

Cercle de 3m de diamètre, légèrement surélevé, délimité par 15 petites dalles dont 5 dans le secteur Ouest et 10 dans le secteur Est ; la [ciste]{.subject} centrale, de 1m x 0,50m, est délimitée par 5 petites dalles dont une nettement plus grande que les autres.

Historique

Monument découvert en [juin 1971]{.date}.

### [Bidarray]{.spatial} - [Plateau Vert]{.coverage} 5 - [Tumulus-cromlech]{.type}

Localisation

Carte 1345 Ouest Cambo-les-Bains.

Altitude : 600m.

Il est à environ 100m au Nord de *Plateau Vert 4 - Cromlech*, sur le début du plat de la cote 600.

Description

Très beau tumulus-cromlech terreux de 14m de diamètre et 1m de haut entouré de 19 dalles, au ras du sol, mais nettement visibles, particulièrement en secteur Sud Sud-Ouest.

Historique

Monument découvert en [juin 1971]{.date}.

### [Bidarray]{.spatial} - [Plateau Vert]{.coverage} 6 - [Cromlech]{.type}

Localisation

Il est situé à150m à l'Est Sud-Est de la petite mare déjà signalée au *Plateau Vert 4 - Cromlech*, et à 30m environ au Nord du rebord Sud-Ouest du plateau.

Description

Cercle de 3m de diamètre délimité par une vingtaine de dalles plus ou moins inclinées, couchées ; 2 seulement sont restées verticales. Deux dalles au centre pourraient faire partie de la [ciste]{.subject}.

Historique

Monument découvert en [juin 1971]{.date}.

### [Bidarray]{.spatial} - [Plateau Vert]{.coverage} 6 bis - [Cromlech]{.type}

Localisation

Altitude : 611m.

Description

Un cercle de 3,20m de diamètre est matérialisé par une quinzaine de pierres au ras du sol, et 2 au centre, verticales ; on note qu'au point de tangence des deux cercles, le n°6 bis est incomplet, 3 pierres ne sont pas dans le bon axe, la construction du 6 bis étant postérieure à celle du n°6.

Historique

Monument découvert par [Blot J.]{.creator} et [Meyrat F.]{.creator}, en [octobre 2010]{.date}.

### [Bidarray]{.spatial} - [Plateau Vert]{.coverage} 6 ter - [Cromlech]{.type}

Localisation

À environ 2 mètres au Sud de *Plateau Vert n°6 bis - Cromlech*.

Description

Ce monument se présente comme un faible relief de terrain circulaire, de 5,20m de diamètre, avec une légère dépression centrale ; 3 pierres visibles à la périphérie au Sud.

Historique

Monument découvert par [Blot J.]{.creator} et [Meyrat F.]{.creator}, en [octobre 2010]{.date}.

### [Bidarray]{.spatial} - [Plateau Vert]{.coverage} 9 bis - [Coffre dolménique]{.type} (ou [ciste]{.subject} d'incinération ?)

Localisation

Altitude : 611m

Description

Une dizaine de dalles de [grès]{.subject} roses enfoncées dans le sol paraissent délimiter un coffre (?) d'environ 2,50m de long et 0,80m de large. Au Nord, la dalle la plus importante mesure 0,56m de long et 0,14m de haut ; au Sud, une autre, qui lui est parallèle, mesure 0,70m de long et 0,33m de haut. À l'Est et à l'Ouest, deux dalles inclinées l'une vers l'autre mesurant chacune 0,63m à la base, pourraient, alors, faire plutôt partie d'une petite [ciste]{.subject} de dimensions bien plus modestes (0,70m x 0,70m).

Historique

Monument découvert par [Blot J.]{.creator} et [Meyrat F.]{.creator}, en [octobre 2010]{.date}.

### [Bidarray]{.spatial} - [Plateau Vert]{.coverage} 10 - [Dolmen]{.type}

Localisation

Altitude : 575m.

À une dizaine de mètres l'Est Nord-Est de la piste qui se rend au col de l'Âne.

Description

Au centre d'un tumulus de terre de 7m de diamètre environ apparaît une dalle de [grès]{.subject} [triasique]{.subject} inclinée vers le Nord-Ouest, de forme triangulaire, orientée selon un axe Est Nord-Est Ouest Sud-Ouest. Elle mesure 1,47m de long, 0,48m dans sa plus grande hauteur et 0,13m d'épaisseur. Toute sa périphérie visible présente des traces d'épannelage.

Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2010]{.date}.

### [Bidarray]{.spatial} - [Plateau Vert]{.coverage} 11 - [Dolmen]{.type}

Localisation

Altitude : 611 m 

Il est situé à environ 8m au Nord-Est de la piste empruntée par les véhicules 4x4, selon un axe Est-Ouest.

Description

On note une belle dalle de [grès]{.subject} rose, couchée au sol selon un axe Est-Ouest, de forme rectangulaire, mesurant 1,80m de long et 0,75m de large et d'une épaisseur moyenne de 0,10m. Quelques traces d'épannelage sont visibles à l'angle Sud et sur son bord Nord. Au Sud, un dégagement sommaire a mis en évidence de nombreux blocs de [grès]{.subject} qui semblent bien faire partie d'un tumulus, peu marqué mais visible cependant, d'environ 5 à 6 mètres de diamètre. Certains de ces blocs recouvrent par endroit le bord Sud de la dalle, qui est légèrement inclinée vers le Sud.

Historique

Monument découvert en [février 2011]{.date} par [F. Meyrat]{.creator}.

### [Bidarray]{.spatial} - [Plateau vert]{.coverage} 12 - [Dolmen]{.type}

Localisation

Altitude : 605m

Il est situé à 4 mètres au Nord de la piste des 4x4, tracée selon un axe Est-Ouest et à une quarantaine de mètres au Nord Nord-Ouest d'une petite mare.

Description

Au milieu d'un tumulus de 5 m de diamètre, de 0,40m de hauteur, dont 2 pierres assez importantes (l'une au Nord-Est, mesure 0,80m x 0,75m) l'autre au Sud-Ouest, semblent baliser la périphérie, on remarque les restes d'une [chambre funéraire]{.subject} d'environ 1,50m de long et 1m de large, orientée selon un axe Est Sud-Est Ouest Nord-Ouest, dont les montants ne dépassent que de peu la surface du sol. Elle est délimitée au Nord par une dalle de 1,04m de long et 0,15m de haut, à l'Est Sud-Est par une dalle de 0,80m de long et 0,32m de haut, et au Sud Sud-Est par une autre dalle de 0,90m de long et 0,15m de haut.

Historique

Monument découvert par [F. Meyrat]{.creator} en [février 2011]{.date}.

### [Bidarray]{.spatial} - [Plateau Vert]{.coverage} 13 - [Dolmen]{.type}

Localisation

Altitude : 605m

Il est situé à une dizaine de mètres à l'Ouest d'une petite mare.

Description

[chambre funéraire]{.subject} à grand axe orienté Nord-Sud, au milieu d'un tumulus terreux, d'environ 6 mètres de diamètre sans péristalithe visible. La chambre, longue de 1,40m environ et 1,10m de large est délimitée par 3 dalles au ras du sol. À l'Ouest, une dalle de 0,87m de long, 0,15m de haut et 0,05m d'épaisseur ; à l'Est, une autre dalle de 0,90m de long, 0,08m de haut et 0,04m d'épaisseur. Enfin, au Nord, une dernière dalle pratiquement couchée sur le sol et qui mesure 0,45m à la base et 1,10m de « haut » ainsi qu'environ 0,05m d'épaisseur peut être valablement considérée comme faisant partie de la chambre. À l'extérieur de la dalle Est, et à 2 mètres du centre, on note, enfoncée dans le sol une dalle de 0,56m de long et 0,40m de large.

Historique

Monument découvert par [F. Meyrat]{.creator} en [février 2011]{.date}.

### [Bidarray]{.spatial} - [Plateau Vert]{.coverage} 14 - [Dolmen]{.type}

Localisation

Altitude : 605m.

Il est situé très exactement au Nord de la mare citée plus haut (*Plateau vert n°12 - Dolmen*), et tangent à elle.

Description

C'est surtout la dalle de [couverture]{.subject}, déplacée sans doute depuis longtemps, qui attire le regard. Les montants de la [chambre funéraire]{.subject}, à peine visibles au ras du sol ont nécessité d'être un peu dégagés pour confirmer la réalité du monument.

La dalle de [couverture]{.subject} est donc actuellement à la périphérie du tumulus qui devait mesurer environ 6 mètres de diamètre, et sans doute de nature pierreuse comme le laisse supposer la pierraille qui s'éboule dans la mare, sous l'extrémité Sud Sud-Est de cette dalle. Celle-ci, de forme approximativement rectangulaire, mesure 1,50m dans son plus grand axe (orienté Sud Sud-Est, Nord Nord-Ouest), et 1,35m dans l'autre ; son épaisseur varie de 0,06m à 0,08m. On peut voir, semble-t-il, des traces d'épannelage au niveau des extrémités Nord-Ouest et Sud-Est de celle-ci.

La [chambre funéraire]{.subject}, visible au Nord de cette dalle de [couverture]{.subject}, n'est délimitée actuellement que par trois dalles : l'une, au Sud, mesure 1,20m de long, et se trouve à 2,30m de la mare ; la dalle Est mesure 0,65m de long, enfin la dalle Ouest, est à 0,64m de la dalle Sud, et mesure 0,40m de long.

### [Bidarray]{.spatial} - [Plateau Vert]{.coverage} 15 - [Dolmen]{.type}

Localisation

Altitude : 611m.

Description

La dalle de [couverture]{.subject} de forme grossièrement triangulaire, mesure 1,05m dans son plus grand axe Nord-Est Sud-Ouest et 0,90m dans le sens Nord-Ouest Sud-Est ; son épaisseur visible est de 3 à 4 centimètres environ. La partie supérieure d'une autre dalle est visible à son bord Nord-Ouest, dont elle rejoint l'angle Est. ; elle mesure 0,60m de long et 3 cm d'épaisseur ; deux autres dalles apparaissent légèrement au Nord-Ouest et au Nord-Est. Il semble qu'il existe un tumulus de très faible relief, d'environ 5 à 6 mètres de diamètre.

Historique

Monument découvert par [J. Blot]{.creator} en [mars 2011]{.date}.

### [Bidarray]{.spatial} - [Talatzé]{.coverage} - [Dolmen]{.type} ou [ciste]{.subject} ([?]{.douteux})

Localisation

Altitude : 906m.

Il est situé à environ 25 mètres au Sud-Est du GR 10, dont il est bien visible - sur la crête dénommée Talatzé, sur un terrain en pente vers le Nord Nord-Ouest.

Description

Il est au centre d'un tumulus de terre et de quelques pierres, de 6 mètres de diamètre, actuellement peu marqué (0,20m), mais l'on doit tenir compte du soutirage dû à la pente. Un ensemble de dalles plantées qui émergent de quelques centimètres au-dessus du sol, délimitent une [chambre funéraire]{.subject} mesurant 2,40m de long et 1,20m de large, orientée Nord Nord-Ouest Sud Sud-Est. Le côté Est est marqué par 5 dalles dont 2 parallèles, mesurant 1,40m et 1,18m. Du côté Ouest, on note 6 dalles de dimensions variables, plus ou moins parallèles deux à deux, dont certaines atteignent 1m ou plus. (il semble que certaines dalles, à l'Est comme à l'Ouest puissent résulter du délitement d'une seule dalle originelle). Le côté Sud est indiqué par 4 dalles disposées de façon assez lâche, et de dimensions inférieures aux précédentes : 0,50m et 0,58m par exemple. Au Nord, la limite paraît indiquée par 3 dalles, dont celle du milieu mesurant 0,50m de long et 0,29m de large.

Il semblerait que 6 autres dalles dans le plongement de cette chambre, (3 au Nord et 3 au Sud), fassent penser soit à une chambre double, soit à un « couloir », soit à une disposition naturelle, tout ceci reste très discutable. Ce prolongement mesurerait 3m de long et 1m de large.

Historique

Monument découvert par [Velche P.]{.creator} en [août 2012]{.date}.

### [Bidarray]{.spatial} - [Urdaindenzarretako Zelaia]{.coverage} 1 - [Tumulus-cromlech]{.type}

Localisation

Altitude : 642m.

Il se trouve sur le plat de Lacho, à environ 65m au Sud-Ouest de la borde la plus au Nord du plateau. Une piste pastorale est tangent à sa périphérie Sud-Ouest.

Description

Tumulus-cromlech de 5m de diamètre environ et 0,40m de haut, de terre et de pierres ; quelques dalles plantées ou au ras du sol paraissent bien délimiter un péristalithe de 4m de diamètre.

Historique

Monument découvert par [A. Martinez]{.creator} en [juillet 2012]{.date}.

### [Bidarray]{.spatial} - [Urdaindenzarrateko Zelaia]{.coverage} 2 - [Cromlech]{.type}

Localisation

Altitude : 642m.

Il est à 9m à l'Est de *Urdaindenzarretako Zelaia 1 - Tumulus-cromlech*.

Description

Cercle de pierres ou de dalles en [grès]{.subject} rose local, de 5m de diamètre, délimité par 9 pierres, dépassant de peu le sol ; la plus notable est au Sud Sud-Est et mesure 1,20m de long et 0,30m de large. Le centre est légèrement surélevé (0,20m), et 5 dalles y délimitent une [ciste]{.subject} carrée de 1,10m de côté. La dalle la plus importante, à l'Ouest mesure 1m de long et 0,35m de large.

Historique

Monument découvert par [M. Inarra]{.creator} en [2012]{.date}.

### [Bidarray]{.spatial} - [Urdaindenzarrateko Zelaia]{.coverage} 3 - [Cromlech]{.type}

Localisation

Altitude : 642m.

Il est situé à 2m à l'Est de *Urdaindenzarrateko Zelaia 2 - Cromlech*.

Description

Dix pierres (ou les dalles périphériques), en [grès]{.subject} rose local, sont au ras du sol et délimitent un cercle de 4,80m de diamètre, au centre très légèrement surélevé (0,10m). Trois dalles à l'Est (dont une mesure 0,65m de long et 0,30m de haut), et deux autres au Nord et à l'Ouest délimitent une [ciste]{.subject} centrale bien visible.

Historique

Monument découvert par [M. Inarra]{.creator} en [2012]{.date}.

### [Bidarray]{.spatial} - [UrdaindenZarrateko Zelaia]{.coverage} 4 - [Cromlech]{.type}

Localisation

Altitude :649m.

Il est sur le plat de Lacho. Ce monument, *C4*, est à 1,50m au Sud de *Urdaindenzarrateko Zelaia 3 - Cromlech*.

Description

Une dalle de [grès]{.subject} [triasique]{.subject} apparaît au ras du sol, grossièrement quadrangulaire, mesurant 0,90m x 0,80m ; elle est entourée d'un péristalithe formé de 6 à 7 pierres au ras du sol, délimitant un cercle de 4,80m de diamètre ; pierres de dimensions très modestes, l'une d'elles, à l'Est atteint cependant 0,50m x 0,12m.

Historique

Monument découvert par [Meyrat F.]{.creator} en [mars 2014]{.date}.

### [Bidarray]{.spatial} - [Urdaindenzarrateko Zelaia]{.coverage} - [Borne]{.type}

Localisation

Altitude : 645m.

Cette borne est à 30m au Sud Sud-Ouest d'un groupe de 3 cabanes en pierres sèches ruinées, et à 54m au Nord Nord-Est de *UrdaindenZarrateko Zelaia 4 - Cromlech*, (à 52m de *Urdaindenzarrateko Zelaia 3 - Cromlech*).

Description

Bloc de [grès]{.subject} [triasique]{.subject} local en forme de parallélépipédique rectangle, couché au sol suivant un axe Nord-Est Sud-Ouest. Il mesure 1,30m de long, avec un côté Sud-Ouest (la base dirions-nous), un peu plus grand (0,66m) que le côté opposé (0,55m). De même cette base est plus épaisse (0,31m) que le sommet (0,14m). De très nombreuses traces d'épannelage sont visibles sur toute la périphérie, en particulier à la base, au sommet et sur le côté Sud-Est.

Historique

Borne trouvée par [Meyrat F.]{.creator} en [mars 2014]{.date}.

### [Bidarray]{.spatial} - [Zelai]{.coverage} - [Monolithe]{.type}

Localisation

Altitude : 725m.

Il se trouve à une cinquantaine de mètres environ au Sud-Ouest du *dolmen de Zelai*, sur un terrain en pente douce vers le Sud-Est.

Description

Dalle peu visible avant un dégagement important, lequel a été assumé par notre ami Francis Meyrat. Elle se présente sous la forme d'une grande dalle triangulaire, plus ou moins cordiforme, et très symétrique par rapport à un axe Nord-Ouest Sud-Est. De surface parfaitement plane, on note que toute sa périphérie présente des traces de taille (épannelage). Elle mesure 3,20m dans son axe Nord-Ouest Sud-Est et 3,50m dans sa plus grande largeur ; son épaisseur moyenne est d'environ 0,35m. Située au pied d'une forte pente, elle a très bien pu subir au cours des siècles les effets de la colluvion qui l'a en partie recouverte, comme l'avait été, à moindre échelle le monolithe *Artzamendi,* mais dont la pente sus-jacente était beaucoup moins forte. Cette grande dalle est tangente à son bord Sud Sud-Est, à un bloc rocheux enfoui en partie enfoui dans le sol : pierre de calage d'un menhir qui aurait été érigé ?

Historique

Monolithe découvert par [Blot J.]{.creator} en [décembre 2011]{.date}.

### [Bidarray]{.spatial} - [Zelaïkogaina]{.coverage} - [Monolithe]{.type}

Localisation

Il se trouve à 70 mètres à l'Ouest Sud-Ouest du *cromlech* dénommé *Mendittipia* par J.M. de Barandiaran [@barandiaranContribucionEstudioCromlechs1949 p.205].

Altitude :735m.

Description

On note un bloc de [grès]{.subject} rose - couché sur le sol selon un axe Est-Ouest - de forme grossièrement parallélépipédique, mesurant 2,52m de long, 0,80 m dans sa plus grande largeur, (due à un renflement sur son bord Sud), et 0,42m de large à ses deux extrémités. Son épaisseur, que l'on ne peut vraiment apprécier que sur son bord Sud, (le Nord étant en partie enfoui sous terre) atteint 0,28m en moyenne. Certes, cette pierre isolée au milieu du pâturage, est assez remarquable ; toutefois ses dimensions fort modestes laissent planer un sérieux doute sur son éventuel rôle de monolithe-borne pastorale... à noter qu'à une quinzaine de mètres au Nord, il semble que l'on puisse évoquer la présence d'un cromlech de 6 mètres de diamètre, dont 6 pierres sont visibles.

Historique

Nous avions repéré cette pierre dès le début de nos prospections dans cette montagne en 1970, et ne l'avions pas retenue ; [L. Millan San Emeterio]{.creator} (San Sebastian) a de nouveau attiré notre attention sur cette pierre en [1985]{.date}, aussi avons-nous préféré la publier ici plutôt que de commettre une éventuelle omission.

### [Bidarray]{.spatial} - [Zutarria]{.coverage} - [Pierre couchée]{.type}

Localisation

Altitude : 813m.

Cette pierre gît sur un sol incliné vers l'Ouest, à plus de 300m au Nord de la croix qui est plantée à l'Ouest du GR 10, et à une dizaine de mètres à l'Est du tracé de ce même GR 10.

Description

Bloc de [grès]{.subject} [triasique]{.subject} parallélépipédique allongé selon un axe Nord-Sud, mesurant 2,90m de long, 0,60m à sa base Nord et 0,70m dans sa plus grande largeur ; l'épaisseur moyenne est de 0,25m environ.

L'extrémité Sud apparaît avoir été taillée en pointe, et l'ensemble de cette pierre semble présenter des traces d'épannelage.

Historique

Pierre découverte par [J.M. Lecuona Guelbenzu]{.creator} en [octobre 2010]{.date}.
