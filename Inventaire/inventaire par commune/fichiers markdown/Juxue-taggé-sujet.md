# Juxue

### [Juxue]{.spatial} - [Méhalzu]{.coverage} 3 - [Tumulus]{.type}

Localisation

Altitude : 641m.

Il est situé à 200m environ à l'Est Nord-Est du sommet de Méhalzu.

Description

Tumulus circulaire de faible hauteur (0,30m), et de 4,50m de diamètre. Une trentaine de petites plaquettes calcaires délimitent sa périphérie, et sont particulièrement visibles dans sa partie Nord.

Ce qui nous semble particulièrement remarquable, c'est que ce modeste monument, qui évoque un petit tumulus à [incinération]{.subject}, a été construit sur le premier remblai de défense de l'enceinte de Méhalzu, qui lui est donc antérieur. Ce remblai à grand axe Nord-Sud barre (avec un second, plus haut et plus discret), toute la partie Est de l'enceinte de Méhalzu, la seule dont la faible pente permettrait un assaut plus aisé que sur les autres flancs, beaucoup plus pentus.

Historique

Tumulus découvert par [F. Meyrat]{.creator} en [juin 2015]{.date}.

### [Juxue]{.spatial} - [Oxaraniako Borda]{.coverage} Ouest - [Tumulus]{.type} 

Ces monuments sont la suite de la description faite des 9 autres tumulus (numérotés de 1 à 9) au chapitre de la commune de Pagolle : *Oxaraniako Borda Est (ou Méhalzu ) - Tumulus*.

Ces 7 tumulus ont été découverts par [Meyrat F.]{.creator} en [novembre 2015]{.date}.

- *Tumulus n°10* - Altitude : 472m.

    Tumulus mixte situé à 70m à l'Ouest du n°1 de la description des tumulus d'Oxaraniako borda Est. Il mesure 7m x 6m et 0,40m de haut.

- *Tumulus n°11* - Altitude : 460m.

    Tumulus mixte situé à 45m au Nord-Ouest du n°10 ; il mesure 7m x 6m et 0,30m de haut.

- *Tumulus n°12* - Altitude : 465m.

    Tumulus mixte circulaire, situé à 50m au Nord-Ouest du n°11 ; 5m de diamètre et 0,30m de haut.

- *Tumulus n°13* - Altitude : 456m.

    Tumulus circulaire mixte, situé à 20m à l'Ouest Nord-Ouest du n°12. Diamètre de 6m et 0,40m de haut.

- *Tumulus n°14* - Altitude : 451m.

    Tumulus circulaire mixte situé à 15m au Nord-Est du n°13 ; 5m de diamètre et 0,30m de haut.

- *Tumulus n°15* - Altitude : 445m.

    Tumulus mixte, situé à 18m au Nord Nord-Ouest du n°14 ; mesure 9m x 6m, à grand axe Nord-Sud, et 0,30m de haut.

- *Tumulus n°16* - Altitude : 450m.

    Tumulus mixte situé à 20m au Sud-Ouest du n°15 ; mesure 8m x6m et 0,40m de haut.
