# Larceveau-Arreau-Cibits

### [Larceveau-Arreau-Cibits]{.spatial} - [Arapidekoborda]{.coverage} 1 - [Tumulus]{.type}

Localisation

Altitude : 374m.

Monument situé à mi-pente de la croupe qui se détache au Sud-Est du mont Currutcheberry, sur un terrain en très légère pente Sud-Est, à 2m au Nord-Est d'une piste pastorale. Il est à 25m au Nord-Ouest d'un abreuvoir de construction récente.

Description

Tumulus circulaire pierreux de 10m de diamètre environ et 0,30m de haut environ. De nombreux petits blocs [calcaires]{.subject} sont visibles dans le secteur Sud-Est. Ce monument semble plus évoquer un tertre pierreux qu'un reste de tertre d'habitat.

Historique

Tumlus découvert par [Meyrat F.]{.creator} en [mars 2016]{.date}.

### [Larceveau-Arreau-Cibits]{.spatial} - [Arapidekoborda]{.coverage} 2 - [Tumulus]{.type}

Localisation

Altitude : 368m.

Il est situé à 2m au Sud-Est de *Arapidekoborda 1 - Tumulus*.

Description

Tumulus circulaire pierreux de 5m de diamètre, 0,20m de haut. Un maximum de petits blocs [calcaires]{.subject} est visible dans le secteur Sud-Est. Là encore il semble qu'on soit devant un Tumulus plutôt qu'un terre d'habitat.

Historique

Tumulus découvert par [F. Meyrat]{.creator} en [mars 2016]{.date}.
