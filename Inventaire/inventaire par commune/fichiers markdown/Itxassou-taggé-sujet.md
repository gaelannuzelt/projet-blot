# Itxassou

### [Itxassou]{.spatial} - [Ane (Col de l')]{.coverage} - [Monolithe]{.type}

Localisation

Altitude : 590m.

Il est à environ une centaine de mètres à l'Est Nord-Est de *Âne (Col de l') - Dolmen*, et à trois mètres au Nord-Ouest de la piste qui se rend à Arluxetta.

Description

Belle dalle de [grés]{.subject} rose gisant sur le sol, de forme grossièrement triangulaire, à grand axe orienté Nord-Est Sud-Ouest. Elle mesure 1,79m de long, 1,04m à sa base et 0,20m d'épaisseur. Tout son bord Nord-Ouest présente des traces d'épannelage.

Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2010]{.date}.

### [Itxassou]{.spatial} - [Ane (Col de l')]{.coverage} - [Tumulus]{.type}

Il existe à 2 mètres au Nord Nord-Ouest de *Ane (Col de l') - Dolmen* un tumulus de terre mesurant 5m de diamètre et 0,30m de haut.

Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2010]{.date}.

### [Itxassou]{.spatial} - [Ane (Col de l')]{.coverage} - [Dolmen]{.type}

Localisation

Altitude : 585m.

Monument situé à l'extrémité Sud d'un vaste espace de gazon vert, où ne poussent pas les fougères et à la confluence de plusieurs pistes pastorales montant au flanc Est de l'Artzamendi, dont deux le contournent en son secteur Ouest.

Description

On note un tumulus de terre de 6m de diamètre et 0,40m de haut.

Au centre, apparaît une grande dalle de [grés]{.subject} [triasique]{.subject}, inclinée vers le Nord, orientée Est Sud-Est Ouest Nord-Ouest, mesurant 1,38m de long, 0,38m de hauteur apparente, et 0,14m d'épaisseur ; toute sa périphérie visible présente des traces d'épannelage.

Dans le secteur Sud du tumulus une dizaine de pierres, disposées en demi-cercle, semblent bien pouvoir être considérées comme les vestiges d'un péristalithe. Leurs dimensions sont variables, allant de 0,82m de long pour la plus grande à 0,42m pour la plus petite.

Historique

Monument découvert en [octobre 2010]{.date} par [Currutchet J.]{.creator} et [Régnier J.]{.creator}.

### [Itxassou]{.spatial} - [Ane (Col de l')]{.coverage} - [Dalle couchée]{.type} ([?]{.douteux})

Localisation

Altitude : 600m.

Elle se trouve à 1,60m à l'Ouest du monolithe décrit ci-dessus sous le nom de *Âne (Col de l') - Monolithe*.

Description

Elle mesure 1,40m dans son axe Nord-Ouest Sud-Est, et 1,10m selon l'axe Sud-Ouest Nord-Est. Son épaisseur varie de 0,12m à 0,30m suivant les endroits. Des traces d'épannelage semblent pouvoir être notées tout le long du bord Sud-Est de cette dalle.

Historique

Pierre découverte en [février 2011]{.date} par [F. Meyrat]{.creator}.

### [Itxassou]{.spatial} - [Apalaenea]{.coverage} - [Cromlech]{.type}

Localisation

A{.date}ltitude : 492m.

Il est situé à l'extrémité Nord du Plateau Vert et à 8m à l'Est d'un filon rocheux nettement visible.

Description

Cromlech de 7m de diamètre constitué d'une dizaine de dalles plus ou moins grandes souvent au ras du sol ; cependant, la plus grande, en secteur Ouest atteint 0,60m de long et 0,45m de haut, ainsi qu'une autre en secteur Sud-Est, mesurant 0,65m de long et 0,25m de haut. Trois autres dalles apparaissent en superficie à l'intérieur du cercle.

Historique

Monument découvert par [A. Martinez]{.creator} en [février 2013]{.date}.

### [Itxassou]{.spatial} - [Arimaluchenekoborda]{.coverage} - [Monolithe]{.type}

Localisation

Altitude : 560m.

Monolithe gisant sur la gauche du chemin qui descend vers la borde du même nom.

Description

Belle dalle de [grés]{.subject} rose de forme parallélépipédique, à grand axe Est-Ouest mesurant 2,40m. Sa partie la plus large mesure 1,60m, et ses deux extrémités Est et Ouest, 1,30m ; son épaisseur peut atteindre de 0,24m à 0,30m. Il semble y avoir des traces d'épannelage aux bords Ouest et Nord.

Historique

Monolithe découvert par [J. Blot]{.creator} en [mars 2011]{.date}.

### [Itxassou]{.spatial} - [Arluxeta]{.coverage} - [Tertres d'habitat]{.type}

Localisation

Altitude : 557m.

Les 14 tertres sont situés à l'extrémité du contrefort Nord-Est de l'Artzamendi qui domine (de loin...) le Pas de Roland ; ils sont au nombre de 14, répartis de part et d'autre d'un petit ruisseau orienté Sud-Ouest Nord-Est, dans le sens de la pente. Une borde en ruine entourée d'arbres est située à une quarantaine de mètres au Sud-Est.

Description

Ils sont érigés sur un terrain en légère pente vers le Nord-Est, leur diamètre est, en moyenne, de 10 à 11m et leur hauteur de 0,50m.

- *TH n°1* : coordonnées ci-dessus. Il est à 13m au Sud-Ouest d'un amas de rochers bien visibles, et à l' Ouest du ruisseau, sur sa rive gauche,comme les autres tertres d'habitat jusqu'au n°8 inclus. Du TH 9 au TH 14, ils sont à l'Est de ce ruisseau. Il mesure 11m x 8m et 0,30m de haut.

- *TH n°2* : à 15m au Sud du précédent. Mesure 10m x 7m et 0,30m de haut.

- *TH n°3* : à 18m au Sud Sud-Ouest du précédent. Mesure 10m x 8m et 0,10m de haut.

- *TH n°4* : à 18m au Sud Sud-Ouest du précédent. Mesure 6m x 6m et 0,30m de haut.

- *TH n°5* : à 18m au Sud Sud-Ouest du précédent. Mesure 8m x 6m et 0,10m de haut.

- *TH n°6* : à 2m au Sud Sud-Ouest du précédent. Mesure 11m x 6m et 0,60m de haut ; légère dépression à son sommet.

- *TH n°7* : à 8m au Sud Sud-Ouest du précédent. Mesure 10m x 8m et 0,40m de haut ; légère dépression au sommet.

- *TH n°8* : à 6m au Sud-Ouest du précédent. Mesure10m x 8m et 0,60m de haut. Le dernier TH à l'Est du ruisseau. Légère dépression au sommet avec 4 pierres visibles.

- *TH n°9* : à 8m à l'Est du précédent, à l'Ouest du ruisseau. Mesure 10m x 8m et 0,30m de haut.

- *TH n°10* : à 4m au Nord Nord-Est du précédent. Mesure 10m x 8m et 0,30m de haut.

- *TH n°11* : tangent au précédent. Mesure 11m x 8m et 0,50m de haut ; légère dépression au sommet.

- *TH n°12* : à 8m à l'Est Sud-Est du précédent. Mesure 9m x 8m et 0,60m de haut ; légère dépression au sommet.

- *TH n°13* : à 4m au Nord du n° 11. Mesure 8m x 6m et 0,30m de haut.

- *TH n°14* : à 2m au Nord Nord-Est du précédent. Mesure 11m x 8m et 0,50m de haut.

Historique

Cet ensemble de tertres d'habitat a été découvert par [Blot J.]{.creator} en [novembre 1974]{.date} et retrouvé et étudié par [Meyrat F.]{.creator} en [février 2014]{.date}.

### [Itxassou]{.spatial} - [Arrokagaray]{.coverage} - [Dolmen]{.type}

Localisation

Carte 1345 Ouest Cambo-les-Bains.

Altitude : 319m.

Il est situé au Sud du chemin qui parcourt le plateau presque au sommet du mont Arrokagarray, à 30m à l'Ouest d'un volumineux amas rocheux.

Description

La piste empiète sur la périphérie Nord-Ouest du tumulus pierreux qui mesure environ 10m de diamètre, et 0,80m de haut. La [chambre funéraire]{.subject}, orientée Ouest Sud-Ouest, Est Nord-Est, mesure 1,50m de long, 0,50m de large et 0,85m de haut. Elle est délimitée par deux grandes dalles de [grès]{.subject} : une au Sud, presque verticale, de 1,40m de long et 0,85m de haut ; l'autre au Nord, de mêmes dimensions semble-t-il, mais presque couchée, et en partie enfouie sous la terre. Pas de table visible.

Historique

Dolmen découvert en [avril 1972]{.date}. Les dalles centrales ont depuis disparu (2004), détruites par la confection d'une prairie artificielle.

### [Itxassou]{.spatial} - [Arrokagarray]{.coverage} 1 - [Tumulus]{.type}

Localisation

À environ 250m à l'Ouest de *Arrokagaray - Dolmen*, sur le même plateau, à environ une cinquantaine de mètres avant la dernière montée vers le sommet.

Description

On note un amas de volumineux blocs rocheux qui a été déposé, relativement récemment, à cet endroit, en vrac, avec un engin de terrassement. Il semble bien, toutefois, que ce dépôt ait été effectué sur un substrat tumulaire pré-existant, de 20m de diamètre et d'une hauteur difficile à apprécier maintenant, mais qui devait avoisiner 0,50m environ. Ce tumulus est formé de petits blocs rocheux de la taille approximative d'un pavé, dissimulés sous la couche herbeuse très drue.

Historique

Tumulus découvert en [décembre 2003]{.date}.

### [Itxassou]{.spatial} - [Arrokagaray]{.coverage} 2 - [Tumulus]{.type}

Localisation

Carte 1345 Ouest Cambo-les-Bains.

Altitude : 850m.

Il est sur le sommet plat du mont Arrokagaray, à environ 250m à l'Est du point culminant et à l'endroit où s'amorce la descente vers le plateau où sont érigés *Arrokagarray 1 - Tumulus* et *Arrokagarray 2 - Tumulus*.

Description

Tumulus mixte de forme ovale, à grand axe Est-Ouest, mesurant 22m x 16m et près de 2m de haut. Des blocs de pierre sont particulièrement visibles en son quart Sud Sud-Est.

Historique

Tumulus découvert en [avril 1972]{.date}.

### [Itxassou]{.spatial} - [Artzamendi]{.coverage} 1 - [Monolithe]{.type}

Localisation

Altitude : 367m.

Monolithe situé en rupture de pente, au bord Sud du vaste plateau qui s'étend au Sud-Est et en-dessous du sommet de l'Artzamendi. Situé sur un terrain en pente douce vers le Sud-Ouest.

Description

Très belle dalle de [grès]{.subject} [triasique]{.subject}, allongée sur le sol selon un axe Nord-Ouest Sud-Est, de forme rectangulaire à sommet Sud taillé en pointe, l'ensemble étant assez bien symétrique par rapport à l'axe cité. Elle mesure 3,50m de long, 1,02m dans sa plus grande largeur, et présente une épaisseur moyenne de 0,22m. On note de très nombreuses traces d'épannelage sur la quasi-totalité de son pourtour.

Historique

Monolithe trouvé par [Blot J.]{.creator} en [juin 2016]{.date}.

### [Itxassou]{.spatial} - [Artzamendi]{.coverage} 2 - [Monolithe]{.type}

Localisation

Altitude : 844m.

Situé sur le même plateau au Sud-Est et en-dessous du mont Artzamendi que *Artzamendi 1 - Monolithe*, mais de façon plus centrale, bien visible à quelques mètres au Sud d'une piste tracée par le passage répété du bétail et des hommes circulant selon un axe Est-Ouest.

Ce monolithe est situé à côté (0,15m) et à l'Est d'une ébauche de meule taillée dans du [grès]{.subject} [triasique]{.subject} (1,43m de diamètre et 0,30m d'épaisseur.).

Description

Monolithe de [grès]{.subject} [triasique]{.subject} parallélépipédique, couché sur le sol selon un axe Nord-Sud, pratiquement rectangulaire à la coupe.

Il mesure 2,70m de long, 0,70m de large en moyenne et 0,30m d'épaisseur en moyenne ; son pourtour présente de nombreuses traces de régularisation. Sa présence à côté de l'ébauche de meule nous incite à lui donner un âge relativement récent...

Historique

Monolithe trouvé par [Blot J.]{.creator} en [juin 2016]{.date}.

### [Itxassou]{.spatial} - [Artzamendi]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 914m.

On peut le voir à 4m à l'Ouest du dernier et plus petit mur érigé près de radôme.

Description

Petit tumulus pierreux circulaire en forme de galette aplatie, mesurant 1,90m à 2m de diamètre et 0,25m de haut. Une quarantaine de petits blocs de [grès]{.subject} [triasique]{.subject} sont visibles en surface. Monument douteux.

Historique

Monument découvert par [Blot J.]{.creator} en [juin 2016]{.date}.

### [Itxassou]{.spatial} - [Atharri]{.coverage} - [Tertre d'habitat]{.type}

Localisation

Altitude : 371m.

Il est situé à l'amorce du versant Nord du point culminant de la colline Atharri.

Description

Beau tertre de forme ovale, à grand axe Est-Ouest, et dissymétrique (1m dans sa plus grande hauteur au Nord), mesurant 15m de long et une dizaine de large. Il paraît seul bien que certains reliefs soient visibles à quelque distance au Sud-Est.

Historique

Monument découvert par [Blot J.]{.creator} en [novembre 2011]{.date}.

### [Itxassou]{.spatial} - [Atharri]{.coverage} 1 - [Tumulus]{.type}

Localisation

Altitude : 355m.

Le monument est situé à environ 350m à l'Est du col de Legarre, tangent au Sud de la piste.

Description

Il s'agit d'un tumulus circulaire de 5,30m de diamètre, et quelques centimètres de haut, constitué de nombreuses pierres de petites dimensions, profondément enfouies dans le sol. On distingue parfaitement la quasi-totalité de sa périphérie, sauf le tiers Sud Sud-Est qui est recouvert, ainsi que la partie centrale du monument, par un amoncellement de pierrailles mobiles qui ont été déversées dessus à l'occasion d'aménagements relativement récents de la piste et de ses environs. Est-ce un tumulus dolménique ou un tumulus simple ? Au vu des dimensions nous opterions plus volontiers pour la seconde hypothèse.

Historique

Monument découvert par [J. Blot]{.creator} en [avril 2010]{.date}.

### [Itxassou]{.spatial} - [Atharri]{.coverage} 2 - [Tumulus]{.type}

Localisation

Altitude : 360m.

On le trouve à environ 80m plus à l'Est de *Atharri 1 - Tumulus*, légèrement en surplomb de la piste au moment où elle aborde le plateau, cachée, à gauche, par des touffes de touyas.

Description

Comme précédemment, le monument est en grande partie recouvert par des apports récents de pierraille ; néanmoins, on distingue parfaitement la moitié Nord de la périphérie du tumulus circulaire originel sous-jacent, de 4,40m de diamètre et quelques centimètres de haut. Là encore, nous optons pour un tumulus simple. À noter, juste à côté un autre important amas de pierraille dont il est impossible de dire s'il recouvre ou non un autre tumulus.

Historique

Monument découvert en [avril 2010]{.date} par [F. Meyrat]{.creator}.

### [Itxassou]{.spatial} - [Atharri]{.coverage} 3 - [Tumulus]{.type}

Localisation

Il est situé à 15m au Sud-Ouest de *Atharri - Tertre d'habitat*, au point sommital de la ligne de crète.

Description

Ce tumulus a été en grande partie arasé lors de la création de la prairie artificielle où il se situe actuellement ; on peut toutefois encore distinguer son relief (modeste, de quelques centimètres) et sa périphérie, donc son diamètre, soit 6m environ actuellement ; au centre se voit une légère dépression d'une trentaine de centimètres de diamètre.

Historique

Monument découvert par [Blot J.]{.creator} en [novembre 2011]{.date}.

### [Itxassou]{.spatial} - [Ezuretta]{.coverage} - [Cromlechs]{.type}

On peut décrire 7 cromlechs (+1...) au col d'Ezuretta situé au Sud du mont Ourezti :

- *Ezurreta 1 - Cromlech*

    Altitude : 690m.

- *Ezuretta 1 bis - Cromlech*

    Il est tangent au Sud-Ouest du n°1, avec lequel il a 3 témoins en commun et 6 autres pierres extérieures : ces 9 pierres semblent délimiter un cercle d'environ 1,50m de diamètre. Monument douteux ? Découvert par J. Blot en [avril 2013]{.date}.

- *Ezuretta 2 - Cromlech*

    Il a été décrit par Blot J. [@blotInventaireMonumentsProtohistoriques2009].

    Il est situé à 4m à l'Ouest du n°1.

- *Ezuretta 3 - Cromlech*

    Il a été décrit par Blot J. [@blotInventaireMonumentsProtohistoriques2009, p.8-9].

    Le cromlech n°3 est à 15m au Sud-Ouest du n°2 et est traversé par la piste.

Parmi les 4 cromlechs suivants, 3 ont été découverts ces dernières années par le [groupe Hilharriak]{.creator} et 1 par [J. Blot]{.creator}, sans qu'il soit bien aisé de préciser qui est l'inventeur de chaque monument, ce qui n'a d'ailleurs qu'une importance très relative !

- *Ezuretta 4 - Cromlech*

    - Localisation

        Situé à 2m au Sud-Ouest du n° 3 et à l'Ouest de la piste qui traverse le n°3.

    - Description

        Huit pierres bien visibles, de calibres variés, délimitent un cercle de 4m de diamètre.

- *Ezuretta 5 - Cromlech*

    Il est situé à 9m au Sud-Est du n°4 ; six pierres, surtout visibles dans la moitié Est, délimitent un cercle de 5,40m de diamètre.

- *Ezuretta 6 - Cromlech*

    Situé à 15m au Sud Sud-Ouest du n°5. Il mesure 2m de diamètre et 8 pierres, au ras du sol, en délimitent la périphérie.

- *Ezuretta 7 - Cromlech (?)*

    Il est à 6m au Sud du n°6 et 5 pierres, peu visibles délimitent un cercle de 4,70m de diamètre.

    Monument douteux.

### [Itxassou]{.spatial} - [Ezuretta]{.coverage} 2 - [Cromlech]{.type}

Localisation

Carte 1345 Ouest Cambo-les-Bains.

Altitude : 620m.

On le trouve au flanc Sud du mont Ourezti - lui-même au Sud du Mondarrain - et à 100m au Nord du col Ezuretta.

Il est situé sur un sol en légère pente vers le Sud, à 4m à l'Ouest Sud de *Arrokagarray 1 - Tumulus*, qui est un très beau cercle de 6m de diamètre, formé de 17 pierres bien visibles, découvert par JM de Barandiaran en 1941 (ref)(Barandiaran JM. de,1949, p.205).

Description

Il semble que l'on puisse décrire 2 cercles concentriques : un cercle extérieur de 10m de diamètre, constitué de 12 pierres, et un cercle intérieur de 8m de diamètre délimité par 21 pierres. Les pierres constitutives de ce monument sont, dans l'ensemble, plus visibles dans le quart Sud-Ouest. Au centre apparaissent les sommets de 3 petites dalles.

Historique

Monument découvert en [octobre 1974]{.date}.

### [Itxassou]{.spatial} - [Ezuretta]{.coverage} 3 - [Cromlech]{.type}

Localisation

À 15m au Sud-Ouest de *Ezuretta 2 - Cromlech*, édifié sur un sol plat.

Description

Cromlech légèrement surélevé, mesurant 6,80m de diamètre, formé de 19 pierres ne dépassant le sol que de quelques centimètres.

Historique

Monument découvert en [octobre 1974]{.date}.

### [Itxassou]{.spatial} - [Iguzki]{.coverage} - [Monolithe]{.type}

Localisation

Au flanc Nord-Ouest du pic Iguzki. La route qui mène au col de Méatsé et aux émetteurs de l'Artzamendi, effectue un **Z** avec deux angles droits ; le monolithe est visible à environ 60m en contre-bas du deuxième virage en montant, érigé sur un terrain en pente. On notera la présence à environ 80m à l'Ouest, d'un point d'eau sous forme d'un petit ruisseau.

Altitude : 640m.

Description

Il s'agit d'une importante dalle de [grés]{.subject} rose, de forme grossièrement trapézoïdale et inclinée à 55° vers le Nord-Est. Sa base, enfouie dans le sol, est orientée Sud-Est Nord-Ouest. Il semble qu'elle ait pu être plus verticale dans le passé comme le suggère un bloc rocheux sur lequel elle prend appui au Sud-Est, et qui paraît s'être en partie brisé sous son poids. Les mensurations de ce monolithe sont les suivantes : 2,30m de hauteur, 2,80m à la base, épaisse elle-même de 0,80m ; l'épaisseur au sommet n'étant que de 0,52m. Il n'y a pas de traces d'épannelage. Il semble peu probable, compte tenu du contexte géologique et de ses dimensions, que cette dalle ait pu adopter de façon naturelle cette position sur la tranche. La situation de cette dalle à flanc de montagne n'est pas sans nous rappeler le *monolithe Iparla 1* ou ceux d'Athékaleun ou Gaztenbakarre (dans la Rhune).

Historique

Monument découvert [J. Blot]{.creator} en [1982]{.date}.

### [Itxassou]{.spatial} - [Iguzki]{.coverage} 3 - [Tumulus]{.type}

Localisation

Carte 1345 Ouest.

Altitude : 580m.

Il est situé à 20m au Sud-Ouest de *Ezuretta 2 - Cromlech*.

Description

Tumulus mixte de terre et de blocs calcaire de [grès]{.subject} local, mesurant 6,50m de diamètre et 0,40m de haut. Au centre est visible une dépression de 2m de long et un de large ; trace d'une fouille ancienne ?

Historique

Monument découvert en [janvier 1973]{.date}.

### [Itxassou]{.spatial} - [Iguzki]{.coverage} 4 - [Tumulus]{.type}

Localisation

Il est à 3m au Sud-Ouest de *Iguzki 3 - Tumulus*.

Description

Tumulus mixte, de terre et de pierres, de 6m à 7m de diamètre et 0,90m de haut. Il semble délimité à sa périphérie par une couronne de pierres calcaires blanches particulièrement visibles en secteur Ouest. Au centre il existe un petit amas pierreux, aux éléments assez mobiles.

Historique

Monument découvert en [janvier 1973]{.date}.

### [Itxassou]{.spatial} - [Iguzki]{.coverage} 5 - [Tumulus]{.type}

Localisation

Carte 1345 Ouest.

Altitude : 403m.

Il est sur un replat dominant à une cinquantaine de mètres à l'Est la route menant aux émetteurs de l'Artzamendi, et à 10m à l'Ouest d'un double pylône électrique ; sol légèrement en pente vers l'Ouest.

Description

Tumulus de terre de 5,50m de diamètre et 0,80m de haut ; il existe au centre une dépression de 2m de diamètre et 0,30m de profondeur : fouille ancienne ?

N.B. : [Nous]{.creator} avons publié ce monument, (situé sur la commune d'Itxassou), en 1973 dans [@blotNouveauxVestigesMegalithiques1973 p10].

Historique

Monument découvert en [janvier 1973]{.date}.

### [Itxassou]{.spatial} - [Iguski]{.coverage} 6 - [Cromlech]{.type}

Localisation

Altitude : 570m.

Il se trouve sur un élargissement du terrain sur lequel passe le sentier de Grande Randonnée, lors de son contour des prairies artificielles situées à la partie inférieure du flanc Ouest du Pic Iguski.

Description

Monument difficile à voir (douteux ?), érigé sur terrain plat. On note un cercle de 5m de diamètre environ, en très léger relief délimité par de nombreuses pierres de dimensions variables, au ras du sol, plus abondantes dans le secteur Nord-Ouest.

Historique

Monument découvert par [Blot J.]{.creator} en [novembre 2011]{.date}.

### [Itxassou]{.spatial} - [Iguski]{.coverage} 7 - [Cromlech]{.type}

Localisation

Altitude : 589m.

Il est situé au milieu d'une petite croupe qui s'étend à gauche de la route qui monte au col de Méatsé (Artzamendi).

Description

Une dizaine de belles dalles de [grès]{.subject} rose, dont certaines atteignent 1,10m x 1m, délimitent approximativement un cercle de 3m à 3,50m de diamètre, dont le centre excavé traduit l'existence d'une fouille ancienne. Celle-ci a de plus bouleversé la structure du cercle, dont les témoins ont été plus ou moins déplacés de leur implantation d'origine, ou même brisés, d'où l'irrégularité du cercle.

Historique

Cromlech découvert par [J. Blot]{.creator} en [mars 2016]{.date}.

### [Itxassou]{.spatial} - [Iguskiegi]{.coverage} - [Monolithe]{.type}

Localisation

Altitude : 545m.

Cette pierre est située au col d'Iguskiegi, en bordure de piste.

Description

Cette importante dalle de [grés]{.subject} rose est actuellement verticale ; de forme triangulaire elle mesure 1,83m de haut, 2,30m à sa base et 0,30m d'épaisseur. Elle est orientée suivant un axe Nord-Sud.

Historique

Ce monument m'a été signalé par [A. Martinez Manteca]{.creator} en [février 2010]{.date}. Toutefois, mon ami [F. Meyrat]{.creator} avait déjà noté la présence de cette dalle, à cet endroit, en [2000]{.date} ; mais elle était couchée au sol, et affectait une forme plutôt rectangulaire ; on notait que l'angle supérieur gauche était séparé de la dalle par une très nette fracture, (très probablement naturelle) tout en étant resté en place. La mise (remise ?) en place, verticale de cette pierre, effectuée depuis, a laissé au sol le fragment détaché, d'où son aspect actuel. Bien qu'il s'agisse d'un travail récent, on ne peut manquer de penser que cette grande dalle, gisant à cet endroit (un très beau carrefour de pistes pastorales), avait pu être disposée là.... il y a fort longtemps, et que l'on n'a fait que « remettre les choses en place ».

### [Itxassou]{.spatial} - [Iguzkimendi]{.coverage} - [Monolithe]{.type} ([?]{.douteux})

Localisation

Situé en Navarre (côté espagnol), sur le flanc Sud du mont Iguzki.

Altitude : 757m.

On y accède en empruntant le sentier qui part vers le Sud-Ouest du col de Méatsé. Ce monolithe est sur un replat à une quarantaine de mètres au-dessus du sentier.

Description

Monolithe de [grès]{.subject} [triasique]{.subject} de forme oblongue, couché au sol, orienté Nord-Sud, avec pointe au Nord et partie renflée au Sud. Il mesure 3,70m de long, 1,07m de haut et possède une largeur variant entre 1,40m au milieu et 1,70m vers l'arrière où il est le plus épais. 

Sa particularité, outre sa proximité avec la frontière, est de présenter sur sa face Ouest 4 traces verticales équidistantes et parallèles pour 3 d'entre elles. 
- La n°1 (la plus au Nord), est verticale, mesure 0,86m de long, est large de 0,06m ; 
- la n°2, plus au Sud, lui est parallèle et mesure 0,87m de long et 0,06m de large ; 
- la troisième, elle aussi parallèle, ne mesure que 0,54m de long et 0,05m de large. 
- Enfin la quatrième, légèrement oblique à droite, mesure 0,64m de long. Ces traces sont-elles le résultat d'une action humaine (laquelle ?) ou de la nature (érosion ?).

Historique

Ce monument nous a été signalé par le [groupe Herri Harriak]{.creator} en [décembre 2014]{.date}.

### [Itxassou]{.spatial} - [Itxassou]{.coverage} 1 - [Tumulus]{.type}

Localisation

Carte IGN 1345 Ouest Cambo-les-Bains.

Altitude : 85m.

On trouve 2 tumuli dans la propriété Hirriberia, sous les arbres, à l'Est de la route Cambo-Louhossoa.

Description

Le tumulus n°1 est formé de terre et de galets, possède un diamètre de 25m et une hauteur de 0,80m. Un passage de bulldozer l'a considérablement aplani depuis la première observation.

Historique

Monument découvert en [mars 1968]{.date}.

### [Itxassou]{.spatial} - [Itxassou]{.coverage} 2 - [Tumulus]{.type}

Localisation

Carte IGN 1345 Ouest Cambo-les-Bains.

Il est tangent au Nord-Ouest de *Itxassou 1 - Tumulus*.

Description

Constitué lui aussi de terre et de galets, il a été lui aussi très remanié par le passage d'un bulldozer ; il mesurait initialement 12m de diamètre et 0,70m de haut.

Historique

Monument découvert en [mars 1968]{.date}.

### [Itxassou]{.spatial} - [Itxassou]{.coverage} 3 - [Tumulus]{.type}

Localisation

Carte IGN 1345 Ouest Cambo-les-Bains.

À une cinquantaine de mètres au Nord-Ouest, dans un champ de l'autre côté de la route.

Description

Ce tumulus a été rasé par la préparation d'un lotissement. Nous n'avons aucune notion de ses dimensions, et n'avons pu que constater, sur le sol, une nappe circulaire de galets, d'environ une vingtaine de mètres de diamètre, et des traces de charbons de bois au centre ; pas de fragments de céramique visibles.

Historique

Monument découvert en [mars 1968]{.date}.

### [Itxassou]{.spatial} - [Itxassou]{.coverage} 4 - [Tumulus]{.type}

Localisation

Carte IGN 1345 Ouest Cambo-les-Bains.

Altitude : 60m.

Il est situé dans un pré, à 50m à l'Est de l'ancienne route qui va de Cambo à Louhossoa, en face de l'embranchement qui conduit à l'église d'Itxassou, au lieu-dit «les cinq cantons» et au Sud-Ouest de la nouvelle route.

Description

Tumulus terreux de 16m de diamètre et 0,80m de hauteur ; [monument douteux]{.douteux}.

Historique

Monument découvert en [mars 1968]{.date}.

### [Itxassou]{.spatial} - [Legarreko]{.coverage} Lepoa - [Pierre couchée]{.type}

Localisation

Altitude : 345m.

On peut voir cette pierre dans le jardin de la maison qui jouxte l'angle de la route au niveau du col de Legarre, à 6m au Nord du portail, repoussée contre la haie.

Description

Ce bloc, de gneiss semble-t-il, couché au sol, affecte la forme d'un parallélépipède rectangle de 2m de long, 0,98m de large à sa base, 0,48m au sommet qui est tronqué, et 0,45m d'épaisseur.

Cette pierre est intéressante par sa situation dans le col et le fait qu'elle semble présenter des traces d'épannelage sur toute sa face Nord.

Historique

Pierre découverte par [F. Meyrat]{.creator} en [avril 2010]{.date}.

### [Itxassou]{.spatial} - Les monuments du [col de Méatsé (Artzamendi)]{.coverage}

Localisation

Carte 1345 Ouest Cambo-les-Bains.

Altitude : 716m.

Ce col, situé entre le mont Artzamendi au Nord-Est, et le mont Iguski au Sud-Ouest, est le siège d'une importante nécropole.

#### Histoire et description sommaire

Le site de Méatsé a été découvert par [J.M. De Barandiaran]{.creator}, en [1943]{.date}, qui signale [0barandiaranCronicaPrehistoriaAlduides1949] un dolmen (*dolmen de Iuskadi*) (fouillé en 1973) et 3 cromlechs au niveau du col (*groupe Méatséko-Lepoa*), avec, au Sud-Ouest de celui-ci, les 5 cercles du *groupe Iuskadi*, et au Nord-Est, le *groupe Méatseko-Bizkarra* avec 3 cercles, dont un contenant la borne frontière n°81 en son centre. Par la suite, ce col fit l'objet, sous la pression des circonstances, de nombreuses interventions qui permirent de mieux connaître le nombre des monuments et d'en préciser les architectures.

Dès 1970, nous remarquons que le *[cromlech]{.type} n°10* a été amputé de son tiers Nord-Ouest par le passage de la route, et que des éléments de son coffre central apparaissent dans la coupe, alors que son voisin immédiat, au Sud-Ouest, le *cromlech n°9*, est toujours intact.

Nous effectuons une fouille de sauvetage sur 2 [cistes]{.subject} (numéros 4 et 7 du plan), qui ne font que préciser leur structure, et sur le *cromlech n°1*, menacés par des engins de terrassement [@blotCromlechMeatseItxassou1970]. Ce dernier cercle mesure 5,50m environ de diamètre, et il est formé d'une alternance régulière de petits amas de dalles horizontales séparées par des dalles plantées de chant et de façon radiale. Au centre un volumineux coffre, à grand axe Nord-Ouest Sud-Est, formé d'une dalle mesurant 1,30m de long et 1,10m de large reposant sur 7 montants, dont le plus important est un bloc de [grès]{.subject} de 1,50m de long, disposé horizontalement au Nord-Ouest sur le sol préalablement décapé (comme cela a été la règle pour tous les monuments de cette nécropole). Six autres dalles de modestes dimensions complètent ce coffre. À part un semis de particules carbonées, réparti dans l'ensemble du monument, il n'y avait ni mobilier ni dépôts d'ossements humains calcinés.

En 1971 et 1973, les travaux sont poursuivis par Cl. Chauchat sur le n°1, [@chauchatNecropolePrehistoriqueCol1977 p. 323], dont il achève de dégager la totalité du péristalithe ainsi que sur le *cromlech n°2* tangent au précédent qui laisse apparaître une petite partie de sa couronne et son caisson central, à grand axe Ouest Nord-Ouest Est Sud-Est, constitué d'une dalle reposant sur 4 montants verticaux, et sans autre dépôt que des particules de charbons de bois qui ont pu être datées : (Ly 881) mesure d'âge : 2380+-130 (BP ), soit en date calibrée (BC) : 800-165. Le caisson n°5 est dégagé, mesurant environ 0,90m x 0,90m, formé d'une dalle horizontale reposant sur 5 petits montants verticaux ; pas de mobilier, ni charbons de bois, ni dépôts osseux. Le *cromlech n°6* a fait l'objet d'une fouille complète : une couronne de dalles de [grès]{.subject} empilées les unes sur les autres, inclinées vers l'extérieur, réalisant une murette de pierres sèches de 4m de diamètre et 0,50m de large, entoure un caisson central. Ce dernier est formé d'un petit cercle de 1,20m de diamètre constitué de petites dalles plantées obliquement s'appuyant sur une dalle de [couverture]{.subject} horizontale. Pas de mobilier ni de dépôts d'aucune sorte.

Le *cromlech n°7*, tangent au Sud-Ouest du précédent, n'a été que très partiellement dégagé, mettant au jour une petite portion d'un péristalithe mesurant environ 3m de diamètre, constitué de dalles empilées de façon désordonnée, et un coffre central rectangulaire de1,10m de long et 0,50m de large à grand axe orienté Nord-Est Sud-Ouest, avec une dalle de [couverture]{.subject} reposant sur 4 dalles verticales ; ni mobilier, ni dépôts.

Tous ces monuments, après leur fouille, ont été laissés à l'air libre, à la vue de tous et ils ont donc été considérablement dégradés dans les mois suivants, non seulement par les intempéries, mais en servant aussi bien de foyer de camping que de dépotoirs pour les promeneurs ou l'armée en manœuvre...

Devant un tel massacre du patrimoine archéologique, nous avons décidé, avec l'accord du Directeur de Antiquités Historiques d'Aquitaine, de procéder à l'avenir, après toute fouille, au recouvrement total des monuments.

En 1979, nous reprenons les fouilles de sauvetage, cette fois sur le monument contenant le *caisson n°5*, que nous fouillons totalement [@blotCromlechUniteMeatse1979]. Tout autour du caisson apparaît un véritable pavage de dalles disposées horizontalement, délimité à sa périphérie par une petite murette circulaire, continue, de 6m de diamètre et 1m de largeur, formée de nombreuses petites dalles de [grès]{.subject} sans ordre apparent ou soigneusement empilées et inclinées, suivant les endroits, rappelant la disposition constatée au cromlech n°6. À l'extérieur de cette murette gisaient sur le sol, à intervalles réguliers, de grandes dalles mesurant 0,50m x 1m et paraissant avoir été initialement plantées verticalement, comme l'était encore l'une d'entre elles ; la dénomination de tumulus-cromlech n°5 nous a paru dès lors totalement justifiée.

En 1992 et 1993 nous fouillons en totalité le *Cromlech n°8*, dont la très belle architecture a été partiellement endommagée par un engin de terrassement [@blotCromlechUniteMeatse1979 p.115]. La couronne périphérique mesure 4,30m de diamètre et elle est constituée de petits amas de dallettes horizontales, séparés les uns des autres par des dalles verticales disposées de façon radiale. Au centre se trouve un coffre dont la dalle de [couverture]{.subject} repose sur 7 montants verticaux ; de petites dallettes rendent ce réceptacle « étanche », tandis que de nombreuses autres dalles sont disposées en « pelure d'oignon » autour de ce coffre, plus à titre décoratif, semble-t-il, que de soutien. Aucun mobilier ni dépôts osseux, mais de nombreux petits amas de charbons de bois disséminés dans et hors le coffre central nous ont permis d'avoir une datation au 14C : Gif 9573 : mesure d'âge : 2960+-50 BP, soit en date calibrée (BC.). :1313-1004. Lors de la fouille nous avons pu constater la présence d'un cercle tangent au Sud Sud-Est (le futur n°11).

En 1994, nous intervenons sur le *Cromlech n°12* [@blotCromlechMeatse121996a p.167.], dégradé par le passage des tous-terrains, et dont la structure est, elle aussi, très élaborée : il mesure 5m de diamètre, et sa couronne externe, ou péristalithe, est formée de 2 types d'éléments : un cercle interne de petites dalles groupées en amas distincts mais tangents les uns aux autres, et un cercle externe, discontinu, de grandes dalles disposées tangentiellement aux précédentes. Au centre, un coffre à grand axe orienté Sud-Ouest Nord-Est, constitué d'une belle dalle de [couverture]{.subject} reposant sur 4 montants ; il est complètement stérile ; aucune datation n'a pu être effectuée. Disposés sur les dalles du péristalithe, nous avons recueilli un outil, façon «chopping-tool» qui a pu servir à épanneller les dalles de ce monument, et un galet de poudingue en forme d'œuf. Nous voyons, dans ces dépôts, et tout particulièrement dans le dernier, un geste symbolique par excellence car, pour beaucoup d'ethnologues, l'œuf est un symbole de la vie future, celle qu'il recèle en lui à l'état potentiel.

En 1994 encore, une prospection électrique (étude de la résistivité des sols), est effectuée par Mr Martinaud, à la recherche de monuments rendus invisibles par les colluvions issues des deux sommets encadrant le col. Des « anomalies électriques », (auxquelles nous attribuons des lettres majuscules), vont ainsi faire l'objet de sondages et parfois de fouilles plus complètes au cours des années suivantes, afin de déterminer si il s'agit de vestiges d'origine anthropique ou d'éléments naturels.

En 1995, nous effectuons ainsi des sondages [@blotRapportSondagesArcheologiques1995] dont un au voisinage immédiat du *cromlech n°8*, sur *C 11*, qui confirme bien l'existence d'un autre cercle, et aux points **A**, **X1**, **Q**, **U**. (voir plan). Le sondage au point **U** révèle une structure de dallettes empilées en position horizontale, l'ensemble formant un arc de cercle pouvant être interprété comme la fraction d'un cercle de pierres de 7m de diamètre environ. Au point **Q**, il existe une grande dalle de [couverture]{.subject} reposant en partie sur des montants verticaux, le tout très difficile à interpréter : on peut supposer qu'il y a 1 ou peut-être 2 caissons, dont l'un possède une grande dalle de [couverture]{.subject}, et une fraction de cercle de pierres faisant partie de cet ensemble, à l'évidence d'origine anthropique.

En 1996, nous effectuons la fouille complète du *cromlech n°11* [@blotBaratzCerclePierres2002, p.81], tangent au *n°8*, qui met en évidence un cercle de 4m environ de diamètre, une architecture semblable à celle de son voisin, et aussi soignée, avec amas de dallettes horizontales séparées par des dalles verticales ; deux galets de poudingue, en forme d'œuf, ont été recueillis entre et sur les dalles de ce péristalithe. Le caisson central, rectangulaire, à grand axe Nord Nord-Ouest, Sud Sud-Ouest mesurant 1,15m x 1m, n'est formé que par une dalle de [couverture]{.subject} reposant sur 4 montants verticaux, sans dallettes ou blocs complémentaires de calage. Quelques petits dépôts de charbons de bois avaient été effectués dans et hors le coffre central ; nous les avons recueillis pour datation. Résultat : Gif 10284 : mesure d'âge : 2705+- 75 (BP), soit en date calibrée (BC) : 1041-605. Il est donc plus récent que son voisin, ce que les modifications architecturales, au point de contact, avaient clairement évoqué.

Nous effectuons aussi des sondages [@blotRapportSondagesArcheologiques1996] aux points **A1**, **C**, **U5**, **T**, **S**, **P**. Le point **U5** révèle un coffre probable (centre d'un cromlech ?), avec des charbons de bois. En **S**, on note un volumineux bloc de [grès]{.subject} de 1,10m de large, de part et d'autre duquel s'appuient deux dalles verticales, avec présence de charbons de bois... pourrait-il s'agir d'un monolithe couché (?).

En 1997, les sondages [@blotCromlechMeatseCommune1997] portent sur les points **E**, **F**, **G**, **H**, **D**, **I**, **J**, **L**, et **L**. Les points **H** et **L** représentent deux cercles très probables. Le point **E** correspond au péristalithe du caisson du cromlech n°2, péristalithe déjà partiellement dégagé par Cl. Chauchat en 1973. On note une structure en dalles de [grès]{.subject} rose, présentant des traces d'épannelage, empilées les unes sur les autres d'une façon assez désordonnée, et sans dalles verticales ; le cercle ainsi délimité a 4,50m de diamètre.

Le point **F** correspond à l'environnement du *caisson n°4*, que nous avions fouillé en 1970. On dégage ainsi un amoncellement de dalles de [grès]{.subject} rose, disposées tout autour du caisson de manière plus ou moins désordonnée, le tout correspondant parfaitement à un tumulus pierreux, dès lors appelé *Tumulus n°4*.

En 1998, nous pratiquons des sondages étendus sur les deux derniers points cités [@blotSondagesArcheologiquesAu1998] :

Le point **H** correspond bien au *cromlech n°3*, dont on voit parfaitement le péristalithe en secteur Nord Nord-Est et Sud Sud-Ouest, du même type que celui du *cromlech n°8*, c'est à dire une alternance d'amas de petites dalles horizontales, séparées par des dalles verticales en position radiale ; ce cercle mesure 5m de diamètre. Le caisson central, mesurant 1,30m x 0,90m, à grand axe Est-Ouest ; il est constitué par une dalle de [couverture]{.subject} reposant sur 4 blocs parallélépipédiques, avec de nombreux petits blocs de calage ; pas de mobilier ni de fragments osseux calcinés, seulement un léger semis de particules de charbons de bois à l'intérieur du monument, mais qui n'ont pas pu permettre de datation.

Le point **L** met en évidence l'architecture d'un autre cercle avec son petit coffre central, monument que nous dénommons *cromlech n°13*. Le péristalithe, dont le diamètre atteint 4,90m, est, là encore, formé par des assemblage de dallettes horizontales séparées par d'autres verticalement disposées ; le caisson central, mesurant 1,10m x 0,75m, à grand axe Nord-Ouest Sud-Est, présente une dalle couvercle reposant sur 4 autres dalles verticales, sans petits blocs de calage, ni dépôts osseux ; par contre il existait un semis de particules carbonées à tous les niveaux du monument et du coffre ; la datation au 14 C nous donne : Gif 11091 : mesure d'âge: 2640+- 70 ans (BP), ce qui place ce monument à une époque plus récente que *Méatsé 8* et *Méatsé 11*, mais plus ancienne que *Méatsé 2* (voir aussi le tableau des datations en fin de texte). Enfin un petit galet de poudingue, de la forme et de la taille d'un œuf, avait été disposé entre péristalithe et coffre central, en secteur Est Sud-Est.

Ce bref bilan des travaux effectués à Méatsé permet de comprendre que nous ayons pu faire une certaine approche du symbolisme sous-jacent au rite d'incinération [@blotContributionEtudeCercles1995] et [@blotMessageArchitecturesProtohistoriques2003, p.45]. Tout d'abord, il est particulièrement perceptible dans le contraste entre la modicité des vestiges visibles au-dessus de la surface du sol - ceci même immédiatement après la construction du monument - et la complexité réelle de ces architectures. Par ailleurs, celles-ci sont très riches en témoins de gestes symboliques - que nous constatons, même sans pouvoir en expliquer les motivations - tels les semis de particules carbonées dans l'ensemble du monument, les dépôts de charbons de bois dans ou hors de la [ciste]{.subject} centrale, ou les dépôts de galets en forme d'œuf sur la couronne extérieure, tout ceci coexistant avec une absence de mobilier en général, et surtout avec l'absence (ou l'extrême discrétion) de restes humains. On a évoqué la possibilité que les restes osseux aient été détruits par l'acidité du sol. Il n'en est pas toujours ainsi, car les charbons de bois neutralisent cette acidité, et nous avons par exemple trouvé des fragments de côtes, éléments particulièrement fragiles, qui avaient été protégés par un environnement de charbons de bois (Errozaté) ; inversement, il nous est souvent arrivé de ne pas trouver de restes osseux au milieu d'abondants dépôts carbonés...

Cette absence, voulue, est la preuve que ces monuments funéraires sont plus des « cénotaphes », que des sépultures vraies (le cas unique du *tumulus-cromlech Millagate 4*, à Lecumberry, contenant la **totalité** des ossements incinéré d'un défunt, reste une exception tout à fait remarquable).

### [Itxassou]{.spatial} - [Méatsékobizkarra]{.coverage} 5 - [Cromlech]{.type}

Localisation

Carte 1345 Ouest.

Altitude : 750m.

Trois cromlechs du groupe Méatsékobizkarra ont déjà été décrits [@barandiaranContribucionEstudioCromlechs1949 p. 202] :

- Le *Cromlech n°1*, contenant la borne Frontière n°80 en son centre, est le plus spectaculaire.

- Le *Cromlech n°2*, est tangent à l'Ouest Sud-Ouest du n°1.

- Le *Cromlech n°3*, un tumulus-cromlech, est à 8m à l'Ouest du n° 1.

- Nous avons même publié en 1972 [@blotNouveauxVestigesMegalithiques1972c, p.20] un quatrième monument à 3m à l'Ouest Sud-Ouest du n°2.

- Le *Cromlech n°5* est situé à 38m à l'Ouest du n°1, sur un sol en très légère pente.

Description

Cercle bien visible de 3,70m de diamètre, délimité par 8 pierres.

Historique

Monument découvert en [septembre 1972]{.date}.

### [Itxassou]{.spatial} - [Mendittipiko Bizkarra]{.coverage} - [Monolithe]{.type} ([?]{.douteux})

Localisation

Altitude : 750m.

On le voit à gauche de la piste qui se rend à Mendittipiko lepoa.

Description

Volumineuse dalle couchée, de forme grossièrement triangulaire, orientée selon un axe Nord-Sud. Elle mesure 3m de long, 1,92m à sa base, et 0,40m d'épaisseur. Il semble que sa partie la plus étroite, son « sommet » qui mesure 1,12m de large, présente, semble-t-il, des traces d'épannelage.

Historique

Dalle découverte par [J. Blot]{.creator} en [mars 2010]{.date}.

### [Itxassou]{.spatial} - [Méatsekobizkarra]{.coverage} 2- [Monolithe]{.type}

Localisation

Altitude : 750m.

Le monolithe se trouve à une quinzaine de mètres au Nord de la piste qui descend vers Bidarray, à environ 130m après sa naissance de la route qui monte au sommet de l'Artzamendi (après le col de Méatsé).

Description

Il s'agit d'une grande dalle de [grés]{.subject} [triasique]{.subject}, gisant au sol, affectant la forme d'une pointe de lance losangique, à grand axe orienté Nord Nord-Ouest Sud Sud-Est, mesurant 4,20m de long, 1,40m de large et 0,25m d'épaisseur en moyenne. Sa surface supérieure est parfaitement lisse. Son bord Ouest présente des traces d'épannelage très nets ; on note en particulier un volumineux éclat enlevé à l'extrémité Sud de ce côté. Le bord Est se divise en un segment Sud-Est de 1,90 m de long et un autre, Nord-Est, de 2,50m de long.

Historique

Monument découvert par [Manteca Martinez A.]{.creator} en [mai 2010]{.date}.

### [Itxassou]{.spatial} - [Méatsékobizkarra]{.coverage} - [Pierre couchée]{.type}

Localisation

Altitude : 675m.

Elle est située à proximité de la BF 81.

Description

Pierre de forme allongée et plus ou moins pointue à ses extrémités, en partie enfouie dans le sol, selon un axe Est-Ouest. Une fois dégagée, elle mesure 2,70m de long ; sa face Sud mesure 0,77m de large - on note des traces d'épannelage très nettes à son extrémité Est et au bord inférieur de sa face Sud.

Historique

Pierre découverte par [Blot J.]{.creator} en [janvier 2012]{.date}.

### [Itxassou]{.spatial} - [Mondarrain]{.coverage} Nord 1 - [Tertre d'habitat]{.type}

Localisation

Altitude : 520m.

Il est situé sur le flanc Nord du Mondarrain et un peu au-dessus d'un petit col, à gauche et aux abords immédiats d'une piste qui monte vers le Mondarrain, partant de son flanc Est avant la maison Ithurrartia.

Description

Tertre asymétrique, de 4m de long, 3m de large et 1m de haut.

Historique

Monument découvert par [J. Blot]{.creator} en [avril 2013]{.date}.

### [Itxassou]{.spatial} - [Mondarrain]{.coverage} Nord 2 - [Tertre d'habitat]{.type}

Localisation

Il est situé à 38m au Nord et au-dessous de *Mondarrain Nord 1 - Tertre d'habitat*.

Altitude : 515m.

Description

Tertre de terre asymétrique, de forme oblonge de 6 à 8m de diamètre, moins élevé que *Mondarrain Nord 1 - Tertre d'habitat*.

Historique

Monument découvert par [J. Blot]{.creator} en [avril 2013]{.date}.

### [Itxassou]{.spatial} - [Mondarrain]{.coverage} Est (Groupe 1) - [Tertres d'habitat]{.type}

Cet ensemble de 6 tertres est situé au-dessus de la piste qui longe à l'horizontal le flanc Est du Mondarrain, sur un terrain en pente légère vers l'Est et irrigué par un petit riu.

- *TH n°1* : Altitude : 600m. Tertre de terre de faible hauteur de 4m de diamètre.

- *TH n°2* : situé à 1m à l'Est du précédent. Mesure 3,50m de diamètre ; faible hauteur.

- *TH n°3* : situé à 8m au Nord-Ouest du précédent. Mesure 3,50m de diamètre ; de faible hauteur.

- *TH n°4* : situé à 1m au Sud-Est du n° 2. Mêmes dimensions.

- *TH n°5* : situé à 10m à l'Est Sud-Est du n°4. Mesure 4m de diamètre.

- *TH n°6* : situé à 9m au Nord du précédent. Mesure 3,50m de diamètre.

### [Itxassou]{.spatial} - [Mondarrain]{.coverage} Est (Groupe 2) - [Tertres d'habitat]{.type}

- *TH n°1* : Situé au flanc Est du Mondarrain, plus haut que la piste qui le longe à l'horizontal. Altitude : 600m. Tertre mixte (pierres et terre) de 3,50m de diamètre et de faible hauteur.

- *TH n°2* : Situé à 7m à l'Est du précédent. Tertre mixte, sensiblement de mêmes dimensions que le précédent.

Historique

Tertres découverts par [Blot Jacques]{.creator} en [avril 2013]{.date}.

### [Itxassou]{.spatial} - [Mendittipiko Bizkarra]{.coverage} 2 - [Monolithe]{.type}

Localisation

Altitude : 869m.

Monolithe situé presque au sommet de l'Artzamendi, à gauche du dernier virage avant le sommet, où se trouve une petite aire de parking. De là part une piste pastorale vers le Sud-Ouest, sur un joli replat, et le monolithe se voit à 8m à droite de cette piste, à une quarantaine de mètres du point de départ.

Description

Dalle de [grès]{.subject} [triasique]{.subject} allongée sur le sol selon un axe Ouest Sud-Ouest Est Nord-Est, de forme parallélépipédique, mesurant 2,53m de long, 0,77m de large à son sommet Sud-Ouest, et 1,07m à sa base Nord-Est ; son épaisseur, appréciable à son bord Sud, est de 0,32m en moyenne.

Il semble qu'il y ait des traces d'épannelage sur ce bord Sud.

Historique

Monolithe découvert par [Blot J.]{.creator} en [octobre 2015]{.date}.

### [Itxassou]{.spatial} - [Mendittipiko Bizkarra]{.coverage} 3 - [Monolithe]{.type}

Localisation

Altitude : 850m.

Il se trouve tangent au Sud de la même piste pastorale que Mendittipiko Bizkarra 2 - Monolithe, qui se prolonge toujours vers le Sud-Ouest. Il gît à une quarantaine de mètres au Nord-Est d'un petit col où plonge cette piste.

Description

Dalle de [grès]{.subject} [triasique]{.subject} de forme approximativement triangulaire, à base Nord-Est.

Elle mesure 2,90m de long, 1,54m à sa base et 1,40m dans sa partie médiane. Son épaisseur est en moyenne de 0,25m. Elle présente semble-t-il des traces d'épannelage aux deux extrémités de son bord Nord-Ouest, à son sommet Sud-Ouest et à l'angle Sud-Est de sa base.

Historique

Monolithe découvert par [J. Blot]{.creator} en [octobre 2015]{.date}.

### [Itxassou]{.spatial} - [Méatseko Bizkarra]{.coverage} 2 - [Dalle couchée]{.type}

Localisation

Altitude : 734m.

Cette dalle se trouve à 41m à l'Est Nord-Est du grand cromlech ayant au centre la BF 81.

Description

Dalle de [grès]{.subject} [triasique]{.subject} couchée sur un sol en légère pente vers le Sud. Elle affecte la forme d'un triangle grossièrement isocèle dont le grand axe, orienté Sud-Est Nord-Ouest, mesure 1,70m de long.

La base Nord-Ouest mesure 0,93m de long, le côté Sud-Ouest : 1,80m ; le côté Nord-Est est en partie amputé vers la base. L'épaisseur moyenne de cette dalle est très régulière : aux alentours de 0,14m.

On note des traces d'épannelage tout le long de tout le bord Sud-Ouest ainsi qu'à sa jonction avec la base, ainsi qu'au sommet, à sa jonction avec le côté Nord-Est.

Cette dalle aurait pu avoir été taillée pour être, par exemple, un élément des cromlechs voisins...

Historique

Dalle découverte par [Blot J.]{.creator} en [août 2018]{.date}.

### [Itxassou]{.spatial} - [Ourezti]{.coverage} Nord - [Tumulus]{.type}

Localisation

Sur la ligne de crête du premier relief qui surgit après le col au Sud du Mondarrain.

Altitude : 680m.

Description

Petit tumulus de 2,60m de diamètre, de faible hauteur (0,20m...) mais présentant un agencement de pierres de calibres variés, qui ne laisse aucun doute quant à l'origine anthropique de cette construction.

Historique

Tumulus découvert par [Blot Jacques]{.creator} en [avril 2013]{.date}.

### [Itxassou]{.spatial} - [Ourezti]{.coverage} 1 - [Tumulus]{.type}

Localisation

Carte 1345 Ouest.

Altitude : 690m.

Il est situé sur le petit sommet au Nord-Est du pic Ourezti, et est tangent à la crête rocheuse qui le borde à l'Ouest.

Description

Tumulus pierreux de 6m de diamètre et 0,30m de haut, édifié sur un sol plat, fait de blocs de quartzite enfoncés dans le sol ; au centre, quelques dalles au ras du sol pourraient appartenir à la [chambre funéraire]{.subject}. Il semblerait que les pierres périphériques soient disposées en couronne, sans que l'on puisse parler de péristalithe.

Historique

Monument découvert en [mars 1971]{.date}.

### [Itxassou]{.spatial} - [Ourezti]{.coverage} 2 - [Tumulus]{.type}

Localisation

Il est à 8m au Nord-Est de *Ourezti 1 - Tumulus*.

Description

Tumulus pierreux de 6m de diamètre et 0,30m de haut présentant une légère excavation centrale dans laquelle 3 dalles, dont une atteint 1m de long, pourraient faire partie de la [chambre funéraire]{.subject}.

Historique

Monument découvert en [mars 1971]{.date}.

### [Itxassou]{.spatial} - [Plateau Vert]{.coverage} n°7 - [Cromlech]{.type}

Localisation

Altitude : 611m.

Description

Une vingtaine de pierres, au ras du sol, délimitent un cercle de 3 mètres de diamètre ; il existe une pierre visible dans le centre qui présente une légère dépression.

Historique

Monument découvert par [J. Blot]{.creator} en [mars 2010]{.date}.

### [Itxassou]{.spatial} - [Plateau Vert]{.coverage} n°8 - [Cromlech]{.type}

Localisation

À deux mètres à l'Est de *Plateau Vert n°7 - Cromlech*.

Description

Une vingtaine de pierres au ras du sol délimitent un cercle de 2,50m de diamètre dont le centre est marqué par une dalle à plat, bien visible.

Historique

Monument découvert par [J. Blot]{.creator} en [mars 2010]{.date}.

### [Itxassou]{.spatial} - [Plateau Vert]{.coverage} n°9 - [Cromlech]{.type}

Localisation

À 2 mètres à l'Est de *Plateau Vert n°8 - Cromlech*.

Description

Une dizaine de pierres périphériques délimitent un cercle bien visible de 4,70m de diamètre ; au centre, une structure en petites dalles forme un relief évoquant un deuxième cercle interne (?), de 2m de diamètre.

Historique

Monument découvert par [J. Blot]{.creator} en [mars 2010]{.date}.

### [Itxassou]{.spatial} - [Veaux (col des)]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 544m.

Il est situé juste à l'Est du parking aménagé au Col des Veaux, à la confluence des différentes routes à cet endroit.

Description

On note un relief de terrain à peu près circulaire de 8m de diamètre et environ 1m de haut, sur un terrain en très légère pente vers l'Ouest ; quelques pierres apparaissent à sa surface. Il est difficile de dire s'il s'agit d'un tumulus ou d'un tertre d'habitat.

Historique

Monument découvert par [J. Blot]{.creator} en [février 2010]{.date}.
