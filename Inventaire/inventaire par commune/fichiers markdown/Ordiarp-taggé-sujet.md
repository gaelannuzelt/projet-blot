# Ordiarp

### [Ordiarp]{.spatial} - [Etxecortia]{.coverage} - [Pierre couchée]{.type}

Localisation

Altitude : 919m.

Cette pierre couchée se trouve à environ 75m au Sud Sud-Est des cayolars d'Etxecortia, dans un très vaste pâturage qui s'étend au Sud de la piste d'accès à ces cayolars.

Description

Elle gît dans une légère dépression circulaire, dans laquelle elle a tendance à s'enfoncer et à disparaître peu à peu ; il semble qu'elle soit en cet endroit depuis fort longtemps. Nous avons dû la dégager à la pelle pour en distinguer la périphérie avec précision. Par ailleurs, elle est la seule pierre des environs et on ne note aucun éboulement rocheux en provenance du sommet qui s'élève, à distance, plus au Sud.

En forme de triangle isocèle, cette pierre de [calcaire]{.subject} gris est orientée Nord-Sud, à sommet Sud. Son bord Est mesure 1,28m, son bord Ouest 1,34m, et sa base 0,75m. Son épaisseur varie aux alentours de 0,26m. Son bord Ouest, dans sa totalité, ainsi que sa base et la partie Sud du bord Est paraissent présenter des traces d'épannelage. Nous émettons l'hypothèse qu'il puisse s'agir d'une très ancienne borne pastorale (époque ?).

Historique

Pierre découverte par [J. Blot]{.creator} en [février 2017]{.date}.

### [Ordiarp]{.spatial} - [Etxecortia]{.coverage} (groupe 2) - [Tertres d'habitat]{.type}

Localisation

À 80m environ au Sud-Est du cayolar *actuel* de ce nom (le *groupe 1* a déjà été décrit par nous).

Altitude : 925m.

Description

Un ensemble de 9 tertres de tailles variables s'échelonnent le long de petites ravines ; leurs dimensions vont de 9m à 4m de diamètre et les hauteurs de 1m à quelques centimètres.

Historique

Tertres d'habitat découverts par [J. Blot]{.creator} en [juin 2013]{.date}.

### [Ordiarp]{.spatial} - [Etxecortia]{.coverage} (en bas) - [Tertre d'habitat]{.type}

Localisation

À plus basse altitude que *Etxecortia - Tertres d'habitat (groupe 2)*, ce TH est au confluent de 2 petites ravines.

Altitude : 864m.

Description

Tertre de terre mesurant 9m de diamètre et 0,90m de haut.

Historique

Tertre d'habitat découvert par [J. Blot]{.creator} en [juin 2013]{.date}.

### [Ordiarp]{.spatial} - [Ezeloua]{.coverage} 2 - [Tumulus]{.type}

Rappelons un premier tumulus (*T1*) découvert par J.M. de Barandiran.

Localisation

À 5om à l'Est Sud-Est des ruines du cayolar.

Altitude : 855m.

Description

Tumulus formé de « gravillons », oblongue à grand axe Est-Ouest, mesurant 14m x 10m avec une hauteur de 0,50m.

Historique

Tumulus découvert par [J. Blot]{.creator} en [juin 2013]{.date}.

### [Ordiarp]{.spatial} - [Etxecortia]{.coverage} - [Tumulus]{.type} ([?]{.douteux})

Localisation

Il est tangent à *Etxecortia - Tertres d'habitat (groupe 2)*, le plus bas situé et près de la route.

Altitude : 925m.

Description

Tumulus de 30m de haut et 7m de diamètre, avec une large excavation centrale (fouille ancienne ?).

Historique

Tumulus découvert par [J. Blot]{.creator} en [juin 2013]{.date}.

### [Ordiarp]{.spatial} - [Gatagorena (Col de)]{.coverage} - [Tumulus]{.type} ([?]{.douteux})

Localisation

Altitude : 817m.

Ce tumulus (?) se trouve au point culminant du petit plateau qui domine au Nord le col de Gatagorena ; il est à 150m environ à l'Est de la « piste des sabotiers », bien visible, et à la même distance, à l'Ouest de la piste carrossable qui mène à Etxecortia.

Description

« Tumulus » de 6m de diamètre et 0,30m à 0,40m de haut. Pas de pierres visibles ; nous pensons qu'il pourrait aussi s'agir d'un mouvement naturel du terrain atteignant son point culminant à cet endroit.

Historique

Monument découvert par [Meyrat F.]{.creator} en [février 2017]{.date}.

### [Ordiarp]{.spatial} - [Saint Grégoire]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 421m.

Ce tumulus est à 30m au Nord-Ouest de la clôture de la chapelle St Grégoire et à 9m à l'Ouest de la fin de la route qui mène à cette même chapelle.

Description

Tumulus de terre de forme ovale à grand axe Nord-Sud mesurant 7,60m x 5,50m et 0,40m de haut.

Historique

Tumulus découvert par [Blot J.]{.creator} en [avril 2015]{.date}.
