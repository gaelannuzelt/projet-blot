# Barcus

### [Barcus]{.spatial} - [Ahargo]{.coverage} 3 - [Tumulus-cromlech]{.type}

Localisation

Altitude : 598m.

Ce tumulus - ainsi que deux autres trouvés par nous en 1978 [@blotSouleSesVestiges1979, p.26] se trouve sur un replat à environ 500m à l'Ouest du mont Ahargo (609m), au Nord de Barcus et à 500m au Sud Sud-Ouest du camp protohistorique à la cote 560. Il est au point culminant de la ligne de crête et la piste pastorale passe à 6m au Nord.

Ce tumulus cromlech TC3 est à 47m au Nord-Est de *TC1*, lequel se trouve lui-même à 20m au Nord-Est de TC2*.

Description

Tumulus circulaire en forme de galette aplatie, de 7m de diamètre environ et 0,40m de haut. Un grand nombre de petits blocs en [calcaire]{.subject} est particulièrement visible en périphérie, en particulier à l'Est et au Sud. On notera que l'aspect des *TC1* et *TC2* s'est bien modifié depuis nos découvertes en 1978, les éléments pierreux des péristalithes étant recouverts en grande partie de terre et beaucoup moins visibles.

Historique

Tumulus trouvé par [Meyrat F.]{.creator} en [mai 2016]{.date}.

### [Barcus]{.spatial} - [Lexeguita]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 655m.

Situé dans le col du même nom, à 40m au Sud d'une cabane de chasse.

Description

Tumulus terreux érigé sur terrain plat, mesurant 7m de diamètre et 0,40m de haut.

Historique

Tumulus découvert par [Meyrat F.]{.creator} en [octobre 2011]{.date}.
