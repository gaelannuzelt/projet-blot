# Alçay-Alçabéhéty-Sunharette

### [Alçay-Alçabéhéty-Sunharette]{.spatial} - [Albinze]{.coverage} - [Cromlech]{.type}

Localisation

Altitude : 1110m.

Il se trouve au Nord, et un peu en contre-bas du *TC d'Ugatze* que nous avons fouillé en 1974, de l'autre côté du virage de la route.

Description

Petit cercle de 3m de diamètre, légèrement en relief, délimité par 7 pierres au ras du sol.

Historique

Monument découvert par [L. Millan]{.creator} en [mai 1989]{.date}.

### [Alçay-Alçabéhéty-Sunharette]{.spatial} - [Eltzegagne]{.coverage} - [Tertre d'habitat]{.type}

Localisation

Altitude : 900m.

Il est à quelques dizaines de mètres à l'Ouest de la piste qui se rend du cayolar Eltzegagne au petit col situé plus à l'Est.

Description

Tertre de terre de forme classique, circulaire, asymétrique, mesurant 7m de diamètre et 1m de haut dans sa partie la plus marquée.

Historique

Tertre découvert par [Blot J.]{.creator} en [mai 2013]{.date}.

### [Alçay-Alçabéhéty-Sunharette]{.spatial} - [La Croix Garat]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 1039m.

Description

Contrairement à ce que nous avions décrit en 1979, il n'a pas été endommagé en 1974 comme nous l'avions écrit par erreur, il est intact. (Il s'agit plus d'un [Tumulus-cromlech]{.type}).

### [Alçay-Alçabéhéty-Sunharette]{.spatial} - [Pic des Vautours]{.coverage} 3 (Belhigagne) - [Tumulus]{.type}

Localisation

Altitude : 854m.

Il se trouve à une quarantaine de mètres à l'Est de la clôture qui sépare une prairie artificielle d'un terrain en friche. Sur le plat de cette prairie artificielle se trouvaient les 2 tumulus décrits et publiés par nous [@blotSouleSesVestiges1979 p.6] sous le nom de tumulus du Pic des vautours ou de Belhigagne. Ils ont été rasés par la création de la prairie.

Description

Petit tumulus de terre, de 5,20m environ de diamètre, et 0,40m de hauteur, aux contours un peu irréguliers.

Historique

Monument découvert par [Meyrat F.]{.creator} en [mai 2013]{.date}.

### [Alçay-Alçabéhéty-Sunharette]{.spatial} - [Ursoy]{.coverage} (groupe Ouest) - [Tertres d'habitat]{.type} 

Localisation

Altitude : 916m.

Le groupe de 9 tertres d'habitat est situé au-dessus du chemin qui se rend au cayolar Uztarila, et qui reprend l'ancienne piste que nous empruntions en 1971.

Description

Ces 9 tertres de terre sont répartis sur une pente assez raide, qui borde au Sud un riu. Ils sont, comme d'habitude, asymétrique et dans l'ensemble bien conservés ; ils mesurent en moyenne 10m de diamètre et 1,50m de haut. 

- Le *TH 2* est à 20m au Nord du *TH 1*.
- Le *TH 3* à 20m au Nord du n° 2.
- Le *TH 4* est à 5m au Nord-Est du n° 3.
- Le *TH 5* est à 7m au Sud-Est du n°4.
- Le *TH 6* à 6m à l'Est du n° 5.
- Le *TH 7* à 10m à l'Ouest du n° 4.
- Le *TH 8* à 20m au Nord du n° 4.
- Le *TH 9* est à 7m à l'Est du n° 8.

Historique

Tertres d'habitat vus par [Blot J.]{creator} en [1971]{.date} et décrits succinctement.

### [Alçay-Alçabéhéty-Sunharette]{.spatial} - [Ursoy]{.coverage} (groupe Est 1) - [Tertres d'habitat]{.type} 

Localisation

Altitude : 890m.

Les 3 tertres d'habitat sont situés à l'Est du chemin. Ils sont de moindre importance que *Ursoy (groupe Ouest) - Tertres d'habitat*, échelonnés sur la crête qui borde le ru au Sud.

Description

Erigés sur une pente douce, ils sont moins asymétriques que les précédents et de dimensions moindres.

Historique

Tertres d'habitat vus par [Blot J.]{creator} en [1971]{.date} et décrits succinctement [@blotSouleSesVestiges1979 p. 10].

### [Alçay-Alçabéhéty-Sunharette]{.spatial} - [Ursoy]{.coverage} (groupe Est 2) - [Tertres d'habitat]{.type} 

Localisation

Altitude : 880m.

Les 4 tertres d'habitat sont situés au Nord de *Ursoy (groupe Est 1) - Tertres d'habitat*, sur l'autre rive du ru.

Description

Tertres de terre érigés sur une pente douce, de taille variable, les plus nets étant les *TH 1*, *2* et *3*. Ils mesurent en moyenne 7m de diamètre et 1,20m de haut.

Le *TH2* est à 25m au Sud du *TH 1* et le *TH 3* à 25m à l'Ouest du n° 1.

Historique

Tertres d'habitat vus par [Blot J.]{creator} en [1971]{.date} et décrits succinctement.
