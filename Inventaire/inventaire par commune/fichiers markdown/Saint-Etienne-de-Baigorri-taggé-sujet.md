# Saint-Etienne-de-Baigorri

### [Saint-Etienne-de-Baigorri]{.spatial} - [Aintziagakolepoa]{.coverage} 1 - [Cromlech]{.type}

Localisation

Altitude : 777m.

Monument situé sur une ligne de crête, en terrain plat, à 15m environ à l'Est d'un pointement rocheux et à 12m au Nord du GR 10.

Description

Cercle d'environ 2,20m de diamètre, légèrement surélevé semble-t-il, avec légère excavation centrale, le tout délimité par 8 pierres bien visibles, mais de dimensions très variables : les hauteurs pouvant varier de 0,05m à 0,20m et longueurs et largeurs peuvent aller de 0,25m x 0,20m à 0,90m x 0,80m...

Historique

Monument découvert par [F. Meyrat]{.creator} en [mai 2014]{.date}.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Aintziagakolepoa]{.coverage} 2 - [Cromlech]{.type}

Localisation

Altitude : 797m.

Il est situé sur la même ligne de crête que *Aintziagakolepoa 1 - Cromlech*, mais à environ 250m plus au Nord. Il est à 3m au Nord du GR 10. Il se trouve à 7m à l'Ouest d'un pointement rocheux très visible.

Description

Ce cromlech à fait l'objet d'une fouille clandestine, non datable, qui a remanié sa structure initiale : ainsi, le secteur Sud-Ouest du cercle paraît démuni de témoins. On peut estimer son diamètre à 9m ; il reste une dizaine de pierres de dimensions variables, visibles autour de l'excavation de fouille, de forme ovale (2,10m x 1,60m, et 0,90m de profondeur). Certaines de ces pierres sont assez volumineuses (1,70mx 0,79m et 0,25m de haut), en secteur Nord par exemple.

Historique

Monument trouvé par [F. Meyrat]{.creator} en [mai 2014]{.date}.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Astate]{.coverage} - [Cromlech tumulaire]{.type} ([?]{.douteux})

Monument situé en territoire du Baztan, à la limite avec la commune de St Etienne de Baigorri.

Localisation

Altitude : 959m.

Le [dolmen]{.subject} d'Astate se trouve dans le col du même nom, bien visible. Le possible Cromlech tumulaire, qui lui serait bien postérieur, se trouve lui-même construit sur le flanc Nord de ce tumulus dolménique et tangent au montant latéral Nord de la [chambre funéraire]{.subject}.

Description

Tumulus circulaire, mesurant 2,50m de diamètre et 0,30m de haut ; environ une vingtaine de dalles ou fragments de dalle, de dimensions très variables, paraissent en délimiter la périphérie, ainsi qu'un possible second cercle interne au précédent. Les dimensions en sont variables, atteignant pour l'une d'elles, au Nord-Est, 0,50m x 0,45m.

Historique

Monument découvert par [F. Meyrat]{.creator} en [juin 2014]{.date}.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Astate]{.coverage} - [Monolithe]{.type} ([?]{.douteux})

Territoire du Baztan, décrit par nous dans notre Inventaire [@blotInventaireMonumentsProtohistoriques2009, p44].

Localisation

Altitude : 959m.

Il est situé à une quarantaine de mètres au Nord Nord-Est du [dolmen]{.subject} d'Astate et de la ligne frontière.

Description

Très sommaire : gros bloc de [grés]{.subject}, de près de deux mètres de long, couché au sol suivant un axe Est-Ouest, isolé en bord de piste pastorale, et ne semblant pas provenir d'un éboulis naturel.

Historique

Monolithe trouvé par [Meyrat F.]{.creator} en [juin 2014]{.date}.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Baïhuntza]{.coverage} 1 - [Dolmen]{.type}

Localisation

Carte 1345 Ouest Cambo-les-Bains.

Altitude : 420m.

Il est, dans le mont Larla, à environ 900m à vol d'oiseau au Sud-Est du sommet d'Urchilo (cote 566), à l'extrémité Sud-Est de cette longue crête aux pâturages d'assez médiocre qualité. Le [dolmen]{.subject} est construit à 40m au Sud-Est d'une murette de pierres sèches, sur un terrain en légère pente vers l'Est.

Description

Tumulus pierreux formé de blocs de la dimension d'un ou deux pavés, mesurant 8m de diamètre et 1m de haut. La [chambre funéraire]{.subject}, ouverte à l'Est, est orientée Ouest Nord-Ouest, Est Sud-Est. Elle est délimitée par 3 grandes dalles de grès rose, profondément enfoncées dans le sol.

La dalle Ouest, dalle de chevet, de forme grossièrement triangulaire mesure 0,72 m à sa base, 0,38 au sommet, et 0,40m d'épaisseur. Elle présente des traces d'épannelage.

La dalle Sud, la plus grande, mesure 2,20m de long, 1,15m de haut et 0,30m d'épaisseur.

La dalle Nord mesure 1m de long, 1,40m de haut, et 0, 30m d'épaisseur.

On note dans le secteur Sud-Est une dalle isolée, inclinée vers l'extérieur, mesurant 1m de long, 0,80m de haut et 0,30m d'épaisseur ; il est difficile de dire si elle fait partie de la [chambre funéraire]{.subject} ou non.

Historique

[dolmen]{.subject} découvert en [février 1982]{.date}.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Baïhuntza]{.coverage} 2 - [Dolmen]{.type}

Localisation

On le trouve à 80m au Sud de *Baïhuntza 1 - Dolmen*, en contre-bas, sur l'axe de la crête rocheuse, là où elle présente un petit col.

Altitude : 400m.

Description

Tumulus pierreux de 8m de diamètre et 0,50m de haut.

La [chambre funéraire]{.subject} est orientée Sud-Ouest Nord-Est et mesure 2m de long et 1m de large.

Elle est délimitée : au Sud-Ouest par 2 dalles identiques accolées l'une à l'autre, formant ainsi la paroi du chevet de 0,74m de large, 0,70m de haut et 0,30m d'épaisseur. Au Sud-Est une dalle de 1m de long, et 0,74m de haut est complétée par une seconde de 0,70m de long et 0,60m de haut. Au Nord-Est on trouve une dalle de 0,60 de long et autant de haut ; enfin au Nord-Ouest il n'existe qu'une très petite dalle appuyée au chevet, mesurant 0,70m de long et 0,40 de haut. Il semble qu'à 0,80m plus au Nord-Est, un bloc parallélépipédique de 1,30m de long et 0,40m d'épaisseur ait pu faire partie de cette paroi Nord-Ouest, et qu'il ait été déplacé ultérieurement. Il n'y a pas de table visible.

Historique

[dolmen]{.subject} découvert en [février 1982]{.date}.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Baïhuntza]{.coverage} 3 - [Dolmen]{.type}

Localisation

Il est à 60m au Nord-Ouest de *Baïhuntza 1 - Dolmen* et inclus dans l'extrémité Ouest d'une murette en pierres sèches, au moment où elle naît de la paroi rocheuse. À cet endroit la murette paraît «chevaucher» un tumulus pierreux qui lui aurait donc été pré-existant.

Description

Tumulus pierreux de 7m de diamètre environ. Une dalle centrale, horizontale, de forme rectangulaire, pourrait être la table (ou couvercle) de ce dolmen qui pourrait être vierge. Cette dalle, à grand axe orienté Nord-Sud, mesure 1,70m de long, 1,20m de large, et 0,25m d'épaisseur ; tous ses côtés ont été épannelés. En outre la murette pourrait avoir réutilisé quelques pierres du tumulus qui paraît un peu dégarni dans sa partie Est.

Historique

[dolmen]{.subject} découvert en [février 1982]{.date}.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Buztanzelhay]{.coverage} - [Cairn]{.type} ou [Cromlech]{.type} ([?]{.douteux})

Localisation

Altitude : 1007m.

Ce tas de pierre, ce [cairn]{.subject}, se trouve au sommet du pic de Buztanzelay.

Description

On note un amoncellement de petites dalles, de blocs de [grés]{.subject} issus des filons voisins, qui forment un «[cairn]{.subject}» de 4,40m de diamètre et 0,80m de haut. Ce qui a attiré notre attention, c'est qu'il semble que ce dépôt récent de dalles, ait pu être fait sur un cercle de pierres, un cromlech, sous-jacent, bien plus ancien, et dont on pourrait distinguer une partie du péristalithe dans le secteur Sud.

Historique

Monument découvert par [F. Meyrat]{.creator} en [juin 2014]{.date}.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Buztanzelhay]{.coverage} - [Monolithe]{.type}

Localisation

À la limite des communes de Saint-Etienne de Baigorri et du Baztan.

Altitude : 1011m.

Il est, lui aussi, situé au sommet du pic de Buztanzelhay, à une trentaine de mètres au Sud Sud-Ouest de *Buztanzelhay - [cairn]{.subject} ou Cromlech (?)*, sur la ligne frontière ; la rupture de pente est à l'Est.

Description

Monolithe de [grés]{.subject} [triasique]{.subject}, de forme triangulaire, couché sur le sol suivant un axe Nod-Sud, à base Nord. Il mesure 2,77m de long, et 1,26m de large à sa base ; son épaisseur est variable, le bord Est étant le plus épais, environ 0,70m à 0,80m en moyenne, alors que le bord Ouest n'atteint que 0,46m environ. Les deux bords Est et Ouest semblent présenter des traces d'épannelage.

Historique

Monument découvert par [F. Meyrat]{.creator} en [juin 2014]{.date}.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Buztanzelhay]{.coverage} - [Dolmen]{.type} ou [Ciste]{.type} ([?]{.douteux})

Localisation

Territoire du Baztan, tangent à la frontière.

Altitude : 1010m.

Il est à environ 50m environ au Nord de *Buztanzelhay - [cairn]{.subject} ou Cromlech (?)*.

Description

Monument douteux, situé à l'Est et à proximité immédiate d'un éboulis de pierraille, il semble qu'on puisse distinguer un tumulus pierreux de 6 à 8m de diamètre et environ 1m de haut, très irrégulier dans ses dimensions, (avec ébauche de péristalithe au Sud ?). La [chambre funéraire]{.subject} ne serait pas au centre mais décalée vers le Nord ; environ 7 dalles paraissent la délimiter : le montant Sud mesure 1,15m de long et 0,80m de haut ; la paroi Est est marquée par deux dalles : l'une 0,80m de long, l'autre de 0,60m de long, délimitant ainsi une [chambre funéraire]{.subject} de 1,55m x 1,10m, orientée Nod-Sud.

Historique

Monument découvert par [Meyrat F.]{.creator} en [juin 2014]{.date}.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Buztanzelhay]{.coverage} 1 - [Cromlech]{.type}

Sur la ligne frontière entre territoire du Baztan et commune de Baigorri.

Localisation

Altitude : 1002m.

En commune du Baztan, quasi tangent à la ligne frontière, sur un terrain en pente légère vers le Nord Nord-Ouest., la rupture de pente est à 3m à l'Est. Il est à 17m à l'Est de *Buztanzelhay - [dolmen]{.subject} ou [ciste]{.subject} (?)*.

Description

Cercle assez mal identifiable du côté Sud, de 4,50m de diamètre, délimité par environ 8 à 9 pierres ; une excavation centrale de 1,60m x 1,10m semble signer une ancienne fouille clandestine.

Historique

Monument trouvé par [Meyrat F.]{.creator} en [juin 2014]{.date}.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Buztanzelhay]{.coverage} 2 - [Cromlech]{.type}

Localisation

En territoire du Baztan, quasi tangent à la ligne frontière.

Il est à 0,80m au Nord de *Buztanzelhay 1 - Cromlech*.

Description

Cercle légèrement tumulaire de 2,80m de diamètre, délimité par 7 pierres ; 4 autres sont visibles au centre ; hauteur du tumulus : 0,40 à 0,50m.

Historique

Monument trouvé en [juin 2014]{.date} par [F. Meyrat]{.creator}.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Buztanzelhayko lepoa]{.coverage} - [Tertres d'habitat]{.type}

Localisation

On trouve, sur les deux versants du col de Buztanzelay, (essentiellement sur le versant Sud-Est), et répartis de part et d'autre de la ligne frontière, un ensemble de 16 tertres d'habitat et plus. Nous ne citerons que 16 tertres d'habitat ; il en existe au moins une dizaine d'autres sur le versant Sud-Ouest du col, côté Baztan, dissimulés actuellement par les fougères...

- *TH 1* (Baztan) : Altitude : 862m. Tertre ovale asymétrique mesurant 11m x 10m et 1,20m de haut.

- *TH 2* (Baztan) : situé à 11m au Nord du précédent. Tertre ovale asymétrique mesurant 10m x 8m et 1 m de haut.

- *TH 3* (Baztan) : situé à 2m à l'Ouest du précédent ; tertre ovale asymétrique de 8m x 6m et 0,40m de haut.

- *TH 4* (St Etienne de Baigorri) : situé à 40m au Nord du précédent. Tertre ovale asymétrique mesurant 10m x 8m et 0,30m de haut.

- *TH 5* (St Etienne de Baigorri) : situé à 35m à l'Ouest du précédent. Tertre ovale de 11m x 8m et 0,40m de haut.

- *TH 6* (St Etienne de Baigorri) : situé à3m à l'Ouest du précédent. Tertre ovale mesurant 15m x 10m et 0,80m de haut.

- *TH 7* (St Etienne de Baigorri) : Altitude : 841m. Situé à 2m à l'Ouest Nord-Ouest du précédent. Tertre ovale mesurant 8m x7m et 0,30m de haut.

- *TH 8* (St Etienne de Baigorri) : Altitude : 843m. Situé à 65m au Nord-Ouest du précédent. Tertre ovale asymétrique mesurant 11m x 6m et 0,40m de haut.

- *TH 9* (St Etienne de Baigorri). Situé à 30m au Nord-Ouest du précédent. Tertre ovale asymétrique mesurant 17m x 7m et 0,40m de haut.

- *TH 10* (St Etienne de Baigorri) : Altitude : 854m. Situé à 150m au Nord-Ouest du précédent. Tertre ovale asymétrique mesurant 11m x 8m et 1,30m de haut. Il y a 100m environ entre ce tertre et le rocher situé au Sud Sud-Est, à la bifurcation du GR 10.

- *TH 11* (Baztan) : Altitude : 857m. Il est situé à 30m au Sud-Ouest du rocher situé à la bifurcation du GR10. Tertre ovale asymétrique mesurant 11m x 8m et 0,60m de haut.

- *TH 12* (Baztan) : situé à 3m au Sud-Ouest du précédent. Tertre ovale asymétrique mesurant 6m x 5m et 0,30m de haut.

- *TH 13* (Baztan) : situé à 3m au Sud-Est du précédent. Tertre ovale asymétrique mesurant 5m x 4m et 0,20m de haut.

- *TH 14* (Baztan) : Altitude : 841m. Situé à 80 mètres à l'Est du précédent. Tertre ovale asymétrique mesurant 15m x 8m et 0,40m de haut.

- *TH 15* (Baztan) : situé à 3m au Sud-Est du précédent. Tertre ovale asymétrique mesurant 17m x 8m et 0,60m de haut.

- *TH 16* (Baztan) : Altitude : 835m. Situé à 55m au Sud-Est du n°15 et à 15m au Sud-Ouest du GR10. Tertre (?) très volumineux de 24m x 12m et 2,60m de haut.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Elhorriko-kaskoa]{.coverage} - [Cromlech]{.type}

Localisation

Carte 1346 Ouest Saint-Etienne-de-Baigorri.

Altitude : 910m.

Il est situé sur un petit replat à l'extrémité Sud-Est de la crête de ce nom qui domine, au Nord-Est, le col d'Elhorietta.

Description

Cercle de 3m de diamètre délimité par 7 pierres bien visibles, dont certaines atteignent 0,70m de haut ; on en compte 3 au Nord, 1 à l'Est, 1 à l'Ouest, 2 au Sud-Ouest.

Historique

Monument découvert en [mai 1974]{.date}.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Elhorrieta]{.coverage} - [Monolithe]{.type} ([?]{.douteux})

Localisation

Baztan

Altitude : 850m.

On le trouve sur le versant Sud de Nekaitz, couché dans le sens de pente (vers l'Ouest).

Description

Bloc de [calcaire]{.subject} parallélépipédique, pointu à son extrémité Est. Il mesure, au total, plus de 5m de long, mais est fragmenté (gel ?) en 3 morceaux dont le plus grand mesure 3,40m de long, 2,05m à sa base, et 0,50m d'épaisseur. Etant donné sa structure, il n'est pas aisé de distinguer d'éventuelles traces d'épannelage. *Monolithe douteux*.

Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Elhorrieta]{.coverage} (groupe Nord) - [Tertres d'habitat]{.type} 

Localisation

Altitude : 860m.

Ils sont situés au Nord de la piste qui rejoint la BF 99 au col de Nekaitz, en venant d'Elhorrieta, à l'endroit où nait un petit riu descendant vers le Nord.

Description

On distingue 6 tertres d'habitat (4 à l'Est et 2 à l'Ouest), au relief atténué par la colluvion.

Historique

Monuments découverts par [Blot J.]{.creator} en [octobre 2011]{.date}.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Elhorrieta]{.coverage} (groupe Sud (a) et (b)) - [Tertres d'habitat]{.type} 

Baztan

Nous citons ici ces deux groupes de tertres d'habitat, car, avec le précédent, ils forment un ensemble - concernant la vie de ces bergers de la protohistoire - complémentaire de leurs nécropoles si proches.

- *Groupe (a)*

    Altitude : 820m.

    On distingue les reliefs de plusieurs tertres d'habitat en bordure de la naissance d'un petit riu qui descend vers l'Ouest.

- *Groupe (b)*

    Altitude : 810m.

    Même chose pour les berges de ce petit riu né plus bas.

Historique

Tertres d'habitat découverts par [Blot J.]{.creator} en [octobre 2011]{.date}.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Galarzé]{.coverage} - [Tumulus]{.type}

Localisation

Carte 1345 Ouest Cambo-les-Bains.

Altitude : 530m.

Un ensemble d'une quinzaine de tumulus pierreux se trouve dans une fougeraie enclose d'une murette de pierres, sur un terrain en légère pente, à l'Est du pic Iparla, en contre-bas et au Nord-Ouest du col de Galarzé.

Description

Ces 15 tumulus sont identiques et espacés les uns des autres de 2 à 6m. Chacun d'eux se présente sous la forme d'une couronne de pierres, de 2m à 2,70m de diamètre, à l'intérieur de laquelle on trouve un remplissage de blocailles de dimensions plus modestes, le tout atteignant 0,40m de haut. Nous ignorons totalement la signification de ces constructions.

Historique

Cet ensemble de tumulus nous a été indiqué le [29 mars 1993]{.date} par Mr [Peyo Currutcharry]{.creator} que nous tenons à remercier ici.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Iparla]{.coverage} 3 - [Monolithe]{.type}

Localisation

Altitude : 990m.

Il est situé à une cinquantaine de mètres au Sud de la BF 90, et à environ 6 mètres à l'Ouest du GR 10, sur une pente inclinée vers l'Ouest.

Description

Important bloc parallélépipédique de [grés]{.subject} [triasique]{.subject}, allongé selon un axe Est-Ouest, présentant une extrémité Est plus rétrécie (0,60m) sur laquelle on voit la marque blanche et rouge du GR 10. Il mesure 4m de long, 1,40m de large et 0,45m d'épaisseur en moyenne. Il semble présenter des traces d'épannelage à son extrémité Est et sur son bord Nord, sur un mètre environ, près de la pointe. Pour P. Velche, il semblerait qu'à une cinquantaine de mètres au Sud, une faille naturelle ait pu être le lieu d'extraction de ce monolithe.

Historique

Monument découvert par [P. Velche]{.creator} en [août 2010]{.date}.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Ispégui]{.coverage} - [Monolithe]{.type}

Localisation

Altitude : 680m.

Au col d'Ispégui, à 12m environ au Nord-Est de la route goudronnée, et à une vingtaine de mètres au Nord-Est de la BF 91.

Description

Monolithe dalle de [grés]{.subject} [triasique]{.subject}, couché sur un sol en légère pente vers le Sud Sud-Est. De forme grossièrement parallélépipédique, il mesure 1,90m de long, 0,70m de large à son sommet le plus étroit (à l'Ouest), et son épaisseur moyenne y est de 0,24m. À l'autre extrémité, il mesure 1,04m de large et 0,06m d'épaisseur. De nombreuses traces d'épannelage sont visibles à la totalité de sa périphérie.

Historique

Monolithe signalé par [J. Aguirre Mauleon]{.creator} (Aranzadi), en [avril 2014]{.date}.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Leispars]{.coverage} - [Tumulus]{.type}

Localisation

Carte 1346 Ouest Saint-Etienne-de-Baigorri.

Altitude : 140m.

Il se trouve à gauche de la route, au sortir de Leispars quand on se dirige vers Saint-Etienne-de-Baïgorry. Il est visible juste de l'autre côté de la murette qui le sépare de la route.

Description

Important tumulus de 30m de diamètre et de près de 1,50m de haut, recouvert de gazon ; la murette a amputé environ le cinquième Ouest du monument. Les propriétaires de la maison Sallaberia, de l'autre côté de la route, se rappellent avoir vu, au début du XXème siècle, de belles dalles entourer ce tumulus. Des labours les ont ensuite supprimées et ont aussi contribué à aplanir sensiblement le monument ; il ne semblerait pas que la partie centrale ait été touchée...

Historique

Monument découvert en [août 1972]{.date}.

### [Saint-Etienne-de-Baigorri]{.spatial} - Les monuments du [Col d'Elhorietta]{.coverage}

Localisation

Carte 1346 Ouest Saint-Etienne-de-Baigorri.

Altitude : 840m.

Il existe toute une nécropole, presque aussi riche que celle d'Okabé, au col d'Elorietta située dans la longue ligne de croupes qui délimite, à l'Ouest, la Vallée des Aldudes. Huit monuments ont déjà été décrits [@barandiaranCronicaPrehistoriaBaigorri1949, p.71] ; nous apportons ici un complément à cet inventaire, tout en faisant remarquer que la ligne frontière, passant par le col, certains monuments se trouvent en territoire du Baztan. [Nous]{.creator} les décrivons cependant pour l'unité de cette nécropole qui ignorait les frontières à cette époque.

L'ensemble des monuments se trouve entre les BF 101 et 102 ; nous partirons de la BF 101. Rappelons que [J.M. de Barandiaran]{.creator} décrivait en [1949]{.date} :

- Un *cromlech n°1*, de 4m de diamètre et 13 pierres ;

- Un *cromlech n°2*, de 4m de diamètre et 20 pierres ;

- Un *cromlech n°3*, de m de diamètre, 11 pierres, légèrement tumulaire ;

- Un *cromlech n°4*, de 2m de diamètre et 12 pierres ;

- Un *cromlech n°5*, de 6 m de diamètre et 23 pierres ;

- Un *cromlech n°6*, de 5m de diamètre et 6 pierres ;

- Un *cromlech n°7*, de 3m de diamètre et 12 pierres ;

- enfin un *tumulus (n° 8)*, de terre et de pierres, de 6m de diamètre, profondément excavé ([dolmen]{.subject} complètement détruit ?).

- Le *tumulus n° 9* 
    Se trouve à 1m au Sud de la BF 101.
    - Description : Petit tumulus de 5m de diamètre et 0,30m de haut, à la surface duquel apparaissent une quinzaine de petites dalles sans ordre apparent ; il a été excavé et remanié dans son secteur Sud-Est.
    - Historique : Monument découvert en [mai 1974]{.date}.

- *Cromlech Elhorrieta 10*.
    [Baztan]{.spatial}.
    - Localisation : Il est tangent au Nord-Est du n°3.
    - Description : Cercle de 2m de diamètre, délimité par 9 petites pierres, dont 2 ou 3 seraient communes au cromlech n°3.
    - Historique : Découvert en mai 1974.

- *Cromlech Elhorrieta 11*.
    Baztan.
    - Localisation : Tangent à l'Ouest au n°3.
    - Description : Une dizaine de pierres délimite un petit cercle de 2m de diamètre, dont 2 à 3 seraient communes au n°3. Peut-être même y aurait-il un cromlech 11 bis à l'Ouest du n°11 et tangent au n°4.
    - Historique : Découvert en mai 1974.

- *Cromlech Elhorrieta 12*.
    Saint-Etienne-de-Baigorri.
    - Localisation : À 5m au Sud-Ouest du n°4.
    - Description : Cercle de 4m de diamètre délimité par 5 pierres.
    - Historique : Découvert en mai 1974.

- *Cromlech Elhorrieta 13*.
    Baztan.
    - Localisation : À 20m au Nord Nord-Est du n°7 et à 22m au Sud-Est du n°8.
    - Description : Cercle de 2,50m de diamètre, délimité par une quinzaine de pierres, bien visibles, blocs de grès arrondis du volume d'un gros pavé.
    - Historique : Découvert en mai 1974.

- *Cromlech Elhorrieta 14*.
    Baztan.
    - Localisation : Tangent au Nord Nord-Ouest du n°13.
    - Description : Petit cercle de 1,50m de diamètre, délimité par une douzaine de pierres du type galet arrondi ; 2 sont visibles au centre du cercle.
    - Historique : Découvert en mai 1974.

- *Cromlech Elhorrieta 15*.
    Baztan.
    - Localisation : À 2m à l'Ouest Sud-Ouest du n°8.
    - Description : Cercle de 3m délimité par une dizaine de pierres au ras du sol.
    - Historique : Découvert en mai 1974.

- *Cromlech Elhorrieta 16*.
    Baztan.
    - Localisation : A 2m à l'Ouest Sud-Ouest du n°15.
    - Description : Cercle de 5m de diamètre délimité par une douzaine de pierres.
    - Historique : Découvert en mai 1974.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Nekaitz]{.coverage} - [Tumulus]{.type} ([?]{.douteux})

Localisation

Baztan

Altitude : 840m.

Au milieu du petit col qui sépare Elhorrieta de Nekaitz, à l'Ouest de la BF 100.

Description

Petit tumulus terreux, circulaire, parfaitement régulier, de 2,30m de diamètre et 0,15m de haut. En son centre apparaît une grosse pierre.

Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Oilarandoy]{.coverage} - [Tumulus]{.type} ([?]{.douteux})

Localisation

Altitude = 921m.

Ce probable tumulus est situé en contre-bas de la chapelle, à environ 70m au Nord-Est de celle-ci sur le replat qui s'amorce après la pente ; il est à environ 10m au Sud Sud-Ouest d'un ensemble de blocs rocheux.

Description

Tumulus terreux circulaire, de 7m de diamètre environ, en forme de galette très aplatie (environ 0,10m de haut) ; on distingue dans sa moitié E. cinq pierres au ras du sol : (éléments d'un péristalithe ?). Monuments douteux.

Historique

Tumulus découvert par [F. Meyrat]{.creator} en [Août 2019]{.date}.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Urdiako Lepoa]{.coverage} - [Cromlech]{.type}

Localisation

Carte 1346 Ouest Saint-Etienne-de-Baigorri.

Altitude : 960m.

Il est situé dans le col situé au Sud et en contre-bas des monuments d'Urdiakoharria (*Urdiakoharria 3 - Cromlech*...), juste à la fin de la descente.

Description

Cercle de 4m de diamètre, délimité par 6 pierres dont l'une atteint 0,40m de long et l'autre 0,60m. Au centre, une dalle allongée dans le sens Nord-Ouest Sud-Est pourrait faire partie de la [ciste]{.subject}.

Historique

Monument découvert en [juillet 1972]{.date}.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Urdiakoharria]{.coverage} 3 - [Cromlech]{.type}

Localisation

Carte 1346 Ouest Saint-Etienne-de-Baigorri.

Altitude : 960m.

Nous avons publié [@blotNouveauxVestigesMegalithiques1972b, p. 206], un cromlech (n°1) et un tumulus (n°2), sur cette longue croupe orientée Est-Ouest, dominée au Sud-Ouest par le sommet Adarza, et au Nord-Est par le Munhoa.

Description

Ce n°3 est à 17m au Nord-Est du n°1.

Vingt pierres plantées très régulièrement en arc de cercle paraissent bien représenter les vestiges d'un cromlech de 20m de diamètre environ.

Historique

Monument découvert en [juillet 1972]{.date}.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Urdiakoharria]{.coverage} 4 - [Cromlech]{.type}

Localisation

Il est situé à 130m à l'Ouest Sud-Ouest du n°1 [@blotNouveauxVestigesMegalithiques1972b, p. 206].

Description

Cercle de 3m de diamètre délimité par 6 pierres ; au centre, une dalle de forme grossièrement triangulaire, couchée sur le sol, pourrait être le couvercle de la [ciste]{.subject}.

Historique

Monument découvert en [juillet 1972]{.date}.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Urdiakoharria]{.coverage} 5 - [Cromlech]{.type}

Localisation

À 14m au Nord-Ouest de *Urdiakoharria 3 - Cromlech*.

Description

Cercle de 4,40m de diamètre délimité par 11 pierres, dont une particulièrement importante, de 1m de long, dans le secteur Est Sud-Est. Au centre, 2 dalles verticales de 0,40m de long, allongées selon un axe Est-Ouest et distantes l'une de l'autre pourraient faire partie de la [ciste]{.subject} centrale.

Historique

Monument découvert en [juillet 1972]{.date}.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Urdiakoharria]{.coverage} - [Dolmen]{.type}

Localisation

Altitude : 950m.

Il est à 12 mètres au Sud-Est de *Urdiakoharria 5 - Cromlech*.

Description

Au centre d'un tumulus faiblement marqué (0,20m de haut) de 5 mètres de diamètre environ, on note une dalle de [calcaire]{.subject} rectangulaire mesurant 1,56m de long, 1,10m de large et 0,18m d'épaisseur ; elle présente des traces très nettes d'épannelage sur tout son pourtour. Son grand axe est orienté Nord-Sud, et elle est inclinée, son extrémité Nord-Ouest s'enfonçant légèrement sous terre. On note 2 petits blocs rocheux à quelques centimètres de son angle Sud-Ouest.

Historique

Monument découvert par [J. Blot]{.creator} en [juillet 2011]{.date}.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Urdiakoharria]{.coverage} 3 - [Tumulus]{.type}

Localisation

Altitude : 940 m.

Il est à 18mètres au Nord-Ouest de la route.

Description

Tumulus terreux de faible hauteur (0,30m) mesurant 7,30m de diamètre.

Historique

Monument découvert par [J. Blot]{.creator} en [juillet 2011]{.date}.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Urdiakoharria]{.coverage} - [Tertre d'habitat]{.type}

Localisation

Il est à 10 mètres à l'Ouest de *Urdiakoharria 4 - Cromlech*.

Altitude : 945m.

Description

Tertre ovale à grand axe Nord-Est Sud-Ouest, érigé sur un terrain en légère pente vers le Nord.

Il mesure 11m de long, 8,50m de large et 0,60m à 0,80m de haut.

Historique

Tertre découvert par [J. Blot]{.creator} en [juillet 2011]{.date}.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Nékaitzko lepoa]{.coverage} - [Dalle couchée]{.type}

Localisation

Territoire du Baztan, à côté de la BF 99.

Altitude : 857m.

Description

Dalle de [grés]{.subject} couchée au sol suivant un axe Est Nord-Est Ouest Sud-Ouest. Affectant grossièrement une forme de stèle discoïdale, elle mesure 1,44m de long, 0,52m de large à son sommet et 0,41m de large à la base ; sa partie la plus étroite mesure 0,29m. Son épaisseur varie entre 0,24m et 0,32m. Tout son pourtour présente des traces évidentes d'épannelage.

Il est probable qu'il s'agisse d'une ancienne BF ou d'une ébauche abandonnée...

Historique

Dalle trouvée en [août 2014]{.date} par [F. Meyrat]{.creator}.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Olhate]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 815m.

Ce tumulus est à 80m au Sud Sud-Ouest de la BF 95, et tangent à l'Est à la piste de crête.

Description

Tumulus mixte de terre et de pierres, d'environ 7m de diamètre, de faible hauteur : 0,30m. De nombreuses pierres sont visibles en surface et 7 à 8 pourraient délimiter un péristalithe... La présence d'une dalle plus importante (0,57m x 0,40m) bien visible au Nord-Est du centre pourrait évoquer un montant de [chambre funéraire]{.subject} ([ciste]{.subject} dolménique ?).

Historique

Monument trouvé par [F. Meyrat]{.creator} en [août 2014]{.date}.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Munhogain]{.coverage} - [Dolmen]{.type}

Localisation

Altitude : 900m.

Ce monument est situé au flanc Sud Sud-Est du mont Munhogain, à 16m au Sud-Ouest de la piste qui monte au sommet.

Description

De ce monument, érigé sur un terrain en très faible pente vers le Sud-Est., il ne reste qu'un tumulus de très faible hauteur et une «grande» dalle, verticale, bien visible au Sud-Ouest, ainsi que 4 autres fragments de dalles constitutifs du montant Nord-Ouest, probablement la dalle de chevet, dont il ne reste que ces fragments. La grande dalle, en [calcaire]{.subject} blanc, mesure 1,40m de long à sa base et 0,96m au sommet ; elle atteint 0,50m de haut, et 0,17m d'épaisseur.

Les 4 fragments de dalle sont disposés perpendiculairement à cette grande dalle, à son extrémité Nord-Ouest ; ils mesurent respectivement 0,40m, 0,27m, et 0,26m de long.

On peut estimer que la [chambre funéraire]{.subject}, orientée Nord-Ouest Sud-Est., mesurait environ 1,50m de long et 0,87m de large.

Il semble persister les vestiges d'un péristalithe constitué de 8 pierres, visibles au ras du sol.

A noter enfin, à 20m à l'Est, un bloc de [calcaire]{.subject} bien visible (Borne ?), mesurant, à sa base 0,60m x 0,40m et 0,70m de haut.

Historique

Monument découvert par [Meyrat F.]{.creator} en [avril 2017]{.date}.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Munhogain]{.coverage} - [Tumulus]{.type}

Localisation

Ce tumulus est tangent à *Munhogain - Dolmen*, au Nord-Ouest.

Description

Tumulus terreux, de forme ovale, mesurant 3,80m dans son plus grand axe Nord-Ouest Sud-Est, 3m de large et 0,30m de haut. Il semble bien que l'on puisse décrire un péristalithe, surtout bien visible à ses deux extrémités (6 pierres au Nord-Ouest et 4 au Sud-Est).

Historique

Tumulus découvert par [Meyrat F.]{.creator} en [avril 2017]{.date}.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Urdoze]{.coverage} (d') - [Dolmen]{.type} 

Nous tenons à signaler que ce [dolmen]{.subject} trouvé par [nous]{.creator} en [1972]{.date} et que nous avons publié dans le [@blotNouveauxVestigesMegalithiques1973 p.199] a depuis été détruit, sa [chambre funéraire]{.subject} rasée ; il n'en reste que le tumulus pierreux, de forme ovoïde, selon un grand axe Est Sud-Est Ouest Nord-Ouest, mesurant 8m x 6,80m et 0,30m de haut.

Altitude : 266m.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Oilarandoy]{.coverage} - [Tumulus]{.type} ([?]{.douteux})

Localisation

Altitude : 921m.

Ce probable tumulus est situé en contre-bas de la chapelle, à environ 70m au Nord-Est de celle-ci, sur le replat qui s'amorce après la pente ; il est à environ 10m au Sud Sud-Ouest d'un ensemble de blocs rocheux...

Description

Tumulus terreux circulaire, de 7m de diamètre environ, en forme de galette très aplatie (environ 0,10m de haut) ; on distingue dans sa moitié Est cinq pierres au ras du sol : (éléments d'un péristalithe ?)... Monuments douteux.

Historique

Tumulus découvert par [F. Meyrat]{.creator} en [août 2019]{.date}.

## [Saint-Etienne-de-Baigorri]{.spatial} - Les monuments du secteur de l'[Antenne relai d'Urxilo]{.coverage}

Nous allons traiter maintenant d'un ensemble de « monuments » - ou supposés tels pour certains - dont la liste nous a été confiée par Eric Dupré en novembre 2019. Ces vestiges ont été identifiés par lui et son équipe lors des leurs recherches dans les communes de Baigorri et St Martin d'Arrossa.

Ces éléments archéologiques se trouvent répartis essentiellement sur la commune de Baigorri, en descendant de l'antenne-relai d'Urxilo vers la Nive de Baigorri et en suivant la crête de Baïhuntza. La nature du terrain, couvert naturellement, de par sa structure géologique, de très nombreux et abondants débris de dalles - lesquelles sont par endroit soulevées par la pousse des arbres - peut être source de nombreuses erreurs ; c'est pourquoi nous resterons très prudent quant aux qualifications à attribuer aux différents éléments ci-après décrits.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Antenne relai d'Urxilo]{.coverage} - Bai 001 - [Coffre dolménique]{.type} ([?]{.douteux})

Localisation

Altitude 537m.

Ce monument est érigé sur terrain plat, à environ 10m au Sud-Est du bâtiment de l'antenne relai d'Urxilo, et le chemin qui monte à l'antenne est à 3m au Nord-Est environ semble avoir coupé un éventuel tumulus, dont les pierres s éboulent sur la coupe.

Description

Dénommé par [E. Dupré]{.creator} « coffre dolménique ruiné ». On peut en effet décrire une [chambre funéraire]{.subject} rectangulaire d'environ 2,50m de long et 0,60m de large, délimitée, semble-t-il par 5 dalles plantées : deux dalles au nord, non jointives dont l'une, la plus à l'ouest, mesure 1,16m de haut. Le tumulus aurait un diamètre - difficile à apprécier - de 4,80m (7,50m pour E. Dupré).

Monument cependant douteux.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Antenne relai d'Urxilo]{.coverage} - Bai 002 - [Coffre dolménique]{.type} ([?]{.douteux})

Localisation

Altitude 537 m.

La piste qui mène à l'antenne passe à 12 m à l'Est du « monument ».

Description

Coffre dolménique ruiné pour [E.Dupré]{.creator}.

La pousse d'un arbre au centre d'un « tumulus » qui aurait 8m de diamètre a soulevé un ensemble de dalles de dimensions variables pourraient évoquer une « [chambre funéraire]{.subject} » de 1,70m de long et 0,50m de large....

Monument très douteux.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Antenne relai d'Urxilo]{.coverage} - Bai 003 ([?]{.douteux})

Ce « monument ruiné » n'a pas pu être retrouvé sur le terrain.

Altitude : 528m.

Il aurait été un [cairn]{.subject .type} de 4,50m de diamètre.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Antenne relai d'Urxilo]{.coverage} - Bai 004 - [Coffre dolménique]{.type} ([?]{.douteux})

Localisation

Altitude 529m.

Ce monument est à 10m à l'ouest de *Bai 002*.

Description

Pour [E. Dupré]{.creator} il s'agirait d'un coffre dolménique ruiné avec un [cairn]{.subject} de 4,5m de diamètre. Il n'a cependant été vu sur le terrain qu'un amas pierreux sans tumulus et quelques dalles plantées pouvant éventuellement délimiter une [chambre funéraire]{.subject} de 1,66m de long et 0,38m de large.

Monument douteux.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Antenne relai d'Urxilo]{.coverage} - Bai 005 - [Coffre dolménique]{.type} ([?]{.douteux})

Localisation

Altitude : 532m.

Description

"Coffre dolménique ruiné" avec [cairn]{.subject} de 9m de diamètre pour [E. Dupré]{.creator}.

Il semblerait que l'on puisse distinguer un « tumulus » pierreux de 9m de diamètre et 0,50m de haut. Un arbre a poussé, un peu décentré vers le sud, mais des dalles plantées (relevées sans doute par la pousse de l'arbre), ne semblent délimiter aucune [chambre funéraire]{.subject}.

Monument très douteux.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Antenne relai d'Urxilo]{.coverage} - Bai 006 - [Tumulus]{.type} ([?]{.douteux})

Localisation

Altitude : 526m.

Description

[E. Dupré]{.creator} cite « un coffre dolménique ruiné » avec un [cairn]{.subject} de 6m de diamètre. Il n'a été vu que quelques dalles jonchant le sol, dont certaines soulevées par la présence d'un arbre ; il très difficile de parler d'un tumulus (vu leur petit nombre) ou d'une [chambre funéraire]{.subject}.

Monument très douteux.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Antenne relai d'Urxilo]{.coverage} - Bai 007 - [Coffre dolménique]{.type} ([?]{.douteux})

Ce monument n'a pas pu être identifié.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Antenne relai d'Urxilo]{.coverage} - Bai 008 - [Cromlech]{.type} ([?]{.douteux})

Localisation

Altitude 525m.

Le chemin qui monte à l'antenne se trouve à 6m à l'ouest.

Description

Présenté par [E. Dupré]{.creator} comme « coffre dolménique ruiné » avec un [cairn]{.subject} de 5m de diamètre ; on note sur le terrain une série de dalles orientées Est - Ouest, faisant partie d'un filon rocheux en place, ceci étant particulièrement visible dans le quart Nord-Ouest ; sept autres dalles paraissent plantées dans le sol, selon une disposition grossièrement circulaire, (voir dessin, dalles en hachuré), mais il est très difficile de parler de "cromlech" !

On ne note ni tumulus, ni [chambre funéraire]{.subject} évidente.

Monument très douteux.

## [Saint-Etienne-de-Baigorri]{.spatial} - Les monuments du secteur des Cabanes

Nous allons traiter maintenant d'un ensemble de « monuments » - ou supposés tels pour certains - dont la liste nous a été confiée par Eric Dupré en novembre 2019. Ces vestiges ont été identifiés par lui et son équipe lors des leurs recherches dans les communes de Baigorri et St Martin d'Arrossa.

Ces éléments archéologiques se trouvent répartis essentiellement sur la commune de Baigorri, en descendant de l'antenne-relai d'Urxilo vers la Nive de Baigorri et en suivant la crête de Baïhuntza. La nature du terrain, couvert naturellement, de par sa structure géologique, de très nombreux et abondants débris de dalles - lesquelles sont par endroit soulevées par la pousse des arbres - peut être source de nombreuses erreurs ; c'est pourquoi nous resterons très prudent quant aux qualifications à attribuer aux différents éléments ci-après décrits.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Les Cabanes]{.coverage} - Bai 009 - [Dolmen]{.type}

Description

Cité comme « [dolmen]{.subject} ruiné » par [E. Dupré]{.creator} : au point GPS indiqué, la très forte pente excluait un monument, et au point culminant, à l'aplomb, il n'a été vu qu'un semis de pierraille désordonné.

Monument en définitive non vu.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Les Cabanes]{.coverage} - Bai 010 - [Coffre dolménique]{.type} ([?]{.douteux})

Localisation

Altitude 467 m.

Le chemin de crête passe à environ 18m à l'ouest du monument.

Description

Celui-ci est érigé sur terrain plat (ou en très légère pente vers l'est ). Il est présenté par [E. Dupré]{.creator} comme « [dolmen]{.subject} ruiné » ; il semblerait en effet que l'on puisse reconnaître une [chambre funéraire]{.subject}, mesurant 1,90m de long et 1,50m de large, orientée Nord Nord-Ouest, et délimitée par 8 dalles plus ou moins horizontales actuellement mais qui semblent cependant avoir été plantées dans le sol : à noter la dalle **A**, au Nord-Est, mesurant 0,90m x 0,62m et 0,20m de haut. On ne distingue pas de tumulus, mais une ébauche de péristalithe serait plus ou moins visible dans le secteur **E** constituée de 8 pierres (?).

Monument possible, mais douteux.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Les Cabanes]{.coverage} - Bai 011 - [Coffre dolménique]{.type}

Localisation

Altitude : 459 m

Il est distant d'environ 45m de *Bai 010* qui est au Nord, et sur terrain plat ou en légère pente vers l'Est Le sentier de crête pour Urxilo passe à 20m à l'ouest du monument.

Description

Il semble bien que l'on puisse décrire une chambrer funéraire, orientée Est Ouest mesurant environ 2m x 0,45m délimitées par 3 dalles plantées, de dimensions fort modestes, et inclinées vers le Nord.

Une dalle plus importante, couchée au sol a 1,10m l'Ouest Sud-Ouest, pourrait être un monolithe (pierre **J** sur le dessin) présentant des traces d'épannelage à son sommet et sur son bord Sud-Ouest. 1,26m de long, 0,41m de large et 0,60m d'épaisseur.

Monument très possible.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Les Cabanes]{.coverage} - Bai 012 - ([?]{.douteux})

Localisation

Altitude : 461m.

Description

« Monument ruiné » pour [E. Dupré]{.creator}.

Sur un terrain très en pente Est Ouest, on voit une abondante pierraille, faite de très nombreuses dalles de grès gisant au sol, sans qu'il y ait pour autant une [chambre funéraire]{.subject} discernable.

« Monument » plus que douteux.

## [Saint-Etienne-de-Baigorri]{.spatial} - Les monuments du secteur du Saroi

Nous allons traiter maintenant d'un ensemble de « monuments » - ou supposés tels pour certains - dont la liste nous a été confiée par [Eric Dupré]{.creator} en novembre 2019. Ces vestiges ont été identifiés par lui et son équipe lors des leurs recherches dans les communes de Baigorri et Saint-Martin-d'Arrossa.

Ces éléments archéologiques se trouvent répartis essentiellement sur la commune de Baigorri, en descendant de l'antenne-relai d'Urxilo vers la Nive de Baigorri et en suivant la crête de Baïhuntza. La nature du terrain, couvert naturellement, de par sa structure géologique, de très nombreux et abondants débris de dalles - lesquelles sont par endroit soulevées par la pousse des arbres - peut être source de nombreuses erreurs ; c'est pourquoi nous resterons très prudent quant aux qualifications à attribuer aux différents éléments ci-après décrits.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Saroi]{.coverage} - Bai 013 - [Tumulus]{.type} ([?]{.douteux})

Localisation

Altitude : 435m.

Description

Il s'agit d'un amas pierreux, plus ou moins circulaire visible sur un terrain plat, de 0,15m de haut et d'environ 4m à 5m de diamètre, constitué de terre et de blocs de grès de la taille d'un pavé ; il ne semble pas que l'on puisse y distinguer ni péristalithe, ni [chambre funéraire]{.subject}.

Monument douteux.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Saroi]{.coverage} - Bai 014 - [Dolmen]{.type} ([?]{.douteux})

Localisation

Altitude : 433m.

Historique

[Nous]{.creator} avons découvert en [février 1982]{.date} ce [dolmen]{.subject} (qui semble bien avoir été par la suite, inclus dans une murette).

### [Saint-Etienne-de-Baigorri]{.spatial} - [Saroi]{.coverage} - Bai 015 - [amas pierreux]{.type} ([?]{.douteux})

Localisation

Il n'a pas pu être identifié sur le terrain.

Description

Amas pierreux. Très semblable à *Bai 016 - amas pierreux (?)*.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Saroi]{.coverage} - Bai 016 - [amas pierreux]{.type} ([?]{.douteux})

Localisation

Altitude : 432m.

Il est situé à environ 16m à l'Est de *Bai 013 - Tumulus (?)*.

Description

Cité comme amas pierreux, par [E. Dupré]{.creator}, c'est en effet le cas. On ne note ni [chambre funéraire]{.subject} ni tumulus....

Monument très douteux.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Saroi]{.coverage} - Bai 017 - [dalle plantée]{.type} - [amas pierreux]{.type} ([?]{.douteux})

Localisation

Altitude 437m.

Description

L'amas pierreux n'a pas été vu à l'endroit indiqué par les coordonnées GPS d'[E. Dupré]{.creator} (voir plus bas *Bai 017 bis*) mais une simple borne avec une croix.

Par contre, il a été noté par un important amas pierreux, véritable cône de déblais, s'étendant en pente vers le sud sur une vingtaine de mètres et 3m de haut en moyenne à sa partie la plus fournie. Il est recouvert de végétation - Est-ce l'amas pierreux cité par E. Dupré ?

### [Saint-Etienne-de-Baigorri]{.spatial} - [Saroi]{.coverage} - Bai 017 bis - [borne]{.type} 

Localisation

Le sentier de crête passe à 2m à l'Est.

Description

Cette borne en grès mesure 0,42m de haut, 0,24m de long et 0,13m de large et porte une croix gravée à son sommet.

Historique

Borne découverte par [F. Meyrat]{.creator} en [octobre 2020]{.date}.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Saroi]{.coverage} - Bai 018 - [Dolmen]{.type} ([?]{.douteux})

Localisation

Altitude : 420m

Historique

Ce monument, découvert par [nous]{.creator} en [février 1982]{.date}.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Saroi]{.coverage} - Bai 019 - [Tumulus pierreux]{.type} ([?]{.douteux}) 

Description

[E. Dupré]{.creator} cite un « tumulus pierreux » qui n'a pas été vu.

Par contre il a été noté un amas pierreux sur un terrain très en pente vers l'Est et délimité à l'Est et à l'Ouest par une bifurcation du sentier qui vient de Baihuntza D2 à 25m plus au sud. Ce « tumulus » pierreux de forme très irrégulièrement circulaire, (de 6m environ) présente un arbre décentré vers le Nord-Est.

Monument extrêmement douteux.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Saroi]{.coverage} - Bai 020 ([dolmen]{.type} détruit) 

Localisation

Altitude 405m.

Description

[E. Dupré]{.creator} cite un [dolmen]{.subject} détruit avec un tumulus de 5m. Aucun élément correspondant n'a été vu sur le terrain.
Monument très douteux.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Saroi]{.coverage} - Bai 021 - [Dolmen]{.type}

Localisation

Altitude 398m.

Historique

Ce [dolmen]{.subject} a été trouvé par [nous]{.creator} en [février 1982]{.date}.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Saroi]{.coverage} - Bai 022 - [Amas pierreux]{.type} sous [monolithe]{.type} ([?]{.douteux})

Localisation

Altitude 382m.

Description

[E. Dupré]{.creator} évoque « un monument sous monolithe » qui n'a pas été vu.

Monument très douteux.

## [Saint-Etienne-de-Baigorri]{.spatial} - Les monuments du secteur des Poteaux

Ce secteur n'a pas encore pu être exploré, compte tenu tout d'abord des conditions exubérantes du couvert végétal en cet automne et, ensuite, des exigences du "confinement" pour cause de Covid 19.

Nous donnons donc ici uniquement une liste fournie par [E. Dupré]{.creator}.

### [Saint-Etienne-de-Baigorri]{.spatial} - [Les Poteaux]{.coverage} - Bai 023 - [Coffre dolménique]{.type}

Description

Monument détruit

### [Saint-Etienne-de-Baigorri]{.spatial} - [Les Poteaux]{.coverage} - Bai 024 - [Coffre dolménique]{.type}

Description

Monument détruit

### [Saint-Etienne-de-Baigorri]{.spatial} - [Les Poteaux]{.coverage} - Bai 025 - [Coffre dolménique]{.type}

Description

Monument détruit

### [Saint-Etienne-de-Baigorri]{.spatial} - [Les Poteaux]{.coverage} - Bai 026 - [Coffre dolménique]{.type}

Description

Monument détruit

### [Saint-Etienne-de-Baigorri]{.spatial} - [Les Poteaux]{.coverage} - Bai 027 - [Coffre dolménique]{.type}

Description

Monument détruit

### [Saint-Etienne-de-Baigorri]{.spatial} - [Les Poteaux]{.coverage} - Bai 028 - [Coffre dolménique]{.type}

Description

Monument détruit

### [Saint-Etienne-de-Baigorri]{.spatial} - [Les Poteaux]{.coverage} - Bai 029 - [Cairn]{.type} 

Description

[cairn]{.subject} circulaire creusé de 6m de diamètre.
