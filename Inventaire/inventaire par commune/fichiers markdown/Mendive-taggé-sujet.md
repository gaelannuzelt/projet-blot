# Mendive

### [Mendive]{.spatial} - [Apanice]{.coverage} - [Tertre d'habitat]{.type}

Localisation

On le trouve à 35m environ au Sud-Ouest de la route et à quelques dizaines de mètres au Nord de l'embranchement qui conduit au menhir d'Ithurroché.

Description

Tertre de terre de 15m de diamètre environ, à sommet discrètement arrondi et de 0,30m de haut, au Nord-Est, et jusqu' à 1m au Sud-Ouest.

Historique

Tertre découvert par [A. Martinez Manteca]{.creator} en [juillet 2011]{.date}.

### [Mendive]{.spatial} - [Artzainharria]{.coverage} haut - [Tertres d'habitat]{.type} 

Localisation

Ils sont répartis sur le flanc nord d'une éminence qui fait face au pic de Béhorlégui ; et qui est séparée du *Groupe de TH* dit *du bas* par un petit col ou aboutit le chemin et où est construit le cayolar Idioineko Borda.

Description

Un ensemble de 11 tertres de terre est érigé sur cette pente assez marquée, séparés par des distances comprises entre 10m et 20m. Ces tertres circulaires, asymétriques, ont des diamètres de 7 à 8 m en moyenne et des hauteurs de 0,50m à 0,80m environ.

À noter, au sommet de cette éminence, à son extrémité Sud-Est, un petit tertre en bordure d'une dépression (artificielle ?).

Historique

Tertres découverts par [J. Blot]{.creator} en [avril 2013]{.date}.

### [Mendive]{.spatial} - [Artzainharria]{.coverage} bas - [Tertres d'habitat]{.type}

Localisation

De l'autre côté du col et donc au Nord-Est du cayolar Idioineko Borda.

Description

Quatre TH de terre, érigés sur la partie la plus élevée de cette prairie.

- *TH 1* : le plus grand, le plus visible, mesure 17m de diamètre et 1,50m de haut.

- *TH 2* : situé à 1m au Nord-Est ; moins net, mesure 11m de diamètre et 0,80m de haut.

- *TH 3* : situé à 10m au Sud-Est du n°1. ; mesure 11m de diamètre et 0,50m de haut.

- *TH 4* : situé à 10m au Sud du n°3, très visible, mesure 16m de diamètre et 1,40m de haut.

Historique

Tertres découverts par [Blot J.]{.creator} en [1971]{.date}, et contrôlés en [avril 2013]{.date}.

### [Mendive]{.spatial} - [Béhorlégi]{.coverage} 1 - [Cromlech]{.type}

Localisation

Carte 1346 est. Saint-Jean-Pied-de-Port.

Altitude 870m.

Un ensemble de 3 cromlechs et 1 tumulus est situé sur un replat à gauche de la route qui monte de Mendive au flanc sud du pic de Béhorlégi. Ce site domine un autre replat, en contre-bas et au sud, dénommé Sare-Sare.

Description

Le *cromlech n°1* mesure 6m de diamètre, est délimité par 12 pierres de [grés]{.subject} blanc, bien visibles ; il est érigé sur un sol en légère pente vers le sud-ouest et tangent à l'est au *n°2*.

Historique

Monument découvert en [avril 1973]{.date}.

### [Mendive]{.spatial} - [Béhorlégi]{.coverage} 2 - [Cromlech]{.type}

Localisation

Il est tangent au sud-est du cromlech *Béhorlégi 1 - Cromlech*.

Description

Cercle de 7,50m de diamètre, délimité par 10 pierres en grès blanc de 0,30m à 0,40m de haut.

Historique

Monument découvert en [avril 1973]{.date}.

### [Mendive]{.spatial} - [Béhorlégi]{.coverage} 3 - [Tumulus]{.type}

Localisation

À environ 80m à l'est du *Cromlech n°1*, aux deux tiers de la montée qui débute sur le replat où se trouvent les *cromlechs 1* et *2*.

Description

Tumulus mixte de terre et de pierres, érigé sur un sol en légère pente vers le nord-ouest ; il mesure 3,70m de diamètre et 0,40m de haut.

Historique

Monument découvert en [avril 1973]{.date}.

### [Mendive]{.spatial} - [Béhorlégi]{.coverage} 4 - [Cromlech]{.type}

Localisation

À environ 130m au sud-est du cromlech *Béhorlégi 2 - Cromlech*, il est situé sur la piste pastorale qui a un parcours légèrement ascendant quand on vient du *n°2*.

Description

Cercle de 6m de diamètre, délimité par 22 pierres bien visibles, atteignant 0,30m à 0,40m de haut. On distingue très nettement 4 pierres centrales.

Historique

Monument découvert en [avril 1973]{.date}.

### [Mendive]{.spatial} - [Burkidoy]{.coverage} Ouest - [Tumulus]{.type}

Localisation

Altitude : 1071m

Ce tumulus est situé au sommet d'un promontoire qui domine la D 19, à l'ouest du mont Burkidoy. Ce monument, ainsi que le suivant, n'est visible que lorsque les fougères sont à leur minimum...

Description

Tumulus terreux, en forme de galette aplatie, mesurant 3,50m de diamètre et 0,40m de haut. À sa périphérie Nord-Est se voit une pierre plantée dont il est difficile de dire quels sont ses rapports avec le tumulus...

Historique

Tumulus découvert par [J. Blot]{.creator} en [février 2020]{.date}.

### [Mendive]{.spatial} - [Burkidoy]{.coverage} Ouest - [Cromlech]{.type} ([?]{.douteux})

Localisation

Ce cromlech est situé à environ 5m au Nord-Est de *Burkidoy Ouest - Tumulus*.

Description

Cinq pierres de volume très variable, délimitent un cercle de 3m de diamètre, plus fourni dans le secteur Nord-Est. Monument douteux.

Historique

Monument découvert par [J. Blot]{.creator} en [février 2020]{.date}.

### [Mendive]{.spatial} - [Burkidoy]{.coverage} - [Tertres d'habitat]{.type} (nb = 4)

Localisation

Alt : 1291m.

À environ 300mau nord du cayolar Cihigolatzé, en suivant l'ancienne piste qui monte vers les pâturages, (au-dessus du tracé actuel, qui s'en détache environ 80m au Nord du cayolar).

Description

Tous ces tertres sont érigés sur un terrain en pente vers le Sud-Ouest.

- *Tertre n°1* : Il est situé à une trentaine de mètres au Nord-Ouest des ruines bien visibles d'un très ancien cayolar, où aboutissait une bretelle de l'ancienne piste. Mesure 5m de diamètre et 0,50m de haut.

- *Tertre n°2* : à 3m à l'Ouest du précédent, de l'autre côté de la piste, et mêmes mensurations.

- *Tertre n°3* : à 4m au Nord Nord-Est du n°1 ; mesure 3m de diamètre et 0,30m de haut.

- *Tertre n°4* : situé à 9m à l'Ouest Sud-Ouest du n°2 ; mesure 3m de diamètre et 0,30m de haut.

Historique

Tertres découverts par [Blot J.]{.creator} en [1975]{.date}.

### [Mendive]{.spatial} - [Calichaga]{.coverage} - [Cromlech]{.type}

Localisation

Carte 1346 est. Saint-Jean-Pied-de-Port

Altitude 489m.

Ce monument est situé sur un pâturage à 90m au Nord-Ouest du virage de la route qui va de Mendive à Ahuski, par Armiague, au moment où elle passe du flanc nord au flanc sud de la ligne de croupes baptisée *Calichaga eta Pegaretta*.

Description

Cercle de 7m de diamètre, délimité par 14 pierres au ras du sol, mais nettement visibles. Le monument a été détérioré par la pose d'une barrière barbelée qui le coupe selon un axe est-ouest dans son secteur nord.

Historique

Monument découvert en [avril 1973]{.date}.

### [Mendive]{.spatial} - [Calichaga]{.coverage} - [Tumulus]{.type}

Localisation

Alt : 489m.

Il se trouve au point culminant du même replat que le cromlech de ce nom et à 15m au Sud-Est de la première barrière de barbelés (qui coupe ce dernier).

Description

Tumulus de terre de 3,30m de diamètre et 0,30m à 0,40m de haut.

Historique

Monument découvert par [Blot J.]{.creator} en [août 2011]{.date}.

### [Mendive]{.spatial} - [Cihigolatzé]{.coverage} - [Tertre d'habitat]{.type}

Localisation

Alt : 1241m

Le tertre est situé à une trentaine de mètres à l'Ouest des ruines du cayolar, et donne sur la rive droite du ruisseau en contre-bas.

Description

Tertre bien visible, de 8m de diamètre environ et 0,40m de haut.

Historique

Monument découvert par [Blot J.]{.creator} en [avril 2014]{.date}.

### [Mendive]{.spatial} - [Egurce]{.coverage} - [Monolithe]{.type}

Localisation

Alt : 995m.

Il est situé dans le deuxième petit col après la montée.

Description

Belle dalle de [grés]{.subject} parallélépipédique gisant au sol suivant un axe Ouest-Est, mesurant 2,30m de long, 0,90m de large, 0,27m d'épaisseur et présentant des traces d'épannelage sur tout son pourtour semble-t-il.

Historique

Monolithe découvert par [Alfonso Martinez Manteca]{.creator} en [juillet 2011]{.date}.

### [Mendive]{.spatial} - [Egurce]{.coverage} - [Tertres d'habitat]{.type}

Localisation

Alt : 970m

Un premier ensemble de 6 TH se trouve au niveau d'un premier petit col après la montée ; 4 autres se trouvent avant un deuxième col, enfin 2 ou 3 autres TH sont érigés au nord du monolithe ci-après décrit dans ce 2^è^ col.

Description

Mêmes formes et dimensions variables que *ceux d'Egurcekobidea*.

Historique

Tertres découverts par [A. Martinez Manteca]{.creator} en [juillet 2011]{.date}.

### [Mendive]{.spatial} - [Egurcekolepoa]{.coverage} 1 - [Tertre d'habitat]{.type}

Localisation

Alt : 913m

Erigé sur la gauche du col, en montant.

Description

Tertre de terre d'une quinzaine de mètres de diamètre, et 3m environ dans sa plus grande hauteur, à l'Est.

Historique

Tertre découvert par [A. Martinez Manteca]{.creator} en [juillet 2011]{.date}.

### [Mendive]{.spatial} - [Egurcekobidea]{.coverage} - [Tertres d'habitat]{.type}

Localisation

Alt : 970m

Cette trentaine de tertres est échelonnée de part et d'autre d'une piste qui, partant du col d'Egurce, monte vers l'Est.

Description

Certains sont très marqués, d'autres beaucoup plus érodés, les dimensions varient entre 12m et 15m de diamètre, pour des hauteurs variables.

Historique

Tertres découverts par [A. Martinez Manteca]{.creator} en [juillet 2011]{.date}.

### [Mendive]{.spatial} - [Gahalarbe]{.coverage} 2 - [Tumulus]{.type}

Localisation

Même carte.

Altitude : 947m.

Nous avons publié [@blotNouveauxVestigesMegalithiques1972a, p.50], le *Cromlech n°1*, qui a été ultérieurement détruit par l'aménagement de la piste pastorale.

Altitude : 970m.

Ce *tumulus n°2* est situé à environ 100m au Nord Nord-Est du *n°1*, sur la même crête, érigé sur terrain plat.

Description

Tumulus ovale à grand axe Nord-Sud, (axe de la crête), mesurant 10m pour ce dernier, et 7,30m pour le plus petit, et une hauteur de 0,90m. Il s'agit d'un tumulus pierreux, fait de blocs de schiste de la taille d'un petit pavé ; il est longé par la piste pastorale à son flanc Ouest.

Historique

Monument découvert en [avril 1972]{.date}.

### [Mendive]{.spatial} - [Ilhareko-lepoa]{.coverage} 1 - [Tumulus]{.type}

Localisation

Carte 1446 ouest. Ordiarp.

Altitude : 926m.

*Ilhareko-lepoa 1* et *2* sont érigés au sommet d'une petite colline qui domine, au sud-ouest, le col de ce nom, dénomination qui avait d'ailleurs attiré notre attention (le col des morts), mais malgré plusieurs visites en ces lieux, nous n'avions rien remarqué. Ce n'est que par une belle soirée d'automne, avec l'éclairage à jour frisant du soleil couchant, que nous sont apparus les deux tumulus. Nous pensons que, compte tenu de la difficulté à les voir pour un œil non averti, la dénomination du lieu remonte à l'époque de la construction de ces monuments funéraires.

*Ilhareko-lepoa 1* est à l'extrémité sud-est de cette colline.

Description

Tumulus aplati en galette de 8m de diamètre et 0,40m de haut ; 4 pierres apparaissent dans le secteur est du monument.

Historique

Monument découvert en [octobre 1971]{.date}.

### [Mendive]{.spatial} - [Ilhareko-lepoa]{.coverage} 2 - [Tumulus]{.type}

Localisation

Situé à À 15m au nord-ouest de *Ilhareko-lepoa 1*.

Description

Petit tumulus de 5m de diamètre, aplati en galette lui aussi, de 0,40m de haut. Quelques pierres apparaissent dans le quart Nord-Ouest, et au centre.

Peut-être y aurait-il encore un troisième tumulus à 5m au sud de ce *tumulus n°2*, de 4m de diamètre et 0,50m de haut, sans que l'on puisse l'affirmer.

Historique

Monument découvert en [octobre 1971]{.date}.

### [Mendive]{.spatial} - [Irati-Soro]{.coverage} Nord 3 - [Tumulus]{.type}

Localisation

Carte 1446 ouest. Ordiarp.

Altitude : 1027m

Deux cromlechs très voisins ont été décrits [@barandiaranCronicaPrehistoria1952, p. 158], mais ont semble-t-il été détruits depuis par le passage de la route.

Ce tumulus est tout proche de la confluence des ruisseaux Ataramatze et Irati, et à droite de la route qui monte aux chalets d'Irati.

Description

Tumulus pierreux de 11m de diamètre et 1,50m de haut environ ; son centre présente une excavation de 5m de diamètre et 1m de profondeur, trace d'une ancienne fouille. Le versant nord-est du tumulus domine le ruisseau, le versant sud-ouest le chemin pastoral.

Historique

Monument découvert en [juillet 1971]{.date}.

### [Mendive]{.spatial} - [Irati-Soro]{.coverage} (N) - [Tertre d'habitat]{.type}

Localisation

Altitude : 1027m

Il est situé à une quinzaine de mètres à l'Ouest du tumulus *T3*.

Description

Tertre de 7m de diamètre et 1m de haut.

Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

### [Mendive]{.spatial} - [Irati-Soro]{.coverage} Nord - [Tertres d'habitat]{.type}

Localisation

Altitude : 1038m

Ils sont échelonnés le long de la rive droite de l'Iratiko erreka, avant qu'il ne soit traversé par la route qui mène aux chalets d'Irati, au moment où elle amorce sa montée, après avoir laissé une bretelle vers le chalet Pedro.

Description

Huit tertres, dont un bon nombre sont de faible hauteur.

- *Tertre n°1* : correspond aux coordonnées ci-dessus indiquées. Mesure 10m de diamètre, hauteur faible.

- *Tertre n°2* : situé à 16m au Nord du précédent. Mesure 6m de diamètre.

- *Tertre n°3* : situé à 20m au Nord Nord-Est du précédent. Mesure 10m x 8m.

- *Tertre n°4* : situé à 10m au Nord Nord-Est du précédent. Mesure 7m de diamètre.

- *Tertre n°5* : situé à 60m au Nord-Est du précédent. Mesure 9m de diamètre.

- *Tertre n°6* : situé à 4m au Nord-Est du précédent. Mesure 10m x 8m de diamètre.

- *Tertre n°7* : situé à 4m au Nord Nord-Est du précédent. Mesure 8m x 5m.

- *Tertre n°8* : situé à 4m au Nord Nord-Est du précédent. Mesure 8m de diamètre.

Historique

Tertres découverts par [Blot J.]{.creator} en [1975]{.date}.

### [Mendive]{.spatial} - [Iratiko erreka]{.coverage} 1 - [Tertre d'habitat]{.type}

Localisation

Altitude : 1056m

Tertre situé à une cinquantaine de mètres au sud de la route qui rejoint le plateau d'Irati aux motels, et à une centaine de mètres environ à l'Est du Tumulus (probablement dolménique) dit *T3*. Il est à 8m à l'Ouest d'un petit ru.

Description

Tertre terreux asymétrique, incliné vers le Sud-Est, mesurant 8m de long, 5m de large et 0,80m de haut.

Historique

Monument découvert par [J. Blot]{.creator} en [avril 2015]{.date}.

### [Mendive]{.spatial} - [Iratiko erreka]{.coverage} 2 - [Tertre d'habitat]{.type}

Localisation

Altitude : 1054m

Il est situé à 90 mètres environ au Sud-Ouest du précédent et à 15m à l'Ouest du petit riu déjà cité.

Description

Tertre asymétrique terreux de 6m de long, 4,50m de large et 0,20m de haut environ.

Historique

Monument découvert par [J. Blot]{.creator} en [avril 2015]{.date}.

### [Mendive]{.spatial} - [Ithurroché]{.coverage} - [Tertres d'habitat]{.type}

Localisation

Ces 4 tertres sont situés au Nord d'une très importante doline et ils dominent de quelques mètres la piste pastorale antique qui s'étend du nord au sud du vallon que longe, à l'est, la route actuelle.

- Les *tertres 1* et *2*

    Altitude : 1000m. Sont sur un sol en pente vers l'Est alignés dans le sens Est-Ouest, et tangents, le 2 étant plus en altitude que le 1.

- Les *tertres 3* et *4* 

    Ils forment le deuxième groupe, à 15m au Sud du premier ; ils sont disposés de la même manière, le 3 étant plus en hauteur que le 4.

Description

Ils mesurent entre 7m et 9m de diamètre et 0,90m de haut.

Historique

Tertres découverts par [J. Blot]{.creator} en [1971]{.date}, et contrôlés en [avril 2013]{.date}.

### [Mendive]{.spatial} - [Olhazarre]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 810m

Situé dans un petit col au Nord de la route qui monte de Mendive, et à 7m au sud de l'ancienne piste de crête.

Description

Tumulus de terre et de pierres de 9m de diamètre et 0,40m de haut, présentant une dépression centrale de 3m de large et de 0,30m de profondeur environ. Un bloc rocheux, qui ne paraît pas en rapport avec ce monument, occupe une partie Sud-Est de la dépression centrale.

Historique

Monument découvert par [J. Blot]{.creator} en [mai 2011]{.date}.

### [Mendive]{.spatial} - [Saint-Sauveur-d'Irati]{.coverage} - [Tumulus]{.type}

Localisation

Carte 1346 est. Saint-Jean-Pied-de-Port.

Altitude 900m.

Ce tumulus, actuellement surmonté de la croix d'un calvaire, est situé à quelques mètres au Nord Nord-Est de la chapelle Saint-Sauveur.

Description

On note, sous le calvaire une butte plus ou moins informe qui est le reliquat d'un tumulus « préhistorique » signalé par le Cdt Rocq [@rocqEtudePeuplementPays1935, p.371]. D'après lui, lors de la réfection de la chapelle, de nombreux ossements furent trouvés dans ce tumulus ; le curé voulut donner une sépulture chrétienne à ces «païens», et la croix fut installée. On ignore tout de l'époque de cette sépulture, mais nous avons préféré la signaler ici, avec les réserves qui s'imposent.

Historique

Monument signalé par le [Cdt Rocq]{.creator} en [1935]{.date}.

### [Mendive]{.spatial} - [Sare-Sare]{.coverage} - [Cromlech]{.type} ([?]{.douteux})

Localisation

Altitude : 806m

Il est au beau milieu du replat que forme la croupe de Sare au flanc sud du pic de Béhorlégui.

Description

Cercle de pierre peu visible, de 9m de diamètre, délimité par 7 pierres au ras du sol ; elles sont absentes dans les deux quarts Sud-Est et Sud-Ouest. Monument douteux.

Historique

Monument découvert par [Meyrat F.]{.creator} en [avril 2013]{.date}.

### [Mendive]{.spatial} - [Sare-Sare]{.coverage} - [Tertre d'habitat]{.type}

Localisation

Altitude : 870m.

On peut le voir au bord de la route actuelle, dont il touche le bord Sud.

Description

Quoiqu'en partie détérioré par la route, ce tertre de terre et de pierres affecte la forme classique du tertre circulaire dissymétrique, mesurant 12m de diamètre pour un dénivelé de 2m à 3m : il ne semble pas que nous soyons devant le résultat d'un amas dû aux engins de terrassement lors de la construction de la route.

Historique

Monument trouvé par [Blot J.]{.creator} en [avril 2013]{.date}.

### [Mendive]{.spatial} - [Xuberaxain]{.coverage} - [Tertres d'habitat]{.type}

Localisation

Il y a 8 TH se trouvant dans la prairie qui surplombe le *Dolmen de Xuberaxain Harria*.

- *Xuberaxain TH1* - Altitude : 612m.

- *Xuberaxain TH2* - Altitude : 608m.

- *Xuberaxain TH3* - Altitude : 620m. Plus haut situé que les précédents, relief peu marqué

- *Xuberaxain TH4* - Altitude : 623m. À quelques mètres au-dessus du n° 3 - relief peu marqué.

- *Xuberaxain TH5* - Altitude : 600m. Le plus bas situé-Relief mieux marqué.

- *Xuberaxain TH6* - Altitude : 612m. Relief peu marqué

- *Xuberaxain TH7* - Altitude : 620m. Relief très visible - à proximité des bâtiments agricoles.

- *Xuberaxain TH8* - Altitude : 622m. Relief très net - plus en altitude que le précédent - à proximité et au même niveau que les bâtiments agricoles.

Historique

- Xuberaxain TH3, TH4 et TH5 : trouvés par [Blot J.]{.creator} [avril 2013]{.date}.

- Xuberaxain TH7 et TH8 : Trouvés par le [groupe Hilharriak]{.creator} en [avril 2013]{.date}.

### [Mendive]{.spatial} - [Xuberaxain]{.coverage} - [Tertres d'habitat]{.type}

Localisation

Altitude : 630m

Ces deux tertres sont à une centaine de mètres au Nord-Est du *dolmen Xuberaxain*, sur un terrain en pente.

Description

- Le *Tertre n°1*, le plus net mesure une quinzaine de mètres de diamètre. Du fait de la pente il est asymétrique, sa hauteur moyenne étant d'environ 0,70m.

- Le *Tertre n°2* est à une trentaine de mètres à l'E du précédent. Il est plus modeste (10m de diamètre), et moins marqué en hauteur.

Historique

Tertres découverts par [Blot J.]{.creator} en [décembre 2011]{.date}.
