# Tardets-Sorholus

### [Tardets-Sorholus]{.spatial} - [Juge]{.coverage} 1 - [Tumulus]{.type}

Localisation

Altitude : 573m.

Sur la ligne de crête de la longue croupe allongée d'Ouest en Est, au flanc Est du sommet de La Madeleine. Il est à environ 100m à l'Est de la maison « Juge ».

Description

Tumulus (ancien dolmen ?) de terre et de pierres abondantes, de 18m de diamètre environ et, en moyenne, de 1m de haut érigé sur terrain plat ; au centre on peut voir une légère dépression circulaire de 2,40m de diamètre. Tout au bord et au Nord de celle-ci est construit sur ce monument, un important poste de tir à la palombe.

Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

Ajoutons ici que le *Dolmen de Bordetta* [@blotSouleSesVestiges1979, p.26], situé sur cette même ligne de crête, plus à l'Est, a été rasé par les travaux agricoles. Il n'en reste que la silhouette du tumulus qui faisait 26m de diamètre et 1,50m de haut.

Altitude : 563m

### [Tardets-Sorholus]{.spatial} - [Juge]{.coverage} 2 - [Tumulus]{.type} ([?]{.douteux})

Localisation

Altitude : 562m.

Situé au Sud-Est du précédent, à proximité immédiate d'une clôture de barbelés.

Description

Petit tumulus terreux de 4m de diamètre et 0,50m de haut.

Historique

Tumulus découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

### [Tardets-Sorholus]{.spatial} - [Juge]{.coverage} 3 - [Tumulus]{.type} ([?]{.douteux})

Localisation

A 7m au Nord de *Juge 2 - Tumulus (?)*.

Description

Constitué de terre et de pierraille, il mesure 7m de diamètre et 0,80m de haut.

Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.
