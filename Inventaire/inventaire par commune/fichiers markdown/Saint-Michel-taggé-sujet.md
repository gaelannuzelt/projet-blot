# Saint-Michel

### [Saint-Michel]{.spatial} - [Arboze]{.coverage} 1 - [Tumulus]{.type}

Localisation

Altitude : 525m.

Il est situé à droite de la petite route qui se détache de la « voie romaine » après le Honto et se dirige, à droite, vers les collines de Arboze (ou Arbosse) et de Coscohandi. On le trouve donc après le *dolmen d'Arboze* décrit par nous en 1972 et 1978 [@blotNouveauxVestigesMegalithiques1972b, p.162] et [@blotVestigesProtohistoriquesVoie1978, p.54].

Altitude : 530m.

Description

Vaste tumulus circulaire de terre mesurant une trentaine de mètres de diamètre et plus de 2m de haut ; on note les vestiges d'une excavation dans le secteur Sud de sa partie supérieure, de 4m de long et 3m de large, profonde de 0,30m à 0,40m.

À la base du monument, longeant son flanc Nord-Est, on distingue très nettement les traces d'un « cheminement » pour ne pas dire d'une voie, qui se prolonge vers le Nord Nord-Ouest, dans le champ voisin... L'époque de la construction du tumulus, et sa finalité... comme celle de cette voie sont dans l'état actuel impossibles à préciser.

Historique

Monument découvert par [Blot J.]{.creator} en [août 2011]{.date}.

### [Saint-Michel]{.spatial} - [Arboze]{.coverage} 2 - [Tumulus]{.type}

Localisation

Tangent au Nord-Ouest à *Arboze 1 - Tumulus*.

Description

Il est de dimensions plus modestes : 15m de diamètre et 0,80m de haut environ. Il est tout aussi énigmatique que *Arboze 1 - Tumulus*.

Historique

Monument découvert par [Blot J.]{.creator} en [août 2011]{.date}.

### [Saint-Michel]{.spatial} - [Arnostégui]{.coverage} (Cabane d') - [Tertre d'habitat]{.type}

Localisation

Altitude : 1136m.

Ce tertre est situé juste au-dessus du cours débutant du ruisseau Oillascoa à une quarantaine de mètres au Nord des bâtiments de la cabane d'Arnostégui.

Description

Le tertre lui-même, de forme ovale à grand axe Nord-Sud mesure 8m de long, 4m de large, et son sommet arasé ne dépasse que de peu la surface du sol ambiant. Il présente les traces d'une construction ancienne, de forme rectangulaire, actuellement matérialisée par une vingtaine de blocs de [grés]{.subject}, de volume variable profondément enfouis dans le sol, délimitant un espace de 4,30m x 2,50m.

Historique

Monument trouvé par [A. Martinez]{.creator} en [juillet 2019]{.date}.

### [Saint-Michel]{.spatial} - [Arnostégui]{.coverage} 4 - [Tumulus]{.type}

Localisation

Altitude : 1316m.

Ce monument est situé, en Navarre, mais tangent à la ligne frontière, au Sud de celle-ci, entre les BF 203 et 202 et plus près de cette dernière. Comme toujours dans les cas semblables, nous faisons la description ici de ces monuments.... existant bien avant les tracés frontaliers actuels ! Il complète la liste des 3 monuments déjà relevés dans ce secteur [@boucherNotesProspectionMegalithique1968, p.75].

Description

Tumulus terreux, en forme de galette circulaire de 5,50m de diamètre et 0,40m de haut, érigé sur terrain plat. Trois pierres sont visibles dans le secteur Nord-Ouest et deux dans le secteur Sud-Est, sans que l'on puisse vraiment parler de péristalithe.

Historique

Monument découvert par [F. Meyrat]{.creator} en [août 2018]{.date}.

### [Saint-Michel]{.spatial} - [Arnostégui]{.coverage} 5 - [Tumulus]{.type} ([?]{.douteux})

Localisation

En territoire Navarrais.

Altitude : 1321m.

Monument douteux, situé à 60m à l'Est Sud-Est de *Arnostégui 4 - Tumulus*, au sommet d'une petite éminence, et à une cinquantaine de mètres au Sud de la ligne frontière.

Description

Monument en forme de galette très aplatie de 4,50m de diamètre et 0,10m de haut, dont la partie centrale présente une légère dépression. Deux pierres sont visibles en secteur Sud.

Historique

Monument découvert par [F. Meyrat]{.creator} en [août 2018]{.date}.

### [Saint-Michel]{.spatial} - [Atheketa]{.coverage} - [Tertres d'habitat]{.type}

Nous citons ici ce groupe de 3 tertres d'habitat, dont la localisation peu précise dans le [@blotEtatConnaissancesArcheologiques1978], en rendait la découverte sur le terrain très difficile. Par contre la description est toujours valable.

### [Saint-Michel]{.spatial} - [Azqueta]{.coverage} - [Tertres d'habitat]{.type}

Localisation

Altitude : 937m.

Situés sur une éminence qui domine, à l'Ouest, la piste se rendant au cayolar Asketa.

Description

Ensemble de 3 tertres tangents édifiés en rupture de pente, alignés selon un axe Nord-Sud, constitués de terre, et mesurant chacun environ 12m à 14m x 9m et 0,30m de haut.

Historique

Tertres découverts par [Blot J.]{.creator} en [septembre 1980]{.date}.

### [Saint-Michel]{.spatial} - [Azqueta]{.coverage} 4 - [Tertre d'habitat]{.type}

Localisation

Altitude : 903 m.

Il est tangent au côté Nord de la route venant d'Orisson à sa jonction avec la piste qui se rend au cayolar d'Asketa.

Description

Tertre « classique », terreux, demi-circulaire, en rupture de pente, mesurant 8m de diamètre et 0,50m de haut environ.

Historique

Tertre découvert par [C. Blot]{.creator} en [novembre 2018]{.date}.

### [Saint-Michel]{.spatial} - [Azqueta]{.coverage} 1 - [Tumulus]{.type}

Localisation

Altitude : 952m.

Il est situé en terrain plat, à une trentaine de mètres au Sud-Ouest de *Azqueta - Tertres d'habitat*.

Description

Tumulus terreux, circulaire, aplati, mesurant 10m x 10m et 0,20m à 0,30m de haut. Son centre présente une vaste dépression, mais peu profonde, s'étendant vers la partie Est du monument.

Historique

Tumulus découvert par [Meyrat F.]{.creator} en [octobre 2018]{.date}.

### [Saint-Michel]{.spatial} - [Azqueta]{.coverage} 2 - [Tumulus]{.type}

Localisation

Altitude : 956m.

Il est situé à environ une trentaine de mètres au Sud-Ouest de *Azqueta 1 - Tumulus*, légèrement plus en altitude.

Description

Tumulus de terre, édifié sur un terrain en très légère pente, de forme plutôt ovalaire, aplati, mesurant 10m x 8m et 0,30m de haut.

Historique

Tumulus découvert par [Meyrat F.]{.creator} en [octobre 2018]{.date}.

### [Saint-Michel]{.spatial} - [Azqueta (cayolar)]{.coverage} - [Tertre d'habitat]{.type} ([?]{.douteux})

Localisation

Altitude : 916m.

Description

Ces 3 éléments sont situés à l'arrière des cayolars d'Azketa, à flanc de la colline qui les surmonte à l'Ouest. Ils mesurent entre 12m et 14m x 9m. Il nous semble qu'il s'agit plus d'amoncellements de déblais consécutifs au nettoyage d'aires de traite que de véritables tertres d'habitat.

Historique

Eléments signalés par [Meyrat F.]{.creator} en [octobre 2018]{.date}.

### [Saint-Michel]{.spatial} - [Azqueta]{.coverage} - [Faux tumulus]{.type}

Localisation

Altitude : 943m.

Au sommet d'une petite éminence dominant au Sud la jonction de la piste venant du cayolar Asketa avec la route venue d'Orisson.

Description

Tumulus circulaire de terre et pierres de 3,50m de diamètre et 0,40m de haut. Cependant nous pensons qu'il s'agit très probablement d'un filon rocheux naturel, modelé par l'érosion des intempéries.

Historique

Elément découvert par [Blot J.]{.creator} en [novembre 2018]{.date}.

### [Saint-Michel]{.spatial} - [Elhursaro]{.coverage} Nord - [Tertres d'habitat]{.type} 

aussi appelé les *TH de Zerkupé*.

[Nous]{.creator} avions publié en [1972]{.date}, dans le [@blotNouveauxVestigesMegalithiques1972b p.178- 180], les 6 tertres d'habitat d'Elhursaro (ou Zerkupé), sous la rubrique *Les tumulus d'Oriune (Orion)*.

À notre passage en Janvier 2018, nous avons noté la destruction du *tertre n°1* (17m x 10m et 1m de haut), lors de l'édification d'un parc à brebis en parpaings.

### [Saint-Michel]{.spatial} - [Elhursaro]{.coverage} Sud - [Tertre d'habitat]{.type}

Localisation

Altitude : 945m.

Ce tertre se trouve sur un petit replat à 60m au Nord-Ouest des deux cabanes dElhursaro les plus au Sud et à l'Est du groupe ; il est à 3m au Sud-Ouest d'un pointement rocheux calcaire qui entoure sa moitié Est.

Description

Très beau tertre, de forme ovale, de 16m de long, 8m de large et 1,70m de haut, érigé sur un sol plat. Il n'y a aucune pierre apparente.

Historique

Tertre trouvé par [Blot J.]{.creator} en [février 2018]{.date}.

### [Saint-Michel]{.spatial} - [Harrixuri]{.coverage} - [Tertre d'habitat]{.type}

Localisation

Altitude : 940m.

Il est sur un petit replat, à une trentaine de mètres au Nord de la route qui va au col d'Irey.

Description

Petit tertre asymétrique, très érodé semble-t-il, de 8m de diamètre environ et 0,35m de haut.

Historique

Monument découvert par [Blot J.]{.creator} en [novembre 2011]{.date}.

### [Saint-Michel]{.spatial} - [Harrietta]{.coverage} - [Pierre plantée]{.type}

Localisation

Altitude : 900m.

Elle est plantée au milieu du petit col où passe la piste pastorale, et à quelques mètres au Nord du *cromlech Sohandi 2* ayant fait l'objet d'une fouille en 1984 ( Blot J.).

Description

Bloc de calcaire, de forme parallélépipédique, orienté selon un axe Sud-Est Nord-Ouest, mesurant 0,98m de haut, 1,40m de long et 0,40m de large à la base ; il semble que l'on puisse parler de pierres de calage du côté Est.

Historique

Pierre découverte par [Blot J.]{.creator} en [juillet 2011]{.date}.

### [Saint-Michel]{.spatial} - [Harrietta]{.coverage} - [Tumulus]{.type} ([?]{.douteux})

Localisation

Altitude : 903m.

Description

Monument douteux se présentant sous la forme d'un tumulus d'environ 6,50m de diamètre et 0,40m de haut formé de terre et de pierres. Une dizaine de celles-ci, de volume parfois important, paraissent disposées de façon circulaire... Monument ou mouvement naturel du terrain ?

Historique

Elément découvert par [Blot J.]{.creator} en [août 2011]{.date}.

### [Saint-Michel]{.spatial} - [Harrietta]{.coverage} - [Tertres d'habitat]{.type}

Localisation

Altitude : 903m.

On note un ensemble de 4 tertres d'habitat se succédant de haut en bas à partir de l'éminence qui domine à quelques dizaines de mètres à l'Ouest Sud-Ouest la pierre plantée du col.

Description

- *Le 1^er^ tertre* : Au sommet de l'éminence. Le plus imposant : allongé suivant un grand axe Nord-Ouest Sud-Est, il mesure 19m, et dans sa partie déclive, vers le Nord-Est, il a environ 3m de haut.

- *Le 2^ème^ tertre* fait suite au précédent et lui est tangent. On peut lui attribuer 9m de long et 1m de haut à son versant Nord-Est.

- *Le 3^ème^ tertre*, tangent au précédent, mesure 11m de long et 80 de haut.

- *Le 4^ème^ tertre* a les mêmes caractéristiques que le précédent. Il se trouve à 10m au Sud Sud-Est du *cromlech Sohandi 2*, fouillé en 1984

Historique

Tertres découverts en [août 2011]{.date} par [J. Blot]{.creator}.

### [Saint-Michel]{.spatial} - [Irei (Col d')]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 751m.

Il est situé sur le plat du col d'Irei, à l'extrémité Nord de celui-ci, à côté de nombreux vestiges de travaux défensifs anciens ; toutefois il s'en distingue facilement et la confusion n'est pas possible. Il est à 4m au Sud Sud-Ouest d'une aubépine et à 21m à l'Ouest d'un pointement rocheux.

Description

Tumulus terreux circulaire, en forme de dôme, mesurant 4,10m de diamètre et 0,40m de haut.

Historique

Tumulus découvert en [février 2018]{.date} par [C. Blot]{.creator}.

### [Saint-Michel]{.spatial} - [Itsasegui]{.coverage} Sud - [Tertre d'habitat]{.type}

Localisation

Altitude : 1070m.

Situé à mi-pente du versant Nord du mont Itsaségui, au bord Ouest de la naissance d'un ravin.

Description

Tertre de terre, très net, de 15m de diamètre et de plus de 2m de haut à son versant Nord.

Historique

Tertre découvert par [Blot J.]{.creator} en [août 2011]{.date}.

### [Saint-Michel]{.spatial} - [Itsasegui]{.coverage} 2 - [Tumulus]{.type}

Localisation

Altitude : 1161m.

Il est situé à l'extrémité Sud-Ouest du replat sommital.

Description

Tumulus de terre et de pierres de 5m de diamètre et 0,30m à 0,40m de haut environ. Les pierres sont plus abondantes dans la moitié Est et paraissent disposées de façon circulaire.

Historique

Tumulus découvert par [Blot J.]{.creator} en [2011]{.date}.

### [Saint-Michel]{.spatial} - [Itsasegui]{.coverage} Est - [Tertre d'habitat]{.type}

Localisation

Altitude : 812m.

Ce monument se trouve à environ 120m à l'Est du sommet de l'Itsaségui, et domine l'horizon.

Description

Érigé sur un terrain en pente douce vers l'Est, ce volumineux tertre, terreux, de forme ovale, mesure 14m dans son grand axe (Nord-Sud), 12m de large et environ 1m de haut.

Historique

Tertre découvert par [A. Martinez]{.creator} en [juillet 2019]{.date}.

### [Saint-Michel]{.spatial} - [Jatsagune]{.coverage} 1 - [Cromlech]{.type}

Localisation

Altitude : 1224m.

Il est à 3m à l'Ouest du tracé du chemin de Compostelle.

Description

Cercle de 4,30m de diamètre, délimité par une douzaine de petites pierres, peu visibles ; on note une très légère dépression centrale.

Historique

Cercle découvert par le [groupe Hillariak]{.creator} en [juillet 2013]{.date}.

### [Saint-Michel]{.spatial} - [Jatsagune]{.coverage} 2 - [Cromlech]{.type}

Localisation

Altitude : 1226m.

Il est à 20m au Nord Nord-Est de Jatsagune 1 - Cromlech, et tangent, à l'Ouest au chemin de Compostelle.

Description

Petit cercle de 2m de diamètre délimité par une douzaine de pierres au ras du sol, mais bien visibles.

Historique

Monument découvert par [Blot J.]{.creator} en [septembre 2014]{.date}.

### [Saint-Michel]{.spatial} - [Jatsagune]{.coverage} 1 - [Tumulus]{.type}

Localisation

Altitude : 1234m.

Ce groupe de 3 petits tumuli tangents est à 20m à l'Ouest Sud-Ouest du poste de chasse érigé sur un tertre d'habitat *Urdanarre - Tertre d'habitat* et à 25m à l'Ouest de la « route des cîmes ».

Description

Petit tumulus de 3,50m de diamètre et 0,25m de haut, sans pierres apparentes.

Historique

Cet ensemble de 3 tumuli a été trouvé en [septembre 2014]{.date} par [F. Meyrat]{.creator}.

### [Saint-Michel]{.spatial} - [Jatsagune]{.coverage} 2 - [Tumulus]{.type}

Localisation

Tangent à l'Ouest de *Jatsagune 1 - Tumulus*.

Description

Tumulus de 3m de diamètre et 0,30m de haut, à la surface duquel apparaissent 8 pierres, au ras du sol, ne formant pas un péristalithe.

### [Saint-Michel]{.spatial} - [Jatsagune]{.coverage} 3 - [Tumulus]{.type}

Localisation

Tangent à l'Ouest Nord-Ouest de *Jatsagune 2 - Tumulus*.

Description

Tumulus de 2,50m de diamètre et 0,10 à 0,15m de haut, avec 4 pierres visibles en surface, au ras du sol.

### [Saint-Michel]{.spatial} - [Landarre]{.coverage} - [Tertre d'habitat]{.type} ([?]{.douteux})

Localisation

Altitude : 1082m.

Situé sur un petit replat du versant Nord du mont Itsaségui, ce « tertre » est situé à une dizaine de mètres plus en altitude que le tertre que nous avons décrit et publié en 2012 dans [@blotInventaireMonumentsProtohistoriques2012, p.19] sous le nom de *TH Itsaségui Sud* (altitude : 1070m), par rapport à d'autres tertres d'habitat identifiés plus bas au niveau du col et dénommés *Itsaégui Nord*.

Description

Il se présente sous la forme d'un grand tertre oblongue, à grand axe Nord-Ouest Sud-Est, mesurant 15m x 8m et d'une hauteur approximative de 1,20m. On note à ses deux extrémités deux reliefs qui en sont comme les prolongements : au Nord-Ouest une éminence de forme circulaire de 8m x 8m, et au Sud-Est une autre de 12m x 6m. À l'arrière de ces structures, on distingue nettement les traces d'excavation du sol pour les réaliser ; il pourrait s'agir de « banquettes de tir »...

Historique

Structure identifiée par [F. Meyrat]{.creator} en [juillet 2019]{.date}.

### [Saint-Michel]{.spatial} - [Landarre]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 933m.

Situé sur un petit replat d'un terrain en forte pente vers l'Est. Il est à 130m au Sud Sud-Est - et plus bas - que les ruines d'une cabane en pierres.

Description

Tumulus circulaire, en forme de galette aplatie, mesurant 6,50m de diamètre. Ce tumulus mixte, constitué de terre et de nombreux fragments de schiste, répartis sans ordre apparent, présente en son milieu une dépression circulaire - très probablement due à une fouille ancienne - mesurant 1,70m de diamètre, 0,15m de profondeur, et légèrement décentrée vers l'Ouest.

Historique

Monument découvert par [F. Meyrat]{.creator} en [juillet 2019]{.date}.

### [Saint-Michel]{.spatial} - [Leizahandi]{.coverage} - [Tertre d'habitat]{.type}

Localisation

Altitude : 1102m.

Visible à droite de la route qui monte au cayolar Leizahandi.

Description

Tertre de terre mesurant 14m x 10m et 0,70m de haut.

Historique

Tertre découvert par [Meyrat F.]{.creator} en [octobre 2013]{.date}.

### [Saint-Michel]{.spatial} - [Leizahandi]{.coverage} - [Borne pastorale]{.type}

Localisation

Altitude : 1157m.

Entre la BF 209 (à l'Est) et la BF 208 (60m à l'Ouest).

Description

Petite borne de [calcaire]{.subject} blanc, allongée selon un axe Nord-Ouest Sud-Est, mesurant 1m de long, 0,40m de large à sa base rectiligne et 0,27m de large à son sommet arrondi.

Historique

Borne signalée par [Dupré E.]{.creator} en [octobre 2013]{.date}.

### [Saint-Michel]{.spatial} - [Minassaro]{.coverage} - [Tertres d'habitat]{.type}

Localisation

Ces 5 tertres sont répartis sur un vaste pâturage à gauche de la route qui se rend au col d'Arnostégui, au niveau où se détache la bretelle menant au cayolar Minassaro.

Description

Plus de 5 tertres mesurant en moyenne 5m à 6m x 5m et 0,60m de haut.

(Il existe la possibilité que certains reliefs, (en plus des 5 cités), soient d'anciens postes de tir).

Historique

Tertres découverts par [Blot J.]{.creator} en [octobre 2013]{.date}.

### [Saint-Michel]{.spatial} - [Orizune]{.coverage} - [Tumulus-Cromlech]{.type}

Localisation

Vers l'extrémité Ouest du plateau sommital, à 1060m d'altitude, et à 30m à l'Est d'un gros bloc rocheux.

Altitude : 1060m.

Description

Un discret péristalithe formé d'une dizaine de pierres, au ras du sol, entoure un tumulus terreux d'une dizaine de mètres de diamètre, atteignant 0,40m de haut environ.

Historique

Ce monument a été découvert par l'[association Hilharriak]{.creator} (Saint Sébastien) en [1999]{.date}.

### [Saint-Michel]{.spatial} - [Orizune]{.coverage} 2 - [Tumulus]{.type}

Rappelons que nous avons déjà décrit [@blotNouveauxVestigesMegalithiques1972b, p166], un premier tumulus, à 1030m d'altitude, sur un replat dominant le col de Landarre, à une trentaine de mètres de celui-ci.

Localisation

Carte 1346 Est Saint-Jean-Pied-de-Port.

Altitude :1062m.

Ce monument est situé à environ 30mètres au Sud Sud-Est de la borne sommitale du mont Orizune, sur un terrain en légère pente vers le Sud-Est.

Description

Tumulus d'environ 4,70m de diamètre et 0,30m de haut, constitué de terre et de nombreuses dalles de [grés]{.subject}. Il ne semble pas possible de préciser l'existence d'un péristalithe.

Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2009]{.date}.

### [Saint-Michel]{.spatial} - [Oilaskoa]{.coverage} 1 - [Cromlech]{.type}

Localisation

Altitude : 1068m.

Ce monument, ainsi que *Oilaskoa 2 - Cromlech*, est situé sur un replat herbeux à environ l4 mètres au Sud de la piste principale qui sillonne cette crête d'Ouest en Est ; il est aussi à 12 mètres à l'Est de la piste qui se rend au cayolar Oilaskoa.

Description

Ce cromlech légèrement en relief peut être qualifié de légèrement tumulaire ou surélevé. Il mesure environ 4,30m de diamètre, ce dernier étant difficile à évaluer étant donnée la disposition un peu irrégulière des pierres témoins. La périphérie est cependant balisée par environ 25 éléments pierreux bien enfoncés dans le sol. On note des blocs assez volumineux (au Nord par exemple : 0,63m x 0,50m et 0,40m de haut, et au Sud un bloc de 0,69m x 0,42m et 0,25m de haut) ; certains paraissent avoir été déterrés et être devenus mobiles. Au centre sont visibles 5 pierres ou petites dalles (éléments d'une ciste ?).

Historique

Monument découvert par [J. Blot]{.creator} en [mars 2018]{.date}.

### [Saint-Michel]{.spatial} - [Oilaskoa]{.coverage} 2 - [Cromlech]{.type}

Localisation

Ce monument est situé à 10m au Sud de *Oilaskoa 1 - Cromlech*.

Description

On ne voit, à l'heure actuelle que la moitié Est de ce monument, qui mesure 2m de diamètre et qui est balisée par 11 pierres de dimensions très variables. Elles sont plus volumineuses et visibles à l'Est, où elles sont souvent tangentes, alors qu'elles sont plus espacées aux extrémités du demi-cercle. Deux d'entre elles, au Sud, au ras du sol, atteignent 0,83m et 0,84m de long. Deux petites pierres apparaissent au centre.

Historique

Monument découvert par [J. Blot]{.creator} en [mars 2018]{.date}.

### [Saint-Michel]{.spatial} - [Oilaskoa]{.coverage} Nord - [Tertre d'habitat]{.type}

Localisation

Situé au pied des premiers rochers d'Athéketa.

Altitude : 1048m.

Description

Tertre terreux asymétrique, allongé dans le sens Nord-Ouest Sud-Est, mesurant 9m de long, 5 de large environ et 1m de haut environ.

Historique

[J. Blot]{.creator} en [mars 2018]{.date} (Dans notre publication des TH d'Oilaskoa dans le [@blotVestigesProtohistoriquesVoie1978], nous citions 3 tertres d'habitat à l'Est Nord-Est des 15 décrits. Il semble bien que seul celui décrit ci-dessus soit valable, les 2 autres paraissant des formations naturelles).

### [Saint-Michel]{.spatial} - [Oilaskoa]{.coverage} 3 - [Cromlech]{.type}

Localisation

Il est tangent au Sud-Ouest au *cromlech Oilaskoa n°1* décrit dans le Tome 10 [@blotInventaireMonumentsProtohistoriques2018], de notre *Inventaire des Monuments protohistoriques en Pays Basque*. Ce C1 est situé sur un replat herbeux, à 14m au Sud de la piste principale qui sillonne la crête d'Oilaskoa d'Ouest en Est, et il est à 12m à l'Est de la piste qui se rend au cayolar d'Oilaskoa.

Altitude : 1068m.

Description

Huit blocs pierreux, de même nature que ceux formant le cromlech n°1 (situé à 0,30m au Nord-Est), délimitent un cercle de 1,40m de diamètre ; les dimensions sont variables, le bloc le plus important, à l'Ouest, mesurant 0,20m x 0,30m et 0,25m de haut.

Historique

Monument découvert par [C. Blot]{.creator} en [juillet 2018]{.date}.

On notera que depuis sa découverte, le *cromlech n°2* a été en partie recouvert par un dépôt intempestif de gravats par un engin de chantier...

### [Saint-Michel]{.spatial} - [Oillaskoa]{.coverage} (ruisseau) - [Tertre d'habitat]{.type}

Localisation

Altitude : 1088m.

Ce tertre est situé sur un terrain en pente orientée vers l'Est, à environ 100m à l'Ouest du ruisseau Oilaskoa qu'il domine et à 300m au Nord du tertre dit *Cabane d'Arnostégui*.

Description

Important tertre terreux, de forme ovale à grand axe Nord-Sud, mesurant 10m x 8m et 0,50m de haut.

Historique

Tertre découvert par [A. Martinez]{.creator} en [juillet 2019]{.date}.

### [Saint-Michel]{.spatial} - [Orizune]{.coverage} Est - [Tertre d'habitat]{.type}

Localisation

Altitude : 917m.

Tertre situé au flanc Est du mont Orizune (Orisson), sur un très léger replat.

Description

Tertre mixte de terre, dans laquelle apparaissent quelques blocs de [schiste]{.subject}, dans les secteurs Est, Nord et au centre. De forme asymétrique (sur terrain en légère pente vers l'Est), et ovale, il mesure 6m x 5m et 0,15m de haut.

Historique

Tertre découvert par [F. Meyrat]{.creator} en [juillet 2019]{.date}.

### [Saint-Michel]{.spatial} - [Pagoberri]{.coverage} - [cromlechs]{.type} T ( ?)

Localisation

Altitude : 1055m.

À une cinquantaine de mètres au Sud et en contrebas de la piste qui se rend au cayolar de Pagoberri : sur terrain en légère pente vers le Sud.

Description

Un ensemble de blocs en [calcaire]{.subject} blanc, de taille variable, forme une figure géométrique grossièrement trapézoïdale dans laquelle il serait peut-être possible de distinguer 3 « cercles de pierres » de diamètre allant de 3m à 3,50m .... Très douteux.

Historique

Ensemble découvert par [Meyrat F.]{.creator} en [octobre 2013]{.date}.

### [Saint-Michel]{.spatial} - [Pagoberri]{.coverage} - [Monolithe]{.type}

Localisation

Altitude : 1043m.

Plus à l'Est que les éléments précédents, ce monolithe est à 5m à l'Est des ruines d'un cayolar, et à 14m au Nord de la piste pastorale, mais toujours en contre-bas de la piste qui mène à Pagoberri. Il est très proche du tracé de la ligne frontière...

Description

Bloc de [calcaire]{.subject} de forme rectangulaire, couché au sol, ne présentant pas de trace d'épannelage (peut-être avait-il la forme désirée d'emblée). Il mesure 2,70m de long, 0,58m de large à sa base Nord, arrondie, 0,73m à son extrémité Sud, plus pointue et 0,44m d'épaisseur en moyenne. Il présente de fortes traces de dissolution du calcaire à sa surface supérieure.

Historique

Monolithe signalé par [E. Dupré]{.creator} en [octobre 2013]{.date}.

### [Saint-Michel]{.spatial} - [Pagoberri]{.coverage} 1 - [Tumulus]{.type}

Localisation

Altitude : 1107m.

Dans un petit cirque rocheux à 100m au Nord du cayolar Pagoberri.

Description

Petit tertre pierreux de 2,70m de diamètre et 0,50m de haut environ formé d'un peu plus d'une vingtaine de blocs de [calcaire]{.subject} blanc, non jointifs, mais disposés de façon circulaire. Pas de péristalithe visible.

Historique

Monument découvert par [Meyrat F.]{.creator} en [octobre 2013]{.date}.

### [Saint-Michel]{.spatial} - [Pagoberri]{.coverage} 2 - [Tumulus]{.type}

Localisation

Altitude : 1043m.

Il est à 20m à l'Est de *Pagoberri - Monolithe*.

Description

Tumulus pierreux circulaire, parfaitement visible, de 6m de diamètre et 0,40m de haut environ. Il est constitué de blocs en [calcaire]{.subject} blanc, de volumes variables, disposés sans ordre apparent ; il ne semble pas y avoir de péristalithe vrai.

Historique

Monument découvert par [Meyrat F.]{.creator} en [octobre 2013]{.date}.

### [Saint-Michel]{.spatial} - [Pagoberri]{.coverage} - [Tertres d'habitat]{.type}

Nous publions ici cet ensemble de 10 tertres d'habitat dont certains ont été trouvés par [Blot J.]{.creator} et par [Meyrat F.]{.creator} en [octobre 2013]{.date}.

Localisation

- *TH 1* : Altitude : 1000m. À l'amorce d'un vaste pâturage qui s'étend vers l'Est, entre deux barres rocheuse. Tertre de 10m x 10m et 0,40m de haut.

- *TH 2* : Situé à 10m au Nord-Est du précédent (altitude : 1002m). Adossé à la barrière rocheuse, il mesure 15m x 10m et 1,50m de haut.

- *TH 3* : Altitude : 1002m. Situé à 15m à l'Est du précédent, et adossé à la barrière rocheuse. Tertre de 18m x 12m et 2m de haut.

- *TH 4* : Altitude : 997m. Situé à 40m au Sud-Est du précédent. Mesure 20m x 10m et 1m de haut. Présente une excavation à son sommet.

- *TH 5* : À 5m au Sud-Est du précédent. Mesure 7m x 5m et 0,70m de haut

- *TH 6* : Altitude : 991m. À 60m à l'Est Sud-Est du précédent. Mesure 12m x 6m et 0,40m de haut.

- *TH 7* : Altitude : 990m. À 40m à l'Est du précédent. Mesure 8m x 5m et 0,40m de haut.

- *TH 8* : Altitude : 984m. À 40m à l'Est du précédent. Mesure 14m x 6m et 0,80m de haut.

- *TH 9* : Altitude : 992m. À 140m environ au Sud-Ouest du tertre n°7. Mesure 20m x 10m et 0,90m de haut.

- *TH 10* : Tangent à l'Ouest du précédent. Mesure 8m x 6m et 0,40m de haut

### [Saint-Michel]{.spatial} - [Sohandi]{.coverage} - [Cromlechs]{.type}

Nous avons publié les 6 cromlechs suivants (*C1*, *C7*, *C8*, *C9*, *C10*, *C11*), trouvés par [nous]{.creator} en [1977]{.date}, dans le [@blotVestigesProtohistoriquesVoie1978]. Ces monuments étaient déjà peu visibles à l'époque, les témoins n'émergeant que peu (ou pas du tout), du sol. Depuis 40 ans, les sédiments et la végétation se sont accumulés, au point que la plupart d'entre eux n'étaient plus discernables en 2018. Il a donc été nécessaire de procéder à un important travail de nettoyage - grâce à la participation très efficace de notre ami Francis Meyrat que nous remercions ici vivement - qui a permis de préciser la structure de ces différents monuments.

- *Sohandi 1 - Cromlech*
    - Localisation
        Il est situé à environ 175m au Sud-Est de la vieille cabane d'Harrieta et à 1m à l'Est de la piste venant de celle-ci.
        
        Altitude : 872m.
    - Description
        Dix-neuf petits blocs de [calcaire]{.subject} de très modeste volume, délimitent un cercle (assez peu régulier) de 6,50m de diamètre. La pierre la plus volumineuse se trouve au Nord et mesure 0,75m x 0,56m et 0,20m de haut ; une autre, à l'Ouest, mesure 0,70m x 0,40m et 0,10m de haut. Les autres ont entre 0,30m et 0,10m d'implantation visible au sol...
- *Sohandi 7 - Cromlech*
    - Localisation
        Situé à 29m au Sud Sud-Ouest de *C1*.
    - Description
        6 pierres délimitent un cercle de 6m de diamètre. Le témoin le plus volumineux se trouve au Nord, et mesure 1,13m x 0,25m et 0,20m de haut ; une seconde, à l'Est, au ras du sol, mesure 0,90m x 0,30m. Les autres sont de volume beaucoup plus discret et peu visibles.
- *Sohandi 8 - Cromlech*
    - Localisation
        Il est situé à 13m au Sud-Est de *C7*.
    - Description
        Neufs petits blocs calcaires délimitent un cercle de 5,80m de diamètre.
        
        Une aubépine a dû être coupée, dont la souche se voit dans le quart Ouest du monument.
        
        Trois pierres atteignent 0,40m de long et 0,10m de haut, les autres sont bien plus petites.
- *Sohandi 9 - Cromlech*
    - Localisation
        Il est à 6m au Sud Sud-Ouest de *C8*.
    - Description
        Ce cromlech de 6m de diamètre est délimité par 10 petits blocs [calcaire]{.subject}, dont 2, au Sud Sud-Ouest sont en commun avec *C10* avec lequel il est donc tangent. La majorité de ces témoins est groupée dans la moitié Sud du monument, et n'atteignent au mieux que 0,40m x 0,25m et 0,12m à 0,20m de haut ; par contre, au Nord Nord-Est, on note une pierre plus importante atteignant 0,90m x 0,12m et 0,24m de haut. Une aubépine a poussé dans sa moitié Sud.
- *Sohandi 10 - Cromlech*
    - Localisation
        Ce cercle est tangent au Sud Sud-Ouest du précédent et possède 2 témoins en commun avec lui.
    - Description
        Il mesure 7m de diamètre et possède 8 pierres à peu près également réparties à sa périphérie.

        On notera aussi qu'au Sud, ce monument possède 2 autres pierres en commun avec *C 11*, avec lequel il est donc aussi tangent. Ces 2 témoins mesurent respectivement 0,53m x 0,05m et 0,14m de haut, et 0,68m x 0,09m et 0,10m de haut.
- *Sohandi 11 - Cromlech*
    - Localisation
        Il est tangent au Sud du précédent, avec lequel il possède 2 pierres en commun.
    - Description
        Ce monument mesure 4,60m de diamètre, délimité par 7 pierres de volume variable (en moyenne 0,30m x 0,30m, plus ou moins au ras du sol) ; une exception toutefois, au Sud Sud-Est, une pierre plus allongée, au ras du sol, mais mesurant 0,90m x 40m. On peut noter au Sud Sud-Ouest une dalle plantée.

### [Saint-Michel]{.spatial} - [Sohandi]{.coverage} - [Dalle plantée]{.type}

Localisation

Dalle plantée à 7m au Sud Sud-Ouest de *Sohandi 11 - Cromlech*.

Description

Épaisse dalle de [grés]{.subject} plantée dans le sol, mesurant 0,75m à sa base et 0,80m à son sommet, ainsi que 0,16m d'épaisseur en moyenne. Elle est orientée selon un axe Sud-Ouest Nord-Est et semble présenter de discrets signes d'épannelage sur toute da périphérie (?). La signification de cette dalle, dont la présence ne paraît pas fortuite, nous échappe totalement : repère pastoral ou municipal moderne ? repère en rapport avec la nécropole à incinération ?

Historique

Dalle découverte par [F. Meyrat]{.creator} en [mars 2018]{.date}.

### [Saint-Michel]{.spatial} - [Urdanazburu]{.coverage} - [Tertres d'habitats]{.type}

Localisation

Altitude : 1100m.

Ces tertres sont situés à une trentaine de mètres l'Est de la route, sur un terrain en très légère pente vers le Sud, de suite après la très importante formation rocheuse dénommée *Urdanazburu*.

Description

- *Tertre 1* : ce tertre de volume très modeste, de forme ovale, allongé selon un axe Nord-Sud, mesure 4m x 2,60m et 0,50m de haut.

Il est à noter qu'il est accompagné de deux autres formations très proches :

- *Tertre 2 (?)* : situé à 2,20m au Nord Nord-Ouest du précédent, lui aussi de forme ovale, il mesure 1,60m x 1m et 0,20m de haut. Est-ce un tertre ?

- *Tumulus 3 (?)* : situé à 12m à l'Ouest du précédent, et à 18m à l'Est de la route, se présente sous la forme d'un «tumulus» (?) arrondi, terreux, mesurant 3m de diamètre et 0,20m de haut, au centre duquel se détache un bloc de grès mesurant environ 0,40m x 0,35m.

Historique

Le *Tertre 1* a été découvert par [A. Martinez]{.creator} en juillet 2019 ; le *Tertre 2 (?)* et le *Tumulus 3 (?)* l'ont été par [J. Blot]{.creator} en [juillet 2019]{.date}.

### [Saint-Michel]{.spatial} - [Urdanarre]{.coverage} - [Tertre d'habitat]{.type}

Localisation

Altitude : 1236m.

Il est sur la pente Sud du mont Urdanarre. Un poste de tir à la palombe en ciment a été construit à l'intérieur.

Description

Tertre dissymétrique, en terre, mesurant 15m de large et 12m dans le sens de la pente et 0,45m de haut ; une large excavation de 3,50m de diamètre a été aménagée au centre pour le poste de tir.

Historique

Monument découvert par le [groupe Hillariak]{.creator} en [juillet 2013]{.date}.

### [Saint-Michel]{.spatial} - [Urdanarre]{.coverage} Nord 3 - [Cromlech]{.type}

Localisation

Altitude : 1225m.

Ce cercle, situé au sommet d'une petite éminence, est à 14m au Sud Sud-Est du *Tumulus-cromlech 2*.

Description

Il semble bien qu'on puisse distinguer un cercle périphérique, d'environ 6m de diamètre, constitué d'une trentaine de dalles ou de blocs de [schiste]{.subject} de volume modeste, visibles au ras du sol, et au centre un deuxième cercle de 1,90m de diamètre, constitué d'une douzaine du même type de blocs de [schiste]{.subject}.

Historique

Monument identifié par [Blot J.]{.creator} en [août 2018]{.date}.

### [Saint-Michel]{.spatial} - [Urkulu]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 1303m.

Ce tumulus est situé sur terrain plat, au milieu d'un petit ensellement formant limite entre 2 grandes dolines situées au Sud Sud-Est des cabanes d'Urkulu.

Description

Tumulus terreux de 12m de diamètre environ, et 0,50m de haut. Pas d'éléments pierreux visibles.

Historique

Nous avions publié dans le [@blotVestigesProtohistoriquesVoie1978 p.75], 5 tertres d'habitat, dont 4 au bord même de la grande doline la plus proche des cabanes, (mais à une centaine de mètres au Sud Sud-Est de ces dernières, et un cinquième tertre situé au Sud Sud-Est, à environ 90m des précédents tertres. En fait et comme le notait [F. Meyrat]{.creator} en [mars 2018]{.date}, ce cinquième tertre est en vérité plutôt un tumulus.

### [Saint-Michel]{.spatial} - [Urkulu]{.coverage} BF 206 - [Tumulus]{.type}

Localisation

Altitude : 1412m.

Monument situé à 24m au Sud Sud-Est de la BF 206 et à 3m au Nord-Ouest d'une doline.

Description

Petit tumulus en forme de galette aplatie, mesurant 0,10m de haut et 2,20m de diamètre ; à sa surface apparaissent une dizaine de petits blocs de [calcaire]{.subject} blanc, prédominant dans le secteur Nord-Ouest du monument, sans que l'on puisse parler semble-t-il d'un tumulus cromlech, mais plutôt d'un tumulus mixte, constitué de terre et de pierres.

Historique

Monument découvert par [F. Meyrat]{.creator} en [juin 2018]{.date}.

### [Saint-Michel]{.spatial} - [Zerkupé]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 1124m.

Ce tumulus est situé sur un petit ensellement qui domine, au Nord, le camp protohistorique de *Zerhupé*, distant de 300m environ ; la Voie Romaine (ou chemin de Compostelle), est à 250m au Nord ; Château Pignon domine à 500m au Nord-Est. On note une importante doline à quelques dizaines de mètres au Nord Nord-Est.

Description

Cet important tumulus est érigé sur un sol plat ; il est de forme ovale, orienté suivant un axe Est-Ouest et est constitué de terre et de pierres concassées ; il mesure 10,60m de long et 5,20m de large, sa hauteur est de 0,50m au Sud et de 0,70m au Nord. Ses dimensions et sa forme pourraient aussi évoquer un tertre d'habitat ; néanmoins il est isolé, sur un sol plat, et il n'est pas impossible qu'il puisse être un éventuel tumulus funéraire, dont l'époque resterait à préciser par une fouille.

Historique

Monument découvert par [J. Blot]{.creator} en [juillet 2017]{.date}.
