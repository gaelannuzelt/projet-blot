# Etxebar

### [Etxebar]{.spatial} - [Murutxe]{.coverage} Est - [Tertres d'habitat]{.type}

Localisation

Altitude : 980m.

Les 10 tertres d'habitat sont au Nord de la route qui redescend vers Licq, avant la piste qui monte au pic Salhagagne, sur le flanc Est de la colline, en bordure d'un petit ravin boisé.

Description

On peut compter une dizaine de tertres, si ce n'est plus (abondante végétation), espacés les uns des autres de 1,50m à 3m ou 4m environ - Ils sont circulaires, mesurent 5m à 6m de diamètre en moyenne et 0,60m de haut.

Historique

Tertres découverts par [Blot J.]{.creator} en [octobre 2012]{.date}.

### [Etxebar]{.spatial} - [Murutxe Ouest]{.coverage} - [Tertres d'habitat]{.type}

Trois TH se trouvent au Nord Nord-Ouest de la route qui redescend vers Licq, avant la piste qui monte au pic Salhagagne, sur le flanc Est de la colline, en bordure d'un petit ravin boisé.

- *Tertre d'habitat 1* : Il est à 80m au Nord Nord-Ouest de la route. Altitude : 984m - Tertre ovale de 10m x 7m et 0,20m de haut. Terrain en légère pente vers le Sud.

- *Tertre d'habitat 2* : Situé à 10m au Nord de *TH 1* - Mesure 9m x 7m et 0,15m de haut. ; terrain en légère pente vers leSsud.

- *Tertre d'habitat 3* : Situé environ à 120m au Nord de *TH 2*, au sommet de la cote 992, sur le replat de la crête. Tertre de 8m x 6m et 0,25m de haut. Très légère dépression centrale.

Historique

Ces trois tertres ont été trouvés par [F. Meyrat]{.creator} en [novembre 2014]{.date}.
