# Bussunarits

### [Bussunarits]{.spatial} - [Galhareko Pareta]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 593m.

Sur le sommet de la colline de ce nom, à 35m à l'Est d'un important réservoir en béton.

Description

Tumulus terreux, circulaire, très aplati, mesurant 5m à 6m de diamètre environ et 0,20m de haut. Une dizaine de pierres environ apparaissent en surface et en périphérie, sans qu'on puisse vraiment parler d'un péristalithe.

Historique

Tumulus trouvé par [I. Txintxuretta]{.creator} en [avril 2016]{.date}.

### [Bussunarits]{.spatial} - [Lauhiburu]{.coverage} - [Monolithe]{.type} ([?]{.douteux})

Localisation

Altitude : 654m.

Il se dresse verticalement, le long d'un sentier qui longe, en contre-bas, le flanc Nord-Est de la colline Lauhiburu, en lisière de feuillus. Il est à proximité immédiate et à l'Ouest de formations karstiques ; *la question se pose de savoir s'il est en position naturelle, ou disposé, en ce lieu, par la main d'homme*.

Description

Monolithe de 1,75m de haut, de forme généralement triangulaire à base inférieure, et prismatique à la coupe. Il présente donc 3 faces à étudier.

La face Sud, nettement triangulaire, mesurant 1,70m à la base, et assez lisse dans l'ensemble ; la face Ouest, d'aspect rectangulaire, mesurant 1m à la base et 0,75m au sommet, elle aussi sans reliefs notables ; enfin la face Est, assez semblable à la précédente, mesure 1,20m à la base et 0,84m au sommet. Ce monolithe ne présente aucune trace d'épannelage.

Historique

Monolithe trouvé par [I. Txintxuretta]{.creator} en [avril 2016]{.date}.
