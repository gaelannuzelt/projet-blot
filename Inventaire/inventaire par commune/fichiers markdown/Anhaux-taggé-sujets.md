# Anhaux

### [Anhaux]{.spatial} - [Beharria]{.coverage} - [Cromlech]{.type}

#### Localisation

Altitude : 908m.

Ce monument est situé sur un petit replat au flanc Est du Monhoa, dominant un magnifique panorama, très étendu vers le Nord et l'Est, de la côte au piémont.

#### Description

On note une douzaine de blocs de [grès]{.subject}, souvent profondément enfoncés dans le sol et disposés suivant un cercle d'environ 3m de diamètre ; un des blocs périphériques, au Nord, a basculé vers l'extérieur, rompant la régularité du cercle. ; huit autres blocs, de taille plus réduite, apparaissent aussi dans la région centrale. Les dimensions de tous ces éléments sont variables.

- Pierre 1 : 1m x 1,10m.

- Pierre 2 : 1,50 x 0,75m.

- Pierre 3 : 0,75m x 0,76m.

- Pierre 4 : 0,40m x 0,45m.

On ne note pas de traces de fouilles clandestines.

À 2m au Sud-Sud-Est a été déposée une plaque commémorative en basque, dont la traduction est la suivante (M. Duvert) : *Le pays est le corps, la langue (eukara) le coeur*. Il n'est cependant pas évident que cette plaque soit en rapport direct avec ce monument assez discret, mais plus probablement avec le lieu et sa situation exceptionnelle.

#### Historique

Monument découvert par [F. Meyrat]{.creator} en [septembre 2019]{.date}.

### [Anhaux]{.spatial} - [Monhoa]{.coverage} - [Tumulus]{.type} [(?)]{.douteux}

#### Localisation

Plus à l'Ouest de *Beharria - Cromlech* se dresse le sommet du mont Monhoa.

Altitude : 1019m. par une borne érigée sur ce qui donne l'impression d'être un tumulus... remanié pouvant atteindre environ 8m de diamètre et 0,50m de haut. Il est très difficile de se prononcer.

#### Historique

Cas particulier soulevé par [F. Meyrat]{.creator} en [septembre 2019]{.date}.

### [Anhaux]{.spatial} - [Urdiako lepoa]{.coverage} 1 - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 920m.

Il est quasiment tangent à la berge Est de la route, et perpendiculaire à elle.

#### Description

Ce tertre, ovale, à grand axe Est-Ouest, mesure 9,50m de long, 7m de large et 0,40m à 0,80m de haut suivant les endroits. On note l'empreinte, très marquée, du passage de l'ancienne piste (environ 1m de large) à la moitié du tertre, perpendiculairement à son grand axe ; un bloc de [schiste]{.subject} gris apparaît dans son quart Nord-Est.

#### Historique

Tertre découvert par [J. Blot]{.creator} en [juillet 2011]{.date}.

### [Anhaux]{.spatial} - [Urdiako lepoa]{.coverage} 2 - [Tertre d'habitat]{.type}

#### Localisation

Il est à 10m au Sud Sud-Ouest de *Urdiakolepoa 1 - Tertre d'habitat*, et comme lui, tangent à la route actuelle.

#### Description

Il mesure 5 m de long, 3m de large et 0,40m de haut environ. Son grand axe est orienté Nord-Ouest Sud-Est, et on retrouve l'empreinte de la piste ancienne à son 1/3 supérieur, mais en moins marquée que précédemment. À noter une curieuse [excavation circulaire] de 2m de diamètre à son extrémité Sud-Ouest...

#### Historique

Tertre trouvé par [J. Blot]{.creator} en [juillet 2011]{.date}.
