# Saint-Pée-sur-Nivelle

### [Saint-Pée-sur-Nivelle]{.spatial} - [Apeztégi]{.coverage} 2 - [Tumulus]{.type}

Localisation

Il se trouve à 20 mètres à l'Ouest Nord-Ouest du tumulus Apeztegi que nous avons décrit en 1976 [@blotTumulusBixustiaZuhamendi1976, p.111].

Altitude : 150m.

Description

Tumulus circulaire de 7m de diamètre et d'une vingtaine de centimètres de haut, constitué de terre et de pierraille.

À signaler, à 200m au Nord-Ouest, ce qui pourrait être un troisième *Tumulus - Apeztegi 3* de 8m de diamètre et d'une trentaine de centimètres de haut, fait de pierraille ; il est traversé par une clôture en barbelés.

Historique

Monuments trouvés en [décembre 2010]{.date} par [J. Blot]{.creator}.

### [Saint-Pée-sur-Nivelle]{.spatial} - [Apaztégui]{.coverage} Sud - [Pierre plantée]{.type} ([?]{.douteux})

Localisation

Altitude : 105m. Elle se trouve sur l'axe de parcours d'une croupe au Sud et en contre-bas des *Tumulus d'Apeztegui* [@blotInventaireMonumentsProtohistoriques2011 p.21].

Description

Pierre plantée, mais actuellement fortement inclinée vers l'Est. Elle mesure 0,85m de haut, 0,51m à sa base et 0,37m d'épaisseur en moyenne. Elle est la seule borne plantée de ce vaste pâturage.

Historique

Pierre découverte par [Blot J.]{.creator} en [mars 2012]{.date}.

### [Saint-Pée-sur-Nivelle]{.spatial} - [Croix-de-Sainte-Barbe]{.coverage} - [Cromlech]{.type}

Localisation

Altitude : 185m.

Ce cercle de pierres se trouve à 0,50m au Sud du calvaire érigé sur cette colline.

Description

De nombreuses pierres (une trentaine, plus nombreuses en secteur Nord et Sud Sud-Est), souvent au ras du sol mais cependant bien visibles, délimitent un cercle de 2,50m de diamètre environ ; il semble que l'on puisse distinguer, en secteur Nord-Ouest, les ébauches de deux autres cercles.

Historique

Monument découvert par [J. Blot]{.creator} en [novembre 2010]{.date}.

### [Saint-Pée-sur-Nivelle]{.spatial} - [Croix-de-Sainte-Barbe]{.coverage} 2 - [Tumulus]{.type}

Localisation

Altitude : 150m.

Il est situé sur un replat de la ligne de crête qui prolonge à l'Ouest la colline de la Croix-de-Sainte-Barbe, à environ une soixantaine de mètres à l'Ouest Nord-Ouest d'une antenne de relais téléphonique.

Description

Tumulus pierreux circulaire, de 8m de diamètre et environ 0,40m de haut.

Historique

Découvert par [Blot J.]{.creator} en [décembre 2010]{.date}. À noter que le *Tumulus n°1 de la Croix-de_Sainte-Barbe*, déjà décrit par nous, [@blotTumulusBixustiaZuhamendi1976 p. 111], a été évité de justesse par le passage d'engins sur la piste pastorale primitive ; on peut le deviner, enfoui sous les ronces, dans un virage, au Sud de celle-ci, dans le col.

### [Saint-Pée-sur-Nivelle]{.spatial} - [Helbarron]{.coverage} - [Tumulus]{.type} ([?]{.douteux})

Localisation

Cet important relief se voit à quelques dizaines de mètres à droite de la route qui mène de d'Helbarron à Saint-Pée, une fois traversé Helbarron.

Description

On note un relief de terrain d'environ 3m de haut environ et de forme plutôt ovale que circulaire, (70m x 40m). S'agit-il d'un relief d'origine anthropique, ou d'un simple mouvement de terrain, comme nous le penserions plus volontiers ?

Historique

Ce relief nous a été signalé par [F. Meyrat]{.creator} en [février 2011]{.date}.

### [Saint-Pée-sur-Nivelle]{.spatial} - [Larrekokurrutzea]{.coverage} - [Tumulus]{.type} ([?]{.douteux})

Localisation

Carte 1245 Est Espelette.

Altitude : 160m.

La croix d'un calvaire est érigée au-dessus...Volontairement ? le tumulus ne paraît pas contemporain de ce calvaire, mais bien antérieur. L'ensemble se trouve à 15m à l'Ouest d'un réservoir d'eau en ciment.

Description

Tumulus terreux très aplati (peut-être par des labours anciens), de 10m de diamètre et 0,80m de haut.

Historique

Monument découvert en [mars 1975]{.date}. Peut-être y a-t-il 2 autres monuments, douteux, l'un de 10m de diamètre à 60m à l'Ouest Nord-Ouest, et un second à 20m au Sud-Est.

### [Saint-Pée-sur-Nivelle]{.spatial} - [Martinhaurrenborda]{.coverage} 1 - [Pierre plantée]{.type} ([?]{.douteux})

Localisation

Pierre située dans un col, sur un terrain en friche, sur la ligne de crête qui mène à la redoute de Bizkartzu, elle est contiguë à la parcelle Mixalen Xola.

Altitude : 195m.

Description

Cette borne, de section rectangulaire, à grand axe orienté Nord-Est Sud-Ouest, mesure 1,08m de haut, 1,62m de long et 0,84m de large, à sa base. Sa face Nord ainsi que son sommet présentent de nombreuse traces d'épanellage.

Historique

Pierre découverte par [F. Meyrat]{.creator} en [août 2011]{.date}.

### [Saint-Pée-sur-Nivelle]{.spatial} - [Martinhaurrenborda]{.coverage} 2 - [Pierre plantée]{.type} ([?]{.douteux})

Localisation

Borne plantée en limite de parcelle sur Mixelen Xola, à environ 30m à l'Ouest de *Martinhaurrenborda 1 - Pierre plantée (?)*.

Description

De section rectangulaire, elle mesure, 0,52m de long et 0,42m de large, à sa base, et 1,18m de haut. Sur elle s'appuie le grillage de clôture.

Historique

Borne découverte par [F. Meyrat]{.creator} en [août 2011]{.date}.

### [Saint-Pée-sur-Nivelle]{.spatial} - [Pettikenborda]{.coverage} - [Tertres d'habitat]{.type}

Localisation

Altitude : 85m.

Ce groupe de 7 tertres d'habitat est situé à l'Ouest de la route d'Ahetze un peu avant le hameau Orgabidea et au Sud de Pettikenborda. Ces 7 éléments sont érigés sur un terrain en assez forte pente vers le Nord-Ouest, le bas de la prairie étant drainée par 3 petits rius qui convergent en un seul ensuite. Certains reliefs voisins peuvent être interprétés comme des vestiges de tertres d'habitat peu visibles ou de simples mouvements naturels de terrain (colluvion).

Description

- *Tertre n°1* : le plus visible, mesure environ 12m de diamètre et 1,80m de haut.

- *Tertre n°2* : situé à 8m au Nord-Ouest du précédent, mesure 8m de diamètre et 0, 60m de haut.

- *Tertre n°3* : il est tangent au n°1 (au Sud-Ouest). Mesure 8m de diamètre et 0,60m de haut.

- *Tertre n°4* : situé à 3m au Sud-Est du n°1 et à 4m environ à l'Ouest de la route. Mesure 8m de diamètre et 0,80m de haut.

- *Tertre n°5* : il est situé à 15m au Sud Sud-Ouest du précédent et à 8m à l'Ouest de la route. Mesure 6m de diamètre et 0,90m de haut.

- *Tertre n°6* : Situé à 6m à l'Ouest Sud-Ouest du précédent. Mesure 6m de diamètre et 0,8m de haut.

- *Tertre n°7* : Situé à 15m au Sud Sud-Ouest du n°5 et à 5m à l'Ouest de la route. De forme oblongue, il mesure 6 x 3 m de diamètre et 0,8m de haut.

Historique

Le *Tertre n°1* a été trouvé par [J. Blot]{.creator} en [octobre 1971]{.date}, ainsi que les *tertres 2* et *3* en juillet 2013 ; les *tertres 4*, *5*, *6*, *7* ont été trouvés par [F. Meyrat]{.creator} en [juillet 2013]{.date}.

### [Saint-Pée-sur-Nivelle]{.spatial} - [Péritxen]{.coverage} 1 - [Tumulus]{.type}

Localisation

Altitude : 150m.

Situé sur terrain dégagé très légèrement incliné vers le Nord.

Description

Tumulus de terre de 10m de diamètre, et 0,20m à 0,30m de haut environ.

Il semblerait qu'à quelques dizaines de mètres plus au Nord, quelques mouvements de terrain pourraient évoquer des vestiges de tertres d'habitat ?

Historique

Tumulus découvert par [Blot J.]{.creator} en [janvier 2011]{.date}.

### [Saint-Pée-sur-Nivelle]{.spatial} - [Péritxen]{.coverage} 2 - [Tumulus]{.type}

Localisation

Altitude : 155m.

On peut le distinguer, encadré par les deux chemins qui montent vers le Nord-Ouest, à 4m au Sud-Ouest de celui le plus à droite.

Description

Tumulus discret de 6m de diamètre environ et de 0,20m à 0,30m de haut.

Historique

Tumulus découvert par [J. Blot]{.creator} en [janvier 2011]{.date}.

### [Saint-Pée-sur-Nivelle]{.spatial} - [Olhaetcheberrikoborda]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 90m.

On le voit à une dizaine de mètres au Nord de la route, au sommet d'un mouvement naturel du terrain.

Description

Tumulus de terre, ayant été notablement aplati par la transformation du site en prairie artificielle ; nous avions vu ce monument nettement plus en relief, en 1971, quand le terrain n'était qu'une lande. Il mesure actuellement une douzaine de mètres de diamètre et une trentaine de centimètres de haut.

Historique

Monument découvert par [J. Blot]{.creator} en [1971]{.date} et revu en [mars 2011]{.date}.

### [Saint-Pée-sur-Nivelle]{.spatial} - [Opalazio]{.coverage} - [Tumulus]{.type}

Localisation

Carte 1245 Est Espelette.

Altitude : 80m.

Il est situé dans une prairie au Nord-Est de la D 305.

Description

Tumulus terreux de 16m de diamètre et 0,80m de haut.

Historique

Découvert en [mai 1972]{.date}. Il a depuis été rasé par la construction de la maison Zamaldégia.

### [Saint-Pée-sur-Nivelle]{.spatial} - [Serres]{.coverage} - [Tumulus]{.type}

Localisation

Carte 1245 Est Espelette.

Altitude : 65m.

Il est situé à 6 mètres à l'Ouest du carrefour d'où descend une route vers les nouveaux lotissements d'Ascain, et la D 918.

Description

Un calvaire est érigé sur ce qui pourrait avoir été un tumulus antique ayant alors servi de soubassement ; ce tertre a été ensuite coupé dans sa partie Nord-Ouest par le passage de la route asphaltée et un mur de soutènement a été construit au niveau de la coupe pour éviter l'effondrement du calvaire.

On peut estimer les dimensions originelles du tumulus à 30m de diamètre et 1,90m de haut.

Historique

Monument découvert en [juin 1973]{.date}.
