# Béhorlégui

### [Béhorlégui]{.spatial} - [Arlotia]{.coverage} - [Polissoir]{.type}

Localisation

Altitude: 744m.

Ce polissoir est à une centaine de mètres à l'Est du chalet Arlotia et à 6m au Nord Nord-Ouest de la piste qui s'élève de ce chalet vers le Nord.

Description

Bloc de [grès]{.subject} de forme rectangulaire, mais d'épaisseur décroissante, triangulaire à la coupe. Il mesure 0,60m dans son grand axe, 0,50 de large, 0,85m dans sa plus grande épaisseur, au Nord, de sorte qu'ainsi la face supérieure est inclinée vers le Sud. Cette face présente 4 plages de polissage, deux supérieures et deux juste en-dessous, ayant à peu prés toutes les mêmes dimensions: environ 0,30m de long et 0,20 m de large.

Ce polissoir présente des traces de chocs, trés probablement par des engins récents, et pourrait ne pas être à sa place originelle.

Historique

Monument découvert par [Blot J.]{.creator} en [avril 2013]{.date}.

### [Béhorlégui]{.spatial} - [Arlotia]{.coverage} (groupe Sud) - [Tertres d'habitat]{.type} 

On en dénombre 4.

- *Arlotia Sud 1 - Tertre d'habitat* : Tertre terreux de 11m de diamètre et 0,80m de haut, érigé sur terrain en légère pente vers le Nord. C'est lui que nous avions décrit en 1971 comme le tumulus *Arlotia 3*.

- *Arlotia Sud 2 - Tertre d'habitat* : situé à 2m au Nord du précédent -- Mesure 8m de diamètre et 0,30m de haut.

- *Arlotia Sud 3 - Tertre d'habitat* : situé à 1m au Nord du précédent. Mesure 5m de diamètre et 0,30m de haut.

- *Arlotia Sud 4 - Tertre d'habitat* : situé à 2m à l'Ouest du précédent. De très faible relief, il semble mesurer 5m de diamètre.

Historique

Ces 4 tertres ont été identifiés par [Blot J.]{.creator} en [avril 2013]{.date}.

### [Béhorlégui]{.spatial} - [Arlotia]{.coverage} (groupe Nord) - [Tertres d'habitat]{.type}

Trois tertres d'habitat se trouvent dans le vallon (situé au Nord de tertres du groupe Sud), entre les cotes 785 et 812.

- *Arlotia Nord 1 - Tertre d'habitat*

    Altitude : 735m.

    Il est situé au versant Sud du vallon.

    - Description

        Tertre terreux érigé sur terrain en pente vers le Nord. Il mesure environ 8m de diamètre et 0,40m de haut.

- *Arlotia Nord 2 - Tertre d'habitat (?)*

    - Localisation

        Altitude : 740m.

        À environ 100m au Nord du tertre précédent, et sur le versant Nord du vallon.

    Tertre terreux asymétrique de 7 de diamètre, à sommet plat. Elément douteux, (trou de blaireau à côté...).

- *Arlotia 3 - Tertre d'habitat*

    - Localisation

        Altitude : 735m.

        Erigé sur le versant Nord du vallon vers son extrémité Ouest. Il est à une trentaine de mètres au Nord-Ouest de 4 arbres plantés en carré dans le creux du vallon.

Historique

Ces 3 tertres ont été identifiés par [Blot J.]{.creator} en [avril 2013]{.date}.

### [Béhorlégui]{.spatial} - [Arlotia]{.coverage} 1 - [Tumulus]{.type}

Localisation

Carte 1346 Est Saint-Jean-Pied-de-Port.

Altitude : 736m.

Cet ensemble de tumulus est situé dans un col qui domine au Nord-Ouest le village de Béhorlégi, sur cette ligne de croupes qui va de Iramunho à Hauscoa. Le tumulus n°1 est à 30m au Nord Nord-Ouest du chalet d'Arlotia utilisé pour la chasse à la palombe, et du réservoir cylindrique en ciment qui le jouxte.

Description

Tumulus terreux érigé sur sol plat, de 16m de diamètre et 1m de haut.

Historique

Monument découvert en [juin 1971]{.date}.

### [Béhorlégui]{.spatial} - [Arlotia]{.coverage} 2 - [Tumulus]{.type}

Localisation

Il est visible à 5m au Nord Nord-Ouest de *Arlotia 1 - Tumulus*.

Description

Tumulus terreux circulaire très net de 11m de diamètre et 0,80m de haut ; les évolutions d'un bulldozer ont amené tout à fait fortuitement 4 gros blocs de [calcaire]{.subject} à son sommet...

Historique

Monument découvert en [juin 1971]{.date}.

### [Béhorlégui]{.spatial} - [Arlotia]{.coverage} 3 - [Tumulus]{.type}

Localisation

Il est situé à environ 200m au Nord-Est de *Arlotia 2 - Tumulus*, à 150m à l'Ouest d'un réservoir d'eau édifié à flanc de colline, et à l'Ouest de la tranchée comblée de pierres qui recèle la conduite d'eau issue de ce réservoir.

Description

Tumulus terreux circulaire d'un diamètre de 11m et 0,50m de haut, édifié sur un terrain en très légère pente vers l'Ouest ; il est très différent d'un «tertre d'habitat».

Historique

Monument découvert en [juin 1971]{.date}.

### [Béhorlégui]{.spatial} - [Arlotia]{.coverage} 4 - [Tumulus]{.type}

Localisation

Il est situé à environ 60m au Nord Nord-Ouest de *Arlotia 3 - Tumulus*, et près d'un pylône métallique de soutien d'un poste de rabatteur de chasse à la palombe.

Description

Tumulus terreux de 11m de diamètre et 0,40m de haut.

Historique

Monument découvert en [juin 1971]{.date}.

### [Béhorlégui]{.spatial} - [Arroskua]{.coverage} - [Cromlech]{.type}

Localisation

Altitude : 1141m.

Ce monument se trouve quelques dizaines de mètres au Nord du sommet de la colline Arroskua, laquelle domine le col de ce nom, situé au Sud.

Description

Il est situé sur un terrain plat et dépourvu des nombreuses pierres qui apparaissent en général à la surface du sol de cette colline. Une vingtaine de blocs de [calcaire]{.subject}, qui n'ont subi aucune retouche, délimitent un cercle de 8, 10m de diamètre ; le secteur Nord est particulièrement fourni. Ces blocs sont de taille modeste - de 1 à 4 poings, en moyenne - et plus ou moins facilement visibles, ne dépassant souvent que fort peu la surface.

Historique

Cercle découvert par [A. Martinez]{.creator} en [octobre 2015]{.date}.

### [Béhorlégui]{.spatial} - [Athermigna]{.coverage} - [Tertres d'habitat]{.type}

Localisation

Ce bel ensemble de 8 tertres d'habitat est dans le vallon entre Harribilibila (797m) et la cote 785, érigés sur un terrain en légère pente vers le Nord Nord-Est.

- *Athermigna TH 1* : Altitude : 760m. Tertre terreux de 8m de diamètre environ, et 1m de haut.

- *Athermigna TH 2* : situé à 4m au Nord du précédent - mesure environ 4m de diamètre et 0,30m de haut.

- *Athermigna TH 3* : situé à 6m au Nord-Est du n°2. Mesure 10m de diamètre et 0,40m de haut.

- *Athermigna TH 4* : situé à 7m au Nord-Ouest du n°3. Environ 6m de diamètre.

- *Athermigna TH 5* : à 12m au Sud-Ouest du précédent. Environ 7m de diamètre.

- *Athermigna TH 6* : situé à 5m au Nord Nord-Ouest du précédent. Mesure environ 5 à 6m de diamètre.

- *Athermigna TH 7* : situé à 6m au Nord Nord-Ouest du précédent, de très faible relief.

- *Athermigna TH 8* : situé à 7m à l'Est Nord-Est du n° 1, et de très faible relief.

Historique

Ces 8 monuments ont été découverts par [Blot J.]{.creator} en [avril 2013]{.date}.

### [Béhorlégui]{.spatial} - [Béhorlégui]{.coverage} - [Monolithe]{.type}

Localisation

Nous dirons ici quelques mots de ce curieux rocher, que l'on pourrait qualifier d'«anthropomorphe», et que toute personne traversant le village de Béhorlégui ne peut manquer d'apercevoir au dernier virage en sortant du bourg, en direction du col d'Apanicé, sur le côté gauche de la route.

Description

Il s'agit d'un bloc de [calcaire]{.subject} [grès]{.subject}eux compact de 1,85m de haut, modelé par l'érosion qui lui a donné une allure anthropomorphe : on peut en effet - avec un peu d'imagination, et en regardant de l'Ouest - distinguer une partie supérieure renflée (à tête), séparée du thorax par un rétrécissement bien marqué. Encore doit-on faire remarquer que cette « tête » était encore plus nette avant que les transports successifs (nous en reparlerons), ne lui aient enlevé d'importants fragments.

On peut distinguer ensuite le segment sous-jacent, assimilable au thorax, avec la naissance des épaules, surmontant un « abdomen » volumineux (1,40m de large), dont il est séparé par un nouveau rétrécissement ; un troisième rétrécissement annonçant les membres inférieurs... Si nous devions évoquer une ressemblance, ce serait avec les « Déesses- Mères » du Néolithique d'Europe ou du Proche-Orient, aux formes généreuses.

Historique

C'est J. Casaubon qui, en mai 2008, nous a mis en relation avec le héros de cette histoire. Il y a quelques décennies en effet, le maire de Béhorlégui - à l'époque [Jean Cubiat]{.creator} - se rendant en voiture vers les pâturages d'Apanicé, vit sur sa droite, au col de Landerre, à quelques dizaines de mètres en contre-bas de la route, ce bloc de pierre aux formes curieuses. Il avait été déplacé à cet endroit lors des touts récents travaux de creusement de la D 117, mais semblait avoir séjourné sous terre, comme le laissait supposer sa teinte, et la terre dont il était encore abondamment enveloppé. Quelques jours plus tard, le monolithe avait disparu. C'est tout à fait fortuitement que J. Cubiat le revit, dans une propriété privée, à Ordiarp. Considérant que cette pierre appartenait à sa commune, le maire de Béhorlégui obtint donc que ce monolithe soit ramené ; toutefois, pour éviter toute nouvelle disparition, il jugea préférable de le disposer à sa place actuelle...

Ce monolithe « anthropomorphe », a-t-il été vu comme tel dans le passé, quand il était à l'air libre ? A-t-il fait l'objet de dévotion en des temps lointains ? Nous ne le saurons jamais, mais rien n'interdit de le penser...

### [Béhorlégui]{.spatial} - [Harribilibila]{.coverage} Sud - [Tertres d'habitat]{.type} ([?]{.douteux})

Localisation

Altitude : 740m.

Les 4 tertres d'habitat se trouvent à flanc de colline, longés par une draille, et dominant des dolines.

Description

Quatre tertres de terre, tangents, s'échelonnent selon un axe Nord-Ouest Sud-Est.

Ils mesurent 4m à 5m de diamètre pour un dénivelé de 1m. Des trous de blaireau pourraient faire penser que ces tertres sont le résultat de ces trous... mais rien n'est évident !

Historique

Monuments découverts par [Blot J.]{.creator} en [avril 2013]{.date}.

### [Béhorlégui]{.spatial} - [Ithurrotché]{.coverage} - [Tumulus]{.type}

Localisation

Carte 1446 Ouest Ordiarp.

Altitude : 1010m.

Il est tangent, au Nord, à l'ancienne piste pastorale qui, au Sud-Est du pic de Béhorlégi, mène du col d'Apanicé à la fontaine d'Oxolatzé ; il est à 200m à vol d'oiseau à l'Est de ce col. On le trouve sur un petit ressaut entre deux dépressions, à 50m au Sud de la route goudronnée.

Description

Tumulus terreux très visible de 6m de diamètre et 0,50m de hauteur ; quelques pierres sont visibles à sa périphérie sans que l'on puisse parler de péristalithe.

Historique

Monument découvert en [août 1973]{.date}.

### [Béhorlégui]{.spatial} - [Ithurrotché]{.coverage} - [Monolithe]{.type}

Localisation

Altitude : 1045m.

Il est situé au niveau du col d'Apanicé, au lieu-dit *Ithurrotché*, sur le bord droit d'un petit chemin goudronné, qui part vers le Nord, à environ 100m de son détachement de la D 117.

Description

Le monolithe gît sur un sol légèrement incliné vers le Nord (sur le versant de la Bidouze) ; il a la forme d'un parallélépipède rectangle de [calcaire]{.subject} compact, assez régulier dans l'ensemble, mesurant 4m de long ; dans sa partie médiane la plus large, il mesure 0,70m et 0,61m d'épaisseur. Sa base, de section rectangulaire, (0,70m de large et 0,70m d'épaisseur) est assez régulière et plane dans son ensemble. Son sommet orienté plein Nord présente des traces d'épannelage - afin de lui donner sa forme pointue (0,37m dans sa partie la plus large)- qui sont bien visibles au niveau de son bord Nord-Ouest. Il ne semble pas y avoir eu d'autres retouches, mais on doit signaler, le long et au milieu du bord Ouest du monolithe, un bloc de pierre du même type, qui pourrait bien avoir été détaché du bloc d'origine afin de lui donner sa forme régulière définitive. Comme d'habitude en Pays Basque, c'est un bloc naturel d'emblée très proche des formes et mensurations désirées qui a été choisi et amené en ces lieux, et il n'a donc eu à subir que peu de modifications.

On remarque enfin qu'il est situé tout à côté de la naissance même d'un petit ruisseau permanent ; la présence de ce point d'eau confirme le très probable rôle de borne pastorale de ce monolithe, dont l'environnement archéologique est riche.

Historique

Ce monument nous a été signalé en [2006]{.date} par [Jacques Casaubon]{.creator}.

### [Béhorlégui]{.spatial} - [Mugarco]{.coverage} - [Tumulus]{.type}

Localisation

Au sommet de la colline dénommée *Mugarco* par les bergers, à 500m au Nord Nord-Est des bergeries d'Elhorta.

Altitude : 1083m.

Description

Au sommet de cette colline on peut distinguer deux petits tumuli, en forme de galettes, circulaires, chacun mesurant 1,80m de diamètre et 0,25m de haut. Le plus au Sud est séparé du second, au Nord-Ouest, par environ une dizaine de mètres. Ils sont essentiellement constitués de terre et de petits galets du [calcaire]{.subject} environnant, bien visibles en particulier à leur périphérie.

Historique

[Nous]{.creator} nous sommes rendus sur les lieux en [mai 2008]{.date} avec [J. Casaubon]{.creator} sur les indications du [berger Lérissa]{.creator}, de la maison Aristalde de Béhorlégui.
