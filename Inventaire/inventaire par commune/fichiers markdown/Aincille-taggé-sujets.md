# Aincille

### [Aincille]{.spatial} - [Goyhenetchea]{.coverage} 1 - [Tumulus]{.type}

Localisation

Altitude : 417m.

On trouve ce tumulus au sommet de la colline qui surplombe, à l'Ouest, la maison Goyhenetchea.

Description

Erigé sur terrain plat, ce tumulus de terre, de forme circulaire, mesure 9 mètres de diamètre et 0,80m de haut.

Historique

Tumulus découvert en [janvier 2011]{.date} par [J. Blot]{.creator}.

### [Aincille]{.spatial} - [Goyhenetchea]{.coverage} 2 - [Tumulus]{.type}

Localisation

À 20 mètres à l'Ouest Sud-Ouest de *Goyhenechea 1 - Tumulus*.

Description

Tumulus circulaire, mixte, de terre et de pierres, mesurant 5m de diamètre et 0,30m à 0,40m de haut.

Historique

Tumulus découvert par [J. Blot]{.creator} en [janvier 2011]{.date}.

### [Aincille]{.spatial} - [Goyhenetchea]{.coverage} 3 - [Tumulus]{.type}

Localisation

Situé à 20 mètres au Sud de *Goyhenetchea 2 - Tumulus*.

Description

Tumulus mixte, circulaire, mesurant 3,50m de diamètre et 0,20m de haut.

Historique

Tumulus découvert par [J. Blot]{.creator} en [janvier 2011]{.date}.

### [Aincille]{.spatial} - [Handiague]{.coverage} 1 - [Tertre d'habitat]{.type}

Localisation

Altitude : 700m.

Il est situé à l'extrémité Est de la longue crête d'Handiague, un peu décentré vers le Sud par rapport au sommet, érigé sur un terrain en légère pente vers l'Est. Il est à 80m à l'Ouest d'une piste pastorale et à 70m au Nord d'une seconde, carrossable, perpendiculaire à la première.

Description

Tumulus de 6m de diamètre et 0,40m de haut à l'Ouest, présentant une dépression centrale de cinq à dix centimètres de profondeur. Son asymétrie, et le terrain en pente semblent évoquer un tertre d'habitat, quoique les points d'eau soient assez éloignés.

Historique

Tertre découvert par [Meyrat F.]{.creator} en [août 2012]{.date}.

### [Aincille]{.spatial} - [Handiague]{.coverage} 2 - [Tertre d'habitat]{.type}

Localisation

Altitude : 730m.

Il est situé à la naissance d'un petit ruisseau sur le flanc Nord du plus haut sommet de la crête d'Handiague (761m d'altitude), à proximité de la piste qui y monte.

Description

Tertre circulaire de près de 4m de diamètre et 0,80m de haut, constitué de terre.

Historique

Tertre découvert par [F. Meyrat]{.creator} en [janvier 2013]{.date}.

### [Aincille]{.spatial} - [Handiague]{.coverage} 2 - [Cromlech]{.type} [(?)]{.douteux}

Localisation

Altitude : 480m.

Les deux cromlechs *C2* et *C3 (?)* sont quasi tangents et situés à l'extrémité d'un promontoire qui apparaît au flanc Ouest de la crête d'Handiague.

Description

Petit cromlech de 3,50m de diamètre ; 4 pierres de faible volume sont visibles dans la moitié Nord, et 1 autre au Sud-Ouest. Légère dépression centrale.

Historique

Monument trouvé par le [groupe Hillarriak]{.creator} en [janvier 2013]{.date}.

### [Aincille]{.spatial} - [Handiague]{.coverage} 3 - [Cromlech]{.type} [(?)]{.douteux}

Localisation

Distant d'une trentaine de centimètres au Nord-Ouest de *Handiague 2 - Cromlech (?)*.

Description

9 pierres de très faible volume, au ras du sol, sont visibles sur un demi-cercle qui serait la moitié Nord du cromlech. Légère dépression visible au centre.

Historique

Monument trouvé par le [groupe Hillarriak]{.creator} en [janvier 2013]{.date}.

### [Aincille]{.spatial} - [Handiague]{.coverage} 4 - [Cromlech]{.type}

Localisation

Altitude : 533m.

Toujours sur le flanc Ouest de la crête Handiague, mais sur un promontoire plus en altitude que celui qui portent *Handiague 2 - Cromlech (?)* et *Handiague 3 - Cromlech (?)*.

Description

Ces deux monuments sont eux aussi tangents.

Ce monument se rattache aux cromlechs surélevés ou aux « tumulus-cromlechs ». Tumulus de 0,30m de haut environ avec une dépression centrale peu marquée. Au centre une « structure » formée de 3 pierres d'un volume notable (0,90m x 0,40m en moyenne) plus 3 ou 4 autres nettement plus petites pourraient représenter une [« ciste » centrale]{.subject}. À la périphérie du tumulus une dizaine de pierres de volume modeste semblent délimiter un cercle de 6m de diamètre. Etant proche d'une rupture de pente, des phénomènes de [soutirage]{.subject} semblent avoir eu lieu dans le secteur Sud-Est, par contre le secteur Nord-Est paraît avoir bénéficié d'un [empierrage de soutènement]{.subject}.

Historique

Monument découvert par le [groupe Hillarriak]{.creator} en [janvier 2013]{.date}.

### [Aincille]{.spatial} - [Handiague]{.coverage} 5 - [Cromlech]{.type}

Localisation

À quelques centimètres au Nord-Ouest de *Handiague 4 - Cromlech*.

Description

Monument très légèrement tumulaire, et petite dépression centrale, de 4m de diamètre. Une vingtaine de pierres sont visibles en périphérie, au ras du sol, plus nombreuses dans la moitié Est, la moitié Ouest est le siège de soutirage dû à la proximité de la rupture de pente.

Historique

Monument découvert par le [groupe Hillarriak]{.creator} en [janvier 2013]{.date}.

### [Aincille]{.spatial} - [Handiague]{.coverage} 8 - [Cromlech]{.type}

Localisation

Altitude : 690m.

Situé sur un plateau, avant la rupture de pente, il est à une quinzaine de mètres à l'Est de la piste qui descend vers le niveau inférieur de la crête d'Handiague.

Description

Cercle de 3m de diamètre environ, délimité par 4 pierres dont 3 blocs de poudingue très visibles (le plus grand, au Nord, mesure 0,90m x 0,90m et 0,40m de haut).

Historique

Monument trouvé par [Blot J.]{.creator} en [janvier 2013]{.date}.

### [Aincille]{.spatial} - [Handiague]{.coverage} 7 - [Cromlech]{.type} [(?)]{.douteux}

Localisation

Altitude : 623m.

Description

« Monument » très curieux constitué de grandes pierres, plus ou moins espacées, (pouvant atteindre 1,80m de haut) dont certaines paraissent naturellement en place, d'autre non, délimitant un cercle (approximatif) de 10m de diamètre, dont le centre est parfaitement plat et dépourvu de pierres. Est-ce un espace parfaitement naturel, ou aménagé (parc pastoral ? espace funéraire, lieu de réunion ?).

Historique

Monument signalé par le [groupe Hillarriak]{.creator} en [janvier 2013]{.date}.
