# Saint-Just-Ibarre

### [Saint-Just-Ibarre]{.spatial} - [Belchou]{.coverage} 9 - [Cromlech]{.type}

Localisation

À 150m à l'Est Nord-Est de *Belchou 8 - Cromlech*, dans la commune d'Hosta, sur un terrain légèrement en pente vers le Nord, toujours en bordure du relèvement de la cuvette.

Description

Petit cercle de 4m de diamètre délimité par une vingtaine de pierres au ras du sol.

Historique

Monument découvert en [août 1978]{.date}.

À 100m à l'Est Nord-Est de ce monument et à 10m à l'Est d'une profonde doline peut-être pourrait-on décrire un dixième monument, un cercle de pierres, quasi tangent à la piste, de 4,10m de diamètre et formé par 7 pierres ; il nous paraît toutefois douteux.

### [Saint-Just-Ibarre]{.spatial} - [Belchou]{.coverage} 14 - [Cromlech]{.type}

Localisation

Altitude : 830m.

À une centaine de mètres à l'Est Nord-Est du cromlech *C 13* (voir Hosta).

À noter que le cromlech *C 8* déjà publié [@blotInventaireMonumentsProtohistoriques2009 p.38]) se trouve lui-même à quelques dizaines de mètres à l'Est

Altitude : 830m

Description

Cercle de 7m de diamètre, érigé sur un terrain en légère pente vers le Nord-Est, délimité par 8 pierres bien visibles, mais absentes dans le quart Nord-Ouest ; une autre plus volumineuse se voit au centre.

Historique

Monument découvert par [Blot J.]{.creator} en [août 2011]{.date}.

### [Saint-Just-Ibarre]{.spatial} - [Belchou]{.coverage} (groupe Nord-Est) - [Tertres d'habitat]{.type}

Localisation

Altitude : 860m.

Cet important groupe de 33 tertres d'habitat est situé au pied du pic de Belchou, au débouché du vallon compris entre ce dernier et l'éminence de 998m d'altitude situé à son flanc Sud-Est.

Description

Ces 33 tertres sont érigés, de part et d'autre d'un petit ruisseau, sur un terrain en très légère pente, soit vers le Sud-Est (tertres de 1 à 18), soit vers le Nord-Ouest (tertres de 19 à 33).

- Premier groupe au Nord-Ouest du petit ruisseau (tertres 1 à 18) :
    - Le *tertre n°1* est le plus au Nord du groupe et mesure 6 x3m ; 
    - le *n°2* est à 5m au Sud-Ouest (4 x 2m) ; 
    - le *n°3* est à 4m au Sud-Ouest du précédent (15 x 8m) ; 
    - le *n°4* est à 25m au Sud-Est du précédent (13 x 10m)  ; 
    - le *n°5* est à 8m Sud-Ouest (16 x 11m) ; 
    - le *n°6* est tangent (4m de diamètre) ; 
    - le *n°7* tangent aussi (10m de diamètre) ; 
    - le *n°8* à 2m au Nord-Ouest (6m de diamètre) ; 
    - le *n°9* à 6m au Sud (10m de diamètre) ; 
    - le *n°10*, tangent (5m de diamètre) ; 
    - le *n°11* tangent aussi au n°9 (9m x 3m) ; 
    - le *n°12* tangent au n°11 (9m x 5m), 
    - le *n°13* tangent au 12, (13m x 10m) ; 
    - le *n°14*, tangent au 13 (13m x 9m ) ; 
    - le *n°15* à 3m au Sud du 14 (10m x 8m) ; 
    - le *n°16* à 4m au Sud du 15 (13m x 10m) ; 
    - le *n°17* à 7m au Sud du 16 (11m de diamètre) ; 
    - le *n°18* à 3m au Sud du 17 (3m de diamètre).

- Deuxième groupe, au Sud-Est du petit ruisseau : (tertre 19 à 33) :
    - Le *tertre n°19* à 15m au Sud-Est du 18 (10m de diamètre) ; 
    - le *n°20*, à 24m à l'Est du 19 (12m x 9m ) ; 
    - le *n°21* à 5m au Nord du 20 (13m x 10m) ; 
    - le *n°22* à 2m à l'Est du 21 (7m x 3m ) ; 
    - le *n°23* (à 3m au Nord-Ouest du 21 (12m x 7) ; 
    - le *n°24* à 20m au Nord-Est du 23 (8m de diamètre) ; 
    - le *n°25* à 5m à l'Est du 24(6m de diamètre) ; 
    - le *n°26*, à 4m à l'Est du 25 (7m de diamètre) ; 
    - le *n°27*, à 8m au Nord du 26 (14m x 8m) ; 
    - le *n°28* à 8m au Nord du 27 (11m x 9m) ; 
    - le *n°29*, à 10m au Nord-Ouest du 28 (12m x 8m) ; 
    - le *n°30*, à 25m au Nord-Est du 29, (10m x 4m), 
    - le *n°31*, à 2m au Nord-Est du 30 (5m de diamètre) ; 
    - le *n°32*, à 7m au Nord-Est du 31 (6m de diamètre) ; 
    - le *n°33*, à 8m au Nord-Est du 32 (9m x 4m).

Historique

Tertres découverts par [Blot J.]{.creator} en [1986]{.date}.
