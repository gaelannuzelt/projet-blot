# Urrugne

### [Urrugne]{.spatial} - [Aire Leku]{.coverage} 1 - [Tumulus-cromlech]{.type}

Localisation

Altitude : 290m.

Très visible à une vingtaine de mètres au Nord de la piste principale, au milieu du plateau.

Description

Tumulus pierreux de 7m de diamètre et 0,40m de haut constitué d'assez volumineux blocs de grés. On note un péristalithe très net, particulièrement visible dans sa moitié Nord-Ouest, où il semble y avoir une alternance de 3 grandes pierres avec d'autres plus petites, environ tous les 2m.

Historique

Monument découvert par [J. Blot]{.creator} en [octobre 2009]{.date}.

### [Urrugne]{.spatial} - [Aire Leku]{.coverage} 1 - [Cromlech]{.type} [(?)]{.douteux}

Localisation

Altitude : 290m.

Il est à tangent au Nord de la piste principale, et se trouve à une trentaine de mètres au Sud-Est de *Aire Leku 1 - Tumulus-cromlech*.

Description

Le cercle, de 3m de diamètre est délimité par huit pierres au ras du sol.

Historique

Monument, douteux, découvert par [J. Blot]{.creator} en [octobre 2009]{.date}.

### [Urrugne]{.spatial} - [Aire Leku]{.coverage} 2 - [Cromlech]{.type}

Localisation

Altitude : 285m.

Il est situé à l'extrémité Est du plateau, à environ une trentaine de mètres à l'Ouest d'un cayolar en ruines.

Description

Cromlech très légèrement surélevé, de 5m de diamètre, délimité par environ 7 pierres, visibles surtout dans la moitié Est de la périphérie ; le centre est marqué par deux autres pierres.

Historique

Monument découvert par [J. Blot]{.creator} en [octobre 2009]{.date}.

### [Urrugne]{.spatial} - [Aire Leku]{.coverage} 2 - [Tumulus-cromlech]{.type}

Localisation

Altitude : 290m.

Il est situé à 5m au Nord de la piste principale et à quelques dizaines de mètres au Sud-Ouest de *Aire Leku 1 - Tumulus-cromlech*.

Description

Petit tumulus pierreux de 0,30m à 0,40m de haut, en grande partie recouvert de broussailles, entouré d'un bourrelet de terre circulaire de 5,20m de diamètre où apparaissent de nombreuses pierres.

Historique

Monument découvert par [J. Blot]{.creator} en [avril 2010]{.date}.

### [Urrugne]{.spatial} - [Aire Leku]{.coverage} 3 - [Cromlech]{.type}

Localisation

Il est tangent à l'Ouest de *Aire Leku 2 - Tumulus-cromlech*.

Description

Petit cercle de 1,50m de diamètre, souligné par un petit bourrelet de terre dans lequel apparaissent de nombreuses petites pierres.

Historique

Monument découvert par [J. Blot]{.creator} en [avril 2010]{.date}.

### [Urrugne]{.spatial} - [Aire Leku]{.coverage} 4 - [Cromlech]{.type}

Localisation

Monument tangent au Sud-Ouest de *Aire Leku 2 - Tumulus-cromlech* et au Sud de *Aire Leku 3 - Cromlech*.

Description

Une quinzaine de pierres au ras du sol délimitent un cercle de 1,90m de diamètre, au centre duquel on note une légère dépression.

Historique

Monument découvert par [J. Blot]{.creator} en [avril 2010]{.date}.

### [Urrugne]{.spatial} - [Aire Leku Lepoa]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 250m.

Il est situé dans le petit col séparant le Plateau d'Aire-Leku de la colline Muxugorrigane, tangent au Sud de la piste qui les relie.

Description

Tumulus pierreux de 3m à 4m de diamètre, et d'environ 0,40m de haut.

Historique

Monument découvert par [J. Blot]{.creator} en [octobre 2009]{.date}.

### [Urrugne]{.spatial} - [Aire Leku]{.coverage} - [Pierre plantée]{.type} [(?)]{.douteux}

Localisation

Altitude : 285m.

Elle se trouve à flanc Nord du plateau, à l'amorce de son inclinaison vers le Nord.

Description

Pierre ou dalle épaisse en forme de tronc de cône, plantée dans le sol, mesurant 1,20m de haut, 0,80m à sa base, et 0,40m en moyenne d'épaisseur. Pas de traces évidentes d'épannelage.

Historique

Pierre découverte par [J. Blot]{.creator} en [octobre 2009]{.date}.

### [Urrugne]{.spatial} - [Aire Leku]{.coverage} Nord - [Dalle plantée]{.type} [(?)]{.douteux}

Localisation

Altitude : 200m.

On le trouve à environ 2m au Nord-Ouest de l'ancienne piste pastorale qui descend directement vers Urtubienborda.

Description

Dalle de grés de forme lancéolée à pointe supérieure, plantée dans le sol mais inclinée vers le Sud-Ouest. On ne distingue pas de traces d'épannelage.

Historique

Dalle découverte par [I. Txintxuretta]{.creator} en [2009]{.date}.

### [Urrugne]{.spatial} - [Aire Ona]{.coverage} - [Monolithe]{.type}

Localisation

Altitude : 75m.

Ce monolithe se trouve dans un champ, près de la clôture le séparant de la route, à quelques mètres à l'Est de la bifurcation routière au niveau du camping Aire Ona.

Description

Monolithe planté dans le sol, en forme de pain de sucre mesurant 0,60m à sa base la plus large, et 1,80m de haut. Il ne semble pas présenter de traces d'épannelage ni d'inscriptions.

Historique

Monolithe découvert par [I. Txintxuretta]{.creator} en [2005]{.date}.

### [Urrugne]{.spatial} - [Anduretako Erreka]{.coverage} - [Pierre plantée]{.type}

Localisation

Altitude : 227m.

Il est sur un terrain en pente, au flanc Nord-Est de Mugi, dominant le cours du ruisseau Anduretako erreka.

Description

Bloc de grés verticalement planté, de forme rectangulaire, mesurant 1,20m de haut, dont la base, épaisse, rectangulaire à la coupe mesure 0,67m x 0,55m ; le sommet est beaucoup plus mince (0,13m) et lui donne un aspect triangulaire, vue de profil.

Historique

Pierre découverte en [mars 2012]{.date} par [D. Ibarzola]{.creator}.

### [Urrugne]{.spatial} - [Bartzeleku haut]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 390m.

Il est situé à l'extrémité Nord-Est de la croupe du même nom, dans un petit col, avant la remontée du terrain vers le Nord.

Description

Petit tumulus de terre et de pierres de 3m de diamètre et 0,30m de haut.

Historique

Monument découvert par [Blot J.]{.creator} en [mars 2012]{.date}.

### [Urrugne]{.spatial} - [Erentzu]{.coverage} 3 - [Dolmen]{.type}

Localisation

Carte 1245 OT. Hendaye-St Jean-de-Luz.

Altitude : 420 m.

Erentzu n°3 est situé à 50m à l'Est Nord-Est de la BF 15, sur un terrain en pente douce vers l'Est, mais qui présente un très léger replat à cet endroit.

Description

Il ne reste de visible, semble-t-il, qu'une belle dalle de grés enfoncée dans le sol mesurant 1,20m de long, 0,40m de haut, et 8 centimètres d'épaisseur en moyenne. Elle est légèrement inclinée vers le Nord, et orientée Est Nord-Est. Tout son pourtour libre présente des traces d'épanelage. Une épaisse végétation de robustes genêts rend difficile l'appréciation d'éventuels autres éléments architecturaux. On ne note pas la présence d'un tumulus, lequel peut avoir été éliminé par solifluxion. Cette dalle unique paraît cependant pouvoir être considérée, par ses caractéristiques, comme les restes d'un petit dolmen de montagne.

Historique

Ce vestige a été découvert par [I. Gaztelu]{.creator} en [1989]{.date}. Les deux autres dolmens d'Erentzu, ont été trouvés, le *n°1*, par Barandiaran J.M., et le *n°2* par Blot J.

### [Urrugne]{.spatial} - [Barzeleku]{.coverage} 2 - [Dolmen]{.type}

Localisation

Altitude : 290m.

Au col situé au flanc Sud de la colline Mokua, prendre un sentier à droite qui mène à un petit replat orienté vers l'Ouest.

Description

On note, au sol, une dalle de grés rose rectangulaire, mesurant 1,70m de long pour environ 0,75m de large et 0,20m d'épaisseur. Elle est orientée Est-Ouest, et à son extrémité Nord-Ouest, on note une dalle plantée de 0,71m de long, 0,21m d'épaisseur et 0,23m de haut ; de même qu'apparaît une autre dalle à son autre extrémité Sud-Ouest, de dimensions difficilement appréciables (enfouie).

Historique

Monument remarqué depuis longtemps par [F. Iturria]{.creator} (ONF) qui nous l'a signalé en [septembre 2010]{.date}.

### [Urrugne]{.spatial} - [Barzeleku]{.coverage} - [Monolithe]{.type} [(?)]{.douteux}

Localisation

Altitude : 295m.

À quelques mètres à l'Ouest de *Barzeleku 2 - Dolmen*, ce monolithe (ou simple rocher ?), attire l'attention par sa position dominante sur le col et sur tout l'horizon à l'Ouest.

Description

Il mesure 2,80m de haut, 2,30m à sa base, et environ 0,90m d'épaisseur. Il semble posséder des pierres de calage à sa base, côté Ouest. On note qu'un volumineux bloc a été séparé du flanc Sud, par un phénomène de gel semble-t-il. Il ne semble pas y avoir de traces d'épannelage.

Historique

Elément noté par [J. Blot]{.creator} en [septembre 2010]{.date}.

### [Urrugne]{.spatial} - [Biskartxu]{.coverage} - [Tumulus]{.type} [(?)]{.douteux}

Localisation

Altitude : 380m.

Il se trouve à l'extrémité d'un petit replat, à une vingtaine de mètres au Nord-Est du GR 10, après que celui-ci ait contourné par le Nord la colline de Mandale, en venant d'Ibardin.

Description

Il s'agit d'une structure constituée d'une quarantaine de pierres enfoncées dans le sol, affectant la forme d'une galette circulaire de 3m de diamètre dont 3 pierres jointives pourraient concrétiser le centre. Cet ensemble, bien que douteux, est très semblable au *tumulus Apatessaro 8* [@blotInventaireMonumentsProtohistoriques2009, p.39], ou au *tumulus Erreta 1* [@blotInventaireMonumentsProtohistoriques2009 p.36] ou encore au *tumulus Caminarte 3*.

Historique

Tumulus découvert pat [Blot J.]{.creator} en [novembre 2010]{.date}.

### [Urrugne]{.spatial} - [Descarga (Grand)]{.coverage} 2 - [Dolmen]{.type} [(?)]{.douteux}

Localisation

Commune d'[Ascain]{.spatial} (enclave dans la commune d'Urrugne)

Altitude : 271m.

Description

Une quarantaine de blocs pierreux éparpillés sans ordre apparent, pourraient être les restes d'un tumulus pierreux ayant 4,50m de diamètre environ. (Ce sont les seules pierres visibles dans l'environnement immédiat).

Au centre de ce groupement pierreux, se remarque une dalle de grés local, de forme grossièrement rectangulaire, mesurant 1,27m x 1,20m, à grand axe Nord-Sud. Son bord Ouest est épais (0,39m) et la dalle va en s'amincissant vers son bord Est, qui ne fait qu'un à trois centimètres d'épaisseur. À une vingtaine de centimètres au Nord du bord Nord existe un bloc pierreux de 0,86m x 0,35m ; de même au Sud, à 0,80m, il est une deuxième pierre de 0,92m x 0,58m. L'ensemble de tous ces éléments fait soupçonner un probable monument (dolmen ?) que seule une exploration un peu plus précise permettra de confirmer.

Historique

Cet ensemble pierreux avait été remarqué depuis longtemps par de nombreux promeneurs et par nous-mêmes, sans avoir jamais été décrit.

### [Urrugne]{.spatial} - [Erentzu]{.coverage} 4 - [Dolmen]{.type} [(?)]{.douteux}

Localisation

Altitude : 350m.

Il est situé au flanc Nord du mont Erentzu, sur un terrain en légère pente.

Description

On ne distingue, à l'heure actuelle, qu'une belle dalle de grés rose local, verticalement plantée dans le sol, dont le sommet semble avoir été volontairement retaillé ; elle mesure 1,30m de long à sa base, 1m de haut et 0,20m d'épaisseur. De nombreux fragments de dalles sont visibles aux alentours ; il est possible que des phénomènes de colluvion, au cours des siècles, aient modifié la nature du sol qui a pu être plus horizontal dans le passé, comme cela semble avoir été le cas pour nombre de dolmens existant au flanc Nord du Xoldokocelay.

Historique

Ce monument nous a été signalé par [F. Iturria]{.creator} (ONF) en [septembre 2010]{.date}.

### [Urrugne]{.spatial} - [Erentzu]{.coverage} 5 - [Dolmen]{.type}

Localisation

Altitude : 390m.

Description

Au milieu d'un tumulus pierreux de 8m de diamètre et 0,40m de haut, on peut distinguer un ensemble de dalles, dépassant de peu le sol, qui semblent bien délimiter une chambre funéraire, à grand axe Nord-Sud, d'environ 2,30m de long et 1,20m de large. Une grande dalle de 1,94m de long délimite la paroi Est, tandis que, à l'Ouest, la paroi est matérialisée par les sommets de 3 dalles (d'environ 0,35m chacun). Au Nord et au Sud, d'autres pierres pourraient faire partie de la chambre.

Historique

Monument découvert par [J. Blot]{.creator} en [janvier 2011]{.date}.

### [Urrugne]{.spatial} - [Erentzu]{.coverage} - [Dalle couchée]{.type} ? Filon naturel ?

Localisation

Altitude : 343m.

Dalle située à une dizaine de mètres au Nord-Ouest de la piste qui se dirige vers la frontière, du ruisseau qui la longe, et d'une pierre gravée d'un lauburru daté de 1894.

Description

Il s'agit, en apparence, d'une dalle de grés rose de 3,60m de long et 1,35m de large et 0,12m d'épaisseur en moyenne, allongée selon un axe Nord-Ouest Sud-Est. Toutefois, un décapage plus poussé de cette dalle (Meyrat F., juin 2011) a montré qu'elle se prolongeait sous l'humus, bien au-delà, en particulier à son flanc Nord-Est et à son extrémité Sud-Est (qui lui donne alors une longueur de 4,30m). Il nous semblerait peut-être plus valable de parler de filon naturel...

Historique

Dalle découverte par [Blot J.]{.creator} en [janvier 2011]{.date}.

### [Urrugne]{.spatial} - [Erentzu]{.coverage} 3 - [Dolmen]{.type}

Localisation

Altitude : 420m.

Il est situé à 50m environ de la BF 15 (entre la 15 et la 16), à 4m au Nord du sentier frontière, et sur un terrain en déclivité vers l'Est.

Description

Ce monument a déjà été décrit par nous dans notre *Inventaire* [@blotInventaireMonumentsProtohistoriques2010 p.33], mais de récents travaux de défrichage, signalés par F. Ithurria en juin 2011, ont permis de mieux apprécier ce qui reste de ce monument qui était enfoui sous une abondante végétation de robustes genêts. Outre la grande dalle de grés local déjà décrite, (orientée Nord-Est Sud-Ouest, qui mesure 1,33m de long, de 0,08m à 0,15m d'épaisseur, et 0,35m de haut), on peut maintenant distinguer une autre dalle de dimensions plus modestes, orientée Sud-Ouest Nord-Est, qui lui est *presque* perpendiculaire (à son extrémité Nord-Ouest). Elle mesure 0,66m de long, 0,11m d'épaisseur et 0,36m de haut, matérialisant ainsi une chambre funéraire orientée Nord-Ouest Sud-Est, mesurant environ 1,80m de long et 0,80m à 1m de large.

Historique

Monument découvert par [I. Gaztelu]{.creator} en [1989]{.date}.

### [Urrugne]{.spatial} - [Erentzu]{.coverage} 6 - [Dolmen]{.type}

Localisation

Altitude : 345m.

On le trouve à quelques dizaines de mètres au Sud-Ouest du «parking» anciennement utilisé par les carriers d'Erentzu et ses environs, à 25m environ au Nord de *Erentzu 4 - Dolmen (?)*.

Description

On note tout d'abord, sur un terrain en légère pente vers le Nord, une belle dalle de grés rose, implantée selon un axe Nord-Sud haut (légèrement inclinée vers l'Est), mesurant 1,30m à sa base, 0,60m de haut et d'une vingtaine de centimètres d'épaisseur. Elle se situe dans un tumulus pierreux, d'environ 5,50m de diamètre et O,30m de haut. Il semble bien que l'on puisse distinguer, à la périphérie Nord de ce tumulus, une dizaine de pierres du péristalithe..

Historique

Monument découvert par [P. Badiola]{.creator} en [mai 2011]{.date}.

### [Urrugne]{.spatial} - [Erentzu]{.coverage} 7 - [Dolmen]{.type} [(?)]{.douteux}

Localisation

Altitude : 370m.

On le trouve dans le même secteur que *Errentzu 6 - Dolmen*, plus en hauteur, à environ 80m au Sud Sud-Est de *Erentzu 4 - Dolmen (?)*.

Description

Au centre d'un tumulus constitué essentiellement de petites dalles de grés rose, mesurant 5,60m de diamètre et 0,40m de haut, apparaît le sommet d'une dalle verticale, orientée Est-Ouest, d'environ 1,50m de long, 0,50m de haut et une vingtaine de centimètres d'épaisseur. Nous faisons quelques réserves quant à l'authenticité de ce dolmen, en raison de son tumulus (uniquement dalles et diamètre plutôt réduit), et du contexte immédiat (une exploitation de carriers...).

Historique

Monument découvert par [P. Badiola]{.creator} en [mai 2012]{.date}.

### [Urrugne]{.spatial} - [Erentzu]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 420m.

Il est situé à 14m à l'Ouest Nord-Ouest de la BF 15. À noter qu'il existe un second tumulus en territoire navarrais, à environ 12m au Sud-Est de la BF 15, de l'autre côté du barbelé frontière (information I. Gastelu).

Description

Tumulus pierreux, en forme de galette circulaire de 4m de diamètre et 0,30m de haut, présentant une excavation centrale.

Historique

Tumulus découvert par [Blot J.]{.creator} en [février 2012]{.date}.

### [Urrugne]{.spatial} - [Galbario]{.coverage} 3 - [Dolmen]{.type}

Localisation

Carte 1245 Ouest Saint-Jean-de-Luz.

Altitude : 224m.

Les *dolmens Galbario 1* et *2* ont été décrits en 1966, [@chauchatSeptDolmensNouveaux1966 p.6]. Altitude : 190m. ; le n°2 se trouve à 200m au Sud du n°1.

Le n°3 se trouve à environ 400m à l'Est Nord-Est de la Croix du Calvaire, sur une petite éminence bien délimitée et bien visible.

Description

Tumulus mixte de terre et de pierres, de 8m de diamètre et 0,80m de haut. La chambre funéraire rectangulaire, visible au centre, mesure 2,40m de long et 0,60m de large et est orientée Est-Ouest. Près d'une trentaine de petites dalles de grès local en marquent les limites. La paroi Sud est particulièrement bien délimitée par 9 d'entre-elles, de quelques centimètres de haut, mais restées bien verticales ; il n'y a pas de couvercle visible.

Historique

Dolmen découvert en [mars 1970]{.date}. Totalement détruit par les travaux de passage du gazéoduc.

### [Urrugne]{.spatial} - [Galbario]{.coverage} 4 - [Dolmen]{.type}

Ont déjà été publiés les *dolmens Galbario 1* et *2* [@chauchatSeptDolmensNouveaux1966, p.100].

Localisation

Carte 1245 OT. Hendaye-St Jean-de-Luz.

Altitude : 250m.

Ce monument est situé à l'Ouest du chemin qui relie le sommet du Mont du Calvaire au petit col situé à son flanc Sud, d'où part la piste qui mène au Mont Xoldokogaina. Il est érigé sur un petit replat, et à 4m au Nord d'un pylône d'une ligne à haute tension.

Description

Sur un terrain en très légère pente vers le Sud, on distingue un tumulus de quelques centimètres de haut et de 7,50m de diamètre, constitué de terres et de pierres. Au centre apparaît une dalle, de 0,88m de long et 0,23m d'épaisseur, orientée Nord-Sud et inclinée vers l'Est. À l'extrémité Nord-Est de celle-ci, on distingue une autre dalle, horizontale, en grande partie cachée sous l'humus, et qui pourrait mesurer environ 0,57m de large. Ces deux éléments semblent pouvoir être rattachés à la chambre funéraire d'un petit dolmen de montagne.

Historique

Monument découvert par [I. Txintxurreta]{.creator}.

### [Urrugne]{.spatial} - [Galbario]{.coverage} - [Tumulus]{.type}

Localisation

Carte 1245 OT. Hendaye-St Jean-de-Luz.

Altitude : 270m.

On le trouve sur terrain plat, à une centaine de mètres au Nord Nord-Ouest de *Galbario 4 - Dolmen*, à l'extrémité du plateau où est érigée la croix du calvaire, à environ une centaine de mètres au Sud-Ouest de celle-ci.

Description

Tumulus circulaire de 8m de diamètre et 0,40m de haut, essentiellement constitué de pierraille et de terre. La végétation, dense au centre, semble cacher une dépression qui pourrait être la trace d'une fouille ancienne ou le reste d'une chambre funéraire dolménique dont les dalles auraient disparu.

Historique

Monument découvert en [1998]{.date} par [A. Martinez Manteca]{.creator}.

### [Urrugne]{.spatial} - [Herboure]{.coverage} - [Monolithe]{.type}

Localisation

Altitude : 230m.

On trouve cette pierre dans un tournant du chemin récemment tracé par l'ONF : prendre la première route goudronnée à gauche, après l'ancienne douane d'Herboure, et ensuite le chemin de terre (barrière), qui démarre, à gauche, de suite après le franchissement d'un petit ruisseau.

Description

Bloc de grés triasique, parallélépipédique, mesurant 2,80m de long et 1,03m dans sa plus grande largeur. Il présente une extrémité arrondie (avec probables traces d'épannelage), d'environ 0,60m de large, l'autre extrémité, rectangulaire n'a que 0,54m de large. L'un des deux côtés les plus longs est naturellement rectiligne, l'autre plus sinueux, ne rompt pas l'harmonie de l'ensemble, mais on note à son niveau des traces très nettes de l'engin mécanisé qui l'a enlevé de son lieu d'origine, peu éloigné toutefois. La patine marron est absente sur une partie de la face supérieure plane de cette pierre : était-elle partiellement enfouie avant son déplacement ?

Historique

Monolithe signalé par [F. Ithurria]{.creator} en [avril 2011]{.date}.

### [Urrugne]{.spatial} - [Ibardineko Erreka]{.coverage} - [Tumuli]{.type} et [tertres d'habitat]{.type}

Ce site a été découvert par [I. Txintxuretta]{.creator} en [avril 2005]{.date}, qui signale 6 tertres d'habitat au lieu dénommé : *Aire Leku Zelaia*... Comme le nom *Aire Leku* est déjà attribué à un autre site, nous l'avons appelé : *Ibardineko erreka*, du fait des ruisseaux de ce nom qui encadrent ce vaste pâturage. Nous l'avons revisité en [décembre 2011]{.date}, et avons noté 9 tertres d'habitat et 9 tumuli. Les dimensions et le nombre de tumuli nous évoquent la nécropole à tumuli d'Olhette, toute proche, que nous avons publié en 2008 [@blotInventaireMonumentsProtohistoriques2009, p. 4].

- *Ibardineko Erreka 1 - Tumulus*

    Altitude : 58m.

    Tumulus de terre et de pierres, sur sol horizontal, mesurant 15m de diamètre et 0,40m de haut, à une quinzaine de mètres à l'Ouest de la piste (I. Txintxuretta : *Tertre d'habitat 1*).

- *Ibardineko Erreka 2 - Tumulus*

    Tumulus de terre et de pierres situé à 15m à l'Est Nord-Est de *T1*, sur sol horizontal. Il mesure 10m de diamètre et 0,30m de haut (I. Txintxuretta : *Tertre d'habitat 2*).

- *Ibardineko Erreka 3 - Tumulus*

    Altitude : 59m.

    Tumulus de terre et quelques pierres visibles, érigé sur sol horizontal, mesurant 10m de diamètre et 0,40m de haut ([Blot J.]{.creator}).

- *Ibardineko Erreka 4 - Tumulus*

    Altitude : 60m.

    Tumulus constitué de terre et de très nombreuses pierres, mesurant 8m de diamètre et 0,30m de haut, sur terrain plat (Blot J.).

- *Ibardineko Erreka 5 - Tumulus*

    Altitude : 60m.

    Tumulus de 12m de diamètre et 0,50m de haut, de terre et de pierres, sur terrain plat (Blot J.)

- *Ibardineko Erreka 6 - Tumulus*

    Situé, sur terrain plat, à 4m à l'Est du précédent, il mesure 12m de diamètre et 0,35m de haut. Constitué de terre et de pierres (Blot J.).

- *Ibardineko Erreka 7 - Tumulus*

    Situé à 12m au Sud de T5, sur terrain plat. Constitué de terre et de pierres, il mesure 12m de diamètre et 0,30m de haut (Blot J.)

- *Ibardineko Erreka 8 - Tumulus (?)*

    Très petit tumulus ovale, bien visible cependant.

    Altitude : 70m.

    Constitué de terre essentiellement, il mesure 4m x 3m et 0,40m de haut (Blot J.).

- *Ibardineko Erreka 9 - Tumulus*

    Altitude : 60m.

    Constitué de terre et de pierres, érigé sur terrain plat. Mesure 8m de diamètre et 0,30m de haut (I. Txintxuretta : *Tertre d'habitat 4*).

    Peut-être y aurait-il un dixième tumulus très discret, à 15m au Nord du *Tertre d'habitat n°9*, mesurant 5m de diamètre et de quelques centimètres de haut (Blot J.).

- *Ibardineko Erreka 1 - Tertre d'habitat*

    Altitude : 58m.

    Situé à 40m au Nord-Ouest et au-dessus d'un ruisseau, et sur terrain en pente.

    Tertre de terre et quelques pierres, mesurant 8 à 9m de diamètre, 0,30m de haut, de forme asymétrique.

- *Ibardineko Erreka 2 - Tertre d'habitat*

    Altitude : 60m.

    Érigé en rupture de pente, au-dessus et au Nord-Ouest du ruisseau, ce tertre, asymétrique, de terres et de nombreuses pierres, mesure 10m de diamètre et 0,50m de haut.

- *Ibardineko Erreka 3 - Tertre d'habitat*

    Situé à une quinzaine de mètres à l'Est Nord-Est du *TH n°2*, sur terrain en pente, il est plus discret, asymétrique, et mesure 9m de diamètre et 0,30m de haut.

- *Ibardineko Erreka 4 - Tertre d'habitat*

    Situé à 5m à l'Est Nord-Est du *TH n°3*, sur terrain en pente, ce tertre asymétrique mesure 8m de diamètre et 0,20m de haut (Blot J.).

- *Ibardineko Erreka 5 - Tertre d'habitat*

    Altitude : 55m.

    Situé à 30m au Nord-Ouest du *TH n° 2*.

    Tertre asymétrique, discret, érigé sur terrain en pente, mesurant 5m de diamètre et 0,30m de haut (Blot J.).

- *Ibardineko Erreka 6 - Tertre d'habitat*

    Altitude : 60m.

    Il est situé à 70m environ au Sud Sud-Ouest de *TH n°2*.

    Asymétrique sur terrain en pente, fait de terre et de pierres nombreuses, il mesure 8m x 8m (Blot J.).

- *Ibardineko Erreka 7 - Tertre d'habitat*

    Altitude : 60m.

    Il est tangent à l'Ouest à la piste et à 20m à l'Ouest du *tumulus n°5*.

    Tertre essentiellement de terre mesurant 10m de diamètre et 0,40m de haut (Blot J.).

- *Ibardineko Erreka 8 - Tertre d'habitat*

    Altitude : 60m.

    Tertre asymétrique de terre et de pierres, mesurant 8m de diamètre et quelques centimètres de haut (Blot J.).

- *Ibardineko Erreka 9 - Tertre d'habitat*

    Altitude : 60m.

    Situé à l'Est Nord-Est du *TH n°8* ; asymétrique, il mesure 8m de diamètre et 0,40m de haut (I. Txintxuretta).

### [Urrugne]{.spatial} - [Ibardin]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 335m.

Il est situé sur un petit replat et tangent, au Nord-Ouest, à une piste qui descend des ventas d'Ibardin, situées au Sud-Ouest.

Description

Tumulus de terre et pierraille, d'environ 8m de diamètre, 0,30m de haut ; on note une très faible dépression centrale, et la périphérie Sud-Est a été entamée par le passage de la piste.

Historique

Monument découvert par [Blot J.]{.creator} en [décembre 2011]{.date}.

### [Urrugne]{.spatial} - [Larrauntiki]{.coverage} 1 - [Tumulus-cromlech]{.type}

Localisation

Carte 1245 Ouest Saint-Jean-de-Luz.

Altitude : 660m.

Urrugne. (Vera de Bidassoa).

Il est situé, ainsi que *Larrauntiki 2 - Tumulus-cromlech* et *Larrauntiki 3 - Tumulus*, dans le col de Zizkouitz, entre Larraun et Larrauntiki, à 10m au Sud Sud-Est de la BF 22, donc en territoire de Vera de Bidassoa.

Description

Petit tumulus aplati de 0,30m de haut et d'un diamètre de 4m, délimité par une quinzaine de pierres dont 11 sont particulièrement visibles dans la moitié Sud. On note une petite dalle dans le secteur Sud-Ouest, à l'intérieur du monument : fait-elle partie d'une ciste ?

Historique

Monument découvert en [septembre 1975]{.date}.

### [Urrugne]{.spatial} - [Larrauntiki]{.coverage} 2 - [Tumulus-cromlech]{.type}

Localisation

Urrugne. (Vera de Bidassoa).

À 8m à l'Ouest Sud-Ouest de la BF 22.

Description

Tumulus aplati de 5m de diamètre et 0,30m de haut. Une quinzaine de pierres bien visibles en marquent la périphérie. On note une légère dépression centrale où apparaissent 4 petites dalles : ciste ?

Historique

Monument découvert en [septembre 1975]{.date}.

### [Urrugne]{.spatial} - [Larrauntiki]{.coverage} 3 - [Tumulus]{.type}

Localisation

Urrugne. (Vera de Bidassoa).

Il est à 60m à l'Ouest Sud-Ouest de *Larrauntiki 2 - Tumulus-cromlech*, au bord Sud de la crête, un peu plus en altitude que les précédents, à l'altitude de 662m.

Description

Tumulus mixte mesurant 7m de diamètre et 0,30m de haut, à prédominance pierreuse, affectant une forme de couronne du fait d'une légère dépression centrale de 2m de diamètre.

Historique

Monument découvert en [septembre 1975]{.date}.

### [Urrugne]{.spatial} - [Lezante]{.coverage} 3 - [Dolmen]{.type}

Localisation

Carte 1245 OT. Hendaye-St Jean-de-Luz.

Altitude : 240m.

Ce monument se trouve à environ 200m à l'Ouest de la station filtre du Lezante, sur un replat herbeux au flanc Nord du massif du Xoldokogagna. C'est à ce niveau que la route aboutit, dans un virage à cette station filtre. On rappellera qu'il a déjà été décrit 2 dolmens à quelques dizaines de mètres au Sud-Ouest de cette station [@chauchatSeptDolmensNouveaux1966 p.110].

Description

On note un très léger tumulus de terre, d'environ 5m de diamètre et 0,30m de haut actuellement. Au centre sont visibles deux dalles de grés rose parallèles, très inclinées, pratiquement couchées au ras du sol, vers le Nord. La dalle la plus au Nord mesure 0,65m de long et 0,20m de large ; elle est distante d'environ 0,70m de la seconde qui mesure 1,17m de long et 0,30m de large. Ces deux dalles sont tout ce qui reste d'une chambre funéraire orientée Ouest-Est. Il semble exister un péristalithe d'une dizaine de pierres autour du tumulus.

Historique

Monument trouvé par [Blot J.]{.creator} en [décembre 2009]{.date}.

### [Urrugne]{.spatial} - [Mandale]{.coverage} 1 à 5 - [Cromlechs]{.type}

Localisation

Carte 1245 Ouest Saint-Jean-de-Luz.

Altitude : 500m.

[J.M. de Barandiaran]{.creator} a publié en [1953]{.date} [@barandiaranHombrePrehistoricoPais1953, p.251] et décrit en 1962 [@barandiaranProspecionesExcavacionesPrehistoricas1962, p.18] sur ce petit replat qui domine à l'Ouest le col d'Ibardin, un groupe de 5 cromlechs : 
- le n°1 : 6m de diamètre et 19 pierres périphériques ; 
- le n°2 : 5m et 13 pierres ; 
- le n°3 : 4m et 10 pierres ; 
- le n°4 : 7m et 11 pierres ; 
- le n°5 : 8m et 4 pierres ; 

Le matériau employé est la quartzite blanche abondante dans l'environnement du site. Pour certains de ces monuments, nous n'avons pas retrouvé, sur le terrain, les mêmes dimensions, ni le même nombre de pierres, ni les mêmes orientations ; enfin il nous a paru possible de proposer deux monuments supplémentaires.

- *Le cromlech n°1,* le plus au Nord, mesure, suivant les axes, entre 5,70m et 6m de diamètre, avec 19 pierres périphériques. Deux d'entre elles sont particulièrement remarquables : l'une, marquée **A** sur le schéma, plantée au Nord-Est, en forme de pain de sucre, mesure 0,70m de haut et 0,50m à sa base ; l'autre, marquée **B**, au Sud, allongée, mesure 1,65m de long et 0,25m d'épaisseur.

- *Le cromlech n°2* est situé à 9m au Sud Sud-Est du précédent. Il mesure 6m de diamètre avec 20 pierres périphériques. On retrouve, en plus de dimensions et d'un nombre de pierres similaires, un autre point commun avec le n°1 : les pierres **A** au Nord-Est et **B** au Sud, ont sensiblement les mêmes positions, formes et dimensions.

- *Le cromlech n°3* est à 4,30m à l'Ouest Sud-Ouest du n°1. Il mesure 3,50 à 4m de diamètre et 7 pierres périphériques, au ras du sol, peu visibles.

- *Le cromlech n°4* est à 21m au Sud-Ouest du n°1. Il mesure 5,50m de diamètre avec 16 pierres périphériques ; la pierre marquée **A**, au Sud-Est, mesure 0,70 à sa base et 0,70m de haut.

- *Le cromlech n°5* est à 2m au Sud du précédent. On ne voit actuellement que 3 pierres qui paraissent décrire un arc de cercle ayant 6,50m de diamètre ; la plus volumineuse, au Nord Nord-Est, marquée **A**, mesure 0,70m à sa base et 0,30m de haut.

### [Urrugne]{.spatial} - [Mandale]{.coverage} 6 - [Cromlech]{.type}

Localisation

À 12m au Sud-Ouest du n°2, et à 3m au Sud Sud-Ouest d'un volumineux conifère, le plus à l'Ouest du groupe d'arbres à cet endroit.

Description

Cercle très discret de 3m de diamètre, délimité par 5 pierres, profondément enfouies dans le sol et dont seuls les sommets sont visibles. Comme au col de Méatsé, dans l'Artzamendi (Itxassou), on peut évoquer des colluvions importantes venant, ici, du sommet de Mandale à l'Ouest et qui ont d'ailleurs pu recouvrir totalement d'autres monuments sur ce site ; une prospection géophysique, comme celle qui a été pratiquée au col de Méatsé, pourrait répondre à la question.

Historique

Monument découvert en [juin 1968]{.date}.

### [Urrugne]{.spatial} - [Mandale]{.coverage} 7 - [Cromlech]{.type}

Localisation

À 4,50m au Sud-Est du n°1.

Description

Cercle de 7,50m de diamètre, délimité par 5 pierres. On note, au Nord, une très volumineuse dalle de 1,50m de long, 1,30m de large et 0,15m d'épaisseur, à grand axe Nord-Ouest Sud-Est, en grande partie recouverte de mousse. Cette dernière dissimule presque aussi en totalité les 4 autres blocs de quartzite, dans la moitié Sud du monument.

Historique

Monument découvert en [juin 1968]{.date}.

### [Urrugne]{.spatial} - [Manuelenborda]{.coverage} - [Tertres d'habitat]{.type}

Localisation

Altitude : 212m.

Ces trois tertres d'habitat (peut-être 4), sont situés dans une fougeraie à gauche de la piste qui monte à Arlépoa.

Description

Ces trois tertres sont érigés sur terrain en pente vers le Nord-Est et présentent la dissymétrie habituelle à ce type de monument.

- *Le tertre n°1*, (le plus bas situé), est à 2m de la piste ; mesure environ 6m x 4m et 0,50m de haut.

- *Le tertre n°2*, à 1m de la piste, est 6m au Sud-Ouest et mesure environ 5m x 3m et 0,40m de haut. À 1m à l'Est se voit un relief discret qui pourrait correspondre à un éventuel 4^ème^ tertre.

- *Le tertre n°3*, tangent à la piste et le plus haut situé est à 12m au Sud-Ouest ; il mesure environ 4m x 3m et 0,40m de haut, paraissant en partie effondré.

Historique

Tertres découverts par [Domeka Ibarzola]{.creator} en [2014]{.date}.

### [Urrugne]{.spatial} - [Mokua]{.coverage} 1 - [Monolithe]{.type}

Localisation

Altitude : 343m.

Il est semble-t-il issu du filon rocheux sur lequel il repose, bien individualisé.

Description

Bloc de grés parallélépipédique irrégulier, mesurant 2,07m de long, couché sur le sol selon un axe Est Nord-Est Ouest Sud-Ouest, présentant une base Ouest Sud-Ouest d'une épaisseur de 0,44m et un sommet Est Nord-Est lui aussi de 0,30m d'épaisseur. Au centre il atteint 0,50m d'épaisseur. Il semblerait que ce monolithe ait subi des traces de régularisation, en particulier sur son bord Nord.

Historique

Monolithe découvert par [I. Txintxurreta]{.creator} en [décembre 2015]{.date}.

### [Urrugne]{.spatial} - [Mokua]{.coverage} 2 - [Monolithe]{.type} (dit aussi «Vierge Marie»)

Localisation

Altitude : 200m.

Ce monolithe se trouve à 6m à 7m au Sud-Ouest d'un petit sanctuaire dédié à la Vierge Marie, et à 6m à l'Ouest de la piste de crête.

Description

Petit bloc de grés couché sur le sol selon un axe Nord Nord-Ouest Sud Sud-Est, de forme parallélépipédique allongée, mesurant 1,60m de long pour une largeur au centre de 0,38 m et une épaisseur de 0,38m.

Sa largeur à son extrémité Nord-Ouest est de 0,18m et son épaisseur de 0,44m.

À son extrémité Sud-Est, sa largeur est de 0,24m et son épaisseur de 0,30m.

Pas de traces d'épannelage visible... Borne pastorale ?

Historique

Monolithe découvert par [I. Txintxurreta]{.creator} en [décembre 2015]{.date}.

### [Urrugne]{.spatial} - [Mokua]{.coverage} 3 - [Monolithe]{.type}

Localisation

Altitude : 344m.

Monolithe couché au sol à 8m au Sud-Est de la piste de crête.

Description

Dalle de grés de forme triangulaire de 1,90m de long dans son plus grand axe Nord-Est Sud-Ouest. Sa base Sud-Ouest mesure 1,55m et présente une épaisseur de variant entre 0,24m et 0,32m. Cette épaisseur semble aller en décroissant vers le sommet. Celui-ci est séparé du monolithe par une cassure de 0,72m de long, survenue après une probable chute... ce qui impliquerait qu'elle ait été plantée. De plus, cette dalle triangulaire paraît avoir été épannelée sur tout son pourtour.

Historique

Monolithe découvert par [F. Meyrat]{.creator} en [janvier 2016]{.date}.

### [Urrugne]{.spatial} - [Mokua]{.coverage} 2 - [Dolmen]{.type}

Localisation

Altitude : 230m.

On le trouve sur un replat de la croupe du flanc Nord du mont Mokua.

Description

Au milieu d'un tumulus de 15m de diamètre environ, de terre et de pierres, et d'une hauteur variant de 1m à l'Est à près de 1,50m à l'Ouest, on note deux dalles verticales de grés triasique, les restes de la chambre funéraire, à grand axe Nord Nord-Est Sud Sud-Ouest. Ses dimensions avoisinaient 2,50m de long et 1,20m de large environ ; il n'en reste que le montant Nord-Ouest, une dalle longue de 1,80m, haute de 0,30m à 0,50m et épaisse d'une dizaine de centimètres.

Au Sud-Ouest, à 1m de distance environ et en oblique par rapport à la précédente, (elle est orientée Nord-Sud), la deuxième dalle, légèrement inclinée vers le Nord-Est, est longue de 1,40m, haute de 0,70m en moyenne, et épaisse de 0,10m environ.

Historique

Monument trouvé par [F. Iturria]{.creator} en [avril 2011]{.date}.

### [Urrugne]{.spatial} - [Mugi]{.coverage} 3 - [Dolmen]{.type}

Localisation

Carte 1245 OT. Hendaye-St Jean-de-Luz.

Altitude : 300 m.

Description

Un tumulus de 6m de diamètre se signale à l'attention par une hauteur de quelques centimètres à peine et aussi par la qualité de l'herbe qui y pousse, d'aspect différent du gazon environnant. Au milieu de ce tumulus de terre et de pierres on peut distinguer une belle dalle de 1,10m de long, 0,30m de large, enfoncée dans le sol et complètement couchée ; une autre dalle, plus petite, de 0,22m de long, elle aussi enfoncée dans le sol, est visible à une quarantaine de centimètres au Nord-Ouest. Ce sont les seuls vestiges visibles d'une chambre funéraire orientée Nord-Est Sud-Ouest.

(Signalons pour mémoire deux dalles épaisses plantées dans le sol ainsi que quelques autres, à courte distance, dont l'interprétation n'est pas aisée)

Historique

Monument découvert par [J. Blot]{.creator} en [janvier 2010]{.date}.

### [Urrugne]{.spatial} - [Muxugorrigaine]{.coverage} Nord - [Pierre couchée]{.type} [(?)]{.douteux}

Localisation

Altitude : 269m.

Description

Bloc de grés de forme triangulaire, allongé selon un axe Nord-Ouest Sud-Est, à sommet Nord-Ouest. Il atteint 1,82m de long, et de 0,60m à 0,70m de large à sa base.

Son épaisseur moyenne est de 0,30m. On note près de son sommet, côté Ouest, des traces d'épannelage (?). Il n'y a pas d'autres pierres dans son environnement immédiat. Ce bloc est remarquable lui aussi, comme *Muxugorrigaine Sud - Dalle couchée (?)*, par sa position et ses caractéristiques.

Historique

Monument découvert par [J. Blot]{.creator} en [janvier 2010]{.date}.

### [Urrugne]{.spatial} - [Muxugorrigaine]{.coverage} Nord 2 - [Tumulus]{.type}

Localisation

Carte 1245 OT. Hendaye-St Jean-de-Luz.

Altitude : 270m.

Il est situé à une centaine de mètres au Sud-Est du *tumulus Muxugorrigaine Nord 1*, et il est tangent, à l'Ouest, à une piste pastorale qui s'est détachée, à quelques dizaines de mètres en amont, de celle qui mène de *Muxugorrigaine Sud* à *Muxugorrigaine Nord 1*.

Description

Sur un terrain en très légère pente, on note un tumulus pierreux de 5m de diamètre et une vingtaine de centimètres de haut. Quelques pierres au centre, sont plus volumineuses que les autres et font penser à un éventuel remaniement du tumulus (petit abri pastoral ?) plus qu'à un tas d'épierrage, inattendu dans le contexte environnant.

Historique

Monument découvert par [J. Blot]{.creator} en [janvier 2010]{.date}.

### [Urrugne]{.spatial} - [Muxugorrigaine]{.coverage} Sud - [Dalle couchée]{.type} [(?)]{.douteux}

Localisation

Altitude : 276m.

Elle est tangente à la piste pastorale et à 9m au Sud-Ouest du *Tumulus-cromlech Muxugorrigaine Sud*.

Description

Dalle de grés allongée sur le sol, selon un axe Nord-Sud, en forme de pain de sucre à sommet Sud. Elle mesure 1,45m de long, 0,50m à sa base et 0,33m à son sommet ; son épaisseur est de 0,17m en moyenne. Elle présente des traces évidentes d'épannelage sur tout son pourtour visible ; si elle n'a pas les dimensions habituelles des monolithes, elle est cependant remarquable par ses caractéristiques de localisation et de sa régularisation.

Historique

Dalle découverte par [I. Gaztelu]{.creator} en [novembre 1987]{.date}.

### [Urrugne]{.spatial} - [Mugi]{.coverage} - [Cromlech]{.type}

Localisation

Altitude : 319m.

Ce monument est situé sur un petit replat de ce terrain en pente générale vers le Nord.

Description

Une dizaine de pierres, peu visibles, au ras du sol et de taille variable, délimitent un cercle de 2,80m de diamètre.

Historique

Cercle découvert par [I. Txintxurreta]{.creator} en [décembre 2015]{.date}.

### [Urrugne]{.spatial} - [Mugi]{.coverage} 1 - [Tumulus]{.type}

Localisation

Altitude : 314m.

Ce tumulus se trouve à 30m au Nord de *Mugi - Cromlech*, et à 40m au Sud de *Mugi 4 - Dolmen*.

Description

Tumulus terreux, circulaire, en forme de galette aplatie, de 6m de diamètre et 0,30m de haut.

Historique

Monument découvert par [Blot J.]{.creator} en [janvier 2016]{.date}.

### [Urrugne]{.spatial} - [Mugi]{.coverage} 4 - [Dolmen]{.type}

Localisation

Altitude : 301m.

Ce monument est situé à 40m au Sud de *Mugi 1 - Tumulus*, sur un léger replat, et bordé sur son flanc Ouest par une petite ravine qui lui a entamé une faible partie de son tumulus.

Description

Tumulus circulaire mixte, de terre et de pierres, en forme de galette aplatie, mesurant 10m de diamètre et 0,40m de haut environ. Au centre sont bien visibles, quoiqu'au ras du sol, 3 dalles ou fragment de dalles, profondément enfouies, orientées Est-Ouest, et faisant partie de la chambre funéraire, dont les parois se sont inclinées vers le Nord.

Le fragment de dalle le plus au Nord mesure 0,60m de long ; la seconde dalle, qui lui est parallèle, mesure 0,90m de long, et la troisième, elle aussi parallèle à la précédente, mesure 0,73m de long ; leur épaisseur à toutes trois est de 0,08m en moyenne.

Historique

Monument découvert par [I. Txinturreta]{.creator} en [décembre 2015]{.date}.

### [Urrugne]{.spatial} - [Mugi]{.coverage} 2 - [Tumulus]{.type}

Localisation

Altitude : 306m.

Il est situé à 10m à l'Ouest de *Mugi 4 - Dolmen* et il est lui aussi longé sur son flanc Ouest par une autre petite ravine.

Description

Tumulus pierreux de 4,50m de diamètre et 0,40m de haut ; de nombreux blocs de pierre de taille variable sont visibles en surface, sans aucun ordre apparent.

Historique

Monument découvert par [Blot J.]{.creator} en [janvier 2016]{.date}.

### [Urrugne]{.spatial} - [Munhoa]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 340m.

Il se trouve au pied du mont Munhoa, sur un replat dominant un petit col, à 4m à l'Ouest des ruines d'une bergerie recouverte d'un abondant roncier.

Description

Tumulus essentiellement pierreux, fait de petits fragments de dalles ou d'éléments nettement plus volumineux, le tout particulièrement bien visible dans la moitié Est.

Ce tumulus mesure 4m de diamètre, et a 0,40m de haut.

Historique

Tumulus découvert par [Duvert M.]{.creator} en [décembre 2011]{.date}.

### [Urrugne]{.spatial} - [Oneaga]{.coverage} 2 - [Tumulus]{.type}

Localisation

Altitude : 334m.

Ce tumulus est au Nord-Est d'un enclos à bétail métallique.

Description

Tumulus mixte de terre et de pierres, de forme conique, mesurant 5,90m de diamètre et 0,45m de haut. Était dans les années passées entièrement recouvert d'ajoncs.

Historique

Tumulus découvert par [Meyrat F.]{.creator} en [janvier 2016]{.date}. Nous rappelons qu'il existe un tumulus (*Onéaga T1*), découvert par nous en 2009 et publié dans [@blotInventaireMonumentsProtohistoriques2010].

### [Urrugne]{.spatial} - [Oneaga]{.coverage} - [Ciste]{.type}

Localisation

Altitude : 334m.

Description

Ciste rectangulaire à grand axe orienté Nord-Ouest Sud-Est, délimitée par deux dalles enfouies dans le sol dont seuls les sommets affleurent de quelques millimètres au-dessus de la surface. La dalle Sud-Ouest mesure 0,60m de long et est séparée de 0,48m de la dalle Nord-Est qui lui est parallèle et qui mesure 0,54m de long. Une troisième portion de dalle est visible, 0,18m dans l'angle Sud qui complète ainsi cette chambre funéraire. Il n'est pas possible de dire, dans l'état actuel, s'il s'agit de la ciste d'un cromlech, d'un tumulus plus ou moins arasé, ou si elle est isolée...

Historique

Ciste découverte par le [groupe Hilhariak]{.creator} en [décembre 2015]{.date}.

### [Urrugne]{.spatial} - [Oneaga]{.coverage} (ou Gainxobordakolepoa) - [Cromlechs]{.type}

Localisation

Dans le petit col, au Sud-Est du mont Oneaga, où passe la piste qui se rend au dolmen du même nom.

Altitude : 300m.

Description

Ces 5 cromlechs sont très difficiles à voir et seul *le n°5* est absolument certain.

- *Cromlech n°1* : Trois chênes sont alignés selon un axe Nord-Sud sur la gauche de la piste quittant le chemin empierré et traversant le gazon du col (l'arbre du milieu est mort et en partie abattu). Le cromlech se trouve immédiatement au Nord du 3^ème^ arbre, ^soit^ à environ 30m de la naissance de la piste. Cercle de 8m de diamètre, délimité par une dizaine de pierres, au ras du sol. Une pierre est visible au centre.

- *Cromlech n°2* : Il est pratiquement tangent, au Nord du précédent. Cercle de 6 m de diamètre, délimité par une quinzaine de pierres, toujours au ras du sol.

- *Cromlech n°3* : Situé à 1m du précédent. Cercle de 3,50m de diamètre, délimité par une dizaine de pierres au ras du sol.

- *Cromlech n°4* : Situé à 3m au Nord du n°2. Un cercle de 5,20m de diamètre serait délimité par environ 8 pierres, toujours au ras du sol.

- *Cromlech n°5* : On le trouve à l'Est de la piste se rendant au dolmen ; il est à environ 30m à l'Ouest du *cromlech n°2*.

S'il n'y a que 5 pierres pour délimiter un cercle de 6,50m de diamètre, au moins sont-elles bien visibles : tout d'abord, dans le secteur Nord, on note 3 dalles dépassant légèrement la surface du gazon. Les deux plus longues mesurent 0,76m de long et sont séparées par une troisième plus petite de 0,18m de long ; la quatrième dalle, de 0,40m de long, se trouve en secteur Est, et la cinquième, au Sud.

Nous n'avons pas noté de sixième cromlech, quoique qu'il nous ait été signalé... de même qu'un tumulus.

Historique

Les *cromlechs 1*, *2*, *3*, *4* ont été trouvés par [L. Millan]{.creator} ; le *5* par [A. Martinez]{.creator} ; le *6* par A. Martinez et L. Millan - [juillet 1997]{.date}.

### [Urrugne]{.spatial} - [Oneaga]{.coverage} 6 - [Cromlech]{.type}

Localisation

Carte 1245 OT. Hendaye-St Jean-de-Luz.

Altitude : 330m.

Il se trouve à 6m au Nord du *dolmen Oneaga 1* que nous avons décrit en 1972 [@blotNouveauxVestigesMegalithiques1972c p. 12].

Description

Petit cercle de 3m de diamètre, délimité par 4 pierres profondément enfoncées dans le sol, dont deux atteignent 0,75 m de long. Quatre autres pierres complètent le cercle mais leur mobilité les rend douteuses.

Historique

Monument découvert par [J. Blot]{.creator} en [1971]{.date} et confirmé en [2010]{.date}.

### [Urrugne]{.spatial} - [Oneaga]{.coverage} 7 - [Cromlech]{.type}

Localisation

Carte 1245 OT. Hendaye-St Jean-de-Luz.

Altitude : 330m.

Il est situé à l'extrémité Sud-Est du sommet de la première petite colline au Sud-Est du mont Oneaga, et qui délimite avec lui le col du même nom.

Description

On note un cercle délimité par plus d'une vingtaine de pierres, bien visibles, et en plus grand nombre dans le quart Sud-Est ; il existe une légère dépression centrale.

Historique

Monument découvert par [J. Blot]{.creator} en [février 2010]{.date}.

### [Urrugne]{.spatial} - [Oneaga]{.coverage} - [Tumulus]{.type}

Localisation

Carte 1245 OT. Hendaye-St Jean-de-Luz.

Altitude : 350m.

On le trouve à l'extrémité Est du sommet de la petite colline (cote 351), située juste à l'Est du mont Oneaga.

Description

Tumulus constitué de terres et de pierres, mesurant environ 5m de diamètre et 1m de haut.

Historique

Monument découvert par [J. Blot]{.creator} en [février 2010]{.date}.

### [Urrugne]{.spatial} - [Oneaga]{.coverage} 2 - [Dolmen]{.type}

Localisation

Carte 1245 OT. Hendaye-St Jean-de-Luz.

Altitude : 382m.

Ce dolmen se trouve exactement au point le plus élevé du mont Oneaga, avec une vue admirable à 360° sur les lointains.

Description

On note un tumulus de 8m de diamètre, bien délimité, d'environ une trentaine de centimètres de haut, couvert d'une herbe fine. Quelques rares pierres apparaissent par endroits.

Au centre se trouve une grande dalle, de forme grossièrement triangulaire, dont la base Sud enfoncée dans le sol mesure 1,25m de long ; sa largeur (sa hauteur quand elle était verticale) atteint 1,25m et son épaisseur *apparente* est de 0,10m. C'est tout ce qui reste de la chambre funéraire, orientée probablement Est-Ouest.

Historique

Monument découvert par [J. Blot]{.creator} en [janvier 2010]{.date}.

### [Urrugne]{.spatial} - [Oneaga]{.coverage} 3 - [Dolmen]{.type}

Localisation

Altitude : 300m.

Il se trouve sur le flanc Nord Nord-Est du mont Onéaga.

Monument assez difficile à trouver, au milieu d'une abondante végétation.

Description

On remarque 5 dalles qui émergent du sol, suivant un axe Nord-Sud, et dont l'épaisseur moyenne est de 0,20m à 0,25m. On peut considérer que la paroi Est du monument est formée par un alignement de 4 dalles. La plus au Nord mesure 0,60m de haut et 0,45m à sa base ; la suivante, en allant vers le Sud, mesure 1m de haut et 0,45m à sa base ; la troisième : 0,35m de haut et 0,60m à sa base ; enfin la dernière mesure 0,37m de haut et 0,45m à sa base. De la paroi Ouest, il ne subsiste qu'une dalle de 0,25m de haut et de 0,45m à la base.

Historique

Ce monument nous a été signalé par [F. Iturria]{.creator} (ONF), en [septembre 2010]{.date}.

### [Urrugne]{.spatial} - [Subisia]{.coverage} - [Tumulus]{.type}

Localisation

Carte 1245 Ouest Saint-Jean-de-Luz.

Altitude : 550m.

Il est situé à une vingtaine de mètres au Sud du petit col, au pied de Soubisia, en bordure de la piste pastorale.

Description

Tumulus mixte de terre et de pierres de 6m de diamètre et 0,30m de haut ; monument douteux ?

Historique

Monument découvert en [octobre 1969]{.date}.

### [Urrugne]{.spatial} - [Urbisi]{.coverage} 3 - [Dolmen]{.type} [(?)]{.douteux}

Localisation

Carte 1245 OT. Hendaye-St Jean-de-Luz.

Altitude : 352 m.

On le trouve à l'extrémité d'un petit replat qui domine l'horizon, à l'Ouest de la principale piste pastorale qui gravit le mont Xoldokogagna en venant de la station filtre du Lezante.

Description

Au milieu d'un probable tumulus pierreux peu appréciable, on note un ensemble de 3 dalles de grés rose. Une première, presque rectangulaire, horizontale, mesure 0,90m x 0,80m, la seconde, à l'Ouest, mesure 0,90m de long et 0,45m de large. La troisième, au Nord, mesure 1,40m de long et 0,50m de large. Dolmen douteux.

Historique

Monument découvert par [A. Martinez]{.creator} en [2009]{.date}.

- Rappelons que le *dolmen Urbisi 1* a été publié en 1966 [@chauchatSeptDolmensNouveaux1966 p.108].

    Altitude : 370m.

### [Urrugne]{.spatial} - [Urbisi]{.coverage} 2 - [Dolmen]{.type}

Localisation

Carte 1245 OT. Hendaye-St Jean-de-Luz.

Altitude : 432m.

Il est situé à une dizaine de mètres à l'Est de la piste de transhumance qui monte du Lezante, sur un terrain en légère pente vers le Nord, mais il semble que ce soit un glissement de terrain venu du Sud qui donne cette impression.

Description

Il ne reste que 2 dalles plantées dans le sol, qui devaient délimiter une chambre funéraire mesurant environ 1,80m de long et 0,80 de large, orientée Est Sud-Est, Ouest Nord-Ouest.

La dalle Nord mesure 1,44m de long, 0,76m de haut et 0,12m d'épaisseur. Elle présente des signes d'épannelage très net à sa partie supérieure. La dalle Est, qui lui est perpendiculaire, en est distante de 0,50m ; elle mesure 0,38m de large et 0,68m de haut. On ne note pas de relief tumulaire.

Historique

Monument découvert par [J. Blot]{.creator} en [1976]{.date}.

### [Urrugne]{.spatial} - [Urbisi]{.coverage} - [Monolithe]{.type}

Localisation

Carte 1245 OT. Hendaye-St Jean-de-Luz.

Altitude : 447m.

Il se trouve à une dizaine de mètres à l'Ouest de la piste pastorale qui monte du Lezante, sur un léger replat de terrain.

Description

Il se présente sous la forme d'une belle dalle de grés gris, couchée au sol, orientée Nord-Est Sud-Ouest, dont l'extrémité Nord-Est, taillée en pointe, présente des traces d'épannelage ; sa base est rectangulaire. Il mesure 3m de long, 1,40m de large et 0,40m d'épaisseur en moyenne. Il n'y a pas d'autres pierres visibles dans les alentours.

Historique

Monolithe découvert par [J. Blot]{.creator} en [janvier 2010]{.date}.

### [Urrugne]{.spatial} - [Usatuita]{.coverage} 2 - [Dolmen]{.type}

Localisation

Carte 1245 Ouest Saint-Jean-de-Luz.

Altitude : 260m.

Le *dolmen n°1* a été publié en 1966 [@chauchatSeptDolmensNouveaux1966 p.10].

Le n°2 est à environ 9m au Sud du n°1 et situé, comme le précédent en bordure de la piste pastorale, sur un terrain en très légère pente vers le Nord, ce qui donne l'impression qu'il est plus épais dans sa moitié Sud.

Description

Tumulus pierreux ovale de 7m x 6m de diamètre et 0,60m de haut dans lequel une grande dalle de grès rose, inclinée vers le Nord, forme la limite Sud de ce qui reste de la chambre funéraire que l'on pourrait estimer avoir eu 2,50m de long, 1,70m de large, et qui orientée Est-Ouest semble-t-il.

Historique

Dolmen découvert en [janvier 1970]{.date}.

NB : à 3m au Sud Sud-Est du n°1, on peut distinguer, à jour frisant, une sorte de bourrelet circulaire qui pourrait être le vestige d'un tumulus terreux de 6m de diamètre environ ; au centre, une dalle de grès rose de 0,50m de long et 0,70m de haut pourrait être... une simple borne, ou l'unique témoin des montants d'une chambre funéraire disparue.

### [Urrugne]{.spatial} - [Urbisi]{.coverage} 4 - [Dolmen]{.type}

Localisation

Altitude : 370m.

Sur un replat important, au milieu de la montée qui, du Lezante, va au mont Xoldokogagna. Il est situé à environ 100m au Nord-Est d'une grande bergerie détruite.

Description

On note un tumulus circulaire de faible hauteur fait de petites pierres et de dallettes, mesurant environ 5m de diamètre, érigé sur un terrain en très légère pente vers le Nord-Est. La chambre funéraire qui mesurerait probablement 1,40m de long et 0,90m de large est marquée par une légère dépression orientée Nord Nord-Est Ouest Sud-Ouest, dont seule demeure, à sa partie Nord-Est, une petite dalle verticale à sommet arrondi.

Historique

Monument découvert par [J. Blot]{.creator} en [mars 2011]{.date}.

### [Urrugne]{.spatial} - [Urbisi]{.coverage} 2 - [Monolithe]{.type}

Localisation

Altitude : 460m.

Il est situé à gauche de la piste qui monte du Lezante.

Description

Grande dalle de grés triasique gisant sur un sol en pente douce vers le Nord-Est, de forme grossièrement parallélépipédique ; elle mesure 4,43m dans son plus grand axe (orienté Est-Ouest), 2m dans sa plus grande largeur et d'une épaisseur apparente, suivant les endroits, variant de 0,10 à 0,15 m. Des traces d'épannelage apparaissent en de nombreux endroits du bord Nord, et sur la partie Est du bord Sud.

Historique

Monument découvert par [P. Badiola]{.creator} en [février 2011]{.date}.

### [Urrugne]{.spatial} - [Urbisi]{.coverage} - [Cromlech]{.type} [(?)]{.douteux}

Localisation

Altitude : 370m.

Sur un grand replat de la colline d'Urbisi, et à 100m environ au Nord-Est des ruines d'un grand cayolar ; il est aussi à 5m au Sud-Ouest de *Urbisi 4 - Dolmen*.

Description

Une trentaine de dalles et de pierres plus ou moins grandes, souvent très modestes, délimitent un cercle de 12m de diamètre ; les dalles les plus visibles, plus ou moins verticales, se trouvent dans le secteur Sud-Est du cercle bien que dans un certain désordre ; les autres, au ras du sol, ont nécessité un décapage énergique pour être visibles...

Historique

Monument découvert par [I. Txintxuretta]{.creator} en [novembre 2011]{.date}.

### [Urrugne]{.spatial} - [Urbisi]{.coverage} 5 - [Dalle couchée]{.type} [(?)]{.douteux}

Localisation

Altitude : 472m.

Quand le sentier venant du Lezante atteint son sommet, continuer encore 200m environ, puis aller en direction Sud pour trouver cette dalle à une quarantaine de mètres.

Description

Dalle de [grés]{.subject} [triasique]{.subject}, gisant au sol, de forme losangique, mesurant 3,47m dans son plus grand axe, orienté Nord-Est Sud-Ouest, et 1,10m de large. Lorsque nous avons voulu apprécier son épaisseur, nous avons constaté (F. Meyrat) qu'une grande partie du bord Nord-Ouest ne faisait qu'un avec une grande dalle sous-jacente. On note tout le long du bord libre, au Sud-Est, des traces de taille, et semble-t-il aussi des traces de barre à mine au Nord ; il semble enfin que la dalle qui gît à quelques centimètres au Nord-Ouest (1,45m x 0,90m) soit un éclat enlevé à ce même «monolithe». Au vu de l'ensemble de ces constatations, nous éliminons cette importante masse de grés de la catégorie des « monolithes ».

Historique

Dalle découverte par [A. Martinez]{.creator} en [avril 2011]{.date}.

### [Urrugne]{.spatial} - [Urbisi]{.coverage} 5 - [Dolmen]{.type}

Localisation

Altitude : 390m.

Il est sur ce vaste plateau où se trouvent déjà *Urbisi 2 - Dolmen*... La grande borde en ruine est à une centaine de mètres au Nord Nord-Est, et la piste qui monte vers le sommet au Sud se trouve à 35m à l'Est.

Description

Érigé sur un terrain en légère pente vers le Nord, ce monument, sans tumulus visible, ne se distingue que par sa chambre funéraire, orientée Nord-Sud, délimitée par une dalle à l'Ouest, mesurant 1,30m de long, épaisse de 0,14m et haute de 0,18m. Une deuxième dalle, à l'Est, séparée de la précédente de 0,40m à 0,60m représente l'autre élément de cette chambre ; elle mesure 0,70m de long, 0,12m d'épaisseur et 0,14m à 0,22m de haut. Deux autres dalles posées à proximité immédiate, au Nord et au Sud, pourraient avoir fait partie de cette chambre. Compte tenu de ces dimensions, le terme de *ciste* nous paraît plus approprié que celui de dolmen !

Historique

Monument découvert par [Badiola P.]{.creator} en [mai 2012]{.date}.

### [Urrugne]{.spatial} - [Urbisi]{.coverage} 6 - [Dolmen]{.type} [(?)]{.douteux}

Localisation

Altitude : 455m.

Il se trouve, comme tous les monuments suivants, au milieu d'une abondante végétation d'ajoncs, qui a nécessité un gros travail de dégagement (F. Meyrat) pour étude. (Compte tenu de leur aspect, il serait plus approprié d'appeler ces monuments cistes que dolmens...).

Description

Dolmen (?) érigé sur un terrain en pente légère vers l'Est, au milieu d'un tumulus mixte de terre et de pierres de 6m de diamètre, peu marqué ; on voit la chambre funéraire, à grand axe Sud-Est Nord-Ouest, qui mesurerait 2m x 1,10m, délimitée au Sud-Est par une dalle plantée de 0,74m de haut et 0,85m de long ; au Nord et à l'Est par 3 autres dalles couchées au sol. Monument douteux.

Historique

Monument découvert par [D. Ibarloza]{.creator} en [mai 2012]{.date}.

### [Urrugne]{.spatial} - [Urbisi]{.coverage} 7 - [Dolmen]{.type}

Localisation

Il est à 12m au Sud Sud-Ouest de *Urbisi 6 - Dolmen (?)*.

Description

Tumulus mixte peu marqué, de terre et de pierres, de 6m de diamètre et 0,20m de haut, érigé sur terrain en légère pente vers l'Est. La chambre funéraire, à grand axe Est-Ouest, mesure 2,20m x 1m. Elle est délimitée par 2 dalles verticales au Sud, séparées d'une cinquantaine de centimètres ; leur longueur varie entre 0,88m et 0,59m pour une hauteur de 0,50m ; au Nord : 3 dalles verticales dont l'une atteint 0,74m de long et 0,39m de haut.

Historique

Monument découvert par [D. Ibarloza]{.creator} en [mai 2012]{.date}.

### [Urrugne]{.spatial} - [Urbisi]{.coverage} 8 - [Dolmen]{.type} [(?)]{.douteux}

Description

Pas de tumulus visible. Six dalles délimiteraient une chambre funéraire de 2,20m x 1,10m à grand axe Ouest Nord-Ouest Est Sud-Est, dont quatre couchées au sol et deux dalles verticales : l'une au Sud-Est, l'autre au Nord-Ouest. Monument douteux.

Historique

Monument découvert par [D. Ibarloza]{.creator} en [mai 2012]{.date}.

### [Urrugne]{.spatial} - [Urbisi]{.coverage} 9 - [Dolmen]{.type}

Localisation

Il est à 7m au Sud Sud-Ouest de *Urbisi 7 - Dolmen*.

Description

Tumulus de 6m de diamètre de faible hauteur, érigé sur un terrain en légère pente vers l'Est. La chambre funéraire, à grand axe Est-Ouest, mesure 2,40m x 1,40m. Elle est délimitée par plusieurs dalles dont 5 plantées plus ou moins verticalement, qui se voient au Sud, au Nord, et à l'Est. Cette dernière de 0,47m de long et 0,86m de haut.

Historique

Monument découvert par [D. Ibarloza]{.creator} en [mai 2012]{.date}.

### [Urrugne]{.spatial} - [Urbisi]{.coverage} 10 - [Dolmen]{.type}

Localisation

Altitude : 450m.

Il est à 20m au Sud Sud-Est de *Urbisi 8 - Dolmen (?)*.

Description

Sur un terrain en légère pente vers l'Est est érigé un tumulus de 6m de diamètre environ, de faible hauteur (entre 0,20m et 0,40m) ; une quinzaine de dalles délimitent une chambre funéraire de 2m x 0,80m à grand axe Est-Ouest. Une dizaine de celles-ci sont plantées, plus ou moins verticales, dont une, au Sud mesure 0,97m, 0,47 de haut et 0,11m d'épaisseur. La paroi Est est marquée par plusieurs dalles plantées, et proches les unes des autres.

Historique

Monument découvert par [D. Ibarloza]{.creator} en [mai 2012]{.date}.

### [Urrugne]{.spatial} - [Urbisi]{.coverage} 11 - [Dolmen]{.type}

Localisation

On le voit à 3m au Nord-Ouest de *Urbisi 8 - Dolmen (?)*.

Description

Sur un terrain en légère pente vers l'Est (et sans tumulus apparent), 4 dalles plantées dans le sol délimitent une chambre funéraire de 1,90m x 1,10m environ, à grand axe Nord-Sud. La plus grande dalle, au Nord, mesure 1,47m de long, 0,22m de haut et 0,14m d'épaisseur.

Historique

Monument découvert par [Meyrat F.]{.creator} en [juillet 2012]{.date}.

### [Urrugne]{.spatial} - [Urbisi]{.coverage} 12 et 13 - [Dolmens]{.type}

Localisation

Ils sont à 35m à l'Ouest Sud-Ouest de *Urbisi 10 - Dolmen* et à 8m au Sud-Est de *Urbisi 14 - Dolmen*.

Description

Nous décrivons ensemble ces deux monument, tangents, qu'on aurait pu prendre pour un monument à double chambre funéraire. En fait elles sont distinctes.

- *Le dolmen D 12*, le plus au Nord, possède une chambre funéraire rectangulaire, bien visible sur trois de ses côtés mesurant 2,50m x 1,30m, à grand axe Sud-Est Nord-Ouest, délimitée par 8 dalles plantées verticalement, de longueurs et hauteurs variables (de 0,88m à 0,57m de long et atteignant au maximum 0,34m de haut).

- *Le dolmen D 13*, tangent au Sud au précédent, présente une chambre funéraire moins bien conservée, moins géométrique, mesurant environ 2m x 1m, à grand axe Sud-Est Nord-Ouest ; elle est délimitée par 6 dalles plus ou moins inclinées, l'une d'elles, au Sud, atteignant 0,54m de haut.

Historique

Monuments découverts par [Meyrat F.]{.creator} en [juillet 2012]{.date}.

### [Urrugne]{.spatial} - [Urbisi]{.coverage} 14 - [Dolmen]{.type}

Localisation

Altitude : 460m

Le monument est à une vingtaine de mètres à l'Est du chemin de crête et à 8m au Sud-Est de *Urbisi 12 et 13 - Dolmens*.

Description

Sur un tumulus de 4m environ de diamètre avec de nombreuses pierres (au Sud), on voit que 4 dalles plantées délimitent essentiellement une chambre funéraire de 2,10m x 0,90m à grand axe Nord-Est Sud-Ouest. Les deux dalles les plus notables mesurent respectivement : l'une, 0,86m de long, 0,11m d'épaisseur et 0,22m de haut ; l'autre : 0,58m de long, 0,10m d'épaisseur et 0,40m de haut. La limite Est serait marquée par une dalle verticale de 0,49m de long, 0,11m de large et 0,11m de haut.

Historique

Monument découvert par [Badiola P.]{.creator} en [mai 2012]{.date}.

### [Urrugne]{.spatial} - [Urbisi]{.coverage} 3 - [Monolithe]{.type}

Localisation

Altitude : 465m.

Ce monolithe est situé à environ 30m au Sud Sud-Est de *Urbisi 5 - Dalle couchée (?)*, à l'amorce d'une pente assez marquée qui descend vers le Sud-Est.

Description

Grande dalle de grés triasique, de forme grossièrement rectangulaire, à grand axe orienté Sud-Est Nord-Ouest ; elle mesure 5,50m de long, 3m de large et 0,35m à 0,40m d'épaisseur. Elle présente de nombreuses traces d'épannelage, en particulier à son extrémité arrondie Sud-Est.

Historique

Dalle découverte par [F. Meyrat]{.creator} en [avril 2011]{.date}.

### [Urrugne]{.spatial} - [Urbisi]{.coverage} 6 - [Pierre couchée]{.type} [(?)]{.douteux}

Localisation

Altitude : 470m.

Cette pierre gît sur un sol en pente douce vers le Nord-Est, à environ 30m à gauche de la piste ascendante.

Description

Elle affecte la forme d'un parallélépipède rectangle à grand axe Est Nord-Est Ouest Sud-Ouest, et mesure 1,70m de long, 0,85m de large et son extrémité Ouest est plus étroite (0,33m) ; son épaisseur moyenne est d'environ 0,40m. Pas de traces d'épannelage visibles semble-t-il.

Historique

Pierre découverte par [P. Badiola]{.creator} en [février 2011]{.date}.

### [Urrugne]{.spatial} - [Urbisi]{.coverage} 7 - [Pierre plantée]{.type}

Localisation

Altitude : 390m.

Pierre plantée isolée, à flanc de colline en pente vers le Nord.

Description

Pierre plantée verticalement, mesurant 1,03m de haut, 0,80m à sa base, et 0,34m d'épaisseur. Pas de trace d'épannelage visible. On distingue très nettement une structure de soutient faite de pierres entassées à la base de cette dalle, côté Nord.

Historique

Pierre découverte par [I. Txintxuretta]{.creator} en [novembre 2011]{.date}.

### [Urrugne]{.spatial} - [Urbisi]{.coverage} 8 - [Pierre plantée]{.type}

Localisation

Altitude : 365m.

Dalle plantée comme *Urbisi 7 - Pierre plantée*, au flanc Nord de la colline, sur terrain en pente.

Description

Dalle de grès, rectangulaire, de 0,90m de haut, 0,50m à sa base et 0,20m d'épaisseur. Elle est inclinée vers le Nord et deux pierres de soutient y ont été enfouies à sa base.

Historique

Pierre découverte par [I. Txintxuretta]{.creator} en [novembre 2011]{.date}.

### [Urrugne]{.spatial} - [Urbisiko erreka]{.coverage} - [Ciste]{.type}

Localisation

Altitude : 256m.

Description

Cette très modeste ciste est érigée sur un terrain en pente vers l'Ouest. Trois dalles limitent une chambre funéraire, 1m de long et 0,60m de large, orientée selon un axe Nord Nord-Ouest Sud Sud-Est. La dalle au Sud-Est mesure 0,47m à sa base, 0,27m de haut et 0,19m d'épaisseur ; celle au Sud-Ouest mesure 0,90m de long, et celle au Nord-Est, 0,40m.

Historique

Monument découvert par [I. Txintxuretta]{.creator} en [novembre 2011]{.date}.

### [Urrugne]{.spatial} - [Urbisiko erreka]{.coverage} - [Cromlech]{.type}

Localisation

Altitude : 270m.

Situé sur un replat de la dimension du monument.

Description

Entre 15 et 20 pierres, au ras du sol (très plat), délimitent un cercle de 8m de diamètre.

Historique

Monument découvert par [Meyrat F.]{.creator} en [janvier 2011]{.date}.

### [Urrugne]{.spatial} - [Urbisiko erreka]{.coverage} 1 - [Dolmen]{.type}

Localisation

Altitude : 300m.

Description

Tumulus de terre et de pierres de 6m de diamètre et 0,30m de haut. Au centre, une chambre funéraire d'environ 1m de long et 0,60m de large, orientée, semble-t-il, Nord-Sud et essentiellement délimitée au Nord par une dalle de 0,60m x 0,30m et à l'Ouest, par une autre de 0,6m x 0,15m et 9 centimètres de haut. Trois autres petites dalles délimitent le côté Est.

Historique

Monument découvert par [I. Txintxuretta]{.creator} en [novembre 2011]{.date}.

### [Urrugne]{.spatial} - [Urbisiko erreka]{.coverage} 2 - [Dolmen]{.type}

Localisation

Altitude : 300m.

Il se trouve à une quarantaine de mètres au Nord-Ouest de *Urbisiko erreka 1 - Dolmen*.

Description

Tumulus essentiellement pierreux de 8m de diamètre, et d'une hauteur variant entre 0,30m et 1m. Au centre, une chambre funéraire de 1m de long et 0,70m de large, orientée Nord-Ouest Sud-Est, et délimitée par 5 dalles de taille variable. Au Sud-Ouest, une dalle de 0,50m de long et 0,13m d'épaisseur, au ras du sol ; au Sud-Est, une autre dalle de 0,40m de long, 0,10m d'épaisseur et 0,58m de haut ; trois autres dalles au Nord-Ouest et au Nord-Est, dont une de 0,40m de hauteur, complètent cette chambre.

Historique

Monument découvert par [I. Txintxuretta]{.creator} en [novembre 2011]{.date}.

### [Urrugne]{.spatial} - [Urbisiko erreka]{.coverage} 3 - [Dolmen]{.type}

Localisation

Altitude : 241m.

On trouve ce monument sur une petite éminence, à gauche du chemin qui va d'Usatuita au Lézante.

Description

Tumulus de 12m de diamètre, de terre et de pierres, érigé sur un mouvement naturel de terrain, en pente vers le Nord, ce qui lui donne une hauteur plus importante au Nord (1,50m) qu'au Sud (0,40m). Il semble qu'il existe un péristalithe d'une douzaine de pierres.

Au centre du tumulus reste de la chambre funéraire une grande dalle de grés local, orientée selon un axe Ouest Nord-Ouest Est Sud-Est, et inclinée vers le Nord, mesurant 2,20m de long, 0,90m dans sa plus grande largeur, et 0,20m d'épaisseur en moyenne ; deux autres dalles de taille plus petite, au Nord, pourraient évoquer soit des restes de la chambre funéraire, soit faire partie du tumulus...

À la périphérie Ouest de ce dernier on voit une dalle en partie enfouie dans le sol, mesurant 1,80m de long et 1,70m de large ; dalle de couverture ? montant arraché ?

Historique

Monument découvert par [I. Txintxuretta]{.creator} en [novembre 2011]{.date}.

### [Urrugne]{.spatial} - [Urbisiko erreka]{.coverage} - [Pierre dressée]{.type}

Localisation

Altitude : 300m.

Elle se trouve à environ 100m à l'Ouest du tumulus 5, sur un petit plateau d'une centaine de mètres de long, où l'on trouve aussi *Urbisiko erreka 1 - Dolmen* et *Urbisiko erreka 2 - Dolmen*.

Description

Bloc de grés trapu, posé ou planté dans le sol, mesurant 1,40m à la base, 0,60m de large et 1,04m de haut. Sa périphérie ne présente aucune trace d'épannelage. Il peut tout aussi bien s'agir d'un bloc d'éboulis...

Historique

Pierre découverte par [I. Txintxuretta]{.creator} en [novembre 2011]{.date}.

### [Urrugne]{.spatial} - [Urbisiko erreka]{.coverage} 1 - [Tumulus]{.type}

Localisation

Altitude : 290m.

Il est situé au-dessus de *Urbisiko erreka - Cromlech*.

Description

Tumulus de terre et de nombreuses pierres, de 3m de diamètre environ et 0,30m de haut.

Historique

Monument découvert par [I. Txintxuretta]{.creator} en [novembre 2011]{.date}.

### [Urrugne]{.spatial} - [Urbisiko erreka]{.coverage} 2 - [Tumulus-cromlech]{.type}, [Cromlech]{.type} ([?]{.douteux})

Localisation

Altitude : 255m.

La ciste se trouve à 25m au Sud, et il est lui-même à 40m au-dessus et au Nord de *D 3*.

Description

Tumulus de 6m de diamètre et 0,60m de haut ; on voit une vingtaine de pierres disposées de manière plus ou moins circulaire : tumulus-cromlech ou cromlech ou (?) monument douteux.

Historique

Monument découvert par [F. Meyrat]{.creator} en [janvier 2012]{.date}.

### [Urrugne]{.spatial} - [Urbisiko erreka Zutarria]{.coverage} - [Dalle couchée]{.type}, [Monolithe]{.type}, [Dolmen]{.type} [(?)]{.douteux}

Localisation

Altitude : 365m.

Sur un petit replat incliné vers l'Ouest.

Description

Belle dalle de grés rose, gisant selon un axe Est-Ouest, de forme rectangulaire, mesurant 1,90m de long, 0,94m de large et 0,32m d'épaisseur. Il semble qu'il y ait des traces d'épannelage sur les bords Nord et Est. Cette dalle n'est pas à plat sur le sol mais repose sur la périphérie Ouest d'un tumulus (naturel ou non ?), mesurant environ 3,70m de diamètre et 0,50m de haut. S'agit-il donc d'un monolithe, d'une simple dalle couchée, ou d'une table dolménique par exemple ?

Historique

Dalle découverte par [I. Txintxuretta]{.creator} en [novembre 2011]{.date}.

### [Urrugne]{.spatial} - [Xoldokocelay]{.coverage} 3 - [Dolmen]{.type}

Localisation

Carte 1245 OT. Hendaye-St Jean-de-Luz.

Altitude : 365m.

Ce dolmen se trouve sur un replat, au flanc Nord du mont Xoldokogagna, à environ 150m à l'Est Nord-Est de *Xoldokogaina 2 - Dolmen*, lui-même situé à une trentaine de mètres au Nord de *Xoldokogaina 1 - Dolmen*.

Localisation et description de ces deux dolmens dans [@chauchatSeptDolmensNouveaux1966 p.106].

Description

On note un important tumulus pierreux, de 8m de diamètre et 0,80m de haut, formé de nombreux blocs de grés de tailles variables, au centre duquel une dépression d'environ 4,20m de long et 1,20m de large représente ce qui reste d'une chambre funéraire, où apparaissent encore de nombreuses dalles de [grés]{.subject}, dont l'aspect et le désordre montrent que ce monument a fait l'objet de fouilles sauvages dans le passé.

Il semble, en fait, qu'on puisse distinguer deux chambres funéraires distinctes, dans le prolongement l'une de l'autre, alignées suivant un axe Nord-Est Sud-Ouest, séparées par une dalle verticale de 0,73m de long, 0,50m de haut et 0,12m d'épaisseur.

La chambre funéraire située à l'extrémité Sud-Ouest mesure environ 2m de long et paraît avoir été délimitée à l'arrière par une dalle de chevet triangulaire, qui gît actuellement sur le sol ; celle-ci mesure 0,70m à sa base et 0,97m de long, (ou de haut, quand elle était verticale). La paroi latérale, au Nord-Ouest est délimitée par 3 dalles verticalement enfoncées dans le sol, dont les 2 plus importantes, dans le prolongement l'une de l'autre, mesurent respectivement 0,95m et 0,92m de long, et 0,15m d'épaisseur.

La deuxième chambre, dans le prolongement de la première, mesure environ 2m de long et en est séparée par la dalle verticale déjà citée. Sa paroi Nord-Ouest est délimitée par 4 dalles de taille variable ; trois autres dalles de taille plus modeste matérialisent la paroi Sud-Est.

Il est particulièrement intéressant de noter l'existence de ces deux chambres funéraires dans le prolongement l'une de l'autre ; c'est la première fois que nous voyons ceci en pays Basque Nord ; il nous est déjà arrivé de voir deux chambres funéraires, mais contiguës latéralement, comme pour le dolmen de Berdaritz (commune des Aldudes) ou celui d'Abrakou, *Abrakou - Dolmen*.

Historique

Monument découvert par [Goyo Mercader]{.creator} en [1998]{.date}.

### [Urrugne]{.spatial} - [Xoldokocelay]{.coverage} 11 - [Dolmen]{.type}

Localisation

Altitude : 440m.

Ce monument se trouve à 20m au Nord Nord-Ouest de *Xoldokocelai 10 - Dolmen*.

(commune de [Biriatou]{.spatial}).

Description

Chambre funéraire de 1,90m de long, 1,08m de large, orientée Nord-Sud, érigée sur un terrain en légère pente vers le Nord. Elle est délimitée par un ensemble de dalles, en grande partie enfoui sous une épaisse végétation : 2 à l'Est, 1 au Nord, 3 à l'Ouest et une, semble-t- il, au Sud, totalement cachée.

Historique

Monument découvert en [avril 2010]{.date} par [F. Meyrat]{.creator}.

### [Urrugne]{.spatial} - [Xoldokocelay]{.coverage} 11 bis - [Dolmen]{.type}

Localisation

Altitude : 440m.

Il est à quelques dizaines de centimètres au Nord de Xoldokocelai 11 - Dolmen.

Description

Une dizaine de dalles plus ou moins en désordre paraissent délimiter une chambre funéraire rectangulaire, à grand axe Nord-Sud.

Historique

Monument découvert par [F. Meyrat]{.creator} en [février 2010]{.date}.

### [Urrugne]{.spatial} - [Xoldokocelay]{.coverage} 14 - [Dolmen]{.type}

Localisation

Altitude : 380m.

Il est situé sur un petit replat à quelque distance de la rive droite d'un petit ruisseau.

Description

La chambre funéraire, rectangulaire à grand axe orienté Nord-Est Sud-Ouest, mesure 2,30m de long et 1,50m de large. Elle est délimitée par une douzaine de dalles de modestes dimensions. La paroi Nord est délimitée par deux (ou trois ?) dalles dont l'une mesure 0,47m de long et 0,47m de haut, l'autre 0,45m de long et 0,40m de haut. À l'Ouest, on note deux dalles debout et une couchée, aux dimensions semblables aux précédentes ; la paroi Sud est essentiellement marquée par quatre dalles dont les longueurs varient de 0,30m à 0,50m et les hauteurs de 0,10m à 0,30m. Enfin la paroi Est est délimitée par au moins trois dalles, dont les longueurs varient de 0,40m à 0,53m et la hauteur de 0,20m à 0,40m. Cette chambre est inscrite dans un tumulus d'environ 4m de diamètre, essentiellement pierreux, fait de petites dalles de grés local.

Historique

Monument découvert par [P. Badiola]{.creator} en [février 2011]{.date}, et dégagé par F. Meyrat.

### [Urrugne]{.spatial} - [Xoldokocelay]{.coverage} 15 - [Dolmen]{.type}

Localisation

Altitude : 355m.

Situé à 20m à l'Ouest et au-dessus de Xoldokocelay 2 - Tumulus.

Description

Tumulus très peu marqué avec peut-être une ébauche de péristalithe. Au centre, la chambre funéraire, orientée Sud-Est Nord-Ouest, semble essentiellement délimitée au Nord-Est par une grande dalle inclinée vers l'Est, mesurant 1,53m dans sa plus grande dimension, 1,15m de large et 0,35m d'épaisseur. Six à sept autres petites dalles complèteraient cette chambre, dont une dalle au Sud-Est, verticalement plantée, mesurant 0,50m à sa base et 0,30m de haut.

Historique

Monument découvert par [I. Txintxuretta]{.creator} en [novembre 2011]{.date}.

### [Urrugne]{.spatial} - [Xoldokocelay]{.coverage} 2 - [Tumulus]{.type}

Localisation

Altitude : 350m.

Sur un petit replat à droite d'un sentier qui monte après la traversée d'un petit riu. Il est à 20m à l'Est et en contre-bas de *Xoldokocelay 15 - Dolmen*.

Description

Tumulus de faible hauteur, fait de terre et de pierres, mesurant 5m de diamètre environ. Au centre, un ensemble de 5 pierres, dont les dimensions avoisinent 0,30m à 0,40m de long, pourrait représenter la ciste centrale. Il semble qu'on puisse distinguer une ébauche de péristalithe dans le secteur Nord-Est du tumulus.

Historique

Monument découvert par [I. Txintxuretta]{.creator} en [novembre 2011]{.date}.

### [Urrugne]{.spatial} - [Xoldokocelay]{.coverage} 3 - [Tumulus]{.type}

Localisation

Altitude : 460m.

Il est érigé sur un terrain en pente légère vers le Nord et à 250m environ au Nord de la borne géodésique du mont Xoldokogana.

Description

Tumulus de terre et quelques pierres, de 6m de diamètre et 0,50m de haut en moyenne. Quelques dalles couchées apparaissent à sa périphérie sans qu'on puisse parler de péristalithe.

Historique

Tumulus découvert par [Meyrat F.]{.creator} en [avril 2012]{.date}.

### [Urrugne]{.spatial} - [Xoldokogaina]{.coverage} - [Dalle plantée]{.type} [(?)]{.douteux}

Localisation

Altitude : 460m.

Dalle située sur la pente Nord du mont Xoldokogaina, à une centaine de mètre environ au-dessous de *Xoldokogaina 1 - Dolmen*. Cette dalle est érigée sur un « tumulus » d'environ 3,60m de diamètre que traverse une piste pastorale orientée Est-Ouest. La dalle, plantée selon un axe Ouest-Est, est légèrement inclinée vers le Nord.

Description

Elle est en grés local, mesure 0,78m de haut ; sa base, solidement implantée mesure 0,40m de long et l'épaisseur varie de 0,17m à la base à 0,9m au sommet. On distingue sur ce dernier, taillé en pointe, une croix gravée en profondeur, au 2 branches égales (7 centimètres chacune), et orientées selon les 4 points cardinaux ; enfin tout le bord Ouest de la dalle présente des traces évidentes d'épannelage. Le « tumulus », qui se fond dans son secteur Sud avec la piste, est, par ailleurs, nettement visible. Enfin, le sol au pied de la dalle, côté Nord, est creusé, très probablement par les ovins, ce qui a favorisé, avec la pente, l'inclinaison de la dalle. Borne pastorale ? vestige de dolmen ? (Peu probable).

Historique

Dalle découverte par [Blot J.]{.creator} en [octobre 2010]{.date}.

### [Urrugne]{.spatial} - [Xoldokogaina]{.coverage} 1 - [Dolmen]{.type}

Localisation

Altitude : 472m.

Il est situé à 18m au Nord de la piste qui parcours le sommet du plateau et va ensuite vers le Lezante.

Description

Une chambre funéraire de 1,60m de long et 0,90m de large orientée Ouest-Est.

Est délimitée au Sud par 3 dalles dont l'une encore dressée, mesure 0,57m à sa base, 0,81m de haut et 0,13m d'épaisseur ; deux autres dalles complètent cette paroi, dont l'une mesure 0,60m de long, et 0,37m de haut. La paroi Nord est représentée par une dalle rectangulaire couchée et en partie enfouie dans le sol, dont les mensurations apparentes sont de 1,08m de long et 0,68m de large. On ne note pas de tumulus.

Historique

Monument découvert par [J. Blot]{.creator} en [avril 2010]{.date}.

### [Urrugne]{.spatial} - [Xoldokogaina]{.coverage} 2 - [Dolmen]{.type} [(?)]{.douteux}

Localisation

Altitude : 471m.

Il est situé à environ 150m au Sud-Est de *Xoldokogaina 1 - Dolmen*.

Description

Une dalle de grés rose, presque horizontale semble être le couvercle du monument : elle mesure 1,65m de long, 1,17m de large, et 0,22m d'épaisseur ; elle est orientée Sud-Ouest Nord-Est. La paroi du côté Nord-Ouest est formée par une dalle plantée de 1m de long et 0,35m de haut, très inclinée vers l'extérieur, et d'une deuxième, beaucoup plus petite sous la dalle horizontale elle-même. Peut-être y-a-t-il une ébauche de tumulus ? Il *ne semble pas* que cet ensemble fasse partie des remaniements du terrain dus aux carriers...

Historique

Monument (douteux) découvert par [F. Meyrat]{.creator} en [avril 2010]{.date}.

### [Urrugne]{.spatial} - [Xoldokogaina]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 471m.

Il est situé à peu près au milieu entre *Xoldokogaina 2 - Dolmen (?)* et *Xoldokogaina 1 - Dolmen*.

Description

Petit tumulus circulaire de terre et de pierres de 2,90m de diamètre et 0,30m de haut, qui paraît, là encore, tout à fait indépendant de tous les remaniements dus aux carriers.

Historique

Monument découvert par [F. Meyrat]{.creator} en [avril 2010]{.date}.

### [Urrugne]{.spatial} - [Xoldokogaina]{.coverage} - [Dalle couchée]{.type} [(?)]{.douteux}

Localisation

Altitude : 460m.

Description

Cette dalle de grés local, de forme rectangulaire, repose sur une élévation de terre, de 0,60m de haut, elle mesure 1,78m de long, 0,47m à 0,50m de large. Elle est brisée dans son tiers Sud-Est et un gros fragment est absent dans le quart Nord-Est. Des traces d'épannelage sont visibles sur ses bords Nord et Sud.

Historique

Dalle découverte par [Badiola P.]{.creator} en [mai 2012]{.date}.

### [Urrugne]{.spatial} - [Xuanen Borda]{.coverage} 1 - [Tumulus]{.type}

Localisation

Altitude : 159m.

Il est érigé dans un élargissement de la vallée, sur le bord gauche, c'est à dire Nord d'une bretelle que détache la piste qui monte vers Arlepoa, à l'endroit où cette bretelle vient de traverser le Larrungo erreka et chemine sur sa rive droite.

Description

Tumulus ovale, érigé sur terrain plat, à grand axe Nord-Est Sud-Ouest, mesurant 6m x 5m et plus d'1m de haut.

Il est constitué essentiellement d'un amas pierreux, recouvert d'une abondante végétation ; on voit quelques éléments rocheux plus volumineux à la périphérie en particulier dans les quadrants Sud-Ouest et Nord-Est : éléments d'un péristalithe ? S'agit-il d'un tumulus dolménique ?

Historique

Monument découvert par [Blot J.]{.creator} en [avril 2014]{.date}.

### [Urrugne]{.spatial} - [Xuanen Borda]{.coverage} 2 - [Tumulus]{.type}

Localisation

À 15m au Sud Sud-Est de *Xuanen Borda 1 - Tumulus* ; il est de l'autre côté de la piste qui vient de traverser le rio, c'est à dire au Sud de celle-ci, et sur son bord droit en montant vers Xuanen borda.

Description

Tumulus circulaire, plus modeste que *Xuanen Borda 1 - Tumulus*, mesurant environ 2m de diamètre et 0,55m de haut. Il est essentiellement constitué de pierres, et semblerait être entouré d'un péristalithe dont 6 pierres particulièrement visibles.

Historique

Monument découvert par [Blot J.]{.creator} en [avril 2014]{.date}.

### [Urrugne]{.spatial} - [Xapatainbaita]{.coverage} - [Camp protohistorique]{.type}

Localisation

Il est situé à droite de la route qui relie Ciboure à Olhette en passant par Béthanie, au niveau d'un virage, dans une descente. Des lignes à haute tension passent juste au-dessus.

Altitude : 104m.

Avant la construction des habitations actuellement visibles, on remarquait d'importantes levées de terre caractéristiques d'un camp protohistorique, dont il ne reste que des traces actuellement.

Historique

Ce camp a été découvert par [Blot J.]{.creator} en [1976]{.date}, et ne semble pas avoir été décrit par F. Gaudeul.

### [Urrugne]{.spatial} - [Ziburumendi]{.coverage} - [Monolithe]{.type}

Localisation

Altitude : 353m.

Il est situé au sommet de la piste qui monte directement du col du Grand Descarga vers Ziburumendi, sur le premier replat qui domine un col situé à l'Ouest. Il est à 2m à l'Ouest de cette piste.

Description

Bloc de grés triasique en forme de parallélépipède rectangle, couché au sol selon un axe Est Nord-Est Ouest Sud-Ouest, et présentant deux extrémités effilées. Il mesure 2,67m de long, 0,41m dans sa plus grande largeur, et 0,18m à ses deux extrémités. Son épaisseur moyenne est de 0,55m. On note de nombreuses traces d'épannelage : à ses deux extrémités, sur sa face supérieure, et sa face Nord-Ouest.

Historique

Monolithe découvert par [A. Martinez]{.creator} en [2014]{.date}.

### [Urrugne]{.spatial} - [Ziburukomendia]{.coverage} - [Dolmen]{.type}

Localisation

Carte 1245 Ouest Saint-Jean-de-Luz.

Altitude : 310m.

Il est situé sur un replat au flanc Sud-Ouest du mont appelé « Montagne de Ciboure », en surplomb du col du Grand Descarga qui la sépare de Larraun (La Rhune).

Description

Tumulus mixte de terre et de pierres (petits blocs de poudingue), mesurant 6m de diamètre et 0,40m de haut. Au centre une chambre funéraire rectangulaire à grand axe Sud Sud-Ouest, Nord Nord-Est ; elle est délimitée par 4 dalles de grès triasique perpendiculaires les unes aux autres : à l'Est, une dalle 0,90m de long et 0,50m de haut, à l'Ouest une dalle de 1,20m de long et 0,60m de haut ; enfin au Nord comme au Sud une dalle de 0,50m de long et 0,30m de haut. Il n'y a pas de couvercle visible.

Historique

Dolmen découvert en [mars 1973]{.date}. Ce monument a été complètement rasé lors de la plantation de conifères effectuée en 1974.

### [Urrugne]{.spatial} - [Zizkuitz]{.coverage} - [Dalle couchée]{.type} [(?)]{.douteux}

Localisation

Altitude : 643m.

Cette dalle se trouve en bordure Sud-Est de la piste qui, au flanc Ouest de Larrun, se rend à Zizkuitz.

Description

De forme parallélépipédique, elle mesure 1,50m de long, 0,60m de large et pourrait présenter des traces d'épannelage à son bord Sud-Ouest.

Historique

Dalle découverte par [Badiola P.]{.creator} en [mai 2012]{.date}.

### [Urrugne]{.spatial} - [Zubizia]{.coverage} - [Pierre plantée]{.type} [(?)]{.douteux}

Localisation

Altitude : 540m.

Elle se trouve sur terrain en pente à environ 200m à l'Est et au-dessus de la BF 21.

Description

Pierre de grés de forme triangulaire (sur ses faces Est et Ouest), haute de 1,15m et épaisse de 0,50m. À son sommet une petite encoche. Elle est dans un contexte de carrier.

Historique

Pierre trouvée par [Badiola P.]{.creator} en [avril 2012]{.date}.
