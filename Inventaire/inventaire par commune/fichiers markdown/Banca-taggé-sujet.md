# Banca

### [Banca]{.spatial} - [Abrakou]{.coverage} - [Dolmen]{.type}

Localisation

Carte 1346 Ouest.

Altitude : 900m.

Il est érigé en bordure du chemin de ligne de crête qui rejoint Ichtauz à Abrakou-lepoa. Il est situé à 120m à l'Est de la BF 112.

Description

Tumulus pierreux, d'environ 8 à 9m de diamètre et 1m de haut, formé de petits blocs de [grès]{.subject} particulièrement visibles dans sa partie Sud-Ouest, et moins au Nord-Est où a poussé un arbre à environ 2m du centre.

Comme dans le cas du *dolmen de Berdaritz*, situé sur la même ligne de crête, à environ 2000 mètres plus au Sud-Ouest, il semble que l'on puisse distinguer 2 chambres funéraires :

Une [chambre funéraire]{.subject} au Sud-Est, orientée Est Nord-Est, d'environ 1,50m de long et 0,80m de large, bien délimitée par 4 dalles dont 3 sont déversées vers l'intérieur de la chambre.

Une chambre à 1m au Nord-Ouest de la précédente, visible sous la forme d'une dépression de 1,40m x 1m, sans dalles visibles, sans doute le résultat probable d'une fouille ancienne ayant démoli la structure originelle. Il n'y a pas de couvercle(s) visible(s).

Historique

Dolmen découvert en [août 1975]{.date}.

### [Banca]{.spatial} - [Antchola]{.coverage} - [Monolithe fendu]{.type}

Localisation

Altitude : 1118m.

Il est situé à une vingtaine de mètres au Sud Sud-Ouest de la BF 105.

Description

Volumineux bloc de [grès]{.subject} rose rectangulaire couché au sol suivant un axe Est-Ouest, mesurant 1,96m de long, 1,38m de large à sa base et 1,19m dans sa partie moyenne (la séparation étant incluse) ; épaisseur de 0,43m. Il semble présenter des traces d'épannelage à son bord Nord. À l'évidence, cette séparation est soit due à l'action du gel... ou de l'homme.

Historique

Monument découvert par [F. Meyrat]{.creator} en [avril 2010]{.date}.

### [Banca]{.spatial} - [Antchola]{.coverage} 1 - [Cromlech]{.type}

Localisation

Altitude : 1086m.

Il est situé sur un petit replat, dans la montée vers la BF 106 en partant du col d'Ixtauz, à une trentaine de mètres à l'Ouest de la pente Ouest abrupte du mont Antchola.

Description

De nombreuses petites pierres ou dalles de [grès]{.subject} disposées en couronne de 2,50m de diamètre, entourent un amas central de même type.

Historique

Monument découvert par [J. Blot]{.creator} en [avril 2010]{.date}.

### [Banca]{.spatial} - [Antchola]{.coverage} 2 - [Cromlech]{.type} [(?)]{.douteux}

Localisation

Il est situé à 9m à l'Est Sud-Est de *Antchola 1 - Cromlech*, sur terrain plat.

Description

Environ 13 pierres au ras du sol délimitent un cercle de 2,60m de diamètre.

Historique

Monument (douteux ?) découvert par [J. Blot]{.creator} en [avril 2010]{.date}.

### [Banca]{.spatial} - [Antchola]{.coverage} 3 - [Cromlech]{.type} [(?)]{.douteux}

Localisation

Au niveau de la BF 106.

Altitude : 1100m.

Description

On note, entourant l'implantation de la BF 106, un léger relief circulaire de 6 mètres de diamètre environ, fait de petites dalles de [grès]{.subject} ; de plus il semble que l'on puisse distinguer quelques pierres d'un éventuel péristalithe. Enfin, il ne semble pas que l'on puisse attribuer au renforcement de l'implantation de la BF 106 ce relief tumulaire. Monument douteux, mais possible, l'implantation d'une borne frontière au milieu d'un monument n'étant pas exceptionnelle...

Historique

Monument découvert par [I. Gaztelu]{.creator}, [G. Mercader]{.creator} et [L. Millan]{.creator} en [novembre 2007]{.date}.

### [Banca]{.spatial} - [Antchola]{.coverage}  - Deux [tumulus-cromlechs]{.type} [(?)]{.douteux}

Localisation

En montant du col d'Ichtauz vers le sommet d'Antchola, à une dizaine de mètres à l'Ouest de la piste, près d'un gros rocher, il semble que l'on puisse voir deux monuments (?) très semblables à des tumulus-cromlechs, distants l'un de l'autre de 5m environ, selon un axe Nord-Sud, sur un terrain assez pentu.

Altitude : 1045m.

Description

Ils mesurent tous deux environ 6m de diamètre, et sont constitués de pierres inclinées, disposées semble-t-il de façon circulaire autour d'autres pierres en désordre au centre.

Historique

Monuments découverts par [J. Blot]{.creator} en [avril 2010]{.date}.

### [Banca]{.spatial} - [Argaray]{.coverage} 1 - [Cromlech]{.type} [(?)]{.douteux}

Localisation

Altitude : 1206m.

Ce monument, se trouve sur la ligne de crête du mont Argaray, à 3m à l'Ouest de la clôture de barbelés qui matérialise la frontière, à environ 120m au Sud de la BF 171, et à 30m au Sud d'une barre rocheuse perpendiculaire à l'axe de la ligne de crête. Erigé sur un terrain en légère pente vers l'Ouest.

Description

Une bonne vingtaine de pierres de calibres variés, délimitent un cercle approximatif de 4,10m de diamètre ; certaines mesurent plus de 1m de long et 0,45m de large, dépassant la surface du sol de 0,20m à 0,40m. D'autres éléments apparaissent à l'intérieur de ce cercle (?). Traces possibles d'excavation ou de dépression légère au centre de ce cercle... Il semblerait enfin qu'un demi-cercle (de 8,20m de diamètre), formé de 11 pierres plus ou moins au ras du sol, double le premier cercle (?), dans sa moitié Sud. Au total, monument très douteux, (peut être une simple disposition naturelle...) mais que nous ne pouvons totalement exclure de l'inventaire.

Historique

Cercle découvert par [P. Velche]{.creator} en [avril 2015]{.date}.

### [Banca]{.spatial} - [Argaray]{.coverage} 2 - [Cromlech]{.type}

Localisation

Altitude : 1217m.

Situé toujours sur la ligne de crête, à 6m à l'Ouest du barbelé frontière et à 140m environ au Sud de la grande barre rocheuse.

Description

12 pierres au ras du sol, mais bien visibles, délimitent un petit cercle de 3,40m de diamètre, avec un léger tumulus central de 0,10m de haut, nettement visible.

Historique

Cercle découvert par [P. Velche]{.creator} en [avril 2015]{.date}.

### [Banca]{.spatial} - [Argaray]{.coverage} 3 - [Cromlech]{.type} [(?)]{.douteux}

Localisation

Il est tangent au Sud-Ouest de *Argaray 2 - Cromlech*. Terrain en pente légère vers l'Ouest.

Description

Petit cercle de 2,60m de diamètre, délimité par 8 petites pierres, au ras du sol. Monument douteux.

Historique

Cercle découvert par [F. Meyrat]{.creator} en [mai 2015]{.date}.

### [Banca]{.spatial} - [Argaray]{.coverage} 4 - [Cromlech]{.type} [(?)]{.douteux}

Localisation

Tangent à l'Ouest de *Argaray 3 - Cromlech (?)*. Terrain en pente légère vers l'Ouest.

Description

Cercle (?) de 1,40m de diamètre, délimité par 9 pierres de dimensions variables, mais modestes dans l'ensemble. Monument douteux.

Historique

Cercle découvert par [F. Meyrat]{.creator} en [mai 2015]{.date}.

### [Banca]{.spatial} - [Argaray]{.coverage} 5 - [Cromlech]{.type}

Localisation

Altitude : 1212m.

Il se trouve à 90m au Sud Sud-Est de *Argaray 1 - Cromlech*, à 3m au Nord d'un filon rocheux et à 20m au Nord de *Argaray 2 - Cromlech*.

Description

9 pierres au ras du sol, mais bien visible délimitent un cercle de 2,40m de diamètre, avec très léger tumulus central de 0,15m de haut.

Historique

Monument découvert par [F. Meyrat]{.creator} en [mai 2015]{.date}.

### [Banca]{.spatial} - [Argaray]{.coverage} bas - [Tertre d'habitat]{.type}

Localisation

Altitude : 1076m.

Les 2 tertres d'habitat sont situés à une quinzaine de mètres au Sud-Est d'un très volumineux amas rocheux et à une quarantaine de mètres à l'Ouest d'un point d'eau.

Description

Ces deux tertres de terre, tangents, sont érigés sur un terrain en légère pente vers le Nord-Est. Ils sont de dimensions identiques : diamètre de 5m environ et 0,60m de haut.

Historique

Tertres découverts par [F. Meyrat]{.creator} en [août 2014]{.date}.

### [Banca]{.spatial} - [Argaray]{.coverage} haut - [Tertre d'habitat]{.type}

Localisation

Altitude : 1178m.

Il est situé à 80m au Nord de la BF 171, à 1m au Nord-Est du barbelé de la ligne frontière, et à 12m au Sud-Ouest d'un pointement rocheux.

Description

Tertre discret de 9m à 10m de diamètre, et 0,30 à 0,40m de haut, présentant une légère dépression centrale ; 7 pierres sont visibles en surface.

Historique

Monument trouvé par [F. Meyrat]{.creator} en [août 2014]{.date}.

### [Banca]{.spatial} - [Argaray]{.coverage} - [Monolithe]{.type}

Localisation

Altitude : 1181m.

Territoire navarrais, tangent à la commune de Banca.

Il se trouve sur un terrain en pente vers l'Est, à 2m à l'Est du barbelé frontière, et à 120m environ au Nord Nord-Est de la BF 171. On note un point d'eau à 20m à l'Est de ce monolithe.

Description

Bloc de [calcaire]{.subject} de forme parallélépipédique, allongée selon un axe Sud-Ouest Nord-Est, à sommet en pointe au Nord-Est. Il mesure 2,90m de long, 1,30m de large en moyenne ; son épaisseur à la base est de 0,34m et au sommet de 0,18m. Il présente sur tout son pourtour des traces évidentes d'épannelage.

Historique

Monolithe découvert par [F. Meyrat]{.creator} en [mai 2014]{.date}.

### [Banca]{.spatial} - [Axistoi]{.coverage} 3 - [Tumulus]{.type}

Localisation

Carte 1346 Ouest Saint-Etienne-de-Baïgorry.

Altitude : 1220m.

Il est situé sur cette longue ligne de crête qui borde, à l'Est, la Vallée des Aldudes.

Nous avons publié en 1972 [@blotNouveauxVestigesMegalithiques1972b p.196], deux cromlechs : *le n°1*, de 4m de diamètre et 12 pierres, à 25m au Nord-Est de la borne-frontière n°157 ; *le n°2*, à 12m au Nord-Est du précédent, mesure 4m de diamètre, et possède 5 pierres.

Description

Tumulus à prédominance pierreuse (nombreuses petites plaquettes de [schiste]{.subject}), mesurant 5m de diamètre et 0,30m de haut. Une dépression centrale de 1m de diamètre et 0,30m de profondeur pourrait être la trace d'une ancienne fouille pratiquée au niveau de la [ciste]{.subject} centrale.

Historique

Monument découvert en [novembre 1974]{.date}.

### [Banca]{.spatial} - [Ehunzaroy]{.coverage} (Hegoalde) - [Cromlech]{.type}

Localisation

Altitude : 977m.

Situé en territoire de Valcarlos

Ce monument, érigé sur un terrain en pente très douce vers le Nord-Est, est situé à 80m au Sud Sud-Ouest du col d'Ehunzaroy et de la piste qui le traverse, et à une dizaine de mètres au Sud-Est d'une clôture de barbelés qui symbolise la ligne frontière.

Description

Cercle de 12m de diamètre, délimité par 6 pierres de dimensions très variables : l'une, au Nord-Est atteint 1,23m x 0,30m, l'autre au Sud-Ouest de 0,80m x 0,60m ; petite pierre centrale.

On note un léger tumulus central, n'excédant pas 6m de diamètre et 0,30m de haut.

Historique

Monument découvert par [F. Meyrat]{.creator} en [Août 2014]{.date}.

### [Banca]{.spatial} - [Ehunzaroy]{.coverage} (col) 1 - [Tertre d'habitat]{.type}

Localisation

Altitude : 978m.

Situé à 27m au Nord de la route qui aboutit au col, avant les panneaux solaires.

Description

Tertre classique, dissymétrique, sur terrain en pente vers le Nord, mesurant 8m x 6m et 0,40m de haut environ.

Historique

Monument découvert par [F. Meyrat]{.creator} en [septembre 2014]{.date}.

### [Banca]{.spatial} - [Ehunzaroy]{.coverage} (col) 2 - [Tertre d'habitat]{.type}

Localisation

Altitude : 976m.

À 6m au Nord du bord de la route, un peu avant la barrière canadienne.

Description

Vaste tertre dissymétrique, érigé sur terrain en pente vers Nord-Est de 12m x 12m et de 1m de haut environ. Il domine le fond du vallon que forme le col.

Historique

Monument trouvé par [F. Meyrat]{.creator} en [septembre 2014]{.date}.

### [Banca]{.spatial} - [Ehunzaroy]{.coverage} (col) 3 - [Tertre d'habitat]{.type}

Localisation

À 2m au Sud-Est de *Ehunzaroy col 2 - Tertre d'habitat* et tangent à la route.

Description

Tertre de dimensions beaucoup plus modestes, mesurant 7m x 5m et 0,40m de haut.

Historique

Monument découvert par [Blot J.]{.creator} en [septembre 2014]{.date}.

### [Banca]{.spatial} - [Ehunzaroy]{.coverage} (col) 4 - [Tertre d'habitat]{.type}

Localisation

Altitude : 976m.

Il est à 3m au pied et au Nord du vaste *Ehunzaroy col 2 - Tertre d'habitat*, et à 8m environ au Sud-Ouest du petit ru du fond de vallon.

Description

Tertre sur terrain en pente vers Nord Nord-Est, dissymétrique, mesurant 8m x 5m et 0,40m de haut.

Historique

Monument découvert par [F. Meyrat]{.creator} en [septembre 2014]{.date}.

### [Banca]{.spatial} - [Errola]{.coverage} Nord 1 - [Tumulus]{.type} [(?)]{.douteux}

Localisation

Altitude : 856m.

Il est situé le à côté de la piste de crête qui relie le mont Errola au mont Otsamunho près de la naissance d'un petit ru.

Description

Ce tumulus (douteux ?), terreux, mesure 4,60m de diamètre et 0,40m de haut. À son sommet on note une légère dépression centrale.

Historique

Tumulus découvert par [F. Meyrat]{.creator} en [novembre 2014]{.date}.

### [Banca]{.spatial} - [Errola]{.coverage} Nord 2 - [Tumulus]{.type}

Localisation

Altitude : 845m.

Situé à quelques dizaines de mètres au Nord-Est de *Errola nord 1 - Tumulus*, à la confluence de deux pistes pastorales, à 4m au Sud-Ouest de la piste nouvelle qui se rend du col de Mizpira à un nouveau cayolar plus au Sud.

Description

Tumulus terreux de 7,20m de diamètre, peu élevé (0,20m) mais néanmoins fort bien visible. Une pierre apparaît dans sa périphérie Sud Sud-Ouest et on note une dépression centrale, peu marquée, de 1,40m de diamètre environ.

Historique

Tumulus découvert par [Blot J.]{.creator} en [décembre 2014]{.date}.

### [Banca]{.spatial} - [Haradoko lepoa]{.coverage} - [Pierres couchées]{.type} ([?]{.douteux})

La première est située à 9m au Nord Nord-Est de la BF 165 : cette pierre, de forme parallélépipédique, qui mesure 1,40m de long, 0,78m de large, 0,38m d'épaisseur, couchée selon un axe Nord-Sud, présente un sommet Sud arrondi très probablement de main d'homme ; elle ne porte aucune inscription, mais semble bien avoir été volontairement amenée là... quand et pourquoi ? ancienne borne frontière ?

Une autre pierre couchée parallélépipédique, de l'autre côté de la frontière, est située à 4m au Nord Nord-Est de la BF 165, et paraît-elle aussi avoir subi quelque régularisation ; elle mesure 1,55m de long, 1m de large et 0,19m d'épaisseur. 

([J. Blot]{.creator} et [F.Meyrat]{.creator} - [avril 2010]{.date})

### [Banca]{.spatial} - [Harrigorri]{.coverage} - [Cromlech]{.type}

Localisation

Carte 1346 Ouest Saint-Etienne-de-Baïgorry.

Altitude : 1020m.

Il est sur la ligne de crête qui borde à l'Ouest la Vallée des Aldudes, et au flanc Sud-Est du mont Autza, sur un petit replat à une quarantaine de mètres à l'Est de la verticale du rocher dénommé *Harrigorri*.

Description

Cercle de pierres de 5,50m de diamètre, délimité par une vingtaine de pierres, au ras du sol.

Historique

Monument découvert en [juin 1971]{.date}.

### [Banca]{.spatial} - [Harrigorri]{.coverage} 2 - [Cromlech]{.type}

Localisation

Altitude : 1117m.

Erigé sur un petit sommet dominant le col.

Description

Cromlech surélevé mesurant 3,70m de diamètre et délimité par une quinzaine de pierres.

Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

### [Banca]{.spatial} - [Harrigorri]{.coverage} 3 - [Cromlech]{.type}

Localisation

À 20m au Nord-Est de *Harrigorri 2 - Cromlech*.

Description

Petit cercle de 1,80 m de diamètre, légèrement en relief, érigé sur un terrain en pente douce vers le Nord-Ouest et délimité par une quarantaine de petites pierres.

Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

### [Banca]{.spatial} - [Hortz-Zorroz]{.coverage} 3 - [Tumulus]{.type}

Localisation

Carte 1346 Ouest Saint-Etienne-de-Baïgorry.

Altitude : 1023m.

Monument faisant partie de ceux échelonnés sur la longue crête montagneuse, étendue dans la Vallée des Aldudes selon un axe Sud Sud-Est Nord Nord-Ouest, du mont Lindus au Munhoa. Nous avons déjà décrit [@blotNouveauxVestigesMegalithiques1972b p.192], 2 cromlechs en ces lieux. Le n°1 : 6m de diamètre et 30 pierres ; le n°2 : à 5m au Sud du précédent, possède 13 pierres et un diamètre de 4m.

Description

Tumulus mixte de 4m de diamètre et 0,30m de haut. Une vingtaine de pierres apparaissent à la surface du tumulus, assez inégalement réparties.

Historique

Monument découvert en [mai 1973]{.date}.

### [Banca]{.spatial} - [Hortz-Zorroz]{.coverage} 4 - [Tumulus]{.type}

Localisation

Carte 1346 Ouest Saint-Etienne-de-Baïgorry.

Monument faisant partie de ceux échelonnés sur la longue crête montagneuse, étendue dans la Vallée des Aldudes selon un axe Sud Sud-Est Nord Nord-Ouest, du mont Lindus au Munhoa [@blotNouveauxVestigesMegalithiques1972b]

Description

Tumulus mixte de 2,80m de diamètre et 0,30m de haut ; parmi quelques pierres qui apparaissent à sa surface, l'une d'elles est particulièrement visible à la périphérie, au Sud.

Historique

Monument découvert en [mai 1973]{.date}.

### [Banca]{.spatial} - [Ichtauz]{.coverage} 1 - [Tumulus]{.type}

Localisation

À une trentaine de mètres au Nord Nord-Est de la BF 110.

Altitude : 970m.

Description

Tumulus pierreux de faible hauteur et de 3,60m de diamètre à la surface duquel apparaissent de nombreuses petites dalles de [grès]{.subject} rose ; pas de péristalithe visible. Ces deux derniers monuments, comme d'autres, sont ou à cheval sur la ligne frontière, ou très légèrement à l'Ouest de celle-ci, ce qui ne les empêche pas de faire partie intégrante de l'ensemble des vestiges protohistoriques d'Ichtauz ; c'est pourquoi nous les avons cités ici.

Historique

Monument découvert par [J. Blot]{.creator} en [avril 2010]{.date}.

### [Banca]{.spatial} - [Ichtauz]{.coverage} 2 - [Tumulus]{.type} [(?)]{.douteux}

Localisation

Au niveau de la BF 111.

Altitude : 960m.

Description

On note un très léger relief tumulaire circulaire de 3,50m de diamètre entourant la base de la borne frontière, fait de nombreuses petites pierres entièrement recouvertes de mousse. Il n'y a pas de péristalithe visible.

Historique

Monument découvert par [J. Blot]{.creator} en [avril 2010]{.date}.

### [Banca]{.spatial} - [Ichtauz]{.coverage} - [Tumulus-cromlech]{.type}

Localisation

À 5 mètres au Nord Nord-Est de la BF 110.

Altitude : 970m.

Description

On note un une quinzaine de pierres de dimensions variables, enfouies dans le sol qui délimitent un tumulus de faible hauteur et de 3m de diamètre environ.

Historique

Monument découvert par [J. Blot]{.creator} en [avril 2010]{.date}.

### [Banca]{.spatial} - [Ichtauzkolepoa]{.coverage} 2 - [Cromlech]{.type}

Localisation

Altitude : 1000m.

Il est situé à 3 mètres au Sud Sud-Est du *cromlech n° 1* découvert par J. Blot [@blotNouveauxVestigesMegalithiques1973a] et à une vingtaine de mètres au Sud-Est de la récente route. Ces deux monuments n'ont heureusement pas été touchés par les travaux routiers tout proches.

Description

Cinq pierres en [grès]{.subject} rose, au ras du sol, délimitent un cercle 2,50m de diamètre.

Historique

Monument découvert par [J. Blot]{.creator} en [avril 2010]{.date}.

### [Banca]{.spatial} - [Ichtauzkolepoa]{.coverage} 1 - [Monolithe]{.type}

Localisation

Altitude : 970m.

Au-dessus de la naissance d'un petit ruisseau et à quelques mètres à l'Est de la piste pastorale.

Description

Belle dalle de [grès]{.subject} rose rectangulaire, couchée au sol suivant un axe Sud-Ouest Nord-Est, mesurant 2,64m de long, 1,57m de large et 0,34m d'épaisseur. Bien que ne présentant pas de traces évidentes d'épannelage, sa situation auprès de points d'eau, de pistes pastorales, et de si abondants pâturages, rend fort probable son rôle de *Muga*.

Historique

Dalle découverte par [I. Gaztelu]{.creator} en [novembre 1997]{.date}.

### [Banca]{.spatial} - [Ichtauzkolepoa]{.coverage} 2 - [Monolithe]{.type}

Localisation

À 25 mètres environ au Nord-Est de *Ichtauzkolepoa 1 - Monolithe*.

Altitude : 975m.

Description

Belle dalle de [grès]{.subject} rose, couchée au sol selon un axe Nord-Sud, mesurant 2,40m de long, 0,78m de large, et 0,25m d'épaisseur. Peut-être y aurait-il quelques traces d'épannelage le long du côté Est. Il est difficile de savoir si une seule, ou les 2 dalles, ont joué (en même temps ?) le rôle de *Muga*.

Historique

Dalle découverte par [I. Gaztelu]{.creator} en [novembre 1997]{.date}.

### [Banca]{.spatial} - [Ichtauzkolepoa]{.coverage} - [Pierre couchée]{.type} [(?)]{.douteux}

Localisation

Elle est située plus bas que *Ichtauzkolepoa 1 - Monolithe*, à 10m au Sud du petit ruisseau.

Altitude : 960m.

Description

Bloc de [grès]{.subject} rose couché au sol selon un axe Est-Ouest, mesurant 1,80m de long 0,49m de large et 0,31m d'épaisseur. *Très douteux*.

Historique

Pierre découverte par [J. Blot]{.creator} en [avril 2010]{.date}.

### [Banca]{.spatial} - [Ichtauzkolepoa]{.coverage} - [Pierre plantée]{.type} [(?)]{.douteux}

Localisation

Elle se trouve au Nord de *Ichtauzkolepoa - Pierre couchée (?)*, de l'autre côté du ruisseau, sur le terrain en pente, et à 10m au Nord de la piste pastorale.

Altitude : 985m.

Description

Cette pierre de [grès]{.subject} rose, émerge du sol sur une hauteur de 0,94m, et mesure 0,56m à sa base visible et 0,26m d'épaisseur. Seule cette position dressée attire l'attention. *Très douteux* ; borne actuelle ?, effet du hasard ?

Historique

Pierre découverte par [J. Blot]{.creator} en [avril 2010]{.date}.

### [Banca]{.spatial} - [Ichtauzkolepoa]{.coverage} - [Pierre couché]{.type} [(?)]{.douteux}

Localisation

Altitude : 1000m.

Elle est située à une dizaines de mètres au Nord-Est de l'extrémité de la route qui aboutit au col.

Description

Dalle de [grès]{.subject} local, mesurant 1,50m de long, 0,50m de large, et approximativement 0,40m d'épaisseur - pourrait faire partie d'un filon rocheux en place.

Historique

Dalle trouvée par [Badiola P.]{.creator} en [octobre 2012]{.date}.

### [Banca]{.spatial} - [Ichtauzkolepoa]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 1010m.

Sur une petite hauteur dominant le col et l'arrivée de la route.

Description

Tumulus de terre mesurant environ 5m de diamètre et 0,30m de haut, à la surface duquel apparaissent quelques pierres, sans que l'on puisse parler de cromlech.

Historique

Monument découvert en [octobre 1972]{.date} par [J. Blot]{.creator}.

### [Banca]{.spatial} - [Laurigna]{.coverage} sud - [Tumulus]{.type}

Localisation

Carte 1346 Ouest Saint-Etienne-de-Baïgorry.

Altitude : 1150m

Monument faisant partie de ceux échelonnés sur la longue crête montagneuse, étendue dans la Vallée des Aldudes selon un axe Sud Sud-Est - Nord Nord-Ouest, du mont Lindus au Munhoa.

Situé à 100m environ au Nord Nord-Est de la BF 160, sur une petite éminence naturelle.

Description

Tumulus terreux, de 7m de diamètre et 0,40m de haut avec deux pierres bien visibles en secteur Nord et Nord-Ouest.

Historique

Monument découvert par [nous]{.creator} en [novembre 1974]{.date}. Nous le publions ici car il nous semble être distinct, au moins quant à la situation, du tumulus décrit par J.M. de Barandiaran sous le nom de *Traikarlepo ou Beai* (ref)(**12 ;** p.247, n°7).

### [Banca]{.spatial} - [Lepeder]{.coverage} - [Cromlech]{.type}

Localisation

Carte IGN 1346 Ouest Saint-Etienne-de-Baigorry

Altitude : 780m.

On le trouve à l'Est du col de Lepeder, et presque à l'extrémité de la croupe orientée Ouest-Est, née au sommet du mont Antchola. De ce site, on a une vue panoramique magnifique.

Description

Une quinzaine de petits blocs de [schiste]{.subject} ardoisier, la pierre locale, délimite un cercle de 3 de diamètre ; au centre se distingue un amas de ces mêmes pierres.

Historique

Monument découvert par [J. Blot]{.creator} en [avril 2010]{.date}.

### [Banca]{.spatial} - [Mehatzé]{.coverage} 4 - [Cromlech]{.type}

Localisation

Carte 1346 Ouest Saint-Etienne-de-Baïgorry.

Altitude : 1168m.

Il est situé dans un petit col au Sud Sud-Ouest du mont Méhatzé sur la ligne de crête, entre les bornes frontières 167 et 168. Le cromlech n° 4 est situé à 9m au Sud Sud-Est du n°1 [@blotNouveauxVestigesMegalithiques1972b]

Description

Cercle de 4m de diamètre délimité par 6 pierres, dont deux particulièrement visibles en secteur Est : l'une de 1,40m de long et 0,70m de large, l'autre de 1,50m x 1,30m.

Historique

Monument découvert en [septembre 1972]{.date}.

### [Banca]{.spatial} - [Méhatzé]{.coverage} 5 - [Cromlech]{.type}

Localisation

Carte 1346 Ouest Saint-Etienne-de-Baïgorry.

Monument faisant partie de ceux échelonnés sur la longue crête montagneuse, étendue dans la Vallée des Aldudes selon un axe Sud Sud-Est Nord Nord-Ouest, du mont Lindus au Munhoa. Il est situé à 23m au Nord du n° 3, et à 27m au Nord Nord-Est du n°2 [@blotNouveauxVestigesMegalithiques1972b].

Description

Cercle discret de 4m de diamètre délimité par 4 pierres. Nous avons dû pratiquer une fouille de sauvetage en juillet 1977 (ref)(Blot J, 1983, p. 191), qui a mis au jour une couronne périphérique en double assise de petits blocs de pierre disposée autour d'une [ciste]{.subject} centrale en forme de fer à cheval ouvert au Nord-Ouest. Le résultat de la datation au 14 C : (Gif 4470), mesure d'âge (BP) : 2730+- 100, soit en date calibrée : 1118-812 BC.

Historique

Monument découvert en [septembre 1974]{.date}.

### [Banca]{.spatial} - [Méhatzé]{.coverage} Sud 1 - [Tumulus]{.type}

Localisation

À 80m au Nord de la BF 166 et à 10m à l'Ouest de la clôture barbelée.

Description

Petit tumulus circulaire de 3m de diamètre et 0,30m de haut au sommet duquel apparaissent quelques pierres.

Historique

Monument signalé en [août 1989]{.date} par [Luis Millan San Emeterio]{.creator} (San Sebastian).

### [Banca]{.spatial} - [Méhatzé]{.coverage} Sud 2 - [Tumulus]{.type}

Localisation

À 26m à l'Ouest de *Méhatzé Sud 1 - Tumulus*, au voisinage immédiat de la piste pastorale.

Description

Tumulus bien visible de 6m de diamètre et 0,40m de haut environ, formé de blocs rocheux.

Historique

Monument signalé en [août 1989]{.date} par [Luis Millan San Emeterio]{.creator} (San Sebastian).

### [Banca]{.spatial} - [Minchondo]{.coverage} - [Tumulus Cromlech]{.type}

Localisation

Altitude : 796m.

Ce monument est construit dans l'ensellement d'une croupe, elle-même située au flanc Nord-Ouest du mont Adarza ; cette région est desservie par la route qui monte vers le col Urdiako Lepoa, et qui longe ainsi le flanc Sud de cette colline, à quelques dizaines de mètres au Sud de ce monument.

Description

Tumulus en forme de galette aplatie de 3 à 3,10m de diamètre et 0,40m de haut maximum. Une quinzaine de pierres, de volume modeste et plus ou moins au ras du sol - mais bien visibles - matérialisent un péristalithe.

Historique

Ce monument nous a été signalé par [P. Badiola]{.creator} en [mai 2016]{.date}.

### [Banca]{.spatial} - [Minchondo]{.coverage} - [Cromlech]{.type}

Localisation

Il est situé à 1m à l'Est de *Minchondo - Tumulus Cromlech*.

Description

Ce très probable cromlech possède un péristalithe incomplet - en particulier dans le secteur Nord - cependant 6 pierres bien visibles s'inscrivent dans un cercle de 3m de diamètre. L'une d'entre elles, en secteur Sud, atteint même 0,40m x 0,40m.

Historique

Monument découvert par [Blot J.]{.creator} en [mai 2016]{.date}.

### [Banca]{.spatial} - [Munhogaina]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 829m.

Petit tumulus situé à 3m à l'Est de la base du Munhogaine, sur un petit plateau se terminant lui-même, à 40m à l'Est de ce tumulus, par un amas rocheux important au flanc duquel se voient les restes d'une cabane de berger en pierres sèches.

Description

Petit tumulus de terre et quelques pierres, mesurant 2,50m à 3m de diamètre et 0,30m de haut.

Historique

Tumulus découvert par [Meyrat F.]{.creator} en [septembre 2014]{.date}.

### [Banca]{.spatial} - [Otsachar]{.coverage} 1 - [Tumulus]{.type}

Localisation

Altitude : 808m.

Ce monument se trouve à l'extrémité Sud du plateau sommital du mont Otsachar, et tangent, à l'Ouest, à la piste de crête ; il est à 2m au Sud d'un poste de tir à la palombe...

Description

Tumulus circulaire de 3,30m de diamètre, et 0,30m de haut, à la surface duquel apparaît une trentaine de blocs de [calcaire]{.subject} blanc, qui semblent aussi délimiter un péristalithe. Ce monument paraît en parfait état de conservation...

Historique

Tumulus découvert par [Meyrat F.]{.creator} en [novembre 2014]{.date}.

### [Banca]{.spatial} - [Otsachar]{.coverage} 2 - [Tumulus]{.type}

Localisation

Ce très modeste monument est tangent au Nord-Est de *Otsachar 1 - Tumulus* ; la piste de crête passe très exactement entre eux deux.

Description

Il mesure environ 2m de diamètre et se présente comme un relief peu visible, de 0,25m de haut, couvert de gazon, où seul un petit bloc de [calcaire]{.subject} blanc est visible en son centre.

Historique

Tumulus découvert par [Meyrat F.]{.creator} en [novembre 2014]{.date}.

### [Banca]{.spatial} - [Otsachar]{.coverage} 3 - [Tumulus]{.type}

Localisation

Altitude : 809m.

Il est à l'extrémité Nord du même plateau sommital d'Otsachar, à une quarantaine de mètres au Sud d'un amas rocheux bien visible.

Description

Tumulus herbeux circulaire de 4,60m de diamètre et de 0,25m à 0,30m de haut.

Un bon quart de sa partie Est a été piétinée par les animaux.

Historique

Monument identifié par [Blot J.]{.creator} en [décembre 2014]{.date}.

### [Banca]{.spatial} - [Otsachar]{.coverage} 4 - [Tumulus]{.type} [(?)]{.douteux}

Localisation

Altitude : 799m.

Situé dans une prairie sur un replat au Nord de *Otsachar 3 - Tumulus*, à quelques mètres de la clôture Est de cette prairie.

Description

Tumulus herbeux, dans l'ensemble circulaire, d'environ 3,30m de diamètre et 0,30m de haut. Monument douteux...

Historique

Tumulus découvert par[Meyrat F.]{.creator} en [novembre 2014]{.date}.

### [Banca]{.spatial} - [Pago Zelhay]{.coverage} - [Cromlech]{.type} [(?)]{.douteux}

Localisation

Altitude : 894m.

Il est à 50m au Sud de la BF 114.

Description

Une dizaine de pierres au ras du sol, délimitent un cercle (approximatif), de 2,80m de diamètre. Monument douteux ?.

Historique

Monument découvert par [J. Blot]{.creator} en [avril 2010]{.date}.

### [Banca]{.spatial} - [Pago Zelhay]{.coverage} - [Monolithe]{.type}

Localisation

Altitude : 900m.

Il est à 40 mètres au Nord-Est de la BF 114.

Description

Bloc de [grès]{.subject} [triasique]{.subject}, de forme grossièrement parallélépipédique, couché au sol suivant un axe Nord Nord-Ouest Sud Sud-Est. Il mesure 3m de long, 1,57m de large à sa base et 1,35m à son sommet. Son épaisseur est variable : maximum à la base : 0,60m et moindre au sommet en partie enfoui dans le sol, et donc difficilement appréciable. Il présente des traces d'épannelage sur ses bords Sud-Est et Sud-Ouest.

Historique

Monument découvert par [J. Blot]{.creator} en [avril 2010]{.date}.

### [Banca]{.spatial} - [Pago Zelhay]{.coverage} - [Tumulus pierreux]{.type} [(?)]{.douteux}

Localisation

Altitude : 905m.

Situé à quelques dizaines de mètres à l'Ouest de *Pago Zelhay - Monolithe*, et au Nord de la BF 114, se trouve un amas de dalles [triasiques]{.subject}, étalé sur une surface de 10m environ, et sur terrain en pente vers le Sud ; quelques dalles plantées et inclinées, au centre de l'amas pourraient évoquer (??) les restes d'une [chambre funéraire]{.subject}.

### [Banca]{.spatial} - [Turaor]{.coverage} 2 - [Cromlech]{.type}

Localisation

Carte 1346 Ouest Saint-Etienne-de-Baïgorry.

Altitude : 1181m.

Nous avons publié en 1972 [@blotNouveauxVestigesMegalithiques1972b p.202], le cromlech Turaor n°1, situé à 75m au Sud de la BF 163. Le cromlech Turaor n° 2 est situé sur le petit replat au sommet de Turaor, et à 30m au Nord de la BF 163.

Description

Cercle de 3m de diamètre délimité par 5 pierres.

Historique

Monument découvert en [juin 1979]{.date}.

### [Banca]{.spatial} - [Turaor]{.coverage} 3 - [Tumulus]{.type}

Localisation

Carte 1346 Ouest Saint-Etienne-de-Baïgorry.

Monument faisant partie de ceux échelonnés sur la longue crête montagneuse, étendue dans la Vallée des Aldudes selon un axe Sud Sud-Est Nord Nord-Ouest, du mont Lindus au Munhoa.

Ce troisième monument est à 120m au Sud du n°1 [@blotNouveauxVestigesMegalithiques1972b p.202] et à 2m au Sud d'un important pointement rocheux.

Description

Tumulus pierreux de 5m de diamètre et 0,60m de haut, bien délimité à sa périphérie.

Historique

Monument découvert en [septembre 1978]{.date}.

### [Banca]{.spatial} - [Turaor lepoa]{.coverage} 4 - [Cromlech]{.type}

Localisation

Carte 1346 Ouest Saint-Etienne-de-Baïgorry.

Monument faisant partie de ceux échelonnés sur la longue crête montagneuse, étendue dans la Vallée des Aldudes selon un axe Sud Sud-Est Nord Nord-Ouest, du mont Lindus au Munhoa.

Altitude : 1140m.

Dans la même publication, nous avons décrit [@blotNouveauxVestigesMegalithiques1972b p. 200], dans ce petit col 3 cromlechs (1-2-3).

Description

Petit cercle de 2m de diamètre délimité par une dizaine de blocs pierreux.

Historique

Monument découvert en [septembre 1978]{.date}.

### [Banca]{.spatial} - [Turaor lepoa]{.coverage} 5 - [Cromlech]{.type}

Localisation

Carte 1346 Ouest Saint-Etienne-de-Baïgorry.

Monument faisant partie de ceux échelonnés sur la longue crête montagneuse, étendue dans la Vallée des Aldudes selon un axe Sud Sud-Est Nord Nord-Ouest, du mont Lindus au Munhoa.

À 80m au Nord du n° 1 [@blotNouveauxVestigesMegalithiques1972b].

Description

Cromlech légèrement surélevé de 3,60m de diamètre, délimité par une douzaine de pierres au ras du sol.

Historique

Monument découvert en [juin 1979]{.date}.

### [Banca]{.spatial} - [Turaor lepoa]{.coverage} 6 - [Cromlech]{.type}

Localisation

Carte 1346 Ouest Saint-Etienne-de-Baïgorry.

Monument faisant partie de ceux échelonnés sur la longue crête montagneuse, étendue dans la Vallée des Aldudes selon un axe Sud Sud-Est Nord Nord-Ouest, du mont Lindus au Munhoa.

Il est à environ 16m au Nord Nord-Est de *Turaor 2 - Cromlech*.

Description

Cercle peu visible de 5m de diamètre environ.

Historique

Monument découvert par [nous]{.creator} en [septembre 1978]{.date}.

### [Banca]{.spatial} - [Turaor lepoa]{.coverage} 7 - [Tumulus]{.type}

Localisation

Carte 1346 Ouest Saint-Etienne-de-Baïgorry.

Monument faisant partie de ceux échelonnés sur la longue crête montagneuse, étendue dans la Vallée des Aldudes selon un axe Sud Sud-Est Nord Nord-Ouest, du mont Lindus au Munhoa.

Au Sud de *Turaor-lepoa 6 - Cromlech*, à environ 150m au Nord Nord-Est de la BF 162.

Description

Tumulus pierreux de 5m de diamètre et 0,40m de haut environ.

Historique

Monument découvert par [nous]{.creator} en [septembre 1978]{.date}.

### [Banca]{.spatial} - [Turaor]{.coverage} n°2 - [Cromlech]{.type}

Localisation

Carte 1346 Ouest Saint-Etienne-de-Baïgorry.

Monument faisant partie de ceux échelonnés sur la longue crête montagneuse, étendue dans la Vallée des Aldudes selon un axe Sud Sud-Est Nord Nord-Ouest, du mont Lindus au Munhoa.

À 30m au Nord de la BF 163, sur le replat du sommet.

Description

Environ 5 pierres pourraient délimiter un cercle de 3,20m de diamètre.

Historique

Monument découvert par nous en [septembre 1978]{.date}.

### [Banca]{.spatial} - [Turaor]{.coverage} n°5 - [Tumulus]{.type} 

Localisation

Carte 1346 Ouest Saint-Etienne-de-Baïgorry.

Monument faisant partie de ceux échelonnés sur la longue crête montagneuse, étendue dans la Vallée des Aldudes selon un axe Sud Sud-Est Nord Nord-Ouest, du mont Lindus au Munhoa.

Description

Tumulus pierreux, de 6m de diamètre et 0,40m de haut, à environ 280 *pas* au Sud de la BF 163.

Un monolithe nous avait été signalé en août 1989 par Luis Millan San Emeterio (San Sebastian), gisant entre les BF 163 et 164 que nous n'avons pas retrouvé.

Par contre nous signalerons un bloc rocheux, régulier qui a été très probablement amené là où il se trouve maintenant (mais depuis quand ?), à 9m au Nord Nord-Est de la BF 165, près des fils barbelés. Il s'agit d'un bloc parallélépipédique, sans inscription visible, mesurant 1,40m de long, 0,78m de largeur et 0,38m d'épaisseur.
