# Osses

### [Osses]{.spatial} - [Elguet]{.coverage} 1 - [Tumulus]{.type}

Localisation

Carte 1345 Ouest Cambo-les-Bains.

Altitude : 664m.

Il est situé au versant Sud-Ouest du mont Baygoura, à environ 1000m à vol d'oiseau au Sud du sommet du pic Haltzamendi, et à 250m à l'Est de la ligne de crête.

Description

Modeste tumulus herbeux de 5m de diamètre et 0,50m de haut ; la piste pastorale passe en son milieu.

Historique

Monument découvert en [janvier 1974]{.date}.

### [Osses]{.spatial} - [Elguet]{.coverage} 2 - [Tumulus]{.type}

Localisation

Il est pratiquement tangent au Sud-Est de *Elguet 1 - Tumulus*.

Description

Tumulus de terre de 5m de diamètre et 0,40m de haut.

Historique

Monument découvert en [janvier 1974]{.date}.

### [Osses]{.spatial} - [Horza]{.coverage} 3 - [Tumulus]{.type}

Localisation

Coordonnées.

Carte 1345 Ouest Cambo-les-Bains.

Altitude : 550m.

Sur une vaste croupe, il existe au versant méridional du Baygoura, [@barandiaranProspecionesExcavacionesPrehistoricas1962, p.16], un ensemble de cromlechs, que nous avons ensuite complété [@blotNouveauxVestigesMegalithiques1972c, p. 28].

Le tumulus n°3 est tangent, à l'Est, au *Tumulus Horza 2*.

Description

Petit tumulus mixte de 2,50m de diamètre, et 0,30m de haut, constitué de terre et de plaquettes schisteuses délitées comme le n°2.

Historique

Monument découvert en [septembre 1980]{.date}.

### [Osses]{.spatial} - [Horza]{.coverage} 5 - [Cromlech]{.type}

Localisation

Il est tangent au Sud du précédent.

Description

Cromlech légèrement tumulaire, de 4m de diamètre, délimité par 7 pierres au ras du sol ; 4 autres sont visibles au centre.

Historique

Monument découvert en [septembre 1980]{.date}.

### [Osses]{.spatial} - [Ibidia]{.coverage} - [Tumulus]{.type} ([?]{.douteux})

Localisation

Carte 1345 Ouest Cambo-les-Bains.

Altitude : 527m.

Du mont Baigoura se détache, au niveau du pic Laina, une piste pastorale, suivant une ligne de crête perpendiculaire à l'axe principal, et qui se dirige vers le Sud-Est. Le tumulus est au milieu d'un petit col, à la cote 527m, à 10m à l'Ouest d'une bergerie démolie.

Description

Tumulus pierreux de 5m de diamètre et 0,50m de haut ; monument douteux ?

Historique

Monument découvert en [mars 1973]{.date}.

### [Osses]{.spatial} - [Ibidia]{.coverage} 2 - [Tumulus]{.type}

Localisation

Altitude 545m.

Situé en bordure de la piste qui descend le long de la croupe de ce nom, sur le flanc Est du mont Baygoura, sur un terrain en légère pente vers l'Est. Situé à l'Ouest du Tumulus Ibidia 1 qui est dans le col sous-jacent.

Description

Tumulus pierreux discret de 4m à 5m de diamètre et quelques centimètres de haut.

Historique

Monument découvert par [Meyrat F.]{.creator} en [février 2013]{.date}.

### [Osses]{.spatial} - [Iriapixta]{.coverage} - [Tertre d'habitat]{.type} ou [Tumulus]{.type} ([?]{.douteux})

Localisation

Altitude : 324m.

Il est situé au flanc Nord-Ouest de la colline dénommée Iriapixta.

Description

Très imposant tertre terreux ovale, mesurant 24m de long, 18m de large et 2,50m de haut environ, érigé sur un terrain en très légère pente vers le Nord. Un point d'eau se trouve à environ 250m au Nord-Ouest, près de l'ancienne borde. Il est difficile de préciser l'époque de construction et la finalité d'un tertre de cette importance ; nous n'en connaissons que 3 autres ayant aussi des dimensions «hors normes» mais moindre : les 2 tertres d'Hurlastère (Lecumberri) [@blotInventaireMonumentsProtohistoriques2014], et celui d'Orkhatzolhatz (Sainte-Engrace) [@blotInventaireMonumentsProtohistoriques2015, p.15] ; ils nous posent eux-aussi les mêmes problèmes diagnostiques...

Historique

Monument découvert par [F. Meyrat]{.creator} en [août 2015]{.date}.

### [Osses]{.spatial} - [Laina]{.coverage} - [Dalle couchée]{.type}

Localisation

Altitude : 821m.

Se trouve juste en bas de la pente Sud du Mont Baygoura, en bordure de la ligne de crête et de la rupture de pente côté Est.

Description

Dalle couchée de calcaire local, de forme rectangulaire, à grand axe Est-Ouest, mesurant 1,68m de long, 1,15m de large et de 0,24m d'épaisseur en moyenne.

Elle présente de très nettes traces d'épannelage sur tous les côtés (surtout au Sud), sauf au Nord.

Historique

Dalle trouvée par [Blot J.]{.creator} en [février 2013]{.date}.

### [Osses]{.spatial} - [Laina]{.coverage} 1 - [Tumulus-cromlech]{.type}

Localisation

Carte 1345 Ouest Cambo-les-Bains.

Altitude : 850m.

Il est visible juste après un important amas rocheux, sur un replat quand on descend du sommet du Baïgoura vers le Nord. Il est sur la ligne de crête, tout au bord du versant Est, abrupt.

Description

Tumulus ovale (diamètres : 6,50m x 4,50m), et de 0,90m de haut, marqué par 13 blocs périphériques de [quartzite]{.subject} local, avec, au centre, un petit amas pierreux.

Historique

Monument découvert en [février 1974]{.date}.

### [Osses]{.spatial} - [Laina]{.coverage} 2 - [Tumulus]{.type}

Localisation

Il est situé à 100m au Sud de *Laina 1 - Tumulus-cromlech*, et un peu plus en altitude, à 869m, à l'extrémité Est du sommet plat de Laina.

Description

Tumulus mixte où prédominent les petits blocs et les plaquettes de [schiste]{.subject}, mobiles, souvent délités ; le sommet est aplati avec légère dépression centrale. Il mesure 5m de diamètre et 0,80m de haut.

Historique

Monument découvert en [février 1974]{.date}.

### [Osses]{.spatial} - [Laina]{.coverage} 3 - [Cromlech]{.type}

Localisation

Carte 1345 Ouest Cambo-les-Bains.

On le trouve à 400m au Sud Sud-Ouest de *Laina 2 - Tumulus*.

Altitude : 780m.

Il est à l'amorce d'un petit replat, au flanc Sud Sud-Ouest du pic Laina.

Description

Cercle de 5m de diamètre, délimité par 11 pierres ; de nombreuses petites autres sont visibles à l'intérieur qui est légèrement surélevé.

Historique

Monument découvert en [février 1974]{.date}.

### [Osses]{.spatial} - [Laina]{.coverage} 4 - [Tumulus]{.type}

Localisation

Altitude : 869m.

Il est au bord de la rupture de pente (à l'Est), à quelques dizaines de mètres au Sud de *Laina 1 - Tumulus-cromlech*

Description

Tumulus de terre de 9m de diamètre et 1m de haut environ.

Historique

Monument découvert par [Blot J.]{.creator} en [février 2013]{.date}.

### [Osses]{.spatial} - [Laina]{.coverage} 5 - [Cromlech]{.type}

Localisation

Altitude : 780m.

Il se trouve à 4,60m au Sud de *Laina 3 - Cromlech*.

Description

Cercle délimité par une douzaine de pierres au ras du sol, plus nombreuses dans la moitié Sud (1 seule au Nord). Il semble qu'on puisse distinguer une ébauche de cercle intérieur formé de 5 pierres, plus 1 pierre centrale.

Historique

Monument découvert par [Meyrat F.]{.creator} en [février 2013]{.date}.

### [Osses]{.spatial} - [Orgueletegui]{.coverage} 3 - [Cromlech]{.type}

Localisation

Altitude : 740m.

Il est situé très exactement entre les deux cromlechs cités par JM de Barandiaran [@barandiaranContribucionEstudioCromlechs1949, p.199], soit donc à 1,20m à l'Est du cromlech n°1 et à 4m à l'Ouest du cromlech n°2.

Description

Quatre pierres (dont une au Sud-Ouest mesure 0,70m x 0,30m) délimitent un petit cercle de 2,10m de diamètre ; on en note une cinquième, à l'intérieur, dans le quart Sud-Est.

Historique

Ce monument trouvé par [Blot J.]{.creator} en [1974]{.date}.

### [Osses]{.spatial} - [Orgueletegui]{.coverage} - Pierre couchée

Localisation

Elle est située à 3,70m au Sud-Est de *Orgueletegui 3 - Cromlech*, sur un terrain en pente vers le Sud.

Description

Dalle de [grés]{.subject} parallélépipédique de 1,80m de long, 0,70m de large, et 0,40m d'épaisseur en moyenne, allongée selon un axe Nord-Est Sud-Ouest ; elle présente, semble-t-il des traces d'épannelage sur le bord de son extrémité Nord-Est.

Historique

Pierre couchée trouvée par [Meyrat F.]{.creator} en [février 2013]{.date}.

## Horza : les cromlechs d'Horza

La première publication de 5 cromlechs à Horza est faite par J.M. de Barandiaran [@barandiaranProspecionesExcavacionesPrehistoricas1962] ; nous avons ensuite publié [@blotNouveauxVestigesMegalithiques1972c, p.18] un cromlech supplémentaire, puis en 2008 [@blotInventaireMonumentsProtohistoriques2009, p.46] un tumulus et un autre cromlech. Lors d'une nouvelle visite sur les lieux en mai 2012, puis à nouveau en mai 2013, nous avons été dans l'impossibilité de retrouver les monuments par nous décrits ou par J.M. de Barandiaran, sauf le n°1 de ce dernier. La végétation très drue, les intempéries... sont peut-être responsables de cet état de chose. Nous avons donc été amenés à partir du n°1, à rechercher ce qui pouvait correspondre aux publications antérieures : mais quand la distance entre 2 monuments correspondait, c'est le diamètre ou l'orientation qui différaient. Nous avons donc été contraints, pour éviter les confusions, de donner des lettres au lieu de numéros aux monuments que nous réussissions à identifier comme tels, puisque seul le n°1 de J.M. Barandiran est bien retrouvé : ce sera le cromlech A.

Par contre un nouveau cromlech a été trouvé un peu plus au Sud : *Cromlech Horza Sud F*.

### [Osses]{.spatial} - [Horza]{.coverage} - [Cromlech]{.type} A

Localisation

Sur le replat d'Horza, à l'extrémité Sud du mont Baigura.

Altitude : 520m.

Il est de suite visible à droite de la piste qui parcours ce replat du Nord au Sud.

Description

D'un diamètre de 5,20m et en très léger relief, il est marqué par un grand nombre de petites pierres périphériques, ainsi qu'en l'intérieur.

Historique

Monument décrit en [1962]{.date} par [J.M. de Barandiaran]{.creator}.

### [Osses]{.spatial} - [Horza]{.coverage} - [Cromlech]{.type} B

Localisation

Il est à 5m au Sud-Ouest de *Horza - Cromlech A* (et non au Sud-Est).

Description

Mesure 3,20m de diamètre (et non 5m) et possède une douzaine de pierres en périphérie (et non 5).

Historique

Monument découvert par [Meyrat F.]{.creator} et [Blot J.]{.creator} en [mai 2013]{.date}.

### [Osses]{.spatial} - [Horza]{.coverage} - [Cromlech]{.type} C

Localisation

Situé à 22m au Sud de *Horza - Cromlech B*.

Description

Il présente un aspect nettement tumulaire (0,30m de haut) et mesure 2,70m de diamètre possédant une dizaine de pierres périphériques.

Historique

Monument découvert par [Meyrat F.]{.creator} et [Blot J.]{.creator} en [mai 2013]{.date}.

### [Osses]{.spatial} - [Horza]{.coverage} - [Cromlech]{.type} D

Localisation

À 12m au Sud de *Horza - Cromlech C*.

Description

Mesure 3m de diamètre avec 7 pierres périphériques.

Historique

Monument découvert par [Meyrat F.]{.creator} et [Blot J.]{.creator} en [mai 2013]{.date}.

### [Osses]{.spatial} - [Horza]{.coverage} - [Cromlech]{.type} E

Localisation

Il est à 2m au Sud de *Horza - Cromlech D*.

Description

Cercle de 6m de diamètre délimité par une quinzaine de pierres.

Historique

Monument découvert par [Meyrat F.]{.creator} et [Blot J.]{.creator} en [mai 2013]{.date}.

### [Osses]{.spatial} - [Horza]{.coverage} Sud - [Cromlech]{.type} F

Localisation

Altitude : 519m.

Il est à 3m à l'Ouest de la piste pastorale.

Description

Petit cercle de 3,20m de diamètre délimité par 8 pierres au ras du sol environ.

Historique

Monument découvert par découvert par [Blot J.]{.creator} en [mai 2013]{.date}.
