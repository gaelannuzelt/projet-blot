# Sare

### [Sare]{.spatial} - [Altsaan]{.coverage} 1 - [Cromlech]{.type} ([?]{.douteux})

Localisation

Altitude : 520m.

Description

Un cercle de 3m de diamètre paraît délimité par un ensemble de dalles horizontales, de dimensions variables, avec peut-être, au centre, une [ciste]{.subject} formée de 5 autres dalles, dont une plus importante, au ras du sol ou à l'horizontale.

Historique

Monument découvert par [J. Blot]{.creator} en [mai 2011]{.date}.

### [Sare]{.spatial} - [Altsaan]{.coverage} 2 - [Dalle couchée]{.type}

Localisation

Altitude : 430m.

Description

Dalle de [grés]{.subject} de forme oblongue dont le grand axe Est-Ouest mesure 2,77m ; sa largeur maximum est de 0,90m et son épaisseur de 0,25m à 0,30m en moyenne. Quelques traces d'épannelage semblent pouvoir être notées aux bords Sud, Nord, et à son extrémité Ouest.

Historique

Dalle vue par [F. Meyrat]{.creator} en [2011]{.date} et, peut-être avant lui, par [I. Txintxuretta]{.creator}.

### [Sare]{.spatial} - [Altsaan]{.coverage} 3 - [Dolmen]{.type}

Localisation

Carte 1245 Est Espelette.

Altitude : 500m.

JM de Barandiaran [@barandiaranCatalogueStationsPrehistoriques1946 p. 24] avait publié le dolmen n°1 (Alt 528m), et nous même le n°2 [@blotNouveauxVestigesMegalithiques1971 p.26], sous le nom d'Altsaan Est. Tous les monuments suivants sont échelonnés le long de la crête rocheuse de ce nom qui descend vers le Sud-Est.

Altsaan 3 est situé à 15m au Sud du n°2.

Description

Petit tumulus pierreux, fait de fragments de dalles de [grès],{.subject} d'environ 5m de diamètre et 0,60m de haut. La [chambre funéraire]{.subject}, quadrangulaire, orientée Ouest-Est, mesure 2,10m de long, 1,10m de large et 0,40m de profondeur ; elle est délimitée par de petites dalles de [grès rose]{.subject} local. Au Sud : 3 dalles dans le prolongement les unes des autres, mais non jointives, ayant les mêmes dimensions : 0,50m de long et 0,40m de haut. La paroi Nord est constituée de 2 dalles de mêmes dimensions que les précédentes, plus ou moins inclinées vers le sol ; il en est de même pour les parois Est et Ouest. Il n'y a pas de couvercle visible.

Historique

Dolmen découvert en [octobre 1977]{.date}.

### [Sare]{.spatial} - [Altsaan]{.coverage} 4 - [Dolmen]{.type}

Localisation

Carte 1245 Est Espelette.

Altitude : 450m.

On trouve ce monument en continuant à descendre la crête rocheuse vers l'Est, à environ 300m au Sud-Est du précédent ; il est érigé dans le dernier petit col, près d'une échancrure qui fait communiquer les deux versants de cette crête.

Description

Tumulus pierreux de 7m de diamètre. La [chambre funéraire]{.subject} n'est pas centrale, mais dans le quadrant Nord-Est, peut-être du fait que le tumulus pourrait avoir été remanié par la construction d'un abri pastoral dans le secteur Sud. Cette chambre, orientée Est-Ouest, mesure approximativement 2m de long et 1m de large. À l'Ouest, une dalle de chevet reste plantée et verticale de même qu'une autre au Sud ; 3 autres dalles, couchées, complètent les parois Sud, Est et Nord.

Historique

Dolmen découvert en [octobre 1977]{.date}.

### [Sare]{.spatial} - [Altsaan]{.coverage} 5 - [Dolmen]{.type}

Localisation

Carte 1245 OT Hendaye St-Jean-de-Luz.

Altitude : 527m.

Monument situé, sur un terrain en très légère pente vers l'Est.

Description

Cinq dalles de grés enfoncées dans le sol délimitent une [chambre funéraire]{.subject} d'environ 1,80m de long et 0,80m de large, orientée au Sud-Est.

La dalle, au Nord-Ouest, que l'on peut considérer comme étant celle du chevet, mesure 0,75m de long et 0,35mde haut ; les deux dalles limitant la chambre à l'Ouest, mesurent, la première : 0,54m de long et 0,25m de haut, la seconde : 0,50m de long et 0,55m de haut. On note au Sud-Est qu'une des dalles atteint 0,70 m de haut et 0,45m à sa base.

Tumulus avec de nombreuses dalles ou fragments de dalles, de faible hauteur et d'environ 5m à 6m de diamètre.

Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2009]{.date}.

### [Sare]{.spatial} - [Altsaan]{.coverage} 6 - [Dolmen]{.type} ([?]{.douteux})

Localisation

Carte 1245 OT Hendaye St-Jean-de-Luz.

Altitude : 522m.

Il est situé à environ 100m à l'Est de la voie ferrée du petit train de la Rhune, au début de la crête rocheuse d'Alsaan qui s'étend vers l'Ouest.

Description

Monument de forme approximativement circulaire (3m de diamètre environ), constitué de larges dalles, les unes profondément enfoncées dans le sol, les autres horizontales, en désordre apparent à sa surface. À l'évidence, il s'agit d'une construction faite de main d'homme, mais dont la finalité reste difficile à préciser dans son état actuel ; dolmen ?

Historique

Construction découverte par [Blot J.]{.creator} en [octobre 2009]{.date}.

### [Sare]{.spatial} - [Altsaan]{.coverage} 7 - [Dolmen]{.type}

Localisation

Carte 1245 OT Hendaye St-Jean-de-Luz.

Altitude : 425m.

Description

La chambre funéraire, large de 0,88m, est orientée Nord-Sud, et se trouve dans un tumulus pierreux d'environ 5m de diamètre ; elle est délimitée par deux dalles parallèles : la dalle Ouest mesure 1,30m de long, 0,33m de large ; elle est prolongée par un deuxième fragment de 0,50m de long, résultat d'une probable fracture. La dalle Est mesure 0,69m de long, et 0,30 d'épaisseur.

Historique

Monument découvert par [J. Blot]{.creator} en [mars 2010]{.date}.

### [Sare]{.spatial} - [Altsaan]{.coverage} 8 - [Dolmen]{.type} ([?]{.douteux})

Localisation

Altitude : 525m.

Description

Trois dalles, dont on ne voit que les sommets semblent délimiter une [chambre funéraire]{.subject} orientée Sud-Est Nord-Ouest, d'environ 2m de long et 1m de large. Les deux dalles au Nord-Est mesurent respectivement 1,30m de long et 0,35m de long ; celle au Sud-Ouest mesure 0,70m de long. Il semble qu'il y ait un léger relief tumulaire, sur lequel quelques dalles horizontales sont visibles. Monument très douteux.

Historique

Monument découvert par [J. Blot]{.creator} en [mars 2010]{.date}.

### [Sare]{.spatial} - [Altsaan]{.coverage} 9 - [Dolmen]{.type}

Localisation

Il est très exactement tangent au Nord-Ouest du *dolmen Altsaan n°2*.

Description

Tumulus pierreux d'environ 5m à 6m de diamètre, au milieu duquel apparaît une importante [chambre funéraire]{.subject} orientée Sud-Ouest Nord-Est, mesurant environ 3m de long et 1,30m de large. Elle est constituée d'une dizaine de dalles dont la position et l'ordonnance ont été plus ou moins modifiées avec le temps. On peut toutefois signaler que la paroi Sud-Ouest est formée d'une dalle de 0,87m de long et 0,52m de haut, la dalle formant la paroi Nord-Est, en grande partie basculée vers l'extérieur, mesure 1,10m de long et 0,73m de large.

Historique

Monument découvert en [mars 2010]{.date} par notre ami [F. Meyrat]{.creator}.

### [Sare]{.spatial} - [Altsaan]{.coverage} 10 - [Dolmen]{.type}

Localisation

Altitude : 405m.

Monument fort difficile à trouver. Le mieux est de partir du petit col situé à 430 m d'altitude, où se trouve Altsaan 7 - Dolmen, un monolithe, ainsi que 2 dalles couchées. De ce col, se diriger vers le Nord-Est, une centaine de mètres plus loin et plus bas. Un long filon rocheux descendant lui aussi vers le Nord-Est, fait d'empilement de dalles épaisses, sur environ 80m de long, en arrêtant la solifluxion des terres en amont, a déterminé, à son extrémité supérieure Sud-Ouest, la formation d'un pseudo-replat, d'une vingtaine de mètres de large. C'est sur ce dernier, qui forme lui aussi un angle d'environ 30° par rapport à l'horizontale que le dolmen est érigé, ce qui reste tout à fait exceptionnel.

Description

On note un tumulus très érodé et remanié, de 7 à 8m de diamètre et 0,70m de haut environ ; il est fait de petites dalles disposées en «écaille de poisson» de façon très régulière, en particulier dans son secteur la mieux conservé, au Sud-Ouest. Dans la région centrale, apparaissent deux fragments de dalles, verticaux et parallèles qui peuvent matérialiser une [chambre funéraire]{.subject} de 1,50m de long et 1m de large, à grand axe orienté Sud-Est Nord-Ouest. Dans le secteur Sud-Est de ce tumulus, il semble que certaines dalles puissent être attribuées à un péristalithe ; sur son flanc Nord-Ouest, à 2,80m de la [chambre funéraire]{.subject}, gît une grande dalle parallélépipédique, de 2,60m de long, 1,50 de large et d'une épaisseur variant de 0,14 à 0,30m ; il semble qu'on puisse la considérer comme la [table de couverture]{.subject}, de même que, peut-être, deux autre fragments situés au flanc Nord-Est du tumulus et mesurant respectivement 0,80m x 0,74m et 1,02m x 0,82m.

Historique

Monument découvert par [I. Txintxuretta]{.creator} en [mars 2011]{.date}.

### [Sare]{.spatial} - [Altsaan]{.coverage} 11 - [Dolmen]{.type}

Localisation

Altitude : 512m.

Il se trouve sur la longue crête d'Altsaan.

Description

Dolmen construit sur un terrain en légère pente vers l'Est. La [chambre funéraire]{.subject} qui mesure environ 2m de long et 1,50m de large, est orientée Nord-Sud... dans la mesure où nous ne confondons pas longueur et largeur, compte tenu de l'état de délabrement du monument !!

La dalle la plus importante est à l'Ouest. Elle est verticale, enfouie au ras du sol, mais sa face interne, dans la [chambre funéraire]{.subject}, est dégagée ; elle mesure 1,76m de long, 0,13m d'épaisseur et sa hauteur, (dans la chambre), est de 0,55m. - La dalle Sud, qui pourrait correspondre à la dalle de chevet, lui est perpendiculaire, verticale ; elle mesure 1,83m de long à sa base, 1,03m dans sa plus grande hauteur, et 0,18m d'épaisseur. Les autres parois ne sont pas identifiables.

Un tumulus pierreux constitué de fragments de dalles, entoure la chambre ; sa hauteur est plus marquée à L'Est (0,90m de haut) qu'à l'Ouest.

Historique

Monument découvert par [P. Velche]{.creator} en [juillet 2011]{.date}.

### [Sare]{.spatial} - [Altsaan]{.coverage} 12 - [Dolmen]{.type}

Localisation

Altitude : 510m.

Il est quasi tangent au Sud de la piste pastorale.

Description

On ne distingue pas de tumulus, mais une très probable [chambre funéraire]{.subject}, orientée Est-Ouest., mesurant 2,10m de long et 1,50m de large environ. Parmi les nombreuses dalles du site, il semble que la paroi Ouest de la chambre soit représentée par une dalle inclinée, de 1,20m de long et 0,10m de haut environ ; la paroi Sud est constituée de deux dalles verticales et plantées dans le sol, mesurant chacune 1,10m de long et huit centimètre d'épaisseur - Enfin au Nord, on voit deux autres dalles, dont l'une, verticale, épaisse de 0,16m, longue de 0,56m et haute de 0,50m est, elle aussi, bien plantée dans le sol.

Historique

Monument découvert par [Meyrat F.]{.creator} en [octobre 2011]{.date}.

### [Sare]{.spatial} - [Altsaan]{.coverage} 13 - [Dolmen]{.type} (ou [dalle couchée]{.type} ?)

Localisation

Altitude : 530m.

Description

Cette dalle de grés [triasique]{.subject}, de forme grossièrement parallélépipédique, est allongée suivant un grand axe Nord-Est Sud-Ouest Sud-Est. Elle mesure 3,56m de long et 2,84m de large pour une épaisseur moyenne de 0,33m ; elle paraît présenter des traces d'épannelage sur toute sa périphérie. Ce qui est remarquable, c'est la présence, le long de son bord Nord-Ouest, du sommet de 3 dalles, profondément enfoncées dans le sol ; il pourrait s'agir des montants d'une [chambre dolménique]{.subject}, dont les autres seraient cachés sous cette dalle (de couverture...).

Historique

Monument découvert par [Meyrat F.]{.creator} en [Janvier 2012]{.date}.

### [Sare]{.spatial} - [Altsaan]{.coverage} 14 - [Dolmen]{.type} ([?]{.douteux})

Localisation

Altitude : 530m.

Description

Un ensemble de 5 dalles semble délimiter une chambre funéraire dolménique (ou une ciste), allongée suivant un axe Nord-Est Sud-Ouest, mesurant 0,95m de long et 0,81 de large.

La paroi Sud-Est est représentée par 2 dalles plantées dans le sol, mesurant 0,80m de long, 0,50m de haut pour l'une et 1,16m de long et 0,45m de haut pour la seconde. La paroi Nord-Ouest est formée de 2 autres dalles, dont la plus longue fait 1,06m de long et 0,68m de haut. Une seule petite dalle, elle aussi plantée dans le sol fermerait la chambre au Nord-Est. La paroi Sud-Ouest est représentée par un bloc de [grés]{.subject} de 0,82m de large sur lequel repose une autre importante dalle qui pourrait être la [dalle de couverture].[chambre funéraire]{.subject} Monument douteux.

Historique

{.date}Monument découvert par [Meyrat F.]{.creator} en [janvier 2012]{.date}.

### [Sare]{.spatial} - [Altsaan]{.coverage} - [Cromlech]{.type} ([?]{.douteux})

Localisation

Altitude : 415 m.

Il se trouve dans le dernier petit col, à l'extrémité Est de la crête rocheuse d'Altsaan.

Description

On distingue nettement un grand cercle de 7m de diamètre, délimité par une trentaine de pierres, sur sol plat, contigu à une murette de pierres sèches qui empiète légèrement sur son secteur Sud-Ouest.

Historique

Monument découvert par [J. Blot]{.creator} en [mars 2010]{.date}.

### [Sare]{.spatial} - [Altsaan]{.coverage} - Grande [dalle couchée]{.type} ([?]{.douteux})

Localisation

Il est situé dans le même col que *Altsaan 7 - Dolmen*, à une vingtaine de mètres à l'Est Sud-Est du *Dolmen n°4*.

Description

Grande dalle couchée de [grés]{.subject} gris, de forme triangulaire, à sommet Nord. Elle mesure 4,20m de long, 2,24m dans sa plus grande largeur, et 0,44m d'épaisseur en moyenne. Il n'y a aucune trace d'épannelage.

Historique

Découvert par [I. Txintxuretta]{.creator} en [2009]{.date}.

### [Sare]{.spatial} - [Altsaan]{.coverage} - [Monolithe]{.type}

Localisation

Altitude : 430m.

Il est à une cinquantaine de mètres au Nord du petit col où se trouve *Altsaan 7 - Dolmen*, le long de la piste ascendante vers le Nord.

Description

Belle dalle de [grés]{.subject} gris, grossièrement rectangulaire couchée au sol, mesurant 2,39m de long, 1,66m de large et une vingtaine de centimètres d'épaisseur. Il est particulièrement remarquable de noter que tout le pourtour de cette dalle porte des traces évidentes d'épannelage.

Historique

Monument découvert par [J. Blot]{.creator} en [mars 2010]{.date}.

### [Sare]{.spatial} - [Altsaan]{.coverage} - [Tumulus]{.type}

Localisation

Carte 1245 Est Espelette.

Altitude : 320m.

Il est situé sur un petit replat à 80m environ à l'Est de l'extrémité rocheuse de la crête d'Altsaan, et à 100m au Sud-Ouest d'une bergerie. Le terrain est en légère pente vers le Sud-Est.

Description

Tumulus mixte de terre et de pierres de 10m de diamètre et 0,20m de haut, avec une légère dépression en son centre.

Historique

Monument découvert en [septembre 1976]{.date}.

### [Sare]{.spatial} - [Altsaan]{.coverage} bis - [Tumulus]{.type}

Localisation

Carte 1245 OT Hendaye St Jean-de-Luz.

À 1,30m à l'Ouest de *Altsaan 1 - Tumulus-cromlech*.

Description

Ce monument se présente comme un tumulus pierreux de faible hauteur, mesurant 3m de diamètre, constitué de dalles de formes et dimensions variées, apparaissant en désordre à la surface du monument.

Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2009]{.date}.

### [Sare]{.spatial} - [Altsaan]{.coverage} 1 - [Tumulus-cromlech]{.type}

Localisation

Carte 1245 OT Hendaye St Jean-de-Luz.

Altitude : 515 m.

Ce monument se trouve sur un terrain en légère pente vers l'Ouest.

Description

On note un ensemble de dalles de dimensions variées, enfoncées dans le sol, délimitant un léger tumulus circulaire de 2,50m de diamètre, à la surface duquel apparaissent d'autre dalles.

Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2009]{.date}.

### [Sare]{.spatial} - [Altsaan]{.coverage} 2 - [Tumulus-cromlech]{.type}

Localisation

Carte 1245 OT Hendaye St Jean-de-Luz.

Altitude : 500m.

Ce monument est situé sur une petite éminence.

Description

Tumulus circulaire de 2,80m de diamètre et 0,20m de haut environ, dont une trentaine de pierres marquent la périphérie.

Historique

Monument découvert par [J. Blot]{.creator} en [mars 2010]{.date}.

### [Sare]{.spatial} - [Altsaan]{.coverage} 3 - [Tumulus-cromlech]{.type}

Localisation

Altitude : 520m.

On le trouve dans une sorte d'abri sous-roche.

Description

Tumulus de 3,20m de diamètre et 0,35m à 0,40m de haut, formé de terre et de pierres de dimensions variables. Le péristalithe est particulièrement bien individualisé, dans le secteur Est par une grande dalle verticale de 1,62m de long, 0,30m de haut et une quinzaine de centimètres d'épaisseur. La moitié Ouest est délimitée par un demi-cercle de pierres plus ou moins rectangulaires, presque jointives, profondément enfouies dans le sol, et dont certaines atteignent 0,45m x 0,35m.

Historique

Monument découvert par [J. Blot]{.creator} en [mai 2011]{.date}.

### [Sare]{.spatial} - [Ametzia]{.coverage} - [Cromlech]{.type}

Localisation

Altitude : 220m.

Il est situé au pied de la colline Ametzia, qui prolonge au Sud les crêtes de Faague, au bord de la piste antique, partie de Harotzegikoboeda et qui rejoint le col Kondediagalepoa. On le trouve sur un petit replat à 50m à l'Est du franchissement par la piste d'un petit ru qui descend d'Ametzia.

Description

Petit cercle de 2,50m de diamètre, délimité par une dizaine de pierres, au ras du sol, plus visibles et plus nombreuses en secteur Est.

Historique

Monument découvert par [nous]{.creator} en [mai 2009]{.date}.

### [Sare]{.spatial} - [Ametzia]{.coverage} 1 - [Tumulus]{.type}

Localisation

Carte 1245 OT Hendaye St Jean-de-Luz.

Altitude : 420m.

Il est situé à 3m à l'Ouest du *Dolmen Ametzia 1* déjà décrit par nous ([@lotNouveauxVestigesMegalithiques1971 p.4]

Description

Tumulus circulaire pierreux de 4m de diamètre et 0,30m de haut, dont les éléments les plus importants sont visibles dans sa moitié Ouest (comme disposés en demi-cercle ?). S'agit-il d'un tumulus dolménique ruiné ou d'un tumulus simple ?

Historique

Monument découvert par [J. Blot]{.creator} en [novembre 2009]{.date}.

### [Sare]{.spatial} - [Ametzia]{.coverage} 2 - [Tumulus]{.type}

Localisation

Carte 1245 OT Hendaye St Jean-de-Luz.

Altitude : 440m.

Il est situé sur la gauche de la piste qui monte du col au pied de la chapelle d'Olhain, au moment où elle arrive sur le plat.

Description

Tumulus circulaire terreux, d'environ 5m de diamètre et 0,60m de haut, dans lequel quelques pierres sont cependant bien visibles.

À 6m au Nord se voit une *pierre plantée*, bien connue des randonneurs, en bordure de piste. Elle mesure 0,95m de haut, 0,60m à sa base et 0,22m d'épaisseur. Nous ne la rattacherions pas à la catégorie des monuments protohistoriques.

Historique

Monument découvert par [I. Gaztelu]{.creator} en [mars 2002]{.date}.

### [Sare]{.spatial} - [Ameztia]{.coverage} 2 - [Dolmen]{.type}

Localisation

Altitude : 440m.

À environ 230m au Nord Nord-Ouest du dolmen d'Ameztia publié par nous en 1971 [@blotNouveauxVestigesMegalithiques1971 p. 14]. À partir du col d'Olhain, prendre le sentier ascendant direction Sud-Ouest. Sur la partie plane, en haut de la colline, au niveau d'un petit col, se trouve une dalle verticalement plantée dans le sol. Continuer 80 pas sur le sentier en direction Nord Nord-Est, le monument est visible tangent à la piste sur son côté gauche, un terrain très légèrement en pente.

Description

On distingue les restes d'un petit tumulus pierreux d'environ 5m de diamètre, constitué de blocs de [grés]{.subject} certains pouvant atteindre le volume d'un gros pavé, ou même plus. On ne distingue pas de péristalithe nettement défini. Une dalle en [grés]{.subject}, orientée Sud-Ouest Nord-Est, de 1,15m de long, 0,25m de large et 0,45 de haut, est profondément enfoncée dans le sol dans la partie Sud-Est de ce tumulus. À une trentaine de centimètres au Nord, on distingue au ras du sol deux dalles brisées, contiguës, dont l'ensemble atteint 0,40m, pour ce qui en est visible et 0,15 d'épaisseur. L'ensemble de ces éléments paraît bien pouvoir être considéré comme les vestiges d'une modeste chambre funéraire.

Historique

Monument découvert par [A. Martinez]{.creator} en [2000]{.date}. À noter qu'il ne ressemble en rien aux descriptions que fait J.M. de Barandiaran des dolmens *Arribeltz 1* et *2* qui devraient être dans les environs...([?]{.douteux}). Nous ne les avons jamais vus.

### [Sare]{.spatial} - [Aniotzbeherekoborda]{.coverage} - [Dalle plantée]{.type} ([?]{.douteux})

Localisation

Altitude : 260m.

Elle est sur un petit plateau dominant le dépôt de la gare du petit train, qui est à 500m à l'Est Sud-Est, la borde est à 600m au Nord-Est et la piste montant à la redoute passe à 60m au Sud-Est.

Description

Dalle de grès local plantée dans le sol, légèrement inclinée vers l'Est. Elle mesure 1,90m de long, de 0,20m à 0,30m d'épaisseur et 0,52m de haut. Elle présente des traces d'épannelage à sa périphérie. Pas de tumulus visible ni d'autres dalles. Vestige d'un dolmen ?

Historique

Dalle découverte par [Meyrat F.]{.creator} en [juin 2012]{.date}.

### [Sare]{.spatial} - [Airagarri]{.coverage} 2 - [Tumulus-cromlech]{.type}

Localisation

Carte 1245 Est Espelette.

Altitude : 540m.

Le cromlech n°1 a été décrit en 1949 [@barandiaranContribucionEstudioCromlechs1949 p.206] et 1953 [@barandiaranHombrePrehistoricoPais1953 p. 250 n°85].

Il est situé sur le plateau très remanié par l'homme, à environ 70m au Sud Sud-Est de l'ancienne redoute en pierres sèches, et à 25m à l'Est de la piste qui va de cette redoute au chemin Altsaan-Ihicelhaya.

Description

Petit tumulus mixte de terre et dallettes de [grés]{.subject} rose, mesurant 3,50m de diamètre et 0,20m de hauteur. Douze pierres délimitent la périphérie, dont une, très visible, de 0,40m de haut.

Historique

Monument découvert en [février 1974]{.date}.

### [Sare]{.spatial} - [Airagarri]{.coverage} 3 - [Cromlech]{.type}

Localisation

Sur le même plateau que *Airagarri 2 - Tumulus-cromlech*, et à 15m au Sud Sud-Ouest de *Airagarri 2 - Tumulus-cromlech*.

Description

Une dizaine de pierres délimitent un cercle de 2m de diamètre ; en secteur Ouest, l'une d'elles, mesurant 1m de long et 0,20m de haut est particulièrement visible.

Historique

Monument découvert en [février 1974]{.date}.

### [Sare]{.spatial} - [Airagarri]{.coverage} 4 - [Cromlech]{.type}

Localisation

Altitude : 530m.

Il est situé à une dizaine de mètres au Sud-Ouest de *Airagarri 3 - Cromlech*.

découvert par nous en février 1974, ainsi que le n°2 [@blotInventaireMonumentsProtohistoriques2009, chapitre 1]

Altitude : 530m

Description

À la périphérie d'un léger relief circulaire de 4,80mètres de diamètre, trois dalles situées dans sa moitié Nord-Ouest font partie d'un très probable péristalithe. Elles mesurent respectivement 0,50m, 0,90m et 0,80m de long ; au centre est visible une autre pierre.

Historique

Ce monument a été découvert par notre ami [F. Meyrat]{.creator} en [mars 2010]{.date}.

### [Sare]{.spatial} - [Argaïne]{.coverage} - [Monolithe]{.type}

Localisation

Altitude : 373m.

Il gît sur le sol, tangent à l'Ouest, au sentier GR 10, au niveau d'un vaste replat marqué par un beau pâturage.

Description

Il s'agit d'une dalle de [grés]{.subject} rose, de forme grossièrement parallélépipédique, à grand axe orienté Nord-Sud, mesurant 2,27m dans sa plus grande dimension, 1,12m de large et dont l'épaisseur varie, suivant les côtés, de 0,10m à 0,23m. Il semble que l'on puisse noter quelques traces d'épannelage à son bord Ouest. À quelques centimètres de son extrémité Nord, existe un bloc de grés triangulaire, mesurant 1,05m à sa base, qu'il est difficile de rattacher à la dalle précédente.

Par contre, tangente à l'Est, une dalle de [grés]{.subject}, elle aussi de forme approximativement triangulaire, mesurant 1,10m de long, 0,98m de large, et 0,11m d'épaisseur (apparente) à l'Ouest, pourrait être, semble-t-il, rattachée à la dalle principale. Nous pensons, comme l'inventeur, que cette dalle pourrait en constituer le sommet « en pointe » qui se serait brisé et retourné avant de toucher le sol - dans le cas d'un monolithe dressé - (dont la base Nord aurait été enfoncé dans le sol). Des irrégularités en forme de « cannelures » sont en effet retrouvées tant au bord Ouest de la grande dalle que sur le bord Nord-Ouest de ce fragment triangulaire, suggérant qu'il s'agit bien d'un même bloc brisé. Seul un dégagement complet de l'ensemble pourrait trancher le problème.

Historique

Monolithe découvert par [P. Velche]{.creator} en [août 2010]{.date}.

### [Sare]{.spatial} - [Argaïne]{.coverage} - [Tumulus]{.type}

Localisation

On le remarque à 12m au Sud-Est du monolithe précédent.

Description

Tumulus pierreux, en partie dissimulé par la végétation, de près de 6m de diamètre et 0,20m à 0,30m de haut.

Historique

Tumulus découvert par [J. Blot]{.creator} en [janvier 2011]{.date}.

### [Sare]{.spatial} - [Argaïne]{.coverage} 2 - [Dolmen]{.type}

Localisation

Carte 1245 Est Espelette.

Altitude : 330m.

Ce dolmen n'a aucun rapport avec le dolmen du même nom décrit par JM de Barandiaran au Sud Sud-Ouest des grottes de Sare. [@barandiaranCronicaPrehistoriaPirineo1951 p.240, n°267]. Il s'agit d'une simple homonymie.

*Argaine 2* est situé dans le petit vallon qui naît entre la crête d'Altsaan au Nord et Athekaleoun et Ourkilepoa au Sud. Il est visible à 100m au Sud Sud-Est de la borde Argaïne et il tangent au Nord d'un chemin rural ; il bénéficie d'une très belle vue dégagée à l'Est.

Description

Tumulus mixte de terre et de pierrailles, d'environ 11m de diamètre et 0,60m de haut. Une dépression centrale contient la [chambre funéraire]{.subject} orientée Sud-Est Nord-Ouest, et mesurant 2,50m de long, 1,50m de large et 1,15m de profondeur. 

Il ne reste que 2 dalles limitant cette chambre : Une dalle Nord mince, mesurant 1,80m de long, 1,15m de haut et une autre, à l'Ouest, pratiquement perpendiculaire à la précédente, de 2,40m de long, et 0,50m de haut, beaucoup plus épaisse (près de 0,40m) et profondément enfoncée dans le sol. Pas de couvercle visible.

Historique

Dolmen découvert en [septembre 1976]{.date}.

### [Sare]{.spatial} - [Arrosagaraykoborda]{.coverage} - [Monolithe]{.type}

Localisation

Altitude : 340m.

Situé à une soixantaine de mètres à l'Ouest de la borde du même nom.

Description

Dalle de [grés]{.subject} parallélépipédique, à sommet effilé, plantée perpendiculairement au sol. Elle mesure 1,70m de haut, 0,75m de large et 0,37m d'épaisseur à sa base. Elle présente des traces d'épannelage à son sommet et à son bord Est.

Historique

Monument découvert par [J.M. Lecuona]{.creator} en [2011]{.date}.

### [Sare]{.spatial} - [Arrosagaraykoborda]{.coverage} - [Dalle plantée]{.type}

Localisation

À 11m à l'Est Sud-Est de *Arrosagaraykoborda - Monolithe*.

Description

Dalle de [grés]{.subject} épaisse de 0,37m, 1,05m de large et 1,15m de haut, orientée suivant un axe Est-Ouest. Présente des traces évidentes d'épannelage sur son bord Est.

Historique

Dalle découverte par [Blot J.]{.creator} en [janvier 2012]{.date}.

### [Sare]{.spatial} - [Airagarri]{.coverage} 4 - [Cromlech]{.type}

Localisation

Altitude : 530m.

Il est à 2m au Sud-Est de la piste pastorale.

Description

Cercle assez irrégulier, (le diamètre varie entre 7,50 et 8m), délimité par 17 pierres environ de dimensions très variables. Il semble qu'il existe une légère surélévation centrale de 0,20m. À l'intérieur de ce monument apparaissent 4 pierres de dimensions notables (l'une atteint 0,80m de long) dont seule une fouille pourrait donner la signification éventuelle...

Historique

Ce monument découvert par [F. Meyrat]{.creator} en [2010]{.date} a de nouveau été étudié par ce même auteur en [2015]{.date}.

### [Sare]{.spatial} - [Airagarri]{.coverage} - [Monolithe]{.type}

Localisation

Altitude : 559m.

Ce monolithe se trouve à 3m au Sud Sud-Ouest du cromlech *Airagarri C1* (décrit par JM de Barandiaran) et à 9m à l'Ouest d'un gros bloc rocheux, repère très visible sur ce petit replat.

Description

Monolithe de forme générale parallélépipédique, couché au sol selon un axe Nord-Sud. Il mesure 2,30m de long, 1,20m de large à son extrémité Sud, 0,78m à son extrémité Nord, et 0,20m d'épaisseur en moyenne. Il présente des traces de régularisation sur ses bords Nord, Est et Ouest.

Historique

Monolithe découvert par [F. Meyrat]{.creator} en [janvier 2015]{.date}.

### [Sare]{.spatial} - [Arrondoa]{.coverage} 1 - [Dolmen]{.type} ([?]{.douteux})

Localisation

Altitude : 131m.

Ce monument ([?]{.douteux}) est inclus dans le mur de clôture de la maison Arrondoa. Toutefois les bords latéraux verticaux de la dalle sont restés libres, tout en restant jointifs aux autres éléments constitutifs de la murette, construite en 1951 (témoignage de Michel et Joseph Lasaga, propriétaires des lieux).

Description

Le « monument » se résume actuellement à une belle dalle de grès [triasique]{.subject}, orientée Nord-Ouest Sud-Est, enfoncée dans le sol, dont tout le pourtour montre des traces d'épannelage. Ses dimensions sont les suivantes : hauteur : 1,32m ; largeur au sommet : 1,09m ; largeur maximum : 1,31m ; épaisseur : 0,18m. Cette dalle pourrait très bien être la dalle de chevet restante de la chambre funéraire d'un dolmen orientée vers le Nord-Est, dont les autres montants auraient disparu. À signaler qu'une autre dalle de 3m de long, et 0,30m à 0,40m d'épaisseur gisait à 3 ou 4m en avant et au Nord-Est de la dalle ci-dessus décrite et aurait pu être un élément constitutif d'un dolmen, sa table de [couverture]{.subject} par exemple...

Ce vestige, et a été enlevé et brisé en plusieurs morceaux en 1967.

Historique

C'est l'historique de cette dalle qui attire l'attention : l'abbé [J.M. de Barandiaran]{.creator} l'avait vue dans les années 1940-1950, alors qu'elle n'était pas encore incluse dans la murette actuelle. Il avait alors dit au propriétaire des lieux (le père de Joseph et Michel), qu'on avait probablement à faire à une « tombe ». Nous signalons donc cet dalle, possible vestige d'un dolmen, pour être le plus exhaustif possible... Elle nous a été signalée par [Cl. Chauchat]{.creator} en [octobre 2015]{.date}.

### [Sare]{.spatial} - [Arrondoa]{.coverage} 2 - [Dolmen]{.type} ([?]{.douteux})

Localisation

Altitude : 139m.

À 80m environ au Nord Nord-Ouest de *Arrondoa 1 - Dolmen (?)*.

Description

On peut voir une construction très curieuse, incluse, là encore, dans une murette bordant un champ en pente assez marquée.

Il s'agit d'une sorte d'abri, ouvert vers l'Est, construit sur un sol pratiquement plat à cet endroit. Il est constitué d'une vaste dalle de [couverture]{.subject}, en grès [triasique]{.subject}, de forme grossièrement arrondie, d'environ 2,50m à 3m de diamètre et 0,55m d'épaisseur ; sa périphérie présente de nombreuses traces de retouche.

Elle repose sur deux blocs rocheux parallélépipédiques reposant sur le sol, non pas parallèlement, mais disposés de manière divergente formant un angle aigu à l'Ouest ; l'un, au Sud, mesure 1,20m de long environ, et 0,60m d'épaisseur ; l'autre au Nord, de 1,90m de long et 0,60m d'épaisseur. Au Sud, l'espace entre les deux blocs est fermé par un autre beaucoup plus modeste, de 0,60m de long.

Ces deux blocs et la dalle de [couverture]{.subject}, disposés par la main de l'homme, pourraient évoquer un abri ; mais le poids de la dalle de [couverture]{.subject}, disproportionné pour un simple abri peut aussi faire penser à un dolmen fort rustique quant à son architecture : le sol, constitué à cet endroit d'une roche plate, en place, aurait pu empêcher d'enfoncer les montants latéraux.

Historique

Cette construction nous a été signalée par Mr. [Michel Lasaga]{.creator}, qui l'a toujours connue ainsi.

### [Sare]{.spatial} - [Airagarri]{.coverage} 5 - [Cromlech]{.type} ([?]{.douteux})

Localisation

Situé à 70m au Sud Sud-Ouest de *Airagarri C4*.

Description

On distingue 4 dalles à au ras du sol, (3 d'entre elles mesurant 0,60m x 0,40m), qui semblent délimiter un demi-cercle de 5m de diamètre.

Historique

Monument découvert par [J. Blot]{.creator} en [mai 2011]{.date}.

### [Sare]{.spatial} - [Atermin]{.coverage} - [Ciste]{.type}

Localisation

Altitude : 330m.

Il est situé à une centaine de mètres et en contrebas au Nord-Ouest de la borde Arrosakogaraykoborda.

Description

À ne pas confondre avec le *Dolmen Atermin* [@barandiaranHombrePrehistoricoPais1953 p.240]. La chambre funéraire est orientée Est-Ouest, et mesure 1,45m de long et 0,55m de large. La paroi Sud-Ouest est délimitée par une dalle verticalement plantée de 1,43m de long, 0,14m d'épaisseur, et 0,40m de haut. Au Nord-Ouest, on note une dalle perpendiculaire à la précédente ; elle mesure 0,55m de long et 0,38m de haut. La paroi Nord-Est est délimitée par une série de 5 pierres au ras du sol ; deux petites dalles ferment la [chambre]{.subject} au Sud-Est. Enfin une dalle de 1m x 0,51m occupe le fond de la chambre. Cette dernière marque le centre d'un tumulus de 5m de diamètre environ, et 0,40m de haut, constitué de terre et de pierres ; l'existence d'un péristalithe est possible...

Historique

Découvert en [2011]{.date} par [I. Txintxuretta]{.creator} et [X. Taberna]{.creator} (sous le nom de *ciste Atermin*).

### [Sare]{.spatial} - [Athekaleun]{.coverage} - [Dalle plantée]{.type} ([?]{.douteux})

Localisation

Altitude : 550m.

Description

Cette dalle est verticalement plantée dans un sol incliné vers le Nord-Est, près d'un pierrier issu de la crête d'Athekaleun, où se trouve le monolithe d'Athekaleun, que nous avons publié en 1983. Elle mesure environ 0,90m de haut, 0,80m à sa base, et huit centimètres d'épaisseur en moyenne.

Historique

Dalle découverte par [I. Gaztelu]{.creator} en [janvier 1988]{.date}.

### [Sare]{.spatial} - [Athekaleun]{.coverage} 1 - [Dalle couchée]{.type}

Localisation

Altitude : 510m.

À partir de l'ensellement d'Athekaleun (altitude 530m), prendre un petit sentier qui descend vers le Nord-Ouest ; la dalle est sur le chemin.

Description

Cette dalle de [grés]{.subject} [triasique]{.subject} gît sur un sol en légère pente vers le Nord-Est ; elle paraît avoir glissé et repose dans sa partie Nord-Est sur quelques blocs et dalles de [grés]{.subject}, sans que l'on puisse y voir la moindre structure voulue. Elle mesure 3,50m dans son plus grand axe Nord-Ouest Sud-Est, 1,80m de large et de 0,10m à 0,25m d'épaisseur. Elle présente des traces d'épannelage, en particulier à son bord Nord-Est - s'agit-il de l'action de carriers « modernes » ?

Historique

Dalle trouvée par [Velche P.]{.creator} en [juillet 2011]{.date}.

### [Sare]{.spatial} - [Athekaleun]{.coverage} 2 - [Dalle couchée]{.type} ([?]{.douteux})

Localisation

Altitude : 470m.

Description

Dalle couchée sur l'ancien chemin qui se rend d'Urkila aux Trois Fontaines. De grès local rectangulaire elle mesure 1,02m de long, 0,50m de large et 0,19m d'épaisseur. Sa forme régulière et les nombreuses traces d'épannelage sur toute sa surface nous font penser à un travail de carriers récent.

Historique

Dalle découverte par [Badiola P.]{.creator} en [juin 2011]{.date}.

### [Sare]{.spatial} - [Atxurri]{.coverage} - [Dalle couchée]{.type}

Localisation

Altitude : 525m.

Cette dalle est incluse dans un ensemble de trois bordes en ruine ; elle est à 5m au Sud-Ouest de l'une d'elles, et à 30m au Sud-Est de la lisière du bois de conifères.

Description

Cette dalle forme actuellement une partie du «toit» d'une structure qui a pu servir d'abri pour animaux, mesurant 6m de long et 3 de large environ, pour 0,50m en moyenne de profondeur - creusée dans un terrain en pente vers le Nord-Est - et dont il reste des vestiges de parois en pierres sèches ou en dalles. La dalle de [grés]{.subject} [triasique]{.subject}, mesure 3,95m de long, 3,50m dans sa plus grande largeur, et 0,30m d'épaisseur. Son pourtour semble bien avoir été épannelé dans sa totalité.

Elle peut avoir été taillée pour son rôle actuel mais tout aussi être une borne antique récupérée depuis plus ou moins longtemps.

Historique

Dalle découverte par [Meyrat F.]{.creator} en [mars 2012]{.date}.

### [Sare]{.spatial} - [Atxurri]{.coverage} - [Dolmen]{.type}

Localisation

Altitude : 430m.

Il est très près de la ligne frontière, à l'Est d'un important bois de conifères, sur un promontoire relativement plat. Il est à 2m au Nord-Ouest d'une borne marquée d'un **B** gravé à sa partie haute.

Description

Un léger tumulus de 5m de diamètre marque le monument, dont la périphérie semble balisée de 5 pierres (4 au Sud-Ouest et 1 à l'Est). Une chambre funéraire centrale, de 2m x 0,68m orientée Nord-Est Sud-Ouest est délimitée par 8 dalles. La paroi Sud-Est est formée de trois dalles de 0,82m, 0,18m et 0,44m de long, alignées selon l'axe Nord-Est Sud-Ouest ; elles dépassent de peu la surface (0,18m pour la dernière).

Un autre alignement de 4 dalles forme la paroi Nord-Ouest ; elles mesurent respectivement 0,38m (et 0,15m de haut), 0,24m, encore 0,24m, et 0,50m (et 0,30m de haut). L'épaisseur moyenne de toutes ces dalles est de six à huit centimètres.

Historique

Monument découvert par [J.M. Lecuona]{.creator} en [février 2011]{.date}.

### [Sare]{.spatial} - [Bechinen Ardi Borda]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 265m.

Il est sur un replat à 55m à l'Est des ruines de Bechinenborda, et dominant, au Nord, la piste de transhumance antique qui se rend à Kondendiakolepoa.

Description

Tumulus érigé sur terrain plat, de terre et de pierres, mesurant environ 5m de diamètre et 0,40m de haut.

Historique

Tumulus découvert par [Badiola P.]{.creator} en [avril 2012]{.date}.

### [Sare]{.spatial} - [Bechinen Ardi Borda]{.coverage} 1 - [Cromlech]{.type}

Localisation

Il est à 3,50m au Nord de Bechinen Ardi Borda - Tumulus.

Description

7 pierres périphériques au ras du sol délimitent un cercle de 4m de diamètre ; au centre une pierre plus importante.

Historique

Monument découvert par [Badiola P.]{.creator} en [mai 2012]{.date}.

### [Sare]{.spatial} - [Bechinen Ardi Borda]{.coverage} 2 - [Cromlech]{.type} ([?]{.douteux})

Localisation

À 0,80m au Nord de *Bechinen Ardi Borda 1 - Cromlech*.

Description

5 pierres au ras du sol délimitent un cercle de 2,5m de diamètre (le quart Nord-Ouest en est démuni). Monument douteux.

Historique

Cercle découvert par [Badiola P.]{.creator} en [mai 2012]{.date}.

### [Sare]{.spatial} - [Etchegaraikoborda]{.coverage} 1 - [Dolmen]{.type}

Localisation

Carte 1245 OT Hendaye St Jean-de-Luz.

Altitude : 320m.

Ce monument est situé sur un replat d'une croupe issue du mont Faage, (ou Fago), orientée vers l'Est. Elle fait face à la colline de la chapelle d'Olhain, qui se situe au Sud.

Description

Monument très dégradé. Tumulus essentiellement constitué de blocs de [grés]{.subject} et de fragments de dalles, mesurant 5,50m de diamètre et 0,10m de haut. Au centre se voit une dalle de [grés]{.subject} plantée de 0,90m de long, 0,10m d'épaisseur et 0,20m de haut, orientée Ouest-Est. À 0,66m au Nord, on distingue le sommet d'une autre pierre (0,15m de long, et 0,10m de haut) qui semble bien être tout ce qui reste, avec la dalle précédente, d'une [chambre funéraire]{.subject} orientée plein Est ; au Nord-Est, sur le tumulus, gît une grande dalle, en partie enfouie dans la terre, qui pourrait être un élément de cette chambre (la [couverture]{.subject} ?).

Historique

Monument découvert par [J. Blot]{.creator} en [Octobre 2009]{.date}.

### [Sare]{.spatial} - [Etchegaraikoborda]{.coverage} 2 - [Dolmen]{.type}

Localisation

Ce monument est situé à une trentaine de mètres à l'Ouest de *Etchegaraikoborda 1 - Dolmen*.

Description

On note un tumulus pierreux de 60m de diamètre et 0,30m de haut environ, au centre duquel se voit un ensemble de 3 dalles de [grés]{.subject} gris semblant bien n'être que le seul vestige de la paroi Sud d'une [chambre funéraire]{.subject} orientée Est-Ouest, dont la longueur supposée, à s'en tenir à ces seuls éléments, pourrait atteindre environ 1m. Les dimensions de ces 3 dalles quasiment accolées les unes aux autres sont respectivement de 0,72m, 0,88m et 0,64m. Au Nord de celles-ci, et à 0,70m de distance, on note deux dalles couchées au sol, l'une de 1,14m de long et 0,30m de large, l'autre de 0,59m de long et 0,27m de large, qui pourraient avoir aussi fait partie de la chambre, ainsi que de nombreuses autres dalles ou fragments de dalles disséminés sur le tumulus.

Historique

Monument découvert par [J. Blot]{.creator} en [octobre 2009]{.date}.

### [Sare]{.spatial} - [Etxalar]{.coverage} - Dolmen

Localisation

Altitude : 524m.

Monument situé sur la ligne frontière, (à 25m au Sud de la BF 49).

Description

Sur une éminence dominant l'horizon on note un tumulus de terre et de pierres de 7m de diamètre, et 0,40m de haut, avec une légère dépression centrale, correspondant à la [chambre funéraire]{.subject} orientée Est-Ouest. Ses dimensions sont proches de celles du montant Nord toujours en place, penchée vers le Nord, qui mesure 2m de long, 0,73m de haut et 1,03m d'épaisseur.

Historique

Monument découvert par [Blot J.]{.creator} en [mars 2012]{.date}.

### [Sare]{.spatial} - [Faageko erreka]{.coverage} 1 Sud - [Cromlech]{.type}

Localisation

Carte IGN 1245 OT Hendaye - Saint-Jean-de-Luz.

Altitude : 260m.

Ce monument se trouve dans une belle fougeraie parsemée de chênes têtards, à quelques dizaines de mètres au Sud de l'antique piste de transhumance reliant Sare à Vera.

Description

Une vingtaine de pierres de [grés]{.subject} gris d'un volume pouvant atteindre 1m x 0,55m délimitent un cercle bien visible de 5,50m de diamètre. Quelques pierres sont aussi visibles à l'intérieur du cercle.

Historique

Monument découvert en [2000]{.date} par [A. Martinez Manteca]{.creator}.

### [Sare]{.spatial} - [Faageko erreka]{.coverage} 2 Sud - [Cromlech]{.type}

Localisation

À une douzaine de mètres à l'Ouest Nord-Ouest de *Faageko erreka 1 Sud - Cromlech*.

Description

Une quinzaine de pierres enfoncées dans le sol, peu visibles, délimitent un cercle de 3,20m de diamètre, excavé en son centre (fouille ancienne ?) où apparaissent deux petites dalles.

Historique

Monument découvert par [L. Millian]{.creator} en [2000]{.date}.

### [Sare]{.spatial} - [Faageko erreka]{.coverage} 3 Nord - [Cromlech]{.type}

Localisation

Altitude : 271 m.

Ce monument se trouve lui aussi dans une belle fougeraie, surplombant la précédente. On y trouve le dolmen de Faague et ce cromlech est à une distance de 10m au Sud Sud-Ouest de ce dolmen.

Description

Une dizaine de pierres, dépassant de peu la surface du sol, délimitent un cercle d'environ 3m de diamètre.

Historique

Monument trouvé en [2000]{.date} par [A. Martinez Manteca]{.creator}.

### [Sare]{.spatial} - [Faageko erreka]{.coverage} 4 Nord - [Cromlech]{.type}

Localisation

À une vingtaine de mètres au Sud Sud-Est du *Dolmen de Faague*.

Description

Onze pierres de volume assez important (certaines atteignent 0,50m à 0,60m de long), délimitent un cercle bien visible de 3m de diamètre.

Historique

Monument découvert en [octobre 2009]{.date} par [J. Blot]{.creator}.

### [Sare]{.spatial} - [Faague]{.coverage} - [Dolmen]{.type}

Localisation

Altitude : 270m.

Sur un replat transformé en une belle fougeraie, non plantée d'arbres, au flanc Sud du petit sommet dénommé Faague sur les cartes IGN (552m d'altitude). Ce replat fait face au mont Ibantelly, de l'autre côté de la D 406 reliant Sare à Vera.

Description

On distingue facilement, sur ce replat dénudé, ce qui reste de ce très probable monument. Les éléments les plus visibles sont les deux dalles qui apparaissent sur le côté Est de cet ensemble pierreux. Une grande dalle en [grés]{.subject} légèrement inclinée vers le Nord-Ouest et orientée Sud-Ouest Nord-Est, mesure 1,40m de long ; son épaisseur est de 0,34m à son extrémité Sud-Ouest et de 0,45m à l'autre extrémité. Sa hauteur est de 0,80m.

Une seconde dalle, bien visible se dresse à 0,47m au Sud-Ouest de la précédente, et perpendiculaire à l'orientation de cette dernière. Rectangulaire, plane sur ses deux faces, elle mesure 0,88m de haut, 0,15m d'épaisseur en moyenne et 0,47m de large. Elle présente des signes d'épannelage à son bord Nord-Ouest.

Il semblerait qu'une troisième dalle, faisant partie de la structure initiale de la [chambre funéraire]{.subject}, ait été brisée presque au ras du sol, n'ayant plus de visible que sa base à environ 0,80m au Nord-Ouest de la précédente et presque dans son prolongement. Elle mesure quelques centimètres de haut, 0,70m de long, et de 0,25m à 0,19m de large à ses extrémités.

On peut enfin se poser la question du rôle et de la position initiale d'un bloc rocheux parallélépipédique, situé à une quarantaine de centimètres au Nord-Ouest de la seconde dalle

décrite, basculé vers le Sud-Est, et dont la base, n'est plus insérée dans le sol ; il mesure 0,95m de haut, 0,50m de large en moyenne et 0,35m d'épaisseur ; il présente des traces d'épannelage à son sommet.

Ces structures sont visibles dans ce qui a pu être initialement un tumulus pierreux. Toutefois il nous semble que celui-ci a fait l'objet de remaniements profonds (ayant pu affecter aussi la [chambre funéraire]{.subject}). On voit en effet très nettement qu'il existe une structure carrée ([?]{.douteux}) d'environ 5m x 5m, faite de blocs plus ou moins tangents et volumineux., dont les 3 dalles précédemment décrites seraient parties constitutives au Sud-Est et au Sud-Ouest. De nombreux autres blocs sont visibles, en désordre, à l'intérieur de cette structure. Tout ceci n'est pas sans nous rappeler le dolmen *Generalen Tomba*, dans les Aldudes, qui a lui aussi subi ce genre de transformation (pour en faire un modeste abri pastoral).

Historique

Ce monument a été découvert par [A. Martinez]{.creator} en [2000]{.date}.

### [Sare]{.spatial} - [Faague]{.coverage} - [Monolithe]{.type}

Localisation

Altitude : 280m.

En bordure de la vieille piste pastorale qui, partie de la D 506, 1500m avant la borne frontière 36, se dirige en montant vers le Nord-Ouest, pour rejoindre un très beau col, à 600m, où se trouve la BF 32. Arrivée à l'aplomb des rochers de Faague, mais, bien en-dessous, à 270m d'altitude, cette piste traverse une très belle fougeraie, dénuée d'arbres, dont elle en longe la lisière Sud sur une centaine de mètres. Cette fougeraie est délimitée à l'Est et à l'Ouest par deux ravins parcourus par chacun par un ruisseau.

Le monolithe se trouve en bordure de piste, côté Nord, à l'extrémité Ouest de la fougeraie, à une trentaine de mètres après que le terrain ait amorcé sa déclivité vers l'Ouest.

Il est situé, à 300m à l'Ouest de *Faague - Dolmen*.

Description

Ce monolithe affecte la forme, classique en Pays basque, d'un pain de sucre orienté Nord-Sud, à base Nord. Ce bloc de [grés]{.subject} mesure 2,90m de long, 1,70m dans sa partie moyenne, la plus large. Son épaisseur varie de 0,50m à sa base à 0,37m au sommet.

Il semble bien qu'il y ait des traces d'épannelage à sa base, et tout le long du côté Ouest jusqu'à la pointe, ceci ayant eu pour effet de donner une certaine symétrie avec le côté opposé, aux formes naturelles.

On remarquera la situation classique de ce monolithe, en bord de piste pastorale, près de points d'eaux et au milieu de pâturages ; nous avons déjà émis l'hypothèse que les monolithes (ou *Muga* en basque = borne), puissent, en effet, être des bornes ayant servi de points de ralliement pour régler les différends pouvant survenir entre pasteurs, dès les temps protohistoriques, quant à l'exploitation des pâturages et des points d'eaux. De plus, il est particulièrement curieux de noter que ce monolithe se trouve exactement à l'aplomb de la borne 36, et donc des *Tables de Lizuniaga*, elles-mêmes toujours consacrées aux accords pastoraux.

Comme le faisait remarquer J.M. de Barandiaran, ces accords, compte tenu de la richesse des environs en monuments protohistoriques, peuvent fort bien remonter à ces lointaines époques. Par ailleurs, il n'est pas certain que ce fond de vallon, étroit, humide, boueux dans le passé - mais où passe l'actuelle D. 506 - ait été très favorable dans la protohistoire, aux déplacements des troupeaux ou aux réunions de pasteurs, ni que le monolithe originel, dont les Tables ont repris le rôle ensuite, ait été à cet emplacement dès le début.

C'est pourquoi -- *mais ce n'est qu'une hypothèse* - ce monolithe de Faague, placé au bord d'une belle piste pastorale - reliant elle aussi Sare à Vera - au milieu de pâturages dégagés, pourrait - avec quelques probabilités - être considéré comme le véritable ancêtre des Tables de Lizuniaga...

Rappelons qu'il n'y avait, à ce jour, que 2 monolithes connus dans la Rhune, que nous avons déjà publiés : *Gastenbakarre* [@blotMonolithesPaysBasque1983 p. 1] et *Athekaleun* [@blotMonolithesPaysBasque1983 p. 2].

Historique

Ce monument a été découvert par [nous]{.creator} en [mars 2009]{.date}.

### [Sare]{.spatial} - [Fago]{.coverage} - [Monolithe]{.type}

Localisation

Altitude : 605m.

Il se trouve à 25m au Sud-Est de la BF 29, sur terrain plat.

Description

Belle dalle de [grés]{.subject} rose en forme de pain de sucre, gisant au sol selon un axe Nord Nord-Est, Sud Sud-Ouest. D'environ une dizaine de centimètres d'épaisseur, elle mesure 2,64m de long et 1,36m dans sa plus grande largeur. Elle semble présenter des traces d'épannelage à son sommet Sud-Ouest et sur un renflement de son bord Ouest.

Historique

Monolithe découvert par [P. Badiola]{.creator} en [janvier 2011]{.date}.

### [Sare]{.spatial} - [Fago]{.coverage} - [Cromlech]{.type} ([?]{.douteux})

Localisation

On le trouve à environ 5m au Sud-Est de l'angle Sud-Est d'une grande borde en ruine, elle-même située à une centaine de mètres au Sud de la BF 29.

Description

On ne distingue qu'un demi-cercle (3,40m de diamètre environ) constitué d'une dizaine de pierres, visibles au ras du sol ; ce dernier ayant été remanié aux alentours, il est difficile d'être plus précis.

Historique

Elément trouvé par [J. Blot]{.creator} en [mars 2011]{.date}.

### [Sare]{.spatial} - [Fago]{.coverage} 3 - [Dalle couchée]{.type}

Localisation

Nous avons déjà cité, le long de cette ligne frontière, et dans les alentours de la BF 29, les *dalle couchées Fago n°1* et *n°2* [@blotInventaireMonumentsProtohistoriques2013] et le *monolithe Fago* [@blotInventaireMonumentsProtohistoriques2011]. Voici une 3^ème^ dalle couchée, sans que l'on puisse en préciser la finalité ni l'époque.

Altitude : 515m.

Elle est située à 10m au Nord-Est de l'ancienne BF 30, couchée au sol, elle-même aussi à 10m au Nord-Est de la nouvelle BF 30, détruite, en morceaux sur le sol.

Description

Dalle de [grés]{.subject} [triasique]{.subject}, de forme lancéolée, allongée sur le sol selon un axe Nord-Est Sud-Ouest, mesurant 1,12m à sa base, 1,78m de long, et une vingtaine de centimètres d'épaisseur en moyenne. Tout son pourtour montre des traces évidentes d'épannelage.

Historique

Dalle signalée par [P. Badiola]{.creator} en [juin 2014]{.date}.

### [Sare]{.spatial} - [Fago]{.coverage} 1 - [Dalle couchée]{.type}

Localisation

Altitude : 600m.

Elle est à une quarantaine de mètres à l'Est du *monolithe de Fago*.

Description

Dalle de [grés]{.subject} local, mesurant 1m de long et 0,35m de large. Elle présente des traces d'épannelage sur tout son pourtour.

Historique

Dalle découverte par [Badiola P.]{.creator} en [avril 2012]{.date}.

### [Sare]{.spatial} - [Fago]{.coverage} 29 - [Dalle couchée]{.type} ([?]{.douteux})

Localisation

Altitude : 605m.

Description

Cette dalle est plantée selon un axe Sud-Ouest Nord-Est, mais très inclinée vers le Nord. Sa longueur est de 1,30m, sa largeur de 0,53m et son épaisseur de 0,15m. Ses bords Sud-Ouest et Nord-Ouest présentent des traces évidentes d'épannelage.

Historique

Dalle découverte par [Badiola P.]{.creator} en [Mai 2012]{.date}.

### [Sare]{.spatial} - [Fago]{.coverage} 2 - [Dolmen]{.type} ([?]{.douteux})

Localisation

Altitude : 545m.

Ce probable dolmen est tangent au Sud de la piste 4x4, un peu avant qu'elle n'atteigne le col de Fago.

Description

On note, contre un buisson d'ajoncs, quelques pierres d'assez importantes dimensions, qui ont roulé là (naturellement ? action anthropique ?) ; elles dissimulent en partie une dalle de [grés]{.subject} verticale, solidement plantée dans le sol - un tumulus probable - selon un axe Est-Ouest. De forme trapézoïdale, cette dalle verticalement plantée, mesure plus d'un mètre à sa base, 0,76m de haut, et son épaisseur varie de 7 à 10 centimètres.

Historique

Monument trouvé par [J. Blot]{.creator} en [mars 2011]{.date}.

### [Sare]{.spatial} - [Fago]{.coverage} - [Dolmen]{.type}

Localisation

Altitude : 561m.

Il est pratiquement au sommet de ce plateau allongé vers l'Est qui se trouve au Nord-Est du mont Fago. De là, il domine l'horizon.

Description

On note tout d'abord un important tumulus pierreux de 8m de diamètre et 0,40m de haut ; les structures centrales sont difficiles à préciser : dans la partie Nord Nord-Est du tumulus on note une dépression de 1m de large environ et d'une dizaine de centimètres de profondeur (fouille ancienne ?) et dans la partie Sud-Ouest de ce même tumulus apparaît discrètement ce qui pourrait être le sommet d'une longue dalle enfouie dans le sol (plus d'un mètre de long semble-t-il). Est-ce une partie de la [chambre funéraire]{.subject}, mais qui ne serait pas très centrale... ?

Historique

Monument découvert par [J. Blot]{.creator} en [octobre 2009]{.date}.

### [Sare]{.spatial} - [Fago]{.coverage} 1 et 2 - [Tertres d'habitat]{.type}

Localisation

Altitude : 410m.

Se trouvent au col de Fago, de part et d'autre de la piste du même nom.

Description

- *TH n°1* : tangent à la piste, à l'Ouest, il mesure 4m de diamètre, 0,70m de haut.

- *TH n°2* : situé à 10m à l'Ouest du précédent, de l'autre côté de la piste. Mesure 4m de diamètre et 0,40m de haut.

Historique

Tertres signalés par [Badiola P.]{.creator} en [octobre 2011]{.date}.

### [Sare]{.spatial} - [Fago]{.coverage} - [Tumulus]{.type} ([?]{.douteux})

Localisation

Altitude : 405m.

À l'extrémité Sud d'un plateau herbeux, à 3m ou 4m à l'Ouest d'une barre rocheuse.

Description

Monument très douteux. On distingue cependant un relief tumulaire circulaire d'environ 6m de diamètre et 0,30m de haut, à la périphérie duquel on peut compter 5 à 6 pierres, ainsi que deux autres au centre ([ciste]{.subject} centrale ?).

Historique

Monument douteux découvert par [J. Blot]{.creator} en [octobre 2009]{.date}.

### [Sare]{.spatial} - [Gastenbakarre]{.coverage} 5 - [Cromlech]{.type} ([?]{.douteux})

Localisation

Carte IGN 1245 OT. Hendaye Saint-Jean-de-Luz.

Il est situé sur le replat juste au-dessus de *Gastenbakarre n°5 - Dolmen*.

Altitude : 351m.

Description

À la périphérie d'un très léger relief circulaire de 6m de diamètre, il semble que 3 pierres, disposées dans le secteur Nord fassent partie d'un péristalithe (monument très douteux).

Historique

Monument découvert par [J. Blot]{.creator} en [mars 2010]{.date}.

### [Sare]{.spatial} - [Gastenbakarre]{.coverage} 2 - [Dalle couchée]{.type}

Localisation

Altitude : 275m.

Cette dalle se voit en bordure Sud-Ouest de la piste qui passe devant le grand monolithe planté, plus au Nord, et qui se dirige vers le Sud.

Description

Grosse dalle de [grés]{.subject} de forme approximativement triangulaire, allongée selon un axe Ouest Sud-Ouest, Est Nord-Est, à base épaisse au Nord-Est (0,57m), en bordure de piste et dont l'extrémité Sud-Ouest est plus mince et plus effilée. Elle mesure 3,10m de long, 1,14m dans sa plus grande largeur, et semble présenter des traces d'épannelage tout le long de son bord Sud-Est.

Historique

Dalle découverte par [Duvert M.]{.creator} en [décembre 2011]{.date}.

### [Sare]{.spatial} - [Gastenbakarre]{.coverage} 3 - [Dolmen]{.type}

Localisation

Carte 1245 Est Espelette.

Altitude : 269m.

Nous avons décrit [@blotNouveauxVestigesMegalithiques1971 p.6] deux dolmens situés en un petit col au flanc Nord-Est de Larraun, à environ 1000m à vol d'oiseau au Nord du col de Saint-Ignace. Le n°1 a été complètement saccagé depuis par des « chercheurs de trésor » !

Il est à environ 150m au Sud-Est du n°2, et à 3m au Nord-Est du chemin qui monte vers Altsaan.

Description

On note un tumulus mixte de terre et de pierres de 11m de diamètre et 0,70m de haut. Au centre une [chambre funéraire]{.subject} bien visible, orientée plein Est, mesure 2m de long, 0,90m de large et 0,45m de profondeur. Elle est totalement vidée de son contenu puisque le fond en est visible, tapissé d'un petit dallage de plaquettes de [grés]{.subject}, chose assez rare dans le cas de ces monuments dans notre région.

Quatre dalles délimitent par ailleurs cette chambre :

Au Nord, une belle dalle, légèrement inclinée vers l'intérieur, mesure 1,70m de long et 0,45m de haut ; elle est prolongée par une seconde, de dimensions plus restreintes : 0,70m de long et 0,65m de haut.

À l'Ouest, une dalle mesurant 0,90m de long et 0,30m de haut, est perpendiculaire à la précédente.

À l'Est, on note une dalle bien plus petite, de 0,50m de long et 0,45m de haut.

Il n'y a pas de couvercle visible.

Historique

Dolmen découvert en [avril 1974]{.date}.

### [Sare]{.spatial} - [Gastenbakarre]{.coverage} 4 - [Dolmen]{.type}

Localisation

Carte IGN 1245 OT. Hendaye Saint-Jean-de-Luz

Altitude : 271m.

Il est situé au départ de la piste qui se rend, vers l'Ouest, au ruisseau Gastenbakarreko erreka, et à une quinzaine de mètres au Nord-Est de sa jonction avec la piste ascendante vers Altsaan.

Description

Il existe un très discret tumulus de 5m de diamètre environ au milieu duquel apparaissent 4 dalles de [grés]{.subject} rose, très inclinées qui délimitent une [chambre funéraire]{.subject} de 2,25m de long et 0,57m de large environ, orientée Nord-Ouest Sud-Est. Les dimensions de ces dalles sont, au Nord de 0,60m et 1m de long ; au Sud de 0,83m et 0,44m de long. Il existe une cinquième dalle couchée au Sud Sud-Est de la chambre, probablement un de ses éléments constitutifs.

Historique

Monument découvert par [J. Blot]{.creator} en [mars 2010]{.date}.

### [Sare]{.spatial} - [Gastenbakarre]{.coverage} 5 - [Dolmen]{.type}

Localisation

Carte IGN 1245 OT. Hendaye Saint-Jean-de-Luz.

Altitude : 340 m.

Il est situé à 20m au Nord-Est de la piste ascendante, sur un terrain en très légère pente vers le Sud-Est.

Description

On note trois dalles dont les sommets apparaissent au ras du sol, délimitant une [chambre funéraire]{.subject} de 1,40m de long et 0,80m de large, orientée Est-Ouest. Les dimensions des dalles : dalle Nord : 0,70m de long, 0,16m de haut ; dalle Sud : en deux fragments, l'un de 0,67m de long, l'autre de 0,20m de long ; la dalle de chevet, à l'Ouest, mesure 0,52m de long.

On ne note pas de tumulus visible.

Historique

Monument découvert par [J. Blot]{.creator} en [mars 2010]{.date}.

### [Sare]{.spatial} - [Gastenbakarre]{.coverage} 6 - [Dolmen]{.type} ([?]{.douteux})

Localisation

Carte IGN 1245 OT. Hendaye Saint-Jean-de-Luz.

Situé à 10m à l'Est de *Gastenbakarre n°5 - Dolmen*.

Description

On note deux dalles (qui ne sont en fait qu'une seule, fragmentée en deux), orientées Nord-Sud, l'une mesurant 0,78m de long, l'autre 0,84m, et une hauteur de 0,30m et 0,20m. Il n'y a pas de tumulus visible.

Historique

Monument découvert par [J. Blot]{.creator} en [mars 2010]{.date}.

### [Sare]{.spatial} - [Gastenbakarre]{.coverage} 7 - [Dolmen]{.type} ([?]{.douteux})

Localisation

Altitude : 290m.

Il est situé à environ 80m au Nord-Ouest des ruines d'un cayolar et à 20m au Sud-Ouest du ruisseau.

Description

On peut voir 2 dalles verticales, de [grés]{.subject} [triasique]{.subject}, érigées selon un axe Est-Ouest, au centre d'un probable tumulus pierreux dont il est difficile d'apprécier les dimensions sur ce terrain en légère pente vers le Sud. La dalle Nord mesure 1,37m à la base et 0,59m de haut ; son épaisseur atteint 0,14m. Celle au Sud mesure 1,02m à sa base, 0,50m de haut et son épaisseur est de 0,14m.

Historique

Elément trouvé par [J. Blot]{.creator} en [avril 2011]{.date}.

### [Sare]{.spatial} - [Gastenbakarre]{.coverage} 8 - [Dolmen]{.type}

Localisation

Altitude : 300m.

Situé à peu de distance à l'Ouest de *Gastenbakarre n°7 - Dolmen ([?]{.douteux})*, sur un terrain en très légère pente vers l'Est.

Description

La [chambre funéraire]{.subject}, qui mesure 2,50m de long et 1,20m de large, est orientée selon un axe Est Nord-Est, Ouest Sud-Ouest. Elle est délimitée, au Nord, par une grande dalle de 2m de long, 0,33m de haut et 0,10m d'épaisseur en moyenne, dont tout le bord supérieur présente des traces d'épannelage ; au Sud-Ouest, la paroi est formée par deux autre dalles de 0,67m et 0,84m de long, doublées à l'extérieur par une troisième de 0,74m de long. Toutes ces dalles sont au ras du sol.

Historique

Monument découvert par [Meyrat F.]{.creator} en [nombre 2012]{.date}.

### [Sare]{.spatial} - [Gastenbakarre]{.coverage} 2 - [Monolithe]{.type}

Localisation

Altitude : 330m.

Monolithe situé sur un petit replat, dominant la rive droite du ruisseau Gastenbakarrekoerreka, (à environ 80m).

Description

Belle dalle plate de [grés]{.subject} [triasique]{.subject}, gisant selon un axe sensiblement Est-Ouest, qui est celui de la légère pente du sol, et de son plus grand axe, soit une longueur de 3,20m. Elle mesure 1,55m de large et son épaisseur moyenne est d'une vingtaine de centimètres.

Historique

Ce monolithe nous a été signalé par [L. Millan]{.creator} en [mars 2011]{.date}.

### [Sare]{.spatial} - [Gastenbakarre]{.coverage} 3 - [Monolithe]{.type}

Localisation

Altitude : 335m.

Description

Belle dalle de [grés]{.subject} [triasique]{.subject} allongée selon un axe Nord-Est Sud-Ouest, mesurant 2,80m de long, 1m à sa base Nord-Est, 0,68m à son sommet, et 0,20m d'épaisseur en moyenne. Elle présente de nombreuses traces d'épannelage sur 3 de ses côtés (sauf le coté Sud-Ouest).

Historique

Monolithe découvert par [Ph. Velche]{.creator} en [mai 2011]{.date}.

### [Sare]{.spatial} - [Gastenbakarre]{.coverage} - [Pierre couchée]{.type} ([?]{.douteux})

Localisation

Altitude : 275m.

Description

Cette dalle gît sur le flanc Est de Larraoun, à l'Ouest de la piste pastorale empierrée ci-dessus évoquée, à laquelle sa base est tangente.

Dalle de [grés]{.subject} local, de forme parallélépipédique, allongée selon un axe Ouest-Est et à sommet arrondi. Elle mesure 2,10m de long, 1,15m de large à sa base, 0,57m à la corde de l'arc du sommet ; son épaisseur varie de 28 à 32 centimètres. Il semble bien que le sommet arrondi et tout le bord Nord de cette dalle présentent des traces d'épannelage.

Simple dalle naturelle ou monolithe vrai ayant subi une action anthropique ?

Historique

Elle nous a été signalée par [M. Duvert]{.creator} en [octobre 2010]{.date}.

### [Sare]{.spatial} - [Gastenbakarre]{.coverage} - [Tumulus]{.type}

Localisation

Carte IGN 1245 OT. Hendaye Saint-Jean-de-Luz.

Altitude : 275m.

On trouve ce tumulus sur un replat, en montant à une soixantaine de mètres au Nord-Ouest du *Dolmen Gastenbakarre n°3*.

Description

Tumulus de terre et de pierres de 6m de diamètre environ, et 0,45m de hauteur. On note une dalle qui affleure le sol à sa périphérie en secteur Est, et les sommets de quelques autres affleurant très discrètement au centre posent la question d'un éventuel tumulus dolménique.

Historique

Monument découvert par [J. Blot]{.creator} en [mars 2010]{.date}.

### [Sare]{.spatial} - [Gastenbakarreko erreka]{.coverage} - [Monolithe]{.type}

Localisation

Carte IGN 1245 OT. Hendaye Saint-Jean-de-Luz.

Il est situé à 20m à l'Ouest du *Dolmen Gastenbakarreko erreka n°2*.

Description

Volumineux bloc de pierre en [grés]{.subject} gris, grossièrement rectangulaire, couché au sol, selon un axe Nord-Est Sud-Ouest. Ses dimensions : 2,30m de long, 1m de large, et une trentaine de centimètres d'épaisseur. Il est particulièrement remarquable de noter que ce monolithe présente des traces très nettes d'épannelage sur tout son pourtour.

Historique

Monument découvert par [J. Blot]{.creator} en [octobre 2009]{.date}.

### [Sare]{.spatial} - [Gastenbakarreko erreka]{.coverage} 1 et 2 - [Cromlechs]{.type}

Localisation

Carte IGN 1245 OT. Hendaye Saint-Jean-de-Luz.

Altitude : 285 m.

Monuments peu visibles, on les trouve à quelques mètres au Sud du ruisseau Gastenbakarreko erreka qui s'étale dans le gazon ; ils sont à peu près à équidistance des ruines d'un cayolar ruine, au Nord, et du monolithe dalle, bien connu et bien visible, dressé au Sud, à 80m environ.

Description

- *Cromlech n°1* :* Une dizaine de pierres, surtout visible dans sa moitié Sud, enfouies dans le sol, délimitent un cercle de 2m de diamètre.

- *Cromlech n°2* : il est situé à quelques centimètres au Nord-Est du précédent. Huit pierres périphériques délimitent un cercle de 2,60m de diamètre.

Historique

Monuments découverts par [J. Blot]{.creator} en [octobre 2009]{.date}.

### [Sare]{.spatial} - [Gastenbakarreko Erreka]{.coverage} 4 - [Dolmen]{.type}

Localisation

Altitude : 245m.

Ce monument est situé sur un petit replat à une vingtaine de mètres au Sud du ruisseau Gastenbakarreko erreka.

Description

La [chambre funéraire]{.subject}, orientée selon un axe Est-Ouest, se voit au milieu des restes d'un tumulus pierreux, peu élevé, de 4m à 5m de diamètre ; elle mesure environ 3m de long et 1m de large. On distingue 2 montants au Nord : une grande dalle verticale de 1,80m de long à sa base, de 0,26m d'épaisseur et 0,90m de haut, et, dans son prolongement Ouest, une autre dalle, verticale, elle aussi, de 0,95m de long et 0,85m de haut. Au Sud, deux autres montants sont visibles : le premier, vertical, mesure 1,15m de long à sa base, 0,50m de haut et 0,16m d'épaisseur ; le second a basculé vers l'intérieur de la [chambre funéraire]{.subject}, et mesure 1m de long, 0,25m de haut et 0,25m d'épaisseur. De nombreuses autres dalles sont visibles sans qu'il soit possible de leur attribuer un rôle éventuel.

Historique

Monument découvert par [Meyrat F.]{.creator} en [octobre 2011]{.date}.

### [Sare]{.spatial} - [Gorostiarria]{.coverage} - [Monolithe]{.type}

Localisation

Carte IGN 1245 OT. Hendaye Saint-Jean-de-Luz.

Altitude : 513m.

Il se trouve au Sud de la crête d'Altsaan, au niveau de la station de croisement du train à crémaillère, sur une aire gazonnée sous une pinède. C'est la seule pierre importante de ce site, et elle se trouve à une soixantaine de mètres au Nord-Est de la voie ferrée.

Description

Epaisse dalle rectangulaire de [grés]{.subject} rose, dont le bord Nord-Ouest a été brisé assez récemment semble-t-il. Son extrémité Sud Sud-Est, arrondie, présente de très nettes traces d'épannelage. Elle mesure 2,44m de long, 1,60m de large et 0,35m d'épaisseur.

Historique

Monolithe découvert par [J. Blot]{.creator} en [mars 2010]{.date}.

### [Sare]{.spatial} - [Gorostiarria]{.coverage} - [Dolmen]{.type}

Localisation

Carte 1245 Ouest Saint-Jean-de-Luz.

Altitude : 549m.

Il est situé à 30m à l'Est du petit bois marqué 541 sur la carte IGN, et à 30m au Nord des rails du funiculaire, sur un terrain en légère pente vers le Sud. De ce col on a une très belle vue vers les montagnes à l'Est.

Description

Tumulus terreux recouvert d'herbe, de 9m de diamètre et 0,40m de haut. Cinq pierres à sa périphérie évoquent un possible péristalithe. Le tumulus paraît plus marqué dans sa partie Sud du fait de la légère inclinaison du terrain. 

La [chambre funéraire]{.subject}, orientée Est Sud-Est, mesure environ 1m de long, 0,70m de large et 0,25m de haut ; elle est délimitée par 2 modestes dalles en grès rose local : 
- au Sud-Ouest, une dalle de 1m de long, 0,22m d'épaisseur et 0,25m de haut ; 
- une autre, au Nord-Est, de 0,35m de long et 0,25m de haut. Il n'y a pas de couvercle visible.

Historique

Dolmen découvert en [octobre 1975]{.date}.

### [Sare]{.spatial} - [Ibantelli]{.coverage} Sud - [Dolmen]{.type}

Localisation

Carte IGN 1245 OT Hendaye - Saint-Jean-de-Luz.

Altitude : 480m.

Il est érigé à l'extrémité d'une prairie en pente douce vers l'Est, à environ 200m au Nord-Est de la BF n°43.

Description

Il se présente sous la forme d'un tumulus terreux et pierreux de 7m à 8m de diamètre et 0,40m de haut. Une légère excavation centrale (fouille ancienne ?), laisse apparaître une portion de dalle qui pourrait faire partie d'une [chambre funéraire]{.subject} orientée Ouest-Est.

Historique

Monument découvert par [I. Txintxurreta]{.creator} en [2004]{.date}.

### [Sare]{.spatial} - [Ibantelli]{.coverage} - [Monolithe]{.type}

Localisation

Altitude : 600m.

Monolithe situé à 80m au Nord de la BF 39, en bordure de piste, et en lisière d'un bois de conifères, sur terrain plat.

Description

Important bloc de [grés]{.subject} [triasique]{.subject} couché au sol, de forme triangulaire, à grand axe Est-Ouest, mesurant 3,60m de long, 1,04m à sa base, épais de 0,40m à sa base et 0,24m au sommet. Son séjour à cet endroit doit remonter à fort longtemps, cette roche présentant des signes d'un délitement avancé. On ne note pas de traces d'épannelage, la forme naturelle ayant pu convenir d'emblée.

Historique

Monolithe découvert par [Velches P.]{.creator} en [mai 2011]{.date}.

### [Sare]{.spatial} - [Ibantelli]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 470m.

Il est situé sur un vaste plateau, à 8m au Nord-Ouest de la BF 43.

Description

Tumulus de terre de 6m de diamètre, et 0,20m de haut ; la piste passe en son milieu.

Historique

Tumulus découvert par [Meyrat F.]{.creator} en [mars 2012]{.date} (et [L. Millian]{.creator} en [2004]{.date}).

### [Sare]{.spatial} - [Ibantelli]{.coverage} Sud 2 - [Dolmen]{.type}

Localisation

Altitude 611m.

Ce monument est érigé à environ 200 m à l'Est du sommet du mont Ibantelli ; le col de Lizarietta se voit en contre bas au Sud. Il apparaît au milieu d'un vaste éboulis dû au délitement par les intempéries d'un filon de grès. Le terrain à cet endroit est en légère pente vers le Sud-Ouest.

Description

Ce monument n'est pas évident. Si la réalité d'un monument funéraire à inhumation ne fait que peu de doutes, il reste difficile à décrire dans son état actuel.

Il semblerait que l'on puisse voire une [chambre funéraire]{.subject} orientée Sud-Ouest Nord-Est mesurant environ 2,70m ([?]{.douteux}) de long et 1,50m de large, délimitée par 4 dalles et 2 autres dalles de [couverture]{.subject}, ayant légèrement glissé dans le sens de la pente. La partie la plus évidente de cette [chambre funéraire]{.subject} est formée par la jonction de 2 dalles, à angle droit, dans l'angle Nord de cette chambre, dalles de grès de volume modeste n'émergeant du sol que d'une dizaine de centimètres ; la plus au Sud-Ouest (dalle 1), mesure 0,65m de long et 0,11m d'épaisseur ; celle qui lui est perpendiculaire au Sud-Ouest (dalle 2) mesure 1,12m de long et 0,16m d'épaisseur. Une troisième dalle (dalle 3), bien modeste, est visible un peu décalée vers le Nord-Est et mesure 0,70m de long et 0,16m d'épaisseur. La dernière visible (dalle 4) formant la limite extérieure Sud-Est de cette [chambre funéraire]{.subject}, est sensiblement perpendiculaire à la paroi Nord-Est et mesure environ 1,80m de long.

Les autres constituants des montants de ce dolmen (ou ciste dolmenique ?) ne sont pas visibles, cachés dans le sol, ou sous les dalles de [couverture]{.subject}, ou disparus....

Les 2 dalles de couverture : la plus au Nord-Est (dalle A) mesure 1,57m de long et 0,84m de large, avec une épaisseur de 0,42m ; la seconde (dalle B), plus au Sud-Ouest, mesure 1,70m de long, 0,92m de large et 0,15m d'épaisseur.

Un tumulus d'environ 5m de diamètre nous paraît peu probable.

Historique

Monument découvert par [I. Txintxuretta]{.creator} en [septembre 2019]{.date} et revu par [F. Meyrat]{.creator} en [décembre 2019]{.date}.

### [Sare]{.spatial} - [Ibantelli]{.coverage} Nord - [Monolithe]{.type}

Localisation

Altitude 638m.

Ce monolithe couché se trouve dans un col herbeux séparant au nord le sommet de l'Ibantelli d'une importante barre rocheuse au sud (alt. 655m).. Ce col est à la fois un pâturage et un lieu de passage.

Description

Belle dalle de grès [triasique]{.subject} en forme de pain de sucre légèrement rétréci en son milieu, à sommet Nord Nord-Est, couchée sur un sol en légère pente vers le nord ; elle mesure 4,23m de long, 1,75m dans sa plus grande largeur et sa base 1,16m. Son épaisseur moyenne est de 0,45m. Enfin cette dalle paraît bien présenter des signes d'épannelage sur tout son pourtour.

Historique

Monolithe découvert en [janvier 2020]{.date} par [F. Meyrat]{.creator}.

### [Sare]{.spatial} - [Ibantelli]{.coverage} 348 - [Tumulus]{.type} ([?]{.douteux})

Localisation

Altitude : 348m.

Ce « monument » est situé à l'extrémité Est d'une longue croupe qui s'étend au flanc Est de l'Ibantelli et domine directement, au Nord, la D 306 à son arrivée au col de Lizarrieta.

Description

Un assemblage d'une trentaine de pierres est situé au Sud-Est du poste de chasse n°20, et tangent à ce dernier ; ces éléments pierreux, apparaissant au ras du sol et semblent affecter une disposition plus ou moins circulaire une surface de 2m de diamètre environ, (ils prédominent dans le secteur Nord-Ouest), sans aucun relief. Il n'est donc pas possible de parler d'un tumulus, mais peut-être d'évoquer un ancien tumulus pierreux qui aurait été nivelé par l'activité humaine toute proche... À noter, à 2m à l'Est Sud-Ouest, les vestiges d'un ancien foyer dont les pierres disposées en **U** sont encore bien visibles.

Historique

Ce « cas particulier » a été découvert par [F. Meyrat]{.creator} en [mars 2019]{.date}. Nous le publions avec les plus extrêmes réserves.

### [Sare]{.spatial} - [Iratzeburua]{.coverage} - Grande [dalle couchée]{.type} ([?]{.douteux})

Localisation

Situé à une cinquantaine de mètres au Nord-Est de *Sayberri - Cromlechs (?)*
Altitude : 340m.

Description

Monolithe de [grés]{.subject} local, couché sur le sol, en forme de pain de sucre, mesurant 4,40m de long, 1,60m de large et 0,25m d'épaisseur en moyenne. Il ne présente pas, semble-t-il de traces d'épannelage (?).

Historique

Monolithe découvert par [J. Blot]{.creator} en [mai 2009]{.date}.

### [Sare]{.spatial} - [Ithunarria]{.coverage} - [Monolithe]{.type}

Localisation

Altitude : 441m.

Cette pierre plantée se dresse sur la colline qui domine, à l'Ouest, le col d'Olhayn.

Description

Elle est située dans une sorte de couloir rocheux, orientée Nord-Sud, long d'une soixantaine de mètres, délimité par deux barres rocheuses Est et Ouest formées de volumineux amas de poudingue. Cette pierre est plus proche de la paroi Ouest (3m) que de la paroi Est (11m).

Ce bloc de [grés]{.subject}, en forme de dalle parallélépipédique, d'épaisseur moyenne d'environ 0,20m, est orienté suivant un axe Nord-Sud. Il présente un bord Sud haut de 0,95m et un bord Nord de 0,60m ; cette dissymétrie aboutissant à un sommet incliné vers le Nord. La base est large de 0,55m, et l'ensemble va en se rétrécissant légèrement vers le haut (0,40m).

Cette dalle a été volontairement plantée là, comme en témoignent de nombreux blocs rocheux de calage à sa base.

Historique

Cette dalle est signalée par [M. Duvert]{.creator} (communication personnelle).

Identification de cette dalle sur le terrain et photos par [F. Meyrat]{.creator} en [novembre 2019]{.date}.

### [Sare]{.spatial} - [Kondendiagakolepoa]{.coverage} - [Tertre d'habitat]{.type}

Localisation

Altitude : 315m.

Description

Tertre terreux de 5m de diamètre et 0,40m de haut.

Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

### [Sare]{.spatial} - [Larria]{.coverage} 4 - [Dolmen]{.type}

Localisation

Carte 1245 Est Espelette.

Situés au Nord-Ouest du mont Sayberry, les *Dolmens Larria 1*, *2* et *3* ont été décrits initialement en 1946 [@barandiaranCatalogueStationsPrehistoriques1946, p.34] et le *Dolmen Larria 1 Nord* l'a été en 1967 [@chauchatSeptDolmensNouveaux1966 p.83].

Le dolmen Larria 4 est situé à 40m à l'Est Sud-Est du *n°3*.

Description

Tumulus pierreux tout à fait semblable à celui du n°3, formé de petits blocs de [grés]{.subject} rose. Il mesure 8m de diamètre et 0,40m de haut, et il est adossé, au Sud-Ouest, à une crête rocheuse. Ce tumulus, bien qu'érigé au milieu de nombreux blocs de grès épars autour de lui et sur un terrain en très légère pente, est cependant bien identifiable. Il n'y a pas de [chambre funéraire]{.subject} visible, ni de dépression centrale ; peut-être s'agit-il d'un monument vierge ?.

Historique

Monument découvert en [septembre 1970]{.date}.

### [Sare]{.spatial} - [Lezia]{.coverage} - [Dolmen]{.type}

Localisation

Carte 1245 Est Espelette.

Altitude : 210m.

Il est situé sur une petite colline qui domine immédiatement, au Sud, la route qui arrive aux grottes de Sare, à environ 300m à vol d'oiseau au Nord Nord-Ouest du parking de celles-ci.

Il est construit à 20m au Nord d'un amoncellement de gros blocs de poudingue, visible de loin.

Description

Tumulus pierreux de 10m de diamètre et 0,60m de haut, formé de blocs de [grés]{.subject} rose local de la taille d'un pavé. De la [chambre funéraire]{.subject}, orientée Ouest-Est, et qui devait initialement mesurer environ 2m x 1,10m, il ne reste que deux belles dalles de grès rose en place, apparaissant dans une profonde dépression centrale. Le montant Nord mesure 1,70m de long, 0,70m de haut et 0,20m d'épaisseur ; il est incliné vers le Nord et présente des traces d'épannelage à son bord supérieur. Le montant Sud, aux bords arrondis, ne présente pas de traces de régularisation ; il est légèrement plus long que le précédent : environ 2m, et 0,30m de haut avec une épaisseur de 0,25m, et très fortement incliné vers le Nord. Ceci est dû à la pression exercée sur sa face externe par le tumulus pierreux qui la recouvre entièrement, et qui est lui-même édifié sur un terrain en pente vers le Nord. On note sur le bord Sud-Est du tumulus deux gros fragments de dalle, dont un partiellement enfoui ; ils pourraient représenter tout ou partie d'un montant ou de la table de [couverture]{.subject}. De même, il existe à 7m à l'Est Nord-Est du tumulus un autre important fragment de dalle, dont un bord est épannelé ; est-ce une partie d'un montant ?

Historique

Il nous a été signalé en [mai 1999]{.date}.

### [Sare]{.spatial} - [Lezia]{.coverage} - [Dalle plantée]{.type}

Localisation

Altitude : 300m.

Description

Elle est verticalement plantée dans le sol, mesure 1,38m à sa base, 1,10m de haut, et 0,25m d'épaisseur. Il semblerait qu'on puisse distinguer, à sa base, un tumulus pierreux de 6m de diamètre.

Historique

Dalle découverte par [Blot J.]{.creator} en [mars 2012]{.date}.

### [Sare]{.spatial} - [Lizuniaga]{.coverage} - [Tumulus-cromlechs]{.type}

Nous décrivons ici ces deux monuments, qui, bien que ne faisant pas partie de la commune de Sare, mais de celle de Vera, complètent, avec ceux du col de Kondedikolepoa, le contexte archéologique de l'antique voie de transhumance reliant Sare à Vera (voir aussi mon article dans le [@blotMonolitheFaagueTable2009 p.23]

Localisation

Altitude : 240m.

Ces deux monuments se trouvent très près de la frontière, à quelques mètres au Sud-Est de la BF 35, à environ 200m au Nord-Ouest de la venta-restaurant, et à droite du virage qu'effectue le chemin empierré qui, partant de cette venta, se rend à un ensemble de maisons proches.

Description

- Le *premier* tumulus-cromlech mesure environ 4,70m de diamètre et 0,40m de haut ; au centre existe une dépression peu profonde de 2m de diamètre ; de nombreuses pierres blanches apparaissent à la périphérie de celle-ci, et sur le tumulus lui-même. 

- Il semble bien qu'il existe un *deuxième* monument de même type, tangent à l'Est du précédent, de même hauteur et de 3m de diamètre environ.

Historique

Monuments découverts, le premier par [I. Gaztelu]{.creator} en [mars 2001]{.date}, le second par [A. Martinez]{.creator} en [2009]{.date}.

### [Sare]{.spatial} - [Lizuniaga]{.coverage} 3 - [Tumulus]{.type}

Localisation

Altitude : 360m.

Dans un petit col, sur un replat à 5m au Sud-Est de la piste.

Description

Petit tumulus pierreux de 3,50m de diamètre et 0,30m de haut.

Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

### [Sare]{.spatial} - [Lizuniaga]{.coverage} 4 - [Tumulus]{.type}

Localisation

Altitude : 304m.

Ce monument est sur un replat, à une centaine de mètres au Sud Sud-Est de la BF 34, et à 9m à l'Est Sud-Est de la piste qui monte vers Larraun.

Description

Tumulus pierreux de 3,20m de diamètre, en forme de galette circulaire aplatie, constitué de blocs de calcaire blanc qui affleurent la surface du sol, de la taille d'un poing ou d'un pavé.

Historique

Monument découvert par [P. Badiola]{.creator} en [mars 2015]{.date}.

### [Sare]{.spatial} - [Nabalatz]{.coverage} 2 - [Tumulus]{.type}

Localisation

Altitude : 505m.

Il est situé dans le premier col à l'Est de celui où se trouve *Nabalatz T1* (et la BF 50), sur terrain plat, à 50m à l'Est du point déclive de son col, et tangent à la piste ascendante.

Description

Tumulus de terre de 4m de diamètre et 0,40m de haut, avec une légère dépression centrale.

Historique

Monument découvert par [Blot J.]{.creator} en [mars 2012]{.date}.

### [Sare]{.spatial} - [Nabalatz]{.coverage} - [Dalle plantée]{.type}

Localisation

Altitude : 490m.

Elle est sur la droite d'une piste ascendante peu marquée qui rejoint directement la route forestière des Palombières à la ligne de crête, des cote 465 à 505.

Description

Dalle de schiste [grés]{.subject}eux de forme triangulaire plantée dans le sol, recouverte de mousse, inclinée vers l'Est, mesurant 1,30m de haut, 1m à sa base et 0,10m d'épaisseur en moyenne. On note que le gel a détaché un fragment de la partie inférieure de cette dalle, mais qui lui est cependant resté parallèle ; fragment 0,60m de haut et 0,47m de large.

Historique

Dalle découverte par [Colette Blot]{.creator} en [mars 2012]{.date}.

### [Sare]{.spatial} - [Nabarlaz]{.coverage} (ou Nayalaz) - [Tumulus]{.type}

Localisation

Carte 1245 Est Espelette.

Altitude : 430m.

Il est situé dans le col de ce nom, à 14m au Sud-Est de la BF 50, donc en territoire navarrais.

Description

Tumulus de terre, à sommet aplati, de 5m de diamètre et 0,60m de haut.

Historique

Monument découvert en [mars 1970]{.date}.

### [Sare]{.spatial} - [Olhayn]{.coverage} - [Tumulus]{.type}

Localisation

Même carte.

Altitude : 350m.

Il est situé au milieu d'un col, sur un terrain en très légère pente, à l'Ouest du mont du même nom, et de la chapelle en ruine édifiée à son sommet.

Description

Tumulus pierreux, formé de petits galets et de débris de plaquettes de [grés]{.subject} rose local, il mesure 6m de diamètre et 0,40m de haut.

Historique

Monument découvert en [novembre 1974]{.date}.

### [Sare]{.spatial} - [Palombières]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 465m.

Situé sur un replat, à un croisement de pistes forestières et pastorales.

Description

Tumulus de terre mesurant environ 8m de diamètre et 0,80m à 1m de haut - il présente une petite dépression au voisinage de son sommet.

Historique

Monument découvert par [J. Eloségui]{.creator} et « re-découvert » par [Blot J.]{.creator} en [mai 1984]{.date}. Nous publions ici ce tertre, car tous les chercheurs ou amateurs de ces monuments n'ont pas obligatoirement le temps ou la possibilité de lire l'ensemble des publications faites sur ce thème...

### [Sare]{.spatial} - [Sayberri]{.coverage} - [Cromlechs]{.type} ([?]{.douteux})

Localisation

Altitude : 480m.

Situés très exactement au milieu du col de Sayberri, au pied du mont du même nom, (506 m altitude) - à 1 kilomètre au Sud-Est des grottes de Sare.

Description

On note une dizaine de blocs rocheux, délimitant un premier cercle approximatif de 3,50m de diamètre. À 4m au Nord, peut-être existe-t-il un deuxième cercle de 3,50m de diamètre lui aussi, délimité par une quinzaine de pierres de moindre volume, au ras du sol. Enfin, à 5m à l'Ouest de ce second vestige, il pourrait y avoir deux cercles tangents, mesurant chacun 3m de diamètre et aux éléments là encore difficilement visibles.

Historique

Ces monuments, douteux, nous ont été signalés par [J. Régnier]{.creator} en [mai 2009]{.date}.

### [Sare]{.spatial} - [Sayberri]{.coverage} 1 - [Tertre d'habitat]{.type}

Localisation

Altitude : 485m.

Situé à 5m environ au Sud des ruines d'un cayolar.

Description

Tertre de terre (ou plate-forme ?) asymétrique, à pente orientée vers le Sud, mesurant 7m x 12m. La finalité n'est pas évidente...

Historique

Monument découvert par [Blot J.]{.creator} en [décembre 2013]{.date}.

### [Sare]{.spatial} - [Sualar]{.coverage} 1 - [Pierre couchée]{.type}

Localisation

Altitude : 520m.

Elle se trouve à quelques mètres à droite de la piste qui monte de Gaztenbakarre au plateau d'Altsaan.

Description

Bloc de [grés]{.subject} local, de forme parallélépipédique, régulière, orientée selon un axe Sud-Est Nord-Ouest, mesurant 2,50m de long, 0,80m de large et d'une épaisseur de 0,40m. Il semble bien que des traces d'épannelage puissent être notées réparties en de nombreux endroits sur la périphérie de cette pierre.

Historique

Pierre découverte par [P. Badiola]{.creator} en [mai 2011]{.date}.

### [Sare]{.spatial} - [Sualar]{.coverage} 2 - [Pierre couchée]{.type} ([?]{.douteux})

Localisation

Altitude : 515m.

Il est à une quinzaine de mètres au Nord-Est de la piste pastorale qui monte de Gaztenbakarre.

Description

Bloc de [grés]{.subject} local de forme parallélépipédique, allongé dans le sens de la pente (vers le Nord-Est). Il mesure 2,10m de long, 0,62m à son extrémité Nord-Est, 0,70m à son extrémité Sud-Ouest et 0,85m dans sa plus grande largeur. Son épaisseur est d'une vingtaine de centimètres.

Il n'y a pas de traces d'épannelage visibles. Monument douteux.

Historique

Pierre découverte par [Blot J.]{.creator} en [mai 2011]{.date}.

### [Sare]{.spatial} - [Tomba]{.coverage} - [Cromlech]{.type} ([?]{.douteux})

Localisation

Altitude : 476m.

Description

Petit cercle de 1m de diamètre, légèrement surélevé, constitué d'une quinzaine de pierres particulièrement visibles dans la partie Ouest du cercle.

Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2012]{.date}.

### [Sare]{.spatial} - [Tomba]{.coverage} 1 - Tumulus ([?]{.douteux})

Localisation

Altitude : 470m.

Il est à 2m au Nord de la piste (herbeuse) qui se rend vers le *Dolmen de Tomba*, vers la partie haute d'une petite colline et à 15m au Sud du pointement rocheux qui en marque le sommet.

Description

Tumulus circulaire, constitué de pierres enfouies dans le sol, qui devait former un tumulus en galette aplatie de 4m de diamètre environ et 0,20m de haut. ; une dépression circulaire de 1m de diamètre, remplie de pierres, (reste d'une probable fouille clandestine) se voit au centre de ce tumulus dont seule la partie Est reste encore la plus visible.

Historique

Tumulus découvert par [Blot J.]{.creator} en [octobre 2012]{.date}.

### [Sare]{.spatial} - [Tomba]{.coverage} 2 - Tumulus ([?]{.douteux})

Localisation

Altitude : 471m.

Ce tumulus se trouve à une quinzaine de mètres au Nord-Ouest de *Tomba 1 - Tumulus (?)*.

Description

Tumulus pierreux là encore très détérioré par une probable fouille ancienne : de sa forme circulaire primitive il ne reste en effet qu'un bourrelet pierreux de 1m de large environ, qui représente la partie Est de ce tumulus. Dans la dépression centrale due à la fouille on distingue 3 élément de quartzite qui pourraient faire partie d'une ciste centrale mesurant 0,45m x 0,40m.

Historique

Tumulus découvert par [Blot J.]{.creator} en [novembre 2012]{.date}.

### [Sare]{.spatial} - [Trois Fontaines]{.coverage} 1 - [Tumulus]{.type} ou [Tertre d'habitat]{.type} ([?]{.douteux})

Localisation

Altitude : 500m.

Il est à 30m au Nord-Est de la lisière du petit bois des Trois fontaines, à 150m à l'Ouest de *Urkila 3 - Tumulus*, et à 80m environ à l'Ouest de la voie ferrée ascendante. Deux autres tumulus se trouvent plus à l'Ouest, sur la commune d'Ascain.

Description

Tumulus de terre et de pierres, de faible hauteur (0,20m), érigé sur terrain plat, d'un diamètre de 8m environ. Au centre une dépression de 3m de large et 0,70m de profondeur, en partie comblée de grosses pierres. On note une dalle plantée à 2m au Nord-Est.

Historique

Tumulus découvert par [Meyrat F.]{.creator} en [avril 2012]{.date}.

### [Sare]{.spatial} - [Trois Fontaines]{.coverage} 1 - [Dalle plantée]{.type}

Localisation

À 2m au Nord-Est du tumulus *Trois Fontaines n°1*.

Description

Dalle de [grés]{.subject} local, parallélépipédique, verticalement plantée dans le sol, mesurant 0,84m de haut, 9 centimètres d'épaisseur, 0,30m à la base et 0,27m dans sa partie haute.

Historique

Dalle trouvée par [Meyrat F.]{.creator} en [avril 2012]{.date}.

### [Sare]{.spatial} - [Trois Fontaines]{.coverage} 1 - [Dolmen]{.type}

Localisation

Altitude : 540m.

Description

Trois dalles sont alignées, plantées verticalement dans un tumulus pierreux de 0,15m de haut environ. Elles mesurent 0,60m de long chacune et 0,20m de haut ; d'autres apparaissent plus ou moins enfouies à l'extrémité Sud-Ouest de cet alignement ; enfin une autre dalle, au Nord-Est, elle aussi verticalement plantée, est à angle droit avec les précédentes, (contre un arbre poussant à cet endroit), et semble déterminer, avec elles, les parois d'une [chambre funéraire]{.subject} à grand axe Nord-Ouest Sud-Est ([?]{.douteux}).

Historique

Monument trouvé pat [Meyrat F.]{.creator} en [avril 2012]{.date}.

### [Sare]{.spatial} - [Trois Fontaines]{.coverage} 2 - [Dolmen]{.type}

Localisation

Il se trouve à 6m à l'Ouest Nord-Ouest de *Trois Fontaines 1 - Dolmen*.

Description

Un ensemble de 4 dalles délimite une [chambre funéraire]{.subject} à grand axe Nord-Ouest Sud-Est de 2,80m de long et 1m de large environ.

Les dalles sont toutes couchées, éversées. Celles du côté Nord-Est mesurent respectivement 0,90m et 0,66m ; celles du côté Sud-Ouest, mesurent 0,65m et 0,95m de long. Un arbre pousse à l'extrémité Nord-Ouest du monument.

Historique

Monument découvert par [Meyrat F.]{.creator} en [avril 2012]{.date}.

### [Sare]{.spatial} - [Uhardiko Etxola]{.coverage} - [Pierre couchée]{.type}

Localisation

Altitude : 345m.

Elle est située à 1,50m au Nord de la piste et à 3m au Nord des ruines d'un ancien habitat.

Description

Dalle de [grés]{.subject} grossièrement parallélépipédique, allongée selon un axe Ouest Nord-Ouest, Est Sud-Est. Elle mesure 2,12m de long, 0,59m de large et 0,36m d'épaisseur en moyenne. Son pourtour présente de nombreuses traces d'épannelage.

Historique

Pierre découverte par [Badiola P.]{.creator} en [décembre 2011]{.date}.

### [Sare]{.spatial} - [Urio]{.coverage} - [Tumulus]{.type} ([?]{.douteux})

Localisation

Situé dans une fougeraie qui domine le rio Urio, ce petit tumulus pierreux mesure 2,45m de diamètre et 0,20m de haut avec un centre légèrement excavé.

Description

Il semblerait qu'il puisse faire partie d'une structure rectangulaire mesurant 7m x 3,50m, balisée par de gros galets.

À 16m de distance, au Sud-Ouest, il y aurait peut-être un cercle de 5m de diamètre constitué de 16 pierres.

Historique

Ces deux monuments - douteux - nous ont été signalés par [Duvert M.]{.creator} en [octobre 2011]{.date}.

### [Sare]{.spatial} - [Urkila]{.coverage} 1 - [Monolithe]{.type}

Localisation

Altitude : 555m.

Cette dalle gît sur la gauche de la piste pastorale qui se rend à Urkilepoa, à environ 150m du site où se croisent les funiculaires.

Description

Dalle de [grés]{.subject} rose, mesurant 3,10m de long, 1,70m de large et 0,30m à 0,40m d'épaisseur. Quelques traces d'épannelage sont visibles à son extrémité Est, visiblement arrondie.

Historique

Monolithe découvert par [Blot J.]{.creator} en [juin 1982]{.date}.

### [Sare]{.spatial} - [Urkila]{.coverage} 2 - [Monolithe]{.type}

Localisation

Altitude : 625m.

Elle est isolée sur un sol en pente marquée vers le Nord et à une cinquantaine de mètres à l'Est de la voie ferrée qui monte à Larraun.

Description

Beau bloc de [grés]{.subject} presque rectangulaire, dont les 4 bords sont parfaitement épannelés (travail récent ?). Il mesure 2,85m de long, 1,12m dans sa plus grande largeur, 0,96m dans la plus petite, et 0,26m d'épaisseur.

Historique

Monolithe découvert par [P. Badiola]{.creator} en [décembre 2011]{.date}.

### [Sare]{.spatial} - [Urkila]{.coverage} 3 - [Tumulus]{.type}

Localisation

Altitude : 543m.

Il est situé à quelques mètres à l'Est de la voie ferrée qui monte à Larrun.

Description

Il est très semblable aux tumulus 1 et 2 que nous avons déjà décrits [@blotInventaireMonumentsProtohistoriques2010, p. 31] : tumulus de terre, à sommet aplani, de 5m à 6m de diamètre et plus haut à l'Est qu'à l'Ouest, cette dissymétrie évoque un tertre d'habitat, ce que nous ne pensons pas qu'il soit.

Historique

Tumulus découvert par [Meyrat F.]{.creator} en [janvier 2012]{.date}.

### [Sare]{.spatial} - [Urkila]{.coverage} n°1 - [Tumulus]{.type} ([?]{.douteux})

Localisation

Altitude : 590m.

Description

« Tumulus » situé à une quinzaine de mètres à l'Est de la voie ferrée du train à crémaillère, formé de terre et de pierres. Sensiblement circulaire, il est érigé sur un terrain en pente légère, incliné vers le Nord. Mesure 6m de diamètre et 1,50m de haut ; il a plus l'aspect d'un tertre d'habitat (que nous ne pensons pas qu'il soit), que d'un tumulus vrai ou dolménique ; tas de cailloux de construction de la voie ferrée ?

Historique

Découvert par [J. Blot]{.creator} en [mars 2010]{.date}.

### [Sare]{.spatial} - [Urkila]{.coverage} n°2 - [Tumulus]{.type} ([?]{.douteux})

Localisation

Altitude : 650m.

Description

Il est situé dans la concavité de la voie ferrée, à 30m à l'Ouest de celle-ci, sur terrain en légère pente. Ce tumulus de terre et de pierres, ayant sensiblement la même forme que le précédent, mesure 5m de diamètre et 1m de haut environ ; il nous inspire les mêmes commentaires que le « tumulus » précédent, quant à sa nature....

Historique

Découvert par [I. Gaztelu]{.creator} en [janvier 1988]{.date} (dénommé dolmen).

### [Sare]{.spatial} - [Urkila]{.coverage} - [Dalle plantée]{.type} ([?]{.douteux})

Localisation

Altitude : 540m.

Elle est à l'Ouest de la piste qui descend d'Athekaleun, et à 150m au Nord Nord-Est de *Urkila n°1 - Tumulus (?)*.

Description

Dalle verticalement plantée dans le sol, de 0,80m de haut, 1,30m à sa base et 0,10m d'épaisseur.

Historique

Découverte par [J. Blot]{.creator} en [mars 2010]{.date}.

### [Sare]{.spatial} - [Uzkaïne]{.coverage} - [Pierre plantée]{.type} ([?]{.douteux})

Nous avons décrit dans le **Tome 3** de notre Inventaire [@blotInventaireMonumentsProtohistoriques2011 p.25], un probable tumulus situé à la confluence des 3 communes de Sare, Saint-Pée et Ascain, sur lequel est érigée une borne ; celle-ci mesure 1,15m de haut, 0,57m de long et 0,46m de large à sa base ; bien que la date 1955 soit gravée à son sommet, avec les traits de délimitation des trois communes, nous émettons l'hypothèse qu'elle soit bien plus ancienne que cette date... ce qui n'exclut nullement l'existence d'un tumulus sous-jacent.

Altitude : 223m.

### [Sare]{.spatial} - [Uzkaïne]{.coverage} - [Tumulus]{.type} ([?]{.douteux})

Localisation

Altitude : 223m.

Ce probable tumulus se situe au point de rencontre des limites des trois communes de Sare, Saint-Pée-sur-Nivelle, et Ascain.

Description

Une borne plantée en son milieu (une croix est gravée à son sommet ainsi que la date : 1995), matérialise ces délimitations administratives. Ce tumulus de terre et de pierres, mesure 8m de diamètre et environ une quinzaine de centimètres de haut. Nous ne pensons pas qu'il s'agisse des traces des travaux de terrassement nécessités pour planter cette borne, compte tenu de ses dimensions et de sa structure ; par ailleurs il est sur un sol plat, dans un emplacement dominant, au bord de la piste pastorale, en ce lieu privilégié qui a même été choisi pour y ériger un calvaire, à 5m au Sud-Ouest.

Historique

Monument découvert par [J. Blot]{.creator} en [décembre 2010]{.date}.

### [Sare]{.spatial} - [Uzkizelaikoharri]{.coverage} - [Monolithe]{.type}

Localisation

Altitude : 510m.

Il se trouve au pied d'une colline, sur un terrain en pente douce vers le Sud.

Description

Dalle de [grés]{.subject} de forme parallélépipédique allongée au sol suivant un axe Nord-Sud, mesurant 1,46m de long, 0,55m de large à sa base Sud, 0,32m à son sommet Nord, plus étroit ; son épaisseur varie de 8 à 18 centimètres. Elle présente des traces d'épannelage sur tout son pourtour.

Historique

Monolithe découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

### [Sare]{.spatial} - [Uzkizelaikoharri]{.coverage} - [« Rempart » mégalithique]{.type}

Localisation

Altitude : 553m.

Cette colline surplombe la piste qui monte au col d'Olhain.

Description

On note, barrant le sommet ci-dessus un « alignement » de dalles « mégalithiques » au sens littéral du terme (certaines atteignent plus de 2m de haut, 2m de large et 0,80m d'épaisseur), c'est pourquoi nous le signalons ici. Ces dalles ne sont pas fichées en terre mais simplement posées et calées sur d'autres dalles ou blocs, ce qui leur donne un aspect incliné, soit vers l'amont soit vers l'aval de la pente. Certaines gisent au sol, de toute façon elles ne forment pas un « obstacle » continu.

Historique

Ce remarquable ensemble a été noté par [Louis de Buffières]{.creator} en [février 1992]{.date}.

### [Sare]{.spatial} - [Xabaloa]{.coverage} - [Dolmen]{.type}

Localisation

Carte 1245 Est Espelette.

Altitude : 170m.

Nous ne voulons pas terminer ce chapitre sur Sare sans évoquer un monument actuellement disparu, et que nous avons eu beaucoup de mal à localiser.

Ce monument, était érigé sur une petite colline située au flanc Nord-Est du mont Ibantelli, qui domine, au Sud, le ruisseau Xabaloa ; il était à 100m à l'Ouest au-dessus de la route qui mène à Etchalar, sur la gauche du chemin rural reliant cette route à Xabaloa. Ce monument a été découvert en [1944]{.date} [@barandiaranCronicaPrehistoriaPirineo1951a p. 243].

Description

Tumulus pierreux de 6m de diamètre et 0,60m de haut, au centre duquel 2 dalles verticales laissaient entrevoir la forme rectangulaire de la [chambre funéraire]{.subject}, environ 1,50m x 1m et 1m de profondeur, orientée Est-Ouest. La dalle du côté Sud était marquée d'une croix gravée sur la face externe, tandis que l'autre avait un rhombe. Ces dalles ont été enlevées pour la construction de la maison Mailuen Bordaberri. Il existe, très proche, sur cette colline, une borde neuve appelée *Axuria*.

### [Sare]{.spatial} - [Xantakoenea]{.coverage} - [Monolithe]{.type}

Localisation

Altitude : 190m.

Il gît sur le bord du petit chemin qui part de Xantakoenea (altitude : 162m) et rejoint plus haut le GR 10 ; il ne paraît pas « en place » mais « en cours de déplacement ».

Description

Bloc de [grés]{.subject} allongé à grand axe Sud-Ouest Nord-Est (extrémité la plus étroite au Sud-Ouest), mesurant 3,15m de long et 0,90m de large au maximum. Son épaisseur varie de 0,45m à son extrémité Nord-Est, à 0,16m au Sud-Ouest. Il semble y avoir quelques traces d'épannelage aux deux extrémités.

Historique

Monolithe découvert par [Meyrat F.]{.creator} en [mai 2011]{.date}.

### [Sare]{.spatial} - [Xarita]{.coverage} 1 - [Tumulus]{.type}

Description

Petit tumulus circulaire de 4m de diamètre et 0,40m de haut, formé de terre et de pierres ; il paraît indépendant des éléments environnants.

Historique

Tumulus découvert par [Blot C.]{.creator} en [2013]{.date}.

### [Sare]{.spatial} - [Xarita]{.coverage} - [Monolithe]{.type}

Localisation

Altitude : 405m.

Il faut emprunter la piste bien visible qui part sur la gauche de la route, à 250m environ du col de Lizarrieta (en direction de Sare). Le monolithe gît sur le bord Est de la piste à environ 900m du point de départ. Cette piste, étroite en général, longe le flanc Est de l'Ibantelli, et ce monolithe est situé à un élargissement particulièrement notable du terrain, bien plat à cet endroit.

Description

Il se présente sous la forme d'une belle dalle de [grés]{.subject} [triasique]{.subject}, couchée au sol, de forme triangulaire à sommet Sud Sud-Ouest, mesurant 2,24m de long, 0,98m à la base, et 0,94m en son milieu. Son épaisseur est de 0,35m à 0,40m à son bord Est, de 0,20m à sa base, qui présente des traces de délitement probablement naturel ; l'épaisseur du bord Ouest est difficilement appréciable, étant enfoui dans le sol. Il ne semble pas y avoir de traces d'épannelage ; toutefois cette dalle isolée, loin de tout éboulement pierreux ou de tout autre fragment rocheux, attire l'attention en cet emplacement.

Historique

Monolithe découvert par [Luis Millan]{.creator} en [janvier 1990]{.date}.

### [Sare]{.spatial} - [Xarita]{.coverage} 3 - [Dolmen]{.type}

Localisation

Il est situé sur une croupe du flanc Sud de l'Ibantelli, et domine (à une centaine de mètres au Nord) l'ancien sentier minier horizontal qui court sur ce flanc. Ce monument a été érigé sur un terrain en pente vers le Sud, rendu horizontal par d'importants travaux de soutènement (apport de grosses pierres et de terre).

Description

On ne peut pas parler d'un tumulus, mais d'une aire circulaire horizontale de 7m de diamètre, dont la demi-circonférence, côté Sud, est soutenue par de nombreuses pierres de volume important (certaines de 0,90m x 0,60m) en deux ou 3 assises suivant les endroits. Ce soutènement se voit aussi, dans une moindre mesure, à l'Est comme à l'Ouest. Au Nord, seules quelques rares pierres balisent la circonférence.

Au centre de ce cercle apparaît la partie supérieure d'une belle dalle de 1,93m de long, 0,29m de large (à l'Ouest) et 0,15m (à l'Est) ; elle n'émerge du sol que de 0,15m. Elle semble bien former la paroi Sud d'une [chambre funéraire]{.subject} orientée Est-Ouest qui aurait eu 1m de large.

On note, à 2m au Nord-Ouest de cette dalle, une autre dalle couchée de 1,04m x 0,56 et 0,10m d'épaisseur qui aurait pu faire partie de la paroi Nord de la chambre ou de la [couverture]{.subject}.

Historique

Monument découvert par [L. Millan]{.creator} en [1990]{.date}.

### [Sare]{.spatial} - [Xarita]{.coverage} 6 - [Dolmen]{.type} ([?]{.douteux})

Localisation

Altitude : 313m.

Ce monument est difficile à atteindre ; il faut tout d'abord emprunter la piste précédemment décrite à propos du *monolithe Xarita*, et c'est environ à 1000m du point de départ que l'on distingue, plus ou moins facilement suivant la végétation, un petit sentier qui se dirige vers la droite et le bas, en suivant une ligne de crête. Ce sentier aboutit à un petit ensellement où seules les pierres de ces 2 monuments (*Xarita 6* et *Xarita 7*) sont visibles.

Description

Ce monument, si cela en est un, se résume à une volumineuse dalle de grès [triasique]{.subject}, couchée sur le sol, et présentant, à son bord Ouest une série de pierres enfouies dans le sol, pouvant faire partie d'une [chambre funéraire]{.subject}. Toutefois, nous ne pouvons pas éliminer totalement l'hypothèse que cette dalle fasse même partie de *Xarita 7 - Dolmen (?)*.

La dalle de grès, de forme grossièrement rectangulaire, mesure 1,90m dans son plus grand axe Est-Ouest, 1,35m dans l'axe Nord-Sud, et en moyenne 0,37m d'épaisseur. À son extrémité Nord elle repose sur, semble-t-il, une deuxième dalle de moindre taille, qui pourrait n'être qu'un fragment séparé d'elle-même (délité par le gel ?). Cette deuxième dalle, épaisse en moyenne de 0,19m, mesure 1,13m de long et 0,88m de large.

La « [chambre funéraire]{.subject} », rectangulaire, orientée Est-Ouest, aurait aux environs de 3m de long et 1,50m de large et serait délimitée par une quinzaine de blocs ou de dalles de grès de volume variable, mais modéré, bien enfouis dans le sol. Ils sont surtout bien visibles en secteur Nord.

Historique

Monument découvert par [I. Txintxurreta]{.creator} le [19 janvier 2019]{.date}. (Monument douteux).

### [Sare]{.spatial} - [Xarita]{.coverage} 7- [Dolmen]{.type} ([?]{.douteux})

Localisation

Il est situé à 4m au Nord de *Xarita 6 - Dolmen (?)*.

Description

Monument se présentant sous la forme d'une série de 4 dalles de [grés]{.subject} [triasique]{.subject} enfoncées dans le sol selon un axe Est-Ouest (et plus ou moins éversées vers le Sud) et pouvant former la paroi Sud d'une [chambre funéraire]{.subject} de près de 5m de long et 1m de large, orientée Est-Ouest. Il semblerait, de plus, que cette longue [chambre funéraire]{.subject} soit divisée en deux par une dalle médiane, l'une à l'Est, de 2,10m de long et l'autre à l'Ouest, de 2,50m de long.

*La paroi Sud de la chambre* funéraire est constituée de 4 dalles. En allant de l'Est vers l'Ouest :

- la première, presque verticale, et rectangulaire, mesure 0,67m x 0,48m et 0,14m d'épaisseur.

- la seconde, nettement plus inclinée, mesure 1,67m x 0,80m et 0,20m d'épaisseur. ; elle est séparée de la première par 0,17m.

- la troisième, complètement couchée sur le sol, mesure 1,18m x 0,96m et 0,16m d'épaisseur et est distante de 0,40 de la seconde.

- la quatrième dalle, elle aussi couchée sur le sol, est nettement plus modeste et ne mesure que 0,75m x 0,57m et 0,12m d'épaisseur ; la distance entre elle et la troisième dalle est de 0,46m.

*La paroi Est de la chambre* pourrait être marquée par 2 dalles ; la première, couchée au sol, dans le prolongement de la paroi Sud, mais perpendiculaire à elle, mesurant 0,92m x 0,37m et 0,12m d'épaisseur. Une seconde dalle, rectangulaire, est située à l'extrémité Nord de la précédente et mesure 0,58m x 0,44m.

*Une dalle médiane*, semble indiquer une séparation de la [chambre funéraire]{.subject} en 2 parties et mesure 0,84m x 0,22m, et 0,10m d'épaisseur.

On ne distingue pas d'éléments pouvant figurer les parois Nord et Ouest de la [chambre funéraire]{.subject}.

On remarquera que toutes ces dalles, par leur présence et leur disposition évoquent une action anthropique, (de même que la volumineuse dalle de *Xarita 6*, qui ne paraît pas être venue là par l'effet du hasard ou d'une quelconque solifluxion, cet ensellement étant en effet dominé à l'Ouest et à l'Est par deux éminences de terrain).

Historique

Monument découvert par [I. Txintxurreta]{.creator} le [19 janvier 2019]{.date}.

### [Sare]{.spatial} - [Xominen]{.coverage} 5 - [Dolmen]{.type}

Localisation

Altitude : 260m.

Il se trouve à 3m à l'Ouest du chemin qui part de la route goudronnée desservant le nouveau quartier construit au-dessus et au Sud du col de Saint-Ignace.

Description

On peut noter 4 dalles de [grés]{.subject} local dont 2 quasi verticales formant la paroi Sud d'une [chambre funéraire]{.subject} mesurant environ 1,90m x 0,90m, à grand axe Est Sud-Est, Ouest Nord-Ouest.

La plus grande dalle, au Sud-Est mesure 0,60m de long, autant de haut et 0,10m d'épaisseur ; tangente à elle, une seconde dalle la prolonge à l'Ouest, (0,50m de long, 0,24m de haut, 0,10m d'épaisseur). Deux autres dalles plus ou moins couchées au sol compètent la chambre au Nord-Est et au Nord-Ouest. Il semble qu'on puisse distinguer la présence d'un tumulus de faible hauteur, de 5m de diamètre environ, érigé sur un terrain en légère pente vers l'Est.

Historique

Monument découvert par [P. Badiola]{.creator} en [mai 2011]{.date}.

### [Sare]{.spatial} - [Xominen]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 230m.

Il est sur le plateau où se trouvaient les *dolmens 1*, *2*, *3*, *4* et *5* détruits depuis [@blotNouveauxVestigesMegalithiques1971 p 10]. On le voit à l'angle que forment la route goudronnée (privée) et une murette de séparation (à 2m à l'Est de la route et à 2m au de la murette).

Description

Tumulus de terre de 5m de diamètre, mais de faible hauteur (0,25m environ), présentant en son centre une dépression de 1m de diamètre et de quelques centimètres de profondeur.

On peut noter 3 pierres aux flancs du tumulus et 3 autres à sa périphérie.

Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

### [Sare]{.spatial} - [Xominen]{.coverage} 1 et 2 - [Cromlechs]{.type} ([?]{.douteux})

Le premier se trouve à 2m au Nord de *T1*, et le second à 5m à l'Ouest de *T1* ; seraient délimités par quelques pierres au ras du sol - Douteux - ([Blot J.]{.creator}, [octobre 2011]{.date}).

### [Sare]{.spatial} - [Zuhamendi]{.coverage} 5 - [Tumulus]{.type} ([?]{.douteux})

Localisation

Altitude : 214m.

Ce probable tumulus est situé à une bonne centaine de mètres au Sud du *Tumulus de Bixustia*, (lui-même fort dégradé maintenant et coupé en deux par une haie). Il est en position dominante et à une vingtaine de mètres à l'Ouest d'une autre haie qui domine le chemin d'accès à ce secteur.

Description

Monument actuellement peu visible, ayant été en partie arasé par les labours de confection d'une prairie artificielle. Il mesure environ 11m de diamètre et une trentaine de centimètres de haut.

Historique

Ce monument nous a été signalé par [Martinez-Manteca A.]{.creator} en [novembre 2010]{.date}.

### [Sare]{.spatial} - [Zuhamendi]{.coverage} 6 - [Tumulus]{.type} ([?]{.douteux})

Localisation

Altitude : 200m.

Ce probable tumulus est situé en terrain plat, et sur la ligne de crête de la prairie - dans la même prairie artificielle que celle où se trouve le tumulus *Zuhamendi 3* que nous avons fouillé en 1975.

Description

Il se présente sous la forme d'un léger relief circulaire de 6m de diamètre et d'une dizaine de centimètres de haut. Quelques pierres apparaissent en surface.

Historique

Tumulus trouvé par [J. Blot]{.creator} en [décembre 2010]{.date}.

## [Sare]{.spatial} - Les cromlechs de [Gastenbakarre]{.coverage}

### [Sare]{.spatial} - [Gastenbakarre]{.coverage} 1 - [Cromlech]{.type}

Localisation

Altitude : 265m.

Description

Une petite murette de dalles superposées, particulièrement visible dans le secteur Sud Sud-Est, délimite un cercle de 5m de diamètre dont le centre paraît avoir été légèrement excavé (fouille ancienne ?).

Historique

Monument découvert par [J. Blot]{.creator} en [mai 2009]{.date}.

Nous avons noté trois autres cercles au Sud-Est de ce dolmen, sur le replat déjà cité, au Nord-Ouest de la cote 271.

### [Sare]{.spatial} - [Gastenbakarre]{.coverage} 2 - [Cromlech]{.type}

Il est situé à 20m à l'Est Sud-Est de *Gastenbakarre n°4 - Dolmen*.

Quinze à vingt petites dalles de [grés]{.subject}, au ras du sol, délimitent un cercle de 2,70m de diamètre. Elles sont plus abondantes et plus grandes dans le secteur Sud-Ouest du cercle.

### [Sare]{.spatial} - [Gastenbakarre]{.coverage} 3 - [Cromlech]{.type}

Situé à 18m au Sud-Est du précédent. Une vingtaine de petites dalles blanches bien visibles, profondément enfoncées dans le sol, délimitent un cercle de 3,20m de diamètre. Elles sont plus abondantes dans le secteur Ouest du cercle. Une dalle rectangulaire, plus importante que les autres (0,46m x 0,46m) apparaît dans le secteur Sud-Est.

### [Sare]{.spatial} - [Gastenbakarre]{.coverage} 4 - [Cromlech]{.type}

Le plus au Sud-Est de ces trois derniers cercles, à 15mètres au Sud-Est du précédent.

Altitude : 265m.

Il mesure 3m de diamètre, et est délimité par une quarantaine de petites dalles de [grés]{.subject} gris, au ras du sol. En secteur Ouest du cercle, là où elles sont plus nombreuses, l'une d'elles est un peu plus saillante que les autres (0,20m de haut).

Historique

Un de ces quatre cercles nous a été signalé par [L. Millan San Emeterio]{.creator}, en [mars 2009]{.date} ; les trois autres par [J. Blot]{.creator} en [mai 2009]{.date}.

## [Sare]{.spatial} - Les 3 dolmens de [Gaztenbakarreko erreka]{.coverage}

Ces trois dolmens se trouvent en contre-bas de la piste qui part du menhir de Gaztenbakarre, en direction du Sud-Ouest et à environ 100m de celui-ci.

### [Sare]{.spatial} - [Gaztenbakarreko erreka]{.coverage} 1 - [Dolmen]{.type}

Localisation

Le plus proche de la piste, à 40m environ à l'Est sur un léger replat.

Altitude : 265m.

Description

Tumulus pierreux de dalles et de blocs de [grés]{.subject}, de 5m de diamètre environ, très peu élevé. De la [chambre funéraire]{.subject}, orientée Est-Ouest, il ne reste que deux dalles. La première, presque verticale, est une dalle trapézoïdale présentant des traces d'épannelage, de 0,94m de haut, mesurant 1,40m à la base, 0,40m au sommet, et 0,25m d'épaisseur à sa base. Le seconde qui lui est perpendiculaire à son extrémité Ouest, est de dimensions plus modestes : 0,70m à la base, 0,45m de haut et 0,10m d'épaisseur, légèrement inclinée en dedans.

### [Sare]{.spatial} - [Gaztenbakarreko erreka]{.coverage} 2 - [Dolmen]{.type}

Localisation

Altitude : 250m.

Situé à 40m environ à l'Est de *Gaztenbakarreko erreka 1 - Dolmen*, et légèrement plus bas (altitude 254m).

Description

Tumulus pierreux de 6m de diamètre et de faible hauteur, au centre duquel se voit une seule dalle légèrement inclinée, de forme rectangulaire et présentant des traces d'épannelage. Elle mesure 0,98m à sa base, 0,80m de haut et 0,16m d'épaisseur. L'axe de cette dalle est Nord-Sud ; il ne reste en place aucune autre dalle de la [chambre funéraire]{.subject}, orientée Est-Ouest, exceptée, gisant sur le flanc Sud-Est du tumulus une belle dalle de 1,20m de long et 0,90m de large.

### [Sare]{.spatial} - [Gaztenbakarreko erreka]{.coverage} 3 - [Dolmen]{.type}

Localisation

Il est situé à 10m à l'Est de *Gaztenbakarreko erreka 2 - Dolmen* et légèrement plus haut (255 m d'altitude).

Description

Tumulus bien visible de 5,70m de diamètre, formé de gros blocs de [grés]{.subject} gris. De la [chambre funéraire]{.subject} orientée Est Nord-Est, Ouest Sud-Ouest, il ne reste en place qu'une dalle presque verticale, de forme grossièrement rectangulaire, mesurant 0,75m à sa base, 0,80m de haut et 0,13m d'épaisseur. À côté d'elle, au Sud, gît un bloc de [grés]{.subject} qui a pu faire partie des parois de la chambre.

Historique

Ces 3 monuments ont été découverts par [I. Txintxuretta]{.creator} en [2009]{.date}.

### [Sare]{.spatial} - [Gaztenbakarreko bidea]{.coverage} - [Dolmen]{.type}

Localisation

Il est situé à environ 200m au Sud du menhir, à une vingtaine de mètres en contre-bas de la piste.

Altitude : 265m.

Description

Erigé sur un terrain en légère pente, on note un tumulus pierreux de 5 m de diamètre et de faible hauteur, délimité par un très probable péristalithe.

De la [chambre funéraire]{.subject}, orientée Est Nord-Est, Ouest Sud-Ouest, il ne reste que la dalle Ouest, rectangulaire, mesurant 1,20m à sa base et 0,47m de haut, légèrement inclinée vers l'Est.

Historique

Monument découvert par [J. Blot]{.creator} en [mai 2009]{.date}.

### [Sare]{.spatial} - [Iratzeburua]{.coverage} - [Dolmen]{.type}

Localisation

Il est situé à une centaine de mètres au-delà et au-dessus de la bergerie en ruine que longe la piste.

Altitude : 295m.

Description

Erigé sur un terrain en pente, on note un tumulus de faible hauteur d'un diamètre d'environ 6m constitué de terre et de quelques pierres peu visibles. Au centre, la [chambre funéraire]{.subject}, orientée Nord-Est Sud-Ouest, est nettement délimitée par un ensemble de dalles émergeant de quelques centimètres au-dessus du sol. Au Sud, une dalle légèrement inclinée vers le Nord, mesurant 0,47m à sa base, 0,40m au sommet, 0,38m de haut et 0,11m d'épaisseur. À l'Est, et perpendiculaire à la précédente, une seule dalle de 0,63m de long, 0,20m de haut et 0,6m d'épaisseur. Enfin à l'Ouest, deux dalles : l'une de 0,95m de long, 0,20m de haut et 0,10m d'épaisseur, l'autre, la chevauchant de quelques centimètres, mesure 0,60m de long, 0,32m de haut et 0,8m d'épaisseur.

Historique

Ce monument a été découvert par [I. Txintxuretta]{.creator} en [2009]{.date}.

### [Sare]{.spatial} - [Iratzeburua]{.coverage} - [Tumulus-cromlech]{.type}

Localisation

Situé à une centaine de mètres au Sud Sud-Ouest de *Iratzeburua - Dolmen*.

Altitude : 330m.

Description

Sur une très légère éminence, on note un cercle de 5m de diamètre, avec une légère dépression centrale, délimité par une couronne de nombreuse petites dalles de [grés]{.subject} plus ou moins superposées.

Historique

Monument re-découvert et précisé par [J. Blot]{.creator} en [mai 2009]{.date}.

## [Sare]{.spatial} - Cromlechs et tumulus de [Kondendiagako lepoa]{.coverage}

Nous ne voulons pas attribuer indûment à la commune de Sare ce qui revient à celle de Vera. Si nous traitons ici de ces monuments, c'est, d'une part, à cause de leur extrême proximité avec la ligne frontière, d'autre part du fait qu'ils se trouvent à l'arrivée, à ce col, d'une importante et antique piste pastorale reliant Sare à Vera.

Localisation

Au col Kondendiaga - où se trouve la BF 32 - situé juste au-dessus du col de Lizuniaga.

Altitude : 315m.

Description

Tous ces monuments apparaissent sur un sol gazonné, parfaitement dégagé de toutes autres pierres par ailleurs.

### [Sare]{.spatial} - [Kondendiagako lepoa]{.coverage} 1 - [Cromlech]{.type}

Localisation

Il est situé à 10m au Sud Sud-Ouest de la « Venta Negra », et à 30m au Nord Nord-Est de la BF 32.

Description

On note une légère surélévation de terrain en forme de couronne de 3m de diamètre, à la surface de laquelle apparaissent une vingtaine de pierres, au ras du sol mais bien visibles. Quelques pierres marquent le centre du cercle. On allume malheureusement parfois des foyers au centre de ce monument...

### [Sare]{.spatial} - [Kondendiagako lepoa]{.coverage} 2 - [Cromlech]{.type}

Description

Tangent Sud du précédent. Cercle de 3m de diamètre, délimité par quelques pierres apparaissant sur une légère surélévation en couronne surtout marquée en secteur Est ; on note une pierre centrale.

### [Sare]{.spatial} - [Kondendiagako lepoa]{.coverage} 3 - [Cromlech]{.type}

Description

Monument douteux. On note cependant, à 1m à l'Ouest Sud-Ouest du *n°1*, une dizaine de pierres qui paraissent délimiter un cercle de 2m de diamètre.

### [Sare]{.spatial} - [Kondendiagako lepoa]{.coverage} - [Tumulus]{.type}

Localisation

Situé à 6m à l'Ouest Nord-Ouest de *Kondendiagako lepoa 1 - Cromlech*.

Description

On remarque une quinzaine de pierres groupées en un amas circulaire dont la surface apparaît au-dessus du sol. Hélas situé sur une zone de grand passage, il a été fortement détérioré.

Historique

Monuments découverts par [J. Blot]{.creator} en [mai 2009]{.date}.
