# Larrau

### [Larrau]{.spatial} - [Abarrakiko Pekoa]{.coverage} - [Tertres d'habitat]{.type}

Localisation

Altitude : 1070m.

Les 5 tertres se trouvent à droite de la route qui monte vers Abarrakia, dans une prairie en pente douce.

Description

On peut voir éparpillés de la gauche à la droite 5 tertres d'habitat dont les dimensions varient de 6m à 8m de long et 0,80m à 1m de haut environ.

Historique

Tertres trouvés par [J. Blot]{.creator} en [octobre 2013]{.date}.

### [Larrau]{.spatial} - [Ardakhotchia]{.coverage} - [Cromlech]{.type}

Localisation

Altitude : 1109m.

On peut voir ce monument à une centaine de mètres en contre-bas et à l'Ouest de la route.

Description

Cercle bien visible de 3,50m de diamètre, délimité par une dizaine de pierres [calcaire]{.subject}, de la taille d'un gros pavé, ou plus, au ras du sol.

Historique

Cercle découvert par [J. Blot]{.creator} en [octobre 2013]{.date}.

### [Larrau]{.spatial} - [Ardakhotchia]{.coverage} (Groupe Sud-Est) - [Tertres d'habitat]{.type}

Localisation

Rappelons le *Groupe Nord*, déjà décrit [@blotSouleSesVestiges1979, p.3].

Le *Groupe Sud-Est* constitué de 3 tertres.

Altitude : 1067m ; le second est à 5m au Sud du précédent ; le troisième est à 30m au Sud-Ouest du second.

Description

Le premier et le second mesurent 10m x 10m et 0,40m de haut ; le troisième : 13m x 13m et 0,90m de haut.

Historique

Tertres découverts par [J. Blot]{.creator} en [octobre 2013]{.date}.

### [Larrau]{.spatial} - [Ardakhotchia]{.coverage} Est - [Tertre d'habitat]{.type}

Localisation

Altitude : 1085m.

Il est près du riu, en bord Est de la prairie où se trouvent *Ardakhotchia (Groupe Sud-Est) - Tertres d'habitat* et *Ardakhotchia - Cromlech*.

Description

8m à 9m de diamètre et 0,60m de haut environ.

Historique

Monument découvert par [J. Blot]{.creator} en [octobre 2013]{.date}.

### [Larrau]{.spatial} - [Bagozabalaga]{.coverage} 2 - [Tumulus]{.type}

Localisation

Carte 1446 Est Tardets-Sorholus

Altitude : 1010m.

Nous avons publié les monuments de cette région (ref)[@Blot J. 1979 b, p. 19].

Se repérer au niveau du petit col auquel on accède en suivant la piste qui vient du cayolar Idagorria, et qui se dirige d'Est en Ouest vers la grange Halzourdi. Au niveau du col naît, au Sud, une ligne de croupes menant au bois d'Etchelu, passant par Bagozabalaga. On trouve le *tumulus n°2* à 60m environ au Sud-Ouest du col. Rappelons que le *tumulus n°1* (tumulus mixte de 7,60m de diamètre et 0,40m de haut), se trouve à une soixantaine de mètres au Nord-Est du départ de la piste qui se dirige vers Bagozabalaga.

Description

Tumulus pierreux de 8m de diamètre et 0,50m de haut, constitué de nombreuses pierres [calcaires]{.subject} blanches particulièrement visibles en périphérie, sans que l'on puisse pour autant parler de péristalithe. On note une dépression centrale : fouille ancienne ?

Historique

Monument découvert en [1980]{.date}.

### [Larrau]{.spatial} - [Bagurdineta]{.coverage} - [Tertre d'habitat]{.type}

Localisation

Altitude : 1490m.

À mi-chemin de la descente vers *Bagurdineta 3 - Tumulus* et *Bagurdineta 4 - Cromlech*.

Description

Petit tertre de 4m à 5m de diamètre et 0,30m de haut, très remanié par la colluvion.

Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

### [Larrau]{.spatial} - [Betzülagagna]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 1584m.

Ce monument situé sur la piste de crête qui relie le Port de Larrau à Otxogorrigagna à l'Est, avait été décrit et publié par [nous]{.creator} en [1979]{.date} dans le [@blotSouleSesVestiges1979, p.29].

Ce tumulus de 10m de diamètre et 0,50m de haut environ, était formé de petites dalles [schiste]{.subject}uses, et présentait une légère dépression centrale. Lors d'une visite en [octobre 2017]{.date}, [F. Meyrat]{.creator} a pu constater la destruction complète de ce monument par la construction, à son sommet, d'un poste de tir à la palombe...

### [Larrau]{.spatial} - [Burkégui]{.coverage} - [Dolmen]{.type}

Localisation

Carte 1447 Nord Larrau.

Altitude : 1060m.

Il est situé au Nord-Ouest de l'important virage à angle droit que décrit la route qui monte de Larrau vers le pic d'Orhi ; on le trouve à environ 150m au Nord Nord-Ouest et en contre-bas du plateau où sont érigés les tertres de Burkégui que nous avons décrits [@blotCromlechUniteMeatse1979 p.27]. En partant de là, on suit une piste qui se rend à une cabane de chasse : le dolmen est situé à 25m à l'Est de cette cabane et d'une doline qui sert de dépotoir.

Description

Tumulus pierreux d'environ 8m de diamètre et 0,30m de haut, recouvert de fougères aigle. Au centre, une dépression de 0,90m de profondeur laisse apparaître 2 dalles épaisses en [grés]{.subject} poudingue, s'appuyant sur ses bords et légèrement inclinées vers l'extérieur. C'est tout ce qui reste d'une [chambre funéraire]{.subject}, très bouleversée par des fouilles clandestines anciennes, dont le grand axe était probablement orienté Nord-Est Sud-Ouest.

La dalle Ouest mesure 1,90m de long, 1m de large et 0,30m d'épaisseur. La dalle Sud-Est mesure 1,20m de long, 0,90m de large et 0,30m d'épaisseur. Pas de table ni de péristalithe visibles.

Historique

Dolmen signalé par [E. Dupré]{.creator} et vu par [nous]{.creator} en [octobre 1992]{.date}.

### [Larrau]{.spatial} - [Bagurdineta]{.coverage} 2 - [Tumulus]{.type}

Localisation

Carte 1447 Nord Larrau.

Altitude : 1469m.

Il est situé au Nord-Ouest du mont Odeizu-gagna, sur un petit ressaut à l'Ouest du col, et à 9m à l'Est Sud-Est du *Cromlech Bargudineta n°1* déjà publié [@blotSouleSesVestiges1979 , p.16].

Description

Tumulus terreux de 6m de diamètre et 0,40m de haut. Au centre apparaît une dépression ovale à grand axe Nord-Sud de 4m de diamètre et 0,30m de profondeur : fouille ancienne ? Quelques rares pierres sont visibles en secteur Ouest.

Historique

Monument découvert en [septembre 1979]{.date}.

### [Larrau]{.spatial} - [Bagurdineta]{.coverage} 3 - [Tumulus]{.type}

Localisation

Sur la croupe de Bagurdineta.

altitude : 1469m

Description

Tumulus de 4m de diamètre et 0,15m de haut ; de nombreuses pierres apparaissent discrètement à sa surface ; péristalithe discutable.

Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

### [Larrau]{.spatial} - [Bagurdineta]{.coverage} 4 - [Cromlech]{.type}

Localisation

Altitude : 1468m.

Dans le replat du petit col et à 2m au Nord de la piste.

Description

Petit cercle, légèrement surélevé, formé de 8 pierres peu saillantes.

Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

### [Larrau]{.spatial} - [Erroimendi]{.coverage} 1 - [Cromlech]{.type}

Localisation

Altitude : 1360m.

Il est actuellement tout au bord de la tranchée de passage de la route qui l'a en grande partie démoli ; il n'en reste que 3 pierres visibles qui peuvent délimiter un cercle de 5m de diamètre.

Historique

Le *Cromlech C1* avait été décrit en [1962]{.date} par [JM. de Barandiaran]{.creator} dans [@barandiaranProspecionesExcavacionesPrehistoricas1962 p.11], sous le nom de *Cromlech de Arhanolatze* ; à cette époque, il était intact, mesurait bien 5 mètres de diamètre et avait 9 témoins bien visibles.

### [Larrau]{.spatial} - [Erroimendi]{.coverage} 2 - [Cromlech]{.type}

Localisation

Il est à 12m au Nord de *Erroimendi 1 - Cromlech* et en bordure de la tranchée de la route.

Description

Monument intact ; petit cercle de 2,80m de diamètre, délimité par 5 blocs de [grés]{.subject} jaune.

Historique

Monument trouvé par [Blot J.]{.creator} en [avril 2015]{.date}.

### [Larrau]{.spatial} - [Erroimendi]{.coverage} 3 - [Cromlech]{.type}

Localisation

Il est à 2m environ au Nord de *Erroimendi 2 - Cromlech*. Par contre, lui a été entamé par la tranchée routière.

Description

Monument fort dégradé dont il ne reste que 3 dalles visibles, ayant pu délimiter un cercle de 3m de diamètre.

### [Larrau]{.spatial} - [Erroimendi]{.coverage} 1 - [Tumulus]{.type}

Localisation

Altitude : 1362m.

Il est situé à environ 90m à l'Ouest Nord-Ouest de *Erroimendi 1 - Cromlech*, à 2m à l'Est du poste de tir à la palombe et à une dizaine de mètres à l'Est de *Erroimendi 2 - Tumulus*.

Description

Tumulus de 6,50m de diamètre et 0,40m de haut, constitué de terre et de nombreuses petites pierres visibles par endroit. Il semble qu'on puisse distinguer dans sa partie supérieure un cercle de diamètre moindre que le tumulus (environ 5,10m) délimité par une quinzaine de petits blocs de [grés]{.subject}.

Historique

[Nous]{.creator} avons découvert ce tumulus en [mai 1978]{.date}.

### [Larrau]{.spatial} - [Erroimendi]{.coverage} 2 - [Tumulus]{.type}

Localisation

Il est situé à une dizaine de mètres à l'Ouest de *Erroimendi 1 - Tumulus*.

Description

Tumulus bien visible, de 7m à 8m de diamètre et 0,40m de haut, dont la périphérie Sud-Est a été endommagée par les constructions successives de postes de tir à son voisinage immédiat.

Historique

[Nous]{.creator} avons découvert ce tumulus en [mai 1978]{.date}.

### [Larrau]{.spatial} - [Houharguieta]{.coverage} - [Pierre couchée]{.type}

Localisation

Altitude : 1560m.

Elle est située quasiment à l'extrémité Ouest Nord-Ouest de la crête de ce nom.

Description

On note, couchée sur le sol suivant un axe Nord-Sud, une dalle de [schiste]{.subject}, rectangulaire, mesurant 2,20m de long, 0,85m de large et 0,17m d'épaisseur. Elle semble présenter des traces d'épannelage à son extrémité Nord et le long de son bord Ouest, et ne semble pas être en place naturellement.

Historique

Pierre couchée découverte par [Meyrat F.]{.creator} en [août 2013]{.date}.

### [Larrau]{.spatial} - [Idagorria bas]{.coverage} Est 1 - [Tertre d'habitat]{.type}

Localisation

Altitude : 970m.

Il est en bordure de la piste qui longe, en contre-bas la route venant de Lacarry, avant son virage vers Urrutchantzé et à proximité immédiate d'un enclos en poutrelles.

Description

Tertre de terre, semi circulaire, aplani, mesurant 20m de diamètre et 1,80m de haut, dominant au Nord la piste qui le contourne.

Historique

Tertre découvert par [Blot J.]{.creator} en [2012]{.date}.

### [Larrau]{.spatial} - [Ibarrondoa]{.coverage} Sud - [Tertre d'habitat]{.type}

Localisation

Altitude : 1306m.

Il est à 20m au Sud-Ouest et en contre-bas du virage de la route qui mène à Ibarrondoa, peu avant que ne se détache la bretelle conduisant à ces installations.

Description

Grand tertre ovale et asymétrique, de 16m au moins dans son grand axe, et 20m de haut, face au rio (il est le symétrique du plus grand *TH* (près de la bergerie) du groupe Nord, qui en comprends au moins 8).

Historique

Tertre découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

### [Larrau]{.spatial} - [Kurutxetakolepoa]{.coverage} 2 - [Tumulus]{.type}

Localisation

Altitude : 925m.

Il est situé au sommet de la petite colline qui surplombe, au Sud, le col de Kurutxeta, à l'extrémité Nord de ce sommet, plat dans l'ensemble.

Description

Tumulus circulaire mixte de terre et de fragments plus ou moins gros de [calcaire]{.subject}, mesurant 7m de diamètre et 0,30m de haut.

Historique

Monument découvert par [Meyrat F.]{.creator} en [novembre 2017]{.date}. Nous signalons ici qu'en 1979 nous avions décrit [@blotSouleSesVestiges1979, p.17], un tumulus (*T1*). Situé dans le col lui-même. Il s'agissait en fait d'un mouvement de terrain dû aux vestiges des fondations d'un cayolar disparu, et non d'un tumulus.

### [Larrau]{.spatial} - [Kurutxetakolepoa]{.coverage} 3 - [Tumulus]{.type} ([?]{.douteux})

Localisation

Situé à 7m au Sud de *Kurutxetakolepoa 2 - Tumulus*.

Description

Relief circulaire discret, de 3,80m de diamètre et 0,10m de haut, avec légère dépression centrale.

Historique

Monument découvert par [Meyrat F.]{.creator} en [novembre 2017]{.date}.

### [Larrau]{.spatial} - [Kurutxetakolepoa]{.coverage} 4 - [Tumulus]{.type} ([?]{.douteux})

Localisation

Tangent au Sud de *Kurutxetakolepoa 3 - Tumulus (?)*, et au bord de la rupture de pente vers le Sud.

Description

Se présente comme une galette circulaire de 6m de diamètre, en faible relief (0,15m environ), avec une légère dépression centrale.

### [Larrau]{.spatial} - [Lapatignegagne]{.coverage} 2 - [Cromlech]{.type}

Localisation

À 16m environ à l'Ouest Sud-Ouest du *Tumulus n°3* déjà décrit [@blotSouleSesVestiges1979, p.17]

Description

Cinq pierres, au ras du sol, délimitent un cercle de 7m de diamètre : il y en a 1 au Nord, 1 à l'Ouest et 3 au Sud.

Historique

Cercle découvert en [octobre 2011]{.date} par [L. Millan]{.creator}, [M. Tamazyo]{.creator} et [I. Gaztelu]{.creator}.

### [Larrau]{.spatial} - [Lapatignegagne]{.coverage} 4 - [Tumulus]{.type}

Localisation

Altitude : 1350m.

Il est en contre-bas et au Nord-Ouest des monuments précédents, à 3m au Sud de la piste qui se rend au cayolar Arratzolaté, sur le premier plat qu'elle atteint.

Description

Tumulus de terre de 0,40m de haut et 6m de diamètre ; au centre existe une dépression de 1,50m de large et 0,20m de profondeur.

Historique

Tumulus découvert par [Blot J.]{.creator} en [août 2012]{.date}.

### [Larrau]{.spatial} - [Latsagakoborda]{.coverage} - [Tertre d'habitat]{.type}

Localisation

Altitude : 874m.

Description

Tertre asymétrique de 12m environ et 1m de haut, sur terrain incliné.

Historique

Tertre découvert par [J. Blot]{.creator} en [1978]{.date}.

### [Larrau]{.spatial} - [Lepo Xipi]{.coverage} - [Cromlech]{.type}

Localisation

Altitude : 1450m.

Sur un replat à l'extrémité Est du sommet dénommé Pic des Escaliers, à deux mètres au Nord-Est de la piste.

Description

Un cercle de 4,80m de diamètre est délimité par une dizaine de pierres au ras du sol ; au centre se voit un petit amas d'environ une quinzaine de pierres de mêmes dimensions.

Historique

Monument découvert par [Blot Colette]{.creator} en [novembre 2012]{.date}.

### [Larrau]{.spatial} - [Méhatzekolepoa]{.coverage} - [Cromlech]{.type}

Localisation

Altitude : 1383m.

Il se trouve à 10m à l'Est du virage de la route, sur le gazon du col et à 20m au Sud-Est du *TC* [@blotSouleSesVestiges1979, p.13] maintenant détruit.

Description

Petit cercle 2,50m de diamètre, délimité par 5 pierres au ras du sol.

Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

### [Larrau]{.spatial} - [Millagate]{.coverage} 5 - [Tumulus-cromlech]{.type}

Localisation

Carte 1447 Nord Larrau.

Altitude : 1444m.

La nécropole de Millagate a été décrite en 1968 [@boucherNotesProspectionMegalithique1968, p.165], et nous y avons pratiqué deux fouilles de sauvetage : une en 1986 sur le *tumulus-cromlech Millagate 4* [@blotTumuluscromlechMillagateIV1990, p.49], et une sur *Millagate 5* en 1987 [@blotTumuluscromlechMillagateCompte1991, p.145].

Le monument n°5 est situé à 40m à l'Est du n°1 et à 23m à l'Est Nord-Est du n°2.

Description

Tumulus de 8m de diamètre et 0,20m de haut, délimité à sa périphérie par 10 pierres, au ras du sol, mais néanmoins bien visibles. On note une dépression, dans le secteur Sud, de 3m de diamètre et 0,20m de profondeur.

Historique

Monument découvert en [août 1979]{.date}, et fouillé en [1987]{.date}. La datation obtenue, en datation calibrée, est la suivante : 1118 - 812 BC.

### [Larrau]{.spatial} - [Murutxe]{.coverage} Sud - [Tertres d'habitat]{.type} 

On en dénombre trois.

Localisation

Altitude : 970m.

Le premier se trouve (*TH 1*) en bord Sud de la route qui se rend vers le pic Salhagagne et Licq, au pied du relief qui précède à l'Ouest celui dénommé Murutxe.

Description

*TH 1* est accolé au bord de la route ; tertre de terre 7m de long, 4 de large, allongé dans le sens Est Sud-Est Ouest Nord-Ouest ; un relief à son flanc Sud suggère un deuxième tertre (*TH 2*).

Enfin un autre tertre (*TH 3*) a été probablement remanié par les engins routiers ; il est à 8m au Sud-Ouest des précédents ; de terre et de pierres il mesure 7m de long, 2,50m de large.

Historique

Tertres découverts par [J. Blot]{.creator} en [2012]{.date}. Signalons ici que le *Dolmen de Murutxe*, signalé par J.M. Barandiaran, [@barandiaranHombrePrehistoricoPais1953 p.235] a été détruit par les travaux routiers.

### [Larrau]{.spatial} - [Murutxe]{.coverage} Nord 1 - [Tertre d'habitat]{.type}

Localisation

Altitude : 990m.

Au sommet de la colline précède celle dénommée Murutxe.

Description

Tertre asymétrique oblong, de terre et de quelques pierres, mesurant environ 10m de long et 0,50m de haut, a grande pente vers le Sud.

Historique

Tertre découvert par [Blot C.]{.creator} en [2012]{.date}.

### [Larrau]{.spatial} - [Olhaberria]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 960m.

Il est dans le col précédent la colline où se situe *Murutxe Nord 1 - Tertre d'habitat*.

Description

On note un relief circulaire de 10m de diamètre et 0,30m de haut, avec une légère excavation centrale de 3m de diamètre et quelques centimètres de profondeur. S'agit-il d'un tumulus ou d'un relief consécutif à une excavation ? nous pencherions pour la première hypothèse.

Historique

Monument découvert par [Blot J.]{.creator} en [2012]{.date}.

### [Larrau]{.spatial} - [Otxogorrigagne]{.coverage} - [Tumulus]{.type}

On en dénombre quatre.

- *Otxogorrigane 1 - Tumulus*

    - Localisation

         Altitude : 1400m.

         Sur la ligne de crête qui part à gauche de la route qui descend vers Larrau.

    - Description

         Petit tumulus de 4m de diamètre environ et 0,30 de haut, fait de pierrailles essentiellement.

    - Historique

         Monument découvert par [J. Blot]{.creator} en [mai 2011]{.date}.

- *Otxogorrigagne 2 - Tumulus*

    - Localisation

         Il est à une quarantaine de mètres à l'Est Sud-Est du n°1.

    - Description

         Mesure près de 5m de diamètre et 0,40m de haut. Terre et pierrailles.

    - Historique

         Monument découvert par J. Blot en mai 2011.

- *Oxogorrigane 3 - Tumulus*

    - Localisation

         Situé à une vingtaine de mètres à l'Ouest Nord-Ouest du n°1.

    - Description

         Mesure environ 3,50m de diamètre, en pierraille et de faible hauteur.

    - Historique

         Monument découvert par J. Blot en mai 2011.

- *Oxogorrigagne 4 - Tumulus*

    - Localisation

         Situé à une quarantaine de mètres à l'Ouest du précédent, de même nature et dimensions.

    - Historique

         Monument découvert par J. Blot en mai 2011.

### [Larrau]{.spatial} - [Otxogorria]{.coverage} - [Cromlech]{.type}

Localisation

Altitude : 1337m.

Il se trouve à une vingtaine de mètres au Sud de *Otxogorria - Tertre d'habitat*.

Description

Petit cromlech fait de 7 pierres très visibles, mesurant en moyenne 0,70m x 0,25m et disposées suivant un tracé légèrement ovale de 2,30m x 2m.

Historique

Cromlech découvert par [Blot J.]{.creator} en [octobre 2014]{.date}.

### [Larrau]{.spatial} - [Otxogorria]{.coverage} - [Tertre d'habitat]{.type}

Localisation

Altitude : 1337m.

Il se trouve sur le vaste plateau au flanc Nord du sommet de Negumendi et de son pylône relais, en contre-bas de la route qui y monte.

Description

Tertre de terre de faible relief (0,50m de haut), de forme oblongue (26m x 8m) érigé sur terrain presque plat.

Historique

Tertre découvert par [Blot J.]{.creator} en [octobre 2014]{.date}.

### [Larrau]{.spatial} - [Odécharreko lepoa]{.coverage} - [Monolithe]{.type}

Localisation

Altitude : 1590m.

Il gît dans un virage de la route (sur son bord Est), qui passe sous la crête de Millagate, en partant des chalets d'Iraty.

Description

Gros bloc parallélépipédique régulier, de [grés]{.subject} local, allongé selon un axe Nord-Sud. Il mesure 4,10m de long, 1,54m de large à sa base Sud, 1,21m à son sommet Nord. Son épaisseur varie : au côté Ouest elle est de 0,64m à son côté Est. Il ne semble pas présenter de traces d'épannelage, mais sa présence en ce lieu, sa forme et ses dimensions posent la question d'une éventuelle signification anthropique.

Historique

Monolithe découvert par le [groupe Hillariak]{.creator} en [2012]{.date}.

### [Larrau]{.spatial} - [Peko Olhaberria]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 1110m.

Tangent au Nord du GR 10 qui passe au virage de la route et descend vers l'Ouest, en passant par une croupe, d'abord en terrain plat ; il est à l'amorce de ce replat.

Description

Tumulus pierreux de 4,50m de diamètre et 0,20m de haut, constitué de blocs de [calcaire]{.subject} de la taille d'un ou deux pavés.

Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2013]{.date}.

### [Larrau]{.spatial} - [Peko Olhaberria]{.coverage} 1 - [Tertre d'habitat]{.type}

Localisation

Altitude : 1093m.

En suivant le GR 10, sur le replat, à 70m environ à l'Ouest de *Peko Olhaberria - Tumulus*, avant l'amorce de la descente.

Description

Tertre de terre de 8m x 8m et 0,70m de haut environ.

Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2013]{.date}.

### [Larrau]{.spatial} - [Pelusagne]{.coverage} 2 - [Tumulus]{.type}

Localisation

Altitude : 1540m.

Il est au-dessus d'une carrière qui entame la route, au Sud, dans un virage.

Description

Tumulus circulaire de terre et de pierres, de 5m de diamètre et 1m de haut environ. Il semblerait qu'il y ait un péristalithe.

Historique

Tumulus découvert par [Meyrat F.]{.creator} en [août 2013]{.date}.

### [Larrau]{.spatial} - [Pelusagne]{.coverage} 3 - [Tumulus]{.type}

Localisation

À 4m à l'Est de *Pelusagne 2 - Tumulus*.

Description

Tumulus de terre et de pierres, mesurant 4m de diamètre et 0,40m de haut.

Historique

Tumulus découvert par [Meyrat F.]{.creator} en [août 2013]{.date}.

### [Larrau]{.spatial} - [Sagukidoy]{.coverage} Ouest 1 - [Tertre d'habitat]{.type}

Localisation

Altitude : 970m.

Erigé sur terrain en pente légère surplombant un petit ravin. Visible de loin.

Description

Tertre de terre oblong d'une vingtaine de mètres de long, 5m de large et 1m de haut.

Historique

Tertre découvert en [1979]{.date}.

### [Larrau]{.spatial} - [Sagukidoy]{.coverage} centre - [Tertres d'habitat]{.type}

Localisation

Ce groupe, s'élevant plus ou moins à une dizaine de tertres de grandes dimensions, est réparti dans les environs immédiats de la grange Sagukidoy. Le relevé plus précis n'a pu encore être fait.

Altitude moyenne : 920m.

Historique

Tertres découverts par [Blot J.]{.creator} en [1979]{.date}.

### [Larrau]{.spatial} - [Sagukidoy]{.coverage} Est haut - [Tertres d'habitat]{.type}

- *Tertre n°1*

    - Localisation

        Altitude : 980m.

        On le trouve sur la droite du chemin antique qui mène à la grange Sagukidoy, au voisinage immédiat des ruines d'un très vieux cayolar.

    - Description :

        Tertre de terre semi-circulaire de 8m environ de diamètre et 2m de haut.

- *Tertre n°2*

    - Localisation

        Altitude : 978m.

        Il est situé à 3m à gauche du même chemin, une dizaine de mètres plus loin.

    - Description :

        Vaste tertre de terre, de forme oblongue, mesurant 18m de long, 6m de large et 1m de haut.

Historique

Tertres découverts par [Blot C.]{.creator} en [2012]{.date}.

### [Larrau]{.spatial} - [Sagukidoy]{.coverage} Est bas 1 - [Tertre d'habitat]{.type}

Localisation

Altitude : 800m.

Visible de loin, de la route, sur terrain en pente, en bord de petit ravin.

Description

Tertre de terre d'une vingtaine de mètres de diamètre et un de haut.

Historique

Monument découvert par [Blot J.]{.creator} en [1979]{.date}.

### [Larrau]{.spatial} - [Salhagagne]{.coverage} 2 - [Tumulus]{.type}

Localisation

Altitude : 990m.

Il se trouve au Sud-Ouest du virage de la route qui va descendre vers Licq, et à quelques mètres à l'Ouest d'une piste de terre qui monte vers le sommet de Salhagagne.

Description

Tumulus de terre circulaire de 10m de diamètre et 0,40m de haut.

Historique

Tumulus découvert par [Blot J.]{.creator} en [septembre 2012]{.date}.

### [Larrau]{.spatial} - [Saratzé]{.coverage} Sud 1 - [Tertres d'habitat]{.type}

Localisation

Altitude : 1217m.

Ces 3 tertres sont échelonnés en bordure d'un petit riu qu'ils dominent.

Description

Ils mesurent en moyenne 15m de diamètre et 0,50m de haut.

Historique

Tertres découverts par [Blot J.]{.creator} en [octobre 2013]{.date}.

### [Larrau]{.spatial} - [Saratzé]{.coverage} Sud 2 - [Tertres d'habitat]{.type}

Localisation

Altitude : 1196m.

Ces deux tertres sont échelonnés sur une croupe herbeuse, étendue selon un axe Est-Ouest.

Description

Ils mesurent environ 16m à 18m de diamètre et 0,60m de haut.

Historique

Tertres découverts par [Blot J.]{.creator} en [octobre 2013]{.date}.

### [Larrau]{.spatial} - [Sarimendi]{.coverage} - [Cromlech]{.type}

Localisation

Altitude : 1413m.

Cromlech situé au pied du mont Sarimendi - au Sud-Est - au col Arrestelitako lepoa. Il n'est pas au col même - où débouche une piste pastorale côté Sud-Ouest - mais sur une légère éminence en surplomb de celui-ci, au Sud Sud-Est. On le voit à 3,70m à l'Est de la piste de crête, au bord de la rupture de pente.

Description

Il s'agit d'un cromlech qui a été très remanié par une probable fouille clandestine, dont il reste une excavation d'environ 4m de diamètre et 0,30m à 0,45m de profondeur ; en périphérie de cette dépression on note une dizaine de blocs de [calcaire]{.subject} blanc, plus ou moins bien disposés en un cercle d'environ 2,90m de diamètre, mais dont la position initiale a été, à l'évidence, modifiée par l'excavation.

Historique

Monument trouvé par [Ph. Velche]{.creator} en [juin 2016]{.date}.

### [Larrau]{.spatial} - [Sakia]{.coverage} 1 et 2 - [Tumulus]{.type}

Localisation

Altitude : 1358m.

Ce monument se trouve vers la partie haute et Sud de la longue ligne de croupe qui borde la rive droite du ruisseau (ou gorges) d'Olhadubi. Il est situé dans un joli replat herbeux à une soixantaine de mètres à l'Est des cayolars de Sakia et du très beau groupe de tertres d'habitat du même nom (Sakia-Gagnekoa).

Description

- *T1* : tumulus terreux en forme de galette circulaire, mesurant 4,40m de diamètre et 0,30m de haut ; pas de pierres visibles.

- *T2* : Tangent à lui au Sud, il semble qu'on puisse évoquer un deuxième tumulus de 4m de diamètre et 0,20m de haut, dont le tiers Est, peu visible, aurait été détérioré. Nous avions vu le même cas à Apatessaro 1 bis, confirmé par la fouille.

Historique

Le tumulus *T1* a été découvert par [A. Martinez]{.creator} en [septembre 2017]{.date} et le *T2* par [Blot J.]{.creator} à la fin du même mois.

### [Larrau]{.spatial} - [Ugnhurritzé]{.coverage} - [Tertres d'habitat]{.type}

altitude : 999m.

[Nous]{.creator} sommes revenus en [octobre 2014]{.date} sur ce site publié par nous en [1979]{.date} [@blotSouleSesVestiges1979, p.31], et la végétation moins abondante nous a permis de constater que le nombre de vestiges de tertres d'habitat est *plus du double* que celui précédemment indiqué : 13 TH.

### [Larrau]{.spatial} - [Uthurzeheta]{.coverage} - [Tumulus]{.type} ou [Tertre d'habitat]{.type} ([?]{.douteux})

Localisation

Altitude : 1340m.

Au pied du cirque montagneux qui entoure les tertres d'Uthurzeheta, à l'Ouest de ceux-ci, entre l'émergence de plusieurs petits rus.

Description

Vaste tertre (tumulus ?) de forme ovale mesurant de 20m à 25m de long, une quinzaine de large, et 2m à 3m de haut.

On peut, éventuellement, se poser la question d'un mouvement naturel du terrain... relativement peu probable à notre avis.

### [Larrau]{.spatial} - [Xardeka]{.coverage} 6 - [Tumulus-cromlech]{.type} ou [Dolmen]{.type} 

Localisation

Altitude : 1515m.

Il se trouve sur le versant Nord-Ouest du col qui sépare Pelusagne de Xardeka, à une vingtaine de mètres de la naissance du petit ruisseau au point déclive du col.

Description

Beau tumulus de 8m de diamètre et 0,20m de haut, érigé sur terrain en pente vers le Sud-Est. Un bloc de poudingue dans le secteur Sud, de forme semi-circulaire (épannelé ?) mesurant 1m à sa base et 0,10m d'épaisseur. Ferait penser à une dalle dolménique éversée... mais par ailleurs une vingtaine de petites pierres disposées en cercle dans la région centrale, modifient de point de vue. Enfin quelques pierres à la périphérie du tumulus évoquent l'existence possible d'un péristalithe.

Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.
