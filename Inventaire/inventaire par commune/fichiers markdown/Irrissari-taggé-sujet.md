# Irrissari

### [Irrissari]{.spatial} - [Behorsaro]{.coverage} - [Tertres d'habitat]{.type}

Localisation

Altitude : 506m.

Les 8 tertres d'habitat sont situés au flanc Nord-Est du mont Baigoura, au lieu-dit Béhorsaro, sous le point coté 60 sur la ligne de crête. Ils sont érigés sur un terrain en pente très douce vers le Sud-Est.

Description

De forme ovale le plus souvent.

- *Tertre A* : Alttitude : 506m. À quelques mètres au Nord-Ouest d'un pointement rocheux. Mesure 16m x 8m et 0,90m de haut.

- *Tertre B* : Mesure 13m x 8m et 0,80m de haut.

- *Tertre C* : à 2m au Nord-Ouest du précédent, et mêmes dimensions.

- *Tertre D* : à 1m au Nord-Ouest du précédent et mêmes dimensions.

- *Tertre E* : à 30m au Nord Nord-Est du précédent ; mesure 13m x 8m et 1,50m de haut.

- *Tertre F* : à 25m à l'Est Sud-Est du précédent ; mesure 5m x 4m, et 0,20m de haut - Très douteux.

- *Tertre G* : à 2m au Sud Sud-Est du précédent ; mesure 5m x 4m et 0,20m de haut - Très douteux.

- *Tertre H* : Il est situé à environ 300m au Nord Nord-Est du *Tertre F* et au Sud d'une piste pastorale bien marquée, tangent à celle-ci. Mesure 11m x 10m et 0,90m de haut.

Historique

Tertres trouvés par [Blot J.]{.creator} en [1974]{.date}. (Le *Tertre F* l'a été par [Meyrat F.]{.creator} en [décembre 2013]{.date}).

### [Irrissari]{.spatial} - [Margoueta]{.coverage} 1 - [Tumulus]{.type}

Localisation

Altitude : 380m.

Il est tout en bordure du chemin et à l'Est de celui-ci, et à 5m au Sud d'une pierre isolée, elle aussi en bordure du chemin, mesurant 0,70m de long, 0,50m de large et 0,24m de hauteur ; elle paraît naturellement en place.

Description

Tumulus circulaire, mesurant 3m de diamètre et 0,20m de haut, constitué de terre avec quelques pierres visibles. Le chemin en a légèrement entamé le secteur Ouest.

Historique

Monument découvert par [F. Meyrat]{.creator} en [août 2015]{.date}.
