# Esterençuby

### [Esterençuby]{.spatial} - [Arthé]{.coverage} 7 - [Cromlech]{.type}

Localisation

Altitude : 1000m.

Il se trouve sur la ligne de crête, à la limite du terrain remanié par les travaux, juste avant la rupture de pente. Rappelons qu'un ensemble de quatre cromlechs avait été démoli lors des travaux routiers.

Description

Petit cercle de 1m de diamètre environ délimité par une dizaine de pierres, au ras du sol, de dimensions très modestes.

Historique

Cercle découvert par [Blot J.]{.creator} en [octobre 1985]{.date}.

### [Esterençuby]{.spatial} - [Bihurri]{.coverage} - [Tertre d'habitat]{.type} ([?]{.douteux})

Localisation

Altitude : 733m.

Il est situé au Sud d'une clôture de barbelés, dans une prairie en pente douce vers le Sud et à 50m à au Nord-Ouest d'une petite borde.

Description

On note un important relief de terrain aux deux bords Ouest et Est parallèles, se terminant au Sud suivant un tracé semi-circulaire. Cette plate-forme semble bien d'origine anthropique, mais sa finalité n'a rien d'évident : grand tertre d'habitat aplani, plate-forme préparée dans un autre but (soubassement de construction ?) - la question reste en suspens.

Historique

Tertre découvert par [Meyrat F.]{.creator} en [octobre 2018]{.date}.

### [Esterençuby]{.spatial} - [Erreta]{.coverage} 1 - [Cromlech]{.type}

Localisation

Carte 1346 Est Saint-Jean-Pied-de-Port.

Altitude : 702 mètres.

On le trouve au centre d'un petit col, à une dizaine de mètres au Nord-Est de la route qui monte vers l'Errozaté, tout de suite après un tournant en épingle à cheveu.

Description

Une dizaine de petits blocs de [grés]{.subject} gris au ras du sol, délimitent un cercle de 2,60 mètres de diamètre.

Historique

Monument découvert par [A. Martinez Manteca]{.creator} en [2008]{.date}.

### [Esterençuby]{.spatial} - [Erreta]{.coverage} 2 - [Tumulus]{.type}

Localisation

Situé à une vingtaine de mètres au Nord Nord-Est de *Erreta 1 - Cromlech*.

Description

Tumulus pierreux de 5,40m de diamètre et 0,30m de haut ; nombreux petits blocs de [grés]{.subject} gris visibles en surface, mais pas de péristalithe visible.

Historique

Monument découvert en [2008]{.date} par [A. Martinez Manteca]{.creator}.

### [Esterençuby]{.spatial} - [Erreta]{.coverage} 3 - [Cromlech]{.type} ([?]{.douteux})

Localisation

Situé à 3m au Nord-Ouest de *Erreta 2 - Tumulus*.

Description

Huit petites pierres au ras du sol délimitent un cercle de 3,80m de diamètre (Monument douteux ?)

Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2009]{.date}.

### [Esterençuby]{.spatial} - [Erreta]{.coverage} 4 - [Cromlech]{.type}

Localisation

Altitude : 740 mètres.

Il se trouve à 6 mètres à l'Ouest de *Erreta 1 - Tumulus*, qui est très peu marqué en hauteur, avec de nombreuses pierres bien visibles.

Description

Une vingtaine de pierres, de volume variable (certaines atteignent 0,70m de long), délimitent un cercle très net de 3,20m de diamètre.

Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2009]{.date}.

### [Esterençuby]{.spatial} - [Erreta]{.coverage} 5 - [Cromlech]{.type}

Localisation

Il est tangent, à l'Ouest, au monument *Erreta 4 - Cromlech*.

Description

Une vingtaine de pierres au ras du sol délimitent un cercle de 3,50m de diamètre, lequel contient, tangent à sa partie Nord, un second petit cercle formé d'une dizaine de petites pierres.

Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2009]{.date}.

### [Esterençuby]{.spatial} - [Erreta]{.coverage} - [Dolmen]{.type} ([?]{.douteux})

Localisation

Altitude : 700mètres.

À peu près à mi-chemin entre le petit col, à une dizaine de mètres au Nord-Est de la route qui monte vers l'Errozaté et *Erreta 1 - Tumulus*.

Description

On note, immédiatement en bordure Sud de la route, sur un sol en légère pente vers l'Ouest, deux gros blocs rocheux, en [grés]{.subject} gris : l'un, le plus à l'Ouest, sensiblement parallélépipédique, mesure 1,40m de long, 1,30m de large et 0,75m de haut ; l'autre à l'Est du précédent, en est distant de 0,30 à 0,45 m. Ses dimensions sont de 1,20m de long, 0,75m de large et 0,80m de haut. Ce bloc est enfoncé dans le sol, contrairement au premier qui est simplement posé. Ces deux volumineux éléments sont au centre d'un « bourrelet » de terrain circulaire, de 4m de diamètre, formant comme une ébauche de tumulus, et dans lequel sont incluses de nombreuses pierres.

Historique

Monument très douteux découvert par [Blot J.]{.creator} en [octobre 2009]{.date}.

### [Esterençuby]{.spatial} - [Erreta]{.coverage} Est 2 - [Tertres d'habitat]{.type}

Localisation

Altitude : 770m.

Sur le promontoire qui domine la route, au Nord.

Description

Deux petits tertres de terre quasiment tangents, d'environ 3m à 4m de diamètre et 0,40m de haut.

Historique

Tertres découverts en [1971]{.date} par [J. Blot]{.creator}.

### [Esterençuby]{.spatial} - [Erreta]{.coverage} sud n°1 - [Tertre d'Habitat]{.type}

Localisation

Monument le plus à l'Est et en altitude.

Altitude : 690m.

Description

Tertre mesurant 6m à 7m de diamètre ; hauteur difficile à apprécier (0,30m ?) du fait de sa construction sur un terrain en pente vers le Sud.

Historique

Monument découvert par [J. Blot]{.creator} en [1971]{.date}.

### [Esterençuby]{.spatial} - [Erreta]{.coverage} sud n°2 - [Tertre d'Habitat]{.type}

Localisation

Situé à environ 20 mètres au Sud de *Erreta sud n°1 - Tertre d'Habitat*, et légèrement en contrebas.

Description

Réduit à l'état de vestige, érigé sur un sol en pente vers le Sud, il mesure une dizaine de mètres de diamètre, et est d'une hauteur peu appréciable. Il présent une légère excavation en son centre ; quelques pierres apparaissent en surface de ce monument essentiellement fait de terre.

Historique

Monument découvert par [J. Blot]{.creator} en [1971]{.date}.

### [Esterençuby]{.spatial} - [Erreta]{.coverage} 1 - [Tumulus]{.type}

Localisation

Carte 1346 Est Saint-Jean-Pied-de-Port.

Altitude : 730m.

Il est tout à fait en bordure et à droite de la route qui monte d'Esterençubi à Errozaté, sur un petit replat herbeux.

Description

Petit tumulus bien visible, en forme de galette aplatie mesurant 5m de diamètre et 0,25m de haut, en grande partie constitué de pierres : il y aurait même peut-être une double couronne de pierres.

Historique

Monument découvert en [juin 1971]{.date}.

### [Esterençuby]{.spatial} - [Erreta]{.coverage} sud n°3 - [Tumulus]{.type}

Localisation

Situé à environ 25mètres à l'Est Nord-Est de *Erreta sud n°1 - Tertre d'Habitat*.

Altitude : 688m.

Description

Ce monument circulaire, de terre et de pierres, mesure environ 8 mètres de diamètre ; érigé sur un sol en pente vers l'Ouest, sa hauteur peut être estimée à 0,80m environ. On note une légère excavation centrale.

Historique

Monument découvert par [J. Blot]{.creator} en [1971]{.date}. En 2010, Manteca Martinez et ses compagnons ont baptisé ce monument « Tumulus ». La situation sur un terrain en pente, la dissymétrie qui en résulte n'est pas très en faveur de tumulus funéraires. Toutefois, sa structure pierreuse et ses dimensions peuvent en effet laisser planer un doute sur leur identité réelle.

### [Esterençuby]{.spatial} - [Erreta]{.coverage} sud n°4 - [Tumulus]{.type}

Localisation

Situé à une vingtaine de mètres à l'Est Nord-Est de *Erreta sud n°3 - Tumulus*.

Description

Tumulus de terre et de pierres de 7 mètres de diamètre environ, érigé sur un sol à peu près plat. On note quelques pierres qui pourraient représenter une ébauche de [ciste]{.subject} à son sommet, et peut-être un péristalithe à sa périphérie. Monument le plus probable.

Historique

Monument découvert par [J. Blot]{.creator} en [1971]{.date}. En 2010, Manteca Martinez et ses compagnons ont baptisé ce monument « Tumulus ». La situation sur un terrain en pente, la dissymétrie qui en résulte n'est pas très en faveur de tumulus funéraires. Toutefois, sa structure pierreuse et ses dimensions peuvent en effet laisser planer un doute sur leur identité réelle.

### [Esterençuby]{.spatial} - [Erreta]{.coverage} sud n°5 - [Tertre d'habitat]{.type}

Localisation

Situé à 5 mètres à l'Est Nord-Est de *Erreta sud n°4 - Tumulus*.

Description

Tertre de terre et de pierres de 7m de diamètre environ, érigé sur un terrain en pente vers le Sud ; hauteur difficile à apprécier (0,30m environ).

Historique

Monument découvert par [J. Blot]{.creator} en [1971]{.date}.

### [Esterençuby]{.spatial} - [Erreta]{.coverage} sud n°6 - [Tumulus]{.type}

Localisation

Il est situé à environ une trentaine de mètres au Nord Nord-Est de *Erreta sud n°3 - Tumulus*.

Description

Tumulus de terre et de pierres de 6 à 7 mètres de diamètre ; érigé sur un sol incliné vers l'Ouest, sa hauteur est difficile à apprécier (0,40m ?).

Historique

Monument découvert par [J. Blot]{.creator} en [1971]{.date}. En 2010, Manteca Martinez et ses compagnons ont baptisé ce monument « Tumulus ». La situation sur un terrain en pente, la dissymétrie qui en résulte n'est pas très en faveur de tumulus funéraires. Toutefois, sa structure pierreuse et ses dimensions peuvent en effet laisser planer un doute sur leur identité réelle.

### [Esterençuby]{.spatial} - [Errozaté]{.coverage} 6 - [Tumulus]{.type}

Localisation

Carte 1346 Est Saint-Jean-Pied-de-Port.

Altitude : 1300m.

Il est situé à 80m environ au Sud Sud-Ouest du groupe de cromlechs déjà décrits [@blotNouveauxVestigesMegalithiques1972 p 78], et qui ont fait l'objet d'une campagne de fouille de sauvetage en 1976.

Description

Volumineux tumulus pierreux de 2,50 à 3m de haut, constitué d'un amas de blocs rocheux posé sur une assise rocheuse naturelle. Surmontant l'ensemble, a été disposé au sommet un bloc plus important que les autres, mesurant 2,20m de large et 1,30m de haut. Ce monument est longé à son flanc Ouest par une antique piste pastorale. Sa signification nous échappe (de même que son époque approximative de construction), monument funéraire ? commémoratif ? borne repère ?

Historique

Monument découvert en [mars 1974]{.date}.

### [Esterençuby]{.spatial} - [Etchondo]{.coverage} - [Tertres d'habitat]{.type}

Localisation

Altitude : 600m.

- *Le n°1* est visible à environ 7 mètres au Nord-Ouest de la route. ; circulaire, de terre, il mesure environ 4m de diamètre et 0,30m de haut.

- *Le n°2* est à 7m au Sud-Est, au bord de la route, qui l'a détérioré.

### [Esterençuby]{.spatial} - [Etxeparea]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 841m.

Sur un replat situé à l'Est du mont Sohandi, dans la boucle en U que forme une piste forestière et à une vingtaine de mètres au Sud de sa branche Nord.

Description

Tumulus de terre érigé sur terrain plat, de forme très légèrement ovalaire, mesurant 12m x 10m et 0,30m de haut, présentant une faible dépression centrale. On peut aussi émettre l'hypothèse qu'il s'agisse d'un tertre d'habitat, mais alors très dégradé...

Historique

Tumulus découvert par [Meyrat F.]{.creator} en [octobre 2018]{.date}.

### [Esterençuby]{.spatial} - [Etxeparea]{.coverage} 1 - [Tertre d'habitat]{.type}

Localisation

Il est situé à 2m au Nord-Est de *Etxeparréa - Tumulus*, dont il est séparé par le passage d'une piste pastorale herbeuse ; il est adossé à un pointement rocheux naturel.

Description

Tertre herbeux mesurant 10m x 8m et environ 1m de haut.

Historique

Tertre découvert par [Meyrat F.]{.creator} en [octobre 2018]{.date}.

### [Esterençuby]{.spatial} - [Etxeparea]{.coverage} 2 - [Tertre d'habitat]{.type}

Localisation

Altitude : 829m.

Description

Tertre terreux, érigé sur terrain en pente vers le Nord-Ouest, de forme ovalaire, mesurant 10m x 6m et 0,40m de haut.

Historique

Tertre découvert par [Meyrat F.]{.creator} en [octobre 2018]{.date}.

### [Esterençuby]{.spatial} - [Harpéa]{.coverage} - [Tertres d'habitat]{.type}

Localisation

Altitude : 890m.

Les 5 tertres sont situés très près de la frontière, au Nord de la BF 218, et au Sud-Ouest de la route qui conduit au cayolar, et se continue par la piste qui conduit vers la grotte Harpéa.

Description

De forme ovale, ils sont érigés sur un terrain en pente vers le Nord-Est, d'où leur classique forme dissymètrique.

- *Tertre d'habitat 1* : se trouve à une bonne trentaine de mètres à vol d'oiseau, à l'Ouest du cayolar, plus en altitude que lui, et derrière une butte naturelle le séparant de ce cayolar. Tertre bien visible, de 12m x 11m et 1m de haut, ovale à grand axe Nord-Sud.

- *Tertre d'habitat 2* : à 20m au Nord-Est du précédent, situé sur la butte naturelle précitée ; mesure 12m x 9m et 0,80m de haut. Il est très net, bien que l'on puisse (un peu) le confondre avec la butte naturelle.

- *Tertre d'habitat 3* : quasi tangent à l'Est Nord-Est du précédent. Il mesure 11m x 8m et 0,40m de haut.

- *Tertre d'habitat 4* : situé beaucoup plus en hauteur, à une quarantaine de mètres à l'Ouest du *tertre n° 1*. Il mesure 12m x 8m et 0,80m de haut et présente des travaux de captage à sa partie supérieure. (Altitude : 895m).

- *Tertre d'habitat 5* : est à 30m au Sud-Est du précédent. Il mesure 12m x 8m et 0,80m de haut.

Historique

Tertres trouvés par [Blot J.]{.creator} en [1975]{.date}.

### [Esterençuby]{.spatial} - [Heguieder]{.coverage} 8 - [Cromlech]{.type}

Localisation

Altitude : 850m.

Situé à l'Est de la route ; on note surtout le gros abreuvoir en ciment qui a été construit en son milieu. Quand nous l'avions repéré en 1985, ce cercle était très net, ses pierres bien visibles.

Description

Le monument a été fort remanié, mais on peut encore distinguer une quinzaine de pierres délimitant un cercle de 4,50m de diamètre.

Historique

Monument découvert par [Blot J.]{.creator} en [octobre 1985]{.date}. Signalons qu'après cette date des postes de tir à la palombe en parpaings cimentés ont été installés sur toute la ligne de crête d'Heguieder, détruisant par exemple le *Cromlech Heguieder 4*.

### [Esterençuby]{.spatial} - [Heguieder]{.coverage} 9 - [Cromlech]{.type}

Localisation

Altitude : 840m.

Situé à 4m à l'Est de la route.

Description

Tumulus de terre et de pierres, mesurant 4,50m de diamètre et 0,50m de haut.

Historique

Monument découvert par [Blot J.]{.creator} en [octobre 1985]{.date}.

### [Esterençuby]{.spatial} - [Heguieder]{.coverage} 10 - [Tumulus]{.type}

Localisation

Altitude : 830m.

Situé à 4m à l'Est de la route, et à une dizaine de mètres au Sud d'un poste de tir.

Description

Il a probablement été remanié en périphérie par le passage de la route. Tumulus de terre et de pierres, mesurant environ 6m de diamètre et 0,40m de haut.

Historique

Monument découvert par [Blot J.]{.creator} en [octobre 1985]{.date}.

### [Esterençuby]{.spatial} - [Heguieder]{.coverage} 11 - [Tumulus]{.type}

Localisation

Altitude : 830m.

Il se trouve à 8m à l'Ouest de la route, en symétrique au *Heguieder 10 - Tumulus*.

Description

Tumulus très érodé, constitué essentiellement de terre recouverte de gazon, mesurant 6m de diamètre et 0,20m de haut.

Historique

Monument découvert par [Blot J.]{.creator} en [octobre 1985]{.date}.

### [Esterençuby]{.spatial} - [Larregaïchto]{.coverage} Nord - [Cromlech]{.type} ([?]{.douteux})

Localisation

Altitude : 790m.

Il se trouve vers l'extrémité du plateau qui s'étend au Nord de la route.

Description

Cercle de 2,80m de diamètre délimité par une quinzaine de pierres au ras du sol ; y aurait-il d'autres structures voisines ? difficile à dire.

Historique

Monument découvert par [J. Blot]{.creator} en [octobre 2010]{.date}.

### [Esterençuby]{.spatial} - [Larregaïchto]{.coverage} Nord - [Tertre d'habitat]{.type}

Localisation

Altitude : 800m.

Description

Il mesure 5m à 6m de diamètre pour un très faible relief, mais est néanmoins parfaitement visible.

Historique

Monument découvert par [J. Blot]{.creator} en [octobre 2010]{.date}.

### [Esterençuby]{.spatial} - [Larregaïchto]{.coverage} Nord 1 - [Tertre d'Habitat]{.type}

Localisation

Altitude : 730m.

Description :

Il s'agit probablement des restes fusionnés de 3 ou 4 tertres d'habitat, formant une structure allongée suivant un axe Nord-Ouest Sud-Est.

Historique

Monument découvert par [J. Blot]{.creator} en [octobre 2010]{.date}.

À une quinzaine de mètres à l'Ouest Sud-Ouest de ce tertre d'habitat, il semblerait que l'on puisse distinguer, au pied d'une aubépine, un petit cercle (?) de 1,60m de diamètre formé d'une dizaine de pierres au ras du sol.

### [Esterençuby]{.spatial} - [Larregaïchto]{.coverage} Sud 2 - [Tertres d'habitat]{.type}

Localisation

Altitude : 730m.

Description

Tertre de terre, circulaire, de 7m de diamètre et 0,30m de haut.

Un deuxième tertre est visible à 7m au Sud-Ouest de celui-ci et de mêmes dimensions.

Historique

Monument découvert par [J. Blot]{.creator} en [octobre 2010]{.date}.

### [Esterençuby]{.spatial} - [Larregaïchto]{.coverage} Sud - [Tumulus pierreux]{.type} ([?]{.douteux})

Localisation

Altitude : 740m.

Très peu visible au milieu de ce plateau... et des fougères.

Description

Tumulus ovale, à grand axe Nord-Sud, formé de petits blocs pierreux et mesurant 3,50m de long, 2,40m de large et 0,40m de haut environ.

Historique

Monument découvert par [J. Blot]{.creator} en [octobre 2010]{.date}.

### [Esterençuby]{.spatial} - [Mondaizeko Malda]{.coverage} - [Tertre d'habitat]{.type}

Localisation

Altitude : 788m.

Ce tertre se trouve accolé à un petit cayolar relié par une piste carrossable, à la route de crête qui se dirige vers Heguieder, route qui qui le domine à l'Est.

Description

Ce tertre de terre, de forme oblongue, mesurant 16m de long, 13m de large et environ 1m de haut, est allongé selon un axe Nord-Ouest Sud-Est. Il présente, comme souvent, une légère dépression centrale, qui aboutit, au Nord, à un début d'éboulement, ceci paraissant dû aux travaux de construction du cayolar voisin, dont le nivellement du sol a juste côtoyé le tertre dans sa partie Nord. Il nous paraît très peu probable que ce tertre soit le résultat d'un dépôt de déblais à cet endroit.

Historique

Tertre découvert par [Meyrat F.]{.creator} en [décembre 2018]{.date}.

### [Esterençuby]{.spatial} - [Mondaizeko Malda]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 742m.

Ce tumulus, bien visible, est au centre d'un plateau qui s'étend en contre-bas et à l'Ouest de *Mondaizeko Malda - Tertre d'habitat*. Il est à 8m au Nord-Ouest d'un abreuvoir en ciment récemment construit.

Description

Tumulus circulaire, terreux, en forme de galette aplatie, mesurant 9m de diamètre et 0,20m de haut environ. Il n'y a aucune pierre visible.

Historique

Tumulus découvert par [Meyrat F.]{.creator} en [décembre 2018]{.date}.

### [Esterençuby]{.spatial} - [Pagoberry]{.coverage} - [Tertre d'habitat]{.type}

Localisation

Altitude : 980m.

On le voit à une cinquantaine de mètres au Sud-Ouest de la route qui va d'Esterençuby à Urkulu, au moment où elle détache une bretelle menant, au Sud-Ouest, vers Harpea ou à Orbaitzeta.

Ce tertre domine une grande doline qui s'étend au Sud-Ouest et une autre, plus petite, au Sud-Est.

Description

Très beau tertre ovalaire, à grand axe Sud-Est Nord-Ouest, mesurant 16m de long, 12m de large et au moins 1,50m de haut.

Historique

Tertre découvert par [C. Blot]{.creator} en [janvier 2011]{.date}.

### [Esterençuby]{.spatial} - [Salvate]{.coverage} - [Tertre d'habitat]{.type}

Localisation

Altitude : 980m.

Situé à 5m au Nord-Ouest de la route qui mène à Esterençuby, surplombant une importante doline.

Description

Tertre à grand axe orienté Nord-Est Sud-Ouest, mesurant une dizaine de mètres de long, mais dont la largeur est difficile à apprécier du fait du recouvrement partiel de son sommet par des colluvions venant du Sud-Ouest (côté route).

Historique

Tertre découvert par [J. Blot]{.creator} en [janvier 2011]{.date}.

### [Esterençuby]{.spatial} - [Sohandi]{.coverage} - [Tertres d'habitat]{.type}

Localisation

Altitude : 893m.

Ensemble de 3 tertres situé à une centaine de mètres au Sud du groupe précédent.

Description

Ces 3 éléments sont disposés en ligne sur une rupture de pente, et présentent les mêmes dimensions, soit 8m x 8m et 0,30m de haut. Comme pour le groupe précédent, on note immédiatement à l'Ouest les traces du décaissement ayant fourni le matériau nécessaire à leur édification, sous forme d'une surface en demi-cercle de 20m de long et 9m de large.

Historique

Tertres trouvés par [Meyrat F.]{.creator} en [octobre 2018]{.date}.

### [Esterençuby]{.spatial} - [Sohandi]{.coverage} - [Tertres d'habitat]{.type}

Localisation

Cet ensemble de 3 tertres d'habitat est édifié au flanc Est de la colline Sohandi, à peu de distance de son sommet. On note une importante excavation au flanc de cette colline étendue sur près de 45m de long et 17m de large au maximum, orientée Nord-Sud, due au prélèvement de terre nécessaire pour édifier ces 3 tertres très proches les uns des autres.

Description

- *Tertre d'habitat 1* : Altitude : 886m. Volumineux tertre de terre, quasiment circulaire, mesurant 13m de diamètre et 3m de haut environ, à sommet aplati.

- *Tertre d'habitat 2* : presque tangent au précédent, de forme ovalaire (14m x 12m), mais de hauteur identique, il présente une légère dépression à son sommet aplani.

- *Tertre d'habitat 3* : Situé à l'extrémité Sud de l'excavation, il est tangent au précédent, mais de dimensions plus importantes (15m x 6m), présente une forme ovalaire, à sommet plus ou moins pointu.

Historique

Ces trois tertres ont été trouvés par [Blot J.]{.creator} en [septembre 1980]{.date}.

### [Esterençuby]{.spatial} - [Sohandi]{.coverage} 7 - [Tertre d'habitat]{.type}

Localisation

Altitude : 895m.

Ce tertre se trouve légèrement au-dessus de l'ensemble des 3 tertres *Sohandi - Tertres d'habitat*, sur un terrain en pente.

Description

Tertre terreux de forme circulaire, il mesure 4m de diamètre et 0,50m de haut.

Historique

Tertre découvert par [C. Blot]{.creator} en [novembre 2018]{.date}.

### [Esterençuby]{.spatial} - [Ustarazu]{.coverage} - [Tertre d'habitat]{.type}

Localisation

Altitude : 930m.

Il est situé à une dizaine de mètres à au Sud-Ouest de la route qui descend vers Esterençuby, au milieu du *S* qu'elle décrit à cet endroit, et il surplombe la dépression d'une ancienne doline.

Description

Vaste tertre ovale en terre, à grand axe Est Sud-Est, de 16m de long, 8m de large et 0,50m de haut, typiquement « bas-navarrais » (les tertres souletins sont arrondis).

À noter, à quelques 50m au Nord-Ouest., une dalle de [grés]{.subject} isolée, plantée(?) dans le sol de 1,15m de long, distante de 2,20m d'une seconde au Nord-Ouest ; il y aurait un « tumulus » de 5m à 6m de diamètre. Dolmen ? peu probable à notre avis.

Historique

Tertre trouvé par [C. Blot]{.creator} en [janvier 2011]{.date}.
