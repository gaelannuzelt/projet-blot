# Urepel

### [Urepel]{.spatial} - [Caminarte]{.coverage} 1 - [Tumulus]{.type}

Localisation

Carte 1346 Ouest Saint-Etienne-de-Baïgorri.

Altitude : 930m.

Toujours situé, comme les monuments précédents, en forêt d'Hayra, il est à 17m au Sud de la route qui va au col de Teillary, à 500m au Nord-Ouest de ce dernier, et en bordure du chemin de crête qui vient du Sud.

Description

Un amoncellement de blocs de grès et de schiste détermine un tumulus de 5m de diamètre et 0,40m de haut : au centre une légère dépression à grand axe Nord-Sud (fouille ancienne ?).

Historique

Monument découvert en [août 1978]{.date}.

### [Urepel]{.spatial} - [Caminarte]{.coverage} 2 - [Tumulus]{.type}

Localisation

À 1,50m au Sud-Est de *Caminarte 1 - Tumulus*.

Description

Tumulus de 6m de diamètre, plus plat que *Caminarte 1 - Tumulus*, sa hauteur est difficile à évaluer : 0,20m à 0,30m de haut. On distingue bien un petit massif pierreux central, et il y aurait probablement un péristalithe, mais mal individualisé en l'état actuel.

Historique

Monument découvert en [août 1978]{.date}.

### [Urepel]{.spatial} - [Caminarte]{.coverage} 3 - [Tumulus]{.type}

Localisation

Altitude : 920m.

Ce très discret monument se trouve devant un poste de tir à la palombe, à 5m à l'Est des deux autres *Tumulus n°1* et *n°2*, découverts par nous et déjà publiés [@blotInventaireMonumentsProtohistoriques2009 p.54].

Description

Une quinzaine de petits blocs de schiste disposés quasiment en spirale dans le sol, forment comme une galette de 1,40m de diamètre, bien visible, mais dépassant à peine la surface.

Historique

Monument découvert par [J. Blot]{.creator} en [mai 2010]{.date}.

### [Urepel]{.spatial} - [Errola]{.coverage} - [Tertre d'habitat]{.type}

Localisation

Altitude : 790m.

Il est situé sur un petit replat, immédiatement à l'Ouest d'un virage de la piste qui monte vers le sommet du mont Errola en venant du col de Méharostégui.

Description

Tertre de 8m de diamètre, asymétrique, avec un sommet plat d'environ 5m de large et un versant abrupt à l'Ouest.

Historique

Monument découvert par [Blot J.]{.creator} en [décembre 2011]{.date}.

### [Urepel]{.spatial} - [Errola]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 830m.

On le trouve à l'extrémité Sud d'un replat au flanc Sud du mont Errola.

Description

Beau tumulus de 8m de diamètre et plus d'un mètre de haut, constitué de terre et de pierres. Au sommet se voit une légère dépression, allongée suivant un axe Nord-Sud, de 1m de long, 0,80m de large et quelques centimètres de profondeur. Tumulus dolménique ?...

Historique

Monument découvert par [Meyrat F.]{.creator} en [décembre 2011]{.date}.

### [Urepel]{.spatial} - [Harriondoko Kaskoa]{.coverage} - [Tertres d'habitat]{.type}

Localisation

Altitude : 890m.

6 tertres d'habitat sont situés dans un petit col à environ 500m au Sud-Ouest du col d'Hauzay.

Description

On peut décrire 6 tertres d'habitat, établis sur un terrain en légère pente vers le Nord.

- Le *tertre n°1* le plus à l'Est se trouve à environ 200m au Sud-Ouest du petit col à l'arrivée de la route venant d'Hauzay ; de forme ovale comme tous les autres, il mesure 6m x 5m et 0,40m de haut.

- Le *tertre n°2* est à 26m au Sud-Ouest du précédent, il mesure 7m x 6m et 0,30 de haut.

- Le *n°3* est à 14 m au Sud-Ouest du *n°2* ;

- le *n°4* à 15m à l'Ouest du *n°2* et on peut voir les vestiges de 2 autres tertres accolés à lui, côté Est. 

- Le *n°5* est à 5m à l'Est du précédent, et lui aussi possède les restes de 2 ou 3 tertres à son flanc Nord-Ouest. 

- Enfin le *tertre n°6* est à 7m au Nord du précédent et mesure 7m x 7m.


Historique

Tertres découverts par [J. Blot]{.creator} en [mai 2010]{.date}.

### [Urepel]{.spatial} - [Hauzay]{.coverage} - [Dolmen]{.type}

Localisation

Carte 1346 Ouest Saint-Etienne-de-Baïgorri.

Altitude : 990m.

Il est situé sur cette longue ligne de croupes orientée Nord-Sud, qui s'étend du mont Otsaxar au Nord, au Lindus au Sud. Il est à environ 150m au Sud du col d'Hauzay, et en bordure de la piste pastorale antique dominant la route récemment tracée, toute proche.

Description

Tumulus pierreux d'un diamètre de 5,30m environ, et 0,40m de haut. Il semblerait que l'on puisse y distinguer un péristalithe, difficile cependant à affirmer dans le contexte pierreux de ce tumulus. La [chambre funéraire]{.subject}, orientée Sud Sud-Est mesure 1,40m de long, 0,90m de large et 0,60m de profondeur. De cette chambre, il ne reste que 2 dalles plantées dont une, à l'Ouest, mesure 1,40m de long et 0,60m de haut ; à l'Est gît la grande dalle de couverture, mesurant 1,40m de long et 0,90m de large.

Historique

Dolmen découvert en [août 1972]{.date}.

### [Urepel]{.spatial} - [Hortz Zorrotz]{.coverage} - [Dolmen]{.type}

Localisation

Carte 1346 Ouest Saint-Etienne-de-Baïgorri.

Altitude : 1010m.

Il est situé en bordure de la piste pastorale qui vient du cirque rocheux d'Hortz-Zorrotz, et des monuments que celui-ci recèle à 150m au Nord (voir cromlechs et tumulus décrits à la commune de Banca).

Description

Tumulus pierreux de 11m de diamètre et 0,50m de haut, recouvert de mousse. La [chambre funéraire]{.subject}, orientée Sud Sud-Est, mesure 1,44m de long, 0,90m de large et 0,30m de profondeur ; elle est délimitée par 2 dalles perpendiculaires espacées de 0,50m, ayant toutes deux les mêmes mensurations : 0,90m de long, 0,30m de haut et 0,30m d'épaisseur.

Historique

Dolmen découvert en [août 1972]{.date}.

### [Urepel]{.spatial} - [Hortz Zorrotz]{.coverage} 2 - [Dolmen]{.type}

Localisation

Altitude : 1025m.

Ce dolmen est tangent et à droite de la piste qui monte du col d'Hortz Zorrotz vers Urtaray et qui longe des postes de tir à la palombe.

Description

Tumulus formé d'un amoncellement de blocs rocheux couverts de mousse, (très similaire à *Iraztei 3 - Dolmen*), érigé sur un terrain en légère pente vers le Nord-Ouest. Tumulus de forme ovale, allongé dans le sens de la pente, mesurant 7m x 5m et 0,40m de haut.

Au centre une dalle plantée verticalement, selon un axe Nord-Ouest Sud-Est, au milieu d'une légère dépression, marque la [chambre funéraire]{.subject} ; cette dalle mesure 0,80m de long, 0,24m de haut et 0,20m d'épaisseur.

Historique

Dolmen trouvé par [Blot J.]{.creator} en [juin 2012]{.date}.

### [Urepel]{.spatial} - [Hortz Zorrotz]{.coverage} - [Monolithe]{.type}

Localisation

Altitude : 1010m.

Description

Belle dalle de [calcaire]{.subject} blanc, de forme rectangulaire dans l'ensemble, mesurant 3,18m de long et 1,51m dans sa plus grande largeur, et 0,15 à 0,20m d'épaisseur en moyenne. Ce qui la rend particulièrement remarquable c'est l'existence d'un épannelage sur tout son pourtour, et le fait qu'il semble nettement qu'on ait voulu donner à cette dalle un aspect « anthropomorphe » en faisant apparaître deux reliefs, à gauche et surtout à droite, évoquant des épaules de part et d'autre d'une « tête » très schématique, mais bien individualisée. Un trait de cassure incomplet dû au gel ou à l'homme, a presque détaché la « tête » du corps. Il serait intéressant de retourner cette dalle pour vérifier l'existence ou non de gravures sur l'autre face.

Historique

Monolithe découvert par [I. Txintxuretta]{.creator} en [août 2010]{.date}.

### [Urepel]{.spatial} - [Hortz Zorrotz]{.coverage} - [Tertres d'habitat]{.type}

Localisation

Altitude : 980m.

Trois tertres d'habitat se trouvent au flanc Nord de la barre rocheuse d'Hortz Zorrotz, quasiment accolés à elle. Ils sont aussi au Sud-Ouest et au-dessus d'un vaste replat qui domine le col d'Hauzay, au Sud.

Description

- Le *TH n°1*, le plus à l'Ouest des trois, se présente sous la forme d'un important tumulus ovale, de terre et de pierres assez volumineuses, érigé sur un terrain en pente vers le Nord-Ouest, mesurant 8m de long, 7 de large et 2m de haut environ ; le sommet est plat. Il s'agit d'un TH de type bas-navarrais assez démonstratif.

- Le *TH n°2* est à 10m au Sud-Est et lui est semblable en tous points.

- Le *TH n°3* est 8m au Sud-Est du précédent, mais est moins net tant dans sa forme que dans ses dimensions.

Historique

Monuments découverts par [Blot Colette]{.creator} et [Meyrat F.]{.creator} en [septembre 2012]{.date}.

### [Urepel]{.spatial} - [Ichterbégui]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 863m.

Ce monument se trouve à une centaine de mètres à l'Ouest de la borde dénommée Biperren Borda, sur un petit replat existant sur une prairie en pente au flanc Nord du mont Itcherbégui. Ce tumulus est situé à 14m au Nord Nord-Est d'un muret qui s'étend d'Est en Ouest et à une centaine de mètres, toujours au Nord Nord-Est, d'une banquette de tir située dans une prairie, à proximité de sa périphérie marquée par des arbres. Cette montagne d'Itcherbégui est très riche en éléments de défense se rapportant très probablement à l'époque napoléonienne.

Description

Petit tumulus de terre, en forme de dôme circulaire mesurant 4m de diamètre et 0,40m de haut. Pas de pierres visibles.

Historique

Monument découvert par [F. Meyrat]{.creator} en [avril 2019]{.date}.

### [Urepel]{.spatial} - [Iraztei]{.coverage} - [Cromlech]{.type}

Localisation

Altitude : 960m (le point culminant IGN, 962m, se trouve à 8m à l'Ouest Nord-Ouest). Enfin il est à 80m au Sud de *Iraztei 3 - Dolmen*.

Description

Érigé sur terrain plat, on note un cercle de 8m de diamètre, délimité par des pierres de calibre variable - certaines volumineuses - plantées ou enfouies dans le sol. Les plus volumineuses sont dans la moitié Sud du monument, et 4 autres dans une légère dépression centrale mesurant 5,40m de diamètre et 5 centimètres de profondeur.

Dans le quart Nord-Ouest du cercle apparaissent de nombreuses pierres de calibre plus réduit.

Historique

Monument découvert par [L. Millan]{.creator} en [juin 2012]{.date}.

### [Urepel]{.spatial} - [Iraztei]{.coverage} 3 - [Dolmen]{.type}

Localisation

Altitude : 960m.

Ce dolmen se trouve à 20m au Sud Sud-Ouest du *Dolmen n°2*, lequel se trouve à 23m au Sud Sud-Ouest du *n°1*, ces deux derniers monuments ayant été décrits par JM de Barandiaran [@barandiaranCronicaPrehistoriaAlduides1949 p.7].

Description

On note un tumulus pierreux de 6 mètres de diamètre et 0,60m de haut, érigé sur terrain plat, formé de blocs de schiste gréseux de dimensions variables. Au centre se voit une pierre rectangulaire de calibre plus important, presque couchée à la surface du tumulus, selon un axe Nord Sud, l'extrémité Sud étant enfoncée dans le monument. Elle mesure 0,80m de long, 0,71m de large et 0,40m d'épaisseur en moyenne. Vestige d'un élément de la chambre funéraire ou de son couvercle ?

Historique

Monument découvert par [J. Blot]{.creator} en [mai 2010]{.date}.

Rappelons ici pour mémoire, et toujours dans la commune d'Urepel, l'ensemble formé par :

- les *deux cromlechs d'Hortz-Zorrotz*: *n°1* et *n°2* [@blotNouveauxVestigesMegalithiques1972b p.192],

et

- les *deux tumulus* du même nom [@blotInventaireMonumentsProtohistoriques2009 p22].

    À propos de ces tumulus, précisons que :

    - Le *Tumulus n°3 d'Hortz-Zorrotz, à 4m au Sud Sud-Est du *Cromlech n°2*, présente en son centre un petit amas pierreux d'environ 8 pierres - de calcaire local - et que sa périphérie pourrait être délimitée par une douzaine de pierres, peu visibles, disposées en cercle.

    - Le *Tumulus pierreux n°4 d'Hortz-Zorrotz*, à 2m au Sud-Ouest du *Cromlech n°2* mesure une vingtaine de centimètres de haut, et est constitué d'une quinzaine de pierres (calcaire local) dont certaines paraissent délimiter une ébauche de cercle périphérique, surtout visible dans sa moitié Nord-Est.

### [Urepel]{.spatial} - [Lezetako Kaskoa]{.coverage} - [Cromlech]{.type} ([?]{.douteux})

Localisation

Altitude : 600m.

Il est situé à 5m au Nord du chemin empierré qui conduit de Bihurrietabuztanénéa au mines d'or, à son arrivée sur le plateau. Ce chemin se détache lui-même sur la gauche de la route venant de la colline Kutxaxarreta.

Description

Sur un terrain sensiblement plat, on note près d'une quinzaine de pierres, de calibres variés, plantées dans le sol, et qui paraissent délimiter un cercle de 3m de diamètre. Monument douteux...

Historique

Monument découvert par [M. Txoperena]{.creator} et signalé à [L. Millan]{.creator} en [novembre 2011]{.date}.

### [Urepel]{.spatial} - [Lezetako Kaskoa]{.coverage} - [Tumulus-cromlech]{.type} ([?]{.douteux})

Localisation

Il est à 3m au Sud-Est de *Lezetako Kaskoa - Cromlech*.

Description

Tumulus de 3m de diamètre ; une profonde excavation est visible en son centre, mesurant 1,40m de large. Une dizaine de pierres à la périphérie de celle-ci peuvent faire penser à un péristalithe (?). Monument douteux.

Historique

Monument découvert par [M. Txoperena]{.creator} et signalé à [L. Millan]{.creator} en [novembre 2011]{.date}.

### [Urepel]{.spatial} - [Mizpira]{.coverage} 2 - [Tumulus]{.type}

Localisation

Carte 1346 Ouest Saint-Etienne-de-Baïgorri.

Altitude : 830m*.*

Ces monuments sont situés sur la ligne de croupe, bien plus au Nord que les précédents. Nous avions décrit [@blotNouveauxVestigesMegalithiques1972b p.190], un premier tumulus dans la commune des Aldudes ; deux autres tumulus, tangents, sont situés à 180m environ au Sud Sud-Est du précédent, à 830m d'altitude, dans un petit col au carrefour de plusieurs pistes pastorales.

Description

Petit tumulus mixte de terre et de petites pierres, aplati en galette, de 6m de diamètre et 0,50m de haut.

Historique

Ce monument a été découvert en [août 1972]{.date}. Il a été rasé depuis par le passage d'une route.

### [Urepel]{.spatial} - [Mizpira]{.coverage} 3 - [Tumulus]{.type}

Localisation

Il est tangent au Sud de *Mizpira 2 - Tumulus*.

Description

Petit tumulus mixte de terre et de petites pierres, aplati en galette, de 6m de diamètre et 0,50m de haut.

Historique

Ce monument a été découvert en [août 1972]{.date}. Il a été rasé depuis par le passage d'une route.

### [Urepel]{.spatial} - [Patarramunho]{.coverage} - [Tertres d'habitat]{.type}

Localisation

Sur le flanc Ouest du mont Patarramunho, à une soixantaine de mètres au Nord de la borde Zurguinaren borda, sur un terrain en pente. On trouve deux groupes de deux tertres d'habitat.

Description

- Groupe du haut : *Tertres d'habitat A* et *B*.

    - *TH A* :

        Altitude : 850m.

        Tertre de terre, dissymétrique, érigé sur un terrain en pente vers le Nord, allongé dans le sens Est-Ouest, mesurant 6m x 4m et 0,50m de haut.

    - *TH (?) B* : situé à 4m à l'Est du précédent ; beaucoup plus modeste, ne mesure que 3m de diamètre et 0,30m de haut ; tertre douteux.

- Groupe du bas : *Tertres d'habitat C* et *D*. 
    
    Ce groupe du bas est à une centaine de mètres plus bas que le groupe précédent, et dans une zone boisée.

    - *TH C* :

        Altitude : 830m.

        Ce tertre d'habitat, constitué de terre, érigé sur terrain en pente, affecte la forme dissymétrique classique, mesure 6m x 4m et 0,60m de haut.

    - *TH D* :

        Altitude : 812m.

        Tertre de terre, de même forme que *C*, érigé sur terrain en pente, situé à 25m à l'Est Sud-Est de lui mais un peu plus en hauteur, mesure 6m x 4m et 0,50m de haut.

### [Urepel]{.spatial} - [Urtaray]{.coverage} 1 - [Tumulus]{.type}

Localisation

Altitude : 1120m.

Monument érigé sur la ligne de crête.

Description

Petit tumulus pierreux de 30 à 40 centimètres de haut, constitué d'un amas central recouvert par une dalle de 0,84m de long, 0,54m de large et 0,20m d'épaisseur, à base large et arrondie vers l'Est et plus étroite vers l'Ouest. Cette dalle est entourée d'une vingtaine de pierres, de calibre plus modeste, enfouies dans le sol mais cependant mobiles. La périphérie de tumulus est marquée par huit pierres (péristalithe ?) dépassant de peu la surface du sol, certaines atteignant cependant 0,50m à 0,60m de long.

Historique

Monument découvert par [J. Blot]{.creator} en [juillet 2010]{.date}.

### [Urepel]{.spatial} - [Urtaray]{.coverage} 2 - [Tumulus]{.type} ([?]{.douteux})

Localisation

Altitude : 1140m.

Monument érigé sur la ligne de crête.

Description

Petit tumulus pierreux, apparaissant sous la forme d'un léger relief circulaire d'environ 4m de diamètre et quelques centimètres de haut. Au centre se voient une douzaine de petites pierres, tandis qu'à la périphérie, à l'Est et surtout à l'Ouest, on peut en voir quelques autres.

Historique

Monument découvert par [J. Blot]{.creator} en [juillet 2010]{.date}.
