# Saint-Martin-d'Arrossa

### [Saint-Martin-d'Arrossa]{.spatial} - [Dondénia]{.coverage} 4 - [Dolmen]{.type}

Localisation

Carte 1345 Ouest Cambo-les-Bains.

Altitude : 380m.

Nous avons décrit un ensemble de 3 dolmens [@blotNouveauxVestigesMegalithiques1972c p. 32], situé au Sud-Est de la ferme Pagondoa.

Ce n°4 est situé à 13m au Nord-Ouest du n°2 et à 30m au Nord-Est du n°3 [@blotNouveauxVestigesMegalithiques1972c p. 32].

Description

Tumulus pierreux de 9m de diamètre et 0,30m de haut. La [chambre funéraire]{.subject} semble pouvoir être matérialisée par une dalle mesurant 1,40m de long et 0,50m de large, orientée vers le Nord-Est et apparaissant à plat (légèrement inclinée vers le Sud-Est), au sommet du tumulus. Quelques petits fragments de dalles émergent du sol de part et d'autre de la table et pourraient faire partie des montants latéraux ; une dalle couchée dans le quart Sud-Est du tumulus, et mesurant 1m de long et 0,80 de large pourrait, elle aussi, en faire partie.

Historique

Dolmen découvert en [mars 1974]{.date}.

### [Saint-Martin-d'Arrossa]{.spatial} - [Dondénia]{.coverage} 5 - [Tumulus]{.type} ([?]{.douteux})

Localisation

Carte 1345 Ouest Cambo-les-Bains.

Il est situé à 13m à l'Ouest Nord-Ouest du dolmen n°3 [@blotNouveauxVestigesMegalithiques1972c p. 32] et à 40m au Sud-Ouest de *Dondénia 4 - Dolmen*.

Description

Tumulus mixte de pierres et de terre, mesurant 8m de diamètre et 0,30m de haut. *Il est traversé par la piste pastorale* et quelques fragments de dalles, brisées au ras du sol, apparaissent au centre du monument : vestige d'une chambre funéraire ? est-ce un dolmen ?.

Historique

Monument découvert en [mars 1971]{.date}. Rappelons aussi le *dolmen de Mikelare*, à 500m au Nord Nord-Est de Dondénia [@blotNouveauxVestigesMegalithiques1972c p. 30].

### [Saint-Martin-d'Arrossa]{.spatial} - [Dondénia]{.coverage} 6 - [Dolmen]{.type}

Localisation

Altitude : 380m (à 2m au Nord-Ouest de *D 4*).

Description

Un ensemble de six dalles de [grés]{.subject}, plantées verticalement dans le sol et ne le dépassant que de quelques centimètres, délimitent une [chambre funéraire]{.subject} de 3,70m de long et 1,40m de large, orientée Nord-Est Sud-Ouest. On note 2 dalles pour les montants Sud-Est, l'une de 1,10m de long, l'autre de 0,74m. Au Nord-Ouest, 4 dalles, deux de 0,90m de long, la troisième de 2m de long et 0,51m de haut, la quatrième de 1,06m de long.

Historique

Monument découvert par [Blot J.]{.creator} en [novembre 2011]{.date}.

### [Saint-Martin-d'Arrossa]{.spatial} - [Dondénia]{.coverage} - [Pierre couchée]{.type}

Localisation

Situé à quelques dizaines de mètres au Sud Sud-Est du dolmen *Dondénia 2* [@blotNouveauxVestigesMegalithiques1972c p. 32].

Description

Beau bloc parallélépipédique de grés rose, gisant au sol, suivant un axe Est-Ouest, mesurant 1,35m de long, 0,80m de large, et 0,42m d'épaisseur en moyenne.

Il présente de nombreuses traces d'épannelage sur son flanc Sud, et sa base comme son sommet Ouest, plus étroit, semblent avoir été brisés récemment. Travail récent ?

Historique

Monument découvert par [Blot J.]{.creator} en [novembre 2011]{.date}.

### [Saint-Martin-d'Arrossa]{.spatial} - [Dondénia]{.coverage} - [Pierres plantées]{.type} ([?]{.douteux})

Localisation

Très légèrement au Sud du groupe des *dolmens de Dondénia* (2,3,4,6).

Description

On peut voir quelques pierres plantées, de faibles dimensions (0,76m de haut et 0,52m de large pour l'une d'elles séparée de trente centimètres de sa voisine, cette dernière mesurant 0,60m de haut et 30m de long). Ces pierres assez distantes les unes des autres sont plus ou moins alignées selon un axe Est-Ouest, et semblent faire partie d'un très ancien enclos dont les autres vestiges se voient enfouis au ras du sol. Cet enclos (?) forme un angle droit un peu après le dolmen D 3 et se dirige vers le Sud sur environ 12m.

Historique

Pierres vues par [Blot J.]{.creator} en [1972]{.date}.

### [Saint-Martin-d'Arrossa]{.spatial} - [Ondaya]{.coverage} - [Tumulus]{.type}

Localisation

Carte 1345 Ouest Cambo-les-Bains.

Altitude : 310m.

Il est situé à quelques mètres à droite de la route qui monte de Saint-Martin-d'Arossa vers Dondénia, au niveau d'un petit col situé au premier replat après la montée.

Description

Tumulus pierreux de 10m de diamètre et 0,60m de haut ; à son sommet une dalle couchée pourrait être le couvercle d'une [ciste]{.subject} centrale sous-jacente dont les sommets des dalles Ouest sont visibles.

Historique

Monument découvert en [avril 1972]{.date}.

### [Saint-Martin-d'Arrossa]{.spatial} - [Ondaya]{.coverage} 2 - [Tumulus]{.type}

Localisation

Altitude : 314m.

Le n° 1 [@blotInventaireMonumentsProtohistoriques2009, p.53], est situé à 20 mètres au Sud-Ouest de la route et le n°2 est lui-même à une cinquantaine de mètres au Nord Nord-Ouest de ce n°1 (dont on notera la dalle horizontale à son sommet, mesurant 0,70m x 0,60m et 0,30m d'épaisseur apparente).

Description

Tumulus pierreux de 7m de diamètre et d'une trentaine de centimètres de haut. Au centre est parfaitement bien visible une structure en petites dalles verticales disposées en « pelure d'oignon ».

Historique

Monument découvert par [C. Blot]{.creator} en [septembre 2010]{.date}.

### [Saint-Martin-d'Arrossa]{.spatial} - [Harretchekoborda]{.coverage} 1 - [Cromlech]{.type}

Localisation

Altitude : 630m.

On le trouve à gauche d'un virage à angle droit du chemin, bien marqué, qui monte au flanc Nord du pic Larla, le terrain devenant plat, avant de redescendre quelques dizaines de mètres plus loin. Sur la droite de ce virage se trouve la borde *Harretchekoborda*, et à gauche s'amorce une piste se dirigeant vers le sommet de Larla. Le monument est à gauche de cette piste, à quelques dizaines de mètres de son départ.

Description

Sur un terrain en pente douce vers le Nord-Est, une quinzaine de pierres, au ras du sol, paraissent délimiter un cercle en léger relief de 3m de diamètre environ ; au centre, 3 pierres feraient partie d'un petit caisson...

Il pourrait y avoir deux autres cercles, difficiles à identifier (en raison probablement des phénomènes de colluvion dus à la pente) : l'un se trouvant au Nord Nord-Ouest, l'autre à l'Est Sud-Est.

Historique

Monument découvert par [J. Blot]{.creator} en [septembre 2010]{.date}.

### [Saint-Martin-d'Arrossa]{.spatial} - [Harretchekoborda]{.coverage} 2 - [Cromlech]{.type}

Localisation

À une vingtaine de mètres au Sud de *Harretchekoborda 1 - Cromlech*, tangent et à droite de la piste qui monte au Larla.

Description

Une quinzaine de pierres délimitent un cercle bien visible érigé sur un sol en légère pente vers le Nord-Ouest.

Historique

Monument découvert par [J. Blot]{.creator} en [septembre 2010]{.date}.

### [Saint-Martin-d'Arrossa]{.spatial} - [Harretchekoborda]{.coverage} 3 - [Dolmen]{.type}

Localisation

Altitude : 640m.

Il est un peu plus en hauteur et à l'Est de *Harretchekoborda 2 - Cromlech*, à environ 8 mètres au Nord-Est de la piste ascendante vers le Larla.

Description

Monument bien dégradé, dont 7 dalles délimitent une [chambre funéraire]{.subject} à grand axe Nord-Est Sud-Ouest, mesurant environ 1,60m de long et 0,60m de large. La dalle Nord-Ouest, la plus importante, est inclinée vers l'extérieur et mesure 0,70m de long et 0,50m de haut.

Il n'y a pas de tumulus visible.

Historique

Monument découvert par [J. Blot]{.creator} en [septembre 2010]{.date}.

### [Saint-Martin-d'Arrossa]{.spatial} - [Larla]{.coverage} 1 - [Monolithe]{.type}

Localisation

Altitude : 670m.

Description

On trouve ce monolithe à proximité immédiate d'un filon rocheux et de nombreux autres blocs de grés. Toutefois il s'en distingue par sa forme et ses dimensions.

Il affecte, en effet, une forme de parallélépipède rectangle allongé, à grand axe Nord-Ouest Sud-Est, mesurant 3,50m de long, 0,80m dans sa plus grande largeur, 0,47m à sa base et 0,35m à sa pointe Nord-Ouest. Son épaisseur varie d'une trentaine à plus d'une quarantaine de centimètres. De nombreuses traces d'épannelage sont visibles tout le long de son bord Nord-Est, ainsi qu'à sa base, au Sud-Est, et, peut-être, à sa pointe.

Ce monolithe, ainsi que *Larla 2 - Monolithe*, pourraient avoir été mis en forme sur place, au niveau du filon d'origine et ne pas avoir été ensuite amené, pour une raison quelconque, à sa destination.

Historique

Monument découvert par [J. Blot]{.creator} en [septembre 2010]{.date}.

### [Saint-Martin-d'Arrossa]{.spatial} - [Larla]{.coverage} 2 - [Monolithe]{.type}

Localisation

À une dizaine de mètres au Sud Sud-Ouest de *Larla 1 - Monolithe*.

Description

Il se présente sous la forme d'une dalle de [grés]{.subject} rectangulaire, à grand axe Nord-Ouest Sud-Est, mesurant 2,50m de long et 1,65m à la base. Son épaisseur n'est que de quelques centimètres. On note enfin des traces d'épannelage très nettes à son extrémité Nord-Ouest.

Historique

Monument découvert par [J. Blot]{.creator} en [septembre 2010]{.date}.

### [Saint-Martin-d'Arrossa]{.spatial} - [Pikassary]{.coverage} - [Cromlech]{.type}

Localisation

Altitude : 470m.

Il est sur un petit plateau au flanc Nord-Est du mont Larla, à une quinzaine de mètres au Sud-Ouest d'une barre rocheuse.

Description

Petit cercle de 3m de diamètre délimité par une quinzaine de pierres, au ras du sol et de taille modeste (0,20m maximum) ; deux autres pierres apparaissent dans le centre légèrement surélevé.

Historique

Cercle découvert par le [groupe Hilharriak]{.creator} en [décembre 2012]{.date}.

### [Saint-Martin-d'Arrossa]{.spatial} - [Pikassary]{.coverage} - [Dalle couchée]{.type}

Localisation

Elle gît à 12m au Nord-Est de *Pikassary - Cromlech*, tout à côté de l'amas rocheux qui termine le promontoire.

Description

Belle dalle de [grés]{.subject} rose en forme de pain de sucre, mesurant 2,50m de long, 0,80m à sa base et 0,15m d'épaisseur. Elle présente des traces d'épannelage sur son bord Nord-Est et à son sommet Sud-Est.

Historique

Dalle découverte par [Blot J.]{.creator} en [janvier 2012]{.date}.
