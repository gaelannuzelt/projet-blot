# Hasparren

### [Hasparren]{.spatial} - [Haitzeder]{.coverage} 1 - [Tumulus]{.type}

Localisation

Carte 1344 Ouest Hasparren.

Altitude : 90m.

*Haitzeder 1*, *Haitzeder 2* et *Haitzeder 3* se trouvent dans les landes communales d'Hasparren.

Le n°1 est situé le long d'une piste pastorale qui le longe sur son flanc Nord.

Description

Tumulus terreux de 16m de diamètre et 1,70m de haut ; une piste passe sur son flanc Nord.

Historique

Monument découvert en [mai 1972]{.date}.

### [Hasparren]{.spatial} - [Haitzeder]{.coverage} 2 (Enseigne) - [Tumulus]{.type}

Localisation

Carte 1344 Ouest Hasparren.

Altitude : 150m.

Il est à 600m au Nord-Est de *Haitzeder 1 - Tumulus*, à droite de la piste pastorale, quand on se dirige vers le Nord.

Description

Tumulus de terre de 15m de diamètre et 1m de haut.

Historique

Monument découvert en [mai 1972]{.date}. Ce monument a été rasé par des labours ultérieurs.

### [Hasparren]{.spatial} - [Haitzeder]{.coverage} 3 - [Tumulus]{.type}

Localisation

Carte 1344 Ouest Hasparren.

Altitude : 154m.

Il est situé à 900m au Nord-Ouest de *Haitzeder 2 (Enseigne) - Tumulus* et à 4m au Nord du chemin se rendant à Pilota-Plaza et venant de Ensenia-Zaharrea.

Description

Tumulus de terre de 16m de diamètre et 0,80m de haut.

Historique

Monument découvert en [mai 1972]{.date}. Monument rasé par des labours ultérieurs.

### [Hasparren]{.spatial} - [Pelloeneko Oyana]{.coverage} 1 - [Tumulus]{.type}

Localisation

Carte 1344 Ouest Hasparren.

Altitude : 126m.

Il est situé à 30m au Sud-Ouest de la maison Irunagekoborda.

Description

Tumulus terreux de 19m de diamètre et 1,50m de haut, qui présente à son flanc Est une importante dépression qui pourrait bien résulter d'une fouille ancienne.

Historique

Monument découvert en [mai 1972]{.date}.

### [Hasparren]{.spatial} - [Pelloeneko Oyana]{.coverage} 2 - [Tumulus]{.type}

Localisation

Carte 1344 Ouest Hasparren.

Altitude : 159m.

Il est à 1400m au Sud-Est de *Pelloeneko Oyana 1 - Tumulus*, et à proximité immédiate d'un carrefour de deux pistes pastorales.

Description

Tumulus terreux de 25m de diamètre et 1,20m de haut. Une piste passe sur son sommet où apparaissent quelques pierres sans ordre apparent.

Historique

Monument découvert en [mai 1972]{.date}.

### [Hasparren]{.spatial} - [Pitxao Borda]{.coverage} - [Dolmen]{.type}

Localisation

Altitude : 362m.

Il est situé au point le plus élevé d'un vaste plateau situé au flanc Nord de l'Ursuya, à environ 800m environ, à vol d'oiseau, au Sud du chemin qui rejoint la Route de Napoléon, à 200m environ au Nord-Est d'un captage de source et à 150m au Sud-Ouest d'une borde neuve.

Description

Une abondante végétation cachait un ensemble de 5 blocs de [grés]{.subject} local, qui ne paraissent pas être « en place », mais avoir donc été amenés là volontairement ; on notera par ailleurs qu'il n'y a aucun autre élément rocheux dans les environs.

L'important nettoyage effectué par F. Meyrat a permis de mettre en évidence que deux dalles sont solidement enfoncées dans le sol, les dalles *A* et *B*, les autres blocs ne semblant que posés.

- *La dalle A* mesure 0,60m de long, 0,48m de haut et 0,25m d'épaisseur et 0,48m de haut.

- *La dalle B* mesure 1,20m de long, 0,16m d'épaisseur et 0,67m de haut.

- *La dalle C*, de forme triangulaire à base arrondie et à sommet Sud-Est, est couchée au sol et touche les deux précédentes ; elle mesure 1,40m de long, 0,90m de large dans sa partie moyenne, et présente, semble-t-il des traces d'épannelage dans sa partie en pointe au Sud-Est.

- *Le bloc D*, repose en partie sur le bloc *E*, en forme de parallélépipède allongé, mesure 1,50m de long, 0,50m de large à sa partie médiane et 0,46m d'épaisseur en moyenne.

- *Le bloc E*, en forme de parallélépipède rectangle allongé, couché au sol, mesure, en moyenne, 1,60m de long, 0,42m d'épaisseur et 0,40m de largeur. 

S'agit-il d'un dolmen ?

Si on ne note pas la présence de tumulus, ces dalles et blocs de pierre sont parfaitement isolés, il n'y a aucun élément pierreux dans le voisinage et cela ne ressemble en rien à un « tas d'épierrage ». Par contre, la présence de deux dalles plantées et d'une autre (la *C*) présentant des traces de retouche, nous font penser à une possible (??) [chambre funéraire]{.subject} - certes fort endommagée - à grand axe Est Nord-Est Ouest Sud-Ouest.

Historique

Monument signalé par [P. Badiola]{.creator} en [Janvier 2016]{.date}.

### [Hasparren]{.spatial} - [Ursubehere]{.coverage} 1 - [Monolithe]{.type} ([?]{.douteux})

Localisation

Altitude : 463m.

Située *presque* au point culminant d'un petit replat qui fait suite, au Nord, à un autre replat plus haut situé, dénommée Ursubehere, lui-même au flanc Nord Nord-Ouest du mont Ursuya.

Description

On note un gros bloc rocheux de forme générale pyramidale, piriforme. Il mesure 3,30m de haut, et presque autant à sa base. Ce bloc de grès ne présente pas de traces d'épannelage visible ; il est entouré de 6 autres blocs de moindre dimensions, non contigus, qui ne lui servent pas de calage. Ce bloc de [grés]{.subject} présente de nombreuse fissures, dont certaines ont très probablement été dans le passé (gel) à l'origine du détachement de certains des blocs des environs immédiats, dont un au Nord et un autre plus volumineux au Nord-Est.

Il est difficile de voir ici un monolithe au sens anthropique de la définition, mais il n'est pas exclu que ce rocher ait pu servir de repère naturel.

Historique

Pierre signalée par [Badiola P.]{.creator} en [mai 2016]{.date}.

### [Hasparren]{.spatial} - [Ursubehere]{.coverage} 2 - [Monolithe]{.type} ([?]{.douteux})

Localisation

Altitude :457m.

Description

Une autre dalle de [grés]{.subject}, couchée au sol selon un axe Nord-Est Sud-Ouest, de forme triangulaire, mesurant 3m de long, 1,80m dans sa plus grande largeur, et 0,80m d'épaisseur ; elle ne présente aucune trace d'épannelage.

Elle est toute proche d'un second bloc de grès de 3,20m de long, 2,90m de large et 1m de haut, sans aucune caractéristique particulière.

Historique

[Francis Meyrat]{.creator} nous l'a signalé en [mai 2016]{.date}.
