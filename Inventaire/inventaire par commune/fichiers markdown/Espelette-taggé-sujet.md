# Espelette

### [Espelette]{.spatial} - [Errebi]{.coverage} 1 et 2 - [Cromlechs]{.type}

Localisation

Carte 1245 OT Hendaye Saint-Jean-de-Luz.

Altitude : 540m.

Ces deux cromlechs tangents sont situés à environ 40 mètres au Sud Sud-Est du pointement rocheux coté 554, à quelques mètres à l'Ouest de la piste qui suit la ligne de crête.

Description

- *Erebi 1 - Cromlech* : une dizaine de pierres en [grés]{.subject} local gris, au ras du sol, délimitent un cercle de 4,70m de diamètre dont l'intérieur est très légèrement surélevé.

- *Erebi 2 - Cromlech* : est tangent au précédent à l'Ouest Sud-Ouest, matérialisé par 5 pierres disposées en arc de cercle.

On notera, un peu plus bas, à 380m d'altitude, sur un replat et à quelques mètres au Sud-Ouest d'une piste qui rejoint le col de Pinodieta, ce qui paraît être un léger tumulus de blocs de [grés]{.subject}, de 6m de diamètre, avec en son centre une dalle de 0,60m de long, orientée Ouest-Est,

Vestiges d'un dolmen ?

Historique

Monuments découverts par [Blot J.]{.creator} en [septembre 1972]{.date}.

### [Espelette]{.spatial} - [Errebi]{.coverage}- [Dalle couchée]{.type}

Localisation

Altitude : 578m.

Cette dalle couchée se trouve à 32m à l'Est du point géodésique marquant le sommet du mont Errebi, dans l'axe de la piste de crête.

Description

Cette dalle de [grés]{.subject}, couchée au sol est le seul élément lithique visible dans les environs et semble donc bien avoir été volontairement apportée ici.

De forme grossièrement triangulaire à sommet Sud-Ouest, elle mesure 1,20m à la base et 1,75m de la base au sommet. Tout le bord Ouest ainsi qu'une partie de la base Nord paraissent présenter des traces d'epannelage. Elle est brisée en son milieu. (Chute probable d'une dalle plantée...).

Historique

Dalle découverte en [septembre 2019]{.date} par [F. Meyrat]{.creator}.

### [Espelette]{.spatial} - [Errebi]{.coverage} - [Dolmen]{.type} ([?]{.douteux})

Nous tenons à rappeler ici qu'on peut noter, un peu plus bas, à 380m d'altitude, sur un replat et à quelques mètres au sud-ouest d'une piste qui rejoint le col de Pinodieta, ce qui paraît être un léger tumulus de blocs de [grés]{.subject}, de 6 mètres de diamètre, avec en son centre une dalle de 0,60m de long, orientée ouest-est. Est-ce des vestiges d'un dolmen ?

Historique  

Monuments découverts par [Blot J.]{.creator} en [septembre 1972]{.date}.

### [Espelette]{.spatial} - [Gaztelaenea]{.coverage} - [Tumulus]{.type}

Localisation

Carte 1245 Est Espelette.

Altitude : 93m.

Il est situé à droite et à 30m environ du chemin qui relie Gaztelaeneakoborda à la ferme du même nom.

Description

Tumulus de terre bien visible, d'un diamètre de 15m et 0,80m de haut.

Historique

Monument découvert en [janvier 1973]{.date}. Il a été complètement rasé par des labours ultérieurs.

### [Espelette]{.spatial} - [Gorospil]{.coverage} 1 - [Cromlech]{.type}

Localisation

Carte : 1345 Ouest Cambo-les-Bains.

Altitude : 680 mètres.

Il est situé sur la ligne de crête reliant la borne 691 au sommet coté 702, à peu près à mi-chemin entre les deux.

Description

Quatre pierres, au ras du sol, insérées dans un léger renflement circulaire de terrain, délimitent ce monument ; on note deux autre pierres à l'intérieur.

Historique

Monument découvert en [octobre 2009]{.date} par [J. Blot]{.creator}.

### [Espelette]{.spatial} - [Gorospil]{.coverage} 2 - [Cromlech]{.type}

Localisation

Carte OT 1245 Hendaye- Saint-Jean-de-Luz.

Altitude : 692m.

Ce monument est situé à 25 mètres à l'Est Nord-Est de la borne géodésique 691.

Description

On note un léger relief circulaire sur lequel apparaissent, disposées en demi-cercle, 4 pierres, au ras du sol. La plus visible (sur laquelle est la marque peinte du GR), mesure 0,30m de long, les 3 autres mesurent entre 0,15m et 0,20m de long.

Historique

Monument découvert par le [groupe Hiharriak]{.creator} en [1999]{.date}.

### [Espelette]{.spatial} - [Gorospil]{.coverage} 3 - [Cromlech]{.type} ([?]{.douteux})

Localisation

À 11m au Nord de *Gorospil n°2 - Cromlech*.

Description

Sur un très léger relief circulaire de 2,50m à 3m environ, apparaissent 4 pierres de [grés]{.subject} ou de quartzite. La plus grande, en [grés]{.subject} gris, à l'Est, est en forme de dalle triangulaire couchée sur le sol mesure 0,80m de long et 0,70m à sa base.

Historique

Monument découvert en [1999]{.date} par le [groupe Hilharriak]{.creator}.



### [Espelette]{.spatial} - [Gorospil]{.coverage} 4 - [Cromlech]{.type} 

Localisation

À 14 mètres au Sud-Ouest de *Gorospil n°2 - Cromlech* et à 13 m à l'Est Nord-Est de la borne géodésique 691.

Description

Monument douteux ; quatre pierres sont disposées sur un cercle de 2,20m de diamètre environ. La plus grande, à l'Est, en [grés]{.subject} gris, de forme triangulaire, mesure 0,77m de long et 0,37m à sa base.

Historique

Monument découvert par [J. Blot]{.creator} en [février 2010]{.date}.

### [Espelette]{.spatial} - [Mondarrain Ouest]{.coverage} - [Tertre d'habitat]{.type} ([?]{.douteux})

Localisation

Altitude : 490m.

Au flanc Ouest du Mondarrain, à 60m au-dessous de la piste qui le contourne horizontalement.

Description

Vaste tertre oblongue, allongé selon un axe Sud-Est Nord-Ouest, pouvant atteindre 3m de hauteur et mesurant 26m dans sa plus grande dimension, avec une surface plane de 20m x 12m. Il est longé par le riu qui coule vers l'Ouest. On note sur le plat de cette butte, un bourrelet circulaire avec une dépression centrale de 6m de diamètre : reste d'habitat ? sur un très grand « tertre d'habitat » ?

### [Espelette]{.spatial} - [Ouronea]{.coverage} - [Tumulus]{.type}

Localisation

Carte 1245 Est Espelette.

Altitude : 120m.

Il est à environ 100m au Nord Nord-Ouest de la borne IGN 146, dans un champ, et à 150m au Sud de la jonction de la route Espelette - Aïnhoa avec celle venant de Souraïde.

Description

Tumulus de terre de 14m de diamètre et 1m de haut.

Historique

Tumulus découvert en [avril 1974]{.date}. Il a été depuis rasé par des labours.

### [Espelette]{.spatial} - [Pinodieta]{.coverage} - [Tertre]{.type}

Souraïde

Localisation

Carte 1245 OT Hendaye Saint-Jean-de-Luz.

Altitude : 115m.

Tertre situé dans une prairie, à environ 250m au Sud du col, et à une quarantaine de mètres au Sud-Est du chemin qui monte directement vers le mont Erebi.

Description

Erigé sur un sol en légère pente vers le Nord, il est de forme légèrement ovale, mesure une dizaine de mètres dans sa plus grande dimension et 0,80m de haut environ.

Historique

Tertre découvert par [Blot J.]{.creator} en [octobre 2009]{.date}.

### [Espelette]{.spatial} - [Soporro]{.coverage} (Gagnekoborda) - [Cromlech]{.type}

Localisation

Altitude : 431m.

Ce monument se trouve toujours sur cette longue ligne de crête qui s'étend vers le Sud-Est en partant du mont Errebi, après être passé par le Col des Trois Croix ; il est sur la piste de crête, pratiquement tangent à un poste de tir à la palombe.

Description

Monument peu visible mais qui nous paraît fort probable ; on note une très légère surélévation de terrain en forme de galette circulaire très aplatie, mesurant 3m de diamètre et 0,15m de haut, délimitée par 4 pierres périphériques de faibles dimensions mais bien visibles.

Historique

Monument découvert par [F. Meyrat]{.creator} en [septembre 2019]{.creator}.

### [Espelette]{.spatial} - [Soporro]{.coverage} - [Tertre d'habitat]{.type} ([?]{.douteux})

Localisation

Altitude : 543m.

Il est situé un peu en contre bas de la piste de crête.

Description

On note un tertre terreux, de forme ovale, mesurant 12m x 10m et 1m de haut environ, à grand axe Nord-Ouest Sud-Est. On ne doit pas omettre de signaler un évidement du terrain, à la partie Sud-Est du tertre, qui résulte de l'extraction des terres à cet endroit : la question se pose de savoir si le creusement du sol n''avait pour but que de faire un tertre, ou s'il s'agissait d'une action à visée tout à fait différente (mise au jour d'un filon rocheux etc.)

Historique

Tertre découvert par [F. Meyrat]{.creator} en [septembre 2019]{.creator}.
