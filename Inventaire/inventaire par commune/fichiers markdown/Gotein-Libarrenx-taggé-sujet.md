# Gotein-Libarrenx

### [Gotein-Libarrenx]{.spatial} - [Ahantsiga]{.coverage} - [Tertres d'habitat]{.type}

Localisation

Altitude : 570m.

Description

Nous avions succinctement décrit ces 4 tertres d'habitat dans [@blotSouleSesVestiges1979, p.26]. Mais, récemment ces tertres ont étés plus ou moins rasés pour la création de prairies artificielles. Notre ami F. Meyrat, revenu sur les lieux, a pu cependant identifier et photographier quelques reliefs qui sembleraient correspondre aux restes de ces monuments.

En 1978, on pouvait voir, quasiment au sommet de la colline de ce nom, quatre tertres : 3 étaient situés au Sud-Ouest d'une piste pastorale orientée Nord-Ouest Sud-Est :

- Le premier : le plus au Nord (n°1), mesurait 10m de diamètre, l et 0,60m de haut.

- Le second, (n°2), à 2m à l'Est du précédent, mesurait 16m de long, 10m de large et 1m de haut.

- Le troisième (n°3), à 28m à l'Est Sud-Est du précédent, mesurait 11m de diamètre et 0,70m de haut.

- Le quatrième (n°4) était situé à 14m au Nord-Est du précédent, de l'autre côté de la piste pastorale.

Enfin F. Meyrat a peut-être identifié un cinquième monument, à quelques mètres au Nord du n°4.

Historique

Tertres découverts en [1978]{.date} par [J. Blot]{.creator}.
