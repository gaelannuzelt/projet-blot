# Lasse

### [Lasse]{.spatial} - [Beharria]{.coverage} - [Pierre couchée]{.type}

Localisation

Altitude : 774m.

Elle est sur un replat au flanc Nord du Monhoa, dans une boucle de la route et sur le Gr 10.

Description

Grand bloc de [quartzite]{.subject}, allongé en pain de sucre, suivant un axe sensiblement Est-Ouest. Il mesure 3,90m de long, 1,20m de large au maximum et son épaisseur est de 0,75m en moyenne. Ne présente pas de traces d'épannelage semble-t-il. Il peut s'agir d'un bloc venu par glissement de la pente au Sud, mais la présence à peu de distance de deux tertres d'habitat (*Beharria - Tertres d'habitat*) démontre, au moins, une activité pastorale ancienne.

À 5m au Nord-Est on voit un deuxième bloc, triangulaire de 2,80m dans sa plus grande longueur, 1,50m de large et 0,65m d'épaisseur, mais qui semble sans rapport avec le précédent.

Historique

Pierre découverte par Mme [Carde G.]{.creator} en [octobre 2011]{.date}.

### [Lasse]{.spatial} - [Beharria]{.coverage} - [Tertres d'habitat]{.type}

Localisation

Altitude : 640m.

Les 2 tertres sont situés à une centaine de mètres à l'Est et en contre-bas d'abreuvoirs en bord de route au niveau d'un virage, sur un petit replat.

Description

- *TH N°1* : le plus bas situé, mais le plus net. Erigé sur un terrain en pente très légère, il mesure un peu plus de 7m de diamètre, et 0,80m de haut environ. On pourrait presque le confondre avec un tumulus.

- *TH N°2* : la présence à une quinzaine de mètres au Sud-Ouest, et toujours sur terrain en pente, d'un deuxième tertre de 8 à 9m de diamètre, 0,60m de haut avec légère dépression centrale, confirme leur qualité de tertre d'habitat.

Historique

Tertres découverts par [Blot J.]{.creator} en [novembre 2011]{.date}.

### [Lasse]{.spatial} - [Elurméaka]{.coverage} - [Tertre d'habitat]{.type}

Localisation

Altitude : 870m.

Il est situé au point culminant de modeste relief, sur la ligne de crête, à 150m environ à l'Est de *Beharria - Tertres d'habitat*.

Description

Tertre de terre apparemment circulaire, d'environ 1m de haut et 7,30m de diamètre ; en fait il existe un prolongement tumulaire vers le Nord-Est de 2m environ. L'ensemble est à grand axe Nord-Est Sud-Ouest, et est érigé sur un terrain en légère pente vers le Nord-Est.

Historique

Monument découvert par [F. Meyrat]{.creator} en [juillet 2011]{.date}.

### [Lasse]{.spatial} - [Madariakolepoa]{.coverage} - [Tertre d'habitat]{.type} ou [Tumulus cromlech]{.type} ([?]{.douteux})

Localisation

Altitude :860m.

Ce monument est situé à environ une quarantaine de mètres à l'Est de la bergerie du col, et un peu au Sud de la ligne de crête.

Description

Tertre de terre et de pierraille, très légèrement ovale, d'environ 6,50m de long, 6m de large, et 1m de haut, à grand axe Nord-Sud, érigé sur un terrain en pente douce vers l'Ouest. Il existe dans le secteur Nord-Ouest comme une couronne de pierres de volume moyen (certaines de 0,45m x 0,50m) ; vestige d'un péristalithe ?

Historique

Monument découvert par [F. Meyrat]{.creator} en [juillet 2011]{.date}.

### [Lasse]{.spatial} - [Mataria]{.coverage} 2 - [Cromlech]{.type} ([?]{.douteux})

Localisation

Altitude : 790m.

Il se trouve sur un petit replat (terrain en très légère pente vers l'Ouest) un peu avant d'arriver en haut de la montée sur Mataria, en venant du col situé au Sud-Ouest.

Description

Cercle de 3,80m de diamètre délimité par une quinzaine de petites pierres au ras du sol particulièrement visibles dans les deux tiers Ouest du monument.

Historique

Cercle découvert par [J. Blot]{.creator} en [août 2011]{.date}. Rappelons le *C n°1* [@blotNouveauxVestigesMegalithiques1972b, p.210]

### [Lasse]{.spatial} - [Mataria]{.coverage} 3 - [Cromlech]{.type} ([?]{.douteux})

Localisation

Altitude : 780m.

Il se trouve à une quarantaine de mètres à l'Est Sud-Est du très beau tumulus décrit antérieurement [@blotNouveauxVestigesMegalithiques1972b p.210]

Altitude : 780m

Description

Un ensemble de pierres de volumes assez importants (deux ou trois gros pavés), délimite un cercle de 4m de diamètre ; ces pierres sont réparties sur une couronne d'environ 1,70m de large, laissant libre en son centre un espace circulaire de 2,30m de diamètre.

Bon nombre de ces pierres présentent une forte mobilité, ce qui laisse planer un doute sur ce monument, bien que le propriétaire des lieux l'ait toujours connu dans cet état.

Historique

Monument découvert par [Blot. J.]{.creator} en [août 2011]{.date}.

### [Lasse]{.spatial} - [Mataria]{.coverage} - [Dolmen]{.type}

Localisation

Altitude : 787m.

Il est à l'extrémité Nord-Est du plateau sommital.

Description

Un ensemble de dalles plantées sont visible au ras du sol, délimitant une chambre funéraire de 1,20m de long environ, et 0,88m de large, orientée Nord-Ouest Sud-Est. Six petites dalles forment la paroi Sud-Ouest ; deux autres, dont une de 0,60m de long, forment la paroi Nord-Est ; la paroi Nord-Ouest est formée de 3 fragments de dalle. Il semble qu'il existe le vestige d'un tumulus de très faible relief, actuellement de 4m de diamètre.

Historique

Monument découvert par [Blot. J.]{.creator} en [août 2011]{.date}.
