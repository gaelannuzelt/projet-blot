# Ascain

### [Ascain]{.spatial} - [Androla]{.coverage} - [Monolithe]{.type}

Localisation

Altitude : 297m.

Cette longue pierre se trouve couchée au sol en bordure droite de la piste quand on la gravit vers le Sud, un peu avant d'arriver au site des dolmens d'Androla.

Description

Long parallélépipède de [grès]{.subject} local, mesurant 4,40m de long selon son grand axe Nord-Sud. Son sommet Sud est plus étroit que sa base Nord - (0,46m de large) - et sa largeur au milieu est de 0,82m.

Son épaisseur va croissant du Sud au Nord : 0,38m au Sud, 0,57m au milieu, 0,67m à la base.

Les signes de taille ou d'épannelage sont discrets (présents surtout au sommet).

Historique

Cette pierre avait été remarquée par [Blot J.]{.creator} depuis les années 90, en particulier lorsqu'elle avait été érigée, à cette époque, en bordure du chemin, tel un authentique menhir - qu'elle n'était pas en ce lieu - Toutefois, il n'est pas exclu que, située ailleurs dans un passé plus lointain (mais dans un lieu très proche), elle n'ait eu en effet un rôle de *Muga*, compte tenu de sa forme et de ses dimensions.

### [Ascain]{.spatial} - [Androla]{.coverage} Sud - [Dolmen]{.type}

Localisation

Carte IGN 1245 OT Hendaye Saint-Jean-de-Luz.

Altitude : 331m.

Ce monument est situé à quelques dizaines de mètres au Sud du *dolmen Androla* décrit par J.M. de Barandiaran [@blotNouveauxVestigesMegalithiques1971 p.34]. Il est aussi à une vingtaine de mètres à l'Ouest du petit refuge au toit de loses et murs en blocs de [grès]{.subject}, construit en bordure du chemin, à droite en montant.

Description

On note un tumulus constitué de nombreux blocs de [grès]{.subject} de la taille d'un pavé, dont on remarque par endroit l'agencement soigneux. Il mesure environ 8m de diamètre, 0,40m de haut, mais les dimensions, en sont assez difficile à apprécier du fait d'une abondante végétation de touyas. Au centre se devine une légère dépression d'environ 1,20m de diamètre et quelques centimètres de profondeur, qui pourrait correspondre au reste d'une [chambre funéraire]{.subject}, dont on aurait, comme très souvent, arraché les dalles. L'aspect et les dimensions de ce tumulus nous font plus penser à un tumulus dolménique qu'à un tumulus simple.

Historique

Monument découvert en [1989]{.date} par [I. Gaztelu]{.creator}.

### [Ascain]{.spatial} - [Arguibele]{.coverage} - [Monolithe]{.type}

Localisation

Altitude : 174m.

Il est sur le petit plateau où se trouvent les 5 autres dolmens de ce nom.

Il est distant de 13m à l'Est de *D2*, de 20m au Sud-Ouest de *D1*, de 16m au Sud-Est de *D3*, et à 25m au Sud Sud-Est de *D5*.

Description

Monolithe de [grès]{.subject} rose [triasique]{.subject} couché au sol, en forme de pain de sucre, la pointe orientée vers le Nord. Il mesure 2,50m de long, 1,10m dans sa partie la plus large, 0,80m au rétrécissement avant sa pointe, son épaisseur varie entre 0,50m et 0,24m suivant les endroits.

Ce monolithe, ne semble pas présenter de traces d'épannelage, sauf peut-être à sa pointe Nord. Par contre sa présence en ce lieu ne paraît pas *naturelle*... et nous tenons compte de son contexte archéologique pour le décrire ici.

Historique

Monolithe signalée par le [groupe Hilharriak]{.creator} en [mars 2013]{.date}.

### [Ascain]{.spatial} - [Arguibele]{.coverage} 3 - [Dolmen]{.type}

Localisation

Carte 1245 Est Espelette.

Altitude : 150m.

Il est situé à 12m à l'Ouest Nord-Ouest du *dolmen Arguibele 1*, à 5m à l'Ouest du *n°2* déjà décrits [@blotNouveauxVestigesMegalithiques1971 p. 30], et à 4m à l'Ouest de la piste pastorale, elle-même orientée Nord-Sud.

Description

Tumulus pierreux formé de nombreux fragments de dalles de [grès]{.subject}, mesurant 7m de diamètre et 1,50m de haut.

La [chambre funéraire]{.subject} est signalée par une dépression centrale au bord Sud de laquelle on note une grande dalle couchée mesurant 1,60m de long et 1m de large ; s'agit-il du couvercle ?

Peut-être y aurait-il une ébauche de péristalithe.

Historique

Dolmen découvert en [janvier 1973]{.date}.

### [Ascain]{.spatial} - [Arguibele]{.coverage} 4 - [Dolmen]{.type}

Localisation

À 2m au Nord de *Arguibele 3 - Dolmen*.

Description

On note un modeste tumulus mixte, à prédominance pierreuse, d'un diamètre de 6m, et de 0,30m de haut. La [chambre funéraire]{.subject}, centrale, est orientée est Sud-Ouest Nord-Est, et mesure 1,80m de long, 0,55m de large, et 0,40m de haut ; elle est délimitée par trois dalles de [grès]{.subject} rose, bien visibles : 2 au Nord Nord-Ouest, l'une de 0,60m de long et 0,35m de haut, l'autre de 0,90m de long et 0,40m de haut, séparées l'une de l'autre par 0,20m, et une autre dalle au Sud-Est de 0,90m de long et 0,30m de haut.

Historique

Dolmen découvert en [janvier 1973]{.date}.

### [Ascain]{.spatial} - [Arguibele]{.coverage} 5 - [Dolmen]{.type}

Localisation

À 2m au Sud-Ouest de *Arguibele 4 - Dolmen*.

Description

Tumulus pierreux bien visible de 10m de diamètre, et 0,70m de haut; onze pierres périphériques pourraient faire partie d'un possible péristalithe. La [chambre funéraire]{.subject} est bien visible sous la forme d'une dépression centrale dans laquelle apparaissent seulement 2 petites dalles ; mais il est très difficile d'en apprécier l'orientation et les dimensions.

Historique

Dolmen découvert en [janvier 1973]{.date}.

### [Ascain]{.spatial} - [Arraioa]{.coverage} - [Pierre couchée]{.type} ([?]{.douteux})

Localisation

Cette pierre gît en bordure Nord de la piste (une très ancienne voie reliant Ascain au col de Saint-Ignace), à environ six mètres du ruisseau Arrayoko Erreka. C'est la seule pierre importante visible en ces lieux.

Altitude : 60m.

Description

Ce bloc de [grès]{.subject} local affecte une forme en pain de sucre, à base Est Sud-Est, et à sommet Ouest Nord-Ouest. Il mesure 2,30m de long, 0,85m à sa base et 0,50m en son milieu ; son épaisseur moyenne est de 0,30m. Il pourrait y avoir (sous toutes réserves) des traces d'épannelage au niveau du sommet. On note, plantée dans le sol, à deux centimètres de sa base - et parallèle à celle-ci - une dalle de [grès]{.subject} de 0,50m de long et 0,20m d'épaisseur (dalle de calage ?)

Historique

Pierre découverte par [Blot J.]{.creator} en [novembre 2010]{.date}.

### [Ascain]{.spatial} - [Arranoxola]{.coverage} - [Pierre couchée]{.type} ([?]{.douteux})

Localisation

Altitude : 580m.

Elle est située un peu au-dessus et au Sud du col (à 560m d'altitude) qui reçoit la piste venant d'Arranoxola, et à l'Est du chemin dit de l'Impératrice.

Description

Dalle de [grès]{.subject} épaisse de 0,42m couchée au sol, de forme triangulaire, mesurant 2,57m de long et 2,35m de large. On note d'évidentes traces d'épannelage sur son bord Nord, mais il reste impossible de préciser la nature de cette dalle...

Historique

Dalle découverte en [janvier 2013]{.date} par [P. Badiola]{.creator}.

### [Ascain]{.spatial} - [Biskartzun]{.coverage} 1 - [Tumulus]{.type}

Localisation

Altitude : 148m.

Tumulus situé immédiatement à droite de la même piste qui monte de Martinhauren borda, et de *Martinhaurren Borda - Tumulus*, et se rend au sommet de Biskartzun. Ce tumulus n°1 est tangent au Sud-Est de la piste et à 3m au Nord-Ouest de l'angle du mur de clôture (pierres et barbelés) du pâturage voisin.

Description

Petit tumulus de 3m de diamètre et 0,15m de haut avec une petite dépression centrale ; pas de pierres visibles.

Historique

Tumulus découvert par [Meyrat F.]{.creator} en [février 2012]{.date}.

### [Ascain]{.spatial} - [Biskartzun]{.coverage} 2 - [Tumulus]{.type}

Localisation

À une vingtaine de mètres au Nord Est de *Biskartzun 1 - Tumulus* sur un petit replat bien marqué sur la même piste et à droite de celle-ci. Il est dissimulé sous une végétation d'ajoncs abondante.

Description

Tumulus de 3m de diamètre et 0,30m de haut environ, marqué par une dépression centrale ; sept à huit pierres sont visibles sur ce tumulus.

Historique

Tumulus découvert par [Meyrat F.]{.creator} en [février 2012]{.date}.

### [Ascain]{.spatial} - [Gorrostiarria]{.coverage} - [Tumulus-cromlech]{.type}

Localisation

Altitude : 585m.

Il est situé plus en altitude que *Gorrostiarria - Cromlechs*, et à 1m au Nord d'un important filon de [grès]{.subject} [triasique]{.subject} de la bordure Sud de la crête d'Altsaan.

Description

Tumulus de forme plus ovale que circulaire, mesurant 3m dans son grand axe Est-Ouest, et 2,20m selon l'axe Nord-Sud ; hauteur : 0,40m environ. De nombreuses dalles entourent sa périphérie et en partie son sommet. Tumulus-cromlech ou tombe datant des combats de l'ère napoléonienne ?

Historique

Monument découvert par [Blot J.]{.creator} en [mars 2012]{.date}.

### [Ascain]{.spatial} - [Gorrostiarria]{.coverage} - [Tumulus]{.type}

Localisation

Il est à 1,70m au Nord-Est de *Gorostiarria - Tumulus-cromlech*.

Description

Petit tumulus de terre de 3m de diamètre et 0,30m de haut, à la surface duquel quelques pierres apparaissent.

Historique

Monument découvert par [Blot J.]{.creator} en [mars 2012]{.date}.

### [Ascain]{.spatial} - [Gorrostiarria]{.coverage}-[Cromlechs]{.type}

La nécropole des cromlechs de Gorostiarria a été initialement décrite par [J.M. de Barandiaran]{.creator}, avec un plan, dans [@barandiaranContribucionEstudioCromlechs1949 p.206]. Il cite 8 cercles, mais n'en dessine que 7 sur son plan.

Nous avons donc voulu reprendre le plan, mais, les années ayant passées, la végétation et les dégradations ont fait que nous (Blot J. et Meyrat F.) n'avons pas retrouvé le même nombre de pierres que le Père J.M. de Barandiaran - Par ailleurs il nous a bien semblé pouvoir identifier les cercles 9, 10, 11, 12, ci-après brièvement décrits - (les n°9, 10, 11, ont été signalés par [L. Millan]{.creator} en [1989]{.date}, mais non localisables sur le terrain -- Est-ce que ce sont les mêmes que les nôtres ?)

Nous avons aussi repris les autres monuments tout aussi brièvement, afin de donner un aperçu de l'état actuel de cette nécropole, telle qu'elle nous est apparue en ce mois de [mars 2012]{.date}.

- *Cromlech 1* : 6m de diamètre ; 16 pierres

- *Cromlech 2* : 5,60m de diamètre ; 12 pierres

- *Cromlech 3* : 5,50m de diamètre ; 9 pierres

- *Cromlech 4* : 3,30m de diamètre ; 4 pierres

- *Cromlech 5* : 4,20m de diamètre ; 8 pierres

- *Cromlech 6* : 5,40m de diamètre ; 14 pierres

- *Cromlech 7* : 6m de diamètre ; 14 pierres

- *Cromlech 8* : 7,30m de diamètre ; 18 pierres

- *Cromlech 9* : 7,80m de diamètre ; 12 pierres

- *Cromlech 10* : 6,70m de diamètre ; 13 pierres

- *Cromlech 11* : 3,20m de diamètre ; 5 pierres

- *Cromlech 12* : 3m de diamètre ; 9 pierres

### [Ascain]{.spatial} - [Ihicelhaya]{.coverage} - [Monolithe]{.type}

Localisation

Carte IGN 1245 OT Hendaye Saint-Jean-de-Luz.

Altitude : 500 mètres.

Ce monolithe couché se trouve au beau milieu du plateau, à une vingtaine de mètres à l'Ouest de la piste balisée qui monte vers le dolmen Ihcelhaya (lui-même situé dans l'angle formé par la plantation de conifères). Il n'y a pas d'autres blocs rocheux dans les environs immédiats.

Description

Il s'agit d'un monolithe bloc en [grès]{.subject} local, allongé sur le sol en direction Nord-Sud, en pain de sucre à pointe vers le Sud. Il mesure 2,76 m de long, 1,10 m de large dans sa partie moyenne, et 0,95 m à sa base. Son bord Ouest est sensiblement rectiligne et paraît naturel, mais les traces d'épannelage sont nombreuses au sommet et au bord Est. L'épaisseur apparente (le monument est en partie enfoncé dans le sol.) atteint 0,30m.

Historique

Monument découvert en [1977]{.date} par [A. Martinez Manteca]{.creator}.

### [Ascain]{.spatial} - [Ihicelhaya]{.coverage} Est - [Tertres d'habitat]{.type}

Localisation

Altitude : 465m.

Les 2 tertres d'habitat sont à une dizaine de mètres à l'Est d'un petit ruisseau.

Description

Le premier est un petit tertre de terre, bien visible, asymétrique, érigé sur terrain en pente, de 7m de long, 5m de large et 0,40m de haut en moyenne. À une trentaine de mètres au Sud Sud-Ouest, se voit un deuxième tertre, plus important que le premier, érigé sur le bord même du ruisseau ; lui aussi asymétrique, il mesure une vingtaine de mètres de long, une quinzaine de large et 2m à 3m de haut suivant le niveau considéré.

Historique

Ces deux tertres ont été découverts par [Blot J.]{.creator} en [décembre 2011]{.date}.

### [Ascain]{.spatial} - [Jauréguikoborda]{.coverage} - [Dalle plantée]{.type}

Localisation

Altitude : 274m.

Elle est située à 100m au Sud-Est de la cote 277 et à environ 500m à l'Est de la ferme *Jauréguikoborda*. Elle est plantée après le petit col au flanc Sud de la colline, et à deux mètres environ à l'Est de la piste qui en vient.

Description

Dalle plantée, haute de 0,98m, grossièrement rectangulaire, avec un sommet un peu plus étroit (0,22m) que sa base (0,32m) ; son épaisseur est en moyenne de 0,15m.

Sa partie supérieure paraît avoir été volontairement régularisée. Ses deux faces Ouest et Est son lisses, cette dernière s'inclinant légèrement vers le sol.

Historique

Dalle découverte par [A. Martinez]{.creator} en [1999]{.date}.

### [Ascain]{.spatial} - [Ihicelhaya]{.coverage} Nord - [Monolithe]{.type}

Localisation

Altitude : 385m.

Il est sur la pente qui domine, au Sud-Ouest, la piste qui monte du ruisseau des Trois Fontaines au plateau d'Ihicelaya.

Description

Beau bloc de [grès]{.subject} [triasique]{.subject} parallélépipédique, gisant selon un axe Nord Nord-Est Sud Sud-Ouest sur un terrain en forte pente vers le Nord-Ouest. Il mesure 2,71m de long, 0,72m de large à sa base, au Sud-Ouest et 0,26m à sa pointe au Nord-Est ; son épaisseur est de 0,25m en moyenne. Il semble présenter quelques traces d'épannelage à son sommet. Il est notable qu'il n'y a aucun autre bloc de pierre ou de dalle dans les environs. Enfin, tangent au Nord-Ouest on note ce qui pourrait être un petit tertre d'habitat, asymétrique, de 5m de large environ.

Historique

Monument découvert par [Blot J.]{.creator} en [décembre 2011]{.date}.

### [Ascain]{.spatial} - [La Plana]{.coverage} - [Monolithe]{.type}

Localisation

Altitude : 160m.

On voit ce monolithe au départ du premier des deux sentiers qui montent à Larrun en partant du replat (La Plana) au-dessus du parking des carrières d'Ascain.

Description

Très beau bloc de [grès]{.subject} parallélépipédique, gisant au sol selon un axe Est-Ouest. Il mesure 4,10m de long, 0,94m dans sa partie la plus large et 0,60m d'épaisseur en moyenne.

Son extrémité Ouest semble avoir été taillée en pointe, mis à part le fait que cet épanellage ne touche pas la partie inférieure du monolithe ; on retrouve d'autres traces de taille tout le long du bord Nord et à sa base Est.

Historique

Ce monolithe nous a été signalé par [Badiola P.]{.creator} en [octobre 2011]{.date}.

### [Ascain]{.spatial} - [Larrun Erreka]{.coverage} - [Dalle plantée]{.type}

Localisation

Altitude : 515m.

Elle se trouve en contre bas de la piste qui, du plateau des Trois Fontaines, descend vers l'Ouest et la route d'Olhette.

Description

Dalle trapézoïdale plantée selon un axe Nord Nord-Est Sud Sud-Ouest, mesurant 1,40m de haut, 1,30m à sa base, et 0,20m d'épaisseur.

Historique

Dalle découverte par [Badiola P.]{.creator} en [mai 2012]{.date}.

### [Ascain]{.spatial} - [Mantobaita]{.coverage} - [Tumulus]{.type} ([?]{.douteux})

Localisation

Altitude : 95m.

Il est situé à une dizaine de mètres avant le petit ponceau qui franchit le ruisseau Larrunko erreka, sur la rive droite de ce dernier ; il est longé à l'Est par le sentier qui rejoint ce ponceau au parking.

Description

Tumulus pierreux, légèrement ovale, mesurant environ 9,50m x 5,50m et 0,60m de haut. Il est constitué d'un amoncellement de pierres roulées, plus ou moins de la taille d'un pavé. Au centre s'élève un bel arbre. Monument très douteux.

Historique

Tumulus découvert en [avril 2014]{.date} par [C. Blot]{.creator}.

### [Ascain]{.spatial} - [Mantobaita]{.coverage} - [Dalle plantée]{.type} ([?]{.douteux})

Localisation

Nous voudrions signaler la magnifique dalle plantée située en bordure de la propriété existant à droite de la route qui aboutit au parking.

Altitude : 95m.

Description

Cette dalle, de forme rectangulaire, allongée, mesure 1,90m de haut, 0,76m de large et 0,23m d'épaisseur ; tout son pourtour présente de nombreuses et très anciennes traces d'épannelage. Tout en étant incluse dans l'ancienne clôture en dalles, dont il reste quelques exemples, mais de forme plus carrée, on peut émettre l'hypothèse d'une réutilisation locale d'un monolithe trouvé ailleurs et ayant eu une finalité première bien différente... Sa base a été consolidée avec du ciment.

### [Ascain]{.spatial} - [Martinhaurren Borda]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 95m.

Ce tumulus est situé au flanc Sud-Est du mont Bizkarzun, à l'Est d'Ascain. Il est à 50m au Nord-Ouest de la borde « Martinhaurren borda » au milieu de la piste qui se rend de celle-ci au sommet de la colline, et à 4m à l'Ouest d'une clôture de barbelés qui entoure une vaste prairie artificielle dépendant de cette borde.

Description

Tumulus de 3,20m de diamètre environ, un peu plus étendu vers le Sud, du fait de sa construction sur un terrain en pente douce vers le Sud. Il est formé de petits blocs de [grès]{.subject} rose, de la taille du poing, amoncelés sur une faible hauteur de 0,30m environ.

Historique

Tumulus signalé par [Andraud A.]{.creator} en [février 2014]{.date}.

### [Ascain]{.spatial} - [Neguxola]{.coverage} - [Monolithe]{.type}

Localisation

Altitude : 487m.

Elle gît sur un sol en légère pente vers l'Est, à 200m environ de l'enclos de Neguxola (plateau Ihizelhaya).

Description

Dalle de [grès]{.subject} [triasique]{.subject} de forme parallélépipédique, allongée selon un axe Est-Ouest, mesurant 3m de long, à base plus large (1,88m) que le sommet (1,16m) ; la largeur dans la partie médiane est d'environ 1,55m. L'épaisseur de cette dalle varie entre 0,22m et 0,35m. Il semble qu'il y ait des traces d'épannelage sur les côtés, au Nord Nord-Est et au Sud Sud-Est.

Historique

Dalle découverte par [P. Badiola]{.creator} en [septembre 2014]{.date}.

### [Ascain]{.spatial} - [Olhette]{.coverage} 1 - [Tumulus-cromlech]{.type}

Localisation

Carte 1245 Ouest.

Altitude : 40m.

Il est à environ 500m à l'Est du hameau d'Olhette, à droite de la route qui se dirige vers Ascain, et à 25m au Sud de cette route, dans la propriété privée du Docteur Robert. Nous pensons utile de re-décrire ce monument, retrouvé par nous en 1970, mais déjà publié [@veyrinCromlechsPierresLevees1935 p. 219].

Description

[Pierre Dop]{.creator} a le premier décrit ce monument comme un tumulus mixte à prédominance pierreuse de 8m de diamètre et 0,60m de haut entouré d'un très beau péristalithe formé de dalles (en nombre non précisé) dont les dimensions pouvaient atteindre 0,80m à 1m de long et 0,20m à 0,30m de haut. Quand nous avons retrouvé ce monument en 1970, ces dalles avaient été arrachées depuis longtemps et le monument très détérioré.

Historique

Monument découvert par [Duhart J.]{.creator} en [1934]{.date}.

### [Ascain]{.spatial} - [Olhette]{.coverage} 2 - [Tumulus]{.type}

Localisation

À environ 100m au Nord de *Olhette 1 - Tumulus-cromlech*, mais de l'autre côté de la route et à 15m de celle-ci.

Description

Tumulus mixte de pierres et de terre de 18m de diamètre et 0,70m de haut. Il est en partie dissimulé par des accacias.

Historique

Monument découvert en [mars 1972]{.date}. Il a été depuis rasé par l'aménagement d'un lotissement et la construction d'une maison.

### [Ascain]{.spatial} - [Olhette]{.coverage} 3 - [Tumulus]{.type}

Localisation

Il est situé à 25m au Nord Nord-Est de *Olhette 2 - Tumulus*.

Description

Tumulus mixte de pierres et de terre, de 18m de diamètre et 0,50m de haut.

Historique

Monument découvert en [mars 1972]{.date}. Il a depuis été lui aussi rasé par la création d'un lotissement.

### [Ascain]{.spatial} - [Olhette]{.coverage} 4 - [Tumulus]{.type}

Localisation

Il est situé à 100m à l'Ouest de *Olhette 1 - Tumulus-cromlech*, et, comme lui, au Sud de la route Olhette - Ascain. Il est dans une vaste prairie de la propriété *Sainte-Hélène* séparée de celle du Dr Robert par un petit chemin bordé d'une haie. On note 4 tumulus : le premier décrit se trouve le plus à l'Est.

Description

Tumulus mixte de terre et de pierres (en général des galets) de 11m de diamètre 1m de haut.

Historique

Monument découvert en [mars 1972]{.date}.

### [Ascain]{.spatial} - [Olhette]{.coverage} 5 - [Tumulus]{.type}

Localisation

Il est à 40m au Nord Nord-Ouest de *Olhette 4 - Tumulus*.

Description

Tumulus terreux, en partie amputé de son quart sud, d'un diamètre de 11m et 0,30m de haut.

Historique

Monument découvert en [mars 1972]{.date}.

### [Ascain]{.spatial} - [Olhette]{.coverage} 6 - [Tumulus]{.type}

Localisation

Il est à environ 4m à l'Ouest Sud-Ouest de *Olhette 4 - Tumulus*.

Description

Tumulus mixte de terre et de galets de 16m de diamètre et 0,80m de haut.

Historique

Monument découvert en [mars 1972]{.date}.

### [Ascain]{.spatial} - [Olhette]{.coverage} 7 - [Tumulus]{.type}

Localisation

Il est à 5m au Nord-Est de *Olhette 6 - Tumulus*.

Description

Tumulus terreux de 13m de diamètre et de 0,30m de haut. Quelques pierres se voient à sa périphérie, sans que l'on puisse parler de péristalithe.

Historique

Monument découvert en [mars 1972]{.date}.

### [Ascain]{.spatial} - [Peruen Borda]{.coverage} - [Dolmen]{.type}

Localisation

Carte IGN 1245 OT Hendaye Saint-Jean-de-Luz.

Altitude : 137m.

On peut voir ce qui reste de ce dolmen à une cinquantaine de mètres à l'Ouest Nord-Ouest de la maison *Peruen Borda*, dans une prairie en situation dominante sur toute la plaine côtière, et à environ 150m à l'Ouest du *dolmen Xeruen* que nous avons décrit en 1971 [@blotNouveauxVestigesMegalithiques1971 p.38].

Description

Il ne reste qu'un tumulus pierreux, circulaire, d'environ 9m de diamètre et 0,50m de haut. Une dépression centrale de 2,50m de diamètre et quelques centimètres de profondeur, signale ce qui reste de la [chambre funéraire]{.subject}. Il est fort probable que les dalles constitutives de cette dernière aient été récupérées par les anciens habitants de ces lieux qui étaient carriers.

Historique

Ce monument a été signalé à [J. Blot]{.creator} en [décembre 2009]{.date} par l'occupant actuel de *Peruen Borda*, Mr. [J.F. Servier]{.creator}.

### [Ascain]{.spatial} - [Peruen Borda]{.coverage} - [Monolithe]{.type}, [dalle couchée]{.type} ([?]{.douteux})

Localisation

À une cinquantaine de mètres à l'Est du dolmen de ce nom, et à une dizaine de mètres au Nord de la maison *Peruen Borda*.

Description

Dalle de [grès]{.subject} [triasique]{.subject}, en forme de pain de sucre à grand axe Est-Ouest, et à sommet Ouest. Elle mesure 2,27m de long, 0,88m de large et 0,25m d'épaisseur en moyenne. Tout son bord Nord présente des traces d'épannelage.

Historique

Dalle découverte par [Blot J.]{.creator} en [novembre 2011]{.date}.

### [Ascain]{.spatial} - [Trois Fontaines]{.coverage} - [Dalle plantée]{.type} ([?]{.douteux})

Localisation

Altitude : 594m.

Elle est située sur le replat, couvert de résineux, qui domine le sentier menant aux Trois Fontaines. Elle est au-dessus du petit *dolmen des Trois Fontaines* que nous avons décrit [@blotNouveauxVestigesMegalithiques1971 p. 36]

Altitude 498m.

Description

Dalle de [grès]{.subject} plantée verticalement dans le sol, de forme triangulaire mesurant 0,98m de haut, 1,35m à sa base et 0,13m d'épaisseur. Il n'y a pas de tumulus, mais à l'évidence cette dalle a été volontairement plantée là ; borne ancienne, antique, protohistorique ? - ne correspond à aucune limite communale actuelle.

Historique

Dalle découverte par [I. Gaztelu]{.creator}, en [mars 1979]{.date}. Il existe une dalle plantée, de moindre importance, à une quinzaine de mètres au Sud-Ouest.

### [Ascain]{.spatial} - [Trois Fontaines]{.coverage} - Grande [dalle couchée]{.type} 1 ([?]{.douteux})

Localisation

Ressemblant à un véritable « monolithe », elle est située à 80m au Sud Sud-Ouest de *Trois Fontaines - Dalle plantée (?)*. Ses dimensions sont 3,35m de long, 1,78m de large, 0,31m d'épaisseur. Cette grande dalle, bien individualisée, ne présente cependant pas de traces d'épannelage, et surtout possède dans son environnement d'autres grandes pierres ou dalles, en sorte qu'il est très difficile de pouvoir la retenir comme « monolithe » vrai...Nous la citons pour montrer combien les problèmes d'identification, d'authenticité de ce type de monuments sont difficiles. Nous en donnerons deux autres exemples dans la commune de Sare.

Historique

Dalle découverte par [J. Blot]{.creator} en [2010]{.date}.

### [Ascain]{.spatial} - [Trois Fontaines]{.coverage} (Plateau des) - Grande [dalle couchée]{.type} 2 ([?]{.douteux})

Localisation

Altitude : 540m.

Ce grand bloc de [grès]{.subject} gris, en forme de pain de sucre, est couché au sol à une dizaine de mètres au Nord du GR 10.

Description

Il mesure 2,70m de long, 1,40m dans sa plus grande largeur, et 0,18m d'épaisseur. Cette pierre, qui pourrait très bien être un «monolithe» vrai, ne présente aucune trace d'épannelage, mais, surtout, l'environnement de ce plateau abonde en grandes pierres de tous ordres, de sorte qu'il est très difficile de se prononcer sur une éventuelle finalité...

Historique

Dalle découverte par [J. Blot]{.creator} en [mars 2010]{.date}.

### [Ascain]{.spatial} - [Trois Fontaines]{.coverage} 2 - [Tumulus]{.type} ou [Tertre d'habitat]{.type} ([?]{.douteux})

Localisation

Altitude : 540m.

Situé à 32m au Nord-Ouest mètres de *Trois Fontaines 1 - Dolmen* (commune de Sare)

Description

Comme *Trois Fontaines 1 - Dolmen*, ce tumulus de terre et de pierres, de faible relief (0,10m de haut) mesure 8m de diamètre avec une dépression centrale de 3m de largeur et 0,40m de profondeur. Pourrait être un Tertre d'Habitat ?

On note à 4m au Nord-Est de ce tumulus une dalle plantée (*Trois Fontaines 3 - Dalle plantée*).

Historique

Tumulus découvert par [Meyrat F.]{.creator} en [avril 2012]{.date}.

### [Ascain]{.spatial} - [Trois Fontaines]{.coverage} 3 - [Dalle plantée]{.type}

Localisation

Située à 4m au Nord-Est de *Trois Fontaines 2 - Tumulus ou Tertre d'habitat (?)*.

Description

Dalle parallélépipédique de [grès]{.subject} local, verticalement plantée dans le sol, mesurant 0,90m de haut, 0,25m d'épaisseur, 0,44m à la base, et 0,40m dans sa partie haute.

Historique

Dalle découverte par [Meyrat F.]{.creator} en [avril 2012]{.date}.

### [Ascain]{.spatial} - [Trois Fontaines]{.coverage} 3 - [Tumulus]{.type} ou [Tertre d'habitat]{.type} ?

Localisation

Altitude : 540m.

Il est situé à 30m au Nord de *Trois Fontaines 2 - Tumulus ou Tertre d'habitat (?)*.

Description

Tumulus de terre et de pierres de faible hauteur (0,05m) mesure 8m de diamètre avec une dépression centrale de 3m de large et 0,40m de profondeur. Pourrait être un Tertre d'habitat ?

Historique

Tumulus découvert par [Meyrat F.]{.creator} en [avril 2012]{.date}.

### [Ascain]{.spatial} - [Trois Fontaines Erreka]{.coverage} - [Cromlech]{.type}

Localisation

Altitude : 540m.

Situé sur un beau replat planté de mélèzes et à une trentaine de mètres au Nord du sentier qui se rend au col des Trois fontaines, et à 4m au Sud du sentier qui vient de Trois Fontaines Erreka, plus bas.

Description

Une quinzaine de pierres, régulièrement espacées, délimitent un cercle de 12m de diamètre ; elles dépassent de peu le sol, mais sont bien visibles : les pierres de la moitié Est sont les plus volumineuses, certaines dépassant le mètre en longueur, et l'atteignant presque en largeur. On note une borne plantée dans le quart Sud-Est, inclinée vers le Sud (*Trois Fontaines Erreka - Pierre plantée*).

Historique

Cromlech découvert par [Meyrat F.]{.creator} en [avril 2012]{.date}.

### [Ascain]{.spatial} - [Trois Fontaines Erreka]{.coverage} - [Dalle couchée]{.type} 1

Localisation

Altitude : 445m.

Elle est située sur un replat de la crête qui sépare le ruisseau des Trois Fontaines de son affluent de gauche.

Description

Dalle de [grès]{.subject} local, grossièrement parallélépipédique, allongée selon un axe Nord-Est Sud-Ouest, mesurant 1,93m de long, 1,38m dans sa plus grande largeur, 1,06m à sa base et 0,33m d'épaisseur. Cette dalle présente des signes d'épannelage sur tout son pourtour Nord ainsi qu'à sa base Sud-Ouest.

Historique

Dalle découverte par [Meyrat F.]{.creator} en [avril 2011]{.date}.

### [Ascain]{.spatial} - [Trois Fontaines Erreka]{.coverage} - [Dalle couchée]{.type} 2

Localisation

Altitude : 435m.

Elle est à 400m à l'Est de *Trois Fontaines Erreka - Dalle couchée 1*. À 400m à l'Ouest coule le ruisseau Trois Fontaines Erreka.

Description

Elle repose au sommet d'un filon rocheux, dont elle est indépendante. Cette dalle de [grès]{.subject} local de forme triangulaire, mesure 2,80m de long, et 1,25m à sa base. Son épaisseur varie entre 0,30 et 0,40m. Il semble que sa base et une partie de son bord Nord-Est soient épannelés. Dalle de carriers « modernes » ?

Historique

Dalle découverte par [Meyrat F.]{.creator} en [avril 2012]{.date}.

### [Ascain]{.spatial} - [Trois Fontaines Erreka]{.coverage} - [Dolmen]{.type}

Localisation

Altitude : 540m.

Au pied de la crête d'Altxangue, à l'extrémité Ouest de celle-ci, et en bordure de la piste qui la contourne par le Nord.

Description

La description qui suit a été publiée en 2013 dans le Tome 5 de notre *Inventaire* sous la rubrique « Tumulus » et non « dolmen » comme actuellement. En effet d'après Claude Chauchat (communications personnelles), ce monument avait déjà été identifié comme dolmen (avant sa re-découverte en 2012 par F. Meyrat), par [G. Laplace]{.creator} et [J.M. de Barandiaran]{.creator}, il y a des années, alors que le monument était bien plus visible que maintenant (ayant, depuis, été encore plus détérioré par le passage d'engins.).

Tumulus pierreux, mesurant 7m de diamètre et 0,40m de haut, ayant été dégradé sur son flanc Est par le passage de la piste qui ampute son bord Sud. Au centre, 2 petites dalles verticales (0,80m de long et 0,23m de haut pour l'une et 0,52m de long et 0,20m de haut pour l'autre), pourraient représenter les vestiges de la [chambre funéraire]{.subject} ; une belle dalle de [grès]{.subject} au Nord de celle-ci, mesure 2,40m de long, 1,26m de large et 0,20m de haut et pourrait en avoir été le couvercle ; une autre dalle (2m x 1m), se voit au Sud, simplement posée sur le sol.

Il semblerait enfin que 7 pierres délimitent une partie de la périphérie Ouest du tumulus.

Historique

Monument re-découvert par [Meyrat F.]{.creator} en [avril 2012]{.date}.

### [Ascain]{.spatial} - [Trois Fontaines Erreka]{.coverage} - [Pierre plantée]{.type}

Localisation

Elle est plantée dans le quart Sud-Est de *Trois Fontaines Erreka - Cromlech*.

Altitude : 540m.

Description

Borne de forme grossièrement parallélépipédique, en [grès]{.subject} local, inclinée vers le Sud, mesurant 1m de haut et 0,30m d'épaisseur.

Historique

Pierre découverte par [Meyrat F.]{.creator} en [avril 2012]{.date}.

### [Ascain]{.spatial} - [Trois Fontaines Erreka]{.coverage} 1 - [Tertre d'habitat]{.type}

Localisation

Altitude : 475m.

Il est sur un vaste terrain en pente douce, Est-Ouest, à une vingtaine de mètres à l'Ouest de la naissance d'un petit affluent Est du ruisseau des Trois fontaines. Une borde en ruine est à 400m au Nord-Est.

Description

Tertre de 10m de diamètre, asymétrique, de 0,40m dans sa plus grande hauteur.

Historique

Tertre d'habitat découvert par [Meyrat F.]{.creator} en [avril 2012]{.date}.

### [Ascain]{.spatial} - [Trois Fontaines Erreka]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 540m.

Au pied de la crête d'Altxangue, à l'extrémité Ouest de celle-ci, et en bordure du chemin qui la contourne par le Nord.

Description

Tumulus pierreux, mesurant 7m de diamètre et 0,40m de haut, ayant été dégradé sur son flanc Est par le passage de la piste qui ampute son bord Sud. Au centre, 2 petites dalles verticales (0,80m de long et 0,23m de haut ; 0,52m de long et 0,20m de haut), pourraient représenter les vestiges d'un [ciste]{.subject}. On note une pierre à la périphérie Nord qui mesure 2,40m de long, 1,26m de large et 0,20m de haut ; une autre pierre importante (2m x 1m), se voit au Sud, simplement posée sur le sol.

Il semblerait enfin que 7 pierres délimitent une partie de la périphérie, à l'Ouest.

Historique

Tumulus découvert par [Meyrat F.]{.creator} en [avril 2012]{.date}.

### [Ascain]{.spatial} - [Trois maisons]{.coverage} - [Dolmen]{.type}

Localisation

Carte IGN 1245 OT Hendaye Saint-Jean-de-Luz.

Altitude : 165m.

Ce monument très difficile à repérer, perdu dans un océan de fougères et de ronces, même en hiver, est situé sur un petit mamelon au flanc Nord de Larraun, à environ 100m au Sud du quartier dit « des 3 maisons ».

Description

On note un important tumulus de pierres atteignant semble-t-il au moins une quinzaine de mètres de diamètre. Au centre se distingue la [chambre funéraire]{.subject} : une profonde excavation rectangulaire orientée Est-Ouest, limitée au Nord et au Sud par deux importantes dalles verticales de [grès]{.subject} rose local. La dalle Nord mesure environ 3m de long et 1,45m de haut, son épaisseur atteint 0,35m en moyenne ; la dalle Sud possède les mêmes dimensions mais pour une épaisseur moindre (0,10m environ). On note des traces d'épannelage très nettes sur une grande partie de la périphérie de ces dalles.

Historique

Monument vu par [Blot J.]{.creator} en [décembre 2009]{.date}, avec l'aide de M. [J.F. Servier]{.creator}.

Il pourrait peut-être s'agir du dolmen de *Putxerri* signalé par J.M. de Barandiaran, [@barandiaranHombrePrehistoricoPais1953 p.244] bien que la description et les mensurations de ce dernier soient bien différentes de celui-ci...

### [Ascain]{.spatial} - [Xeruen Haut]{.coverage} Ouest - [Dolmen]{.type}

Localisation

Altitude :300m.

À l'extrémité Nord-Ouest d'un vaste plateau au flanc Nord de Larrun.

Description

Monument modeste, sans tumulus apparent et bien dégradé. La [chambre funéraire]{.subject}, qui mesure environ 2m de long et 0,90m de large, est orientée Nord-Sud. Elle est essentiellement marquée par une dalle plantée, à l'Est, mesurant 1,20m de long à sa base, 0,70m de haut et 0,15m d'épaisseur. À l'Ouest, et au Nord, deux dalles couchées, dont celle du Nord mesure 0,59m de long et 0,52m de large.

Historique

Monument signalé par le [groupe Hilharriak]{.creator} en [2011]{.date}.

### [Ascain]{.spatial} - [Xeruen Haut]{.coverage} - [Dalle plantée]{.type}

Localisation

Altitude : 300m.

Située à l'extrémité Sud-Est du plateau.

Description

Dalle de [grès]{.subject} de forme triangulaire, plantée verticalement selon un axe Nord-Sud., mesurant 1,10m à sa base, 0,90m de haut et 0,18m d'épaisseur.

Historique

Dalle découverte par [Blot J.]{.creator} en [mars 2012]{.date}.

### [Ascain]{.spatial} - [Xeruen Haut]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 300m.

Il est à 13m au Nord de *Xeruen Haut - Dalle plantée*.

Description

Petit tumulus, discret, de terre et de pierres, mesurant 3m de diamètre et 0,30m de haut.

Historique

Monument découvert par [Meyrat F.]{.creator} en [mars 2012]{.date}.

### [Ascain]{.spatial} - [Xeruen Haut]{.coverage} Est - [Dolmen]{.type}

Localisation

Altitude : 300m.

Il est à 27m au Nord-Est du *Tumulus du même nom*.

Description

Deux dalles parallèles, très inclinées vers l'Est, quasiment couchées au sol, paraissent être les seuls vestiges d'une [chambre funéraire]{.subject}, orientée Est-Ouest ; la dalle Ouest mesure 1m à sa base, 0,70m de haut, celle à l'Est : 0,90m à sa base, 0,53m de haut et 0,14m d'épaisseur en moyenne.

Historique

Monument signalé par le [groupe Hilharriak]{.creator} en [2011]{.date}.

### [Ascain]{.spatial} - [Xeruen Haut]{.coverage} - [Tertre d'habitat]{.type}

Localisation

Altitude : 280m.

Situé en bordure Nord d'une piste qui descend en oblique vers l'Ouest au flanc Nord de Larrun.

Description

Tertre de 6m de diamètre, 0,40m de haut, asymétrique, érigé sur un terrain en pente vers le Nord.

Historique

Tertre découvert par [Blot J.]{.creator} en [mars 2012]{.date}.

### [Ascain]{.spatial} - [Xeruen haut]{.coverage} 2 - [Dalle couchée]{.type}

Localisation

Altitude : 304m.

Il est à proximité des monuments déjà cités sous la rubrique « Xéruen haut », [@blotInventaireMonumentsProtohistoriques2013 p.7], et à 7m au Sud-Est des ruines d'une borde et d'un enclos de pierres sèches.

Description

Dalle de [grès]{.subject} [triasique]{.subject}, couchée au sol, en forme « de pain de sucre, mesurant 2,27m de long avec une base de 0,70m et un sommet plus étroit de 0,50m ; son épaisseur est d'une vingtaine de centimètres en moyenne. Cette dalle présente des traces d'épannelage tout le long de son bord Nord-Ouest, le sommet et la base ayant aussi des traces de régularisation - Dans le contexte de carrière de ces lieux, rien ne permet de supposer une grande ancienneté à ces traces.

Historique

Dalle découverte par [A. Martinez]{.creator} en [2013]{.date}.

### [Ascain]{.spatial} - [Xuanenborda]{.coverage} - [Grande dalle]{.type}, [dolmen]{.type} ([?]{.douteux})

Localisation

Altitude : 365m.

Elle est située à environ une centaine de mètres en contre-bas et à gauche du GR 10 qui descend des Trois Fontaines vers Olhette.

Description

Elle repose sur un « tumulus » d'une quinzaine de mètres de diamètre, formé de gros blocs de [grès]{.subject} et mesure 3m de long dans son grand axe Est-Ouest, 2,44m de large dans l'axe Nord-Sud et 0,35m d'épaisseur ; un gros fragment s'est détaché de son bord Sud-Ouest. Son bord Nord paraît présenter des traces d'épannelage. Est-ce la dalle de [couverture]{.subject} d'un grand dolmen ?, un « monolithe » ? ou une dalle naturellement venue là ?

Historique

Dalle découverte par [J. Blot]{.creator} en [mars 2010]{.date}.

### [Ascain]{.spatial} - [Zelaia]{.coverage} Nord 1 - [Monolithe]{.type}

Localisation

Carte IGN 1245 OT Hendaye Saint-Jean-de-Luz.

Altitude : 220 mètres.

Ce monument, est situé sur le flanc Nord de la Larraun, à l'extrémité Nord-Ouest du pierrier issu du plateau Ihicelaya. À quelques mètres, du côté Ouest, coule un petit riu où viennent s'abreuver les animaux qui paissent en ces lieux. Monument difficile à trouver compte tenu d'un relief très tourmenté et d'une abondante végétation.

Description

Belle dalle de [grès]{.subject} rose plantée quasiment à la verticale sur un terrain en légère pente vers le Nord-Ouest ; on note qu'au Sud-Est elle prend appui sur trois autres dalles plus petites qui lui servent de blocage amont par rapport à l'axe de la pente. Cette dalle de forme grossièrement trapézoïdale, mesure 3,24m dans sa plus grande hauteur, 2,50m à sa base, et 1,70m à sa partie moyenne. Son épaisseur varie de 0,40m à sa base à 0,16m à sa partie supérieure. Elle présente des traces d'épannelage sous forme d'enlèvements de gros éclats, tant dans ses bords verticaux qu'au sommet.

Historique

Monument découvert par [A. Martinez Manteca]{.creator} en [1999]{.date}.

### [Ascain]{.spatial} - [Zelaia]{.coverage} Nord 2 - [Monolithe]{.type}

Localisation

Carte IGN 1245 OT Hendaye Saint-Jean-de-Luz.

Altitude : 125 m.

Il est situé à 70m environ, au Sud-Ouest d'une maison récemment construite à l'extrémité haute du second chemin qui part vers la droite, sur la route Olhette-Ascain.

Description

Importante dalle de [grès]{.subject} rose de forme ovoïde, gisant sur le sol, orientée Nord-Sud ; elle mesure 2,77m de long, de 1,56m à 1,72m de large. Son épaisseur varie, suivant les endroits de 0,14m à 0,44m. Son bord Ouest présente, semble-t-il, des traces d'épannelage, ainsi que sa base Sud. La végétation cache le bord Est. Elle est la seule dalle de ce type dans les environs immédiats.

Historique

Monument découvert par [J. Blot]{.creator} en [janvier 2010]{.date}.

### [Ascain]{.spatial} - [Zelaia]{.coverage} 2 - [Dolmen]{.type}

Localisation

Altitude : 130m.

Ce monument (douteux), très voisin de *Zelaia Nord 2 - Monolithe*, est situé lui aussi à environ 70m, mais au Sud de la maison récemment construite à l'extrémité haute du second chemin qui part vers la droite, sur la route Olhette-Ascain.

Description

On note deux dalles de [grès]{.subject}, bien visibles et parallèles qui pourraient délimiter une [chambre funéraire]{.subject} orientée Nord-Sud. La dalle est mesure 2m de long, 0,70m de haut et son épaisseur est d'environ 0,12m. Elle est séparée de la dalle Ouest par 0,70m. Celle-ci mesure 1,60m de long, 0,60m de haut et 0,14m d'épaisseur. Il n'y a pas de tumulus visible. À quelques dizaines de centimètres au Sud se distingue une dalle en grande partie enfouie.

Historique

Monument découvert en [mars 1990]{.date} par [I. Gaztelu]{.creator}.
