# Lantabat

### [Lantabat]{.spatial} - [Bertugaine]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 400m.

Il est au point culminant de la colline, au Nord d'un réservoir d'eau en ciment qui lui est tangent au Sud. La piste passe sur son sommet.

Description

Tumulus circulaire de terre et de pierres, de 15m de diamètre et 0,60m de haut ; il semble bien qu'un filon rocheux, orienté Sud-Nord, ait été inclus dans le monument, mais un aménagement a été fait pour rendre ce tumulus parfaitement circulaire.

Historique

Monument découvert par [Blot J.]{.creator} en [novembre 1975]{.date}.

### [Lantabat]{.spatial} - [Cote 395]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 395m.

Ce tumulus est érigé au sommet de la colline située au Nord-Est du col Gagnekolepoa, à 4m au Sud-Ouest de la clôture barbelée qui s'étend selon un axe Nord-Ouest Sud-Est.

Description

Tumulus herbeux circulaire, très érodé, de 5,30m de diamètre semble-t-il, et 0,25m de haut, sur sol plat. Il fait face au tumulus d'Haranbeltz sur la colline visible au Nord-Est.

Historique

Monument découvert par [J. Blot]{.creator} en [avril 2013]{.date}.

### [Lantabat]{.spatial} - [Donapétria]{.coverage} - [Tertre pierreux]{.type}

Localisation

Altitude : 310m.

Situé au flanc Est de la colline *Arxilako Kaskoa*, au milieu d'un vaste replat et tangent au Nord à la piste pastorale qui traverse ce plateau.

Description

Petit tertre pierreux circulaire (Tumulus ?) en forme de galette aplatie, mesurant 5m de diamètre et 0,40m de haut.

Historique

Monument découvert par [Blot J.]{.creator} en [1971]{.date}.

### [Lantabat]{.spatial} - [Gagnekolepoa]{.coverage} - [Tumulus]{.type}

Localisation

Au milieu du col de ce nom, à quelques mètres à l'Est de la piste qui le contourne.

Altitude : 380m.

Description

Vaste tumulus essentiellement pierreux, de 14m de diamètre environ et de 1m de haut.

Historique

Tumulus découvert par le [groupe Hilarriak]{.creator} en [mars 2013]{.date}.

Nous avons aussi noté, à quelques dizaines de mètres plus loin à l'Est, en montant vers la colline, un structure d'origine anthropique mal définissable, sur la droite de la piste...

### [Lantabat]{.spatial} - [Gagnekolepoa]{.coverage} 1 et 2 - [Tertres pierreux]{.type} 

Localisation

Ces deux monuments, tangents, sont aussi tangents au bord Ouest de la piste.

Altitude : 385m.

Description

- *Tp 1* : tertre pierreux tangent à la piste, le plus au Nord, mesure 5m de diamètre et 0,30m de haut.

- *Tp 2* : le plus au Sud, pierreux, mesure 3m de diamètre et 0,15m de haut.

Historique

Tertres découverts par le [groupe Hilarriak]{.creator} en [mars 2013]{.date}.

### [Lantabat]{.spatial} - [Gagnekolepoa]{.coverage} 3 - [Tertre pierreux]{.type}

Localisation

Il est situé à 16m à l'Est de *Gagnekolepoa 1 et 2 - Tertres pierreux*, sur le flanc de la colline.

Altitude : 387m.

Description

Tertre pierreux de 3m de diamètre et 0,30m de haut

Historique

Tertre découvert par [J. Blot]{.creator} en [avril 2013]{.date}.

### [Lantabat]{.spatial} - [Iratze gorriko lepoa]{.coverage} Sud - [Tertre pierreux]{.type}

Localisation

Sur la pente qui domine au Sud le col de ce nom.

Altitude : 371m.

Description

Tertre pierreux de 6m de diamètre, quelques centimètres de haut, érigé sur terrain en pente légère vers le Nord.

Historique

Tertre vu par [J. Blot]{.creator} en [1975]{.date} et revisité par lui en [2013]{.date}.

### [Lantabat]{.spatial} - [Iratze gorriko lepoa]{.coverage} Est - [Tertre pierreux]{.type}

Localisation

Il est plus bas que *Iratzegorriko lepoa Sud - Tertre pierreux* et à 18m au Nord-Est de lui.

Altitude : 368m.

Description

Tertre pierreux sur terrain en légère pente, mesurant 6m de diamètre et 0,30m de haut.

Historique

Tertre vu par [J. Blot]{.creator} en [1975]{.date} et revisité par lui en [2013]{.date}.

### [Lantabat]{.spatial} - [Iratze gorriko lepoa]{.coverage} Ouest - [Tertre pierreux]{.type}

Localisation

À 20m à l'Ouest Nord-Ouest de *Iratze gorriko lepoa Est - Tertre pierreux* et à 18m de *Iratzegorriko lepoa Sud - Tertre pierreux*.

Altitude : 368m.

Description

Tertre pierreux de 6m de diamètre net quelques centimètres de haut

Historique

Tertre vu par [J. Blot]{.creator} en [1975]{.date} et revisité par lui en [2013]{.date}.

### [Lantabat]{.spatial} - [Laparzale]{.coverage} Sud 1 - [Tertre pierreux]{.type}

Localisation

Altitude : 353m.

Description

Tertre pierreux de 6m de diamètre et 0,30m de haut.

Historique

Tertre vu par [J. Blot]{.creator} en [1975]{.date} et revisité par lui en [2013]{.date}.

### [Lantabat]{.spatial} - [Laparzale]{.coverage} Sud 2 - [Tertre pierreux]{.type}

Localisation

Altitude : 353m.

À 20m à l'Ouest Nord-Ouest de *Laparzale Sud 1 - Tertre pierreux* et à 45m à l'Est Sud-Est du *Grand Tumulus*.

Description

Tertre pierreux de 4m de diamètre et quelques centimètres de haut.

Historique

Tertre vu par [J. Blot]{.creator} en [1975]{.date} et revisité par lui en [2013]{.date}.

### [Lantabat]{.spatial} - [Laparzale]{.coverage} Sud 3 - [Tertre pierreux]{.type}

Localisation

Altitude : 353m.

Situé à 20m à l'Ouest du *Grand Tumulus*.

Description

Tertre pierreux de 6m de diamètre et quelques centimètres de haut.

Historique

Tertre vu par [J. Blot]{.creator} en [1975]{.date} et revisité par lui en [2013]{.date}.

### [Lantabat]{.spatial} - [Lindugnekolepoa]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 392m.

Il est à l'extrémité Ouest de la colline Bertugaine, dominant ainsi le col Lindugnekolepoa.

Description

Tumulus circulaire de terre et de pierres de 14m de diamètre et 0,90m de haut.

Historique

Monument découvert par [Blot J.]{.creator} en [novembre 1975]{.date}.

### [Lantabat]{.spatial} - [Pagaburu]{.coverage} 1 - [Tumulus]{.type}

Localisation

Altitude : 437m.

Dans un petit replat qui suit la ligne de crête dénommée « Pagaburu » et qui précède la cote 440. Il se trouve au Sud-Est de la route carrossable menant aux postes d'hébergement des chasseurs. Situé près d'un poste de tir, en bordure de la piste ascendante vers la crête Pagaburru.

Description

Tumulus circulaire de terre et de pierraille, mesurant 7m de diamètre et 0,40m de haut ; il est parfaitement intact.

Historique

[Nous]{.creator} avions brièvement signalé ces 3 tumulus dans la publication suivante : [@blotTumulusBixustiaZuhamendi1976, p.115]. Nous avons revu ces monuments en avril 2015, et les décrivons ici.

### [Lantabat]{.spatial} - [Pagaburu]{.coverage} 2 - [Tumulus]{.type}

Localisation

Il se trouve au Sud-Est de la route carrossable menant aux postes d'hébergement des chasseurs.

Il est situé à une quarantaine de mètres au Sud-Est de *Pagaburu 1 - Tumulus*, et a été remanié en partie dans sa périphérie Sud-Ouest par la création d'un poste de tir.

Description

Tumulus de terre et de pierraille, qui fut circulaire, (mais remanié au Sud-Ouest), mesurant environ 7m de diamètre et 0,30m de haut.

Historique

[Nous]{.creator} avions brièvement signalé ces 3 tumulus dans la publication suivante : [@blotTumulusBixustiaZuhamendi1976, p.115]. Nous avons revu ces monuments en avril 2015, et les décrivons ici.

### [Lantabat]{.spatial} - [Pagaburu]{.coverage} 3 - [Tumulus]{.type}

Localisation

Il se trouve au Sud-Est de la route carrossable menant aux postes d'hébergement des chasseurs.

Il est à 25m au Nord de *Pagaburu 2 - Tumulus* et à 15m à l'Est de *Pagaburu 1 - Tumulus*.

Description

Tumulus circulaire de terre et de pierraille, bien visible, mesurant 10m de diamètre et 0,40m de haut.

Historique

[Nous]{.creator} avions brièvement signalé ces 3 tumulus dans la publication suivante : [@blotTumulusBixustiaZuhamendi1976, p.115]. Nous avons revu ces monuments en avril 2015, et les décrivons ici.

### [Lantabat]{.spatial} - [Pagaburu]{.coverage} 1 - [Tertre d'habitat]{.type}

Localisation

Altitude : 468m.

On peut le voir longé au Nord-Ouest et au Sud-Ouest par les 2 pistes ascendantes, à quelques dizaines de mètres plus au Sud-Est du *tumulus T2*.

Description

Tertre de terre, asymétrique mesurant 12m de long, 8m de large et 0,40m de haut ; en grande partie recouvert par un roncier.

Historique

Monument découvert par [F. Meyrat]{.creator} en [avril 2015]{.date}.

### [Lantabat]{.spatial} - [Pagaburu]{.coverage} 2 - [Tertre d'habitat]{.type}

Localisation

Altitude : 472m.

Il est situé à 65m au Nord-Ouest de *Pagaburu 1 - Tertre d'habitat* et, comme lui, bordé par les 2 pistes ascendantes.

Description

Tertre de terre, asymétrique, mesurant 8m de long, 6m de large et 0,50m de haut.

Historique

Monument découvert par [F. Meyrat]{.creator} en [avril 2015]{.date}.

### [Lantabat]{.spatial} - [Pentzézaharreko Ithurria]{.coverage} - [Tertre d'habitat]{.type}

Localisation

Altitude : 537m.

Il est situé sur un terrain en légère pente vers l'Est, et à une quinzaine de mètres à l'Ouest d'une piste pastorale.

Description

Tertre dissymétrique discret, de 6m de diamètre et 0,40m de haut.

Historique

Monument découvert par [F. Meyrat]{.creator} en [novembre 2014]{.date}.
