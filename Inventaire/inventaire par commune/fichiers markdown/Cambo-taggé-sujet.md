# Cambo

### [Cambo]{.spatial} - [Marienea]{.coverage} - [Tertre d'habitat]{.type} ([?]{.douteux})

Localisation

Altitude : 90m.

On le voit sur la gauche de la route qui se rend à Louhossoa, une centaine de mètres avant le pont qui l'enjambe.

Description

Ce « tertre » est visible sur un terrain pentu, très près du borde de route et mesure, comme d'habitude, environ 2m de haut et 18m à 20m de large. Ce qui pose problème est la présence dans le voisinage proche, de mouvements de terrains qui pourraient être rattachés à des éboulements, à de la solifluxion. Nous éliminons toute parenté avec les travaux du *Camp de César*.

Historique

Tertre vu par [Blot J.]{.creator} en [septembre 2001]{.date}.
