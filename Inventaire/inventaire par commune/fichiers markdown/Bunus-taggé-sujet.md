# Bunus

### [Bunus]{.spatial} - [Gaintzale]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 526m.

Ce tumulus est situé à une quarantaine de mètres à l'Ouest du sommet et à 30m au Nord-Ouest d'un réservoir en ciment.

Description

Tumulus circulaire de 5,50m de diamètre, très aplati (0,15m de haut), matérialisé, outre son faible relief, par une trentaine de petits blocs [calcaire]{.subject} plus ou moins enfouies dans le sol, dont une vingtaine pourraient semble-t-il marquer la périphérie. On note une légère dépression en secteur Sud.

Historique

Monument découvert par [P. Velche]{.creator} en [mai 2015]{.date}.
