
## édition des **Noms des monuments** (dans word)
`Commune - Lieu Dit n - Type`

## bloc yaml pour 'title'
```markdown
---
title: Arnégui

...
```

## la template gère la balise `<article></article>`

## Marquage des valeurs des propriétés
- md `[valeur]{.propriété}` 
- = html `<span class="propriété">valeur</span>` 

### Propriétés // classes
- `[]{.commune}`
- `[]{.lieu}`
- `[]{.type}`
- `[]{.date}`
- `[]{.inventeur}`
- `[]{.sujet}`
- `[]{.monument_détruit}`
- `[]{.rapport}`
- `[]{.hal}`
- `[]{.lien_monument}`
- `[]{.sub}`
    
## Rechecherche des réf()
- ajout de la référence dans Zotero
- remplacer `réf()` par `[prefix @cléBibTex, p.xxx]`
- ajout de `###### Références` à la fin

## création des sections 'monument'

- md `::::: {monument}` ... `:::::` 
- =html `<section class="monument">` ... `</section>`


```sh
sed -e '/^### /i \<\/section\>\n\n\<section class\=\"monument\"\>\n' Arnégui_taggé.md > Arnégui_monument.md
```

à la main, déplacer le premier `</section>` à la fin du dernier monument. 

### pb avec l'extension `fenced_divs`
éditeur de texte
- Remplacer `::: monument` par `<section class="monument">`
- Remplacer `:::` par `</section>`

## h4 
- Localisation
- Description
- Historique

```sh
sed -i -e 's/Localisation/#### Localisation/g' Arnégui_monument.md

sed -i -e 's/Description/#### Description/g' Arnégui_monument.md

sed -i -e 's/Historique/#### Historique/g' Arnégui_monument.md
```

## Pandoc --> html

### correction de `gallia-prehistoire.csl` :
- remplacer `&amp;#160;` par `&#160;`

### générer le fichier `Toutes_communes.html `
```sh
pandoc -o ../fichiersHtml/Toutes_communes.html --self-contained -s --toc --template ./data/templateHtml.html -H ./data/inventaire.css --bibliography=./Data/BiblioInventaire.bib --csl=./data/gallia-prehistoire.csl Ahaxe_monument.md Aincille_monument.md Alçay-Alçabéhéty-Sunharette_monument.md Anhaux_monument.md Arcangues_monument.md Arette_monument.md Arnégui_monument.md Mendive_monument.md
```
### générer tous les `*_monument.html` des `*_monument.md`

```sh
find . -name "*_monument.md" | while read i; do pandoc -o ../fichiersHtml/"${i%.*}.html" --self-contained -s --toc --template ./data/templateHtml.html -H ./data/inventaire.css --bibliography=./data/BiblioInventaire.bib --csl=./data/gallia-prehistoire.csl "$i"; done
```

## BaseX

```sh
for $i in //*:section[@class="monument"]

let $title := string-join(
  for $z in $i/h3//text()
  return
  $z, ' ')

let $spatial := for $a in $i//*:span[@class="spatial"]
return <spatial>{$a/text()}</spatial>

let $coverage := for $b in $i//*:span[@class="coverage"]
return <coverage>{$b/text()}</coverage>

let $type := for $c in $i//*:span[@class="type"]
return <type>{$c/text()}</type>

let $date := for $d in $i//*:span[@class="date"]
return <date>{$d/text()}</date>

let $creator := for $e in $i//*:span[@class="creator"]
return <creator>{$e/text()}</creator>

let $subject := for $f in $i//*:span[@class="subject"]
return <subject>{$f/text()}</subject>

let $douteux := for $g in $i//*:span[@class="douteux"]
return <douteux>{$g/text()}</douteux>

let $description := <description>$i</description>

return <notice><title>{$title}</title>{$spatial}{$coverage}{$type}{$date}{$creator}{$subject}{$douteux}{$description}</notice>
```

## export --> Mendive_notices.xml

ajout de la balise `<notices></notices>` au début et à la fin du document