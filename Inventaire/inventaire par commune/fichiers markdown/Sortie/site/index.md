::: {#gauche}
##### [Mode \"diapo\"](index-diapo.html) ::: [\[\#Top\]](#top) [(Acceuil)]{style="font-size: 0.7em;"} {#mode-diapo-top-acceuil style="margin-bottom: 1.5em;"}

------------------------------------------------------------------------

##### ↓ Sommaire ↓ ::: ↓ Tous les fichiers ↓

------------------------------------------------------------------------

::: {#left}
:::

::: {#gd-menu}
::: {#menu}
::: {#dis-toc}
##### Sommaire -/+

-   [Présentation](#intro)
-   [Objectifs](#objectifs)
-   [Collection `HAL`](#hal)
-   [Bibliographie `Zotero`{.logiciel}](#zotero)
-   [Traitement des sources `Word`{.logiciel}](#word)
-   [Quelles notices de monument ?](#format-notice)
-   [Traitement du texte `Markdown`](#markdown)
-   [html - css - js](#html)
-   [xquery - xslt](#xquery-xslt)
-   [csv](#csv)
-   [OpenRefine](#openRefine)
-   [OmekaS - Import des notices](#omekaS-1)
-   [Tropy](#tropy)
-   [OmekaS - Import des images](#omekaS-2)
:::
:::

::: {#raccourcis}
##### Tous les fichiers +/-

[Le dépôt GitLab du
projet](https://git.univ-pau.fr/gaelannuzelt/projet-blot)

[final.md](final.md){.fichier} : Le fichier `markdown` final

-   [copy.html](copy.html){.fichier} : fichier html-javascript pour
    faciliter le copier/coller des `{.classes}` dans les fichiers
    `markdown`.

[L\'inventaire consultable en ligne](final2.html)

-   [final2.html](final2.html){.fichier} : le fichier complet généré par
    `Pandoc`{.logiciel} (`css` et `js` dans l\'en-tête)
-   [Final-sans.html](Final-sans.html){.fichier} : le fichier html sans
    style ni script.
-   [css_inventaire.css](css_inventaire.css){.fichier} : la feuille
    `css`
-   [js_inventaire.js js](js_inventaire.js){.fichier} : deux petits
    scripts
-   [templateHtml.html](templateHtml.html){.fichier} : La template
    `html` pour `Pandoc`{.logiciel}
-   [BiblioInventaire.json](BiblioInventaire.json){.fichier} : La biblio
    au format `csl-json`
-   [gallia-prehistoire.csl](gallia-prehistoire.csl){.fichier} : Le
    style `csl` *Gallia Préhistoire* modifié

[tableau.html](tableau.html){.fichier} : Les notices extraites de
l\'inventaire dans un tableau html brut

-   [notices.xq](notices.xq){.fichier} : La requête `Xquery` sur
    `final2.html`{.fichier} (`BaseX`{.logiciel})
-   [notices.xml](notices.xml){.fichier} : Le fichier `xml` de
    \'notices\' ainsi obtenu
-   [tableau.xsl](tableau.xsl){.fichier} : La feuille de transformation
    `xsl` pour transformer `notices.xml` en `tableau.html`
-   [tableau.ipynb](tableau.ipynb){.fichier} : Le notebook
    `jupyter`{.logiciel} avec le petit script `python` pour appliquer la
    transformation `xsl` et générer `tableau.html`{.fichier}

Tropy

-   template item
-   template photo
-   

OmekaS

-   template item
-   template collection
-   
:::
:::
:::

+-----------------------------------+-----------------------------------+
| ##                                | ::: {#top}                        |
| Projet Blot {#projet-blot .titre} | ##### Accès direct -/+            |
|                                   |                                   |
|                                   | -   [Inventaire en                |
|                                   |     ligne](final2.html)           |
|                                   |                                   |
|                                   | -   [Tableau des                  |
|                                   |     notices](tableau.html)        |
|                                   |     extrait de l\'inventaire html |
|                                   |                                   |
|                                   | -   [Collection                   |
|                                   |     HAL](https://h                |
|                                   | al-univ-pau.archives-ouvertes.fr) |
|                                   |     travaux et références de J.   |
|                                   |     Blot                          |
|                                   |                                   |
|                                   | -   [Bibliothèque                 |
|                                   |     Omeka](#)[]{#omeka-message    |
|                                   |     styl                          |
|                                   | e="color:red; font-weight: 600;"} |
|                                   | :::                               |
+-----------------------------------+-----------------------------------+

::: {#affiche}
.
:::

::: {#intro .section .etape}
### Présentation
:::

::: {#objectifs .section .etape}
### + - Objectifs

::: {#dis-objectifs style="display: none;"}
Publier les photos de terrain et les croquis de Jacques Blot

-   4600+ photos
-   Utilisation de `Tropy`{.logiciel} pour le catalogage et
    l\'indexation.

Relier les photos aux notices de monuments de l\'inventaire dans une
bibliothèqe numérique

-   Solution `OmekaS`{.logiciel}
-   Créer des notices de monuments à partir des fichiers sources (.docx)
    de l\'inventaire.

Faciliter l\'accès aux publications de Jacques Blot et mettre en valeur
ses recherches

-   Création d\'une collection HAL

Publier le dernier tome de son *Inventaire des Monuments
Protohistoriques en Pays Basque de France*.
:::
:::

::: {#hal .section .etape}
### + - Collection `HAL`

::: {#dis-hal style="display: none;"}
-   Gaëlle et Marina
-   [Patrimoine archéologique du Pays Basque - Collection Jacques
    Blot](https://hal-univ-pau.archives-ouvertes.fr/BLOT).
-   Sur le portail
    [HAL-e2s-UPPA](https://hal-univ-pau.archives-ouvertes.fr).
:::
:::

::: {#zotero .section .etape}
### + - Bibliographie `Zotero`{.logiciel}

::: {#dis-zotero style="display: none;"}
Constitution de la collection `BiblioInventaire` à partir de la biblio
fournie et de la [collection HAL](#hal)

Installer le plugin `Better BibTex`{.logiciel} pour `Zotero`{.logiciel}
\| [lien](https://retorque.re/zotero-better-bibtex/) \|

export de la collection `BiblioInventaire` vers le dossier
`fichiers markdown/sortie/data`{.dossier}

-   avec le convertisseur `Better CSL JSON`
-   avec l\'option `Garder à jour` cochée
-   fichier `BiblioInventaire.json`{.fichier}

Vérouiller les clés de citation

Modifications du style `gallia-prehistoire.csl`

-   suppression des op. cit.
-   ajout des url
-   ajout de la désambiguisation des dates (1990a, 1990b\...)
-   correction entité html (remplacer `\&\amp;#160;` par `\&\#160;`,
    sans les échappements)
:::
:::

::: {#word .section .etape}
### + - Traitement des sources `Word`{.logiciel}

::: {#dis-word style="display: none;"}
-   dossier `sources word`{.dossier}.
-   67 fichiers (1 par commune)
-   tous les titres de monuments en style \"titre 3\"
-   clés bibtex
:::
:::

::: {#format-notice .section .etape}
### + - Quelles notices de monument ?

::: {#dis-format-notice style="display: none;"}
#### Format

-   Dublin Core (`DCMI Metadata Terms`)
-   renseignements sur CIDOC-CRM : trop complexe

#### Propriétés

  Propriétés dcterms      type de valeur                          méthode d\'attribution   markdown                       html
  ----------------------- --------------------------------------- ------------------------ ------------------------------ ------------------------------------------
  `dcterms:title`         *Commune - Lieu dit - type*             Xquery                   `### titre`                    `<h3>titre</h3>`
  `dcterms:spatial`       Commune `pactols:lieux`{.vocabulaire}   Xquery + tableur         `[Commune]{.spatial}`          `<span class="spatial">Commune</span>`
  `dcterms:coverage`      Lieux-dits                              Xquery                   `[Lieu-dit]{.coverage}`        `<span class="coverage">Lieu-dit</span>`
  `dcterms:type`          `pactols:sujets`{.vocabulaire}          Xquery + tableur         `[Chromlech]{.type}`           `<span class="type">Chromlech</span>`
  `dcterms:creator`       inventeur `IdRef`{.vocabulaire}         Xquery + tableur         `[J. Blot]{.creator}`          `<span class="creator">J. Blot</span>`
  `dcterms:date`          découverte                              Xquery + tableur         `[juin 1995]{.date}`           `<span class="date">juin 1995</span>`
  `dcterms:subject`       `pactols:sujets`{.vocabulaire}          Xquery + tableur         `[grès]{.subject}`             `<span class="subject">grès</span>`
  `dcterms:description`   texte `<html>` de l'inventaire          Xquery                   `<section class="monument">`   `<section class="monument">`
  `dcterms:temporal`      `pactols:chronologie`{.vocabulaire}     tableur                  \-                             \-
  `dcterms:audience`      `pactols:peuples`{.vocabulaire}         tableur                  \-                             \-

#### Vocabulaires / Référentiels

`pactols:lieux`{.vocabulaire}

`pactols:sujets`{.vocabulaire}

-   demande d\'ajout du mot-sujet `tertre d'habitat`

`pactols:chronologie`{.vocabulaire}

`pactols:peuples`{.vocabulaire}

`IdRef`{.vocabulaire}

`HAL ?`{.vocabulaire}
:::
:::

::: {#markdown .section .etape}
### + - Traitement du texte `Markdown`

::: {#dis-markdown style="display: none;"}
Commande `bash`{.logiciel} dans le dossier `sources word`{.dossier} pour
convertir avec `Pandoc`{.logiciel} tous les fichiers word en markdown
vers le dossier `fichiers markdown`{.dossier}.

------------------------------------------------------------------------

Afficher/Masquer

::: {#dis-pandoc-word style="display:none"}
``` {#c-pandoc-word}
$> find . -name "*.docx" | while read i; do pandoc -f docx -t markdown -s --atx-headers --wrap=none "$i" -o ../fichiers\ markdown/"${i%.*}.md"; done
                
```

Copier
:::

------------------------------------------------------------------------

#### Travail sur les fichiers par commune

Ajout du nom de la commune au début du nom de chaque monument.

-   Copier-coller multiple avec un bon éditeur de texte

Ajout des classes sur les propriétés dans le texte.

-   `[texte]{.classe}`
-   Le plus long, Gaëlle pour `.sujet` et `.douteux`, Julien pour le
    reste.
-   écriture d\'un petit fichier `html/javascript` pour faciliter le
    copier-coller des `{.classes}` : [`copy.html`{.fichier}](copy.html).

Ajout des titres niveau 4 avec `sed`{.logiciel} pour

-   Localisation
    Afficher/Masquer
    ::: {#dis-h4-loc style="display:none"}
    ``` {#c-h4-loc}
    sed -i -e 's/^Localisation/#### Localisation/g' *.md
                                
    ```

    Copier
    :::
-   Description
    Afficher/Masquer
    ::: {#dis-h4-desc style="display:none"}
    ``` {#c-h4-desc}
    sed -i -e 's/^Description/#### Description/g' *.md
                                
    ```

    Copier
    :::
-   Historique
    Afficher/Masquer
    ::: {#dis-h4-hist style="display:none"}
    ``` {#c-h4-hist}
    sed -i -e 's/^Historique/#### Historique/g' *.md
                                
    ```

    Copier
    :::

Découverte de *Collections* de monuments

-   laissées en titre de niveau 2
-   seront traitées ultérieurement en `item-collection` dans
    `OmekaS`{.logiciel}
-   écrire un template `item-collection` dans `OmekaS`{.logiciel}

#### Travail sur le fichier markdown final

Commande `Pandoc`{.logiciel} pour fusionner tous les fichiers `markdown`
des communes en un fichier `final.md`{.fichier} dans le dossier
`fichiers markdown/Sortie`{.dossier}

------------------------------------------------------------------------

Afficher/Masquer

::: {#dis-md-final style="display:none"}
``` {#c-md-final}
pandoc --wrap=none -o Sortie/final.md *.md
                
```

Copier
:::

------------------------------------------------------------------------

Ajout du bloc de métadonnées `yaml` (utilisé dans la template html)

-   titre, date, auteur, mots-clés\...
-   le Bloc `yaml` à coller au début du document `markdown`
    Afficher/Masquer
    ::: {#dis-yaml style="display:none"}
    ``` {#c-yaml}
    ---
    title: Jacques Blot - Inventaire de monuments protohistoriques en pays basque
    author: Jacques Blot
    date: 2021-02-03
    keywords:
      - Archéologie    
      - Monuments protohistoriques
      - Pyrénées-Atlantiques
      - Pays-Basque
      - Tumulus
      - Chromlech
      - Tertre
    editor:
      - name: Julien Rabaud
      - name: Gaëlle Chancerel
      - name: Marina Lloancy
    publisher: UPPA

    ---
                                
    ```

    Copier
    :::

ajout de la `<div id="refs">` qui accueillera les références générées
par `pandoc-citeproc`{.logiciel}.

-   code `html` à coller à la fin du document `markdown`
    Afficher/Masquer
    ::: {#dis-refs style="display:none"}
    ``` {#c-refs}
    <div id="biblio">
        <h6 class="unnumbered" id="bibliographie">Bibliographie</h6>
        <div id="refs">
        
        </div>
    </div>
                                
    ```

    Copier
    :::

Insérer les balises html `<section class="monument"></section>` autour
des notices de monuments :

Je n\'ai pas pu utiliser le marquage `markdown` des `fenced-divs`
(`:::{.monument}`) car mes sections englobent des titres (?).

Injection directe du `html` avec `Sed`{.logiciel}

Afficher/Masquer

::: {#dis-sections style="display:none"}
``` {#c-sections}
sed -e '/^### /i \<\/section\>\n\n\<section class\=\"monument\"\>\n' final.md > final_monument.md
                            
```

Copier
:::

Nécessite de déplacer à la main certaines balises fermantes :

-   la première (à coller à la fin du dernier monument).
-   toutes celles après un titre de niveau 1 (Commune), à coller à la
    fin du dernier monument de la commune précédente.
-   toutes celles après un titre de niveau 2 (Groupe de monuments), à
    coller à la fin du dernier monument avant le groupe.

Les **1233 monuments** sont maintenant balisés et on va pouvoir faire
des boucles `xquery` (sur l\'export `html`) à l\'intérieur de chacun.

Vérification et consolidation de la bibliographie (`Zotero`{.logiciel}).
:::
:::

::: {#html .section .etape}
### + - `html` - `css` - `javascript`

::: {#dis-html style="display: none;"}
#### Travail sur la template `Pandoc`{.logiciel}

La template `templateHtml.html`{.fichier} pour `Pandoc`{.logiciel}.

------------------------------------------------------------------------

Afficher/Masquer

::: {#dis-template style="display:none"}
``` {#c-template}
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" $if(lang)$ lang="$lang$" xml:lang="$lang$" $endif$$if(dir)$ dir="$dir$" $endif$>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <meta name="generator" content="pandoc" />
    $for(author-meta)$
    <meta name="author" content="$author-meta$" />
    $endfor$
    $if(date-meta)$
    <meta name="date" content="$date-meta$" />
    $endif$
    $if(keywords)$
    <meta name="keywords" content="$for(keywords)$$keywords$$sep$, $endfor$" />
    $endif$
    <title>$if(title-prefix)$$title-prefix$ ÔÇô $endif$$pagetitle$</title>
    <link rel="icon" type="image/png" href="data/dolmen.ico"/>
    <style type="text/css">
        code {
            white-space: pre;
        }

    </style>
    $if(quotes)$
    <style type="text/css">
        q {
            quotes: "ÔÇ£""ÔÇØ""ÔÇÿ""ÔÇÖ";
        }

    </style>
    $endif$
    $if(highlighting-css)$
    <style type="text/css">
        $highlighting-css$

    </style>
    $endif$
    $for(css)$
    <link rel="stylesheet" href="$css$" type="text/css" />
    $endfor$
    $if(math)$
    $math$
    $endif$
    $for(header-includes)$
    $header-includes$
    $endfor$
    
    
</head>

<body>
    <button href="#" onclick="toggle('TOC')">Masquer/Afficher l'index</button><button href="#" onclick="toggle('biblio')">Masquer/Afficher la bibliographie</button>
    
    $for(include-before)$
    $include-before$
    $endfor$
    $if(toc)$
    <div id="$idprefix$TOC">
        $toc$
    </div>
    $endif$
<article>
    $body$
    
    $for(include-after)$
    $include-after$
    $endfor$
</article>
</body>

</html>
                
```

Copier
:::

------------------------------------------------------------------------

Par rapport à la template par défaut (copiée et modifiée) :

-   Ajout des boutons \"masquer/afficher\" et de la favicon.
-   A creuser.

#### Travail sur la CSS

La css donnée à `Pandoc`{.logiciel} (`css_inventaire.css`{.fichier}),
sans les codes de coloration syntaxique.

------------------------------------------------------------------------

Afficher/Masquer

::: {#dis-cssinv style="display:none"}
``` {#c-cssinv}
<style type="text/css" > * {
    box-sizing: border-box;
}
a {
    color: MediumPurple;
    text-decoration: none;
}
a:hover {
    color: deeppink;
}
span.spatial {
    display: none;
}
article span.coverage:hover {
    color: chocolate;
}
article span.type:hover {
    color: brown;
}
span.creator:hover {
    color: steelblue;
}
span.date:hover {
    color: indianred;
}
span.subject {
    text-decoration: underline wavy dodgerblue;
}
span.subject:hover {
    color: dodgerblue;
    text-decoration: none;
}
span.douteux:hover {
    color: deeppink;
}
span.citation a {
    color: teal;
}
hr {
    border: none;
    border-bottom: 1px solid #999;
    width: 80%;
}
html,
body {
    margin: 0;
    margin-bottom: 4em;
    padding: 0;
}
body {
    background-color: MistyRose;
    color: black;
    font-family: 'Sitka', 'Perpetua', 'Garamond', 'Caladea', 'DejaVu', 'Georgia', 'Times New Roman', 'Times', serif;
}
#TOC {
    position: fixed;
    width: 20%;
    left: 1em;
    top: 1em;
    font-size: 120%;
    max-height: 95%;
    overflow-y: auto;
    scrollbar-width: thin;
    scrollbar-color: Tomato MistyRose;
}
#TOC h3 {
    font-size: 100%;
}
#biblio {
    position: fixed;
    width: 25%;
    top: 1em;
    right: 0em;
    font-size: 95%;
    max-height: 95%;
    overflow-y: auto;
    scrollbar-width: thin;
    scrollbar-color: Tomato MistyRose;
    padding: 1em;
    padding-left: 2em;
    text-indent: -1em;
}
article {
    position: fixed;
    top: 0em;
    left: 20%;
    width: auto;
    font-size: 1.2em;
    margin-right: 25%;
    line-height: 110%;
    max-height: 95%;
    padding-left: 2em;
    padding-right: 2em;
    overflow-y: auto;
    scrollbar-width: thin;
    scrollbar-color: Tomato MistyRose;
}

/* Better display on printing */
@media print {
    article {
        position: relative;
        display: block;
        left: -2em;
        width: auto;
        font-size: 10pt;
        margin: 0 auto;
        line-height: 100%;
    }
    #TOC {
        display: none;
    }
    #biblio,
    #refs {
        position: relative;
        display: block;
        width: auto;
    }
    p {
        widows: 1;
        orphans: 1;
    }
    .pagebreakafter {
        page-break-after: always;
    }
    .pagebreakbefore {
        page-break-before: always;
    }
    h1,
    h2,
    h3,
    h4 {
        page-break-after: avoid;
    }
    p,
    code,
    pre,
    blockquote,
    li,
    span,
    table {
        page-break-inside: avoid;
    }
}

article p {
    hyphens: auto;
    text-align: justify;
}
h1 {
    font-family: 'Caladea', 'Raleway', 'Lato', 'Liberation sans', 'Helvetica', sans-serif;
    color: crimson;
    letter-spacing: 3px;
    margin-top: 2em;
}
h2 {
    font-size: 1.25em;
    line-height: 110%;
    font-family: 'Century gothic', 'Raleway', 'Lato', 'Liberation sans', 'Helvetica', sans-serif;
    color: firebrick;
}
h3 {
    font-size: 1.2em;
    font-weight: 600;
    margin-top: 2em;
    margin-bottom: -0.25em;
    letter-spacing: 2px;
    font-family: 'Caladea', 'Tahoma', 'OCR A', 'Alef';
    color: MediumPurple;
}
h4 {
    font-family: 'Century gothic', 'Raleway', 'Lato', 'Liberation sans', 'Helvetica', sans-serif;
    font-size: 0.75em;
    margin-bottom: 0.2em;
    letter-spacing: 4px;
    margin-left: 1em;
    color: firebrick;
}
h5 {
    font-size: 0.8em;
    font-weight: 600;
    margin-bottom: 0.2em;
    margin-left: 2em;
    margin-top: 0.2em;
    font-family: 'Caladea', 'Tahoma', 'OCR A', 'Alef';
    color: tomato;
}
h6 {
    font-size: 95%;
    margin-top: 2em;
    line-height: 110%;
    font-family: 'Papyrus', 'Monotype Corsiva', 'Lucida Calligraphy', 'Century gothic', 'Raleway', 'Lato', 'Liberation sans', 'Helvetica', sans-serif;
    color: teal;
}
img {
    margin-top: 1em;
    max-width: 40%;
    height: auto;
}
figure {
    text-align: center;
}
figcaption {
    text-align: center;
    font-family: 'Century gothic', 'Tahoma', 'OCR A', 'Alef';
    font-style: italic;
    font-size: 0.7em;
    margin-top: 0.6em;
}
blockquote {
    font-size: 80%;
    color: rgba(120, 120, 120, 1);
    margin: 2% 5%;
    line-height: 120%;
}
table {
    border-collapse: collapse;
    width: 100%;
    font-size: 70%;
    font-family: 'Raleway', 'Lato', 'Liberation sans', 'Helvetica', sans-serif;
}
th,
td {
    padding: 4px 20px;
    border-bottom: 1px solid #333;
}
.tit {
    text-align: center;
    letter-spacing: 0px;
    margin-top: 1em;
    line-height: 120%;
}
.citation {
    color: teal;
    font-family: 'Georgia';
    font-size: 0.8em;
}
.references {
    line-height: 110%;
    font-size: 0.9em;
}
.csl-entry {
    margin-bottom: 0.5em;
}
.csl-entry a {
    color: RosyBrown;
}
.csl-entry a:hover {
    color: deeppink;
}
.csl-entry:target {
    color: teal;
    font-size: 120%;
    border-top: 2px solid black;
    border-bottom: 2px solid black;
    padding-top: 0.8em;
    padding-bottom: 0.8em;
    transition: 0.3s;
}
h2 ~ p,
h3 ~ p {
    margin-left: 3em;
    margin-bottom: 0em;
}
h4 ~ p,
section ul {
    margin-left: 3em;
    margin-top: 0em;
    margin-bottom: 0em;
}
li p:not(:first-child) {
    margin-top: -1em;
}
ul {
    list-style-type: '÷ ';
}
ul ul {
    list-style-type: '› ';
    margin-left: -1em;

}
#TOC ul > li > ul {
    display: none;
}
</style >
                
```

Copier
:::

------------------------------------------------------------------------

Layout en 3 colonnes

Afficher/Masquer

::: {#dis-layout style="display:none"}
``` {#c-layout}
    #TOC {
        position: fixed;
        width: 20%;
        left: 1em;
        top: 1em;
        font-size: 120%;
        max-height: 95%;
        overflow-y: auto;
        scrollbar-width: thin;
        scrollbar-color: Tomato MistyRose;
    }
    #biblio {
        position: fixed;
        width: 25%;
        top: 1em;
        right: 0em;
        font-size: 95%;
        max-height: 95%;
        overflow-y: auto;
        scrollbar-width: thin;
        scrollbar-color: Tomato MistyRose;
        padding: 1em;
        padding-left: 2em;
        text-indent: -1em;
    }
    article {
        position: fixed;
        top: 0em;
        left: 20%;
        width: auto;
        font-size: 1.2em;
        margin-right: 25%;
        line-height: 110%;
        max-height: 95%;
        padding-left: 2em;
        padding-right: 2em;
        overflow-y: auto;
        scrollbar-width: thin;
        scrollbar-color: Tomato MistyRose;
    }
                
```

Copier
:::

Usage du pseudo-sélecteur `:target`

-   Permet de mettre en évidence dans la bibliographie la référence
    cliquée dans le texte de l\'inventaire.
    Afficher/Masquer
    ::: {#dis-target style="display:none"}
    ``` {#c-target}
        .references {
            line-height: 110%;
            font-size: 0.9em;
        }

        .csl-entry {
            margin-bottom: 0.5em;
        }

        .csl-entry a {
            color: RosyBrown;
        }

        .csl-entry a:hover {
            color: deeppink;
        }

        .csl-entry:target {
            color: teal;
            font-size: 120%;
            border-top: 2px solid black;
            border-bottom: 2px solid black;
            padding-top: 0.8em;
            padding-bottom: 0.8em;
            transition: 0.3s;
        }
                                
    ```

    Copier
    :::

    .

Classes \"dcterms\" ajoutées dans le markdown.

Afficher/Masquer

::: {#dis-hover style="display:none"}
``` {#c-hover}
    span.spatial {
        display: none;
    }

    article span.coverage:hover {
        color: chocolate;
    }

    article span.type:hover {
        color: brown;
    }

    span.creator:hover {
        color: steelblue;
    }

    span.date:hover {
        color: indianred;
    }

    span.subject {
        text-decoration: underline wavy dodgerblue;
    }

    span.subject:hover {
        color: dodgerblue;
        text-decoration: none;
    }

    span.douteux:hover {
        color: deeppink;
    }

    span.citation a {
        color: teal;
    }
                            
```

Copier
:::

-   Masquage du nom des communes dans les noms de monuments.
-   `:hover` : changement de couleur.

Tests sur `@media print` (à pousser).

Afficher/Masquer

::: {#dis-mediaprint style="display:none"}
``` {#c-mediaprint}
@media print {
    article {
        position: relative;
        display: block;
        left: -2em;
        width: auto;
        font-size: 10pt;
        margin: 0 auto;
        line-height: 100%;
    }
    #TOC {
        display: none;
    }
    #biblio,
    #refs {
        position: relative;
        display: block;
        width: auto;
    }
    p {
        widows: 1;
        orphans: 1;
    }
    .pagebreakafter {
        page-break-after: always;
    }
    .pagebreakbefore {
        page-break-before: always;
    }
    h1,
    h2,
    h3,
    h4 {
        page-break-after: avoid;
    }
    p,
    code,
    pre,
    blockquote,
    li,
    span,
    table {
        page-break-inside: avoid;
    }
}
                            
```

Copier
:::

\...

#### Travail sur le Javascript

Le fichier `js_inventaire.js`{.fichier} donné à `Pandoc`{.logiciel}.

------------------------------------------------------------------------

Afficher/Masquer

::: {#dis-jsinv style="display:none"}
``` {#c-jsinv}
<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){  
    $('#TOC ul>li').click(function () {  
        $('#TOC ul>li').not(this).find('ul').hide();  
        $(this).find('ul').toggle();  
    });  
}); 
</script>

<script type="text/javascript">
function toggle(id) {
    var n = document.getElementById(id);
    n.style.display =  (n.style.display != 'none' ? 'none' : '' );
    }
</script>

<script type="text/javascript">

var links = document.querySelectorAll('div.csl-entry a');
for (var i = 0, linksLength = links.length; i < linksLength; i++) {
    if (!links[i].target) {
        
            links[i].target = '_blank';
    
    }
}
</script>

                
```

Copier
:::

------------------------------------------------------------------------

Contient 3 scripts :

-   
-   
-   

#### Commande Pandoc

Pour export de `final.md`{.fichier} en `final2.html`{.fichier}

------------------------------------------------------------------------

Afficher/Masquer

::: {#dis-pandoc-html style="display:none"}
``` {#c-pandoc-html}
pandoc -o final2.html -s --toc --template=data/templateHtml.html --include-in-header=data/css_inventaire.css --include-in-header=data/js_inventaire.js -C --bibliography=data/BiblioInventaire.json --csl=data/gallia-prehistoire.csl --metadata link-citations=true final.md
                
```

Copier
:::

------------------------------------------------------------------------

Options `Pandoc`{.logiciel}

-   `--self-contained` : abandonnée à cause de l\'utilisation de jquery.
-   `-s` (`--standalone`) : nécessaire pour les autres options.
-   `--toc` : génère le sommaire.
-   `--template`\[=fichier.html\] - ici `templateHtml.html`{.fichier}.
-   `--include-in-header`\[=fichier\] (x2 :
    `css_inventaire.css`{.fichier} et `js_inventaire.js`{.fichier}).
-   `-C` (`--citeproc`) : gestion des références bibliographiques
-   `--bibliography`\[=fichier\] (fichier des références
    bibliographiques) - ici `BiblioInventaire.json`{.fichier}.
-   `--csl`\[=fichier\] (style) - ici
    `gallia-prehistoire.csl`{.fichier}.
-   `--metada link-citations=true`
:::
:::

::: {#xquery-xslt .section .etape}
### + - `Xquery` - `xslt`

::: {#dis-xquery-xslt style="display: none;"}
#### Xquery

La requête (fichier `notices.xq`), exécutée dans `BaseX`{.logiciel}

------------------------------------------------------------------------

Afficher/Masquer

::: {#dis-xquery style="display:none"}
``` {#c-xquery}
for $i in //*:section[@class="monument"]

let $title := string-join(
  for $z in $i/h3//text()
  return
  $z, ' ')

let $spatial := for $a in $i//*:span[@class="spatial"]
return <spatial>"{$a/text()}"</spatial>

let $coverage := for $b in $i//*:span[@class="coverage"]
return <coverage>"{$b/text()}"</coverage>

let $type := for $c in $i//*:span[@class="type"]
return <type>"{$c/text()}"</type>

let $date := for $d in $i//*:span[@class="date"]
return <date>"{$d/text()}"</date>

let $creator := for $e in $i//*:span[@class="creator"]
return <creator>"{$e/text()}"</creator>

let $subject := for $f in $i//*:span[@class="subject"]
return <subject>"{$f/text()}"</subject>

let $douteux := for $g in $i//*:span[@class="douteux"]
(: return <douteux>{$g/text()}</douteux> :)
return <douteux>oui</douteux>

let $ref := for $h in $i//*:span[@class="citation"]
return <ref>"{$h/@data-cites/string()}"</ref>

let $description := <description><cdata>{$i}</cdata></description>

return <notice><title>{$title}</title>{$spatial}{$coverage}{$type}{$date}{$creator}{$subject}{$douteux}{$ref}{$description}</notice>
                
```

Copier
:::

------------------------------------------------------------------------

à noter :

-   
-   
-   
-   

#### Xslt

La feuille de style (fichier `tableau.xsl`), appliquée avec un petit
script python dans jupyter

------------------------------------------------------------------------

Afficher/Masquer

::: {#dis-xsl style="display:none"}
``` {#c-xsl}
<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" doctype-system="about:legacy-compat" encoding="UTF-8" indent="yes" />
    <xsl:template match="/">
        <html lang="fr">

        <head>
            <meta charset="utf-8" />
            <title>Tableau des notices Blot</title>
            <link rel="icon" type="image/png" href="UPPA_logo-rose.png" />
            <link rel="stylesheet" href="styleChercheur.css" />
        </head>
        
        <body>
            <table border="1">
                <tr>
                    <th>#</th>
                    <th>title</th>
                    <th>spatial</th>
                    <th>coverage</th>
                    <th>type</th>
                    <th>date</th>
                    <th>creator</th>
                    <th>subject</th>
                    <th>douteux</th>
                    <th>ref biblio</th>
                    <th>description</th>
                </tr>
                <xsl:variable name="sep" select="', '"/>
                <xsl:for-each select="/notices/notice">
                    <xsl:sort select="title" order="ascending" />
                    <tr>
                        <td style="font-family:monospace;font-size:0.9em;">
                            <xsl:value-of select="position()" />
                        </td>
                        <td>
                            <xsl:value-of select="title" />
                        </td>
                        <td>
                            <xsl:for-each select="spatial">
                                <xsl:if test="position() > 1"><xsl:copy-of select="$sep"/></xsl:if>
                                <xsl:value-of select="."/>
                            </xsl:for-each>
                        </td>
                        <td>
                            <xsl:for-each select="coverage">
                                <xsl:if test="position() > 1"><xsl:copy-of select="$sep"/></xsl:if>
                                <xsl:value-of select="."/>
                            </xsl:for-each>
                        </td>
                        <td>
                            <xsl:for-each select="type">
                                <xsl:if test="position() > 1"><xsl:copy-of select="$sep"/></xsl:if>
                                <xsl:value-of select="."/>
                            </xsl:for-each>
                        </td>
                        <td>
                            <xsl:for-each select="date">
                                <xsl:if test="position() > 1"><xsl:copy-of select="$sep"/></xsl:if>
                                <xsl:value-of select="."/>
                            </xsl:for-each>
                        </td>
                        <td>
                            <xsl:for-each select="creator">
                                <xsl:if test="position() > 1"><xsl:copy-of select="$sep"/></xsl:if>
                                <xsl:value-of select="."/>
                            </xsl:for-each>
                        </td>
                        <td>
                            <xsl:for-each select="subject">
                                <xsl:if test="position() > 1"><xsl:copy-of select="$sep"/></xsl:if>
                                <xsl:value-of select="."/>
                            </xsl:for-each>
                        </td>
                        <td>
                            <xsl:for-each select="douteux">
                                <xsl:if test="position() > 1"><xsl:copy-of select="$sep"/></xsl:if>
                                <xsl:value-of select="."/>
                            </xsl:for-each>
                        </td>
                        <td>
                            <xsl:for-each select="ref">
                                <xsl:if test="position() > 1"><xsl:copy-of select="$sep"/></xsl:if>
                                <xsl:value-of select="."/>
                            </xsl:for-each>
                        </td>
                        <td>
                            <xsl:value-of select="description" />
                        </td>
                    </tr>
                </xsl:for-each>
            </table>
        
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
                
```

Copier
:::

------------------------------------------------------------------------

à noter :

-   
-   
-   
-   
:::
:::

::: {#csv .section .etape}
### + - `csv` (en cours)

::: {#dis-csv style="display: none;"}
-   
:::
:::

::: {#openRefine .section .etape}
### + - `OpenRefine`{.logiciel} (à venir)

::: {#dis-openRefine style="display: none;"}
-   
:::
:::

::: {#omekaS-1 .section .etape}
### + - `OmekaS`{.logiciel} - Import des notices (en cours)

::: {#dis-omekaS-1 style="display: none;"}
Installation des plugins (Yann Foucauld).

-   
-   
-   
-   

Création du Site.

Création du template `monument-blot`.
:::
:::

::: {#tropy .section .etape}
### + - `Tropy`{.logiciel} (en cours)

::: {#dis-tropy style="display: none;"}
-   
:::
:::

::: {#omekaS-2 .section .etape}
### + - `OmekaS`{.logiciel} - Import des images (à venir)

::: {#dis-omekaS-2 style="display: none;"}
-   
:::
:::

::: {#xxx .section .etape}
### + - XXX

::: {#dis-xxx style="display: none;"}
-   
-   
-   
-   
:::
:::
