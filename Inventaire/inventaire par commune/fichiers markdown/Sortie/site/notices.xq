for $i in //*:section[@class="monument"]

let $title := string-join(
  for $z in $i/h3//text()
  return
  $z, ' ')

let $spatial := for $a in $i//*:span[@class="spatial"]
return <spatial>"{$a/text()}"</spatial>

let $coverage := for $b in $i//*:span[@class="coverage"]
return <coverage>"{$b/text()}"</coverage>

let $type := for $c in $i//*:span[@class="type"]
return <type>"{$c/text()}"</type>

let $date := for $d in $i//*:span[@class="date"]
return <date>"{$d/text()}"</date>

let $creator := for $e in $i//*:span[@class="creator"]
return <creator>"{$e/text()}"</creator>

let $subject := for $f in $i//*:span[@class="subject"]
return <subject>"{$f/text()}"</subject>

let $douteux := for $g in $i//*:span[@class="douteux"]
(: return <douteux>{$g/text()}</douteux> :)
return <douteux>oui</douteux>

let $ref := for $h in $i//*:span[@class="citation"]
return <ref>"{$h/@data-cites/string()}"</ref>

let $description := <description><cdata>{$i}</cdata></description>

return <notice><title>{$title}</title>{$spatial}{$coverage}{$type}{$date}{$creator}{$subject}{$douteux}{$ref}{$description}</notice>