---
title: Jacques Blot - Inventaire de monuments protohistoriques en pays basque
author: Jacques Blot
date: 2021-02-03
keywords:
  - Archéologie    
  - Monuments protohistoriques
  - Pyrénées-Atlantiques
  - Pays-Basque
  - Tumulus
  - Chromlech
  - Tertre
editor:
  - name: Julien Rabaud
  - name: Gaëlle Chancerel
  - name: Marina Lloancy
publisher: UPPA

---

# Inventaire de monuments protohistoriques en pays basque {.tit .unlisted}
## par Jacques Blot {.tit .unlisted}

# Ahaxe

<section class="monument">

### [Ahaxe]{.spatial} - [Bilgotza Mendebalde]{.coverage} 1 - [Tumulus-cromlech]{.type}

#### Localisation

Altitude : 720m.

On le trouve à 500m à l'Ouest-Nord-Ouest de *Bilgotza Mendebalde 2 - Cromlech* et *Bilgotza Mendebalde 3 - Cromlech*, sur une petite éminence naturelle, à quelques mètres au Sud de la piste.

#### Description

On distingue nettement un tumulus d'environ une dizaine de mètres de diamètre, et 0,40 à 050m de hauteur; sa périphérie semble bien être délimitée par un péristalithe constitué de nombreuses pierres plus ou moins bien visibles. À environ 1m à l'Est-Sud-Est du centre géométrique, on peut voir 3 pierres de volume différent, la plus volumineuse affecte grossièrement la forme d'un **T** ; elle mesure 1,40m de long et 1,10m au niveau de sa « barre transversale ». Ce qui est très curieux et que nous avons découvert lors de notre passage (le 27 décembre 2010) c'est une grande structure rectangulaire qui prend appui sur cette grosse pierre dans son angle Sud-Ouest. Le tracé est constitué par de nombreux petits blocs de pierre bien visibles au ras du sol, qui délimitent un rectangle de 7,20m de long et 3m de large, orienté Nord-Nord-Est Sud-Sud-Ouest. Le sol est parfaitement plat à l'intérieur de cette surface qui ne ressemble en rien aux vestiges d'une chambre funéraire ; nous n'avons, à l'heure actuelle, aucune interprétation à proposer.

#### Historique

Tumulus-cromlech découvert par [A. Martinez Manteca]{.creator} le [12 décembre 2010]{.date}.

</section>

<section class="monument">

### [Ahaxe]{.spatial} - [Bilgotza Mendebalde]{.coverage} 2 - [Cromlech]{.type}

#### Localisation

Il est à 12m au Sud-Ouest de *Bilgotza Mendebalde 1 - Tumulus-cromlech*, sur terrain plat.

#### Description

Cercle de 10m de diamètre environ, délimité par une vingtaine de pierres dont seule la douzaine du secteur sud sont très nettement visibles.

#### Historique

Monument découvert le [12 décembre 2010]{.date} par [A. Martinez Manteca]{.creator}.

</section>

<section class="monument">

### [Ahaxe]{.spatial} - [Bilgotza Mendebalde]{.coverage} 3 - [Cromlech]{.type}

#### Localisation

Situé à 5m au Sud-Est de *Bilgotza Mendebalde 2 - Cromlech*.

#### Description

Cinq pierres, au ras du sol, semblent bien faire partie d'un cercle de 6m de diamètre environ.

#### Historique

Monument découvert par [A. Martinez Manteca]{.creator} le [12 décembre 2010]{.date}.

</section>

<section class="monument">

### [Ahaxe]{.spatial} - [Bilgotza Mendebalde]{.coverage} 4 - [Cromlech]{.type}

#### Localisation

Il est tangent au Nord Nord-Ouest, au tumulus-cromlech du même nom, *Bilgotza Mendebalde 1 - Tumulus-cromlech*.

#### Description

Une vingtaine de pierres, au ras du sol, délimitent un cercle de près de 6mètres de diamètre. Il semblerait que deux pierres particulièrement visibles appartenant au péristalithe du tumulus-cromlech, soient aussi communes à ce cromlech.

#### Historique

Monument découvert par [Blot, J.]{.creator} le [27 décembre 2010]{.date}.

</section>

<section class="monument">

### [Ahaxe]{.spatial} - [Bilgotza Mendebalde]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 715m.

Il est situé à 6m au Nord-Est de la piste.

#### Description

Petit tumulus, peu visible, de terre et de petites pierre mesurant 3m de diamètre et 0,30m de haut.

#### Historique

Tumulus découvert en [février 2011]{.date} par [A. Martnez]{.creator}.

</section>

<section class="monument">

### [Ahaxe]{.spatial} - [Bilgotza Mendebalde]{.coverage} 2 - [Tumulus]{.type}

#### Localisation

Coordonnées : situé à 15m au Sud de *Bilgotza Mendebalde - Tumulus*, de l'autre côté de la piste.

#### Description

Petit tumulus peu visible, de terre et de petites pierres, mesurant 2,10m de diamètre et quelques centimètres de haut.

#### Historique

Tumulus découvert par le [groupe Hilharriak]{.creator} en [février 2011]{.date}.

</section>

<section class="monument">

### [Ahaxe]{.spatial} - [Bilgotza Mendebalde]{.coverage} 3 - [Tumulus]{.type}

#### Localisation

À 23m à l'Ouest de *Bilgotza Mendebalde - Tumulus*, en plein milieu de la piste

#### Description

Petit tumulus de faible hauteur, mesurant prés de 3m de diamètre, constitué de terre et de pierres.

#### Historique

Tumulus découvert par [Blot J.]{.creator} en [mars 2012]{.date}.

</section>

<section class="monument">

### [Ahaxe]{.spatial} - [Buluntza]{.coverage} 1 - [Tumulus]{.type}

Ce monument est à différencier du tertre d'habitat du même nom, qui se trouve à 60m à l'Ouest-Nord-Ouest du *dolmen de Buluntza*.

#### Localisation

Altitude : 724m.

Il est situé au sommet du ressaut qui domine la descente menant au col de Buluntza quand on vient de Bilgotza, et à une centaine de mètres au Sud-Ouest des monuments de Bilgotza Mendebalde.

#### Description

Tumulus pierreux en forme de galette aplatie, d'environ 2,50m de diamètre et 0,30m de haut. Une quinzaine de pierres, certaines atteignant 0,30m de long, sont visibles ; peut-être y aurait-il une ébauche de péristalithe (?).

#### Historique

Monument découvert par [J. Blot]{.creator} le [27 décembre 2010]{.date}.

</section>

<section class="monument">

### [Ahaxe]{.spatial} - [Buluntza]{.coverage} 2 - [Tumulus]{.type}

#### Localisation

Carte IGN 1346 Est Saint-Jean-Pied-de-Port.

Altitude : 672m.

On peut apercevoir ce tumulus à une soixantaine de mètres à l'Ouest-Nord-Ouest du *dolmen de Buluntza*.

#### Description

Tumulus ovalaire d'environ une quinzaine de mètres de long et 0,80m de haut. Ces caractéristiques nous évoquent un tertre d'habitat ou un tumulus funéraire dolménique dont la [chambre]{.subject} serait détruite.

#### Historique

Monument découvert par [J. Blot]{.creator} en [1975]{.date} et publié dans [@blotArcheologieMontagneBasque1993, p.164].

</section>

<section class="monument">

### [Ahaxe]{.spatial} - [Burguista]{.coverage} - [Dolmen]{.type}

#### Localisation

Altitude : 530m.

Ce monument est situé sur une croupe orientée Sud-Nord, qui se détache du massif dénommé Handiague, au Sud. Il est situé dans un petit ensellement d'où l'on a une vue magnifique sur le début de la vallée de l'Hergaray, à l'Ouest.

#### Description

Il s'agit d'un dolmen de montagne, aux dimensions modestes, ce qui s'explique aisément si l'on considère sa difficulté d'accès, comparée aux dolmens de Buluntza, Armiague etc. On note tout d'abord un tumulus pierreux de 9m de diamètre et environ 0,50m de hauteur.

Les éléments constitutifs en sont des blocs de [grès triasique]{.subject} ou de poudingue, de la taille d'un pavé, récoltés dans les environs immédiats qui sont riches en pointements rocheux.

Une dépression centrale, circulaire, de 4m de diamètre et 0,30m de profondeur environ, signe d'une fouille clandestine ancienne, laisse apparaître une importante dalle de [grès triasique rose]{.subject}, elle aussi de provenance proche. On peut très probablement la considérer comme la [dalle de couverture]{.subject} d'une [chambre funéraire]{.subject} dont les montants ne sont pas visibles dans l'état actuel. Cette dalle de grès, orientée dans le sens EO., affecte grossièrement la forme d'un parallélépipède allongé. Elle mesure 2,28m de long. Sa base Est, arrondi, mesure 1,13m de large. Dans sa partie la plus étroite, cette dalle mesure 1,23m et dans sa partie la plus large 1,34 m. Son sommet Ouest, rétréci ne mesure plus que 0,50m.

Son épaisseur variable est maximum à l'extrémité Ouest (0,33m) et bien moindre sur les côtés ou à sa base Est (entre 0,11m et 0,18m). Il semblerait que l'on puisse noter quelques traces d'épannelage (?) de part et d'autre de la région Ouest et peut être à la partie médiane du bord Sud.

#### Historique

Ce monument nous a été signalé par [C. Chauchat]{.creator} en [mai 2012]{.date}. À notre visite en mars 2020, nous avons constaté que ce monument avait fait l'objet d'une intervention sommaire, dont l'auteur, *véritable amateur*, n'a même pas respecté la règle de base de remise en état des lieux (abandon sur place des terres enlevées, des bâches de protection etc.)

</section>

<section class="monument">

### [Ahaxe]{.spatial} - [Handiague]{.coverage} 1 - [Cromlech]{.type}

#### Localisation

Altitude : 620m.

Il est situé à l'extrême Ouest de la longue crête d'Handiague, sur un terrain en pente très douce vers le Nord-Est. Terrain encombré d'une végétation drue (fougères, touyas), à environ 5m au Nord-Est de la piste qui parcourt la crête.

#### Description

Cercle de 3,30m de diamètre, délimité par 16 blocs bien visibles, dont 14 de poudingue et 2 de [grés]{.subject} local ; au centre trois autres dalles de grés et un petit bloc de poudingue sont visibles sans qu'on puisse dire qu'ils forment une [ciste]{.subject}.

#### Historique

Monument découvert par [I. Gaztelu]{.creator}, [A. Martinez]{.creator} et [L. Millan]{.creator} en [janvier 2013]{.date}.

</section>

<section class="monument">

### [Ahaxe]{.spatial} - [Handiague]{.coverage} 6 - [Cromlech]{.type}

#### Localisation

Altitude : 600m.

Situé plus au Nord que *Handiague 1 - Cromlech*, sur cette même crête, mais un peu plus bas.

#### Description

Avant son dégagement (F. Meyrat), ce monument était peu visible, encombré de végétation avec un houx poussant en son milieu. Il est adossé, à l'Ouest, à un important bloc rocheux. Cromlech légèrement tumulaire (0,20m de haut), il semble présenter deux cercles : un cercle central fait de 8 pierres et ayant 2,20m de diamètre, et un extérieur de 3,30m de diamètre et 9 pierres. Toutes ces pierres ont des volumes variables, allant du pavé au volume de 2 ballons de football.

#### Historique

Monument découvert par le [groupe Hillariak]{.creator} en [janvier 2013]{.date}.

</section>

# Aincille

<section class="monument">

### [Aincille]{.spatial} - [Goyhenetchea]{.coverage} 1 - [Tumulus]{.type}

#### Localisation

Altitude : 417m.

On trouve ce tumulus au sommet de la colline qui surplombe, à l'Ouest, la maison Goyhenetchea.

#### Description

Erigé sur terrain plat, ce tumulus de terre, de forme circulaire, mesure 9m de diamètre et 0,80m de haut.

#### Historique

Tumulus découvert en [janvier 2011]{.date} par [J. Blot]{.creator}.

</section>

<section class="monument">

### [Aincille]{.spatial} - [Goyhenetchea]{.coverage} 2 - [Tumulus]{.type}

#### Localisation

À 20m à l'Ouest Sud-Ouest de *Goyhenechea 1 - Tumulus*.

#### Description

Tumulus circulaire, mixte, de terre et de pierres, mesurant 5m de diamètre et 0,30m à 0,40m de haut.

#### Historique

Tumulus découvert par [J. Blot]{.creator} en [janvier 2011]{.date}.

</section>

<section class="monument">

### [Aincille]{.spatial} - [Goyhenetchea]{.coverage} 3 - [Tumulus]{.type}

#### Localisation

Situé à 20m au Sud de *Goyhenetchea 2 - Tumulus*.

#### Description

Tumulus mixte, circulaire, mesurant 3,50m de diamètre et 0,20m de haut.

#### Historique

Tumulus découvert par [J. Blot]{.creator} en [janvier 2011]{.date}.

</section>

<section class="monument">

### [Aincille]{.spatial} - [Handiague]{.coverage} 1 - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 700m.

Il est situé à l'extrémité Est de la longue crête d'Handiague, un peu décentré vers le Sud par rapport au sommet, érigé sur un terrain en légère pente vers l'Est. Il est à 80m à l'Ouest d'une piste pastorale et à 70m au Nord d'une seconde, carrossable, perpendiculaire à la première.

#### Description

Tumulus de 6m de diamètre et 0,40m de haut à l'Ouest, présentant une dépression centrale de cinq à dix centimètres de profondeur. Son asymétrie, et le terrain en pente semblent évoquer un tertre d'habitat, quoique les points d'eau soient assez éloignés.

#### Historique

Tertre découvert par [Meyrat F.]{.creator} en [août 2012]{.date}.

</section>

<section class="monument">

### [Aincille]{.spatial} - [Handiague]{.coverage} 2 - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 730m.

Il est situé à la naissance d'un petit ruisseau sur le flanc Nord du plus haut sommet de la crête d'Handiague (761m d'altitude), à proximité de la piste qui y monte.

#### Description

Tertre circulaire de près de 4m de diamètre et 0,80m de haut, constitué de terre.

#### Historique

Tertre découvert par [F. Meyrat]{.creator} en [janvier 2013]{.date}.

</section>

<section class="monument">

### [Aincille]{.spatial} - [Handiague]{.coverage} 2 - [Cromlech]{.type} [(?)]{.douteux}

#### Localisation

Altitude : 480m.

Les deux cromlechs *C2* et *C3 (?)* sont quasi tangents et situés à l'extrémité d'un promontoire qui apparaît au flanc Ouest de la crête d'Handiague.

#### Description

Petit cromlech de 3,50m de diamètre ; 4 pierres de faible volume sont visibles dans la moitié Nord, et 1 autre au Sud-Ouest. Légère dépression centrale.

#### Historique

Monument trouvé par le [groupe Hillarriak]{.creator} en [janvier 2013]{.date}.

</section>

<section class="monument">

### [Aincille]{.spatial} - [Handiague]{.coverage} 3 - [Cromlech]{.type} [(?)]{.douteux}

#### Localisation

Distant d'une trentaine de centimètres au Nord-Ouest de *Handiague 2 - Cromlech (?)*.

#### Description

9 pierres de très faible volume, au ras du sol, sont visibles sur un demi-cercle qui serait la moitié Nord du cromlech. Légère dépression visible au centre.

#### Historique

Monument trouvé par le [groupe Hillarriak]{.creator} en [janvier 2013]{.date}.

</section>

<section class="monument">

### [Aincille]{.spatial} - [Handiague]{.coverage} 4 - [Cromlech]{.type}

#### Localisation

Altitude : 533m.

Toujours sur le flanc Ouest de la crête Handiague, mais sur un promontoire plus en altitude que celui qui portent *Handiague 2 - Cromlech (?)* et *Handiague 3 - Cromlech (?)*.

#### Description

Ces deux monuments sont eux aussi tangents.

Ce monument se rattache aux cromlechs surélevés ou aux « tumulus-cromlechs ». Tumulus de 0,30m de haut environ avec une dépression centrale peu marquée. Au centre une « structure » formée de 3 pierres d'un volume notable (0,90m x 0,40m en moyenne) plus 3 ou 4 autres nettement plus petites pourraient représenter une [« ciste » centrale]{.subject}. À la périphérie du tumulus une dizaine de pierres de volume modeste semblent délimiter un cercle de 6m de diamètre. Etant proche d'une rupture de pente, des phénomènes de [soutirage]{.subject} semblent avoir eu lieu dans le secteur Sud-Est, par contre le secteur Nord-Est paraît avoir bénéficié d'un [empierrage de soutènement]{.subject}.

#### Historique

Monument découvert par le [groupe Hillarriak]{.creator} en [janvier 2013]{.date}.

</section>

<section class="monument">

### [Aincille]{.spatial} - [Handiague]{.coverage} 5 - [Cromlech]{.type}

#### Localisation

À quelques centimètres au Nord-Ouest de *Handiague 4 - Cromlech*.

#### Description

Monument très légèrement tumulaire, et petite dépression centrale, de 4m de diamètre. Une vingtaine de pierres sont visibles en périphérie, au ras du sol, plus nombreuses dans la moitié Est, la moitié Ouest est le siège de soutirage dû à la proximité de la rupture de pente.

#### Historique

Monument découvert par le [groupe Hillarriak]{.creator} en [janvier 2013]{.date}.

</section>

<section class="monument">

### [Aincille]{.spatial} - [Handiague]{.coverage} 8 - [Cromlech]{.type}

#### Localisation

Altitude : 690m.

Situé sur un plateau, avant la rupture de pente, il est à une quinzaine de mètres à l'Est de la piste qui descend vers le niveau inférieur de la crête d'Handiague.

#### Description

Cercle de 3m de diamètre environ, délimité par 4 pierres dont 3 blocs de poudingue très visibles (le plus grand, au Nord, mesure 0,90m x 0,90m et 0,40m de haut).

#### Historique

Monument trouvé par [Blot J.]{.creator} en [janvier 2013]{.date}.

</section>

<section class="monument">

### [Aincille]{.spatial} - [Handiague]{.coverage} 7 - [Cromlech]{.type} [(?)]{.douteux}

#### Localisation

Altitude : 623m.

#### Description

« Monument » très curieux constitué de grandes pierres, plus ou moins espacées, (pouvant atteindre 1,80m de haut) dont certaines paraissent naturellement en place, d'autre non, délimitant un cercle (approximatif) de 10m de diamètre, dont le centre est parfaitement plat et dépourvu de pierres. Est-ce un espace parfaitement naturel, ou aménagé (parc pastoral ? espace funéraire, lieu de réunion ?).

#### Historique

Monument signalé par le [groupe Hillarriak]{.creator} en [janvier 2013]{.date}.

</section>

# Alçay-Alçabéhéty-Sunharette

<section class="monument">

### [Alçay-Alçabéhéty-Sunharette]{.spatial} - [Albinze]{.coverage} - [Cromlech]{.type}

#### Localisation

Altitude : 1110m.

Il se trouve au Nord, et un peu en contre-bas du *TC d'Ugatze* que nous avons fouillé en 1974, de l'autre côté du virage de la route.

#### Description

Petit cercle de 3m de diamètre, légèrement en relief, délimité par 7 pierres au ras du sol.

#### Historique

Monument découvert par [L. Millan]{.creator} en [mai 1989]{.date}.

</section>

<section class="monument">

### [Alçay-Alçabéhéty-Sunharette]{.spatial} - [Eltzegagne]{.coverage} - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 900m.

Il est à quelques dizaines de mètres à l'Ouest de la piste qui se rend du cayolar Eltzegagne au petit col situé plus à l'Est.

#### Description

Tertre de terre de forme classique, circulaire, asymétrique, mesurant 7m de diamètre et 1m de haut dans sa partie la plus marquée.

#### Historique

Tertre découvert par [Blot J.]{.creator} en [mai 2013]{.date}.

</section>

<section class="monument">

### [Alçay-Alçabéhéty-Sunharette]{.spatial} - [La Croix Garat]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 1039m.

#### Description

Contrairement à ce que nous avions décrit en 1979, il n'a pas été endommagé en 1974 comme nous l'avions écrit par erreur, il est intact. (Il s'agit plus d'un [Tumulus-cromlech]{.type}).

</section>

<section class="monument">

### [Alçay-Alçabéhéty-Sunharette]{.spatial} - [Pic des Vautours]{.coverage} 3 (Belhigagne) - [Tumulus]{.type}

#### Localisation

Altitude : 854m.

Il se trouve à une quarantaine de mètres à l'Est de la clôture qui sépare une prairie artificielle d'un terrain en friche. Sur le plat de cette prairie artificielle se trouvaient les 2 tumulus décrits et publiés par nous [@blotSouleSesVestiges1979, p.6] sous le nom de tumulus du Pic des vautours ou de Belhigagne. Ils ont été rasés par la création de la prairie.

#### Description

Petit tumulus de terre, de 5,20m environ de diamètre, et 0,40m de hauteur, aux contours un peu irréguliers.

#### Historique

Monument découvert par [Meyrat F.]{.creator} en [mai 2013]{.date}.

</section>

<section class="monument">

### [Alçay-Alçabéhéty-Sunharette]{.spatial} - [Ursoy]{.coverage} (groupe Ouest) - [Tertres d'habitat]{.type}

#### Localisation

Altitude : 916m.

Le groupe de 9 tertres d'habitat est situé au-dessus du chemin qui se rend au cayolar Uztarila, et qui reprend l'ancienne piste que nous empruntions en 1971.

#### Description

Ces 9 tertres de terre sont répartis sur une pente assez raide, qui borde au Sud un riu. Ils sont, comme d'habitude, asymétrique et dans l'ensemble bien conservés ; ils mesurent en moyenne 10m de diamètre et 1,50m de haut.

-   Le *TH 2* est à 20m au Nord du *TH 1*.
-   Le *TH 3* à 20m au Nord du n° 2.
-   Le *TH 4* est à 5m au Nord-Est du n° 3.
-   Le *TH 5* est à 7m au Sud-Est du n°4.
-   Le *TH 6* à 6m à l'Est du n° 5.
-   Le *TH 7* à 10m à l'Ouest du n° 4.
-   Le *TH 8* à 20m au Nord du n° 4.
-   Le *TH 9* est à 7m à l'Est du n° 8.

#### Historique

Tertres d'habitat vus par [Blot J.]{.creator} en [1971]{.date} et décrits succinctement.

</section>

<section class="monument">

### [Alçay-Alçabéhéty-Sunharette]{.spatial} - [Ursoy]{.coverage} (groupe Est 1) - [Tertres d'habitat]{.type}

#### Localisation

Altitude : 890m.

Les 3 tertres d'habitat sont situés à l'Est du chemin. Ils sont de moindre importance que *Ursoy (groupe Ouest) - Tertres d'habitat*, échelonnés sur la crête qui borde le ru au Sud.

#### Description

Erigés sur une pente douce, ils sont moins asymétriques que les précédents et de dimensions moindres.

#### Historique

Tertres d'habitat vus par [Blot J.]{.creator} en [1971]{.date} et décrits succinctement [@blotSouleSesVestiges1979, p.10].

</section>

<section class="monument">

### [Alçay-Alçabéhéty-Sunharette]{.spatial} - [Ursoy]{.coverage} (groupe Est 2) - [Tertres d'habitat]{.type}

#### Localisation

Altitude : 880m.

Les 4 tertres d'habitat sont situés au Nord de *Ursoy (groupe Est 1) - Tertres d'habitat*, sur l'autre rive du ru.

#### Description

Tertres de terre érigés sur une pente douce, de taille variable, les plus nets étant les *TH 1*, *2* et *3*. Ils mesurent en moyenne 7m de diamètre et 1,20m de haut.

Le *TH2* est à 25m au Sud du *TH 1* et le *TH 3* à 25m à l'Ouest du n° 1.

#### Historique

Tertres d'habitat vus par [Blot J.]{.creator} en [1971]{.date} et décrits succinctement.

</section>

# Aldude

<section class="monument">

### [Aldude]{.spatial} - [Arguibel]{.coverage} (ou Harguibel) - [Dolmen]{.type}

### Localisation

Carte 1346 Ouest Saint-Etienne-de-Baïgorry.

Altitude : 952m.

Il est situé au Sud du mont Arguibel et à 12m au Sud-Est de la BF 126.

#### Description

Il n'y a pas de tumulus visible. On note, émergeant du sol une dalle horizontale, disposée à plat, grossièrement rectangulaire, orientée Nord-Ouest Sud-Est, mesurant 1,70m de long et 0,90m de large, qui pourrait être la table (ou couvercle). Au Nord, une deuxième dalle est visible, (dalle de « chevet » ?) qui n'est pas perpendiculaire à la première, mais orientée Est-Ouest ; elle mesure 1m de long, 0,30m de haut et 0,40m d'épaisseur. L'ensemble évoque un coffre dolménique peut-être encore vierge.

#### Historique

Dolmen découvert en [août 1976]{.date}.

</section>

<section class="monument">

### [Aldude]{.spatial} - [Arguibel]{.coverage} - [Tertre cayolar]{.type} ([?]{.douteux})

Enfin, à mi-pente, sur le côté Sud du groupe de cromlechs, on note un relief de terre, sur terrain incliné, avec une structure en grosses pierres au centre, évoquant un [tertre d'habitat]{.type} sur lequel aurait été ensuite érigé un abri (cayolar ?) très primitif.

</section>

<section class="monument">

### [Aldude]{.spatial} - [Arguibel]{.coverage} 6 - [Tertre d'habitat]{.type}

#### Localisation

Carte 1346 Ouest Saint-Etienne-de-Baïgorry.

Altitude : 930m.

Il est situé dans le col, à 30m à l'Est du monolithe d'Arguibel et domine une pente abrupte vers l'Est.

#### Description

Tumulus terreux de 9m de diamètre et 0,80m de haut, présentant une dépression centrale, résultat probable d'une fouille ancienne. Un éboulement de la pente a amputé le secteur Est de ce monument.

#### Historique

Monument découvert en [août 1976]{.date}.

</section>

<section class="monument">

### [Aldude]{.spatial} - [Belaun]{.coverage} Ouest n°1 - [Tumulus-cromlech]{.type} ([?]{.douteux})

#### Localisation

Altitude : 810m.

Sur un replat au flanc du mont Eyharce, dominant le col de Belaun.

#### Description

On note un léger relief circulaire, de 5,50m de diamètre et de 0,30m de haut, délimité par 8 pierres de calibre variés ; deux d'entre elles en secteur Nord, semblent avoir été arrachées à l'extérieur du cercle, et mesurent respectivement 0,50m et 0,70m dans leur plus grand axe ; monument douteux.

#### Historique

Monument découvert par [F. Meyrat]{.creator} en [avril 2010]{.date}.

</section>

<section class="monument">

### [Aldude]{.spatial} - [Belaun]{.coverage} Ouest n°2 - [Tumulus-cromlech]{.type} ([?]{.douteux})

#### Localisation

À 25m à l'Est de *Belaun ouest n°1 - Tumulus-cromlech (?)*.

#### Description

Structure ovalaire de 5,70m de long, suivant un axe Est-Ouest, et 3,70m de large, mesurant 0,30m à 0,40m de haut, constituée de pierres de calibre variés, surtout visibles dans la périphérie Sud. Monument douteux.

#### Historique

Monument découvert par [J. Blot]{.creator} en [avril 2010]{.date}.

</section>

<section class="monument">

### [Aldude]{.spatial} - [Belaun]{.coverage} Est - [Tumulus]{.type} ([?]{.douteux})

#### Localisation

Altitude : 760m.

#### Description

Tumulus de 3,60m de diamètre, et 0,40m de haut environ, constitué de petits blocs calcaires mobiles. Monument douteux.

#### Historique 

Monument découvert par [F. Meyrat]{.creator} en [avril 2010]{.date}.

</section>

<section class="monument">

### [Aldude]{.spatial} - [Berdaritz]{.coverage} - [Monolithe]{.type}

#### Localisation

Carte 1346 Ouest St-Etienne-de-Baïgorri.

Altitude : 690m.

Ce monolithe est situé au niveau des premiers mètres du flanc Nord-Ouest du mont Urrixka, couché sur un sol en légère pente ; il est à 60m au Nord Nord-Est de la BF 117. Comme le monolithe d'Arguibel, auquel il ressemble beaucoup [@blotMonolithesPaysBasque1983, p.23], il est à proximité (à 50m au Nord) d'une source. Il lui ressemble encore non seulement par sa position dans un col, mais aussi par son riche environnement archéologique. Enfin il est important de souligner qu'il n'y a, dans un vaste rayon de plusieurs dizaines de mètres, aucune pierre, aucun bloc rocheux d'éboulis sur cette pente. Cette grande dalle est d'autant plus remarquable dans sa solitude...

#### Description

Il s'agit d'une grande dalle polygonale de [grés]{.subject} [triasique]{.subject}, à sommet triangulaire, mesurant près de 5m dans son grand axe Sud-Est Nord-Ouest ; son sommet a été détaché, très probablement par l'action du gel. L'épaisseur moyenne est de 0,20m à 0,30m. L'extrémité Nord-Ouest est beaucoup plus mince que le reste de la dalle, et on remarque des traces d'épannelage (flêches), sur le bord arrondi, au Nord-Est, et sur le bord Sud-Est du sommet.

Enfin il existe un bloc rocheux massif enfoui dans le sol à proximité du sommet de ce monolithe, qui paraît « en place » et ne présente, semble-t-il, aucun rapport avec lui ; il est en particulier beaucoup plus épais.

#### Historique

Monument découvert en [janvier 2004]{.date}.

</section>

<section class="monument">

### [Aldude]{.spatial} - [Berdaritz]{.coverage} n°1 - [Cromlech]{.type} ([?]{.douteux})

#### Localisation

Altitude : 735m.

Il est à environ 80m à l'Est Nord-Est de la BF 118.

#### Description

On distingue un cercle d'environ 5,50m de diamètre, érigé sur un terrain en légère pente vers le Nord-Est. Il est matérialisé par un bourrelet de terrain contenant de nombreuses pierres, ceci étant particulièrement bien visible dans les secteurs Nord-Est et Sud. Le centre est le siège d'une dépression peu profonde. Monument douteux.

#### Historique

Monument trouvé en [2002]{.date} par [J. Capbodevilla]{.creator} et [I. Zabala]{.creator}.

</section>

<section class="monument">

### [Aldude]{.spatial} - [Berdaritz]{.coverage} n°2 - [Cromlech]{.type} ([?]{.douteux})

#### Localisation

À 30m à l'Ouest Sud-Ouest, du précédent.

#### Description

Une dizaine de pierres au ras du sol délimitent un cercle de 2,60m de diamètre contenant une pierre centrale. Monument douteux.

#### Historique 

Monument trouvé par [J. Blot]{.creator} en [avril 2010]{.date}.

</section>

<section class="monument">

### [Aldude]{.spatial} - [Berdaritz]{.coverage} n°3 - [Cromlech]{.type} ([?]{.douteux})

#### Localisation

Altitude : 732m.

Situé à 60m au Sud de la BF 118 et à 200m environ à l'Ouest du *Cromlech C2* [@blotInventaireMonumentsProtohistoriques2011, p. 3-4].

#### Description

Sur l'étendue plate et sans pierres apparentes de la lande environnante se détachent 5 pierres disposées en cercle sur un bourrelet de terrain circulaire de 4m de diamètre, entourant une légère dépression dans laquelle 3 autres blocs pierreux apparaissent.

Monument douteux, (T ?), comme les 2 autres cromlechs déjà décrits [@blotInventaireMonumentsProtohistoriques2011].

#### Historique

Monument découvert par [F. Meyrat]{.creator} en [mars 2014]{.date}.

</section>

<section class="monument">

### [Aldude]{.spatial} - [Berdaritz]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 685m.

Il est situé à 56m à l'Ouest de la BF 117.

#### Description

Ce monument, probable, dont il ne reste que la moitié, a en effet été amputé par la passage des engins agricoles utilisés pour mettre en valeur la prairie à au Nord de la clôture de barbelés qui passe au milieu de ce monument. Tumulus de terre semble-t-il, de 6m de diamètre et 0,45m de haut.

#### Historique

Monument découvert par [J. Blot]{.creator} en [avril 2010]{.date}.

</section>

<section class="monument">

### [Aldude]{.spatial} - [Eyharzeko lepoa]{.coverage} - [Dalle plantée]{.type} ([?]{.douteux})

#### Localisation

Située à 20m au Sud de la BF 125.

#### Description

Dalle verticale de [grés]{.subject} rose, orientée Sud Sud-Est, de 1m de long, 0,46m de haut et 0,23m d'épaisseur. Son bord supérieur présente des traces très évidentes d'épannelage. Pas de traces de tumulus ; s'agit-t-il d'un vestige dolmenique ?

#### Historique

Dalle découverte par [J. Blot]{.creator} en [avril 2010]{.date}.

</section>

<section class="monument">

### [Aldude]{.spatial} - [Eyharcé]{.coverage} (col de) - [Dalle]{.type} ou [Dolmen]{.type} ([?]{.douteux})

#### Localisation

Altitude : 850m.

Elle est située à 25m au Sud de la BF 125 et à 30m au Nord de la terminaison de la route sur le col.

#### Description 

Dalle verticale de grés rose, orientée Sud Sud-Est, de 1m de long, 0,46m de haut et 0,23m d'épaisseur. La totalité de son bord supérieur présente des traces très évidentes d'épannelage. On ne note pas la présence d'autres dalles mais il semble qu'on puisse distinguer un léger relief tumulaire, circulaire, de 8m de diamètre environ et 0,15 à 0,20m de haut, au centre duquel apparaît la dalle. Nous tenons pour très probable que cet ensemble soit un vestige de coffre dolménique assez semblable au monument *Eyharcé (col de) - Ciste*.

#### Historique 

Dalle revue en mai 2018 mais déjà repérée par nous ([J. Blot]{.creator}), en [avril 2010]{.date} et publiée en 2011 sous la rubrique « cas particulier » : [@blotInventaireMonumentsProtohistoriques2011, p. 5].

</section>

<section class="monument">

### [Aldude]{.spatial} - [Eyharcé]{.coverage} (col de) - [Ciste]{.type}

#### Localisation

Altitude : 851m.

Monument situé à 50m à l'Ouest Sud-Ouest de la BF 125 ; il est, semble-t-il, tangent à la ligne frontière.

#### Description 

Monument très peu visible, au ras du sol. On distingue tout d'abord 2 dalles parallèles au ras du sol, délimitant la chambre funéraire orientée pratiquement plein Est. La dalle Nord mesure 0,85m de long et entre 0,05m et 0,10m d'épaisseur ; la dalle Sud mesure 1,04m de long, et entre 0,2m et 0,5m d'épaisseur. Ces deux dalles paraissent enfoncées bien verticalement dans le sol ; il n'y en a pas d'autres visibles à l'Est ou à l'Ouest, ni de dalle de couverture.

On note quelques rares pierres (7 au total), elles aussi au ras du sol, en forme de galets, situées à proximité de la chambre funéraire et qui pourraient faire partie d'un tumulus mixte de terre et de pierres, ou d'un éventuel péristalithe. On notera particulièrement 2 pierres au Sud, distantes de 2,20m environ de la [chambre funéraire]{.subject}, une autre à l'Est, distante de 2,23m de la [chambre funéraire]{.subject} et une dernière à l'Ouest, distante de 2,15m ; ce qui pourrait faire envisager (dans le cas d'un vestige de péristalithe), un tumulus de 5m de diamètre environ, dimensions parfaitement compatible avec ce type de monument.

#### Historique 

Monument découvert par [J. Blot]{.creator} en [mai 2018]{.date}.

</section>

<section class="monument">

### [Aldude]{.spatial} - [Eyharcé]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 777m.

Il est situé à une dizaine de mètres à l'Est Nord-Est (à droite) de la route qui monte vers le col d'Eyharcé, et bien visible de celle-ci ; on est à 400m avant le dernier virage en épingle à cheveux qui la mène ensuite en direct au col d'Eyharcé. De l'autre côté de la route, monte, sur la gauche, une piste pastorale qui se rend directement au col d'Eyharcé.

#### Description

Tumulus terreux uniquement, en forme de galette aplatie, mesurant 6,20m de diamètre et 0,30m de haut ; on note au centre une légère dépression. Sa périphérie est aussi entourée d'une légère dépression, sans doute en rapport avec l'extraction des terres nécessaires à la confection de ce tumulus.

#### Historique

Tumulus découvert par [F. Meyrat]{.creator} en [juillet 2018]{.date}.

</section>

## [Aldude]{.spatial} - Les [cromlechs]{.type} d'[Arguibel]{.coverage}

#### Localisation

Carte 1346 Ouest Saint-Etienne-de-Baïgorri.

Altitude : 960m.

J.M. de Barandiaran [@barandiaranProspecionesExcavacionesPrehistoricas1962, p.13; @barandiaranHombrePrehistoricoPais1953, p.249] avait décrit sur une petite éminence à environ 130m au Nord-Ouest de la BF 126 et dominant au Nord-Ouest la ligne frontière deux cromlechs (communes d'Aldude et Baztan). Un premier cercle de 6m de diamètre, constitué de 11 pierres bien visibles (la plus haute mesure 0,90m de haut), et un second, à l'Est du précédent, de 2,20m de diamètre, entouré de 8 pierres. N'ayant pas retrouvé sur le terrain ces données, nous proposons la description suivante.

<section class="monument">

### [Aldude]{.spatial} - [Arguibel]{.coverage} 1 - [Cromlech]{.type}

Le plus à l'Est, mesure 6m de diamètre, est entouré de 15 pierres ; il pourrait correspondre au *n°1* de [J.M. de Barandiaran]{.creator}.

</section>

<section class="monument">

### [Aldude]{.spatial} - [Arguibel]{.coverage} 2 - [Cromlech]{.type}

Est tangent au Nord-Ouest du précédent.

Quatorze pierres délimitent un cercle de 6m de diamètre.

#### Historique

Monument découvert en [octobre 1972]{.date}.

</section>

<section class="monument">

### [Aldude]{.spatial} - [Arguibel]{.coverage} 3 - [Cromlech]{.type}

Existe à l'intérieur du n°2, tangent à lui en secteur Nord-Ouest. Il mesure 2m de diamètre, et est entouré de 10 pierres environ. Cette présence, très nette ici, d'un second cercle à l'intérieur d'un premier, est tout à fait exceptionnelle, pour ne pas dire unique, à notre connaissance.

#### Historique

Monument découvert en [août 1976]{.date}.

</section>

<section class="monument">

### [Aldude]{.spatial} - [Arguibel]{.coverage} 4 - [Cromlech]{.type}

Il est tangent au Nord-Ouest du n°2.

Une quinzaine de pierres délimitent un cercle de 2,50m de diamètre. Il est érigé sur un terrain en légère pente vers l'Ouest.

#### Historique

Monument découvert en [octobre 1972]{.date}.

</section>

<section class="monument">

### [Aldude]{.spatial} - [Arguibel]{.coverage} 5 - [Cromlech]{.type}

Monument modeste et fort dégradé, visible au Nord du n°1, et tangent à lui. Il mesure 3m de diamètre, et il serait délimité par 4 à 5 pierres.

</section>

<section class="monument">

### [Aldude]{.spatial} - [Arguibel]{.coverage} 6 - [Cromlech]{.type}

#### Localisation

Au flanc Nord de l'ensemble d'Arguibel, sur terrain incliné.

Altitude : 960m.

#### Description

Une douzaine (une quinzaine ?) de pierres de faibles dimensions paraissent délimiter une stucture circulaire de 3m de diamètre environ.

#### Historique

Monument découvert par [Blot J.]{.creator} en [mai 2013]{.date}.

</section>

<section class="monument">

### [Aldude]{.spatial} - [Arguibel]{.coverage} 7 - [Cromlech]{.type}

#### Localisation

Au flanc Nord Nord-Ouest de l'ensemble d'Arguibel, sur terrain incliné. Mêmes coordonnées que le groupe.

#### Description

7 à 8 pierres délimiteraient une structure circulaire de 2m environ de diamètre.

#### Historique

Monument découvert par [Blot J.]{.creator} en [mai 2013]{.date}.

</section>

<section class="monument">

### [Aldude]{.spatial} - [Arguibel]{.coverage} 8 - [Cromlech]{.type}

#### Localisation

Au flanc Nord du groupe, entre C6 et C7, sur terrain incliné.

#### Description

Une dizaine de pierres semblent délimiter une structure circulaire de 3m de diamètre environ.

#### Historique 

Monument découvert par [Blot J.]{.creator} en [mai 2013]{.date}.

</section>

<section class="monument">

### [Aldude]{.spatial} - [Arguibel]{.coverage} 9 - [Cromlech]{.type}

#### Localisation 

Altitude : 930m.

Il est situé à 20m au Sud Sud-Ouest de la BF 126 ; la ligne frontière est matérialisée par une barrière barbelée qui partage le monument selon un axe orienté sensiblement Nord-Sud, de telle sorte que les ¾ de ce dernier sont en territoire du Baztan, et le reste dans les Aldudes.

#### Description

On peut en estimer le diamètre à 8m environ, bien que le seul quart Nord-Ouest du monument soit visible, matérialisé par un arc de cercle de 5 pierres ; l'une d'elles est un véritable petit monolithe couché au sol, orienté plein Est, de 2,20m de long et 1,15m dans sa partie la plus large, et 0,20m d'épaisseur visible. Les 3 pierres au centre du monument font-elles partie d'une ciste ?

#### Historique

Monuments découverts en [août 1976]{.date}.

</section>

<section class="monument">

### [Aldude]{.spatial} - Les monuments de [Zaho]{.coverage}

Un ensemble de 4 monuments et un [monolithe]{.type}, localisés de part et d'autres de la frontière a été décrit par [J.M. de Barandiaran]{.creator} [@barandiaranCronicaPrehistoriaBaigorri1949, p.74]. Nous en donnons ici, très brièvement, les principales caractéristiques qui diffèrent cependant sensiblement de la description princeps, y ajoutant 4 nouveaux monuments.

-   *Zaho 1 - [Tumulus-cromlech]{.type}*

    Altitude : 997m.

    Mesure 10m de diamètre et possède une trentaine de pierres périphériques ; il est à 20m au Nord-Ouest de la ligne frontière matérialisée par des barbelés, en territoire du Baztan.

-   *Zaho 2 - Tumulus-cromlech*

    Il est situé à 20m à l'Est Sud-Est du premier, dans le territoire des Aldudes, et mesure 9m de diamètre avec 26 pierres périphériques visibles avant la fouille de sauvetage que nous avons effectué en août 1983 [@blotTumuluscromlechZahoII1989, p.49] ; la datation obtenue est la suivante : (Gif 6343) : mesure d'âge (BP) : 2640+-90, soit en date calibrée : 995-497 (BC).

-   *Zaho 3 - Tumulus-cromlech*

    Il est tangent au Sud du précédent, et mesure 13m de diamètre et possède une trentaine de pierres périphériques. Il est à environ 120m au Nord-Est de la BF 128.

-   *Zaho 4 - [Tumulus]{.type}*

    (Appelé dolmen par J.M. de Barandiaran), est situé à 100m au Sud du n°3 et à 20m au Sud-Est du monolithe ; il mesure 16m de diamètre et présente une importante excavation centrale.

Le monolithe de [grés]{.subject} rose, couché au sol, à grand axe orienté Sud-Ouest Nord-Est, mesure 4,90m de long, 1,30m de large et 0,50m d'épaisseur en moyenne.

Nous ajoutons aux monuments ci-dessus un groupe de **4 nouveaux**, découverts en [août 1974]{.date} ; nous décrivons en premier le :

-   *Zaho 5 - Tumulus*

    -   Localisation

        Altitude : 1000m.

        Le n°5 est tangent à l'Ouest du n°4.

    -   Description

        Tumulus mixte de 6m de diamètre et 0,30m de haut; on distingue bien 7 pierres à la périphérie - mais sans que l'on puisse parler de péristalithe - ainsi que 3 pierres centrales.

-   *Zaho 6 - Tumulus*

    -   Localisation

        Il est tangent à l'Ouest du n°5, et à 12m au Sud-Est du monolithe de Zaho.

    -   Description

        Tumulus de terre, de 5m de diamètre et 0,30m de haut ; on voit une pierre en secteur Nord-Ouest de sa périphérie.

-   *Zaho 7 - Tumulus*

    Aldudes (Baztan).

    -   Localisation

        Il est situé à 12m au Nord-Ouest de la BF 128.

    -   Description

        Tumulus de terre de 7m de diamètre et 0,30m de haut, avec une pierre périphérique visible en secteur Nord-Ouest.

-   *Zaho 8 - Tumulus-cromlech*

    -   Localisation

        Sur un petit replat qui jalonne la descente vers le Sud-Ouest en venant du monolithe. Ce monument est à 150m de la BF. 128 et à 20m à l'Est de la ligne frontière.

    -   Description

        Tumulus de 4m de diamètre et 0,40m de haut, délimité par 8 pierres, au ras du sol ; on distingue une très légère dépression centrale.

</section>

<section class="monument">

### [Aldude]{.spatial} - [Urrixka]{.coverage} 1 bis - [Dolmen]{.type} ([?]{.douteux})

#### Localisation

À 80m environ à l'Est de *Urrixka 2 - Dolmen*, plus en hauteur sur le flanc de la montagne.

#### Description

On peut voir un amas de pierraille circulaire, mais relativement peu fourni, d'environ 10m de diamètre pouvant évoquer un tumulus pierreux dolménique assez semblable à ceux des monuments de cette montagne ; au centre, deux dalles importantes, horizontales et en parties enfouies dans le sol, renforcent cette similitude. Toutefois le terrain en pente et la présence en amont d'un filon rocheux naturel délité posent des problèmes...

#### Historique

Monument découvert par [Blot. J.]{.creator} en [juin 2010]{.date}. (Serait inscrit au Patrimonio de Navarra, en 2007, selon L. Millan)

</section>

<section class="monument">

### [Aldude]{.spatial} - [Urrixka]{.coverage} 2 - [Dolmen]{.type}

#### Localisation

Carte 1346 Ouest Saint-Etienne-de-Baïgorri.

Altitude : 870m.

Il est situé au flanc Sud-Est du mont Urrixka ; rappelons que J.M. de Barandiaran avait décrit le dolmen n°1 au flanc Sud-Ouest de ce mont, un peu au-dessus du col de Berdaritz, à 680m d'altitude, et à quelques mètres de ce n°1, un peu plus en altitude et à l'Est, le dolmen dit : *Jeneralen tomba*.

*Urrixka 2* est tout à fait en bordure et à droite du chemin qui gravit cette colline, sur un replat très faiblement incliné vers le Sud-Est.

#### Description

Tumulus pierreux de 8m à 9m de diamètre et 0,40m de haut, constitué de gros blocs amoncelés à sec. Au centre une dépression signale la chambre funéraire qui devait être, à l'origine, quadrangulaire, allongée, ouverte à l'Est Sud-Est. Il n'en reste qu'un montant de 1,65m de long et 0,45m de haut, incliné légèrement vers le Sud ; sur ce support s'appuient deux fragments de la table du [dolmen]{.subject}, brisée en 3 parties, dont une repose librement sur le côté Ouest du tumulus.

#### Historique

Dolmen découvert en [octobre 1986]{.date}.

</section>

<section class="monument">

### [Aldude]{.spatial} - [Urrixka]{.coverage} 4 - [Dolmen]{.type}

#### Localisation

Altitude : 830m.

Pour le trouver, le plus simple est de partir de *Urrixka 2 - Dolmen*. Pour atteindre ce dernier, suivre, à partir du col de Berdaritz, la piste qui monte vers le sommet d'Urrixka, puis prendre, à environ 750m d'altitude, un bel embranchement vers la droite, qui après 800m de parcours en montée douce, arrive sur un replat, dans un sous-bois. *Urrixka 2 - Dolmen* est immédiatement visible sur la droite, érigé sur un sol en légère pente vers l'Est. On note à ce niveau, une bifurcation de la piste, dont une branche redescend vers l'Est. Parcourir environ 70m jusqu'au niveau d'un éboulis de gros rocs remaniés au bulldozer ; elle se dirige vers le Nord-Est sur une centaine de mètres : on arrive à un début de pente vers le Nord, là où se trouve le dolmen.

#### Description

Vaste tumulus pierreux (en grés triasique, comme tous les monuments de cette montagne) de 10m de diamètre et 0,50m de haut, érigé sur un terrain en légère pente vers le Nord. Au centre, trois vastes fragments de dalle paraissent pouvoir être considérés comme les vestiges de la dalle de couverture.

Ils mesurent respectivement : 1,07m de long, 1,36m de large et 0,38m d'épaisseur. Le fragment voisin, à l'Est, mesure 1,52m de long, 0,84m de large et 0,33m d'épaisseur ; enfin le troisième fragment, situé contre le précédent et à son extrémité Nord, mesure 0,81m de long, 0,70m de large et 0,24m d'épaisseur.

Trois autres dalles semblent pouvoir être rattachées aux montants de la [chambre funéraire]{.subject} qui est orientée Est-Ouest. La première dalle, située à l'extrémité Sud-Ouest du fragment de dalle de couverture ci-dessus décrit en premier, est verticalement enfoncée dans le sol selon un axe Est-Ouest ; elle mesure 1,43m de long, 0,13m d'épaisseur et environ 0,30m de haut. On peut distinguer, au Nord de celle-ci, une deuxième dalle, quasiment couchée sur le sol et qui paraît bien en avoir été arrachée ; elle mesure 1,30m de long à sa base, 0,60m au sommet et 0,20m d'épaisseur. Son orientation et ses dimensions nous la font interpréter comme un des montants Nord de la chambre funéraire. Il en est de même pour la dalle suivante, située dans le prolongement et à l'Est de la précédente, (et comme elle probablement arrachée du sol), mesurant 1,27m de long, 0,95m de large et 0,18m d'épaisseur.

#### Historique

Monument découvert par [F. Meyrat]{.creator} en [mai 2010]{.date}.

</section>

<section class="monument">

### [Aldude]{.spatial} - [Urrixka]{.coverage} 3 - [Tumulus-cromlech]{.type} ([?]{.douteux})

#### Localisation

Carte 1346 Ouest St Etienne-de-Baïgorri.

Ce monument est situé sur un éperon rocheux à sommet plat, qui se dresse à 70m environ à l'Ouest de *Urrixka 2 - Dolmen*.

#### Description 

Petite structure difficile à classer, circulaire, de 1m à 1,10m de diamètre, constituée de petites dallettes, au nombre de 12 à 13, certaines disposées un peu en écaille de poisson, se superposant en partie, et formant une couronne enserrant 3 autres petites dalles disposées à plat. Cette structure, édifiée à l'extrémité de l'éperon rocheux, dominant le magnifique panorama de la vallée des Aldudes entourée de ses montagnes, mais exposée en plein vent, évoque plus une structure funéraire que les vestiges d'un foyer de campeurs, par exemple. Monument douteux.

#### Historique

Monument découvert en [juin 2003]{.date}.

</section>

<section class="monument">

### [Aldude]{.spatial} - [Zarkindégui]{.coverage} - [Pierre couchée]{.type} ([?]{.douteux})

#### Description

Il s'agit de la BF 122 gisant au sol ; pierre grossièrement épannelée, à sommet arrondi, pourrait être une « borne antique » réutilisée ultérieurement. Elle mesure 1,27m de long, 0,75m à sa base et 0,38m d'épaisseur en moyenne. Une croix est gravée au-dessus du nombre 122.

#### Historique

Monument découvert par [Blot J.]{.creator} en [juin 2010]{.date}.

</section>

<section class="monument">

### [Aldude]{.spatial} - [Zarkindegui]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 760m.

À environ une centaine de mètres à l'Ouest de la BF 119.

#### Description

Tumulus de 8,50m de diamètre, et de faible hauteur, érigé sur un terrain en légère pente vers le Nord, il est constitué d'un amoncellement de petite pierraille en grés ; on note une dépression centrale de faible profondeur.

#### Historique

Monument découvert par [J. Blot]{.creator} en [avril 2010]{.date}.

</section>

# Anhaux

<section class="monument">

### [Anhaux]{.spatial} - [Beharria]{.coverage} - [Cromlech]{.type}

#### Localisation

Altitude : 908m.

Ce monument est situé sur un petit replat au flanc Est du Monhoa, dominant un magnifique panorama, très étendu vers le Nord et l'Est, de la côte au piémont.

#### Description

On note une douzaine de blocs de [grès]{.subject}, souvent profondément enfoncés dans le sol et disposés suivant un cercle d'environ 3m de diamètre ; un des blocs périphériques, au Nord, a basculé vers l'extérieur, rompant la régularité du cercle. ; huit autres blocs, de taille plus réduite, apparaissent aussi dans la région centrale. Les dimensions de tous ces éléments sont variables.

-   Pierre 1 : 1m x 1,10m.

-   Pierre 2 : 1,50 x 0,75m.

-   Pierre 3 : 0,75m x 0,76m.

-   Pierre 4 : 0,40m x 0,45m.

On ne note pas de traces de fouilles clandestines.

À 2m au Sud-Sud-Est a été déposée une plaque commémorative en basque, dont la traduction est la suivante (M. Duvert) : *Le pays est le corps, la langue (eukara) le coeur*. Il n'est cependant pas évident que cette plaque soit en rapport direct avec ce monument assez discret, mais plus probablement avec le lieu et sa situation exceptionnelle.

#### Historique

Monument découvert par [F. Meyrat]{.creator} en [septembre 2019]{.date}.

</section>

<section class="monument">

### [Anhaux]{.spatial} - [Monhoa]{.coverage} - [Tumulus]{.type} [(?)]{.douteux}

#### Localisation

Plus à l'Ouest de *Beharria - Cromlech* se dresse le sommet du mont Monhoa.

Altitude : 1019m. par une borne érigée sur ce qui donne l'impression d'être un tumulus... remanié pouvant atteindre environ 8m de diamètre et 0,50m de haut. Il est très difficile de se prononcer.

#### Historique

Cas particulier soulevé par [F. Meyrat]{.creator} en [septembre 2019]{.date}.

</section>

<section class="monument">

### [Anhaux]{.spatial} - [Urdiako lepoa]{.coverage} 1 - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 920m.

Il est quasiment tangent à la berge Est de la route, et perpendiculaire à elle.

#### Description

Ce tertre, ovale, à grand axe Est-Ouest, mesure 9,50m de long, 7m de large et 0,40m à 0,80m de haut suivant les endroits. On note l'empreinte, très marquée, du passage de l'ancienne piste (environ 1m de large) à la moitié du tertre, perpendiculairement à son grand axe ; un bloc de [schiste]{.subject} gris apparaît dans son quart Nord-Est.

#### Historique

Tertre découvert par [J. Blot]{.creator} en [juillet 2011]{.date}.

</section>

<section class="monument">

### [Anhaux]{.spatial} - [Urdiako lepoa]{.coverage} 2 - [Tertre d'habitat]{.type}

#### Localisation

Il est à 10m au Sud Sud-Ouest de *Urdiakolepoa 1 - Tertre d'habitat*, et comme lui, tangent à la route actuelle.

#### Description

Il mesure 5 m de long, 3m de large et 0,40m de haut environ. Son grand axe est orienté Nord-Ouest Sud-Est, et on retrouve l'empreinte de la piste ancienne à son 1/3 supérieur, mais en moins marquée que précédemment. À noter une curieuse [excavation circulaire]{.subject} de 2m de diamètre à son extrémité Sud-Ouest...

#### Historique

Tertre trouvé par [J. Blot]{.creator} en [juillet 2011]{.date}.

</section>

# Arcangues

<section class="monument">

### [Arcangues]{.spatial} - [Dornarieta]{.coverage} - [Tumulus]{.type}

#### Localisation

Carte : 1244 Est Espelette.

Altitude : 83m.

Il est situé à gauche de la route qui de Saint-Pée mène à Arcangues, un peu avant le lieu-dit Dornarieta, au sommet d'une petite éminence, cote 83, qui domine un virage important de la route.

#### Description

Important tumulus terreux de 12m de diamètre et 1,20m de haut, érigé sur un sol en très légère pente vers l'Est.

#### Historique

Monument découvert en [octobre 1971]{.date}.

</section>

# Arette

<section class="monument">

### [Arette]{.spatial} - [La Borne frontière]{.coverage} - [Faux dolmen]{.type}

#### Localisation

Altitude : 1756m.

Cette construction est située à gauche de la route quand on se rend en Espagne, à une soixantaine de mètres avant la BF 262 où se renouvellent chaque année le 13 juillet les Faceries entre Roncal et Barétous.

Elle est située très en hauteur par rapport à la route, à une distance d'environ cinquante mètres.

#### Description

Ce monument a été publié - en tant que dolmen - dans [@berdoyValleeBaretousDepuis1990, p. 97-114] par [Anne Berdoy]{.creator} et [Claude Blanc]{.creator}.

Nous publions ici ces quelques lignes à titre rectificatif, car nous ne pensons absolument pas qu'il puisse s'agir d'un dolmen.

Cette construction est érigée sur *un terrain très en pente* vers l'Ouest ; il s'agit ici plus d'un abri, *ouvert plein Ouest*, que d'un [dolmen]{.subject}. Constitué de [calcaire]{.subject} local, *il prend appui sur la colline où il est érigé, et s'enfouit dedans*. Il est délimité par 4 dalles : 3 dalles latérales et une de [couverture]{.subject}.

Tout d'abord, une dalle latérale verticale, côté Nord, mesurant 2,10m de long, 1,10m de haut dans sa moitié arrière, et 0,24m d'épaisseur. *Cette dalle semble bien être en place, d'origine, et avoir été utilisée telle que*.

Une autre dalle verticale, disposée de main d'homme, ferme cet abri au Sud. Elle mesure 2,05 m de long, 1,19m de haut et 0,27m d'épaisseur.

Le fond de l'abri, à l'Est est délimité par une dalle rectangulaire verticale de 1,54m de long et 1,14m de haut *qui prend appui sur le terrain en arrière de lui*. Il est recouvert par une dalle horizontale mesurant 1,75m dans le sens Ouest-Est, et 1,75m dans le sens Nord-Sud, et 0,12m d'épaisseur qui s'appuie sur les 3 dalles précédentes. On note enfin une dalle horizontale disposée à l'avant de la construction, c.à.d. à l'Ouest ; elle mesure 1,63m dans le sens Nord-Sud, et 1,18m dans le sens Ouest-Est, et a une épaisseur de 0,10m. Nous en faisons plus un seuil de l'abri qu'une dalle de « fermeture » d'un [dolmen]{.subject}.

**Discussion** : Un [dolmen]{.subject}, en Pays Basque (et ailleurs), est érigé sur un terrain plat, la base de ses montants est enfoncée dans le sol de manière égale, l'ouverture de la chambre funéraire se fait à l'Est, ou dans une direction où l'Est prédomine, c.à.d. vers le soleil levant. Comme on le voit ici, aucun de ces critères n'est respecté. Enfin, nous avons l'exemple très proche de deux constructions similaires, construites à proximité du vrai dolmen de Caque. Nous renvoyons le lecteur à notre publication dans [@blotSouleSesVestiges1979, p. 42-43].

Altitude : 1726m.

Les deux constructions, autrefois appelées *[dolmen]{.subject} de Caque 2* et *3*, sont situées à une vingtaine de mètres à l'Est du vrai dolmen de Caque. Comme la construction ici décrite, elles utilisent le terrain environnant, le complétant pour en faire un abri. Pierre Boucher, au vu de leurs structures, en faisait des abris à porcs, ceci étant confirmé par le nom béarnais de ces deux constructions : *curtel*, signifiant porcherie.

</section>

<section class="monument">

### [Arette]{.spatial} - [Larranche]{.coverage} 1 - [Tumulus]{.type}

Nous décrirons sous ce nom un tumulus identifié en 1978 et publié en 1979 sous le nom de *tumulus de Garbas* [@blotSouleSesVestiges1979, p. 38]. Avec la précision des GPS actuels, ce monument se trouve être en réalité dans la commune d'Arette, et plus précisément au lieu-dit *Larranche*.

#### Localisation

Altitude : 898m.

Un peu avant le col de Garbas, se trouve un petit col que nous appellerons *col de Larranche* ; à ce niveau, et à 6m environ au Nord la piste, une légère éminence domine celle-ci, sur laquelle est construit le monument.

#### Description

Tumulus terreux d'environ 8m de diamètre et 0,80m de haut. On note une dépression qui ampute en partie son quart Sud-Ouest. Tumulus ou tertre d'habitat ? L'hésitation est permise.

#### Historique

Tumulus découvert par [Blot J.]{.creator} en [juin 1978]{.date} et revisité en juillet 2016.

</section>

<section class="monument">

### [Arette]{.spatial} - [Larranche]{.coverage} 2 - [Tumulus]{.type}

#### Localisation

Altitude : 904m.

En empruntant au col la piste de gauche, horizontale, on distingue à environ 150m au Nord de *Larranche 1 - Tumulus* et à environ 4m au Nord-Est de la piste, un petit tumulus qui la surplombe légèrement.

#### Description

Petit tumulus terreux en forme de galette aplatie, mesurant 2,80m de diamètre et 0,25m de haut ; on distingue à sa périphérie deux pierres profondément enfouies dans son quart Sud-Est ; éléments d'un péristalithe ?

#### Historique

Tumulus découvert par [Blot J.]{.creator} en [juillet 2016]{.date}.

</section>

<section class="monument">

### [Arette]{.spatial} - [Soum de Soudet]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 1491m.

Il est situé vers l'extrémité Sud-Ouest de cette longue croupe allongée suivant un axe Est-Ouest qui domine, au Nord, la route reliant la station de la Pierre St Martin à Lanne, au moment de la bifurcation vers Arette. Il est situé juste avant la rupture de pente, au Nord de la piste de crête, et tangent à celle-ci, à une dizaine de mètres d'un poteau indicateur renversé portant l'inscription : "Soum de Soudet 2250m". On a un panorama exceptionnellement vaste sur tout l'horizon au Sud-Ouest.

#### Description

Tumulus terreux circulaire bien visible, de 12m de diamètre et 0,50m de haut environ, en forme de galette aplatie.

#### Historique

Monument découvert par [Blot J.]{.creator} en [août 2017]{.date}.

</section>

# Arnégui

<section class="monument">

### [Arnégui]{.spatial} - [Elhursaro]{.coverage} 1 - [Cromlech]{.type}

#### Localisation

Altitude : 1125m.

Ce cromlech fait partie d'un ensemble de 5 cromlechs et un tumulus érigés sur un sol plat, au milieu d'un vaste plateau en pente douce vers l'Ouest et lui-même situé à l'Ouest de la voie romaine (au niveau du col d'Elhursaro.).

#### Description

Il est le plus à l'Ouest du groupe. Neuf petits blocs de calcaire, au ras du sol et de dimensions très modestes, délimitent un cercle de 3,30m de diamètre; seule une pierre située au Sud est un peu plus visible (0,80m x 0,50m).

Disons de suite que la modestie et l'architecture en général fort négligée de ces 5 monuments, nous évoquent fortement les cromlechs de Sohandy, dont les exemplaires fouillés par nous ont été datés en période historique, bien que relevant d'une tradition protohistorique.

#### Historique

Monument découvert par [F. Meyrat]{.creator} en [septembre 2018]{.date}.

</section>

<section class="monument">

### [Arnégui]{.spatial} - [Elhursaro]{.coverage} 2 - [Cromlech]{.type}

#### Localisation

Il est situé à 2m au Nord-Nord-Ouest de *Elhursaro 1 - Cromlech*.

#### Description

Une dizaine de petits blocs de [calcaire]{.subject}, au ras du sol, délimitent un cercle de 3,70m de diamètre. Avant un dégagement sommaire, ils étaient très peu visibles ; notons toutefois une pierre au Nord-Ouest, mesurant 0,56m de long et 0,28m de large ainsi qu'une autre, à l'Est mesurant 0,64m de long, 0,10m de large et 0,17m de haut.

#### Historique

Monument découvert par [F. Meyrat]{.creator} en [septembre 2018]{.date}.

</section>

<section class="monument">

### [Arnégui]{.spatial} - [Elhursaro]{.coverage} 3 - [Cromlech]{.type} ([?]{.douteux})

#### Localisation

Il est situé à 9,30 m au Sud-Ouest de *Elhursaro 2 - Cromlech*.

#### Description

Cinq pierres [calcaire]{.subject} délimitent un cercle de 3,30m de diamètre. Trois sont de très faibles dimensions dans la moitié Est, et les deux autres, dans la moitié Ouest sont plus importantes et très proches de *Elhursaro 4 - Cromlech*. : elles peuvent faire partie de son architecture, ou être tangentes à ce cercle, ce qui nous paraît le plus probable. Ce monument, dans son ensemble, nous paraît cependant douteux; seules des fouilles trancheraient la question.

#### Historique

Monument découvert par [F. Meyrat]{.creator} en [septembre 2018]{.date}.

</section>

<section class="monument">

### [Arnégui]{.spatial} - [Elhursaro]{.coverage} 4 - [Cromlech]{.type}

#### Localisation

Ce cercle est tangent au Sud-Est de *Elhursaro 3 - Cromlech*.

#### Description

On compte 7 blocs de [calcaire]{.subject}, délimitant un cercle de 3,40m de diamètre. Dans la moitié Sud-Est, comme nous l'avons signalé, les blocs sont tangents à 2 témoins du *cromlech n°3*, mais, peut-être ces deux-là font-ils aussi partie du *cercle n°4* ?

#### Historique

Monument découvert par [F. Meyrat]{.creator} en [septembre 2018]{.date}.

</section>

<section class="monument">

### [Arnégui]{.spatial} - [Elhursaro]{.coverage} 5 - [Cromlech]{.type} ([?]{.douteux})

#### Localisation

Il est tangent, au Nord-Ouest, de *Elhursaro 3 - Cromlech*.

#### Description

Cercle de 3,70m de diamètre, délimité par 5 petits blocs de [calcaire]{.subject}, dont 2 font partie du péristalithe du *cromlech n°3*, au Nord-Ouest. La discrétion des témoins et l'irrégularité du cercle nous font qualifier ce dernier de douteux. En fait seules des fouilles pourraient, là encore - comme pour le n°3 - témoigner de l'authenticité de ce monument.

#### Historique

Monument découvert par [F. Meyrat]{.creator} en [septembre 2018]{.date}.

</section>

<section class="monument">

### [Arnégui]{.spatial} - [Elhursaro]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 1117m.

Il est situé à une dizaine de mètres au Nord-Ouest de *Elhursaro 4 - Cromlech*.

#### Description

Tumulus en terre, circulaire, en forme de galette aplatie, mesurant 4m de diamètre et 0,10m de haut. Pas de pierres visibles.

#### Historique

Monument découvert par [F. Meyrat]{.creator} en [septembre 2018]{.date}.

</section>

<section class="monument">

### [Arnégui]{.spatial} - [Héganzo]{.coverage} 1 - [Cromlech]{.type}

#### Localisation

Carte IGN 1346 Ouest Saint-Etienne-de-Baïgorri.

Altitude : 1111m.

Ce monument se trouve sur l'éminence qui domine au Sud-Ouest le col d'Héganzo et la route en lacet qui y passe.

#### Description

Sur un terrain en légère pente vers le Sud-Ouest, vingt-cinq pierres, au ras du sol, délimitent un cercle de 4,50m de diamètre, à l'intérieur légèrement surélevé (0,30m environ). On peut distinguer aussi de nombreuses pierres à l'intérieur.

#### Historique

Monument découvert par [A. Martinez Manteca]{.creator} en [2001]{.date}.

</section>

<section class="monument">

### [Arnégui]{.spatial} - [Héganzo]{.coverage} 2 - [Cromlech]{.type}

#### Localisation

À 3m environ au Sud-Ouest de *Héganzo 1 - Cromlech*.

#### Description

Six pierres au ras du sol délimitent un cercle de 2m de diamètre; au centre sont visibles 4 petits blocs de schiste.

#### Historique

Monument découvert par [A. Martinez Manteca]{.creator} en [2009]{.date}.

</section>

<section class="monument">

### [Arnégui]{.spatial} - [Héganzo]{.coverage} 3 - [Cromlech]{.type}

#### Localisation

À environ 2 m au Nord-Est de *Héganzo 1 - Cromlech*.

#### Description

Quelques petits blocs de [schiste]{.subject} au ras du sol semblent délimiter un cercle de 3,50m de diamètre. Monument douteux.

#### Historique

Monument découvert par [Blot. J.]{.creator} en [octobre 2009]{.date}.

</section>

<section class="monument">

### [Arnégui]{.spatial} - [Héganzo]{.coverage} - [Tumulus]{.type} (ou [Tertre]{.type})

#### Localisation

Altitude : 1075m.

Il est situé au Nord-Ouest de *Héganzo 3 - Cromlech*, sur un replat.

#### Description

Tumulus circulaire de 11m de diamètre et 0,70m de haut, constitué, semble-t-il, plus par du cailloutis que de la terre. Ce fait, ainsi que sa situation sur un sol plat, serait plus en faveur d'un tumulus que d'un tertre d'habitat.

#### Historique

Monument découvert par [A. Martinez Manteca]{.creator} en [2001]{.date}.

</section>

<section class="monument">

### [Arnégui]{.spatial} - [Legarre]{.coverage} 1 - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 980m.

À l'extrémité Nord-Ouest d'un petit replat dominant, au Sud-Ouest la bretelle issue de la *Voie Romaine* et descendant vers Arnégui.

#### Description

Tertre de terre ovale d'une vingtaine de mètres de long et 9m de large, orienté selon un axe Nord-Est Sud-Ouest, de 1m de haut à son versant Sud-Est et 5m à 6m sur le versant opposé.

#### Historique

Tertre découvert par [Blot J.]{.creator} en [août 2011]{.date}.

</section>

<section class="monument">

### [Arnégui]{.spatial} - [Legarre]{.coverage} 2 - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 790m.

Situé à quelques mètres au Sud-Ouest d'un virage en épingle à cheveu de la bretelle issue de la *Voie Romaine* et descendant vers Arnégui.

#### Description

Tertre de terre ovale, d'une vingtaine de mètres de long et cinq de large, et de 2m de haut à son versant Est.

#### Historique

Tertre découvert par [Blot J.]{.creator} en [août 2011]{.date}.

</section>

<section class="monument">

### [Arnégui]{.spatial} - [Legarre]{.coverage} - [Tumulus]{.type}

#### Localisation

Carte 1346 Ouest Saint-Etienne-de-Baïgorri.

Altitude : 737m.

Il est situé sur un replat à l'Est Sud-Ouest du pic Beillurti et à une quinzaine de mètres au Nord de la route goudronnée qui relie la *Voie Romaine* à Arnégui.

#### Description

Tumulus de terre à sommet aplati de 7m de diamètre et 0,40m de haut.

#### Historique

Tumulus découvert en [août 1977]{.date}.

</section>

<section class="monument">

### [Arnégui]{.spatial} - [Legarre]{.coverage} 1 - [Tumulus]{.type}

#### Localisation

Altitude : 740m.

Il est situé sur un replat à l'Est Sud-Ouest du pic Beillurti et à une trentaine de mètres au Nord de la route goudronnée qui relie la *Voie Romaine* à Arnégui.

#### Description

Ce tumulus circulaire, que nous avions découvert en 1977, constitué de terre présentait un sommet aplati et mesurait 7m de diamètre et 0,40m de haut. Lors de notre passage en septembre 2018, nous n'avons pu que constater sa quasi totale destruction due à l'érection d'un pylône électrique sur son emplacement.

#### Historique

Tumulus découvert en [août 1977]{.date} par [J. Blot]{.creator} et publié dans [@blotInventaireMonumentsProtohistoriques2009].

</section>

<section class="monument">

### [Arnégui]{.spatial} - [Legarre]{.coverage} 2 - [Tumulus]{.type}

#### Localisation

Altitude : 760m.

Il est situé sur la droite du chemin qui mène au sommet de la colline qui domine, à l'Ouest de *Legarre 1 - Tumulus* ; il est tangent à une clôture de barbelés.

#### Description

Tumulus en terre, plutôt de forme ovalaire à grand axe Ouest Nord-Ouest - Est Sud-Est, mesurant 6m x 3m et 0,40m de haut.

#### Historique

Tumulus découvert par [F. Meyrat]{.creator} en [octobre 2018]{.date}.

</section>

<section class="monument">

### [Arnégui]{.spatial} - [Négoucharo]{.coverage} 1 - [Cromlech]{.type}

Nous avons déjà décrit dans ce site, 3 tumulus [@blotNouveauxVestigesMegalithiques1972c, p.84] et 1 tumulus-cromlech [@blotVestigesProtohistoriquesVoie1978, p.72]. Trois nouveaux monuments sont à décrire.

#### Localisation

Altitude : 1000m.

Le monument se trouve sur une petite éminence qui domine, à l'Est, la route qui a longé le pic de Beillurti, au moment où s'en détache une bretelle destinée à un cayolar voisin.

#### Description

Une vingtaine de pierres, parfaitement visibles au ras du sol, délimitent un cercle de 3,80m de diamètre.

#### Historique

Monument découvert par [A. Martinez Manteca]{.creator} en [2001]{.date}.

</section>

<section class="monument">

### [Arnégui]{.spatial} - [Négoucharo]{.coverage} 4 - [Tumulus]{.type}

#### Localisation

Situé à 25m au Nord-Est de *Négoucharo 1 - Cromlech*.

#### Description

Tumulus constitué de terre et de nombreuses pierres, mesurant 9m de diamètre et 0,85m de haut, édifié sur terrain plat.

#### Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2009]{.date}.

</section>

<section class="monument">

### [Arnégui]{.spatial} - [Négoucharo]{.coverage} 5 - [Tumulus]{.type} ([?]{.douteux})

#### Localisation

Situé à 15m au Nord-Est de *Negoucharo 4 - Tumulus*.

#### Description

Tumulus de 4m de diamètre et 0,40m de haut, constitué de terre et de pierres. Monument douteux.

#### Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2009]{.date}.

</section>

<section class="monument">

### [Arnégui]{.spatial} - [Urdanazburu]{.coverage} N 4 - [Tumulus]{.type}

#### Localisation

Altitude : 1190m.

Ce monument est à environ 70m à l'Ouest de la route, érigé sur un terrain plat.

#### Description

Il se présente comme un tumulus pierreux de 3m de diamètre et 0,45m de haut environ ; les pierres sont bien enfoncées dans la structure mais en désordre et on note une certaine irrégularité dans le secteur Nord-Ouest, comme si un éboulement s'était produit, rompant la circularité du tumulus.

#### Historique

Monument découvert par [Blot J.]{.creator} en [septembre 2019]{.date}.

</section>

# Ascain

<section class="monument">

### [Ascain]{.spatial} - [Androla]{.coverage} - [Monolithe]{.type}

#### Localisation

Altitude : 297m.

Cette longue pierre se trouve couchée au sol en bordure droite de la piste quand on la gravit vers le Sud, un peu avant d'arriver au site des dolmens d'Androla.

#### Description

Long parallélépipède de [grès]{.subject} local, mesurant 4,40m de long selon son grand axe Nord-Sud. Son sommet Sud est plus étroit que sa base Nord - (0,46m de large) - et sa largeur au milieu est de 0,82m.

Son épaisseur va croissant du Sud au Nord : 0,38m au Sud, 0,57m au milieu, 0,67m à la base.

Les signes de taille ou d'épannelage sont discrets (présents surtout au sommet).

#### Historique

Cette pierre avait été remarquée par [Blot J.]{.creator} depuis les années 90, en particulier lorsqu'elle avait été érigée, à cette époque, en bordure du chemin, tel un authentique menhir - qu'elle n'était pas en ce lieu - Toutefois, il n'est pas exclu que, située ailleurs dans un passé plus lointain (mais dans un lieu très proche), elle n'ait eu en effet un rôle de *Muga*, compte tenu de sa forme et de ses dimensions.

</section>

<section class="monument">

### [Ascain]{.spatial} - [Androla]{.coverage} Sud - [Dolmen]{.type}

#### Localisation

Carte IGN 1245 OT Hendaye Saint-Jean-de-Luz.

Altitude : 331m.

Ce monument est situé à quelques dizaines de mètres au Sud du *dolmen Androla* décrit par J.M. de Barandiaran [@blotNouveauxVestigesMegalithiques1971, p.34]. Il est aussi à une vingtaine de mètres à l'Ouest du petit refuge au toit de loses et murs en blocs de [grès]{.subject}, construit en bordure du chemin, à droite en montant.

#### Description

On note un tumulus constitué de nombreux blocs de [grès]{.subject} de la taille d'un pavé, dont on remarque par endroit l'agencement soigneux. Il mesure environ 8m de diamètre, 0,40m de haut, mais les dimensions, en sont assez difficile à apprécier du fait d'une abondante végétation de touyas. Au centre se devine une légère dépression d'environ 1,20m de diamètre et quelques centimètres de profondeur, qui pourrait correspondre au reste d'une [chambre funéraire]{.subject}, dont on aurait, comme très souvent, arraché les dalles. L'aspect et les dimensions de ce tumulus nous font plus penser à un tumulus dolménique qu'à un tumulus simple.

#### Historique

Monument découvert en [1989]{.date} par [I. Gaztelu]{.creator}.

</section>

<section class="monument">

### [Ascain]{.spatial} - [Arguibele]{.coverage} - [Monolithe]{.type}

#### Localisation

Altitude : 174m.

Il est sur le petit plateau où se trouvent les 5 autres dolmens de ce nom.

Il est distant de 13m à l'Est de *D2*, de 20m au Sud-Ouest de *D1*, de 16m au Sud-Est de *D3*, et à 25m au Sud Sud-Est de *D5*.

#### Description

Monolithe de [grès]{.subject} rose [triasique]{.subject} couché au sol, en forme de pain de sucre, la pointe orientée vers le Nord. Il mesure 2,50m de long, 1,10m dans sa partie la plus large, 0,80m au rétrécissement avant sa pointe, son épaisseur varie entre 0,50m et 0,24m suivant les endroits.

Ce monolithe, ne semble pas présenter de traces d'épannelage, sauf peut-être à sa pointe Nord. Par contre sa présence en ce lieu ne paraît pas *naturelle*... et nous tenons compte de son contexte archéologique pour le décrire ici.

#### Historique

Monolithe signalée par le [groupe Hilharriak]{.creator} en [mars 2013]{.date}.

</section>

<section class="monument">

### [Ascain]{.spatial} - [Arguibele]{.coverage} 3 - [Dolmen]{.type}

#### Localisation

Carte 1245 Est Espelette.

Altitude : 150m.

Il est situé à 12m à l'Ouest Nord-Ouest du *dolmen Arguibele 1*, à 5m à l'Ouest du *n°2* déjà décrits [@blotNouveauxVestigesMegalithiques1971, p. 30], et à 4m à l'Ouest de la piste pastorale, elle-même orientée Nord-Sud.

#### Description

Tumulus pierreux formé de nombreux fragments de dalles de [grès]{.subject}, mesurant 7m de diamètre et 1,50m de haut.

La [chambre funéraire]{.subject} est signalée par une dépression centrale au bord Sud de laquelle on note une grande dalle couchée mesurant 1,60m de long et 1m de large ; s'agit-il du couvercle ?

Peut-être y aurait-il une ébauche de péristalithe.

#### Historique

Dolmen découvert en [janvier 1973]{.date}.

</section>

<section class="monument">

### [Ascain]{.spatial} - [Arguibele]{.coverage} 4 - [Dolmen]{.type}

#### Localisation

À 2m au Nord de *Arguibele 3 - Dolmen*.

#### Description

On note un modeste tumulus mixte, à prédominance pierreuse, d'un diamètre de 6m, et de 0,30m de haut. La [chambre funéraire]{.subject}, centrale, est orientée est Sud-Ouest Nord-Est, et mesure 1,80m de long, 0,55m de large, et 0,40m de haut ; elle est délimitée par trois dalles de [grès]{.subject} rose, bien visibles : 2 au Nord Nord-Ouest, l'une de 0,60m de long et 0,35m de haut, l'autre de 0,90m de long et 0,40m de haut, séparées l'une de l'autre par 0,20m, et une autre dalle au Sud-Est de 0,90m de long et 0,30m de haut.

#### Historique

Dolmen découvert en [janvier 1973]{.date}.

</section>

<section class="monument">

### [Ascain]{.spatial} - [Arguibele]{.coverage} 5 - [Dolmen]{.type}

#### Localisation

À 2m au Sud-Ouest de *Arguibele 4 - Dolmen*.

#### Description

Tumulus pierreux bien visible de 10m de diamètre, et 0,70m de haut; onze pierres périphériques pourraient faire partie d'un possible péristalithe. La [chambre funéraire]{.subject} est bien visible sous la forme d'une dépression centrale dans laquelle apparaissent seulement 2 petites dalles ; mais il est très difficile d'en apprécier l'orientation et les dimensions.

#### Historique

Dolmen découvert en [janvier 1973]{.date}.

</section>

<section class="monument">

### [Ascain]{.spatial} - [Arraioa]{.coverage} - [Pierre couchée]{.type} ([?]{.douteux})

#### Localisation

Cette pierre gît en bordure Nord de la piste (une très ancienne voie reliant Ascain au col de Saint-Ignace), à environ 6m du ruisseau Arrayoko Erreka. C'est la seule pierre importante visible en ces lieux.

Altitude : 60m.

#### Description

Ce bloc de [grès]{.subject} local affecte une forme en pain de sucre, à base Est Sud-Est, et à sommet Ouest Nord-Ouest. Il mesure 2,30m de long, 0,85m à sa base et 0,50m en son milieu ; son épaisseur moyenne est de 0,30m. Il pourrait y avoir (sous toutes réserves) des traces d'épannelage au niveau du sommet. On note, plantée dans le sol, à deux centimètres de sa base - et parallèle à celle-ci - une dalle de [grès]{.subject} de 0,50m de long et 0,20m d'épaisseur (dalle de calage ?)

#### Historique

Pierre découverte par [Blot J.]{.creator} en [novembre 2010]{.date}.

</section>

<section class="monument">

### [Ascain]{.spatial} - [Arranoxola]{.coverage} - [Pierre couchée]{.type} ([?]{.douteux})

#### Localisation

Altitude : 580m.

Elle est située un peu au-dessus et au Sud du col (à 560m d'altitude) qui reçoit la piste venant d'Arranoxola, et à l'Est du chemin dit de l'Impératrice.

#### Description

Dalle de [grès]{.subject} épaisse de 0,42m couchée au sol, de forme triangulaire, mesurant 2,57m de long et 2,35m de large. On note d'évidentes traces d'épannelage sur son bord Nord, mais il reste impossible de préciser la nature de cette dalle...

#### Historique

Dalle découverte en [janvier 2013]{.date} par [P. Badiola]{.creator}.

</section>

<section class="monument">

### [Ascain]{.spatial} - [Biskartzun]{.coverage} 1 - [Tumulus]{.type}

#### Localisation

Altitude : 148m.

Tumulus situé immédiatement à droite de la même piste qui monte de Martinhauren borda, et de *Martinhaurren Borda - Tumulus*, et se rend au sommet de Biskartzun. Ce tumulus n°1 est tangent au Sud-Est de la piste et à 3m au Nord-Ouest de l'angle du mur de clôture (pierres et barbelés) du pâturage voisin.

#### Description

Petit tumulus de 3m de diamètre et 0,15m de haut avec une petite dépression centrale ; pas de pierres visibles.

#### Historique

Tumulus découvert par [Meyrat F.]{.creator} en [février 2012]{.date}.

</section>

<section class="monument">

### [Ascain]{.spatial} - [Biskartzun]{.coverage} 2 - [Tumulus]{.type}

#### Localisation

À une vingtaine de mètres au Nord Est de *Biskartzun 1 - Tumulus* sur un petit replat bien marqué sur la même piste et à droite de celle-ci. Il est dissimulé sous une végétation d'ajoncs abondante.

#### Description

Tumulus de 3m de diamètre et 0,30m de haut environ, marqué par une dépression centrale ; sept à huit pierres sont visibles sur ce tumulus.

#### Historique

Tumulus découvert par [Meyrat F.]{.creator} en [février 2012]{.date}.

</section>

<section class="monument">

### [Ascain]{.spatial} - [Gorrostiarria]{.coverage} - [Tumulus-cromlech]{.type}

#### Localisation

Altitude : 585m.

Il est situé plus en altitude que *Gorrostiarria - Cromlechs*, et à 1m au Nord d'un important filon de [grès]{.subject} [triasique]{.subject} de la bordure Sud de la crête d'Altsaan.

#### Description

Tumulus de forme plus ovale que circulaire, mesurant 3m dans son grand axe Est-Ouest, et 2,20m selon l'axe Nord-Sud ; hauteur : 0,40m environ. De nombreuses dalles entourent sa périphérie et en partie son sommet. Tumulus-cromlech ou tombe datant des combats de l'ère napoléonienne ?

#### Historique

Monument découvert par [Blot J.]{.creator} en [mars 2012]{.date}.

</section>

<section class="monument">

### [Ascain]{.spatial} - [Gorrostiarria]{.coverage} - [Tumulus]{.type}

#### Localisation

Il est à 1,70m au Nord-Est de *Gorostiarria - Tumulus-cromlech*.

#### Description

Petit tumulus de terre de 3m de diamètre et 0,30m de haut, à la surface duquel quelques pierres apparaissent.

#### Historique

Monument découvert par [Blot J.]{.creator} en [mars 2012]{.date}.

</section>

<section class="monument">

### [Ascain]{.spatial} - [Gorrostiarria]{.coverage}-[Cromlechs]{.type}

La nécropole des cromlechs de Gorostiarria a été initialement décrite par [J.M. de Barandiaran]{.creator}, avec un plan, dans [@barandiaranContribucionEstudioCromlechs1949, p.206]. Il cite 8 cercles, mais n'en dessine que 7 sur son plan.

Nous avons donc voulu reprendre le plan, mais, les années ayant passées, la végétation et les dégradations ont fait que nous (Blot J. et Meyrat F.) n'avons pas retrouvé le même nombre de pierres que le Père J.M. de Barandiaran - Par ailleurs il nous a bien semblé pouvoir identifier les cercles 9, 10, 11, 12, ci-après brièvement décrits - (les n°9, 10, 11, ont été signalés par [L. Millan]{.creator} en [1989]{.date}, mais non localisables sur le terrain -- Est-ce que ce sont les mêmes que les nôtres ?)

Nous avons aussi repris les autres monuments tout aussi brièvement, afin de donner un aperçu de l'état actuel de cette nécropole, telle qu'elle nous est apparue en ce mois de [mars 2012]{.date}.

-   *Cromlech 1* : 6m de diamètre ; 16 pierres

-   *Cromlech 2* : 5,60m de diamètre ; 12 pierres

-   *Cromlech 3* : 5,50m de diamètre ; 9 pierres

-   *Cromlech 4* : 3,30m de diamètre ; 4 pierres

-   *Cromlech 5* : 4,20m de diamètre ; 8 pierres

-   *Cromlech 6* : 5,40m de diamètre ; 14 pierres

-   *Cromlech 7* : 6m de diamètre ; 14 pierres

-   *Cromlech 8* : 7,30m de diamètre ; 18 pierres

-   *Cromlech 9* : 7,80m de diamètre ; 12 pierres

-   *Cromlech 10* : 6,70m de diamètre ; 13 pierres

-   *Cromlech 11* : 3,20m de diamètre ; 5 pierres

-   *Cromlech 12* : 3m de diamètre ; 9 pierres

</section>

<section class="monument">

### [Ascain]{.spatial} - [Ihicelhaya]{.coverage} - [Monolithe]{.type}

#### Localisation

Carte IGN 1245 OT Hendaye Saint-Jean-de-Luz.

Altitude : 500m.

Ce monolithe couché se trouve au beau milieu du plateau, à une vingtaine de mètres à l'Ouest de la piste balisée qui monte vers le dolmen Ihcelhaya (lui-même situé dans l'angle formé par la plantation de conifères). Il n'y a pas d'autres blocs rocheux dans les environs immédiats.

#### Description

Il s'agit d'un monolithe bloc en [grès]{.subject} local, allongé sur le sol en direction Nord-Sud, en pain de sucre à pointe vers le Sud. Il mesure 2,76m de long, 1,10m de large dans sa partie moyenne, et 0,95m à sa base. Son bord Ouest est sensiblement rectiligne et paraît naturel, mais les traces d'épannelage sont nombreuses au sommet et au bord Est. L'épaisseur apparente (le monument est en partie enfoncé dans le sol). atteint 0,30m.

#### Historique

Monument découvert en [1977]{.date} par [A. Martinez Manteca]{.creator}.

</section>

<section class="monument">

### [Ascain]{.spatial} - [Ihicelhaya]{.coverage} Est - [Tertres d'habitat]{.type}

#### Localisation

Altitude : 465m.

Les 2 tertres d'habitat sont à une dizaine de mètres à l'Est d'un petit ruisseau.

#### Description

Le premier est un petit tertre de terre, bien visible, asymétrique, érigé sur terrain en pente, de 7m de long, 5m de large et 0,40m de haut en moyenne. À une trentaine de mètres au Sud Sud-Ouest, se voit un deuxième tertre, plus important que le premier, érigé sur le bord même du ruisseau ; lui aussi asymétrique, il mesure une vingtaine de mètres de long, une quinzaine de large et 2m à 3m de haut suivant le niveau considéré.

#### Historique

Ces deux tertres ont été découverts par [Blot J.]{.creator} en [décembre 2011]{.date}.

</section>

<section class="monument">

### [Ascain]{.spatial} - [Jauréguikoborda]{.coverage} - [Dalle plantée]{.type}

#### Localisation

Altitude : 274m.

Elle est située à 100m au Sud-Est de la cote 277 et à environ 500m à l'Est de la ferme *Jauréguikoborda*. Elle est plantée après le petit col au flanc Sud de la colline, et à 2m environ à l'Est de la piste qui en vient.

#### Description

Dalle plantée, haute de 0,98m, grossièrement rectangulaire, avec un sommet un peu plus étroit (0,22m) que sa base (0,32m) ; son épaisseur est en moyenne de 0,15m.

Sa partie supérieure paraît avoir été volontairement régularisée. Ses deux faces Ouest et Est son lisses, cette dernière s'inclinant légèrement vers le sol.

#### Historique

Dalle découverte par [A. Martinez]{.creator} en [1999]{.date}.

</section>

<section class="monument">

### [Ascain]{.spatial} - [Ihicelhaya]{.coverage} Nord - [Monolithe]{.type}

#### Localisation

Altitude : 385m.

Il est sur la pente qui domine, au Sud-Ouest, la piste qui monte du ruisseau des Trois Fontaines au plateau d'Ihicelaya.

#### Description

Beau bloc de [grès]{.subject} [triasique]{.subject} parallélépipédique, gisant selon un axe Nord Nord-Est Sud Sud-Ouest sur un terrain en forte pente vers le Nord-Ouest. Il mesure 2,71m de long, 0,72m de large à sa base, au Sud-Ouest et 0,26m à sa pointe au Nord-Est ; son épaisseur est de 0,25m en moyenne. Il semble présenter quelques traces d'épannelage à son sommet. Il est notable qu'il n'y a aucun autre bloc de pierre ou de dalle dans les environs. Enfin, tangent au Nord-Ouest on note ce qui pourrait être un petit tertre d'habitat, asymétrique, de 5m de large environ.

#### Historique

Monument découvert par [Blot J.]{.creator} en [décembre 2011]{.date}.

</section>

<section class="monument">

### [Ascain]{.spatial} - [La Plana]{.coverage} - [Monolithe]{.type}

#### Localisation

Altitude : 160m.

On voit ce monolithe au départ du premier des deux sentiers qui montent à Larrun en partant du replat (La Plana) au-dessus du parking des carrières d'Ascain.

#### Description

Très beau bloc de [grès]{.subject} parallélépipédique, gisant au sol selon un axe Est-Ouest. Il mesure 4,10m de long, 0,94m dans sa partie la plus large et 0,60m d'épaisseur en moyenne.

Son extrémité Ouest semble avoir été taillée en pointe, mis à part le fait que cet épanellage ne touche pas la partie inférieure du monolithe ; on retrouve d'autres traces de taille tout le long du bord Nord et à sa base Est.

#### Historique

Ce monolithe nous a été signalé par [Badiola P.]{.creator} en [octobre 2011]{.date}.

</section>

<section class="monument">

### [Ascain]{.spatial} - [Larrun Erreka]{.coverage} - [Dalle plantée]{.type}

#### Localisation

Altitude : 515m.

Elle se trouve en contre bas de la piste qui, du plateau des Trois Fontaines, descend vers l'Ouest et la route d'Olhette.

#### Description

Dalle trapézoïdale plantée selon un axe Nord Nord-Est Sud Sud-Ouest, mesurant 1,40m de haut, 1,30m à sa base, et 0,20m d'épaisseur.

#### Historique

Dalle découverte par [Badiola P.]{.creator} en [mai 2012]{.date}.

</section>

<section class="monument">

### [Ascain]{.spatial} - [Mantobaita]{.coverage} - [Tumulus]{.type} ([?]{.douteux})

#### Localisation

Altitude : 95m.

Il est situé à une dizaine de mètres avant le petit ponceau qui franchit le ruisseau Larrunko erreka, sur la rive droite de ce dernier ; il est longé à l'Est par le sentier qui rejoint ce ponceau au parking.

#### Description

Tumulus pierreux, légèrement ovale, mesurant environ 9,50m x 5,50m et 0,60m de haut. Il est constitué d'un amoncellement de pierres roulées, plus ou moins de la taille d'un pavé. Au centre s'élève un bel arbre. Monument très douteux.

#### Historique

Tumulus découvert en [avril 2014]{.date} par [C. Blot]{.creator}.

</section>

<section class="monument">

### [Ascain]{.spatial} - [Mantobaita]{.coverage} - [Dalle plantée]{.type} ([?]{.douteux})

#### Localisation

Nous voudrions signaler la magnifique dalle plantée située en bordure de la propriété existant à droite de la route qui aboutit au parking.

Altitude : 95m.

#### Description

Cette dalle, de forme rectangulaire, allongée, mesure 1,90m de haut, 0,76m de large et 0,23m d'épaisseur ; tout son pourtour présente de nombreuses et très anciennes traces d'épannelage. Tout en étant incluse dans l'ancienne clôture en dalles, dont il reste quelques exemples, mais de forme plus carrée, on peut émettre l'hypothèse d'une réutilisation locale d'un monolithe trouvé ailleurs et ayant eu une finalité première bien différente... Sa base a été consolidée avec du ciment.

</section>

<section class="monument">

### [Ascain]{.spatial} - [Martinhaurren Borda]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 95m.

Ce tumulus est situé au flanc Sud-Est du mont Bizkarzun, à l'Est d'Ascain. Il est à 50m au Nord-Ouest de la borde « Martinhaurren borda » au milieu de la piste qui se rend de celle-ci au sommet de la colline, et à 4m à l'Ouest d'une clôture de barbelés qui entoure une vaste prairie artificielle dépendant de cette borde.

#### Description

Tumulus de 3,20m de diamètre environ, un peu plus étendu vers le Sud, du fait de sa construction sur un terrain en pente douce vers le Sud. Il est formé de petits blocs de [grès]{.subject} rose, de la taille du poing, amoncelés sur une faible hauteur de 0,30m environ.

#### Historique

Tumulus signalé par [Andraud A.]{.creator} en [février 2014]{.date}.

</section>

<section class="monument">

### [Ascain]{.spatial} - [Neguxola]{.coverage} - [Monolithe]{.type}

#### Localisation

Altitude : 487m.

Elle gît sur un sol en légère pente vers l'Est, à 200m environ de l'enclos de Neguxola (plateau Ihizelhaya).

#### Description

Dalle de [grès]{.subject} [triasique]{.subject} de forme parallélépipédique, allongée selon un axe Est-Ouest, mesurant 3m de long, à base plus large (1,88m) que le sommet (1,16m) ; la largeur dans la partie médiane est d'environ 1,55m. L'épaisseur de cette dalle varie entre 0,22m et 0,35m. Il semble qu'il y ait des traces d'épannelage sur les côtés, au Nord Nord-Est et au Sud Sud-Est.

#### Historique

Dalle découverte par [P. Badiola]{.creator} en [septembre 2014]{.date}.

</section>

<section class="monument">

### [Ascain]{.spatial} - [Olhette]{.coverage} 1 - [Tumulus-cromlech]{.type}

#### Localisation

Carte 1245 Ouest.

Altitude : 40m.

Il est à environ 500m à l'Est du hameau d'Olhette, à droite de la route qui se dirige vers Ascain, et à 25m au Sud de cette route, dans la propriété privée du Docteur Robert. Nous pensons utile de re-décrire ce monument, retrouvé par nous en 1970, mais déjà publié [@veyrinCromlechsPierresLevees1935, p. 219].

#### Description

[Pierre Dop]{.creator} a le premier décrit ce monument comme un tumulus mixte à prédominance pierreuse de 8m de diamètre et 0,60m de haut entouré d'un très beau péristalithe formé de dalles (en nombre non précisé) dont les dimensions pouvaient atteindre 0,80m à 1m de long et 0,20m à 0,30m de haut. Quand nous avons retrouvé ce monument en 1970, ces dalles avaient été arrachées depuis longtemps et le monument très détérioré.

#### Historique

Monument découvert par [Duhart J.]{.creator} en [1934]{.date}.

</section>

<section class="monument">

### [Ascain]{.spatial} - [Olhette]{.coverage} 2 - [Tumulus]{.type}

#### Localisation

À environ 100m au Nord de *Olhette 1 - Tumulus-cromlech*, mais de l'autre côté de la route et à 15m de celle-ci.

#### Description

Tumulus mixte de pierres et de terre de 18m de diamètre et 0,70m de haut. Il est en partie dissimulé par des accacias.

#### Historique

Monument découvert en [mars 1972]{.date}. Il a été depuis rasé par l'aménagement d'un lotissement et la construction d'une maison.

</section>

<section class="monument">

### [Ascain]{.spatial} - [Olhette]{.coverage} 3 - [Tumulus]{.type}

#### Localisation

Il est situé à 25m au Nord Nord-Est de *Olhette 2 - Tumulus*.

#### Description

Tumulus mixte de pierres et de terre, de 18m de diamètre et 0,50m de haut.

#### Historique

Monument découvert en [mars 1972]{.date}. Il a depuis été lui aussi rasé par la création d'un lotissement.

</section>

<section class="monument">

### [Ascain]{.spatial} - [Olhette]{.coverage} 4 - [Tumulus]{.type}

#### Localisation

Il est situé à 100m à l'Ouest de *Olhette 1 - Tumulus-cromlech*, et, comme lui, au Sud de la route Olhette - Ascain. Il est dans une vaste prairie de la propriété *Sainte-Hélène* séparée de celle du Dr Robert par un petit chemin bordé d'une haie. On note 4 tumulus : le premier décrit se trouve le plus à l'Est.

#### Description

Tumulus mixte de terre et de pierres (en général des galets) de 11m de diamètre 1m de haut.

#### Historique

Monument découvert en [mars 1972]{.date}.

</section>

<section class="monument">

### [Ascain]{.spatial} - [Olhette]{.coverage} 5 - [Tumulus]{.type}

#### Localisation

Il est à 40m au Nord Nord-Ouest de *Olhette 4 - Tumulus*.

#### Description

Tumulus terreux, en partie amputé de son quart sud, d'un diamètre de 11m et 0,30m de haut.

#### Historique

Monument découvert en [mars 1972]{.date}.

</section>

<section class="monument">

### [Ascain]{.spatial} - [Olhette]{.coverage} 6 - [Tumulus]{.type}

#### Localisation

Il est à environ 4m à l'Ouest Sud-Ouest de *Olhette 4 - Tumulus*.

#### Description

Tumulus mixte de terre et de galets de 16m de diamètre et 0,80m de haut.

#### Historique

Monument découvert en [mars 1972]{.date}.

</section>

<section class="monument">

### [Ascain]{.spatial} - [Olhette]{.coverage} 7 - [Tumulus]{.type}

#### Localisation

Il est à 5m au Nord-Est de *Olhette 6 - Tumulus*.

#### Description

Tumulus terreux de 13m de diamètre et de 0,30m de haut. Quelques pierres se voient à sa périphérie, sans que l'on puisse parler de péristalithe.

#### Historique

Monument découvert en [mars 1972]{.date}.

</section>

<section class="monument">

### [Ascain]{.spatial} - [Peruen Borda]{.coverage} - [Dolmen]{.type}

#### Localisation

Carte IGN 1245 OT Hendaye Saint-Jean-de-Luz.

Altitude : 137m.

On peut voir ce qui reste de ce dolmen à une cinquantaine de mètres à l'Ouest Nord-Ouest de la maison *Peruen Borda*, dans une prairie en situation dominante sur toute la plaine côtière, et à environ 150m à l'Ouest du *dolmen Xeruen* que nous avons décrit en 1971 [@blotNouveauxVestigesMegalithiques1971, p.38].

#### Description

Il ne reste qu'un tumulus pierreux, circulaire, d'environ 9m de diamètre et 0,50m de haut. Une dépression centrale de 2,50m de diamètre et quelques centimètres de profondeur, signale ce qui reste de la [chambre funéraire]{.subject}. Il est fort probable que les dalles constitutives de cette dernière aient été récupérées par les anciens habitants de ces lieux qui étaient carriers.

#### Historique

Ce monument a été signalé à [J. Blot]{.creator} en [décembre 2009]{.date} par l'occupant actuel de *Peruen Borda*, Mr. [J.F. Servier]{.creator}.

</section>

<section class="monument">

### [Ascain]{.spatial} - [Peruen Borda]{.coverage} - [Monolithe]{.type}, [dalle couchée]{.type} ([?]{.douteux})

#### Localisation

À une cinquantaine de mètres à l'Est du dolmen de ce nom, et à une dizaine de mètres au Nord de la maison *Peruen Borda*.

#### Description

Dalle de [grès]{.subject} [triasique]{.subject}, en forme de pain de sucre à grand axe Est-Ouest, et à sommet Ouest. Elle mesure 2,27m de long, 0,88m de large et 0,25m d'épaisseur en moyenne. Tout son bord Nord présente des traces d'épannelage.

#### Historique

Dalle découverte par [Blot J.]{.creator} en [novembre 2011]{.date}.

</section>

<section class="monument">

### [Ascain]{.spatial} - [Trois Fontaines]{.coverage} - [Dalle plantée]{.type} ([?]{.douteux})

#### Localisation

Altitude : 594m.

Elle est située sur le replat, couvert de résineux, qui domine le sentier menant aux Trois Fontaines. Elle est au-dessus du petit *dolmen des Trois Fontaines* que nous avons décrit [@blotNouveauxVestigesMegalithiques1971, p. 36]

Altitude 498m.

#### Description

Dalle de [grès]{.subject} plantée verticalement dans le sol, de forme triangulaire mesurant 0,98m de haut, 1,35m à sa base et 0,13m d'épaisseur. Il n'y a pas de tumulus, mais à l'évidence cette dalle a été volontairement plantée là ; borne ancienne, antique, protohistorique ? - ne correspond à aucune limite communale actuelle.

#### Historique

Dalle découverte par [I. Gaztelu]{.creator}, en [mars 1979]{.date}. Il existe une dalle plantée, de moindre importance, à une quinzaine de mètres au Sud-Ouest.

</section>

<section class="monument">

### [Ascain]{.spatial} - [Trois Fontaines]{.coverage} - Grande [dalle couchée]{.type} 1 ([?]{.douteux})

#### Localisation

Ressemblant à un véritable « monolithe », elle est située à 80m au Sud Sud-Ouest de *Trois Fontaines - Dalle plantée (?)*. Ses dimensions sont 3,35m de long, 1,78m de large, 0,31m d'épaisseur. Cette grande dalle, bien individualisée, ne présente cependant pas de traces d'épannelage, et surtout possède dans son environnement d'autres grandes pierres ou dalles, en sorte qu'il est très difficile de pouvoir la retenir comme « monolithe » vrai...Nous la citons pour montrer combien les problèmes d'identification, d'authenticité de ce type de monuments sont difficiles. Nous en donnerons deux autres exemples dans la commune de Sare.

#### Historique

Dalle découverte par [J. Blot]{.creator} en [2010]{.date}.

</section>

<section class="monument">

### [Ascain]{.spatial} - [Trois Fontaines]{.coverage} (Plateau des) - Grande [dalle couchée]{.type} 2 ([?]{.douteux})

#### Localisation

Altitude : 540m.

Ce grand bloc de [grès]{.subject} gris, en forme de pain de sucre, est couché au sol à une dizaine de mètres au Nord du GR 10.

#### Description

Il mesure 2,70m de long, 1,40m dans sa plus grande largeur, et 0,18m d'épaisseur. Cette pierre, qui pourrait très bien être un «monolithe» vrai, ne présente aucune trace d'épannelage, mais, surtout, l'environnement de ce plateau abonde en grandes pierres de tous ordres, de sorte qu'il est très difficile de se prononcer sur une éventuelle finalité...

#### Historique

Dalle découverte par [J. Blot]{.creator} en [mars 2010]{.date}.

</section>

<section class="monument">

### [Ascain]{.spatial} - [Trois Fontaines]{.coverage} 2 - [Tumulus]{.type} ou [Tertre d'habitat]{.type} ([?]{.douteux})

#### Localisation

Altitude : 540m.

Situé à 32m au Nord-Ouest de *Trois Fontaines 1 - Dolmen* (commune de Sare)

#### Description

Comme *Trois Fontaines 1 - Dolmen*, ce tumulus de terre et de pierres, de faible relief (0,10m de haut) mesure 8m de diamètre avec une dépression centrale de 3m de largeur et 0,40m de profondeur. Pourrait être un Tertre d'Habitat ?

On note à 4m au Nord-Est de ce tumulus une dalle plantée (*Trois Fontaines 3 - Dalle plantée*).

#### Historique

Tumulus découvert par [Meyrat F.]{.creator} en [avril 2012]{.date}.

</section>

<section class="monument">

### [Ascain]{.spatial} - [Trois Fontaines]{.coverage} 3 - [Dalle plantée]{.type}

#### Localisation

Située à 4m au Nord-Est de *Trois Fontaines 2 - Tumulus ou Tertre d'habitat (?)*.

#### Description

Dalle parallélépipédique de [grès]{.subject} local, verticalement plantée dans le sol, mesurant 0,90m de haut, 0,25m d'épaisseur, 0,44m à la base, et 0,40m dans sa partie haute.

#### Historique

Dalle découverte par [Meyrat F.]{.creator} en [avril 2012]{.date}.

</section>

<section class="monument">

### [Ascain]{.spatial} - [Trois Fontaines]{.coverage} 3 - [Tumulus]{.type} ou [Tertre d'habitat]{.type} ?

#### Localisation

Altitude : 540m.

Il est situé à 30m au Nord de *Trois Fontaines 2 - Tumulus ou Tertre d'habitat (?)*.

#### Description

Tumulus de terre et de pierres de faible hauteur (0,05m) mesure 8m de diamètre avec une dépression centrale de 3m de large et 0,40m de profondeur. Pourrait être un Tertre d'habitat ?

#### Historique

Tumulus découvert par [Meyrat F.]{.creator} en [avril 2012]{.date}.

</section>

<section class="monument">

### [Ascain]{.spatial} - [Trois Fontaines Erreka]{.coverage} - [Cromlech]{.type}

#### Localisation

Altitude : 540m.

Situé sur un beau replat planté de mélèzes et à une trentaine de mètres au Nord du sentier qui se rend au col des Trois fontaines, et à 4m au Sud du sentier qui vient de Trois Fontaines Erreka, plus bas.

#### Description

Une quinzaine de pierres, régulièrement espacées, délimitent un cercle de 12m de diamètre ; elles dépassent de peu le sol, mais sont bien visibles : les pierres de la moitié Est sont les plus volumineuses, certaines dépassant le mètre en longueur, et l'atteignant presque en largeur. On note une borne plantée dans le quart Sud-Est, inclinée vers le Sud (*Trois Fontaines Erreka - Pierre plantée*).

#### Historique

Cromlech découvert par [Meyrat F.]{.creator} en [avril 2012]{.date}.

</section>

<section class="monument">

### [Ascain]{.spatial} - [Trois Fontaines Erreka]{.coverage} - [Dalle couchée]{.type} 1

#### Localisation

Altitude : 445m.

Elle est située sur un replat de la crête qui sépare le ruisseau des Trois Fontaines de son affluent de gauche.

#### Description

Dalle de [grès]{.subject} local, grossièrement parallélépipédique, allongée selon un axe Nord-Est Sud-Ouest, mesurant 1,93m de long, 1,38m dans sa plus grande largeur, 1,06m à sa base et 0,33m d'épaisseur. Cette dalle présente des signes d'épannelage sur tout son pourtour Nord ainsi qu'à sa base Sud-Ouest.

#### Historique

Dalle découverte par [Meyrat F.]{.creator} en [avril 2011]{.date}.

</section>

<section class="monument">

### [Ascain]{.spatial} - [Trois Fontaines Erreka]{.coverage} - [Dalle couchée]{.type} 2

#### Localisation

Altitude : 435m.

Elle est à 400m à l'Est de *Trois Fontaines Erreka - Dalle couchée 1*. À 400m à l'Ouest coule le ruisseau Trois Fontaines Erreka.

#### Description

Elle repose au sommet d'un filon rocheux, dont elle est indépendante. Cette dalle de [grès]{.subject} local de forme triangulaire, mesure 2,80m de long, et 1,25m à sa base. Son épaisseur varie entre 0,30 et 0,40m. Il semble que sa base et une partie de son bord Nord-Est soient épannelés. Dalle de carriers « modernes » ?

#### Historique

Dalle découverte par [Meyrat F.]{.creator} en [avril 2012]{.date}.

</section>

<section class="monument">

### [Ascain]{.spatial} - [Trois Fontaines Erreka]{.coverage} - [Dolmen]{.type}

#### Localisation

Altitude : 540m.

Au pied de la crête d'Altxangue, à l'extrémité Ouest de celle-ci, et en bordure de la piste qui la contourne par le Nord.

#### Description

La description qui suit a été publiée en 2013 dans le Tome 5 de notre *Inventaire* sous la rubrique « Tumulus » et non « dolmen » comme actuellement. En effet d'après Claude Chauchat (communications personnelles), ce monument avait déjà été identifié comme dolmen (avant sa re-découverte en 2012 par F. Meyrat), par [G. Laplace]{.creator} et [J.M. de Barandiaran]{.creator}, il y a des années, alors que le monument était bien plus visible que maintenant (ayant, depuis, été encore plus détérioré par le passage d'engins.).

Tumulus pierreux, mesurant 7m de diamètre et 0,40m de haut, ayant été dégradé sur son flanc Est par le passage de la piste qui ampute son bord Sud. Au centre, 2 petites dalles verticales (0,80m de long et 0,23m de haut pour l'une et 0,52m de long et 0,20m de haut pour l'autre), pourraient représenter les vestiges de la [chambre funéraire]{.subject} ; une belle dalle de [grès]{.subject} au Nord de celle-ci, mesure 2,40m de long, 1,26m de large et 0,20m de haut et pourrait en avoir été le couvercle ; une autre dalle (2m x 1m), se voit au Sud, simplement posée sur le sol.

Il semblerait enfin que 7 pierres délimitent une partie de la périphérie Ouest du tumulus.

#### Historique

Monument re-découvert par [Meyrat F.]{.creator} en [avril 2012]{.date}.

</section>

<section class="monument">

### [Ascain]{.spatial} - [Trois Fontaines Erreka]{.coverage} - [Pierre plantée]{.type}

#### Localisation

Elle est plantée dans le quart Sud-Est de *Trois Fontaines Erreka - Cromlech*.

Altitude : 540m.

#### Description

Borne de forme grossièrement parallélépipédique, en [grès]{.subject} local, inclinée vers le Sud, mesurant 1m de haut et 0,30m d'épaisseur.

#### Historique

Pierre découverte par [Meyrat F.]{.creator} en [avril 2012]{.date}.

</section>

<section class="monument">

### [Ascain]{.spatial} - [Trois Fontaines Erreka]{.coverage} 1 - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 475m.

Il est sur un vaste terrain en pente douce, Est-Ouest, à une vingtaine de mètres à l'Ouest de la naissance d'un petit affluent Est du ruisseau des Trois fontaines. Une borde en ruine est à 400m au Nord-Est.

#### Description

Tertre de 10m de diamètre, asymétrique, de 0,40m dans sa plus grande hauteur.

#### Historique

Tertre d'habitat découvert par [Meyrat F.]{.creator} en [avril 2012]{.date}.

</section>

<section class="monument">

### [Ascain]{.spatial} - [Trois Fontaines Erreka]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 540m.

Au pied de la crête d'Altxangue, à l'extrémité Ouest de celle-ci, et en bordure du chemin qui la contourne par le Nord.

#### Description

Tumulus pierreux, mesurant 7m de diamètre et 0,40m de haut, ayant été dégradé sur son flanc Est par le passage de la piste qui ampute son bord Sud. Au centre, 2 petites dalles verticales (0,80m de long et 0,23m de haut ; 0,52m de long et 0,20m de haut), pourraient représenter les vestiges d'un [ciste]{.subject}. On note une pierre à la périphérie Nord qui mesure 2,40m de long, 1,26m de large et 0,20m de haut ; une autre pierre importante (2m x 1m), se voit au Sud, simplement posée sur le sol.

Il semblerait enfin que 7 pierres délimitent une partie de la périphérie, à l'Ouest.

#### Historique

Tumulus découvert par [Meyrat F.]{.creator} en [avril 2012]{.date}.

</section>

<section class="monument">

### [Ascain]{.spatial} - [Trois maisons]{.coverage} - [Dolmen]{.type}

#### Localisation

Carte IGN 1245 OT Hendaye Saint-Jean-de-Luz.

Altitude : 165m.

Ce monument très difficile à repérer, perdu dans un océan de fougères et de ronces, même en hiver, est situé sur un petit mamelon au flanc Nord de Larraun, à environ 100m au Sud du quartier dit « des 3 maisons ».

#### Description

On note un important tumulus de pierres atteignant semble-t-il au moins une quinzaine de mètres de diamètre. Au centre se distingue la [chambre funéraire]{.subject} : une profonde excavation rectangulaire orientée Est-Ouest, limitée au Nord et au Sud par deux importantes dalles verticales de [grès]{.subject} rose local. La dalle Nord mesure environ 3m de long et 1,45m de haut, son épaisseur atteint 0,35m en moyenne ; la dalle Sud possède les mêmes dimensions mais pour une épaisseur moindre (0,10m environ). On note des traces d'épannelage très nettes sur une grande partie de la périphérie de ces dalles.

#### Historique

Monument vu par [Blot J.]{.creator} en [décembre 2009]{.date}, avec l'aide de M. [J.F. Servier]{.creator}.

Il pourrait peut-être s'agir du dolmen de *Putxerri* signalé par J.M. de Barandiaran, [@barandiaranHombrePrehistoricoPais1953, p.244] bien que la description et les mensurations de ce dernier soient bien différentes de celui-ci...

</section>

<section class="monument">

### [Ascain]{.spatial} - [Xeruen Haut]{.coverage} Ouest - [Dolmen]{.type}

#### Localisation

Altitude :300m.

À l'extrémité Nord-Ouest d'un vaste plateau au flanc Nord de Larrun.

#### Description

Monument modeste, sans tumulus apparent et bien dégradé. La [chambre funéraire]{.subject}, qui mesure environ 2m de long et 0,90m de large, est orientée Nord-Sud. Elle est essentiellement marquée par une dalle plantée, à l'Est, mesurant 1,20m de long à sa base, 0,70m de haut et 0,15m d'épaisseur. À l'Ouest, et au Nord, deux dalles couchées, dont celle du Nord mesure 0,59m de long et 0,52m de large.

#### Historique

Monument signalé par le [groupe Hilharriak]{.creator} en [2011]{.date}.

</section>

<section class="monument">

### [Ascain]{.spatial} - [Xeruen Haut]{.coverage} - [Dalle plantée]{.type}

#### Localisation

Altitude : 300m.

Située à l'extrémité Sud-Est du plateau.

#### Description

Dalle de [grès]{.subject} de forme triangulaire, plantée verticalement selon un axe Nord-Sud., mesurant 1,10m à sa base, 0,90m de haut et 0,18m d'épaisseur.

#### Historique

Dalle découverte par [Blot J.]{.creator} en [mars 2012]{.date}.

</section>

<section class="monument">

### [Ascain]{.spatial} - [Xeruen Haut]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 300m.

Il est à 13m au Nord de *Xeruen Haut - Dalle plantée*.

#### Description

Petit tumulus, discret, de terre et de pierres, mesurant 3m de diamètre et 0,30m de haut.

#### Historique

Monument découvert par [Meyrat F.]{.creator} en [mars 2012]{.date}.

</section>

<section class="monument">

### [Ascain]{.spatial} - [Xeruen Haut]{.coverage} Est - [Dolmen]{.type}

#### Localisation

Altitude : 300m.

Il est à 27m au Nord-Est du *Tumulus du même nom*.

#### Description

Deux dalles parallèles, très inclinées vers l'Est, quasiment couchées au sol, paraissent être les seuls vestiges d'une [chambre funéraire]{.subject}, orientée Est-Ouest ; la dalle Ouest mesure 1m à sa base, 0,70m de haut, celle à l'Est : 0,90m à sa base, 0,53m de haut et 0,14m d'épaisseur en moyenne.

#### Historique

Monument signalé par le [groupe Hilharriak]{.creator} en [2011]{.date}.

</section>

<section class="monument">

### [Ascain]{.spatial} - [Xeruen Haut]{.coverage} - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 280m.

Situé en bordure Nord d'une piste qui descend en oblique vers l'Ouest au flanc Nord de Larrun.

#### Description

Tertre de 6m de diamètre, 0,40m de haut, asymétrique, érigé sur un terrain en pente vers le Nord.

#### Historique

Tertre découvert par [Blot J.]{.creator} en [mars 2012]{.date}.

</section>

<section class="monument">

### [Ascain]{.spatial} - [Xeruen haut]{.coverage} 2 - [Dalle couchée]{.type}

#### Localisation

Altitude : 304m.

Il est à proximité des monuments déjà cités sous la rubrique « Xéruen haut », [@blotInventaireMonumentsProtohistoriques2013, p.7], et à 7m au Sud-Est des ruines d'une borde et d'un enclos de pierres sèches.

#### Description

Dalle de [grès]{.subject} [triasique]{.subject}, couchée au sol, en forme « de pain de sucre, mesurant 2,27m de long avec une base de 0,70m et un sommet plus étroit de 0,50m ; son épaisseur est d'une vingtaine de centimètres en moyenne. Cette dalle présente des traces d'épannelage tout le long de son bord Nord-Ouest, le sommet et la base ayant aussi des traces de régularisation - Dans le contexte de carrière de ces lieux, rien ne permet de supposer une grande ancienneté à ces traces.

#### Historique

Dalle découverte par [A. Martinez]{.creator} en [2013]{.date}.

</section>

<section class="monument">

### [Ascain]{.spatial} - [Xuanenborda]{.coverage} - [Grande dalle]{.type}, [dolmen]{.type} ([?]{.douteux})

#### Localisation

Altitude : 365m.

Elle est située à environ une centaine de mètres en contre-bas et à gauche du GR 10 qui descend des Trois Fontaines vers Olhette.

#### Description

Elle repose sur un « tumulus » d'une quinzaine de mètres de diamètre, formé de gros blocs de [grès]{.subject} et mesure 3m de long dans son grand axe Est-Ouest, 2,44m de large dans l'axe Nord-Sud et 0,35m d'épaisseur ; un gros fragment s'est détaché de son bord Sud-Ouest. Son bord Nord paraît présenter des traces d'épannelage. Est-ce la dalle de [couverture]{.subject} d'un grand dolmen ?, un « monolithe » ? ou une dalle naturellement venue là ?

#### Historique

Dalle découverte par [J. Blot]{.creator} en [mars 2010]{.date}.

</section>

<section class="monument">

### [Ascain]{.spatial} - [Zelaia]{.coverage} Nord 1 - [Monolithe]{.type}

#### Localisation

Carte IGN 1245 OT Hendaye Saint-Jean-de-Luz.

Altitude : 220m.

Ce monument, est situé sur le flanc Nord de la Larraun, à l'extrémité Nord-Ouest du pierrier issu du plateau Ihicelaya. À quelques mètres, du côté Ouest, coule un petit riu où viennent s'abreuver les animaux qui paissent en ces lieux. Monument difficile à trouver compte tenu d'un relief très tourmenté et d'une abondante végétation.

#### Description

Belle dalle de [grès]{.subject} rose plantée quasiment à la verticale sur un terrain en légère pente vers le Nord-Ouest ; on note qu'au Sud-Est elle prend appui sur trois autres dalles plus petites qui lui servent de blocage amont par rapport à l'axe de la pente. Cette dalle de forme grossièrement trapézoïdale, mesure 3,24m dans sa plus grande hauteur, 2,50m à sa base, et 1,70m à sa partie moyenne. Son épaisseur varie de 0,40m à sa base à 0,16m à sa partie supérieure. Elle présente des traces d'épannelage sous forme d'enlèvements de gros éclats, tant dans ses bords verticaux qu'au sommet.

#### Historique

Monument découvert par [A. Martinez Manteca]{.creator} en [1999]{.date}.

</section>

<section class="monument">

### [Ascain]{.spatial} - [Zelaia]{.coverage} Nord 2 - [Monolithe]{.type}

#### Localisation

Carte IGN 1245 OT Hendaye Saint-Jean-de-Luz.

Altitude : 125 m.

Il est situé à 70m environ, au Sud-Ouest d'une maison récemment construite à l'extrémité haute du second chemin qui part vers la droite, sur la route Olhette-Ascain.

#### Description

Importante dalle de [grès]{.subject} rose de forme ovoïde, gisant sur le sol, orientée Nord-Sud ; elle mesure 2,77m de long, de 1,56m à 1,72m de large. Son épaisseur varie, suivant les endroits de 0,14m à 0,44m. Son bord Ouest présente, semble-t-il, des traces d'épannelage, ainsi que sa base Sud. La végétation cache le bord Est. Elle est la seule dalle de ce type dans les environs immédiats.

#### Historique

Monument découvert par [J. Blot]{.creator} en [janvier 2010]{.date}.

</section>

<section class="monument">

### [Ascain]{.spatial} - [Zelaia]{.coverage} 2 - [Dolmen]{.type}

#### Localisation

Altitude : 130m.

Ce monument (douteux), très voisin de *Zelaia Nord 2 - Monolithe*, est situé lui aussi à environ 70m, mais au Sud de la maison récemment construite à l'extrémité haute du second chemin qui part vers la droite, sur la route Olhette-Ascain.

#### Description

On note deux dalles de [grès]{.subject}, bien visibles et parallèles qui pourraient délimiter une [chambre funéraire]{.subject} orientée Nord-Sud. La dalle est mesure 2m de long, 0,70m de haut et son épaisseur est d'environ 0,12m. Elle est séparée de la dalle Ouest par 0,70m. Celle-ci mesure 1,60m de long, 0,60m de haut et 0,14m d'épaisseur. Il n'y a pas de tumulus visible. À quelques dizaines de centimètres au Sud se distingue une dalle en grande partie enfouie.

#### Historique

Monument découvert en [mars 1990]{.date} par [I. Gaztelu]{.creator}.

</section>

# Aussurucq

<section class="monument">

### [Aussurucq]{.spatial} - [Elsarreko Ordoki]{.coverage} - [Tertres]{.type} et [tumulus-cromlech]{.type}

Le haut-plateau d'Elsarreko Ordoki, (polgé) qui s'étend à l'Est du pic Zabozé, recèle de nombreux vestiges dont 18 ont été identifiés au cours des temps par [Blot J.]{.creator}, [Ebrard D.]{.creator}, et [D. Sarramagnan]{.creator}. On a ainsi pu distinguer : un tumulus-cromlech, 11 tertres pierreux, une dalle « mégalithique » (n°8) déplacée et ensevelie en 1995, un gîte à petits rognons de silex,( n°9) deux meules de charbonnier, (n°10 et 11), un cayolar en ruine, (n°15). Nous citerons et localiserons ici tertres et tumulus-cromlech.

-   *Tertre pierreux 1*

    Altitude : 735m.

    Mitoyen du cayolar Pagolen Olha. Diamètre approximatif : 9m x 9m, 1m de haut.

    (inventeur : D. Ebrard, [1972]{.date}).

-   *Tertre pierreux 2*

    Altitude : 735m.

    Tertre pierreux, mesurant 10m de diamètre, 0,90m de haut, avec dépression centrale de 0,80m de profondeur.

    (inventeur : Blot J., [1979]{.date})

-   *Tumulus-cromlech 3*

    Altitude: 730m.

    Il mesure environ 6m de diamètre et 0,30m de haut. Un péristalithe d'environ 6 pierres est visible à la périphérie du tumulus.

    (inventeur : Blot J., 1979)

-   *Tertre pierreux 4*

    Altitude : 740m. A été remanié superficiellement en 1995 et fouillé par D. Ebrard en 1996.

    Mesure 6m de diamètre et 0,20m de haut.

    (Inventeur : Blot J., 1979)

-   *Tertre pierreux 5*

    Altitude : 740m.

    Remanié par les engins en 1995 ; mesure 9m de diamètre et 0,80m de haut. TH ?

    (Inventeur : D. Ebrard, [1993]{.date}).

-   *Tertre pierreux 6*

    Tangent au précédent au Sud. Remanié par engins en 1993. Mêmes dimensions que le précédent.

    (inventeur : D. Ebrard 1993).

-   *Tertre (?) n°7*

    Altitude : 730m.

    (inventeurs : D. Ebrard et Sarramagnan, [1996]{.date})

-   *Tertre pierreux 12*

    Altitude : 740m.

    Remanié en 1995 - mesure 6,50m de diamètre et 0,20m de haut. Fouilllé en 1996.

    (inventeurs : D. Ebrard et Sarramagnan, 1996)

-   *Tertre pierreux 14*

    Altitude : 740m.

    Arasé en 1995.

    (Inventeurs : D. Ebrard et Sarramagnan, 1996).

-   *Tertre 16*

    Altitude : 750m.

    Arasé en 1995.

    (inventeurs : D. Ebrard et Sarramagnan, 1996).

-   *Tertre 17 *

    Altitude : 740m.

    Défriché en 1995. Mesure 6m de diamètre et 0,20m de haut.

    (inventeurs : D. Ebrard et Sarramagnan, 1996).

-   *Tertre 18 *

    Altitude : 750m.

    Tout en bordure de la piste routière ; Mesure 8m de diamètre et 1 m de haut.

    Défriché en 1995.

    (Inventeur : Blot J., 1979).

#### Historique

On trouvera les détails dans [@ebrard50AnsArcheologie2013, p.285] de la bibliographie des monuments.

</section>

<section class="monument">

### [Aussurucq]{.spatial} - [Ilhasteria]{.coverage} 2 - [Tumulus]{.type}

#### Localisation

Carte 1446 Ouest Ordiarp.

Altitude : 1140m.

Il est situé à 200m au Nord-Est du sommet de la colline Ilhastéria (au Nord-Est de la fontaine d'Ahuski), et à 400m au Sud-Est du cayolar Nabolégui, en bordure de crête ; une vue magnifique sur la chaîne de montagnes s'étend devant lui, à l'Est.

Nous avons décrit le *n°1* en 1979 [@blotSouleSesVestiges1979, p.7].

Ce tumulus n°2 est situé à 1m à l'Ouest Sud-Ouest du *n°1*.

#### Description

Tumulus mixte, de terre et de pierres [calcaires]{.subject} blanches, mesurant 5m de diamètre et 0,40m de haut, et bien que sa périphérie en soit un peu irrégulière, il évoque parfaitement, en plus petit, la facture de son grand voisin.

#### Historique

Tumulus découvert en [octobre 1971]{.date}. Nous avons demandé au professeur Jean Haristchelhar la signification du mot *Ilhasteria* ; sans que ce terme puisse être traduit mot pour mot, il nous a répondu que cela évoquait « l'idée de mort rapide ». Cela correspondrait fort bien à ce site connu pour la fréquence avec laquelle frappe la foudre ; par ailleurs, la difficulté à identifier ces monuments faits d'amoncellement de pierraille dans un contexte lui-même riche en roches, nous conforte dans l'idée que nous avions déjà suggérée pour les tumulus *Ilhareko-lepoa 1* et *2*, à savoir que la dénomination du lieu a été donnée à l'époque de la construction de ces monuments funéraires, et non depuis.

</section>

<section class="monument">

### [Aussurucq]{.spatial} - [Ilhasteria]{.coverage} 3 - [Tumulus]{.type}

#### Localisation

Altitude : 1140m.

Il est à 4m à l'Ouest du *tumulus n°1* déjà décrit par nous [@blotSouleSesVestiges1979, p.7].

#### Description

Petit tumulus circulaire en forme de galette, de 4m de diamètre et 0,40m de haut, formé de terre et de nombreuses pierres locales ; il semble qu'on puisse distinguer un péristalithe.

#### Historique

Ce tumulus a été aperçu en [1971]{.date} par [Blot J.]{.creator} et confirmé ici.

</section>

<section class="monument">

### [Aussurucq]{.spatial} - [Ilhasteria]{.coverage} - [Cromlech]{.type}

#### Description

Très petit cercle de pierres de 0,90m de diamètre délimité par une douzaine d'éléments au ras du sol ; à l'intérieur, 6 autres éléments sont visibles.

#### Historique

Monument découvert par [Blot J.]{.creator} en [mai 2013]{.date}.

</section>

<section class="monument">

### [Aussurucq]{.spatial} - [Ithé]{.coverage} 6 - [Tumulus]{.type}

#### Localisation

Altitude : 700m.

Il est situé à 60m environ à l'Ouest de la route Aussurucq - Mendive.

#### Description

Tumulus pierreux, formé de petits et moyens blocs de [calcaire]{.subject} blanc, mesurant environ 10m de diamètre et 0,50m de haut.

#### Historique

Monument découvert par [Blot J.]{.creator} en [avril 2013]{.date}.

</section>

<section class="monument">

### [Aussurucq]{.spatial} - [Léchareguibela]{.coverage} - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 906m.

Il est au bout du chemin aménagé pour atteindre le cayolar de ce nom, et à 10m au Nord-Est de ce dernier.

#### Description

Tertre de terre et de pierraille de forme ovale à grand axe Sud-Est Nord-Ouest, mesurant 9m x 6m, et présentant une dépression centrale. Erigé sur terrain en pente vers le Nord-Ouest.

#### Historique

Tertre découvert par [Blot J.]{.creator} en [avril 2013]{.date}.

</section>

<section class="monument">

### [Aussurucq]{.spatial} - [Ohaté]{.coverage} 2 - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 869m.

Il est à une vingtaine de mètres en contre-bas des ruines du cayolar Ohaté, lequel est tangent au *TH n°1* (et à un second, évoqué, mais peu probable) que nous avons déjà publié en 1979 [@blotSouleSesVestiges1979, p. 10]. En bordure de piste.

#### Description

Tertre de terre et de pierrailles, dissymétrique en hauteur (maximum 1m de haut) et mesurant 6,50m de diamètre environ.

#### Historique

Tertre découvert par [Blot J.]{.creator} en [avril 2013]{.date}.

</section>

<section class="monument">

### [Aussurucq]{.spatial} - [Olhatzezarre]{.coverage} (groupe Ouest) - [Tertres d'habitat]{.type}

#### Localisation

Altitude : 1000m.

#### Description

-   *TH 1* : Au fond d'une grande doline on note un grand tertre de terre circulaire, (le plus au Sud). D'une dizaine de mètres de diamètre, avec dépression centrale ouverte vers l'Ouest.

-   Le *TH 2* lui est tangent au Nord, de moindre dimension, et sans dépression centrale.

-   Le *TH 3* est plus au Nord, sur les bords de la doline, et de moindre dimensions.

#### Historique

Monuments vus par [Blot J.]{.creator} en [1971]{.date}, et succinctement décrits [@blotSouleSesVestiges1979, p.7].

</section>

<section class="monument">

### [Aussurucq]{.spatial} - [Olhatzezarre]{.coverage} (groupe Est) - [Tertres d'habitat]{.type}

#### Localisation

Altitude : 1000m.

Les 7 tertres d'habitat sont à une cinquantaine de mètres à l'Est du groupe précédent.

#### Description

7 TH sont répartis sur une pente douce inclinée vers l'Est. Les dimensions en sont variables, mais atteignent souvent 7m à 9m de diamètre et 0,80m de haut. Le *TH 2* est à 15m au Sud du *TH 1* (qui est le plus au Nord). Le *TH 3* est à 7m au Sud du n°2. Le *TH 4* est à 6m à l'Ouest du n°3. Le *TH 5* est à 6m au Sud-Est du n° 4. Le *TH 6* est à 20m au Sud du n°5. Le *TH 7* est à 4m à l'Est du n° 6.

On notera enfin un dernier *TH (Sud) effondré* dans une petite doline, à l'extrémité Sud du plateau, à l'amorce de la piste qui redescend vers le cayolar Olhatzezarre. (Blot J. avril 2013).

#### Historique

TH vus par [Blot J.]{.creator} en [1971]{.date}, et succinctement décrits [@blotSouleSesVestiges1979, p.7].

</section>

# Banca

<section class="monument">

### [Banca]{.spatial} - [Abrakou]{.coverage} - [Dolmen]{.type}

#### Localisation

Carte 1346 Ouest.

Altitude : 900m.

Il est érigé en bordure du chemin de ligne de crête qui rejoint Ichtauz à Abrakou-lepoa. Il est situé à 120m à l'Est de la BF 112.

#### Description

Tumulus pierreux, d'environ 8 à 9m de diamètre et 1m de haut, formé de petits blocs de [grès]{.subject} particulièrement visibles dans sa partie Sud-Ouest, et moins au Nord-Est où a poussé un arbre à environ 2m du centre.

Comme dans le cas du *dolmen de Berdaritz*, situé sur la même ligne de crête, à environ 2000m plus au Sud-Ouest, il semble que l'on puisse distinguer 2 chambres funéraires :

Une [chambre funéraire]{.subject} au Sud-Est, orientée Est Nord-Est, d'environ 1,50m de long et 0,80m de large, bien délimitée par 4 dalles dont 3 sont déversées vers l'intérieur de la chambre.

Une chambre à 1m au Nord-Ouest de la précédente, visible sous la forme d'une dépression de 1,40m x 1m, sans dalles visibles, sans doute le résultat probable d'une fouille ancienne ayant démoli la structure originelle. Il n'y a pas de couvercle(s) visible(s).

#### Historique

Dolmen découvert en [août 1975]{.date}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Antchola]{.coverage} - [Monolithe fendu]{.type}

#### Localisation

Altitude : 1118m.

Il est situé à une vingtaine de mètres au Sud Sud-Ouest de la BF 105.

#### Description

Volumineux bloc de [grès]{.subject} rose rectangulaire couché au sol suivant un axe Est-Ouest, mesurant 1,96m de long, 1,38m de large à sa base et 1,19m dans sa partie moyenne (la séparation étant incluse) ; épaisseur de 0,43m. Il semble présenter des traces d'épannelage à son bord Nord. À l'évidence, cette séparation est soit due à l'action du gel... ou de l'homme.

#### Historique

Monument découvert par [F. Meyrat]{.creator} en [avril 2010]{.date}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Antchola]{.coverage} 1 - [Cromlech]{.type}

#### Localisation

Altitude : 1086m.

Il est situé sur un petit replat, dans la montée vers la BF 106 en partant du col d'Ixtauz, à une trentaine de mètres à l'Ouest de la pente Ouest abrupte du mont Antchola.

#### Description

De nombreuses petites pierres ou dalles de [grès]{.subject} disposées en couronne de 2,50m de diamètre, entourent un amas central de même type.

#### Historique

Monument découvert par [J. Blot]{.creator} en [avril 2010]{.date}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Antchola]{.coverage} 2 - [Cromlech]{.type} [(?)]{.douteux}

#### Localisation

Il est situé à 9m à l'Est Sud-Est de *Antchola 1 - Cromlech*, sur terrain plat.

#### Description

Environ 13 pierres au ras du sol délimitent un cercle de 2,60m de diamètre.

#### Historique

Monument (douteux ?) découvert par [J. Blot]{.creator} en [avril 2010]{.date}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Antchola]{.coverage} 3 - [Cromlech]{.type} [(?)]{.douteux}

#### Localisation

Au niveau de la BF 106.

Altitude : 1100m.

#### Description

On note, entourant l'implantation de la BF 106, un léger relief circulaire de 6m de diamètre environ, fait de petites dalles de [grès]{.subject} ; de plus il semble que l'on puisse distinguer quelques pierres d'un éventuel péristalithe. Enfin, il ne semble pas que l'on puisse attribuer au renforcement de l'implantation de la BF 106 ce relief tumulaire. Monument douteux, mais possible, l'implantation d'une borne frontière au milieu d'un monument n'étant pas exceptionnelle...

#### Historique

Monument découvert par [I. Gaztelu]{.creator}, [G. Mercader]{.creator} et [L. Millan]{.creator} en [novembre 2007]{.date}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Antchola]{.coverage}  - Deux [tumulus-cromlechs]{.type} [(?)]{.douteux}

#### Localisation

En montant du col d'Ichtauz vers le sommet d'Antchola, à une dizaine de mètres à l'Ouest de la piste, près d'un gros rocher, il semble que l'on puisse voir deux monuments (?) très semblables à des tumulus-cromlechs, distants l'un de l'autre de 5m environ, selon un axe Nord-Sud, sur un terrain assez pentu.

Altitude : 1045m.

#### Description

Ils mesurent tous deux environ 6m de diamètre, et sont constitués de pierres inclinées, disposées semble-t-il de façon circulaire autour d'autres pierres en désordre au centre.

#### Historique

Monuments découverts par [J. Blot]{.creator} en [avril 2010]{.date}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Argaray]{.coverage} 1 - [Cromlech]{.type} [(?)]{.douteux}

#### Localisation

Altitude : 1206m.

Ce monument, se trouve sur la ligne de crête du mont Argaray, à 3m à l'Ouest de la clôture de barbelés qui matérialise la frontière, à environ 120m au Sud de la BF 171, et à 30m au Sud d'une barre rocheuse perpendiculaire à l'axe de la ligne de crête. Erigé sur un terrain en légère pente vers l'Ouest.

#### Description

Une bonne vingtaine de pierres de calibres variés, délimitent un cercle approximatif de 4,10m de diamètre ; certaines mesurent plus de 1m de long et 0,45m de large, dépassant la surface du sol de 0,20m à 0,40m. D'autres éléments apparaissent à l'intérieur de ce cercle (?). Traces possibles d'excavation ou de dépression légère au centre de ce cercle... Il semblerait enfin qu'un demi-cercle (de 8,20m de diamètre), formé de 11 pierres plus ou moins au ras du sol, double le premier cercle (?), dans sa moitié Sud. Au total, monument très douteux, (peut être une simple disposition naturelle...) mais que nous ne pouvons totalement exclure de l'inventaire.

#### Historique

Cercle découvert par [P. Velche]{.creator} en [avril 2015]{.date}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Argaray]{.coverage} 2 - [Cromlech]{.type}

#### Localisation

Altitude : 1217m.

Situé toujours sur la ligne de crête, à 6m à l'Ouest du barbelé frontière et à 140m environ au Sud de la grande barre rocheuse.

#### Description

12 pierres au ras du sol, mais bien visibles, délimitent un petit cercle de 3,40m de diamètre, avec un léger tumulus central de 0,10m de haut, nettement visible.

#### Historique

Cercle découvert par [P. Velche]{.creator} en [avril 2015]{.date}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Argaray]{.coverage} 3 - [Cromlech]{.type} [(?)]{.douteux}

#### Localisation

Il est tangent au Sud-Ouest de *Argaray 2 - Cromlech*. Terrain en pente légère vers l'Ouest.

#### Description

Petit cercle de 2,60m de diamètre, délimité par 8 petites pierres, au ras du sol. Monument douteux.

#### Historique

Cercle découvert par [F. Meyrat]{.creator} en [mai 2015]{.date}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Argaray]{.coverage} 4 - [Cromlech]{.type} [(?)]{.douteux}

#### Localisation

Tangent à l'Ouest de *Argaray 3 - Cromlech (?)*. Terrain en pente légère vers l'Ouest.

#### Description

Cercle (?) de 1,40m de diamètre, délimité par 9 pierres de dimensions variables, mais modestes dans l'ensemble. Monument douteux.

#### Historique

Cercle découvert par [F. Meyrat]{.creator} en [mai 2015]{.date}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Argaray]{.coverage} 5 - [Cromlech]{.type}

#### Localisation

Altitude : 1212m.

Il se trouve à 90m au Sud Sud-Est de *Argaray 1 - Cromlech*, à 3m au Nord d'un filon rocheux et à 20m au Nord de *Argaray 2 - Cromlech*.

#### Description

9 pierres au ras du sol, mais bien visible délimitent un cercle de 2,40m de diamètre, avec très léger tumulus central de 0,15m de haut.

#### Historique

Monument découvert par [F. Meyrat]{.creator} en [mai 2015]{.date}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Argaray]{.coverage} bas - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 1076m.

Les 2 tertres d'habitat sont situés à une quinzaine de mètres au Sud-Est d'un très volumineux amas rocheux et à une quarantaine de mètres à l'Ouest d'un point d'eau.

#### Description

Ces deux tertres de terre, tangents, sont érigés sur un terrain en légère pente vers le Nord-Est. Ils sont de dimensions identiques : diamètre de 5m environ et 0,60m de haut.

#### Historique

Tertres découverts par [F. Meyrat]{.creator} en [août 2014]{.date}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Argaray]{.coverage} haut - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 1178m.

Il est situé à 80m au Nord de la BF 171, à 1m au Nord-Est du barbelé de la ligne frontière, et à 12m au Sud-Ouest d'un pointement rocheux.

#### Description

Tertre discret de 9m à 10m de diamètre, et 0,30 à 0,40m de haut, présentant une légère dépression centrale ; 7 pierres sont visibles en surface.

#### Historique

Monument trouvé par [F. Meyrat]{.creator} en [août 2014]{.date}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Argaray]{.coverage} - [Monolithe]{.type}

#### Localisation

Altitude : 1181m.

Territoire navarrais, tangent à la commune de Banca.

Il se trouve sur un terrain en pente vers l'Est, à 2m à l'Est du barbelé frontière, et à 120m environ au Nord Nord-Est de la BF 171. On note un point d'eau à 20m à l'Est de ce monolithe.

#### Description

Bloc de [calcaire]{.subject} de forme parallélépipédique, allongée selon un axe Sud-Ouest Nord-Est, à sommet en pointe au Nord-Est. Il mesure 2,90m de long, 1,30m de large en moyenne ; son épaisseur à la base est de 0,34m et au sommet de 0,18m. Il présente sur tout son pourtour des traces évidentes d'épannelage.

#### Historique

Monolithe découvert par [F. Meyrat]{.creator} en [mai 2014]{.date}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Axistoi]{.coverage} 3 - [Tumulus]{.type}

#### Localisation

Carte 1346 Ouest Saint-Etienne-de-Baïgorry.

Altitude : 1220m.

Il est situé sur cette longue ligne de crête qui borde, à l'Est, la Vallée des Aldudes.

Nous avons publié en 1972 [@blotNouveauxVestigesMegalithiques1972c, p.196], deux cromlechs : *le n°1*, de 4m de diamètre et 12 pierres, à 25m au Nord-Est de la borne-frontière n°157 ; *le n°2*, à 12m au Nord-Est du précédent, mesure 4m de diamètre, et possède 5 pierres.

#### Description

Tumulus à prédominance pierreuse (nombreuses petites plaquettes de [schiste]{.subject}), mesurant 5m de diamètre et 0,30m de haut. Une dépression centrale de 1m de diamètre et 0,30m de profondeur pourrait être la trace d'une ancienne fouille pratiquée au niveau de la [ciste]{.subject} centrale.

#### Historique

Monument découvert en [novembre 1974]{.date}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Ehunzaroy]{.coverage} (Hegoalde) - [Cromlech]{.type}

#### Localisation

Altitude : 977m.

Situé en territoire de Valcarlos

Ce monument, érigé sur un terrain en pente très douce vers le Nord-Est, est situé à 80m au Sud Sud-Ouest du col d'Ehunzaroy et de la piste qui le traverse, et à une dizaine de mètres au Sud-Est d'une clôture de barbelés qui symbolise la ligne frontière.

#### Description

Cercle de 12m de diamètre, délimité par 6 pierres de dimensions très variables : l'une, au Nord-Est atteint 1,23m x 0,30m, l'autre au Sud-Ouest de 0,80m x 0,60m ; petite pierre centrale.

On note un léger tumulus central, n'excédant pas 6m de diamètre et 0,30m de haut.

#### Historique

Monument découvert par [F. Meyrat]{.creator} en [Août 2014]{.date}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Ehunzaroy]{.coverage} (col) 1 - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 978m.

Situé à 27m au Nord de la route qui aboutit au col, avant les panneaux solaires.

#### Description

Tertre classique, dissymétrique, sur terrain en pente vers le Nord, mesurant 8m x 6m et 0,40m de haut environ.

#### Historique

Monument découvert par [F. Meyrat]{.creator} en [septembre 2014]{.date}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Ehunzaroy]{.coverage} (col) 2 - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 976m.

À 6m au Nord du bord de la route, un peu avant la barrière canadienne.

#### Description

Vaste tertre dissymétrique, érigé sur terrain en pente vers Nord-Est de 12m x 12m et de 1m de haut environ. Il domine le fond du vallon que forme le col.

#### Historique

Monument trouvé par [F. Meyrat]{.creator} en [septembre 2014]{.date}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Ehunzaroy]{.coverage} (col) 3 - [Tertre d'habitat]{.type}

#### Localisation

À 2m au Sud-Est de *Ehunzaroy col 2 - Tertre d'habitat* et tangent à la route.

#### Description

Tertre de dimensions beaucoup plus modestes, mesurant 7m x 5m et 0,40m de haut.

#### Historique

Monument découvert par [Blot J.]{.creator} en [septembre 2014]{.date}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Ehunzaroy]{.coverage} (col) 4 - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 976m.

Il est à 3m au pied et au Nord du vaste *Ehunzaroy col 2 - Tertre d'habitat*, et à 8m environ au Sud-Ouest du petit ru du fond de vallon.

#### Description

Tertre sur terrain en pente vers Nord Nord-Est, dissymétrique, mesurant 8m x 5m et 0,40m de haut.

#### Historique

Monument découvert par [F. Meyrat]{.creator} en [septembre 2014]{.date}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Errola]{.coverage} Nord 1 - [Tumulus]{.type} [(?)]{.douteux}

#### Localisation

Altitude : 856m.

Il est situé le à côté de la piste de crête qui relie le mont Errola au mont Otsamunho près de la naissance d'un petit ru.

#### Description

Ce tumulus (douteux ?), terreux, mesure 4,60m de diamètre et 0,40m de haut. À son sommet on note une légère dépression centrale.

#### Historique

Tumulus découvert par [F. Meyrat]{.creator} en [novembre 2014]{.date}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Errola]{.coverage} Nord 2 - [Tumulus]{.type}

#### Localisation

Altitude : 845m.

Situé à quelques dizaines de mètres au Nord-Est de *Errola nord 1 - Tumulus*, à la confluence de deux pistes pastorales, à 4m au Sud-Ouest de la piste nouvelle qui se rend du col de Mizpira à un nouveau cayolar plus au Sud.

#### Description

Tumulus terreux de 7,20m de diamètre, peu élevé (0,20m) mais néanmoins fort bien visible. Une pierre apparaît dans sa périphérie Sud Sud-Ouest et on note une dépression centrale, peu marquée, de 1,40m de diamètre environ.

#### Historique

Tumulus découvert par [Blot J.]{.creator} en [décembre 2014]{.date}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Haradoko lepoa]{.coverage} - [Pierres couchées]{.type} ([?]{.douteux})

La première est située à 9m au Nord Nord-Est de la BF 165 : cette pierre, de forme parallélépipédique, qui mesure 1,40m de long, 0,78m de large, 0,38m d'épaisseur, couchée selon un axe Nord-Sud, présente un sommet Sud arrondi très probablement de main d'homme ; elle ne porte aucune inscription, mais semble bien avoir été volontairement amenée là... quand et pourquoi ? ancienne borne frontière ?

Une autre pierre couchée parallélépipédique, de l'autre côté de la frontière, est située à 4m au Nord Nord-Est de la BF 165, et paraît-elle aussi avoir subi quelque régularisation ; elle mesure 1,55m de long, 1m de large et 0,19m d'épaisseur.

([J. Blot]{.creator} et [F.Meyrat]{.creator} - [avril 2010]{.date})

</section>

<section class="monument">

### [Banca]{.spatial} - [Harrigorri]{.coverage} - [Cromlech]{.type}

#### Localisation

Carte 1346 Ouest Saint-Etienne-de-Baïgorry.

Altitude : 1020m.

Il est sur la ligne de crête qui borde à l'Ouest la Vallée des Aldudes, et au flanc Sud-Est du mont Autza, sur un petit replat à une quarantaine de mètres à l'Est de la verticale du rocher dénommé *Harrigorri*.

#### Description

Cercle de pierres de 5,50m de diamètre, délimité par une vingtaine de pierres, au ras du sol.

#### Historique

Monument découvert en [juin 1971]{.date}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Harrigorri]{.coverage} 2 - [Cromlech]{.type}

#### Localisation

Altitude : 1117m.

Erigé sur un petit sommet dominant le col.

#### Description

Cromlech surélevé mesurant 3,70m de diamètre et délimité par une quinzaine de pierres.

#### Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Harrigorri]{.coverage} 3 - [Cromlech]{.type}

#### Localisation

À 20m au Nord-Est de *Harrigorri 2 - Cromlech*.

#### Description

Petit cercle de 1,80 m de diamètre, légèrement en relief, érigé sur un terrain en pente douce vers le Nord-Ouest et délimité par une quarantaine de petites pierres.

#### Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Hortz-Zorroz]{.coverage} 3 - [Tumulus]{.type}

#### Localisation

Carte 1346 Ouest Saint-Etienne-de-Baïgorry.

Altitude : 1023m.

Monument faisant partie de ceux échelonnés sur la longue crête montagneuse, étendue dans la Vallée des Aldudes selon un axe Sud Sud-Est Nord Nord-Ouest, du mont Lindus au Munhoa. Nous avons déjà décrit [@blotNouveauxVestigesMegalithiques1972c, p.192], 2 cromlechs en ces lieux. Le n°1 : 6m de diamètre et 30 pierres ; le n°2 : à 5m au Sud du précédent, possède 13 pierres et un diamètre de 4m.

#### Description

Tumulus mixte de 4m de diamètre et 0,30m de haut. Une vingtaine de pierres apparaissent à la surface du tumulus, assez inégalement réparties.

#### Historique

Monument découvert en [mai 1973]{.date}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Hortz-Zorroz]{.coverage} 4 - [Tumulus]{.type}

#### Localisation

Carte 1346 Ouest Saint-Etienne-de-Baïgorry.

Monument faisant partie de ceux échelonnés sur la longue crête montagneuse, étendue dans la Vallée des Aldudes selon un axe Sud Sud-Est Nord Nord-Ouest, du mont Lindus au Munhoa [@blotNouveauxVestigesMegalithiques1972c].

#### Description

Tumulus mixte de 2,80m de diamètre et 0,30m de haut ; parmi quelques pierres qui apparaissent à sa surface, l'une d'elles est particulièrement visible à la périphérie, au Sud.

#### Historique

Monument découvert en [mai 1973]{.date}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Ichtauz]{.coverage} 1 - [Tumulus]{.type}

#### Localisation

À une trentaine de mètres au Nord Nord-Est de la BF 110.

Altitude : 970m.

#### Description

Tumulus pierreux de faible hauteur et de 3,60m de diamètre à la surface duquel apparaissent de nombreuses petites dalles de [grès]{.subject} rose ; pas de péristalithe visible. Ces deux derniers monuments, comme d'autres, sont ou à cheval sur la ligne frontière, ou très légèrement à l'Ouest de celle-ci, ce qui ne les empêche pas de faire partie intégrante de l'ensemble des vestiges protohistoriques d'Ichtauz ; c'est pourquoi nous les avons cités ici.

#### Historique

Monument découvert par [J. Blot]{.creator} en [avril 2010]{.date}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Ichtauz]{.coverage} 2 - [Tumulus]{.type} [(?)]{.douteux}

#### Localisation

Au niveau de la BF 111.

Altitude : 960m.

#### Description

On note un très léger relief tumulaire circulaire de 3,50m de diamètre entourant la base de la borne frontière, fait de nombreuses petites pierres entièrement recouvertes de mousse. Il n'y a pas de péristalithe visible.

#### Historique

Monument découvert par [J. Blot]{.creator} en [avril 2010]{.date}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Ichtauz]{.coverage} - [Tumulus-cromlech]{.type}

#### Localisation

À 5m au Nord Nord-Est de la BF 110.

Altitude : 970m.

#### Description

On note un une quinzaine de pierres de dimensions variables, enfouies dans le sol qui délimitent un tumulus de faible hauteur et de 3m de diamètre environ.

#### Historique

Monument découvert par [J. Blot]{.creator} en [avril 2010]{.date}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Ichtauzkolepoa]{.coverage} 2 - [Cromlech]{.type}

#### Localisation

Altitude : 1000m.

Il est situé à 3m au Sud Sud-Est du *cromlech n° 1* découvert par J. Blot [@blotNouveauxVestigesMegalithiques1973a] et à une vingtaine de mètres au Sud-Est de la récente route. Ces deux monuments n'ont heureusement pas été touchés par les travaux routiers tout proches.

#### Description

Cinq pierres en [grès]{.subject} rose, au ras du sol, délimitent un cercle 2,50m de diamètre.

#### Historique

Monument découvert par [J. Blot]{.creator} en [avril 2010]{.date}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Ichtauzkolepoa]{.coverage} 1 - [Monolithe]{.type}

#### Localisation

Altitude : 970m.

Au-dessus de la naissance d'un petit ruisseau et à quelques mètres à l'Est de la piste pastorale.

#### Description

Belle dalle de [grès]{.subject} rose rectangulaire, couchée au sol suivant un axe Sud-Ouest Nord-Est, mesurant 2,64m de long, 1,57m de large et 0,34m d'épaisseur. Bien que ne présentant pas de traces évidentes d'épannelage, sa situation auprès de points d'eau, de pistes pastorales, et de si abondants pâturages, rend fort probable son rôle de *Muga*.

#### Historique

Dalle découverte par [I. Gaztelu]{.creator} en [novembre 1997]{.date}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Ichtauzkolepoa]{.coverage} 2 - [Monolithe]{.type}

#### Localisation

À 25m environ au Nord-Est de *Ichtauzkolepoa 1 - Monolithe*.

Altitude : 975m.

#### Description

Belle dalle de [grès]{.subject} rose, couchée au sol selon un axe Nord-Sud, mesurant 2,40m de long, 0,78m de large, et 0,25m d'épaisseur. Peut-être y aurait-il quelques traces d'épannelage le long du côté Est. Il est difficile de savoir si une seule, ou les 2 dalles, ont joué (en même temps ?) le rôle de *Muga*.

#### Historique

Dalle découverte par [I. Gaztelu]{.creator} en [novembre 1997]{.date}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Ichtauzkolepoa]{.coverage} - [Pierre couchée]{.type} [(?)]{.douteux}

#### Localisation

Elle est située plus bas que *Ichtauzkolepoa 1 - Monolithe*, à 10m au Sud du petit ruisseau.

Altitude : 960m.

#### Description

Bloc de [grès]{.subject} rose couché au sol selon un axe Est-Ouest, mesurant 1,80m de long 0,49m de large et 0,31m d'épaisseur. *Très douteux*.

#### Historique

Pierre découverte par [J. Blot]{.creator} en [avril 2010]{.date}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Ichtauzkolepoa]{.coverage} - [Pierre plantée]{.type} [(?)]{.douteux}

#### Localisation

Elle se trouve au Nord de *Ichtauzkolepoa - Pierre couchée (?)*, de l'autre côté du ruisseau, sur le terrain en pente, et à 10m au Nord de la piste pastorale.

Altitude : 985m.

#### Description

Cette pierre de [grès]{.subject} rose, émerge du sol sur une hauteur de 0,94m, et mesure 0,56m à sa base visible et 0,26m d'épaisseur. Seule cette position dressée attire l'attention. *Très douteux* ; borne actuelle ?, effet du hasard ?

#### Historique

Pierre découverte par [J. Blot]{.creator} en [avril 2010]{.date}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Ichtauzkolepoa]{.coverage} - [Pierre couché]{.type} [(?)]{.douteux}

#### Localisation

Altitude : 1000m.

Elle est située à une dizaines de mètres au Nord-Est de l'extrémité de la route qui aboutit au col.

#### Description

Dalle de [grès]{.subject} local, mesurant 1,50m de long, 0,50m de large, et approximativement 0,40m d'épaisseur - pourrait faire partie d'un filon rocheux en place.

#### Historique

Dalle trouvée par [Badiola P.]{.creator} en [octobre 2012]{.date}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Ichtauzkolepoa]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 1010m.

Sur une petite hauteur dominant le col et l'arrivée de la route.

#### Description

Tumulus de terre mesurant environ 5m de diamètre et 0,30m de haut, à la surface duquel apparaissent quelques pierres, sans que l'on puisse parler de cromlech.

#### Historique

Monument découvert en [octobre 1972]{.date} par [J. Blot]{.creator}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Laurigna]{.coverage} sud - [Tumulus]{.type}

#### Localisation

Carte 1346 Ouest Saint-Etienne-de-Baïgorry.

Altitude : 1150m

Monument faisant partie de ceux échelonnés sur la longue crête montagneuse, étendue dans la Vallée des Aldudes selon un axe Sud Sud-Est - Nord Nord-Ouest, du mont Lindus au Munhoa.

Situé à 100m environ au Nord Nord-Est de la BF 160, sur une petite éminence naturelle.

#### Description

Tumulus terreux, de 7m de diamètre et 0,40m de haut avec deux pierres bien visibles en secteur Nord et Nord-Ouest.

#### Historique

Monument découvert par [nous]{.creator} en [novembre 1974]{.date}. Nous le publions ici car il nous semble être distinct, au moins quant à la situation, du tumulus décrit par J.M. de Barandiaran sous le nom de *Traikarlepo ou Beai* [@barandiaranHombrePrehistoricoPais1953, p.247].

</section>

<section class="monument">

### [Banca]{.spatial} - [Lepeder]{.coverage} - [Cromlech]{.type}

#### Localisation

Carte IGN 1346 Ouest Saint-Etienne-de-Baigorry

Altitude : 780m.

On le trouve à l'Est du col de Lepeder, et presque à l'extrémité de la croupe orientée Ouest-Est, née au sommet du mont Antchola. De ce site, on a une vue panoramique magnifique.

#### Description

Une quinzaine de petits blocs de [schiste]{.subject} ardoisier, la pierre locale, délimite un cercle de 3 de diamètre ; au centre se distingue un amas de ces mêmes pierres.

#### Historique

Monument découvert par [J. Blot]{.creator} en [avril 2010]{.date}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Mehatzé]{.coverage} 4 - [Cromlech]{.type}

#### Localisation

Carte 1346 Ouest Saint-Etienne-de-Baïgorry.

Altitude : 1168m.

Il est situé dans un petit col au Sud Sud-Ouest du mont Méhatzé sur la ligne de crête, entre les bornes frontières 167 et 168. Le cromlech n° 4 est situé à 9m au Sud Sud-Est du n°1 [@blotNouveauxVestigesMegalithiques1972c, p.204].

#### Description

Cercle de 4m de diamètre délimité par 6 pierres, dont deux particulièrement visibles en secteur Est : l'une de 1,40m de long et 0,70m de large, l'autre de 1,50m x 1,30m.

#### Historique

Monument découvert en [septembre 1972]{.date}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Méhatzé]{.coverage} 5 - [Cromlech]{.type}

#### Localisation

Carte 1346 Ouest Saint-Etienne-de-Baïgorry.

Monument faisant partie de ceux échelonnés sur la longue crête montagneuse, étendue dans la Vallée des Aldudes selon un axe Sud Sud-Est Nord Nord-Ouest, du mont Lindus au Munhoa. Il est situé à 23m au Nord du n° 3, et à 27m au Nord Nord-Est du n°2 [@blotNouveauxVestigesMegalithiques1972c].

#### Description

Cercle discret de 4m de diamètre délimité par 4 pierres. Nous avons dû pratiquer une fouille de sauvetage en juillet 1977 [@blotTumuluscromlechMehatzeMehatze1983, p. 191], qui a mis au jour une couronne périphérique en double assise de petits blocs de pierre disposée autour d'une [ciste]{.subject} centrale en forme de fer à cheval ouvert au Nord-Ouest. Le résultat de la datation au 14 C : (Gif 4470), mesure d'âge (BP) : 2730+- 100, soit en date calibrée : 1118-812 BC.

#### Historique

Monument découvert en [septembre 1974]{.date}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Méhatzé]{.coverage} Sud 1 - [Tumulus]{.type}

#### Localisation

À 80m au Nord de la BF 166 et à 10m à l'Ouest de la clôture barbelée.

#### Description

Petit tumulus circulaire de 3m de diamètre et 0,30m de haut au sommet duquel apparaissent quelques pierres.

#### Historique

Monument signalé en [août 1989]{.date} par [Luis Millan San Emeterio]{.creator} (San Sebastian).

</section>

<section class="monument">

### [Banca]{.spatial} - [Méhatzé]{.coverage} Sud 2 - [Tumulus]{.type}

#### Localisation

À 26m à l'Ouest de *Méhatzé Sud 1 - Tumulus*, au voisinage immédiat de la piste pastorale.

#### Description

Tumulus bien visible de 6m de diamètre et 0,40m de haut environ, formé de blocs rocheux.

#### Historique

Monument signalé en [août 1989]{.date} par [Luis Millan San Emeterio]{.creator} (San Sebastian).

</section>

<section class="monument">

### [Banca]{.spatial} - [Minchondo]{.coverage} - [Tumulus Cromlech]{.type}

#### Localisation

Altitude : 796m.

Ce monument est construit dans l'ensellement d'une croupe, elle-même située au flanc Nord-Ouest du mont Adarza ; cette région est desservie par la route qui monte vers le col Urdiako Lepoa, et qui longe ainsi le flanc Sud de cette colline, à quelques dizaines de mètres au Sud de ce monument.

#### Description

Tumulus en forme de galette aplatie de 3 à 3,10m de diamètre et 0,40m de haut maximum. Une quinzaine de pierres, de volume modeste et plus ou moins au ras du sol - mais bien visibles - matérialisent un péristalithe.

#### Historique

Ce monument nous a été signalé par [P. Badiola]{.creator} en [mai 2016]{.date}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Minchondo]{.coverage} - [Cromlech]{.type}

#### Localisation

Il est situé à 1m à l'Est de *Minchondo - Tumulus Cromlech*.

#### Description

Ce très probable cromlech possède un péristalithe incomplet - en particulier dans le secteur Nord - cependant 6 pierres bien visibles s'inscrivent dans un cercle de 3m de diamètre. L'une d'entre elles, en secteur Sud, atteint même 0,40m x 0,40m.

#### Historique

Monument découvert par [Blot J.]{.creator} en [mai 2016]{.date}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Munhogaina]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 829m.

Petit tumulus situé à 3m à l'Est de la base du Munhogaine, sur un petit plateau se terminant lui-même, à 40m à l'Est de ce tumulus, par un amas rocheux important au flanc duquel se voient les restes d'une cabane de berger en pierres sèches.

#### Description

Petit tumulus de terre et quelques pierres, mesurant 2,50m à 3m de diamètre et 0,30m de haut.

#### Historique

Tumulus découvert par [Meyrat F.]{.creator} en [septembre 2014]{.date}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Otsachar]{.coverage} 1 - [Tumulus]{.type}

#### Localisation

Altitude : 808m.

Ce monument se trouve à l'extrémité Sud du plateau sommital du mont Otsachar, et tangent, à l'Ouest, à la piste de crête ; il est à 2m au Sud d'un poste de tir à la palombe...

#### Description

Tumulus circulaire de 3,30m de diamètre, et 0,30m de haut, à la surface duquel apparaît une trentaine de blocs de [calcaire]{.subject} blanc, qui semblent aussi délimiter un péristalithe. Ce monument paraît en parfait état de conservation...

#### Historique

Tumulus découvert par [Meyrat F.]{.creator} en [novembre 2014]{.date}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Otsachar]{.coverage} 2 - [Tumulus]{.type}

#### Localisation

Ce très modeste monument est tangent au Nord-Est de *Otsachar 1 - Tumulus* ; la piste de crête passe très exactement entre eux deux.

#### Description

Il mesure environ 2m de diamètre et se présente comme un relief peu visible, de 0,25m de haut, couvert de gazon, où seul un petit bloc de [calcaire]{.subject} blanc est visible en son centre.

#### Historique

Tumulus découvert par [Meyrat F.]{.creator} en [novembre 2014]{.date}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Otsachar]{.coverage} 3 - [Tumulus]{.type}

#### Localisation

Altitude : 809m.

Il est à l'extrémité Nord du même plateau sommital d'Otsachar, à une quarantaine de mètres au Sud d'un amas rocheux bien visible.

#### Description

Tumulus herbeux circulaire de 4,60m de diamètre et de 0,25m à 0,30m de haut.

Un bon quart de sa partie Est a été piétinée par les animaux.

#### Historique

Monument identifié par [Blot J.]{.creator} en [décembre 2014]{.date}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Otsachar]{.coverage} 4 - [Tumulus]{.type} [(?)]{.douteux}

#### Localisation

Altitude : 799m.

Situé dans une prairie sur un replat au Nord de *Otsachar 3 - Tumulus*, à quelques mètres de la clôture Est de cette prairie.

#### Description

Tumulus herbeux, dans l'ensemble circulaire, d'environ 3,30m de diamètre et 0,30m de haut. Monument douteux...

#### Historique

Tumulus découvert par[Meyrat F.]{.creator} en [novembre 2014]{.date}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Pago Zelhay]{.coverage} - [Cromlech]{.type} [(?)]{.douteux}

#### Localisation

Altitude : 894m.

Il est à 50m au Sud de la BF 114.

#### Description

Une dizaine de pierres au ras du sol, délimitent un cercle (approximatif), de 2,80m de diamètre. Monument douteux ?.

#### Historique

Monument découvert par [J. Blot]{.creator} en [avril 2010]{.date}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Pago Zelhay]{.coverage} - [Monolithe]{.type}

#### Localisation

Altitude : 900m.

Il est à 40m au Nord-Est de la BF 114.

#### Description

Bloc de [grès]{.subject} [triasique]{.subject}, de forme grossièrement parallélépipédique, couché au sol suivant un axe Nord Nord-Ouest Sud Sud-Est. Il mesure 3m de long, 1,57m de large à sa base et 1,35m à son sommet. Son épaisseur est variable : maximum à la base : 0,60m et moindre au sommet en partie enfoui dans le sol, et donc difficilement appréciable. Il présente des traces d'épannelage sur ses bords Sud-Est et Sud-Ouest.

#### Historique

Monument découvert par [J. Blot]{.creator} en [avril 2010]{.date}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Pago Zelhay]{.coverage} - [Tumulus pierreux]{.type} [(?)]{.douteux}

#### Localisation

Altitude : 905m.

Situé à quelques dizaines de mètres à l'Ouest de *Pago Zelhay - Monolithe*, et au Nord de la BF 114, se trouve un amas de dalles [triasiques]{.subject}, étalé sur une surface de 10m environ, et sur terrain en pente vers le Sud ; quelques dalles plantées et inclinées, au centre de l'amas pourraient évoquer (??) les restes d'une [chambre funéraire]{.subject}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Turaor]{.coverage} 2 - [Cromlech]{.type}

#### Localisation

Carte 1346 Ouest Saint-Etienne-de-Baïgorry.

Altitude : 1181m.

Nous avons publié en 1972 [@blotNouveauxVestigesMegalithiques1972c, p.202], le cromlech Turaor n°1, situé à 75m au Sud de la BF 163. Le cromlech Turaor n° 2 est situé sur le petit replat au sommet de Turaor, et à 30m au Nord de la BF 163.

#### Description

Cercle de 3m de diamètre délimité par 5 pierres.

#### Historique

Monument découvert en [juin 1979]{.date}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Turaor]{.coverage} 3 - [Tumulus]{.type}

#### Localisation

Carte 1346 Ouest Saint-Etienne-de-Baïgorry.

Monument faisant partie de ceux échelonnés sur la longue crête montagneuse, étendue dans la Vallée des Aldudes selon un axe Sud Sud-Est Nord Nord-Ouest, du mont Lindus au Munhoa.

Ce troisième monument est à 120m au Sud du n°1 [@blotNouveauxVestigesMegalithiques1972c, p.202] et à 2m au Sud d'un important pointement rocheux.

#### Description

Tumulus pierreux de 5m de diamètre et 0,60m de haut, bien délimité à sa périphérie.

#### Historique

Monument découvert en [septembre 1978]{.date}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Turaor lepoa]{.coverage} 4 - [Cromlech]{.type}

#### Localisation

Carte 1346 Ouest Saint-Etienne-de-Baïgorry.

Monument faisant partie de ceux échelonnés sur la longue crête montagneuse, étendue dans la Vallée des Aldudes selon un axe Sud Sud-Est Nord Nord-Ouest, du mont Lindus au Munhoa.

Altitude : 1140m.

Dans la même publication, nous avons décrit [@blotNouveauxVestigesMegalithiques1972c, p. 200], dans ce petit col 3 cromlechs (1-2-3).

#### Description

Petit cercle de 2m de diamètre délimité par une dizaine de blocs pierreux.

#### Historique

Monument découvert en [septembre 1978]{.date}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Turaor lepoa]{.coverage} 5 - [Cromlech]{.type}

#### Localisation

Carte 1346 Ouest Saint-Etienne-de-Baïgorry.

Monument faisant partie de ceux échelonnés sur la longue crête montagneuse, étendue dans la Vallée des Aldudes selon un axe Sud Sud-Est Nord Nord-Ouest, du mont Lindus au Munhoa.

À 80m au Nord du n°1 [@blotNouveauxVestigesMegalithiques1972c].

#### Description

Cromlech légèrement surélevé de 3,60m de diamètre, délimité par une douzaine de pierres au ras du sol.

#### Historique

Monument découvert en [juin 1979]{.date}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Turaor lepoa]{.coverage} 6 - [Cromlech]{.type}

#### Localisation

Carte 1346 Ouest Saint-Etienne-de-Baïgorry.

Monument faisant partie de ceux échelonnés sur la longue crête montagneuse, étendue dans la Vallée des Aldudes selon un axe Sud Sud-Est Nord Nord-Ouest, du mont Lindus au Munhoa.

Il est à environ 16m au Nord Nord-Est de *Turaor 2 - Cromlech*.

#### Description

Cercle peu visible de 5m de diamètre environ.

#### Historique

Monument découvert par [nous]{.creator} en [septembre 1978]{.date}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Turaor lepoa]{.coverage} 7 - [Tumulus]{.type}

#### Localisation

Carte 1346 Ouest Saint-Etienne-de-Baïgorry.

Monument faisant partie de ceux échelonnés sur la longue crête montagneuse, étendue dans la Vallée des Aldudes selon un axe Sud Sud-Est Nord Nord-Ouest, du mont Lindus au Munhoa.

Au Sud de *Turaor-lepoa 6 - Cromlech*, à environ 150m au Nord Nord-Est de la BF 162.

#### Description

Tumulus pierreux de 5m de diamètre et 0,40m de haut environ.

#### Historique

Monument découvert par [nous]{.creator} en [septembre 1978]{.date}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Turaor]{.coverage} n°2 - [Cromlech]{.type}

#### Localisation

Carte 1346 Ouest Saint-Etienne-de-Baïgorry.

Monument faisant partie de ceux échelonnés sur la longue crête montagneuse, étendue dans la Vallée des Aldudes selon un axe Sud Sud-Est Nord Nord-Ouest, du mont Lindus au Munhoa.

À 30m au Nord de la BF 163, sur le replat du sommet.

#### Description

Environ 5 pierres pourraient délimiter un cercle de 3,20m de diamètre.

#### Historique

Monument découvert par nous en [septembre 1978]{.date}.

</section>

<section class="monument">

### [Banca]{.spatial} - [Turaor]{.coverage} n°5 - [Tumulus]{.type}

#### Localisation

Carte 1346 Ouest Saint-Etienne-de-Baïgorry.

Monument faisant partie de ceux échelonnés sur la longue crête montagneuse, étendue dans la Vallée des Aldudes selon un axe Sud Sud-Est Nord Nord-Ouest, du mont Lindus au Munhoa.

#### Description

Tumulus pierreux, de 6m de diamètre et 0,40m de haut, à environ 280 *pas* au Sud de la BF 163.

Un monolithe nous avait été signalé en août 1989 par Luis Millan San Emeterio (San Sebastian), gisant entre les BF 163 et 164 que nous n'avons pas retrouvé.

Par contre nous signalerons un bloc rocheux, régulier qui a été très probablement amené là où il se trouve maintenant (mais depuis quand ?), à 9m au Nord Nord-Est de la BF 165, près des fils barbelés. Il s'agit d'un bloc parallélépipédique, sans inscription visible, mesurant 1,40m de long, 0,78m de largeur et 0,38m d'épaisseur.

</section>

# Barcus

<section class="monument">

### [Barcus]{.spatial} - [Ahargo]{.coverage} 3 - [Tumulus-cromlech]{.type}

#### Localisation

Altitude : 598m.

Ce tumulus - ainsi que deux autres trouvés par nous en 1978 [@blotSouleSesVestiges1979, p.26] se trouve sur un replat à environ 500m à l'Ouest du mont Ahargo (609m), au Nord de Barcus et à 500m au Sud Sud-Ouest du camp protohistorique à la cote 560. Il est au point culminant de la ligne de crête et la piste pastorale passe à 6m au Nord.

Ce tumulus cromlech TC3 est à 47m au Nord-Est de *TC1*, lequel se trouve lui-même à 20m au Nord-Est de *TC2*.

#### Description

Tumulus circulaire en forme de galette aplatie, de 7m de diamètre environ et 0,40m de haut. Un grand nombre de petits blocs en [calcaire]{.subject} est particulièrement visible en périphérie, en particulier à l'Est et au Sud. On notera que l'aspect des *TC1* et *TC2* s'est bien modifié depuis nos découvertes en 1978, les éléments pierreux des péristalithes étant recouverts en grande partie de terre et beaucoup moins visibles.

#### Historique

Tumulus trouvé par [Meyrat F.]{.creator} en [mai 2016]{.date}.

</section>

<section class="monument">

### [Barcus]{.spatial} - [Lexeguita]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 655m.

Situé dans le col du même nom, à 40m au Sud d'une cabane de chasse.

#### Description

Tumulus terreux érigé sur terrain plat, mesurant 7m de diamètre et 0,40m de haut.

#### Historique

Tumulus découvert par [Meyrat F.]{.creator} en [octobre 2011]{.date}.

</section>

# Béhorlégui

<section class="monument">

### [Béhorlégui]{.spatial} - [Arlotia]{.coverage} - [Polissoir]{.type}

#### Localisation

Altitude: 744m.

Ce polissoir est à une centaine de mètres à l'Est du chalet Arlotia et à 6m au Nord Nord-Ouest de la piste qui s'élève de ce chalet vers le Nord.

#### Description

Bloc de [grès]{.subject} de forme rectangulaire, mais d'épaisseur décroissante, triangulaire à la coupe. Il mesure 0,60m dans son grand axe, 0,50 de large, 0,85m dans sa plus grande épaisseur, au Nord, de sorte qu'ainsi la face supérieure est inclinée vers le Sud. Cette face présente 4 plages de polissage, deux supérieures et deux juste en-dessous, ayant à peu prés toutes les mêmes dimensions: environ 0,30m de long et 0,20 m de large.

Ce polissoir présente des traces de chocs, trés probablement par des engins récents, et pourrait ne pas être à sa place originelle.

#### Historique

Monument découvert par [Blot J.]{.creator} en [avril 2013]{.date}.

</section>

<section class="monument">

### [Béhorlégui]{.spatial} - [Arlotia]{.coverage} (groupe Sud) - [Tertres d'habitat]{.type}

On en dénombre 4.

-   *Arlotia Sud 1 - Tertre d'habitat* : Tertre terreux de 11m de diamètre et 0,80m de haut, érigé sur terrain en légère pente vers le Nord. C'est lui que nous avions décrit en 1971 comme le tumulus *Arlotia 3*.

-   *Arlotia Sud 2 - Tertre d'habitat* : situé à 2m au Nord du précédent -- Mesure 8m de diamètre et 0,30m de haut.

-   *Arlotia Sud 3 - Tertre d'habitat* : situé à 1m au Nord du précédent. Mesure 5m de diamètre et 0,30m de haut.

-   *Arlotia Sud 4 - Tertre d'habitat* : situé à 2m à l'Ouest du précédent. De très faible relief, il semble mesurer 5m de diamètre.

#### Historique

Ces 4 tertres ont été identifiés par [Blot J.]{.creator} en [avril 2013]{.date}.

</section>

<section class="monument">

### [Béhorlégui]{.spatial} - [Arlotia]{.coverage} (groupe Nord) - [Tertres d'habitat]{.type}

Trois tertres d'habitat se trouvent dans le vallon (situé au Nord de tertres du groupe Sud), entre les cotes 785 et 812.

-   *Arlotia Nord 1 - Tertre d'habitat*

    Altitude : 735m.

    Il est situé au versant Sud du vallon.

    -   Description

        Tertre terreux érigé sur terrain en pente vers le Nord. Il mesure environ 8m de diamètre et 0,40m de haut.

-   *Arlotia Nord 2 - Tertre d'habitat (?)*

    -   Localisation

        Altitude : 740m.

        À environ 100m au Nord du tertre précédent, et sur le versant Nord du vallon.

    Tertre terreux asymétrique de 7 de diamètre, à sommet plat. Elément douteux, (trou de blaireau à côté...).

-   *Arlotia 3 - Tertre d'habitat*

    -   Localisation

        Altitude : 735m.

        Erigé sur le versant Nord du vallon vers son extrémité Ouest. Il est à une trentaine de mètres au Nord-Ouest de 4 arbres plantés en carré dans le creux du vallon.

#### Historique

Ces 3 tertres ont été identifiés par [Blot J.]{.creator} en [avril 2013]{.date}.

</section>

<section class="monument">

### [Béhorlégui]{.spatial} - [Arlotia]{.coverage} 1 - [Tumulus]{.type}

#### Localisation

Carte 1346 Est Saint-Jean-Pied-de-Port.

Altitude : 736m.

Cet ensemble de tumulus est situé dans un col qui domine au Nord-Ouest le village de Béhorlégi, sur cette ligne de croupes qui va de Iramunho à Hauscoa. Le tumulus n°1 est à 30m au Nord Nord-Ouest du chalet d'Arlotia utilisé pour la chasse à la palombe, et du réservoir cylindrique en ciment qui le jouxte.

#### Description

Tumulus terreux érigé sur sol plat, de 16m de diamètre et 1m de haut.

#### Historique

Monument découvert en [juin 1971]{.date}.

</section>

<section class="monument">

### [Béhorlégui]{.spatial} - [Arlotia]{.coverage} 2 - [Tumulus]{.type}

#### Localisation

Il est visible à 5m au Nord Nord-Ouest de *Arlotia 1 - Tumulus*.

#### Description

Tumulus terreux circulaire très net de 11m de diamètre et 0,80m de haut ; les évolutions d'un bulldozer ont amené tout à fait fortuitement 4 gros blocs de [calcaire]{.subject} à son sommet...

#### Historique

Monument découvert en [juin 1971]{.date}.

</section>

<section class="monument">

### [Béhorlégui]{.spatial} - [Arlotia]{.coverage} 3 - [Tumulus]{.type}

#### Localisation

Il est situé à environ 200m au Nord-Est de *Arlotia 2 - Tumulus*, à 150m à l'Ouest d'un réservoir d'eau édifié à flanc de colline, et à l'Ouest de la tranchée comblée de pierres qui recèle la conduite d'eau issue de ce réservoir.

#### Description

Tumulus terreux circulaire d'un diamètre de 11m et 0,50m de haut, édifié sur un terrain en très légère pente vers l'Ouest ; il est très différent d'un «tertre d'habitat».

#### Historique

Monument découvert en [juin 1971]{.date}.

</section>

<section class="monument">

### [Béhorlégui]{.spatial} - [Arlotia]{.coverage} 4 - [Tumulus]{.type}

#### Localisation

Il est situé à environ 60m au Nord Nord-Ouest de *Arlotia 3 - Tumulus*, et près d'un pylône métallique de soutien d'un poste de rabatteur de chasse à la palombe.

#### Description

Tumulus terreux de 11m de diamètre et 0,40m de haut.

#### Historique

Monument découvert en [juin 1971]{.date}.

</section>

<section class="monument">

### [Béhorlégui]{.spatial} - [Arroskua]{.coverage} - [Cromlech]{.type}

#### Localisation

Altitude : 1141m.

Ce monument se trouve quelques dizaines de mètres au Nord du sommet de la colline Arroskua, laquelle domine le col de ce nom, situé au Sud.

#### Description

Il est situé sur un terrain plat et dépourvu des nombreuses pierres qui apparaissent en général à la surface du sol de cette colline. Une vingtaine de blocs de [calcaire]{.subject}, qui n'ont subi aucune retouche, délimitent un cercle de 8, 10m de diamètre ; le secteur Nord est particulièrement fourni. Ces blocs sont de taille modeste - de 1 à 4 poings, en moyenne - et plus ou moins facilement visibles, ne dépassant souvent que fort peu la surface.

#### Historique

Cercle découvert par [A. Martinez]{.creator} en [octobre 2015]{.date}.

</section>

<section class="monument">

### [Béhorlégui]{.spatial} - [Athermigna]{.coverage} - [Tertres d'habitat]{.type}

#### Localisation

Ce bel ensemble de 8 tertres d'habitat est dans le vallon entre Harribilibila (797m) et la cote 785, érigés sur un terrain en légère pente vers le Nord Nord-Est.

-   *Athermigna TH 1* : Altitude : 760m. Tertre terreux de 8m de diamètre environ, et 1m de haut.

-   *Athermigna TH 2* : situé à 4m au Nord du précédent - mesure environ 4m de diamètre et 0,30m de haut.

-   *Athermigna TH 3* : situé à 6m au Nord-Est du n°2. Mesure 10m de diamètre et 0,40m de haut.

-   *Athermigna TH 4* : situé à 7m au Nord-Ouest du n°3. Environ 6m de diamètre.

-   *Athermigna TH 5* : à 12m au Sud-Ouest du précédent. Environ 7m de diamètre.

-   *Athermigna TH 6* : situé à 5m au Nord Nord-Ouest du précédent. Mesure environ 5 à 6m de diamètre.

-   *Athermigna TH 7* : situé à 6m au Nord Nord-Ouest du précédent, de très faible relief.

-   *Athermigna TH 8* : situé à 7m à l'Est Nord-Est du n° 1, et de très faible relief.

#### Historique

Ces 8 monuments ont été découverts par [Blot J.]{.creator} en [avril 2013]{.date}.

</section>

<section class="monument">

### [Béhorlégui]{.spatial} - [Béhorlégui]{.coverage} - [Monolithe]{.type}

#### Localisation

Nous dirons ici quelques mots de ce curieux rocher, que l'on pourrait qualifier d'«anthropomorphe», et que toute personne traversant le village de Béhorlégui ne peut manquer d'apercevoir au dernier virage en sortant du bourg, en direction du col d'Apanicé, sur le côté gauche de la route.

#### Description

Il s'agit d'un bloc de [calcaire]{.subject} [grès]{.subject}eux compact de 1,85m de haut, modelé par l'érosion qui lui a donné une allure anthropomorphe : on peut en effet - avec un peu d'imagination, et en regardant de l'Ouest - distinguer une partie supérieure renflée (à tête), séparée du thorax par un rétrécissement bien marqué. Encore doit-on faire remarquer que cette « tête » était encore plus nette avant que les transports successifs (nous en reparlerons), ne lui aient enlevé d'importants fragments.

On peut distinguer ensuite le segment sous-jacent, assimilable au thorax, avec la naissance des épaules, surmontant un « abdomen » volumineux (1,40m de large), dont il est séparé par un nouveau rétrécissement ; un troisième rétrécissement annonçant les membres inférieurs... Si nous devions évoquer une ressemblance, ce serait avec les « Déesses- Mères » du Néolithique d'Europe ou du Proche-Orient, aux formes généreuses.

#### Historique

C'est J. Casaubon qui, en mai 2008, nous a mis en relation avec le héros de cette histoire. Il y a quelques décennies en effet, le maire de Béhorlégui - à l'époque [Jean Cubiat]{.creator} - se rendant en voiture vers les pâturages d'Apanicé, vit sur sa droite, au col de Landerre, à quelques dizaines de mètres en contre-bas de la route, ce bloc de pierre aux formes curieuses. Il avait été déplacé à cet endroit lors des touts récents travaux de creusement de la D 117, mais semblait avoir séjourné sous terre, comme le laissait supposer sa teinte, et la terre dont il était encore abondamment enveloppé. Quelques jours plus tard, le monolithe avait disparu. C'est tout à fait fortuitement que J. Cubiat le revit, dans une propriété privée, à Ordiarp. Considérant que cette pierre appartenait à sa commune, le maire de Béhorlégui obtint donc que ce monolithe soit ramené ; toutefois, pour éviter toute nouvelle disparition, il jugea préférable de le disposer à sa place actuelle...

Ce monolithe « anthropomorphe », a-t-il été vu comme tel dans le passé, quand il était à l'air libre ? A-t-il fait l'objet de dévotion en des temps lointains ? Nous ne le saurons jamais, mais rien n'interdit de le penser...

</section>

<section class="monument">

### [Béhorlégui]{.spatial} - [Harribilibila]{.coverage} Sud - [Tertres d'habitat]{.type} ([?]{.douteux})

#### Localisation

Altitude : 740m.

Les 4 tertres d'habitat se trouvent à flanc de colline, longés par une draille, et dominant des dolines.

#### Description

Quatre tertres de terre, tangents, s'échelonnent selon un axe Nord-Ouest Sud-Est.

Ils mesurent 4m à 5m de diamètre pour un dénivelé de 1m. Des trous de blaireau pourraient faire penser que ces tertres sont le résultat de ces trous... mais rien n'est évident !

#### Historique

Monuments découverts par [Blot J.]{.creator} en [avril 2013]{.date}.

</section>

<section class="monument">

### [Béhorlégui]{.spatial} - [Ithurrotché]{.coverage} - [Tumulus]{.type}

#### Localisation

Carte 1446 Ouest Ordiarp.

Altitude : 1010m.

Il est tangent, au Nord, à l'ancienne piste pastorale qui, au Sud-Est du pic de Béhorlégi, mène du col d'Apanicé à la fontaine d'Oxolatzé ; il est à 200m à vol d'oiseau à l'Est de ce col. On le trouve sur un petit ressaut entre deux dépressions, à 50m au Sud de la route goudronnée.

#### Description

Tumulus terreux très visible de 6m de diamètre et 0,50m de hauteur ; quelques pierres sont visibles à sa périphérie sans que l'on puisse parler de péristalithe.

#### Historique

Monument découvert en [août 1973]{.date}.

</section>

<section class="monument">

### [Béhorlégui]{.spatial} - [Ithurrotché]{.coverage} - [Monolithe]{.type}

#### Localisation

Altitude : 1045m.

Il est situé au niveau du col d'Apanicé, au lieu-dit *Ithurrotché*, sur le bord droit d'un petit chemin goudronné, qui part vers le Nord, à environ 100m de son détachement de la D 117.

#### Description

Le monolithe gît sur un sol légèrement incliné vers le Nord (sur le versant de la Bidouze) ; il a la forme d'un parallélépipède rectangle de [calcaire]{.subject} compact, assez régulier dans l'ensemble, mesurant 4m de long ; dans sa partie médiane la plus large, il mesure 0,70m et 0,61m d'épaisseur. Sa base, de section rectangulaire, (0,70m de large et 0,70m d'épaisseur) est assez régulière et plane dans son ensemble. Son sommet orienté plein Nord présente des traces d'épannelage - afin de lui donner sa forme pointue (0,37m dans sa partie la plus large)- qui sont bien visibles au niveau de son bord Nord-Ouest. Il ne semble pas y avoir eu d'autres retouches, mais on doit signaler, le long et au milieu du bord Ouest du monolithe, un bloc de pierre du même type, qui pourrait bien avoir été détaché du bloc d'origine afin de lui donner sa forme régulière définitive. Comme d'habitude en Pays Basque, c'est un bloc naturel d'emblée très proche des formes et mensurations désirées qui a été choisi et amené en ces lieux, et il n'a donc eu à subir que peu de modifications.

On remarque enfin qu'il est situé tout à côté de la naissance même d'un petit ruisseau permanent ; la présence de ce point d'eau confirme le très probable rôle de borne pastorale de ce monolithe, dont l'environnement archéologique est riche.

#### Historique

Ce monument nous a été signalé en [2006]{.date} par [Jacques Casaubon]{.creator}.

</section>

<section class="monument">

### [Béhorlégui]{.spatial} - [Mugarco]{.coverage} - [Tumulus]{.type}

#### Localisation

Au sommet de la colline dénommée *Mugarco* par les bergers, à 500m au Nord Nord-Est des bergeries d'Elhorta.

Altitude : 1083m.

#### Description

Au sommet de cette colline on peut distinguer deux petits tumuli, en forme de galettes, circulaires, chacun mesurant 1,80m de diamètre et 0,25m de haut. Le plus au Sud est séparé du second, au Nord-Ouest, par environ une dizaine de mètres. Ils sont essentiellement constitués de terre et de petits galets du [calcaire]{.subject} environnant, bien visibles en particulier à leur périphérie.

#### Historique

[Nous]{.creator} nous sommes rendus sur les lieux en [mai 2008]{.date} avec [J. Casaubon]{.creator} sur les indications du [berger Lérissa]{.creator}, de la maison Aristalde de Béhorlégui.

</section>

# Bidarray

<section class="monument">

### [Bidarray]{.spatial} - [Artzamendi]{.coverage} - [Dolmen]{.type}

#### Localisation

Altitude : 680m.

Il faut prendre la piste qui relie la BF 81 à Mendittipi puis à la borde Mendigaîna sur le Plateau Vert. Le dolmen se situe à environ 80m au-dessus et au Nord-Ouest d'un cayolar en ruine situé sur cette piste, à peu près entre Mendittipi et Mendigaina.

#### Description

Important tumulus pierreux d'environ 11m de diamètre et 0,80m de haut, constitué de blocs de [grès]{.subject} et de gros fragments de dalles. Au centre se voit la [chambre funéraire]{.subject}, d'environ 2m de long, et 1,30m de large, profonde de 0,80m et orientée Sud-Est Nord-Ouest. Elle est délimitée, au Sud-Ouest par une grande dalle verticale de 1,27m de long, et 0,14m d'épaisseur. Au Nord-Est deux dalles verticales, accolées l'une contre l'autre, forment la paroi opposée, mesurant 0,85m de long et 0,75m de haut. La paroi Sud-Est, est elle aussi formée de deux dalles, mais dans le prolongement l'une de l'autre et très inclinées vers l'intérieur ; l'une mesure 0,62m de long, et 0,11m d'épaisseur, l'autre 0,66m de long. Enfin, la paroi Nord-Ouest est elle aussi constituée de deux dalles qui furent accolées l'une contre l'autre, mais dont la plus interne a basculé en dedans ; dimensions : la dalle interne : 0,97m de long, 0,90 de haut et 0,08m d'épaisseur, l'externe : 0,65m de long, 0,72m de haut et 0,20m d'épaisseur.

Au flanc Sud-Est du tumulus, se voient trois volumineux fragments de dalle qui sont en réalité la table dolménique, basculée sur le côté et fracturée (gel ? ; action de l'homme ? ) ; elle devait mesurer environ 2m de long, autant dans sa plus grande largeur et 0,25m d'épaisseur.

#### Historique

Monument découvert par mon ami [F. Meyrat]{.creator} en [mars 2010]{.date}.

</section>

<section class="monument">

### [Bidarray]{.spatial} - [Bourounzukoborda]{.coverage} - [Tertres d'habitat]{.type}

#### Localisation

Une dizaine de tertres (et même plus semble-t-il), se répartissent sur un plateau en légère pente vers le Nord Nord-Ouest, au pied des crêtes d'Iparla, très en contre-bas de la BF 88, entre la borde Bourounzukoborda à l'Est et deux cayolars près d'un pointement rocheux à l'Ouest. Deux petits ruisseaux encadrent à l'Est et à l'Ouest cet ensemble de tertres. Enfin deux pistes pastorales parallèles parcourent l'ensemble d'Est en Ouest ; les tertres se répartissent le long de ces pistes.

#### Description

-   *Tertre n°1* : De forme ovale et asymétrique, il mesure 10m x 6m et 0,70m de haut.

-   *Tertre n°2* : situé à 18m au Nord-Est du précédent, il mesure 8m x 6m et 0,60m de haut.

-   *Tertre n°3* : juste au Sud de la piste, à 6m au Nord-Est du précédent ; Mêmes dimensions que le précédent.

-   *Tertre n°4* : situé à 10m à l'Ouest Nord-Ouest du précédent, et de la première piste. Mesure 11m x 8m et 0,80m de haut.

-   *Tertre n°5* : situé au Nord-Ouest de la deuxième piste et à 14m à l'Ouest Nord-Ouest du précédent. Mesure 9m x 8m et 0,50m de haut.

-   *Tertre n°6* : situé à 2m à l'Ouest Sud-Ouest du précédent ; mesure 8m x 7m et 0,60m de haut.

-   *Tertre n°7* : situé à 10m à l'Ouest Sud-Ouest du précédent ; mesure 10m x 6m et 0,40m de haut.

-   *Tertre n°8* : situé au Sud de la piste et à 10m au Sud du précédent. Mesure 10m x 8m et 1m de haut.

-   *Tertre n°9* : situé à nouveau au Nord de la piste et à 25m à l'Ouest Nord-Ouest du tertre n°7 : mesure 8m x 6m et 0,50m de haut.

-   *Tertre n°10* : il est traversé par la piste pastorale (qui se rend à Bourounzukoborda), et est à 2m au Sud-Ouest du précédent. Il mesure 9m x 6m et 0,60m de haut.

</section>

<section class="monument">

### [Bidarray]{.spatial} - [Iparla]{.coverage} 1 - [Tumulus-cromlech]{.type}

#### Localisation

Carte 1345 Ouest Cambo-les-Bains.

Altitude : 830m.

Nous avons décrit en [@blotMonolithesPaysBasque1983, p.13] un monolithe dalle (Altitude 740m), érigé en bordure de la piste (actuel GR 10) qui, venant de Bidarray, escalade les premiers contreforts Nord-Est des crêtes d'Iparla, et arrive en terrain plat.

On trouve un ensemble de 6 monuments répartis sur un replat herbeux dominant, au Nord, le GR 10 d'une soixantaine de mètres environ. Le monument n°1 est le plus important et le plus visible de cette belle nécropole.

#### Description

Tumulus de 6m de diamètre et 0,50m de haut, entouré de 14 pierres bien visibles, pouvant atteindre, au Nord par exemple, 0,75m de haut et 0,70m de large.

#### Historique

Monument découvert en [mai 1972]{.date}.

</section>

<section class="monument">

### [Bidarray]{.spatial} - [Iparla]{.coverage} 2 - [Tumulus-cromlech]{.type}

#### Localisation

À 7m à l'Ouest Sud-Ouest de *Iparla 1 - Tumulus-cromlech*.

#### Description

Tumulus aplati de 10m de diamètre et 0,80m de haut, délimité par 14 pierres périphériques de 0,30m à 0,40m de haut en moyenne.

#### Historique

Monument découvert en [mai 1972]{.date}.

</section>

<section class="monument">

### [Bidarray]{.spatial} - [Iparla]{.coverage} 3 - [Tumulus]{.type}

#### Localisation

À 3m à l'Ouest Nord-Ouest de *Iparla 2 - Tumulus-cromlech*.

#### Description

Tumulus de 4m de diamètre et 0,40m de haut ; une vingtaine de pierres apparaissent en désordre à la surface de ce monument, mais il n'y a pas de péristalithe évident.

#### Historique

Monument découvert en [mai 1972]{.date}.

</section>

<section class="monument">

### [Bidarray]{.spatial} - [Iparla]{.coverage} 4 - [Cromlech]{.type}

#### Localisation

Il est à 6m à l'Ouest Sud-Ouest de *Iparla 3 - Tumulus* et à 10m à l'Ouest Sud-Ouest de *Iparla 2 - Tumulus-cromlech*.

#### Description

Cercle de 5m de diamètre, très légèrement surélevé, délimité par 5 petites dallettes disposées de façon radiale, perpendiculaires au tracé du cercle, ce qui est exceptionnel en Pays Basque ; nous avons cependant revu ce type d'architecture dans nos fouilles au col de Méatsé.

#### Historique

Monument découvert en [mai 1972]{.date}.

</section>

<section class="monument">

### [Bidarray]{.spatial} - [Iparla]{.coverage} 5 - [Tumulus-cromlech]{.type}

#### Localisation

Il est à 4m au Nord Nord-Ouest de *Iparla 4 - Cromlech*.

#### Description

Tumulus de 5m de diamètre et 0,30m de haut. Trois pierres bien visibles à sa périphérie, en secteur Sud, permettent de penser qu'il y a bien un péristalithe. On note une pierre centrale.

#### Historique

Monument découvert en [mai 1972]{.date}.

</section>

<section class="monument">

### [Bidarray]{.spatial} - [Iparla]{.coverage} 6 - [Cromlech]{.type}

#### Localisation

Il est à 7m au Nord-Est de *Iparla 5 - Tumulus-cromlech*.

#### Description

Cercle de 4,60m de diamètre délimité par 4 pierres bien visibles dans le secteur Sud ; il existe aussi une pierre centrale.

#### Historique

Monument découvert en [mai 1972]{.date}.

</section>

<section class="monument">

### [Bidarray]{.spatial} - [Iparla]{.coverage} 7 - [Tumulus-cromlech]{.type}

#### Localisation

Même carte.

Altitude : 812m.

Monument isolé, situé sur un vaste plateau à 500m environ au Sud Sud-Ouest de *Iparla 6 - Cromlech*, à quelques mètres à l'Ouest de la piste pastorale reprise par le GR 10.

#### Description

Tumulus de 8m de diamètre et 0,50m de haut. Six dalles bien visibles, particulièrement au Nord et au Sud marquent la périphérie du monument. Au centre, une dépression allongée à grand axe Est-Ouest, réniforme, reste probablement la trace d'une fouille ancienne.

#### Historique

Monument découvert en [septembre 1973]{.date}.

</section>

<section class="monument">

### [Bidarray]{.spatial} - [Iparla]{.coverage} 8 - [Tumulus-cromlech]{.type}

#### Localisation

Même carte.

Altitude : 650m.

Il est situé sur un replat à environ 150m à l'Est Nord-Est et au-dessus de la bergerie Belzaouzk et sur la gauche de la piste qui monte du petit col qui sépare cette bergerie de Bourouzunekoborda.

#### Description

Tumulus de terre, circulaire, à sommet aplati en galette, mesurant 9,50m de diamètre et 0,60m de haut. Sa périphérie est marquée par 6 pierres : 4 en secteur Sud-Ouest et 2 en secteur Nord-Est.

#### Historique

Monument découvert en [octobre 1974]{.date}.

</section>

<section class="monument">

### [Bidarray]{.spatial} - [Lacho]{.coverage} - [Dolmen]{.type}

#### Localisation

Altitude : 550m

Il se trouve à environ 120m à l'Ouest et en contre-bas de *Lacho 1 - Monolithe*.

#### Description

La [chambre funéraire]{.subject} de ce monument, orientée Nord-Sud paraît longue de 0,90m à 1m et large en moyenne de 0,75m. Elle est délimitée par 5 dalles de [grès]{.subject} rose, d'environ 6 à 8 centimètres d'épaisseur dont les sommets dépassent de peu la surface du sol. La dalle Nord mesure 0,69m de long et 0,15m de haut ; la dalle Ouest mesure 0,80m de long et la paroi Est est formée de deux dalles, l'une de 0,36m de long et 0,12m de haut, l'autre de 0,46m de long et 0,13m de haut. Cette chambre est au centre d'un tumulus mixte de terre et de pierres dont certaines apparaissent dans le quadrant Nord-Est ainsi qu'à l'Ouest. À la périphérie Ouest de ce tumulus, faiblement marqué, gît, une dalle de [grès]{.subject} bien visible de forme trapézoïdale, de 2,50m de long et 1,67m de large, à grand axe Nord-Ouest Sud-Est, qui ne semble pas être du tout en rapport avec ce monument.

#### Historique

Monument découvert par [F. Meyrat]{.creator} en [décembre 2010]{.date}.

</section>

<section class="monument">

### [Bidarray]{.spatial} - [Lacho]{.coverage} 1 - [Monolithe]{.type}

#### Localisation

Altitude : 580m.

Il se trouve à une quinzaine de mètres au Sud du sentier qui monte vers col de Lacho en venant des bergeries Turchilen et Larhanton.

On peut voir un éperon rocheux à une centaine de mètres à l'Est et un autre à environ 150m à l'Ouest en contre-bas.

#### Description

Cette dalle de [grès]{.subject} rose gît sur un sol légèrement incliné vers l'Ouest, et présente une forme grossièrement parallélépipédique à sommet orienté vers le Sud-Ouest. Elle mesure 3,30m dans son plus grand axe, 2,05m à sa base, qui est épaisse de 0,42m alors que le sommet n'est que de 0,16m d'épaisseur. Il est intéressant de noter que la forme pointue du sommet résulte d'un épannelage de la totalité de la base Ouest, et d'un autre plus discret au Sud. On retrouve ces traces de régularisation sur le côté Nord, et surtout à l'Est qui présente une très importante encoche, ayant sans doute dépassé l'attente du « tailleur de pierre ».

#### Historique

Ce monolithe a été trouvé en [décembre 2010]{.date} par [F. Meyrat]{.creator}.

</section>

<section class="monument">

### [Bidarray]{.spatial} - [Lacho]{.coverage} 1 - [Pierre couchée]{.type} ([?]{.douteux})

#### Localisation

Altitude : 520m.

Elle gît tout au bord Nord du sentier - qui la contourne nettement - reliant le col de Lacho à Bidarray, quand on se dirige vers l'Est. Le terrain est en pente vers le Nord, ainsi que la pierre. On ne note pas de signes d'éboulis dans les environs immédiats.

#### Description

Ce bloc de [grès]{.subject}, de forme grossièrement parallélépipédique mesure environ 2,50m de long et 1,50m de large, à grand axe orienté Nord-Sud et présente une épaisseur décroissante (0,40m au sud et 0,20m à 0,10m au Nord). Le bord Nord semble résulter d'une cassure, alors qu'un segment du bord Est, à sa jonction avec le bord Nord, présenterait peut-être des traces d'épannelage.

#### Historique

Pierre trouvée par [F. Meyrat]{.creator} en [janvier 2011]{.date}.

</section>

<section class="monument">

### [Bidarray]{.spatial} - [Lacho]{.coverage} 2 - [Pierre couchée]{.type} ([?]{.douteux})

#### Localisation

Elle est située dans une pente assez marquée, à 150m au-dessous et au Sud-Ouest du piton rocheux (à la cote 536), et à environ 400m au Nord-Est de la BF 86.

Altitude : 470m.

#### Description

Gros bloc de [grès]{.subject} en forme de pain de sucre, à grand axe Est-Ouest et à pointe Ouest. Il mesure 4,80m de long, 2,10m à sa base, 0,60m à la pointe, et possède une épaisseur qui varie de 0,75m à 0,95m. Les éventuelles traces d'épannelage, par exemple à la base, sont fort discrètes.

#### Historique

Pierre trouvée par [F. Meyrat]{.creator} en [décembre 2010]{.date}.

</section>

<section class="monument">

### [Bidarray]{.spatial} - [Lacho]{.coverage} - [Tumulus]{.type}

#### Localisation

Il se trouve à 7m à l'Est de la [chambre funéraire]{.subject} de *Lacho - Dolmen*.

#### Description

Petit tumulus pierreux circulaire de 2,40m de diamètre et 0,30m de haut, constitué de petits blocs de [grès]{.subject} et de fragments de dalles, disposés de façon concentrique. Monument très semblable au *tumulus de Buluntza*, à celui d'*Apatessarro 8*, à celui d'*Erreta* ou de *Caminarte*.

#### Historique

Monument découvert par [F. Meyrat]{.creator} en [décembre 2010]{.date}.

</section>

<section class="monument">

### [Bidarray]{.spatial} - [Lacho]{.coverage} - [Tumulus-cromlech]{.type}

#### Localisation

À 3m à l'Ouest Sud-Ouest de *Lacho - Dolmen*.

#### Description

On note un tumulus pierreux d'environ 4m de diamètre et 0,50m de haut, constitué de terre et de nombreux fragments de dalles ; il semble que la périphérie Sud-Ouest de ce tumulus soit assez nettement délimitée par une dizaine de pierres disposées en arc de cercle. Au centre (après un léger dégagement de la couverture herbeuse) on peut voir une petite [ciste]{.subject} rectangulaire, formée de 4 dalles, orientée Nord-Ouest Sud-Est et dont les dimensions sont de 0,65m de long et 0,40m de large.

#### Historique

Monument découvert par [F. Meyrat]{.creator} en [décembre 2010]{.date}.

</section>

<section class="monument">

### [Bidarray]{.spatial} - [Méatsékobizkarra]{.coverage} - [Monolithe]{.type}

#### Localisation

Il est situé à 80m au Sud Sud-Ouest du très beau cromlech entourant la BF 81 (qui l'a massacré), et se trouve en bordure d'un chemin relativement récent, creusé au bulldozer. Il semble donc que sa position actuelle ne soit pas l'ancienne, mais elle ne doit en être éloignée que de quelques mètres au maximum.

Altitude : 715m.

On notera, là encore, la présence d'un point d'eau tout proche, à une dizaine de mètres au Nord. Il est par ailleurs intéressant de constater que ce monolithe ainsi que celui déjà décrit par nous sous le nom de *monolithe de l'Artzamendi*, se trouvent tous deux à proximité d'une borne frontière (la borne 82 pour ce dernier) et il n'est pas exclu que ces pierres soient les ancêtres directs de ces deux bornes frontières ; on note par exemple que le monolithe ici décrit possède deux repères bien visibles, sous forme de deux petites croix profondément gravées sur sa face supérieure. On connaît des monolithes, possédant eux aussi des inscriptions « modernes », qui jouent encore le rôle de borne frontière comme à Eyharcé ou à Gorospil.

#### Description

Il se présente sous la forme d'un imposant bloc de [grès]{.subject} rose couché sur le sol, de forme grossièrement triangulaire, orienté Nord-Ouest Sud-Est. Long de 3m, il présente une épaisseur variable : la base, qui mesure 1,96m de large est épaisse de 0,20m ; la partie médiane, de 1,57m de largeur est épaisse de 0,55m, et le sommet de 1,22m de large, ne mesure que 0,28m d'épaisseur. On ne note pas de traces d'épannelage, mais, sur sa face supérieure, on note deux croix gravées de ½ centimètre de profondeur mesurant 0,16m x 0,16m, situées à 1,02m de la base, et symétriquement par rapport à la ligne médiane.

#### Historique

Ce monolithe nous a été signalé par [Francis Meyrat]{.creator} en [juin 2008]{.date}.

</section>

<section class="monument">

### [Bidarray]{.spatial} - [Mendittipiko lepoa]{.coverage} - [Cromlech]{.type}

#### Localisation

Altitude : 732m.

Il se trouve à environ 1m au Sud du *dolmen de Zelai*, en plein col, et à une vingtaine de mètres à l'Ouest et en bas de la butte sur laquelle est érigé *Mendittipi - Tumulus-cromlech*.

#### Description

Cercle de 4,30m de diamètre, délimité par une dizaine de pierres au ras du sol, plus nombreuses dans la moitié Ouest.

#### Historique

Monument découvert par [J. Blot]{.creator} en [Mars 2010]{.date}.

</section>

<section class="monument">

### [Bidarray]{.spatial} - [Mendittipiko lepoa]{.coverage} 2 - [Cromlech]{.type}

#### Localisation

Altitude : 730m.

Il est à 1m au Sud-Ouest du *dolmen Zelai*.

#### Description

Seules 4 pierres sont visibles, dont une, à l'Ouest, mesure 0,77m de long et 0,20m de haut ; elles délimitent un cercle de 4,30m de diamètre.

#### Historique

Monument découvert par [Blot J.]{.creator} en [novembre 2011]{.date}.

</section>

<section class="monument">

### [Bidarray]{.spatial} - [Olhatia]{.coverage} - [Dalle plantée]{.type} ([?]{.douteux})

#### Localisation

Cette dalle était située sur un replat situé à droite de la route qui se termine en cul-de-sac (lieu-dit Olhatia) avant de se transformer en piste charretière pour se rendre au Plateau Vert en passant au flanc Sud-Ouest du mont Gakoeta.

Altitude : 330m.

#### Description

À cet endroit, était plantée selon un axe Nord-Sud, une belle dalle de [grès]{.subject} rose, à sommet arrondi résultant d'un épannelage évident. Elle mesurait 1,10m de haut, 0,80m de largeur à sa base et 0,40m d'épaisseur au maximum. Deux autres pierres, pratiquement tangentes, l'accompagnaient, l'une au Nord, (0,40m de large et 0,50m de haut), l'autre au Sud, (0,35m de large et 0,20m de haut), sans qu'il soit vraiment possible de parler de [chambre funéraire]{.subject}.

Borne pastorale ? Vestige dolménique ?

#### Historique

Dalle découverte par [J. Blot]{.creator} en [octobre 1971]{.date}, et détruite depuis.

</section>

<section class="monument">

### [Bidarray]{.spatial} - [Plateau Vert]{.coverage} 1 - [Dolmen]{.type}

#### Localisation

Carte 1345 Ouest Cambo-les-Bains.

Altitude : 570m.

Dolmen situé à l'extrémité Sud du Plateau Vert, et à une vingtaine de mètres au Nord du chemin qui monte de Bidarray, un peu avant qu'il n'arrive en terrain plat. Il est à 100m à l'Ouest d'une borde en partie ruinée, située dans un petit bosquet de hêtres.

#### Description

Tumulus terreux d'environ 8m de diamètre et 0,40m de haut, plus marqué dans sa partie Nord, le terrain étant légèrement en pente vers le Sud.

La [chambre funéraire]{.subject} centrale, orientée Nord Nord-Est Sud Sud-Ouest, est bien conservée et mesure 2,70m de long, 2m dans sa partie la plus large, au Nord, et 0,70m de profondeur. Elle est délimitée, au Nord, par une dalle de 2m de long ; à l'Ouest par une dalle de 2,20m de long, 0,70m de haut et 0,30m d'épaisseur. La paroi est se compose de 2 dalles dans le prolongement l'une de l'autre, inclinées vers l'intérieur : la plus grande, au nord, de 1m de long, est doublée à sa face externe par 2 dalles plus petites. De la paroi Sud, il ne reste que 2 dalles brisées et en partie enfouies qui ont glissé un peu à l'extérieur de la [chambre funéraire]{.subject}. Enfin à 1m au Sud-ouest gît une grande dalle de 1,70m de long, 1,20m de large, et 0,30m d'épaisseur, qui pourrait être un élément d'un montant latéral, ou plus probablement de la [couverture]{.subject} (ou table).

#### Historique

Dolmen découvert en [octobre 1972]{.date}.

</section>

<section class="monument">

### [Bidarray]{.spatial} - [Plateau Vert]{.coverage} 2 - [Dolmen]{.type}

#### Localisation

Carte 1345 Ouest Cambo-les-Bains.

Altitude : 490m.

Dolmen très difficile à repérer ; situé à l'extrémité Nord du Plateau Vert, à environ 150m à l'Ouest Nord-Ouest d'une bergerie érigée près d'un pointement rocheux.

#### Description

Il n'y a pas de tumulus visible. La [chambre funéraire]{.subject} mesure 2,30m de long, 0,80m de large, et 0,50m de haut ; elle est délimitée par 8 dalles en partie brisées. La paroi Ouest est formée par 3 dalles alignées selon un axe Nord-Sud ; la plus grande, au Nord, mesure 1m de long à sa base et 0,40m de haut, les 2 autres ne font que 0,50m de long et 0,30m de haut. La paroi Est se compose de 3 dalles alignées selon un axe Nord Nord-Est, Sud Sud-Ouest, de longueur croissante, de 0,40m pour la plus petite au Nord, à 1m de long pour celle du Sud ; 2 autres petites dalles, au ras du sol, complètent cette paroi. Il n'y a pas de couvercle visible.

#### Historique

Dolmen découvert en [mars 1971]{.date}.

</section>

<section class="monument">

### [Bidarray]{.spatial} - [Plateau Vert]{.coverage} 4 - [Cromlech]{.type}

#### Localisation

Carte 1345 Ouest Cambo-les-Bains.

Altitude : 600m.

Nous avons déjà décrit en 1972, [@blotNouveauxVestigesMegalithiques1972a, p.22] près d'une borde, un tumulus (dénommé à tort « dolmen » ; il s'agit en fait du *tumulus n°3 du Plateau Vert*), au flanc Nord-Ouest du Plateau Vert.

Le cromlech n°4 est situé sur la partie la plus élevée de ce Plateau Vert, à 8m au Nord d'une petite mare toujours bien visible.

#### Description

Cercle de 3m de diamètre, légèrement surélevé, délimité par 15 petites dalles dont 5 dans le secteur Ouest et 10 dans le secteur Est ; la [ciste]{.subject} centrale, de 1m x 0,50m, est délimitée par 5 petites dalles dont une nettement plus grande que les autres.

#### Historique

Monument découvert en [juin 1971]{.date}.

</section>

<section class="monument">

### [Bidarray]{.spatial} - [Plateau Vert]{.coverage} 5 - [Tumulus-cromlech]{.type}

#### Localisation

Carte 1345 Ouest Cambo-les-Bains.

Altitude : 600m.

Il est à environ 100m au Nord de *Plateau Vert 4 - Cromlech*, sur le début du plat de la cote 600.

#### Description

Très beau tumulus-cromlech terreux de 14m de diamètre et 1m de haut entouré de 19 dalles, au ras du sol, mais nettement visibles, particulièrement en secteur Sud Sud-Ouest.

#### Historique

Monument découvert en [juin 1971]{.date}.

</section>

<section class="monument">

### [Bidarray]{.spatial} - [Plateau Vert]{.coverage} 6 - [Cromlech]{.type}

#### Localisation

Il est situé à150m à l'Est Sud-Est de la petite mare déjà signalée au *Plateau Vert 4 - Cromlech*, et à 30m environ au Nord du rebord Sud-Ouest du plateau.

#### Description

Cercle de 3m de diamètre délimité par une vingtaine de dalles plus ou moins inclinées, couchées ; 2 seulement sont restées verticales. Deux dalles au centre pourraient faire partie de la [ciste]{.subject}.

#### Historique

Monument découvert en [juin 1971]{.date}.

</section>

<section class="monument">

### [Bidarray]{.spatial} - [Plateau Vert]{.coverage} 6 bis - [Cromlech]{.type}

#### Localisation

Altitude : 611m.

#### Description

Un cercle de 3,20m de diamètre est matérialisé par une quinzaine de pierres au ras du sol, et 2 au centre, verticales ; on note qu'au point de tangence des deux cercles, le n°6 bis est incomplet, 3 pierres ne sont pas dans le bon axe, la construction du 6 bis étant postérieure à celle du n°6.

#### Historique

Monument découvert par [Blot J.]{.creator} et [Meyrat F.]{.creator}, en [octobre 2010]{.date}.

</section>

<section class="monument">

### [Bidarray]{.spatial} - [Plateau Vert]{.coverage} 6 ter - [Cromlech]{.type}

#### Localisation

À environ 2m au Sud de *Plateau Vert n°6 bis - Cromlech*.

#### Description

Ce monument se présente comme un faible relief de terrain circulaire, de 5,20m de diamètre, avec une légère dépression centrale ; 3 pierres visibles à la périphérie au Sud.

#### Historique

Monument découvert par [Blot J.]{.creator} et [Meyrat F.]{.creator}, en [octobre 2010]{.date}.

</section>

<section class="monument">

### [Bidarray]{.spatial} - [Plateau Vert]{.coverage} 9 bis - [Coffre dolménique]{.type} (ou [ciste]{.subject} d'incinération ?)

#### Localisation

Altitude : 611m

#### Description

Une dizaine de dalles de [grès]{.subject} roses enfoncées dans le sol paraissent délimiter un coffre (?) d'environ 2,50m de long et 0,80m de large. Au Nord, la dalle la plus importante mesure 0,56m de long et 0,14m de haut ; au Sud, une autre, qui lui est parallèle, mesure 0,70m de long et 0,33m de haut. À l'Est et à l'Ouest, deux dalles inclinées l'une vers l'autre mesurant chacune 0,63m à la base, pourraient, alors, faire plutôt partie d'une petite [ciste]{.subject} de dimensions bien plus modestes (0,70m x 0,70m).

#### Historique

Monument découvert par [Blot J.]{.creator} et [Meyrat F.]{.creator}, en [octobre 2010]{.date}.

</section>

<section class="monument">

### [Bidarray]{.spatial} - [Plateau Vert]{.coverage} 10 - [Dolmen]{.type}

#### Localisation

Altitude : 575m.

À une dizaine de mètres l'Est Nord-Est de la piste qui se rend au col de l'Âne.

#### Description

Au centre d'un tumulus de terre de 7m de diamètre environ apparaît une dalle de [grès]{.subject} [triasique]{.subject} inclinée vers le Nord-Ouest, de forme triangulaire, orientée selon un axe Est Nord-Est Ouest Sud-Ouest. Elle mesure 1,47m de long, 0,48m dans sa plus grande hauteur et 0,13m d'épaisseur. Toute sa périphérie visible présente des traces d'épannelage.

#### Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2010]{.date}.

</section>

<section class="monument">

### [Bidarray]{.spatial} - [Plateau Vert]{.coverage} 11 - [Dolmen]{.type}

#### Localisation

Altitude : 611 m 

Il est situé à environ 8m au Nord-Est de la piste empruntée par les véhicules 4x4, selon un axe Est-Ouest.

#### Description

On note une belle dalle de [grès]{.subject} rose, couchée au sol selon un axe Est-Ouest, de forme rectangulaire, mesurant 1,80m de long et 0,75m de large et d'une épaisseur moyenne de 0,10m. Quelques traces d'épannelage sont visibles à l'angle Sud et sur son bord Nord. Au Sud, un dégagement sommaire a mis en évidence de nombreux blocs de [grès]{.subject} qui semblent bien faire partie d'un tumulus, peu marqué mais visible cependant, d'environ 5m à 6m de diamètre. Certains de ces blocs recouvrent par endroit le bord Sud de la dalle, qui est légèrement inclinée vers le Sud.

#### Historique

Monument découvert en [février 2011]{.date} par [F. Meyrat]{.creator}.

</section>

<section class="monument">

### [Bidarray]{.spatial} - [Plateau vert]{.coverage} 12 - [Dolmen]{.type}

#### Localisation

Altitude : 605m

Il est situé à 4m au Nord de la piste des 4x4, tracée selon un axe Est-Ouest et à une quarantaine de mètres au Nord Nord-Ouest d'une petite mare.

#### Description

Au milieu d'un tumulus de 5m de diamètre, de 0,40m de hauteur, dont 2 pierres assez importantes (l'une au Nord-Est, mesure 0,80m x 0,75m) l'autre au Sud-Ouest, semblent baliser la périphérie, on remarque les restes d'une [chambre funéraire]{.subject} d'environ 1,50m de long et 1m de large, orientée selon un axe Est Sud-Est Ouest Nord-Ouest, dont les montants ne dépassent que de peu la surface du sol. Elle est délimitée au Nord par une dalle de 1,04m de long et 0,15m de haut, à l'Est Sud-Est par une dalle de 0,80m de long et 0,32m de haut, et au Sud Sud-Est par une autre dalle de 0,90m de long et 0,15m de haut.

#### Historique

Monument découvert par [F. Meyrat]{.creator} en [février 2011]{.date}.

</section>

<section class="monument">

### [Bidarray]{.spatial} - [Plateau Vert]{.coverage} 13 - [Dolmen]{.type}

#### Localisation

Altitude : 605m

Il est situé à une dizaine de mètres à l'Ouest d'une petite mare.

#### Description

[chambre funéraire]{.subject} à grand axe orienté Nord-Sud, au milieu d'un tumulus terreux, d'environ 6m de diamètre sans péristalithe visible. La chambre, longue de 1,40m environ et 1,10m de large est délimitée par 3 dalles au ras du sol. À l'Ouest, une dalle de 0,87m de long, 0,15m de haut et 0,05m d'épaisseur ; à l'Est, une autre dalle de 0,90m de long, 0,08m de haut et 0,04m d'épaisseur. Enfin, au Nord, une dernière dalle pratiquement couchée sur le sol et qui mesure 0,45m à la base et 1,10m de « haut » ainsi qu'environ 0,05m d'épaisseur peut être valablement considérée comme faisant partie de la chambre. À l'extérieur de la dalle Est, et à 2m du centre, on note, enfoncée dans le sol une dalle de 0,56m de long et 0,40m de large.

#### Historique

Monument découvert par [F. Meyrat]{.creator} en [février 2011]{.date}.

</section>

<section class="monument">

### [Bidarray]{.spatial} - [Plateau Vert]{.coverage} 14 - [Dolmen]{.type}

#### Localisation

Altitude : 605m.

Il est situé très exactement au Nord de la mare citée plus haut (*Plateau vert n°12 - Dolmen*), et tangent à elle.

#### Description

C'est surtout la dalle de [couverture]{.subject}, déplacée sans doute depuis longtemps, qui attire le regard. Les montants de la [chambre funéraire]{.subject}, à peine visibles au ras du sol ont nécessité d'être un peu dégagés pour confirmer la réalité du monument.

La dalle de [couverture]{.subject} est donc actuellement à la périphérie du tumulus qui devait mesurer environ 6m de diamètre, et sans doute de nature pierreuse comme le laisse supposer la pierraille qui s'éboule dans la mare, sous l'extrémité Sud Sud-Est de cette dalle. Celle-ci, de forme approximativement rectangulaire, mesure 1,50m dans son plus grand axe (orienté Sud Sud-Est, Nord Nord-Ouest), et 1,35m dans l'autre ; son épaisseur varie de 0,06m à 0,08m. On peut voir, semble-t-il, des traces d'épannelage au niveau des extrémités Nord-Ouest et Sud-Est de celle-ci.

La [chambre funéraire]{.subject}, visible au Nord de cette dalle de [couverture]{.subject}, n'est délimitée actuellement que par trois dalles : l'une, au Sud, mesure 1,20m de long, et se trouve à 2,30m de la mare ; la dalle Est mesure 0,65m de long, enfin la dalle Ouest, est à 0,64m de la dalle Sud, et mesure 0,40m de long.

</section>

<section class="monument">

### [Bidarray]{.spatial} - [Plateau Vert]{.coverage} 15 - [Dolmen]{.type}

#### Localisation

Altitude : 611m.

#### Description

La dalle de [couverture]{.subject} de forme grossièrement triangulaire, mesure 1,05m dans son plus grand axe Nord-Est Sud-Ouest et 0,90m dans le sens Nord-Ouest Sud-Est ; son épaisseur visible est de 3 à 4 centimètres environ. La partie supérieure d'une autre dalle est visible à son bord Nord-Ouest, dont elle rejoint l'angle Est. ; elle mesure 0,60m de long et 3 cm d'épaisseur ; deux autres dalles apparaissent légèrement au Nord-Ouest et au Nord-Est. Il semble qu'il existe un tumulus de très faible relief, d'environ 5m à 6m de diamètre.

#### Historique

Monument découvert par [J. Blot]{.creator} en [mars 2011]{.date}.

</section>

<section class="monument">

### [Bidarray]{.spatial} - [Talatzé]{.coverage} - [Dolmen]{.type} ou [ciste]{.subject} ([?]{.douteux})

#### Localisation

Altitude : 906m.

Il est situé à environ 25m au Sud-Est du GR 10, dont il est bien visible - sur la crête dénommée Talatzé, sur un terrain en pente vers le Nord Nord-Ouest.

#### Description

Il est au centre d'un tumulus de terre et de quelques pierres, de 6m de diamètre, actuellement peu marqué (0,20m), mais l'on doit tenir compte du soutirage dû à la pente. Un ensemble de dalles plantées qui émergent de quelques centimètres au-dessus du sol, délimitent une [chambre funéraire]{.subject} mesurant 2,40m de long et 1,20m de large, orientée Nord Nord-Ouest Sud Sud-Est. Le côté Est est marqué par 5 dalles dont 2 parallèles, mesurant 1,40m et 1,18m. Du côté Ouest, on note 6 dalles de dimensions variables, plus ou moins parallèles deux à deux, dont certaines atteignent 1m ou plus. (il semble que certaines dalles, à l'Est comme à l'Ouest puissent résulter du délitement d'une seule dalle originelle). Le côté Sud est indiqué par 4 dalles disposées de façon assez lâche, et de dimensions inférieures aux précédentes : 0,50m et 0,58m par exemple. Au Nord, la limite paraît indiquée par 3 dalles, dont celle du milieu mesurant 0,50m de long et 0,29m de large.

Il semblerait que 6 autres dalles dans le plongement de cette chambre, (3 au Nord et 3 au Sud), fassent penser soit à une chambre double, soit à un « couloir », soit à une disposition naturelle, tout ceci reste très discutable. Ce prolongement mesurerait 3m de long et 1m de large.

#### Historique

Monument découvert par [Velche P.]{.creator} en [août 2012]{.date}.

</section>

<section class="monument">

### [Bidarray]{.spatial} - [Urdaindenzarretako Zelaia]{.coverage} 1 - [Tumulus-cromlech]{.type}

#### Localisation

Altitude : 642m.

Il se trouve sur le plat de Lacho, à environ 65m au Sud-Ouest de la borde la plus au Nord du plateau. Une piste pastorale est tangent à sa périphérie Sud-Ouest.

#### Description

Tumulus-cromlech de 5m de diamètre environ et 0,40m de haut, de terre et de pierres ; quelques dalles plantées ou au ras du sol paraissent bien délimiter un péristalithe de 4m de diamètre.

#### Historique

Monument découvert par [A. Martinez]{.creator} en [juillet 2012]{.date}.

</section>

<section class="monument">

### [Bidarray]{.spatial} - [Urdaindenzarrateko Zelaia]{.coverage} 2 - [Cromlech]{.type}

#### Localisation

Altitude : 642m.

Il est à 9m à l'Est de *Urdaindenzarretako Zelaia 1 - Tumulus-cromlech*.

#### Description

Cercle de pierres ou de dalles en [grès]{.subject} rose local, de 5m de diamètre, délimité par 9 pierres, dépassant de peu le sol ; la plus notable est au Sud Sud-Est et mesure 1,20m de long et 0,30m de large. Le centre est légèrement surélevé (0,20m), et 5 dalles y délimitent une [ciste]{.subject} carrée de 1,10m de côté. La dalle la plus importante, à l'Ouest mesure 1m de long et 0,35m de large.

#### Historique

Monument découvert par [M. Inarra]{.creator} en [2012]{.date}.

</section>

<section class="monument">

### [Bidarray]{.spatial} - [Urdaindenzarrateko Zelaia]{.coverage} 3 - [Cromlech]{.type}

#### Localisation

Altitude : 642m.

Il est situé à 2m à l'Est de *Urdaindenzarrateko Zelaia 2 - Cromlech*.

#### Description

Dix pierres (ou les dalles périphériques), en [grès]{.subject} rose local, sont au ras du sol et délimitent un cercle de 4,80m de diamètre, au centre très légèrement surélevé (0,10m). Trois dalles à l'Est (dont une mesure 0,65m de long et 0,30m de haut), et deux autres au Nord et à l'Ouest délimitent une [ciste]{.subject} centrale bien visible.

#### Historique

Monument découvert par [M. Inarra]{.creator} en [2012]{.date}.

</section>

<section class="monument">

### [Bidarray]{.spatial} - [UrdaindenZarrateko Zelaia]{.coverage} 4 - [Cromlech]{.type}

#### Localisation

Altitude :649m.

Il est sur le plat de Lacho. Ce monument, *C4*, est à 1,50m au Sud de *Urdaindenzarrateko Zelaia 3 - Cromlech*.

#### Description

Une dalle de [grès]{.subject} [triasique]{.subject} apparaît au ras du sol, grossièrement quadrangulaire, mesurant 0,90m x 0,80m ; elle est entourée d'un péristalithe formé de 6 à 7 pierres au ras du sol, délimitant un cercle de 4,80m de diamètre ; pierres de dimensions très modestes, l'une d'elles, à l'Est atteint cependant 0,50m x 0,12m.

#### Historique

Monument découvert par [Meyrat F.]{.creator} en [mars 2014]{.date}.

</section>

<section class="monument">

### [Bidarray]{.spatial} - [Urdaindenzarrateko Zelaia]{.coverage} - [Borne]{.type}

#### Localisation

Altitude : 645m.

Cette borne est à 30m au Sud Sud-Ouest d'un groupe de 3 cabanes en pierres sèches ruinées, et à 54m au Nord Nord-Est de *UrdaindenZarrateko Zelaia 4 - Cromlech*, (à 52m de *Urdaindenzarrateko Zelaia 3 - Cromlech*).

#### Description

Bloc de [grès]{.subject} [triasique]{.subject} local en forme de parallélépipédique rectangle, couché au sol suivant un axe Nord-Est Sud-Ouest. Il mesure 1,30m de long, avec un côté Sud-Ouest (la base dirions-nous), un peu plus grand (0,66m) que le côté opposé (0,55m). De même cette base est plus épaisse (0,31m) que le sommet (0,14m). De très nombreuses traces d'épannelage sont visibles sur toute la périphérie, en particulier à la base, au sommet et sur le côté Sud-Est.

#### Historique

Borne trouvée par [Meyrat F.]{.creator} en [mars 2014]{.date}.

</section>

<section class="monument">

### [Bidarray]{.spatial} - [Zelai]{.coverage} - [Monolithe]{.type}

#### Localisation

Altitude : 725m.

Il se trouve à une cinquantaine de mètres environ au Sud-Ouest du *dolmen de Zelai*, sur un terrain en pente douce vers le Sud-Est.

#### Description

Dalle peu visible avant un dégagement important, lequel a été assumé par notre ami Francis Meyrat. Elle se présente sous la forme d'une grande dalle triangulaire, plus ou moins cordiforme, et très symétrique par rapport à un axe Nord-Ouest Sud-Est. De surface parfaitement plane, on note que toute sa périphérie présente des traces de taille (épannelage). Elle mesure 3,20m dans son axe Nord-Ouest Sud-Est et 3,50m dans sa plus grande largeur ; son épaisseur moyenne est d'environ 0,35m. Située au pied d'une forte pente, elle a très bien pu subir au cours des siècles les effets de la colluvion qui l'a en partie recouverte, comme l'avait été, à moindre échelle le monolithe *Artzamendi,* mais dont la pente sus-jacente était beaucoup moins forte. Cette grande dalle est tangente à son bord Sud Sud-Est, à un bloc rocheux enfoui en partie enfoui dans le sol : pierre de calage d'un menhir qui aurait été érigé ?

#### Historique

Monolithe découvert par [Blot J.]{.creator} en [décembre 2011]{.date}.

</section>

<section class="monument">

### [Bidarray]{.spatial} - [Zelaïkogaina]{.coverage} - [Monolithe]{.type}

#### Localisation

Il se trouve à 70m à l'Ouest Sud-Ouest du *cromlech* dénommé *Mendittipia* par J.M. de Barandiaran [@barandiaranContribucionEstudioCromlechs1949, p.205].

Altitude :735m.

#### Description

On note un bloc de [grès]{.subject} rose - couché sur le sol selon un axe Est-Ouest - de forme grossièrement parallélépipédique, mesurant 2,52m de long, 0,80m dans sa plus grande largeur, (due à un renflement sur son bord Sud), et 0,42m de large à ses deux extrémités. Son épaisseur, que l'on ne peut vraiment apprécier que sur son bord Sud, (le Nord étant en partie enfoui sous terre) atteint 0,28m en moyenne. Certes, cette pierre isolée au milieu du pâturage, est assez remarquable ; toutefois ses dimensions fort modestes laissent planer un sérieux doute sur son éventuel rôle de monolithe-borne pastorale... à noter qu'à une quinzaine de mètres au Nord, il semble que l'on puisse évoquer la présence d'un cromlech de 6m de diamètre, dont 6 pierres sont visibles.

#### Historique

Nous avions repéré cette pierre dès le début de nos prospections dans cette montagne en 1970, et ne l'avions pas retenue ; [L. Millan San Emeterio]{.creator} (San Sebastian) a de nouveau attiré notre attention sur cette pierre en [1985]{.date}, aussi avons-nous préféré la publier ici plutôt que de commettre une éventuelle omission.

</section>

<section class="monument">

### [Bidarray]{.spatial} - [Zutarria]{.coverage} - [Pierre couchée]{.type}

#### Localisation

Altitude : 813m.

Cette pierre gît sur un sol incliné vers l'Ouest, à plus de 300m au Nord de la croix qui est plantée à l'Ouest du GR 10, et à une dizaine de mètres à l'Est du tracé de ce même GR 10.

#### Description

Bloc de [grès]{.subject} [triasique]{.subject} parallélépipédique allongé selon un axe Nord-Sud, mesurant 2,90m de long, 0,60m à sa base Nord et 0,70m dans sa plus grande largeur ; l'épaisseur moyenne est de 0,25m environ.

L'extrémité Sud apparaît avoir été taillée en pointe, et l'ensemble de cette pierre semble présenter des traces d'épannelage.

#### Historique

Pierre découverte par [J.M. Lecuona Guelbenzu]{.creator} en [octobre 2010]{.date}.

</section>

# Biriatou

<section class="monument">

### [Biriatou]{.spatial} - [Faalégui]{.coverage} - [Dolmen]{.type} ([?]{.douteux})

#### Localisation

Carte IGN 1245 OT Hendaye Saint-Jean-de-Luz.

Altitude : 340m.

On atteint cette structure en montant directement depuis le col de Pittare au sommet de Faalégui, en suivant le flanc Est de ce mont ; on doit atteindre et longer une barre rocheuse, tout près de laquelle elle se situe.

Cette structure nous paraît particulièrement intéressante, dans la mesure où elle illustre bien les confusions possibles entre architectures d'origine anthropique et artefact naturel.

On note, à proximité immédiate d'un important amas rocheux, faisant partie de la barre ci-dessus évoquée, une structure assez évocatrice d'un dolmen. Cependant, seul le sol sur lequel elle repose est plat, alors que le terrain dans sa totalité est en pente accentuée, du Sud vers le Nord.

#### Description

Elle se compose d'un gros bloc rocheux (ou table), reposant à 0,80m de haut, sur deux « montants » presque verticaux qui paraissent délimiter une [chambre funéraire]{.subject} de 1,10m de profondeur, ouverte à l'Est. Le fond de cette chambre est fermé par une petite murette en pierres sèches. Enfin il existe à la périphérie Nord-Ouest comme une ébauche d'un petit amoncellement de pierraille pouvant évoquer les «restes» d'un tumulus pierreux. Les dimensions du bloc de couverture sont de 2,20m dans son axe Nord-Sud ; de 1,80m dans l'axe Est-Ouest, et une épaisseur maximum de 1,14m. Les deux montants sont légèrement inclinés, comme le reste des éléments rocheux du côté Nord.

En fait, il semble bien que la « table » de couverture soit un bloc rocheux ([gneiss]{.subject} probable) qui s'est détaché du filon d'origine, bien visible immédiatement au Sud, à la suite d'un phénomène naturel (érosion, gel...). Ce bloc a ensuite naturellement glissé, en suivant la pente naturelle, sur les éléments rocheux déjà en place, qui jouent maintenant le rôle de « montants ». Il paraît aussi évident que cette structure a pu, ensuite, être aménagée pour en faire un abri, tout comme l'a été le (vrai) *dolmen de Generalen Tomba* dans les Aldudes.

#### Historique

Cette très curieuse structure nous a été signalée par [P. Badiola]{.creator} en [août 2009]{.date}.

</section>

<section class="monument">

### [Biriatou]{.spatial} - [Faalégui]{.coverage} - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 415m.

Il est aménagé sur un terrain en pente vers le Nord-Est, à une quinzaine de mètres à l'Est d'un très petit riu, et à 70m au Nord-Ouest d'un enclos à bétail.

#### Description

Ce tertre asymétrique, au sommet très aplati, (plate-forme de tir ?), est de forme ovale, sa périphérie Nord-Est étant la plus pentue. Il mesure 9m dans son axe Nord-Ouest Sud-Est et 10m dans l'axe de la pente (Nord-Est Sud-Ouest). Sa hauteur, peu marquée est de 0,60m.

#### Historique

A été trouvé en [mai 2011]{.date} par le [groupe Hilharriak]{.creator}.

</section>

<section class="monument">

### [Biriatou]{.spatial} - [Faalégui]{.coverage} - [Tumulus]{.type}

#### Localisation

Carte 1245 Ouest

Altitude : 496m.

Ce tumulus est érigé très exactement au sommet de la colline qui domine au Nord le col de Pittare, dit aussi col des Poiriers.

#### Description

Tumulus pierreux imposant de 16m de diamètre et 2m de haut, formé de blocs de [grès]{.subject} de la taille d'un pavé, ou plus volumineux ; ils sont plus visibles dans la moitié Sud-Ouest du tertre. La périphérie est bien soulignée par de nombreux blocs, sans que l'on puisse cependant parler de « péristalithe ».

#### Historique

Tumulus découvert en [mars 1997]{.date}.

</section>

<section class="monument">

### [Biriatou]{.spatial} - [Lizarlan]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 115m.

Il se trouve sur la rive gauche de la route qui se détache de celle qui monte à Biriatou, et longe la Bidassoa en direction de la maison Lizarlan. Il est érigé sur un petit plateau qui domine la route de 2m environ, à une vingtaine de mètres au Sud-Ouest de celle-ci et tout de suite après un virage très marqué vers la gauche.

#### Description

Tumulus pierreux en forme de galette aplatie, érigé sur un terrain en légère pente vers le Nord-Ouest. Il mesure 8m de diamètre, 0,30m à 0,40m de haut, et 3 arbres poussent à sa surface ; au centre et en partie recouverte par les racines d'un arbre, apparaît, semble-t-il une structure circulaire faite de pierres. Il semblerait aussi que quelques pierres, plus volumineuses que les autres, délimitent un péristalithe.

#### Historique

Monument découvert par [I. Txintxuretta]{.creator} en [octobre 2010]{.date}.

</section>

<section class="monument">

### [Biriatou]{.spatial} - [Lumaberde]{.coverage} - [Dolmen]{.type}

#### Localisation

Altitude : 214m.

Ce dolmen (ou coffre dolménique) est situé dans un ensellement à 40m à l'Ouest du sommet principal de la colline, à une dizaine de mètres à l'Est d'un second sommet moins marqué et avant que le terrain ne redescende. Il est très difficile à trouver lorsque la végétation occupe totalement l'espace. Il est tangent au Sud du sentier d'accès, fort peu marqué par les rares passages humains ou animaux.

#### Description

Ce monument est d'interprétation peu aisée ; il vient d'être dégagé à nouveau en février 2016 par Meyrat F. pour étude. On note un tumulus mixte de 4,20m de diamètre et de très faible hauteur (0,15m à 0,20m), où apparaissent de nombreux blocs ou dalles de [grès]{.subject} local, parmi lesquelles 5 ou 6 paraissant artificiellement disposées, délimitant une [chambre funéraire]{.subject} mesurant environ 2m de long, 0,50m de large et à grand axe orienté Nord-Ouest Sud-Est. Etant érigé sur la ligne de crête, le tumulus est semble-t-il l'objet d'un soutirage qui s'effectue à la fois vers le Nord et surtout vers le Sud, ce qui donne une chambre décentrée vers le Nord.

Il semble bien aussi qu'il existe un péristalithe constitué d'une quinzaine de blocs de [grès]{.subject} local plus ou moins volumineux.

#### Historique

Site découvert par [Cl. Chauchat]{.creator} et [J.L. Tobie]{.creator}, non décrit mais simplement cité en [1966]{.date}{.creator}, dans [@chauchatSeptDolmensNouveaux1966].

</section>

<section class="monument">

### [Biriatou]{.spatial} - [Mandale]{.coverage} - [Tumulus]{.type}

#### Localisation

Carte 1245 Ouest.

Altitude : 480m.

Tumulus pratiquement tangent à la frontière, et à 100m à l'Ouest de la BF 573.

#### Description

Tumulus essentiellement pierreux, mais recouvert de terre ; bien visible, il présente un diamètre est de 10m et une hauteur d'environ 1m.

#### Historique

Tumulus découvert en [mai 1989]{.date} ([J. Blot]{.creator}).

</section>

<section class="monument">

### [Biriatou]{.spatial} - [Mandale]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 547m.

Il se situe à une quinzaine de mètres à l'Ouest de la piste qui se détache, vers le Sud, du GR 10, après que celui-ci ait contourné la colline de Mandale, en venant d'Ibardin. À l'Est de ce GR 10, et en symétrique se voit une autre surélévation de terrain correspondant à un affleurement rocheux naturel ; un peu plus haut, toujours à l'Est, se trouve la « Redoute de la Baïonnette ».

#### Description

Tumulus circulaire, à sommet aplati, recouvert d'une abondante végétation, (ajoncs), comme tout le site. Il mesure environ 0,80m de haut, et 12m de diamètre. On ne distingue pas de péristalithe, ni de pierres à sa surface.

#### Historique

Monument découvert par [Blot J.]{.creator} en [juin 1968]{.date}.

</section>

<section class="monument">

### [Biriatou]{.spatial} - [Col d'Osin]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude :

Il est situé à environ 25m au Nord-Est de la piste (l'ancien GR 10 qui se rend à Biriatou) en partant du col d'Osin.

#### Description

Petit tumulus pierreux, recouvert d'herbe, situé au Nord-Ouest du dolmen d'Osin ; il mesure 3,50m de diamètre et 0,30m de haut.

#### Historique

Monument découvert par [J. Blot]{.creator} en [avril 2010]{.date}.

</section>

<section class="monument">

### [Biriatou]{.spatial} - [Osin]{.coverage} - [Monolithe]{.type}

#### Localisation

Altitude : 371m.

Cette belle dalle gît en bordure Sud de la piste pastorale qui se rend vers Biriatou, après sa bifurcation d'avec celle qui monte vers le Xoldokogagna.

#### Description

Belle dalle de [grès]{.subject} local, de forme parallélépipédique, mesurant 2,02m de long, et 0,42m d'épaisseur en moyenne. Sa largeur est de 0,90m à sa base et de 0,77m, avant son rétrécissement à l'Est. Il semblerait qu'elle présente des traces d'épannelage à son sommet Est et à son bord Nord.

#### Historique

Dalle trouvée par [F. Meyrat]{.creator} en [février 2016]{.date}.

</section>

<section class="monument">

### [Biriatou]{.spatial} - [Osin]{.coverage} 2 - [Tumulus]{.type}

#### Localisation

Altitude : 372m.

#### Description

Petit tumulus circulaire, mixte, de terre et de quelques pierres, mesurant 5m de diamètre et 0,20m de haut.

#### Historique

Tumulus découvert par [F. Meyrat]{.creator} en [février 2016]{.date}.

</section>

<section class="monument">

### [Biriatou]{.spatial} - [Osin]{.coverage} 3 - [Tumulus]{.type}

#### Localisation

Altitude : 375m.

Il est situé à 7m à l'Est de *Osin 2 - Tumulus* et à 5m au Nord Nord-Ouest de *Osin 4 - Tumulus*.

#### Description

Petit tumulus mixte de terre et de pierres circulaire, mesurant 4,70m de diamètre et 0,20m de haut.

#### Historique

Tumulus découvert par [F. Meyrat]{.creator} en [février 2016]{.date}.

</section>

<section class="monument">

### [Biriatou]{.spatial} - [Osin]{.coverage} 4 - [Tumulus]{.type}

#### Localisation

Altitude : 372m.

Il est situé à 5m au Sud Sud-Est de *Osin 3 - Tumulus*.

#### Description

Petit tumulus circulaire mixte mesurant 4,70m de diamètre et 0,20m de haut.

#### Historique

Tumulus découvert par [F. Meyrat]{.creator} en [février 2016]{.date}.

</section>

<section class="monument">

### [Biriatou]{.spatial} - [Osingozelaya]{.coverage} - [Pierre plantée]{.type}

#### Localisation

Altitude : 386m.

Il est situé près du sommet du mont Osingocelaya qui domine au Nord les deux pistes pastorales se rendant au Xoldokogagna et à Biriatou.

#### Description

Pierre plantée verticalement, de forme triangulaire à base inférieure, mesurant 1,70m de haut, 0,80m d'épaisseur en moyenne et 1,30m de large à sa base ; elle est légèrement inclinée vers le Sud et vers le Nord-Est.

#### Historique

Pierre trouvée par [F. Meyrat]{.creator} en [février 2016]{.date}.

</section>

<section class="monument">

### [Biriatou]{.spatial} - [Osinkozelaya]{.coverage} - [Tumulus]{.type}

#### Localisation

Au flanc Sud-Est du mont Osingozelaya qui domine, à l'Ouest, le Col des Poiriers dit aussi de Pittare, riche de ses 6 tumulus-cromlechs. Il se trouve sur une petite éminence, à 340m d'altitude, et à 20m environ au Sud-Ouest de la piste.

Altitude : 340m.

#### Description

Tumulus pierreux, de 10m de diamètre, fait d'un amoncellement de petites dalles de [grès]{.subject} rose. Sa hauteur est difficile à apprécier, entre 30 et 50 centimètres. On note une importante excavation dans le secteur Sud-Est, comme si le monument avait fait l'objet d'une fouille clandestine.

#### Historique

Monument découvert en [mai 2009]{.date} par [J. Blot]{.creator}.

</section>

<section class="monument">

### [Biriatou]{.spatial} - [Rocher des Perdrix]{.coverage} - [Pierre couchée]{.type} et/ou [tumulus]{.type} (??)

#### Localisation

Altitude : 238m et à environ 600m au Sud-Ouest du Rocher des Perdrix, à la jonction des tracés de l'actuel GR et de l'ancien.

#### Description

On peut voir un assez volumineux bloc de [grès]{.subject}, de forme triangulaire, presque isocèle, mesurant 1,52m à sa base ; il semble présenter des traces de régularisation à sa base Est et repose sur l'extrémité Est de ce qui pourrait être un tumulus de forme ovale, de 5,50m x 4m et 0,30m de haut, à la surface duquel apparaissent une vingtaines de pierres.

Phénomène naturel dû à la solifluxion, ou « muga » intentionnellement disposé là ? Très douteux...

#### Historique

Monument découvert par [Meyrat F.]{.creator} en [janvier 2017]{.date}.

</section>

<section class="monument">

### [Biriatou]{.spatial} - [Rocher des Perdrix]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 350m.

Il est situé quasiment à l'extrémité d'une petite croupe, au Nord-Ouest des ruines d'une bergerie, et à une vingtaine de mètres à l'Est du rocher le plus proéminent.

#### Description

Tumulus parfaitement circulaire de 6m de diamètre, et 0,30m de haut, constitué d'une multitude de petits fragments de dalles et de terre ; au centre se voit une dépression de 2,30m de diamètre (vestige d'une fouille ancienne ?).

#### Historique

Monument découvert par [J. Blot]{.creator} en [avril 2010]{.date}.

</section>

<section class="monument">

### [Biriatou]{.spatial} - [Xoldokogaina]{.coverage} - [Cromlech]{.type}

#### Localisation

Altitude : 460m.

Il est tangent à la piste qui monte au Xoldokogagna, sur la gauche en montant.

#### Description

Edifié sur un terrain en légère pente ; une vingtaine de pierres délimitent un cercle de 2,70m de diamètre légèrement surélevé en son centre, où se trouve une dalle plantée.

#### Historique

Monument découvert par [J. Blot]{.creator} en [avril 2010]{.date}.

</section>

<section class="monument">

### [Biriatou]{.spatial} - [Xoldokocelai]{.coverage} - [Dalle plantée]{.type}

#### Localisation

Altitude : 370m.

À une quinzaine de mètres au Sud-Ouest de la piste pastorale.

#### Description

On voit une pierre plantée de 1,60m de long à sa base et 0,75m de haut, épaisse de 0,11m. Il semblerait que l'on puisse deviner à sa périphérie les vestiges d'un tumulus qui aurait eu environ 2m à 3m de diamètre... S'agit-il des restes d'un dolmen ?

#### Historique

Dalle découverte par [J. Blot]{.creator} en [avril 2010]{.date}.

</section>

<section class="monument">

### [Biriatou]{.spatial} - [Xoldokocelai]{.coverage} 2 - [Dalle plantée]{.type} ([?]{.douteux})

#### Localisation

Altitude : 430m.

Erigée sur un terrain en pente vers l'Ouest, en pleine fougeraie.

#### Description

Cette dalle plantée verticalement mesure 1,08m de haut, 0,77m de large et 0,15m d'épaisseur ; elle est légèrement inclinée vers l'Ouest.

#### Historique

Dalle découverte par [Badiola P.]{.creator} en [mai 2012]{.date}.

</section>

<section class="monument">

### [Biriatou]{.spatial} - [Xoldokocelai]{.coverage} 4 - [Dolmen]{.type} ([?]{.douteux})

#### Localisation

Altitude : 370m.

Monument situé à environ 80m au Nord-Est de la piste, à la rupture de pente.

#### Description

On note une grande dalle de [grès]{.subject} gris horizontale, rectangulaire, mesurant 1,35m de long et 1m de large et 0,24m d'épaisseur, orientée Nord-Est Sud-Ouest.

Elle repose à son bord Nord-Ouest, sur une longue dalle verticalement enfouie dans le sol, mesurant 1,10m de long ; une autre pierre apparaît sous l'extrémité Est de la dalle horizontale.

#### Historique

Monument (douteux ?) découvert par [J. Blot]{.creator} en [avril 2010]{.date}.

</section>

<section class="monument">

### [Biriatou]{.spatial} - [Xoldokocelai]{.coverage} 5 - [Dolmen]{.type}

#### Localisation

Altitude : 390m.

Il est situé à environ 150m plus loin vers le Sud-Est, à quelques mètres au Sud-Ouest de la piste pastorale.

#### Description

On remarque un important tumulus circulaire fait de terre et de pierres, d'environ 9m de diamètre et 1m de haut, présentant une excavation centrale de 4m de diamètre. On ne distingue pas de dalles délimitant une [chambre funéraire]{.subject}, sauf, au bord Nord-Ouest du tumulus : une grande dalle, enfouie dans le sol, ainsi que deux autres, dont la longueur totale atteint 1,83m et qui pourraient représenter ce qui reste du chevet de cette [chambre funéraire]{.subject}. Il ne serait pas étonnant, dans le contexte de cette montagne, que les autres dalles de la chambre aient pu été récupérées par des carriers.

#### Historique

Monument découvert par [J. Blot]{.creator} en [avril 2010]{.date}.

</section>

<section class="monument">

### [Biriatou]{.spatial} - [Xoldokocelai]{.coverage} 6 - [Dolmen]{.type}

#### Localisation

Il se voit à très courte distance au Nord-Ouest de *Xoldokocelai 5 - Dolmen*.

#### Description

Il se présente sous la forme d'un tumulus circulaire de terre et de pierres d'environ 0,80m de haut au centre duquel apparaissent 3 dalles pouvant délimiter une [chambre funéraire]{.subject} de 1,40m de large et de longueur indéfinie, orientée Ouest-Est.

La dalle Ouest mesure 0,35m à sa base et 0,50m de haut ; celle du Sud mesure 1,05m de long et 0,35m de large.

#### Historique

Monument découvert par [J. Blot]{.creator} en [avril 2010]{.date}.

</section>

<section class="monument">

### [Biriatou]{.spatial} - [Xoldokocelai]{.coverage} 7 - [Dolmen]{.type} ([?]{.douteux})

#### Localisation

Altitude : 400m.

Situé un peu en hauteur et au Sud de *Xoldokocelai 6 - Dolmen*.

#### Description

On note un tumulus érigé sur un terrain assez en pente, mesurant 7m à 8m de diamètre et 0,80m dans sa plus grande hauteur, au sommet duquel apparaissent deux dalles verticales, dont l'une mesure 0,58m à la base et 0,55m de haut.

#### Historique

Monument (douteux ?), découvert par [J. Blot]{.creator} en [avril 2010]{.date}.

</section>

<section class="monument">

### [Biriatou]{.spatial} - [Xoldokocelai]{.coverage} 8 - [Dolmen]{.type} ([?]{.douteux})

#### Localisation

Altitude : 444m.

Ce monument, tout comme *Xoldokocelai 9 à 13 - Dolmen*, est caché dans la très abondante végétation qui recouvre tout le flanc Nord du mont Xoldokogaina. Pour le trouver, il faut partir de la borne 486 au sommet du plateau et prendre la direction Nord ; les monuments sont répartis le long de la ligne de séparation entre les communes de Biriatou et d'Urrugne.

#### Description

[chambre funéraire]{.subject} en grande partie cachée par la végétation, constituée d'une dizaine de dalles, orientée Ouest-Est et mesurant environ 3m de long et 1,70m de large. On compte 1 dalle à l'Est, 4 dalles au Sud, et 4 autres dalles au Nord ; elles ne paraissent pas jointives. Il n'y a pas de tumulus visible.

#### Historique

Monument découvert par [J. Blot]{.creator} en [avril 2010]{.date}.

</section>

<section class="monument">

### [Biriatou]{.spatial} - [Xoldokocelai]{.coverage} 9 - [Dolmen]{.type}

#### Localisation

Il est à 7mètres au Sud de *Xoldokocelai 8 - Dolmen ([?]{.douteux})*.

#### Description

Une [chambre funéraire]{.subject} de 1,83m de long et 1,50m de large, orientée Nord-Ouest Sud-Est, est délimitée par un ensemble de 8 dalles ; la paroi Nord-Ouest est formée par 2 dalles de 0,91m et 0,73m de long ; la paroi Sud-Ouest par 4 dalles et la paroi Sud-Est par 2 autres dalles de 0,46m et 1,02m de long. Seule la paroi Nord-Est ne possède pas de dalles actuellement visibles. Cette chambre est érigée sur un terrain en légère pente vers le Nord-Est, et ne possède aucun tumulus.

#### Historique

Monument découvert par [J. Blot]{.creator} en [avril 2010]{.date}.

</section>

<section class="monument">

### [Biriatou]{.spatial} - [Xoldokocelai]{.coverage} 10 - [Dolmen]{.type}

#### Localisation

Altitude : 440m.

On le trouve à 25m à l'Est de *Xoldokocelai 8 - Dolmen (?)*.

#### Description

Une grande [chambre funéraire]{.subject} de 3,20m de long et 2,55m de large est orientée Nord Nord-Est-Sud Sud-Ouest. Là encore la végétation rend la lecture du monument difficile ; toutefois, la paroi Sud-Ouest est formée par une grande dalle de 0,85m de long et 0,45m de large ; la paroi Sud-Est est délimitée par 2 dalles dont une de 1,10m de long (en deux fragments) et l'autre de 1,05m de long et 0,24m d'épaisseur. Enfin la paroi Nord-Ouest est marquée, semble-t-il, par 4 dalles ; aucune ne se voit au Nord-Est. Le monument est érigé sur un terrain en légère pente vers le Nord-Est. ; il n'y a pas de tumulus visible.

#### Historique

Monument découvert par [F. Meyrat]{.creator} en [avril 2010]{.date}.

</section>

<section class="monument">

### [Biriatou]{.spatial} - [Xoldokocelai]{.coverage} 12 - [Dolmen]{.type}

#### Localisation

Il est à une quinzaine de mètres au Nord de *Xoldokocelhay 11 bis - Dolmen*.

#### Description

Très belle [chambre funéraire]{.subject} de plus de 2m de long et 1,22m de large, orientée selon un axe Ouest-Est.

La paroi Sud est formée par une grande dalle de [grès]{.subject} gris, plantée dans le sol et inclinée vers l'extérieur, mesurant 1,91m à sa base, 0,72m de haut, 2,08m de long à son sommet et 0,12m d'épaisseur. La paroi Nord est représentée par une autre belle dalle de 1,74m à sa base, 0,53m de haut et 0,16m d'épaisseur. Il semble bien que les 3 grands fragments de dalles visibles dans la chambre, correspondent à la dalle de couverture brisée ; le plus à l'Est mesure 0,96m de long et 0,58m de large, le fragment central : 0,96m de long et 0,68m de large. Pas de tumulus visible, et terrain en légère pente vers l'Est.

#### Historique

Monument découvert par [F. Meyrat]{.creator} en [avril 2010]{.date}.

</section>

<section class="monument">

### [Biriatou]{.spatial} - [Xoldokocelai]{.coverage} 13 - [Dolmen]{.type}

#### Localisation

Il est tangent à l'Est au *Xoldokocelai 12 - Dolmen*.

#### Description

[chambre funéraire]{.subject} de 1,76m de long et 1,38m de large, orientée selon un axe Ouest-Est. La végétation est importante mais permet de distinguer une paroi Sud formée de 4 dalles, et une paroi Nord formée de 4 autres éléments. À l'Ouest se dresse une dalle de 0,97m de long à sa base, 0,60m de haut et 0,11m d'épaisseur. Pas de tumulus visible et terrain en légère pente vers l'Est. Une vue d'ensemble de ces deux monuments, de ces deux architectures dans le prolongement l'une de l'autre, nous suggère qu'il puisse s'agir non pas de deux monuments contigus mais d'un seul dolmen à deux chambres, d'un total de près de 4m de long, du même type que ce que nous décrivons aussi pour *Xoldokocelai 3 - Dolmen*.

#### Historique

Monument découvert par [F. Meyrat]{.creator} en [avril 2010]{.date}.

</section>

<section class="monument">

### [Biriatou]{.spatial} - [Xoldokocelai]{.coverage} 16 - [Dolmen]{.type}

#### Localisation

Altitude : 440m.

Il est situé à environ 13m à l'Ouest de la piste qui monte du Rocher des Perdrix.

#### Description

Tumulus surtout pierreux, érigé sur un terrain en légère pente, de 3 à 4m de diamètre et 0,30m de haut, la périphérie étant délimitée par une douzaine de pierres. Au centre se voit une [chambre funéraire]{.subject}, orientée Nord-Sud, de 1,70m x 0,50m délimitée 4 éléments, au ras du sol. À l'Ouest une dalle de 0,36m de long et 0,9m d'épaisseur ; à l'Est, 2 dalles, l'une de 0,32m de long, l'autre de 0,16m ; enfin au Sud, un fragment de dalles marque ce qui reste de la paroi initiale.

#### Historique

Monument découvert par [F. Meyrat]{.creator} en [avril 2012]{.date}.

</section>

<section class="monument">

### [Biriatou]{.spatial} - [Xoldokogaina]{.coverage} - [Monolithe]{.type}

#### Localisation

Altitude : 480m.

Il est à 60m au Nord de la borne géodésique et et à 50m à l'Ouest de la piste de crète.

#### Description

Cette dalle de [grès]{.subject} local en forme de pain de sucre, gît sur un terrain en légère pente. Elle mesure 3,10m de long, 0,90m de large au maximum, et 0,12m d'épaisseur.

Tout son bord Sud-Est présente des traces d'épannelage.

#### Historique

Monolithe découvert par [Badiola P.]{.creator} en [mai 2012]{.date}.

</section>

<section class="monument">

### [Biriatou]{.spatial} - [Xoldokocelai]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 371m.

On le trouve à une quinzaine de mètres à droite de la piste qui monte vers le sommet du Xoldokogaina, au niveau du replat d'où se détache la piste qui se rend vers les dolmens de Xoldokocelai. Il est sur une petite surélévation de terrain.

#### Description

Tumulus pierreux de 4,20m de diamètre et 0,20m de haut. On distingue assez nettement dans le secteur Sud 5 pierres qui forment une ébauche de péristalithe.

#### Historique

Monument découvert par [J. Blot]{.creator} en [avril 2010]{.date}.

</section>

<section class="monument">

### [Biriatou]{.spatial} - [Xoldokocelai]{.coverage} 4 - [Tumulus]{.type} ([?]{.douteux})

#### Localisation

Altitude : 450m.

La piste passe à 30m à l'Ouest et *Xoldokocelay 16 - Dolmen* est à 15m au Nord-Ouest. Erigé sur terrain en pente légère vers le Nord.

#### Description

Tumulus de terre et quelques pierres de 4m de diamètre et 0,40m de haut maximum. On note une dalle importante plantée dans le sol au Nord, de 1,50m de long, 0,32m de haut et 0,19m d'épaisseur. Cette dalle aurait pu bloquer une coulée de solifluxion.

#### Historique

Tumulus découvert par [F. Meyrat]{.creator} en [mai 2012]{.date}.

</section>

<section class="monument">

### [Biriatou]{.spatial} - [Xoldokocelai]{.coverage} - [Tumulus-cromlech]{.type}

#### Localisation

Altitude : 415m.

Situé à une vingtaine de mètres à droite de la piste qui monte au sommet de Xoldokocelai, sur un petit replat avec vue sur Hendaye et les Trois Couronnes.

#### Description

Tumulus de très faible hauteur, (0,20m environ) essentiellement formé d'une quarantaine de petites dalles ou fragments de dalles, plus ou moins enfouies dans le sol, et particulièrement nombreuses dans la moitié Sud du monument ; le centre en est peu fourni.

#### Historique

Monument découvert par [Blot C.]{.creator} en [décembre 2011]{.date}.

</section>

# Bunus

<section class="monument">

### [Bunus]{.spatial} - [Gaintzale]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 526m.

Ce tumulus est situé à une quarantaine de mètres à l'Ouest du sommet et à 30m au Nord-Ouest d'un réservoir en ciment.

#### Description

Tumulus circulaire de 5,50m de diamètre, très aplati (0,15m de haut), matérialisé, outre son faible relief, par une trentaine de petits blocs [calcaire]{.subject} plus ou moins enfouies dans le sol, dont une vingtaine pourraient semble-t-il marquer la périphérie. On note une légère dépression en secteur Sud.

#### Historique

Monument découvert par [P. Velche]{.creator} en [mai 2015]{.date}.

</section>

# Bussunarits

<section class="monument">

### [Bussunarits]{.spatial} - [Galhareko Pareta]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 593m.

Sur le sommet de la colline de ce nom, à 35m à l'Est d'un important réservoir en béton.

#### Description

Tumulus terreux, circulaire, très aplati, mesurant 5m à 6m de diamètre environ et 0,20m de haut. Une dizaine de pierres environ apparaissent en surface et en périphérie, sans qu'on puisse vraiment parler d'un péristalithe.

#### Historique

Tumulus trouvé par [I. Txintxuretta]{.creator} en [avril 2016]{.date}.

</section>

<section class="monument">

### [Bussunarits]{.spatial} - [Lauhiburu]{.coverage} - [Monolithe]{.type} ([?]{.douteux})

#### Localisation

Altitude : 654m.

Il se dresse verticalement, le long d'un sentier qui longe, en contre-bas, le flanc Nord-Est de la colline Lauhiburu, en lisière de feuillus. Il est à proximité immédiate et à l'Ouest de formations karstiques ; *la question se pose de savoir s'il est en position naturelle, ou disposé, en ce lieu, par la main d'homme*.

#### Description

Monolithe de 1,75m de haut, de forme généralement triangulaire à base inférieure, et prismatique à la coupe. Il présente donc 3 faces à étudier.

La face Sud, nettement triangulaire, mesurant 1,70m à la base, et assez lisse dans l'ensemble ; la face Ouest, d'aspect rectangulaire, mesurant 1m à la base et 0,75m au sommet, elle aussi sans reliefs notables ; enfin la face Est, assez semblable à la précédente, mesure 1,20m à la base et 0,84m au sommet. Ce monolithe ne présente aucune trace d'épannelage.

#### Historique

Monolithe trouvé par [I. Txintxuretta]{.creator} en [avril 2016]{.date}.

</section>

# Cambo

<section class="monument">

### [Cambo]{.spatial} - [Marienea]{.coverage} - [Tertre d'habitat]{.type} ([?]{.douteux})

#### Localisation

Altitude : 90m.

On le voit sur la gauche de la route qui se rend à Louhossoa, une centaine de mètres avant le pont qui l'enjambe.

#### Description

Ce « tertre » est visible sur un terrain pentu, très près du borde de route et mesure, comme d'habitude, environ 2m de haut et 18m à 20m de large. Ce qui pose problème est la présence dans le voisinage proche, de mouvements de terrains qui pourraient être rattachés à des éboulements, à de la solifluxion. Nous éliminons toute parenté avec les travaux du *Camp de César*.

#### Historique

Tertre vu par [Blot J.]{.creator} en [septembre 2001]{.date}.

</section>

# Espelette

<section class="monument">

### [Espelette]{.spatial} - [Errebi]{.coverage} 1 et 2 - [Cromlechs]{.type}

#### Localisation

Carte 1245 OT Hendaye Saint-Jean-de-Luz.

Altitude : 540m.

Ces deux cromlechs tangents sont situés à environ 40m au Sud Sud-Est du pointement rocheux coté 554, à quelques mètres à l'Ouest de la piste qui suit la ligne de crête.

#### Description

-   *Erebi 1 - Cromlech* : une dizaine de pierres en [grés]{.subject} local gris, au ras du sol, délimitent un cercle de 4,70m de diamètre dont l'intérieur est très légèrement surélevé.

-   *Erebi 2 - Cromlech* : est tangent au précédent à l'Ouest Sud-Ouest, matérialisé par 5 pierres disposées en arc de cercle.

On notera, un peu plus bas, à 380m d'altitude, sur un replat et à quelques mètres au Sud-Ouest d'une piste qui rejoint le col de Pinodieta, ce qui paraît être un léger tumulus de blocs de [grés]{.subject}, de 6m de diamètre, avec en son centre une dalle de 0,60m de long, orientée Ouest-Est,

Vestiges d'un dolmen ?

#### Historique

Monuments découverts par [Blot J.]{.creator} en [septembre 1972]{.date}.

</section>

<section class="monument">

### [Espelette]{.spatial} - [Errebi]{.coverage}- [Dalle couchée]{.type}

#### Localisation

Altitude : 578m.

Cette dalle couchée se trouve à 32m à l'Est du point géodésique marquant le sommet du mont Errebi, dans l'axe de la piste de crête.

#### Description

Cette dalle de [grés]{.subject}, couchée au sol est le seul élément lithique visible dans les environs et semble donc bien avoir été volontairement apportée ici.

De forme grossièrement triangulaire à sommet Sud-Ouest, elle mesure 1,20m à la base et 1,75m de la base au sommet. Tout le bord Ouest ainsi qu'une partie de la base Nord paraissent présenter des traces d'epannelage. Elle est brisée en son milieu. (Chute probable d'une dalle plantée...).

#### Historique

Dalle découverte en [septembre 2019]{.date} par [F. Meyrat]{.creator}.

</section>

<section class="monument">

### [Espelette]{.spatial} - [Errebi]{.coverage} - [Dolmen]{.type} ([?]{.douteux})

Nous tenons à rappeler ici qu'on peut noter, un peu plus bas, à 380m d'altitude, sur un replat et à quelques mètres au sud-ouest d'une piste qui rejoint le col de Pinodieta, ce qui paraît être un léger tumulus de blocs de [grés]{.subject}, de 6m de diamètre, avec en son centre une dalle de 0,60m de long, orientée ouest-est. Est-ce des vestiges d'un dolmen ?

#### Historique 

Monuments découverts par [Blot J.]{.creator} en [septembre 1972]{.date}.

</section>

<section class="monument">

### [Espelette]{.spatial} - [Gaztelaenea]{.coverage} - [Tumulus]{.type}

#### Localisation

Carte 1245 Est Espelette.

Altitude : 93m.

Il est situé à droite et à 30m environ du chemin qui relie Gaztelaeneakoborda à la ferme du même nom.

#### Description

Tumulus de terre bien visible, d'un diamètre de 15m et 0,80m de haut.

#### Historique

Monument découvert en [janvier 1973]{.date}. Il a été complètement rasé par des labours ultérieurs.

</section>

<section class="monument">

### [Espelette]{.spatial} - [Gorospil]{.coverage} 1 - [Cromlech]{.type}

#### Localisation

Carte : 1345 Ouest Cambo-les-Bains.

Altitude : 680m.

Il est situé sur la ligne de crête reliant la borne 691 au sommet coté 702, à peu près à mi-chemin entre les deux.

#### Description

Quatre pierres, au ras du sol, insérées dans un léger renflement circulaire de terrain, délimitent ce monument ; on note deux autre pierres à l'intérieur.

#### Historique

Monument découvert en [octobre 2009]{.date} par [J. Blot]{.creator}.

</section>

<section class="monument">

### [Espelette]{.spatial} - [Gorospil]{.coverage} 2 - [Cromlech]{.type}

#### Localisation

Carte OT 1245 Hendaye- Saint-Jean-de-Luz.

Altitude : 692m.

Ce monument est situé à 25m à l'Est Nord-Est de la borne géodésique 691.

#### Description

On note un léger relief circulaire sur lequel apparaissent, disposées en demi-cercle, 4 pierres, au ras du sol. La plus visible (sur laquelle est la marque peinte du GR), mesure 0,30m de long, les 3 autres mesurent entre 0,15m et 0,20m de long.

#### Historique

Monument découvert par le [groupe Hiharriak]{.creator} en [1999]{.date}.

</section>

<section class="monument">

### [Espelette]{.spatial} - [Gorospil]{.coverage} 3 - [Cromlech]{.type} ([?]{.douteux})

#### Localisation

À 11m au Nord de *Gorospil n°2 - Cromlech*.

#### Description

Sur un très léger relief circulaire de 2,50m à 3m environ, apparaissent 4 pierres de [grés]{.subject} ou de quartzite. La plus grande, en [grés]{.subject} gris, à l'Est, est en forme de dalle triangulaire couchée sur le sol mesure 0,80m de long et 0,70m à sa base.

#### Historique

Monument découvert en [1999]{.date} par le [groupe Hilharriak]{.creator}.

</section>

<section class="monument">

### [Espelette]{.spatial} - [Gorospil]{.coverage} 4 - [Cromlech]{.type}

#### Localisation

À 14m au Sud-Ouest de *Gorospil n°2 - Cromlech* et à 13m à l'Est Nord-Est de la borne géodésique 691.

#### Description

Monument douteux ; quatre pierres sont disposées sur un cercle de 2,20m de diamètre environ. La plus grande, à l'Est, en [grés]{.subject} gris, de forme triangulaire, mesure 0,77m de long et 0,37m à sa base.

#### Historique

Monument découvert par [J. Blot]{.creator} en [février 2010]{.date}.

</section>

<section class="monument">

### [Espelette]{.spatial} - [Mondarrain Ouest]{.coverage} - [Tertre d'habitat]{.type} ([?]{.douteux})

#### Localisation

Altitude : 490m.

Au flanc Ouest du Mondarrain, à 60m au-dessous de la piste qui le contourne horizontalement.

#### Description

Vaste tertre oblongue, allongé selon un axe Sud-Est Nord-Ouest, pouvant atteindre 3m de hauteur et mesurant 26m dans sa plus grande dimension, avec une surface plane de 20m x 12m. Il est longé par le riu qui coule vers l'Ouest. On note sur le plat de cette butte, un bourrelet circulaire avec une dépression centrale de 6m de diamètre : reste d'habitat ? sur un très grand « tertre d'habitat » ?

</section>

<section class="monument">

### [Espelette]{.spatial} - [Ouronea]{.coverage} - [Tumulus]{.type}

#### Localisation

Carte 1245 Est Espelette.

Altitude : 120m.

Il est à environ 100m au Nord Nord-Ouest de la borne IGN 146, dans un champ, et à 150m au Sud de la jonction de la route Espelette - Aïnhoa avec celle venant de Souraïde.

#### Description

Tumulus de terre de 14m de diamètre et 1m de haut.

#### Historique

Tumulus découvert en [avril 1974]{.date}. Il a été depuis rasé par des labours.

</section>

<section class="monument">

### [Espelette]{.spatial} - [Pinodieta]{.coverage} - [Tertre]{.type}

Souraïde

#### Localisation

Carte 1245 OT Hendaye Saint-Jean-de-Luz.

Altitude : 115m.

Tertre situé dans une prairie, à environ 250m au Sud du col, et à une quarantaine de mètres au Sud-Est du chemin qui monte directement vers le mont Erebi.

#### Description

Erigé sur un sol en légère pente vers le Nord, il est de forme légèrement ovale, mesure une dizaine de mètres dans sa plus grande dimension et 0,80m de haut environ.

#### Historique

Tertre découvert par [Blot J.]{.creator} en [octobre 2009]{.date}.

</section>

<section class="monument">

### [Espelette]{.spatial} - [Soporro]{.coverage} (Gagnekoborda) - [Cromlech]{.type}

#### Localisation

Altitude : 431m.

Ce monument se trouve toujours sur cette longue ligne de crête qui s'étend vers le Sud-Est en partant du mont Errebi, après être passé par le Col des Trois Croix ; il est sur la piste de crête, pratiquement tangent à un poste de tir à la palombe.

#### Description

Monument peu visible mais qui nous paraît fort probable ; on note une très légère surélévation de terrain en forme de galette circulaire très aplatie, mesurant 3m de diamètre et 0,15m de haut, délimitée par 4 pierres périphériques de faibles dimensions mais bien visibles.

#### Historique

Monument découvert par [F. Meyrat]{.creator} en [septembre 2019]{.creator}.

</section>

<section class="monument">

### [Espelette]{.spatial} - [Soporro]{.coverage} - [Tertre d'habitat]{.type} ([?]{.douteux})

#### Localisation

Altitude : 543m.

Il est situé un peu en contre bas de la piste de crête.

#### Description

On note un tertre terreux, de forme ovale, mesurant 12m x 10m et 1m de haut environ, à grand axe Nord-Ouest Sud-Est. On ne doit pas omettre de signaler un évidement du terrain, à la partie Sud-Est du tertre, qui résulte de l'extraction des terres à cet endroit : la question se pose de savoir si le creusement du sol n''avait pour but que de faire un tertre, ou s'il s'agissait d'une action à visée tout à fait différente (mise au jour d'un filon rocheux etc.)

#### Historique

Tertre découvert par [F. Meyrat]{.creator} en [septembre 2019]{.creator}.

</section>

# Esterençuby

<section class="monument">

### [Esterençuby]{.spatial} - [Arthé]{.coverage} 7 - [Cromlech]{.type}

#### Localisation

Altitude : 1000m.

Il se trouve sur la ligne de crête, à la limite du terrain remanié par les travaux, juste avant la rupture de pente. Rappelons qu'un ensemble de quatre cromlechs avait été démoli lors des travaux routiers.

#### Description

Petit cercle de 1m de diamètre environ délimité par une dizaine de pierres, au ras du sol, de dimensions très modestes.

#### Historique

Cercle découvert par [Blot J.]{.creator} en [octobre 1985]{.date}.

</section>

<section class="monument">

### [Esterençuby]{.spatial} - [Bihurri]{.coverage} - [Tertre d'habitat]{.type} ([?]{.douteux})

#### Localisation

Altitude : 733m.

Il est situé au Sud d'une clôture de barbelés, dans une prairie en pente douce vers le Sud et à 50m à au Nord-Ouest d'une petite borde.

#### Description

On note un important relief de terrain aux deux bords Ouest et Est parallèles, se terminant au Sud suivant un tracé semi-circulaire. Cette plate-forme semble bien d'origine anthropique, mais sa finalité n'a rien d'évident : grand tertre d'habitat aplani, plate-forme préparée dans un autre but (soubassement de construction ?) - la question reste en suspens.

#### Historique

Tertre découvert par [Meyrat F.]{.creator} en [octobre 2018]{.date}.

</section>

<section class="monument">

### [Esterençuby]{.spatial} - [Erreta]{.coverage} 1 - [Cromlech]{.type}

#### Localisation

Carte 1346 Est Saint-Jean-Pied-de-Port.

Altitude : 702m.

On le trouve au centre d'un petit col, à une dizaine de mètres au Nord-Est de la route qui monte vers l'Errozaté, tout de suite après un tournant en épingle à cheveu.

#### Description

Une dizaine de petits blocs de [grés]{.subject} gris au ras du sol, délimitent un cercle de 2,60m de diamètre.

#### Historique

Monument découvert par [A. Martinez Manteca]{.creator} en [2008]{.date}.

</section>

<section class="monument">

### [Esterençuby]{.spatial} - [Erreta]{.coverage} 2 - [Tumulus]{.type}

#### Localisation

Situé à une vingtaine de mètres au Nord Nord-Est de *Erreta 1 - Cromlech*.

#### Description

Tumulus pierreux de 5,40m de diamètre et 0,30m de haut ; nombreux petits blocs de [grés]{.subject} gris visibles en surface, mais pas de péristalithe visible.

#### Historique

Monument découvert en [2008]{.date} par [A. Martinez Manteca]{.creator}.

</section>

<section class="monument">

### [Esterençuby]{.spatial} - [Erreta]{.coverage} 3 - [Cromlech]{.type} ([?]{.douteux})

#### Localisation

Situé à 3m au Nord-Ouest de *Erreta 2 - Tumulus*.

#### Description

Huit petites pierres au ras du sol délimitent un cercle de 3,80m de diamètre (Monument douteux ?)

#### Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2009]{.date}.

</section>

<section class="monument">

### [Esterençuby]{.spatial} - [Erreta]{.coverage} 4 - [Cromlech]{.type}

#### Localisation

Altitude : 740m.

Il se trouve à 6m à l'Ouest de *Erreta 1 - Tumulus*, qui est très peu marqué en hauteur, avec de nombreuses pierres bien visibles.

#### Description

Une vingtaine de pierres, de volume variable (certaines atteignent 0,70m de long), délimitent un cercle très net de 3,20m de diamètre.

#### Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2009]{.date}.

</section>

<section class="monument">

### [Esterençuby]{.spatial} - [Erreta]{.coverage} 5 - [Cromlech]{.type}

#### Localisation

Il est tangent, à l'Ouest, au monument *Erreta 4 - Cromlech*.

#### Description

Une vingtaine de pierres au ras du sol délimitent un cercle de 3,50m de diamètre, lequel contient, tangent à sa partie Nord, un second petit cercle formé d'une dizaine de petites pierres.

#### Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2009]{.date}.

</section>

<section class="monument">

### [Esterençuby]{.spatial} - [Erreta]{.coverage} - [Dolmen]{.type} ([?]{.douteux})

#### Localisation

Altitude : 700mètres.

À peu près à mi-chemin entre le petit col, à une dizaine de mètres au Nord-Est de la route qui monte vers l'Errozaté et *Erreta 1 - Tumulus*.

#### Description

On note, immédiatement en bordure Sud de la route, sur un sol en légère pente vers l'Ouest, deux gros blocs rocheux, en [grés]{.subject} gris : l'un, le plus à l'Ouest, sensiblement parallélépipédique, mesure 1,40m de long, 1,30m de large et 0,75m de haut ; l'autre à l'Est du précédent, en est distant de 0,30m à 0,45m. Ses dimensions sont de 1,20m de long, 0,75m de large et 0,80m de haut. Ce bloc est enfoncé dans le sol, contrairement au premier qui est simplement posé. Ces deux volumineux éléments sont au centre d'un « bourrelet » de terrain circulaire, de 4m de diamètre, formant comme une ébauche de tumulus, et dans lequel sont incluses de nombreuses pierres.

#### Historique

Monument très douteux découvert par [Blot J.]{.creator} en [octobre 2009]{.date}.

</section>

<section class="monument">

### [Esterençuby]{.spatial} - [Erreta]{.coverage} Est 2 - [Tertres d'habitat]{.type}

#### Localisation

Altitude : 770m.

Sur le promontoire qui domine la route, au Nord.

#### Description

Deux petits tertres de terre quasiment tangents, d'environ 3m à 4m de diamètre et 0,40m de haut.

#### Historique

Tertres découverts en [1971]{.date} par [J. Blot]{.creator}.

</section>

<section class="monument">

### [Esterençuby]{.spatial} - [Erreta]{.coverage} sud n°1 - [Tertre d'Habitat]{.type}

#### Localisation

Monument le plus à l'Est et en altitude.

Altitude : 690m.

#### Description

Tertre mesurant 6m à 7m de diamètre ; hauteur difficile à apprécier (0,30m ?) du fait de sa construction sur un terrain en pente vers le Sud.

#### Historique

Monument découvert par [J. Blot]{.creator} en [1971]{.date}.

</section>

<section class="monument">

### [Esterençuby]{.spatial} - [Erreta]{.coverage} sud n°2 - [Tertre d'Habitat]{.type}

#### Localisation

Situé à environ 20m au Sud de *Erreta sud n°1 - Tertre d'Habitat*, et légèrement en contrebas.

#### Description

Réduit à l'état de vestige, érigé sur un sol en pente vers le Sud, il mesure une dizaine de mètres de diamètre, et est d'une hauteur peu appréciable. Il présent une légère excavation en son centre ; quelques pierres apparaissent en surface de ce monument essentiellement fait de terre.

#### Historique

Monument découvert par [J. Blot]{.creator} en [1971]{.date}.

</section>

<section class="monument">

### [Esterençuby]{.spatial} - [Erreta]{.coverage} 1 - [Tumulus]{.type}

#### Localisation

Carte 1346 Est Saint-Jean-Pied-de-Port.

Altitude : 730m.

Il est tout à fait en bordure et à droite de la route qui monte d'Esterençubi à Errozaté, sur un petit replat herbeux.

#### Description

Petit tumulus bien visible, en forme de galette aplatie mesurant 5m de diamètre et 0,25m de haut, en grande partie constitué de pierres : il y aurait même peut-être une double couronne de pierres.

#### Historique

Monument découvert en [juin 1971]{.date}.

</section>

<section class="monument">

### [Esterençuby]{.spatial} - [Erreta]{.coverage} sud n°3 - [Tumulus]{.type}

#### Localisation

Situé à environ 25m à l'Est Nord-Est de *Erreta sud n°1 - Tertre d'Habitat*.

Altitude : 688m.

#### Description

Ce monument circulaire, de terre et de pierres, mesure environ 8m de diamètre ; érigé sur un sol en pente vers l'Ouest, sa hauteur peut être estimée à 0,80m environ. On note une légère excavation centrale.

#### Historique

Monument découvert par [J. Blot]{.creator} en [1971]{.date}. En 2010, Manteca Martinez et ses compagnons ont baptisé ce monument « Tumulus ». La situation sur un terrain en pente, la dissymétrie qui en résulte n'est pas très en faveur de tumulus funéraires. Toutefois, sa structure pierreuse et ses dimensions peuvent en effet laisser planer un doute sur leur identité réelle.

</section>

<section class="monument">

### [Esterençuby]{.spatial} - [Erreta]{.coverage} sud n°4 - [Tumulus]{.type}

#### Localisation

Situé à une vingtaine de mètres à l'Est Nord-Est de *Erreta sud n°3 - Tumulus*.

#### Description

Tumulus de terre et de pierres de 7m de diamètre environ, érigé sur un sol à peu près plat. On note quelques pierres qui pourraient représenter une ébauche de [ciste]{.subject} à son sommet, et peut-être un péristalithe à sa périphérie. Monument le plus probable.

#### Historique

Monument découvert par [J. Blot]{.creator} en [1971]{.date}. En 2010, Manteca Martinez et ses compagnons ont baptisé ce monument « Tumulus ». La situation sur un terrain en pente, la dissymétrie qui en résulte n'est pas très en faveur de tumulus funéraires. Toutefois, sa structure pierreuse et ses dimensions peuvent en effet laisser planer un doute sur leur identité réelle.

</section>

<section class="monument">

### [Esterençuby]{.spatial} - [Erreta]{.coverage} sud n°5 - [Tertre d'habitat]{.type}

#### Localisation

Situé à 5m à l'Est Nord-Est de *Erreta sud n°4 - Tumulus*.

#### Description

Tertre de terre et de pierres de 7m de diamètre environ, érigé sur un terrain en pente vers le Sud ; hauteur difficile à apprécier (0,30m environ).

#### Historique

Monument découvert par [J. Blot]{.creator} en [1971]{.date}.

</section>

<section class="monument">

### [Esterençuby]{.spatial} - [Erreta]{.coverage} sud n°6 - [Tumulus]{.type}

#### Localisation

Il est situé à environ une trentaine de mètres au Nord Nord-Est de *Erreta sud n°3 - Tumulus*.

#### Description

Tumulus de terre et de pierres de 6m à 7m de diamètre ; érigé sur un sol incliné vers l'Ouest, sa hauteur est difficile à apprécier (0,40m ?).

#### Historique

Monument découvert par [J. Blot]{.creator} en [1971]{.date}. En 2010, Manteca Martinez et ses compagnons ont baptisé ce monument « Tumulus ». La situation sur un terrain en pente, la dissymétrie qui en résulte n'est pas très en faveur de tumulus funéraires. Toutefois, sa structure pierreuse et ses dimensions peuvent en effet laisser planer un doute sur leur identité réelle.

</section>

<section class="monument">

### [Esterençuby]{.spatial} - [Errozaté]{.coverage} 6 - [Tumulus]{.type}

#### Localisation

Carte 1346 Est Saint-Jean-Pied-de-Port.

Altitude : 1300m.

Il est situé à 80m environ au Sud Sud-Ouest du groupe de cromlechs déjà décrits [@blotNouveauxVestigesMegalithiques1972c, p. 78], et qui ont fait l'objet d'une campagne de fouille de sauvetage en 1976.

#### Description

Volumineux tumulus pierreux de 2,50 à 3m de haut, constitué d'un amas de blocs rocheux posé sur une assise rocheuse naturelle. Surmontant l'ensemble, a été disposé au sommet un bloc plus important que les autres, mesurant 2,20m de large et 1,30m de haut. Ce monument est longé à son flanc Ouest par une antique piste pastorale. Sa signification nous échappe (de même que son époque approximative de construction), monument funéraire ? commémoratif ? borne repère ?

#### Historique

Monument découvert en [mars 1974]{.date}.

</section>

<section class="monument">

### [Esterençuby]{.spatial} - [Etchondo]{.coverage} - [Tertres d'habitat]{.type}

#### Localisation

Altitude : 600m.

-   *Le n°1* est visible à environ 7m au Nord-Ouest de la route. ; circulaire, de terre, il mesure environ 4m de diamètre et 0,30m de haut.

-   *Le n°2* est à 7m au Sud-Est, au bord de la route, qui l'a détérioré.

</section>

<section class="monument">

### [Esterençuby]{.spatial} - [Etxeparea]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 841m.

Sur un replat situé à l'Est du mont Sohandi, dans la boucle en U que forme une piste forestière et à une vingtaine de mètres au Sud de sa branche Nord.

#### Description

Tumulus de terre érigé sur terrain plat, de forme très légèrement ovalaire, mesurant 12m x 10m et 0,30m de haut, présentant une faible dépression centrale. On peut aussi émettre l'hypothèse qu'il s'agisse d'un tertre d'habitat, mais alors très dégradé...

#### Historique

Tumulus découvert par [Meyrat F.]{.creator} en [octobre 2018]{.date}.

</section>

<section class="monument">

### [Esterençuby]{.spatial} - [Etxeparea]{.coverage} 1 - [Tertre d'habitat]{.type}

#### Localisation

Il est situé à 2m au Nord-Est de *Etxeparréa - Tumulus*, dont il est séparé par le passage d'une piste pastorale herbeuse ; il est adossé à un pointement rocheux naturel.

#### Description

Tertre herbeux mesurant 10m x 8m et environ 1m de haut.

#### Historique

Tertre découvert par [Meyrat F.]{.creator} en [octobre 2018]{.date}.

</section>

<section class="monument">

### [Esterençuby]{.spatial} - [Etxeparea]{.coverage} 2 - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 829m.

#### Description

Tertre terreux, érigé sur terrain en pente vers le Nord-Ouest, de forme ovalaire, mesurant 10m x 6m et 0,40m de haut.

#### Historique

Tertre découvert par [Meyrat F.]{.creator} en [octobre 2018]{.date}.

</section>

<section class="monument">

### [Esterençuby]{.spatial} - [Harpéa]{.coverage} - [Tertres d'habitat]{.type}

#### Localisation

Altitude : 890m.

Les 5 tertres sont situés très près de la frontière, au Nord de la BF 218, et au Sud-Ouest de la route qui conduit au cayolar, et se continue par la piste qui conduit vers la grotte Harpéa.

#### Description

De forme ovale, ils sont érigés sur un terrain en pente vers le Nord-Est, d'où leur classique forme dissymètrique.

-   *Tertre d'habitat 1* : se trouve à une bonne trentaine de mètres à vol d'oiseau, à l'Ouest du cayolar, plus en altitude que lui, et derrière une butte naturelle le séparant de ce cayolar. Tertre bien visible, de 12m x 11m et 1m de haut, ovale à grand axe Nord-Sud.

-   *Tertre d'habitat 2* : à 20m au Nord-Est du précédent, situé sur la butte naturelle précitée ; mesure 12m x 9m et 0,80m de haut. Il est très net, bien que l'on puisse (un peu) le confondre avec la butte naturelle.

-   *Tertre d'habitat 3* : quasi tangent à l'Est Nord-Est du précédent. Il mesure 11m x 8m et 0,40m de haut.

-   *Tertre d'habitat 4* : situé beaucoup plus en hauteur, à une quarantaine de mètres à l'Ouest du *tertre n° 1*. Il mesure 12m x 8m et 0,80m de haut et présente des travaux de captage à sa partie supérieure. (Altitude : 895m).

-   *Tertre d'habitat 5* : est à 30m au Sud-Est du précédent. Il mesure 12m x 8m et 0,80m de haut.

#### Historique

Tertres trouvés par [Blot J.]{.creator} en [1975]{.date}.

</section>

<section class="monument">

### [Esterençuby]{.spatial} - [Heguieder]{.coverage} 8 - [Cromlech]{.type}

#### Localisation

Altitude : 850m.

Situé à l'Est de la route ; on note surtout le gros abreuvoir en ciment qui a été construit en son milieu. Quand nous l'avions repéré en 1985, ce cercle était très net, ses pierres bien visibles.

#### Description

Le monument a été fort remanié, mais on peut encore distinguer une quinzaine de pierres délimitant un cercle de 4,50m de diamètre.

#### Historique

Monument découvert par [Blot J.]{.creator} en [octobre 1985]{.date}. Signalons qu'après cette date des postes de tir à la palombe en parpaings cimentés ont été installés sur toute la ligne de crête d'Heguieder, détruisant par exemple le *Cromlech Heguieder 4*.

</section>

<section class="monument">

### [Esterençuby]{.spatial} - [Heguieder]{.coverage} 9 - [Cromlech]{.type}

#### Localisation

Altitude : 840m.

Situé à 4m à l'Est de la route.

#### Description

Tumulus de terre et de pierres, mesurant 4,50m de diamètre et 0,50m de haut.

#### Historique

Monument découvert par [Blot J.]{.creator} en [octobre 1985]{.date}.

</section>

<section class="monument">

### [Esterençuby]{.spatial} - [Heguieder]{.coverage} 10 - [Tumulus]{.type}

#### Localisation

Altitude : 830m.

Situé à 4m à l'Est de la route, et à une dizaine de mètres au Sud d'un poste de tir.

#### Description

Il a probablement été remanié en périphérie par le passage de la route. Tumulus de terre et de pierres, mesurant environ 6m de diamètre et 0,40m de haut.

#### Historique

Monument découvert par [Blot J.]{.creator} en [octobre 1985]{.date}.

</section>

<section class="monument">

### [Esterençuby]{.spatial} - [Heguieder]{.coverage} 11 - [Tumulus]{.type}

#### Localisation

Altitude : 830m.

Il se trouve à 8m à l'Ouest de la route, en symétrique au *Heguieder 10 - Tumulus*.

#### Description

Tumulus très érodé, constitué essentiellement de terre recouverte de gazon, mesurant 6m de diamètre et 0,20m de haut.

#### Historique

Monument découvert par [Blot J.]{.creator} en [octobre 1985]{.date}.

</section>

<section class="monument">

### [Esterençuby]{.spatial} - [Larregaïchto]{.coverage} Nord - [Cromlech]{.type} ([?]{.douteux})

#### Localisation

Altitude : 790m.

Il se trouve vers l'extrémité du plateau qui s'étend au Nord de la route.

#### Description

Cercle de 2,80m de diamètre délimité par une quinzaine de pierres au ras du sol ; y aurait-il d'autres structures voisines ? difficile à dire.

#### Historique

Monument découvert par [J. Blot]{.creator} en [octobre 2010]{.date}.

</section>

<section class="monument">

### [Esterençuby]{.spatial} - [Larregaïchto]{.coverage} Nord - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 800m.

#### Description

Il mesure 5m à 6m de diamètre pour un très faible relief, mais est néanmoins parfaitement visible.

#### Historique

Monument découvert par [J. Blot]{.creator} en [octobre 2010]{.date}.

</section>

<section class="monument">

### [Esterençuby]{.spatial} - [Larregaïchto]{.coverage} Nord 1 - [Tertre d'Habitat]{.type}

#### Localisation

Altitude : 730m.

#### Description :

Il s'agit probablement des restes fusionnés de 3 ou 4 tertres d'habitat, formant une structure allongée suivant un axe Nord-Ouest Sud-Est.

#### Historique

Monument découvert par [J. Blot]{.creator} en [octobre 2010]{.date}.

À une quinzaine de mètres à l'Ouest Sud-Ouest de ce tertre d'habitat, il semblerait que l'on puisse distinguer, au pied d'une aubépine, un petit cercle (?) de 1,60m de diamètre formé d'une dizaine de pierres au ras du sol.

</section>

<section class="monument">

### [Esterençuby]{.spatial} - [Larregaïchto]{.coverage} Sud 2 - [Tertres d'habitat]{.type}

#### Localisation

Altitude : 730m.

#### Description

Tertre de terre, circulaire, de 7m de diamètre et 0,30m de haut.

Un deuxième tertre est visible à 7m au Sud-Ouest de celui-ci et de mêmes dimensions.

#### Historique

Monument découvert par [J. Blot]{.creator} en [octobre 2010]{.date}.

</section>

<section class="monument">

### [Esterençuby]{.spatial} - [Larregaïchto]{.coverage} Sud - [Tumulus pierreux]{.type} ([?]{.douteux})

#### Localisation

Altitude : 740m.

Très peu visible au milieu de ce plateau... et des fougères.

#### Description

Tumulus ovale, à grand axe Nord-Sud, formé de petits blocs pierreux et mesurant 3,50m de long, 2,40m de large et 0,40m de haut environ.

#### Historique

Monument découvert par [J. Blot]{.creator} en [octobre 2010]{.date}.

</section>

<section class="monument">

### [Esterençuby]{.spatial} - [Mondaizeko Malda]{.coverage} - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 788m.

Ce tertre se trouve accolé à un petit cayolar relié par une piste carrossable, à la route de crête qui se dirige vers Heguieder, route qui qui le domine à l'Est.

#### Description

Ce tertre de terre, de forme oblongue, mesurant 16m de long, 13m de large et environ 1m de haut, est allongé selon un axe Nord-Ouest Sud-Est. Il présente, comme souvent, une légère dépression centrale, qui aboutit, au Nord, à un début d'éboulement, ceci paraissant dû aux travaux de construction du cayolar voisin, dont le nivellement du sol a juste côtoyé le tertre dans sa partie Nord. Il nous paraît très peu probable que ce tertre soit le résultat d'un dépôt de déblais à cet endroit.

#### Historique

Tertre découvert par [Meyrat F.]{.creator} en [décembre 2018]{.date}.

</section>

<section class="monument">

### [Esterençuby]{.spatial} - [Mondaizeko Malda]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 742m.

Ce tumulus, bien visible, est au centre d'un plateau qui s'étend en contre-bas et à l'Ouest de *Mondaizeko Malda - Tertre d'habitat*. Il est à 8m au Nord-Ouest d'un abreuvoir en ciment récemment construit.

#### Description

Tumulus circulaire, terreux, en forme de galette aplatie, mesurant 9m de diamètre et 0,20m de haut environ. Il n'y a aucune pierre visible.

#### Historique

Tumulus découvert par [Meyrat F.]{.creator} en [décembre 2018]{.date}.

</section>

<section class="monument">

### [Esterençuby]{.spatial} - [Pagoberry]{.coverage} - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 980m.

On le voit à une cinquantaine de mètres au Sud-Ouest de la route qui va d'Esterençuby à Urkulu, au moment où elle détache une bretelle menant, au Sud-Ouest, vers Harpea ou à Orbaitzeta.

Ce tertre domine une grande doline qui s'étend au Sud-Ouest et une autre, plus petite, au Sud-Est.

#### Description

Très beau tertre ovalaire, à grand axe Sud-Est Nord-Ouest, mesurant 16m de long, 12m de large et au moins 1,50m de haut.

#### Historique

Tertre découvert par [C. Blot]{.creator} en [janvier 2011]{.date}.

</section>

<section class="monument">

### [Esterençuby]{.spatial} - [Salvate]{.coverage} - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 980m.

Situé à 5m au Nord-Ouest de la route qui mène à Esterençuby, surplombant une importante doline.

#### Description

Tertre à grand axe orienté Nord-Est Sud-Ouest, mesurant une dizaine de mètres de long, mais dont la largeur est difficile à apprécier du fait du recouvrement partiel de son sommet par des colluvions venant du Sud-Ouest (côté route).

#### Historique

Tertre découvert par [J. Blot]{.creator} en [janvier 2011]{.date}.

</section>

<section class="monument">

### [Esterençuby]{.spatial} - [Sohandi]{.coverage} - [Tertres d'habitat]{.type}

#### Localisation

Altitude : 893m.

Ensemble de 3 tertres situé à une centaine de mètres au Sud du groupe précédent.

#### Description

Ces 3 éléments sont disposés en ligne sur une rupture de pente, et présentent les mêmes dimensions, soit 8m x 8m et 0,30m de haut. Comme pour le groupe précédent, on note immédiatement à l'Ouest les traces du décaissement ayant fourni le matériau nécessaire à leur édification, sous forme d'une surface en demi-cercle de 20m de long et 9m de large.

#### Historique

Tertres trouvés par [Meyrat F.]{.creator} en [octobre 2018]{.date}.

</section>

<section class="monument">

### [Esterençuby]{.spatial} - [Sohandi]{.coverage} - [Tertres d'habitat]{.type}

#### Localisation

Cet ensemble de 3 tertres d'habitat est édifié au flanc Est de la colline Sohandi, à peu de distance de son sommet. On note une importante excavation au flanc de cette colline étendue sur près de 45m de long et 17m de large au maximum, orientée Nord-Sud, due au prélèvement de terre nécessaire pour édifier ces 3 tertres très proches les uns des autres.

#### Description

-   *Tertre d'habitat 1* : Altitude : 886m. Volumineux tertre de terre, quasiment circulaire, mesurant 13m de diamètre et 3m de haut environ, à sommet aplati.

-   *Tertre d'habitat 2* : presque tangent au précédent, de forme ovalaire (14m x 12m), mais de hauteur identique, il présente une légère dépression à son sommet aplani.

-   *Tertre d'habitat 3* : Situé à l'extrémité Sud de l'excavation, il est tangent au précédent, mais de dimensions plus importantes (15m x 6m), présente une forme ovalaire, à sommet plus ou moins pointu.

#### Historique

Ces trois tertres ont été trouvés par [Blot J.]{.creator} en [septembre 1980]{.date}.

</section>

<section class="monument">

### [Esterençuby]{.spatial} - [Sohandi]{.coverage} 7 - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 895m.

Ce tertre se trouve légèrement au-dessus de l'ensemble des 3 tertres *Sohandi - Tertres d'habitat*, sur un terrain en pente.

#### Description

Tertre terreux de forme circulaire, il mesure 4m de diamètre et 0,50m de haut.

#### Historique

Tertre découvert par [C. Blot]{.creator} en [novembre 2018]{.date}.

</section>

<section class="monument">

### [Esterençuby]{.spatial} - [Ustarazu]{.coverage} - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 930m.

Il est situé à une dizaine de mètres à au Sud-Ouest de la route qui descend vers Esterençuby, au milieu du *S* qu'elle décrit à cet endroit, et il surplombe la dépression d'une ancienne doline.

#### Description

Vaste tertre ovale en terre, à grand axe Est Sud-Est, de 16m de long, 8m de large et 0,50m de haut, typiquement « bas-navarrais » (les tertres souletins sont arrondis).

À noter, à quelques 50m au Nord-Ouest., une dalle de [grés]{.subject} isolée, plantée(?) dans le sol de 1,15m de long, distante de 2,20m d'une seconde au Nord-Ouest ; il y aurait un « tumulus » de 5m à 6m de diamètre. Dolmen ? peu probable à notre avis.

#### Historique

Tertre trouvé par [C. Blot]{.creator} en [janvier 2011]{.date}.

</section>

# Etxebar

<section class="monument">

### [Etxebar]{.spatial} - [Murutxe]{.coverage} Est - [Tertres d'habitat]{.type}

#### Localisation

Altitude : 980m.

Les 10 tertres d'habitat sont au Nord de la route qui redescend vers Licq, avant la piste qui monte au pic Salhagagne, sur le flanc Est de la colline, en bordure d'un petit ravin boisé.

#### Description

On peut compter une dizaine de tertres, si ce n'est plus (abondante végétation), espacés les uns des autres de 1,50m à 3m ou 4m environ - Ils sont circulaires, mesurent 5m à 6m de diamètre en moyenne et 0,60m de haut.

#### Historique

Tertres découverts par [Blot J.]{.creator} en [octobre 2012]{.date}.

</section>

<section class="monument">

### [Etxebar]{.spatial} - [Murutxe Ouest]{.coverage} - [Tertres d'habitat]{.type}

Trois TH se trouvent au Nord Nord-Ouest de la route qui redescend vers Licq, avant la piste qui monte au pic Salhagagne, sur le flanc Est de la colline, en bordure d'un petit ravin boisé.

-   *Tertre d'habitat 1* : Il est à 80m au Nord Nord-Ouest de la route. Altitude : 984m - Tertre ovale de 10m x 7m et 0,20m de haut. Terrain en légère pente vers le Sud.

-   *Tertre d'habitat 2* : Situé à 10m au Nord de *TH 1* - Mesure 9m x 7m et 0,15m de haut. ; terrain en légère pente vers leSsud.

-   *Tertre d'habitat 3* : Situé environ à 120m au Nord de *TH 2*, au sommet de la cote 992, sur le replat de la crête. Tertre de 8m x 6m et 0,25m de haut. Très légère dépression centrale.

#### Historique

Ces trois tertres ont été trouvés par [F. Meyrat]{.creator} en [novembre 2014]{.date}.

</section>

# Gamarthe

<section class="monument">

### [Gamarthe]{.spatial} - [Eheta]{.coverage} Nord 1 - [Structure rectangulaire]{.type} ([?]{.douteux})

#### Localisation

Altitude : 531m.

Situé à l'extrémité Nord d'un replat qui s'étend lui-même au Nord de la colline d'Eheta, à 3m environ au Sud-Est d'un important filon rocheux.

#### Description

Structure rectangulaire, constituée de petits blocs de [calcaire]{.subject} blanc, plus ou moins de la taille d'un pavé, posés ou peu enfouis dans le sol, disposés de façon assez lâche sur 3 faces et délimitant ainsi un rectangle ouvert au Sud. Les « murs » Est et Ouest mesurent 2,80m de long et plus ou moins 0,30m de large ; le mur Nord mesure 1,90m de long et 0,50m de large en moyenne. Il s'agit fort probablement des vestiges très discrets d'un habitat sommaire érigé à cet endroit... mais quand ? ...

#### Historique

Découvert par [Meyrat F.]{.creator} en [avril 2016]{.date}.

</section>

<section class="monument">

### [Gamarthe]{.spatial} - [Eheta]{.coverage} Nord 2 - [Structure circulaire]{.type} ([?]{.douteux})

#### Localisation

Tangente au Sud-Ouest du précédent, à l'extrémité Sud du « mur » Ouest de la structure rectangulaire.

#### Description

Petite structure circulaire constituée d'une vingtaine de petits blocs de [calcaire]{.subject} blanc plus ou moins enfoncés dans le sol ; « monument » très douteux.

#### Historique

Découvert par [Meyrat F.]{.creator} en [avril 2016]{.date}.

</section>

<section class="monument">

### [Gamarthe]{.spatial} - [Eheta]{.coverage} Nord 3 - [Structure tumulaire]{.type} ([?]{.douteux})

#### Localisation

Il est situé à 2m au Nord de la structure rectangulaire.

#### Description

Tumulus pierreux circulaire de 1,40m de diamètre et 0,20m de haut. ; nombreux petits blocs de [calcaire]{.subject} blanc en surface et en périphérie, évoquant un petit tumulus cromlech... monument douteux.

#### Historique

Découvert par [Meyrat F.]{.creator} en [avril 2016]{.date}.

</section>

<section class="monument">

### [Gamarthe]{.spatial} - [Orgamendy]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 494m.

Situé dans un col au Sud de la colline Orgamendy, et du chemin qui y monte, sur une crête de terrain séparant deux versants vers l'Est et vers l'Ouest.

#### Description

Tumulus terreux, de 9m à 10m de diamètre, (difficile à apprécier, car de faible hauteur : 0,30m environ) et présentant une légère dépression sur sa périphérie Ouest ; pas de pierres visibles.

#### Historique

Tumulus découvert par [Meyrat F.]{.creator} en [avril 2016]{.date}.

</section>

# Garindein

<section class="monument">

### [Garindein]{.spatial} - [Tibarenne]{.coverage} Ouest 3 - [Tumulus]{.type}

#### Localisation

Altitude : 474m.

Tumulus situé tout en bordure et au Sud de la piste pastorale de crête.

#### Description

Petit tumulus circulaire de 3,80m de diamètre et environ 0,40m de haut. On distingue environ 7 blocs de [calcaire]{.subject} blanc de dimensions assez importantes (0,42m x 0,27m x 0,8m), accumulés au centre, mais qui pourraient y avoir été rapportés ultérieurement à l'édification du tumulus originel.

À propos des autres tumulus de Tibarenne Ouest (T1 et T2), nous voudrions ajouter ces récentes constatations. Depuis l'époque de notre première prospection [@blotTumuluscromlechUgatzePic1974] et de notre publication de ces monuments dans [@blotNouveauxVestigesProtohistoriques1975, p.123], ces lieux ont été transformés en prairies artificielles occasionnant d'importants dégâts au niveau des monuments. C'est ainsi que le tumulus :

-   *Tibarenne Ouest T1* mesurant 13m de diamètre et 0,80m de haut a été complètement arasé ; il n'en reste rien.

-   Le tumulus *Tibarenne Ouest T2* :

    Altitude : 459m ;

    A lui aussi été en grande partie arasé ; toutefois on distingue encore un léger relief de 16m à 18m de diamètre et 0,20m à 0,30m de haut au centre duquel on peut voir les restes de la chambre funéraire. Celle-ci devait mesurer 1,15m de long et 0,55m de large, orientée Nord-Sud, et on en voit encore deux dalles jointives délimitant sa paroi Ouest (la plus grande mesurant 0,58m de long et 0,11m d'épaisseur) et une autre dalle au Sud (0,14m x 0,21m).

#### Historique

Tumulus découvert par [J. Blot]{.creator} en [avril 2016]{.date}.

</section>

# Gotein-Libarrenx

<section class="monument">

### [Gotein-Libarrenx]{.spatial} - [Ahantsiga]{.coverage} - [Tertres d'habitat]{.type}

#### Localisation

Altitude : 570m.

#### Description

Nous avions succinctement décrit ces 4 tertres d'habitat dans [@blotSouleSesVestiges1979, p.26]. Mais, récemment ces tertres ont étés plus ou moins rasés pour la création de prairies artificielles. Notre ami F. Meyrat, revenu sur les lieux, a pu cependant identifier et photographier quelques reliefs qui sembleraient correspondre aux restes de ces monuments.

En 1978, on pouvait voir, quasiment au sommet de la colline de ce nom, quatre tertres : 3 étaient situés au Sud-Ouest d'une piste pastorale orientée Nord-Ouest Sud-Est :

-   Le premier : le plus au Nord (n°1), mesurait 10m de diamètre, l et 0,60m de haut.

-   Le second, (n°2), à 2m à l'Est du précédent, mesurait 16m de long, 10m de large et 1m de haut.

-   Le troisième (n°3), à 28m à l'Est Sud-Est du précédent, mesurait 11m de diamètre et 0,70m de haut.

-   Le quatrième (n°4) était situé à 14m au Nord-Est du précédent, de l'autre côté de la piste pastorale.

Enfin F. Meyrat a peut-être identifié un cinquième monument, à quelques mètres au Nord du n°4.

#### Historique

Tertres découverts en [1978]{.date} par [J. Blot]{.creator}.

</section>

# Hasparren

<section class="monument">

### [Hasparren]{.spatial} - [Haitzeder]{.coverage} 1 - [Tumulus]{.type}

#### Localisation

Carte 1344 Ouest Hasparren.

Altitude : 90m.

*Haitzeder 1*, *Haitzeder 2* et *Haitzeder 3* se trouvent dans les landes communales d'Hasparren.

Le n°1 est situé le long d'une piste pastorale qui le longe sur son flanc Nord.

#### Description

Tumulus terreux de 16m de diamètre et 1,70m de haut ; une piste passe sur son flanc Nord.

#### Historique

Monument découvert en [mai 1972]{.date}.

</section>

<section class="monument">

### [Hasparren]{.spatial} - [Haitzeder]{.coverage} 2 (Enseigne) - [Tumulus]{.type}

#### Localisation

Carte 1344 Ouest Hasparren.

Altitude : 150m.

Il est à 600m au Nord-Est de *Haitzeder 1 - Tumulus*, à droite de la piste pastorale, quand on se dirige vers le Nord.

#### Description

Tumulus de terre de 15m de diamètre et 1m de haut.

#### Historique

Monument découvert en [mai 1972]{.date}. Ce monument a été rasé par des labours ultérieurs.

</section>

<section class="monument">

### [Hasparren]{.spatial} - [Haitzeder]{.coverage} 3 - [Tumulus]{.type}

#### Localisation

Carte 1344 Ouest Hasparren.

Altitude : 154m.

Il est situé à 900m au Nord-Ouest de *Haitzeder 2 (Enseigne) - Tumulus* et à 4m au Nord du chemin se rendant à Pilota-Plaza et venant de Ensenia-Zaharrea.

#### Description

Tumulus de terre de 16m de diamètre et 0,80m de haut.

#### Historique

Monument découvert en [mai 1972]{.date}. Monument rasé par des labours ultérieurs.

</section>

<section class="monument">

### [Hasparren]{.spatial} - [Pelloeneko Oyana]{.coverage} 1 - [Tumulus]{.type}

#### Localisation

Carte 1344 Ouest Hasparren.

Altitude : 126m.

Il est situé à 30m au Sud-Ouest de la maison Irunagekoborda.

#### Description

Tumulus terreux de 19m de diamètre et 1,50m de haut, qui présente à son flanc Est une importante dépression qui pourrait bien résulter d'une fouille ancienne.

#### Historique

Monument découvert en [mai 1972]{.date}.

</section>

<section class="monument">

### [Hasparren]{.spatial} - [Pelloeneko Oyana]{.coverage} 2 - [Tumulus]{.type}

#### Localisation

Carte 1344 Ouest Hasparren.

Altitude : 159m.

Il est à 1400m au Sud-Est de *Pelloeneko Oyana 1 - Tumulus*, et à proximité immédiate d'un carrefour de deux pistes pastorales.

#### Description

Tumulus terreux de 25m de diamètre et 1,20m de haut. Une piste passe sur son sommet où apparaissent quelques pierres sans ordre apparent.

#### Historique

Monument découvert en [mai 1972]{.date}.

</section>

<section class="monument">

### [Hasparren]{.spatial} - [Pitxao Borda]{.coverage} - [Dolmen]{.type}

#### Localisation

Altitude : 362m.

Il est situé au point le plus élevé d'un vaste plateau situé au flanc Nord de l'Ursuya, à environ 800m environ, à vol d'oiseau, au Sud du chemin qui rejoint la Route de Napoléon, à 200m environ au Nord-Est d'un captage de source et à 150m au Sud-Ouest d'une borde neuve.

#### Description

Une abondante végétation cachait un ensemble de 5 blocs de [grés]{.subject} local, qui ne paraissent pas être « en place », mais avoir donc été amenés là volontairement ; on notera par ailleurs qu'il n'y a aucun autre élément rocheux dans les environs.

L'important nettoyage effectué par F. Meyrat a permis de mettre en évidence que deux dalles sont solidement enfoncées dans le sol, les dalles *A* et *B*, les autres blocs ne semblant que posés.

-   *La dalle A* mesure 0,60m de long, 0,48m de haut et 0,25m d'épaisseur et 0,48m de haut.

-   *La dalle B* mesure 1,20m de long, 0,16m d'épaisseur et 0,67m de haut.

-   *La dalle C*, de forme triangulaire à base arrondie et à sommet Sud-Est, est couchée au sol et touche les deux précédentes ; elle mesure 1,40m de long, 0,90m de large dans sa partie moyenne, et présente, semble-t-il des traces d'épannelage dans sa partie en pointe au Sud-Est.

-   *Le bloc D*, repose en partie sur le bloc *E*, en forme de parallélépipède allongé, mesure 1,50m de long, 0,50m de large à sa partie médiane et 0,46m d'épaisseur en moyenne.

-   *Le bloc E*, en forme de parallélépipède rectangle allongé, couché au sol, mesure, en moyenne, 1,60m de long, 0,42m d'épaisseur et 0,40m de largeur.

S'agit-il d'un dolmen ?

Si on ne note pas la présence de tumulus, ces dalles et blocs de pierre sont parfaitement isolés, il n'y a aucun élément pierreux dans le voisinage et cela ne ressemble en rien à un « tas d'épierrage ». Par contre, la présence de deux dalles plantées et d'une autre (la *C*) présentant des traces de retouche, nous font penser à une possible (??) [chambre funéraire]{.subject} - certes fort endommagée - à grand axe Est Nord-Est Ouest Sud-Ouest.

#### Historique

Monument signalé par [P. Badiola]{.creator} en [Janvier 2016]{.date}.

</section>

<section class="monument">

### [Hasparren]{.spatial} - [Ursubehere]{.coverage} 1 - [Monolithe]{.type} ([?]{.douteux})

#### Localisation

Altitude : 463m.

Située *presque* au point culminant d'un petit replat qui fait suite, au Nord, à un autre replat plus haut situé, dénommée Ursubehere, lui-même au flanc Nord Nord-Ouest du mont Ursuya.

#### Description

On note un gros bloc rocheux de forme générale pyramidale, piriforme. Il mesure 3,30m de haut, et presque autant à sa base. Ce bloc de grès ne présente pas de traces d'épannelage visible ; il est entouré de 6 autres blocs de moindre dimensions, non contigus, qui ne lui servent pas de calage. Ce bloc de [grés]{.subject} présente de nombreuse fissures, dont certaines ont très probablement été dans le passé (gel) à l'origine du détachement de certains des blocs des environs immédiats, dont un au Nord et un autre plus volumineux au Nord-Est.

Il est difficile de voir ici un monolithe au sens anthropique de la définition, mais il n'est pas exclu que ce rocher ait pu servir de repère naturel.

#### Historique

Pierre signalée par [Badiola P.]{.creator} en [mai 2016]{.date}.

</section>

<section class="monument">

### [Hasparren]{.spatial} - [Ursubehere]{.coverage} 2 - [Monolithe]{.type} ([?]{.douteux})

#### Localisation

Altitude :457m.

#### Description

Une autre dalle de [grés]{.subject}, couchée au sol selon un axe Nord-Est Sud-Ouest, de forme triangulaire, mesurant 3m de long, 1,80m dans sa plus grande largeur, et 0,80m d'épaisseur ; elle ne présente aucune trace d'épannelage.

Elle est toute proche d'un second bloc de grès de 3,20m de long, 2,90m de large et 1m de haut, sans aucune caractéristique particulière.

#### Historique

[Francis Meyrat]{.creator} nous l'a signalé en [mai 2016]{.date}.

</section>

# Haux

<section class="monument">

### [Haux]{.spatial} - [Apolotzegagna]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 703m.

Ce tumulus se trouve à l'extrémité Est de la crête sommitale du sommet du mont Apoltzegagna, avant la rupture de pente.

#### Description

Tumulus de terre et de pierres de 5m à 6m de diamètre et 0,40m à 0,50m de haut.

#### Historique

[Nous]{.creator} avions décrit ce tumulus en [1979]{.date} mais sans en donner la localisation exacte, d'où la rectification actuelle.

</section>

<section class="monument">

### [Haux]{.spatial} - [Haux (Bois de)]{.coverage} - [Tumulus]{.type} ([?]{.douteux})

#### Localisation

Altitude : 1080m.

Situé sur un vaste promontoire herbeux dominant le col d'Ordabure ; il se trouve à l'extrémité Ouest de celui-ci, à la rupture de pente.

#### Description

Tumulus en forme de galette aplatie circulaire, mesurant 4,50m de diamètre et 0,15m à 0,20m de haut ; il est constitué de terre et des fragments de [calcaire]{.subject} concassé sont visibles en surface, en particulier dans la région centrale. Tumulus très douteux, une simple lentille de solifluxion étant aussi fort probable...

#### Historique

Tumulus découvert par [Meyrat F.]{.creator} en [mai 2017]{.date}.

</section>

<section class="monument">

### [Haux]{.spatial} - [Hilague]{.coverage} (col de) - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 1275m.

On peut le voir à environ 80m à l'Est Sud-Est du relais téléphonique érigé au débouché de la piste à ce col ; il domine cette piste, étant construit sur une petite hauteur à l'Est de celle-ci.

#### Description

Tertre classique, asymétrique, allongé selon un axe Est-Ouest mesurant 6m et l'axe Nord-Sud étant de 5m.

#### Historique

Tertre découvert par [J. Blot]{.creator} en [octobre 2016]{.date}.

</section>

<section class="monument">

### [Haux]{.spatial} - [Iguntze]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 1381m.

Situé au sommet d'Iguntze et en descendant le versant Est de cette montagne vers le col de Lacurde (en suivant la ligne de crête), on peut distinguer, sur un léger replat, un probable tumulus.

#### Description

Tumulus terreux en forme de galette circulaire très aplatie, de 8m de diamètre et 0,10m à 0,15m de haut environ ; la piste passe en son milieu. Pas d'éléments pierreux visibles.

#### Historique

Tumulus découvert par [F. Meyrat]{.creator} en [novembre 2016]{.date}.

</section>

<section class="monument">

### [Haux]{.spatial} - [Losco]{.coverage} (col de) - [Tumulus]{.type}

#### Localisation

Altitude : 1062m.

Ce tumulus se trouve au col de Losco, à une quinzaine de mètres à l'Ouest de la piste qu'il domine et qui amorce à cet endroit un virage vers le Nord.

#### Description

Tumulus peu visible, circulaire, terreux (quelques rares petites pierres sont visibles) ; il mesure 8m de diamètre, 0,20m environ de haut, et présente une légère dépression centrale.

#### Historique

Tumulus découvert par [J. Blot]{.creator} en [octobre 2016]{.date}.

</section>

# Helette

<section class="monument">

### [Helette]{.spatial} - [Erregelu]{.coverage} 1 - [Tumulus-cromlech]{.type}

#### Localisation

Altitude : 485m.

Sur un replat au flanc Nord du mont Baigoura, à 600m environ à l'Est de la route asphaltée qui monte aux antennes. Situé vers l'extrémité Est du plateau, à une vingtaine de mètres au Nord de la piste pastorale.

#### Description

Tumulus de terre d'environ 18m de diamètre et 0,40m de haut. Sa périphérie est délimitée par une dizaine de pierres plus ou moins visibles ; il semblerait même qu'on puisse deviner deux autres cercles concentriques, l'un de 10m de diamètre, l'autre de 6m.

#### Historique

Monument découvert par [I. Txintxuretta]{.creator} en [janvier 2015]{.date}.

</section>

<section class="monument">

### [Helette]{.spatial} - [Erregelu]{.coverage} 2 - [Cromlech]{.type}

#### Localisation

Altitude : 486m.

Sur un replat au flanc Nord du mont Baigoura, à 600m environ à l'Est de la route asphaltée qui monte aux antennes. Il est situé à une quinzaine de mètres au Nord-Ouest de *Erregelu 1 - Tumulus-cromlech*.

#### Description

Cercle de 4m de diamètre, délimité par une dizaine de pierres au ras du sol, mais bien visibles.

#### Historique

Monument découvert par [I. Txintxuretta]{.creator} en [janvier 2015]{.date}.

</section>

<section class="monument">

### [Helette]{.spatial} - [Erregelu]{.coverage} 3 - [Cromlech]{.type} ([?]{.douteux})

#### Localisation

Sur un replat au flanc Nord du mont Baigoura, à 600m environ à l'Est de la route asphaltée qui monte aux antennes. Monument que nous n'avons pas bien identifié, qui serait à 4m à l'Est Nord-Est de *Erregelu 2 - Cromlech*.

#### Description

Cercle de 4m délimité par 5 pierres.

#### Historique

Monument découvert par [I. Txintxuretta]{.creator} en [janvier 2015]{.date}.

</section>

<section class="monument">

### [Helette]{.spatial} - [Erregelu]{.coverage} 3 bis - [Cromlech]{.type} ([?]{.douteux})

#### Localisation

Sur un replat au flanc Nord du mont Baigoura, à 600m environ à l'Est de la route asphaltée qui monte aux antennes. Situé à 4m à l'Est Nord-Est de *Erregelu 3 - Cromlech (?)*.

#### Description

Monument tout aussi douteux que le précédent. Mesure 4m de diamètre et délimité par 8 pierres environ.

#### Historique

Monument découvert par [F. Meyrat]{.creator} en [février 2015]{.date}.

</section>

<section class="monument">

### [Helette]{.spatial} - [Erregelu]{.coverage} 4 - [Cromlech]{.type}

#### Localisation

Altitude : 485m.

Sur un replat au flanc Nord du mont Baigoura, à 600m environ à l'Est de la route asphaltée qui monte aux antennes.

#### Description

Cercle de 4m de diamètre, délimité par 6 pierres à la surface aplanie, bien visibles.

#### Historique

Monument découvert par [I. Txintxuretta]{.creator} en [janvier 2015]{.date}.

</section>

<section class="monument">

### [Helette]{.spatial} - [Erregelu]{.coverage} 5 - [Cromlech]{.type} ([?]{.douteux})

#### Localisation

Altitude : 490m.

Sur un replat au flanc Nord du mont Baigoura, à 600m environ à l'Est de la route asphaltée qui monte aux antennes. Situé à une quarantaine de mètres à l'Ouest Sud-Ouest de *Erregelu 4 - Cromlech*.

#### Description

Monument peu facile à voir, douteux ! Cercle de 9m de diamètre, délimité par 3 ou 4 pierres.

#### Historique

Monument découvert par [I. Txintxuretta]{.creator} en [janvier 2015]{.date}.

</section>

<section class="monument">

### [Helette]{.spatial} - [Erregelu]{.coverage} 6 - [Cromlech]{.type}

#### Localisation

Sur un replat au flanc Nord du mont Baigoura, à 600m environ à l'Est de la route asphaltée qui monte aux antennes. Situé à 5m à l'Ouest Nord-Ouest de *Erregelu 5 - Cromlech (?)*.

#### Description

Cercle mesurant 5m de diamètre et délimité par environ 7 pierres.

#### Historique

Monument découvert par [I. Txintxuretta]{.creator} en [janvier 2015]{.date}.

</section>

<section class="monument">

### [Helette]{.spatial} - [Erregelu]{.coverage} 7 - [Tumulus]{.type} ([?]{.douteux})

#### Localisation

Altitude : 488m.

Sur un replat au flanc Nord du mont Baigoura, à 600m environ à l'Est de la route asphaltée qui monte aux antennes. Il est à 35m à l'Ouest de *Erregelu 1 - Tumulus cromlech*.

#### Description

Tumulus (douteux ?) de terre de 10m de diamètre environ et 0,30m de haut, à la surface duquel apparaissent quelques pierres sans ordre apparent.

#### Historique

Monument trouvé par [F. Meyrat]{.creator} en [février 2015]{.date}.

</section>

<section class="monument">

### [Helette]{.spatial} - [Erregelu]{.coverage} 8 - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 498m.

Sur un replat au flanc Nord du mont Baigoura, à 600m environ à l'Est de la route asphaltée qui monte aux antennes. Il est situé sur un terrain en légère pente vers le Sud, à 18m à l'Est d'un petit ru, et à 12m au Nord Nord-Ouest de la piste pastorale.

#### Description

Tertre asymétrique mesurant 7m de long et environ 6m de large pour 0,30m de haut.

#### Historique

Monument trouvé par [F. Meyrat]{.creator} en [février 2015]{.date}.

</section>

<section class="monument">

### [Helette]{.spatial} - [Etxolalde]{.coverage} - [Monolithe]{.type}

#### Localisation

Altitude : 488m

Ce monolithe est situé au versant nord du mont Baigoura, sur un léger replat, à une cinquantaine de mètres au sud du sentier de découverte du Baigoura, et à une centaine de mètres au Nord-Ouest d'une petite borde dénommée Etxolalde. À noter enfin une tourbière et un point d'eau à environ 150m au Nord-Ouest du monolithe.

#### Description

Volumineux bloc de [grés]{.subject} blanc en forme de pain de sucre, couché sur le sol et orienté NE-SO, comme la plupart des grand monolithes d'Iparralde.

Il mesure 4,10m de long, 2,40m dans sa plus grande largeur, 1,42m de large avant le rétrécissement du sommet, et 0,90m à sa base. Son épaisseur est variable : O,50m au sommet ; côté Nord-Ouest : en son milieu elle atteint 0,90m d'épaisseur et 1,90m vers sa base. Le côté Sud-Ouest n'ayant pas été suffisamment dégagé en profondeur, nous n'avons pas d'estimations. À sa base, dans sa partie dégagée, l'épaisseur atteint 1,30m.

Son poids peut être estimé aux alentours de 16 tonnes (B. Auriol). Enfin ce bloc de [grés]{.subject} présente quelques traces d'épannelage à son sommet, régularisant la symétrie de celui-ci, la nature ayant spontanément fourni le reste de la forme souhaitée.

#### Historique

Ce monolithe, profondément enfoui et dans la végétation et dans le sol, a été fortuitement découvert par [Joanes Laco]{.creator} lors d'une partie de chasse en [octobre 2019]{.date}. À notre demande et pour confirmation et étude, le monument a été plus largement dégagé ensuite.

</section>

<section class="monument">

### [Helette]{.spatial} - [Garralda]{.coverage} 3 - [Tumulus]{.type}

#### Localisation

Carte 1345 Est Iholdy.

Altitude : 330m.

Nous avons décrit [@blotNouveauxVestigesMegalithiques1973a, p.195], un dolmen, un tumulus-cromlech (*n°1*) et un tumulus simple (*n°2*). Le tumulus n°3 est situé à 2m à l'Ouest du *n°2*.

#### Description

Tumulus en terre de 7m de diamètre et 0,40m de haut.

#### Historique

Monument découvert en [juin 1975]{.date}.

</section>

# Hosta

<section class="monument">

### [Hosta]{.spatial} - [Belchou]{.coverage} 1 - [Cromlech]{.type}

#### Localisation

Carte 1446 Ouest Ordiarp.

Altitude : 828m.

On le trouve en bordure Sud Sud-Est de la grande cuvette d'un haut plateau situé Sud Sud-Est au pied du mont Belchou.

#### Description

Cercle de 4,60m de diamètre formé de 14 blocs de [grès]{.subject} blanc, de la taille d'un à 2 ou même 3 pavés. Cinq pierres apparaissent au centre du cercle.

#### Historique

Monument découvert en [août 1978]{.date}.

</section>

<section class="monument">

### [Hosta]{.spatial} - [Belchou]{.coverage} 2 - [Tumulus]{.type}

#### Localisation

Il est à 80m au Nord-Ouest de *Belchou 1 - Cromlech*.

#### Description

Tumulus mixte de terre et de blocs de [grès]{.subject} blanc très abondant ; la périphérie est très nettement délimitée, sans qu'on puisse parler cependant de péristalithe.

À 10m du Nord-Est de la bordure de la cuvette, on remarque à une quarantaine de mètres au Nord-Est du n°2, une série de *12 tertres d'habitat*, allongés selon un axe Nord-Ouest Sud-Est, avec parfois à leur sommet des pierres disposées en rectangle ou en ovale.

#### Historique

Monument découvert en [août 1978]{.date}.

</section>

<section class="monument">

### [Hosta]{.spatial} - [Belchou]{.coverage} 3 - [Tumulus]{.type}

#### Localisation

À 150m au Nord Nord-Ouest de *Belchou 2 - Tumulus*.

#### Description

Il se présente sous la forme d'une jolie galette de pierres blanches, légèrement tumulaire.

#### Historique

Monument découvert en [août 1978]{.date}.

</section>

<section class="monument">

### [Hosta]{.spatial} - [Belchou]{.coverage} 4 - [Tumulus]{.type}

#### Localisation

Sur un replat à flanc de montagne, à 30m au Nord Nord-Ouest de *Belchou 3 - Tumulus*.

#### Description

Belle petite galette, légèrement surélevée, de pierres blanches, mesurant 3m de diamètre.

#### Historique

Monument découvert en [août 1978]{.date}.

</section>

<section class="monument">

### [Hosta]{.spatial} - [Belchou]{.coverage} 5 - [Tumulus]{.type}

#### Localisation

À 100m au Nord Nord-Est de *Belchou 4 - Tumulus*, à gauche de la piste qui monte de la plaine du Belchou, et quasi tangente à elle.

#### Description

Belle galette de 5m de diamètre et constituée de blocs de [grès]{.subject} de la taille d'un pavé.

#### Historique

Monument découvert en [août 1978]{.date}.

</section>

<section class="monument">

### [Hosta]{.spatial} - [Belchou]{.coverage} 6 - [Tumulus]{.type}

#### Localisation

À 40m au Nord Nord-Ouest de *Belchou 5 - Tumulus*, au-dessus de lui sur la pente herbeuse, à gauche de la piste qui monte de la plaine.

#### Description

Tumulus pierreux en forme de galette, de 4m de diamètre et de quelques centimètres de hauteur.

#### Historique

Monument découvert en [août 1978]{.date}.

</section>

<section class="monument">

### [Hosta]{.spatial} - [Belchou]{.coverage} 7 - [Cromlech]{.type}

#### Localisation

Il est situé à 100m à l'Est Nord-Est de *Belchou 1 - Cromlech*, en bordure du relèvement de la cuvette, comme *Belchou 8 - Cromlech* et *Belchou - Monolithe*.

#### Description

Cercle de pierres de 5m de diamètre, délimité par 22 pierres ; il est érigé sur un sol très légèrement incliné vers le Nord Nord-Ouest.

#### Historique

Monument découvert en [août 1978]{.date}.

</section>

<section class="monument">

### [Hosta]{.spatial} - [Belchou]{.coverage} 8 - [Cromlech]{.type}

#### Localisation

À 100m au Nord-Est de *Belchou 7 - Cromlech*, à 830m d'altitude.

#### Description

Petit cercle de 3,60m de diamètre, délimité par une quinzaine de pierres au ras du sol ; il présente un aspect très légèrement tumulaire.

#### Historique

Monument découvert en [août 1978]{.date}. *Belchou 9 - Cromlech*, situé à 150m plus à l'Est Nord-Est se trouve sur la commune de [Saint-Just-Ibarre]{.spatial}.

</section>

<section class="monument">

### [Hosta]{.spatial} - [Belchou]{.coverage} - [Monolithe]{.type}

#### Localisation

Altitude : 970m.

Il repose sur le sol, à droite, dans l'avant-dernier virage de la route qui mène à Lahondoko Olha.

#### Description

Gros bloc de [schiste]{.subject} gris local, de forme parallélépipédique, allongé selon un axe Est Nord-Est Ouest Sud-Ouest. L'extrémité Est est arrondie, le bord Nord, rectiligne, le bord Sud, un peu plus irrégulier ; on peut noter des traces d'épannelage au sommet et sur le bord Sud. Il mesure 2,77m de long, 1,37m à sa base Ouest et 0,70m au sommet ; son épaisseur varie de 0,50m à 0,63m.

#### Historique

Monolithe découvert par [F. Meyrat]{.creator} en [août 2011]{.date}.

</section>

<section class="monument">

### [Hosta]{.spatial} - [Belchou]{.coverage} 11 - [Tumulus]{.type}

#### Localisation

Altitude : 828m.

À la limite Sud-Ouest du haut plateau, au tiers de sa longueur.

#### Description

Modeste tumulus en forme de galette de faible relief, de 3,50m de diamètre recouverte de nombreux petits blocs de [calcaire]{.subject}.

#### Historique

Tumulus découvert par [Blot J.]{.creator} en [août 2011]{.date}.

</section>

<section class="monument">

### [Hosta]{.spatial} - [Belchou]{.coverage} 12 - [Cromlech]{.type}

#### Localisation

Altitude : 828m.

À quelques mètres à l'Est de *Belchou 11 - Tumulus*.

#### Description

Huit pierres délimitent un cercle de 4,40m de diamètre ; la partie Sud-Ouest en est moins fournie, et on en note 4 dans la région centrale.

#### Historique

Monument découvert par [Blot J.]{.creator} en [août 2011]{.date}.

</section>

<section class="monument">

### [Hosta]{.spatial} - [Belchou]{.coverage} 13 - [Cromlech]{.type}

#### Localisation

Altitude : 828m.

Situé à quelques mètres à l'Est de *Belchou 12 - Cromlech*, et sur la ligne intercommunale entre Hosta et Saint-Just-Ibarre.

#### Description

On ne voit qu'un demi-cercle (de 4,60m de diamètre) de 4 pierres bien visibles dans la moitié Nord. Au Sud, hors du cercle, un bloc de [calcaire]{.subject} de volume plus important attire le regard.

#### Historique

Monument découvert par [Blot J.]{.creator} en [août 2011]{.date}.

</section>

<section class="monument">

### [Hosta]{.spatial} - [Belchou]{.coverage} 15 - [Cromlech]{.type}

#### Localisation

Altitude : 830m.

Situé à quelques dizaines de mètres à l'Ouest Nord-Ouest du *groupe de TH* au Sud-Ouest du haut plateau.

#### Description

Petit cercle de 3m de diamètre bien délimité par 21 pierres très nettes.

#### Historique

Monument découvert par [F. Meyrat]{.creator} en [août 2011]{.date}.

</section>

<section class="monument">

### [Hosta]{.spatial} - [Belchou]{.coverage} 16 - [Cromlech]{.type}

#### Localisation

Altitude : 836m.

Il se trouve à 40m au Nord-Ouest du *tumulus-cromlech T2*.

#### Description

Petit cromlech surélevé de 3m de diamètre, bien délimité par ce qui pourrait être une double couronne de pierres concentriques.

#### Historique

Monument découvert par [Blot J.]{.creator} en [août 2011]{.date}.

</section>

<section class="monument">

### [Hosta]{.spatial} - [Belchou]{.coverage} 1 (groupe Sud-Ouest - tertres du haut) - [Tertres d'habitats]{.type}

#### Localisation

Altitude : 840m.

Situé à 40m à l'Ouest Nord-Ouest de *Belchou 16 - Cromlech* à l'extrémité Nord-Ouest du haut plateau, il domine l'ensemble de ce dernier où l'on distingue 2 grands groupes de tertres d'habitat (un au Sud-Ouest, 17 TH et un au Nord-Est, 25 TH)

#### Description

Tertre de terre ovale, asymétrique parce que construit sur terrain en pente. Il mesure 10m de long et 5m de large ; sa hauteur est de 1m au Nord-Ouest et de 2m au Sud-Est.

On peut distinguer les vestiges d'un autre tertre à 15m au Nord-Ouest, mesurant environ 9m de long.

#### Historique

Découvert par [Blot J.]{.creator} en [août 2011]{.date}.

</section>

<section class="monument">

### [Hosta]{.spatial} - [Othamunho]{.coverage} - [Dolmen]{.type}

#### Localisation

Carte 1446 Ouest Ordiarp.

Altitude : 710m.

#### Description

Il n'y a pas de tumulus visible. La [chambre funéraire]{.subject}, orientée Sud-Ouest Nord-Est, qui mesure 1,80m de long et 0,50m de large, apparaît délimitée seulement par ses 4 dalles : au Sud-Est, une grande dalle de 1,75m de long et 0,33m de haut ; au Nord-Ouest, deux dalles plus petites et au Nord-Est une seule dalle de 0,55m de long. Il n'y a pas de couvercle visible.

#### Historique

Dolmen découvert en [septembre 1970]{.date}.

</section>

<section class="monument">

### [Hosta]{.spatial} - [Nethé]{.coverage} Nord 1 - [Tumulus]{.type}

#### Localisation

Altitude : 555m.

#### Description

Petit tumulus pierreux légèrement oblong, à grand axe Nord-Sud, de 2,80m de diamètre environ et 0,30m de haut.

La pousse d'un arbre (aubépine) à sa périphérie Nord a bouleversé la disposition des pierres (petits blocs de [calcaire]{.subject} blanc), qui ne paraissent cependant pas avoir été disposés dans un ordre particulier ; il semblerait qu'on puisse cependant par endroits distinguer une ébauche de péristalithe...

#### Historique

Tumulus découvert en [1974]{.date} par [Blot J.]{.creator} Ce tumulus, ainsi que le suivant avait été simplement cité dans le [@blotNouveauxVestigesProtohistoriques1975, p.122]

</section>

<section class="monument">

### [Hosta]{.spatial} - [Nethé]{.coverage} Nord 2 - [Tumulus]{.type}

#### Localisation

Altitude : 555m.

Il est situé à 4m au Nord-Ouest de *Nethe Nord 1 - Tumulus*. Un abreuvoir récent a été construit à 15m au Sud de ce tumulus.

#### Description

Petit tumulus pierreux, légèrement oblong, à grand axe orienté Nord-Est Sud-Ouest, de 3,40m de diamètre environ et 0,40m de haut ; une aubépine a poussé en périphérie Sud-Ouest, perturbant la disposition des pierres (petits blocs de calcaire blanc). Il est difficile d'évoquer un péristhalite (?).

#### Historique

Découvert par [Blot J.]{.creator} en [1974]{.date} et simplement cité dans [@blotNouveauxVestigesProtohistoriques1975, p.122]

</section>

<section class="monument">

### [Hosta]{.spatial} - [Nethé]{.coverage} Nord 3 - [Tumulus]{.type}

Altitude : 763m.

Il est situé à 40m au Nord de la borne géodésique du sommet de Néthé.

#### Description

Petit tumulus pierreux, très net, de forme oblongue, à grand axe Nord-Est Sud-Ouest, de 2,80m de diamètre et 0,30m de haut. Les blocs de [calcaire]{.subject} blanc ne semblent avoir été disposés dans un ordre particulier.

#### Historique

Tumulus découvert par [Meyrat F.]{.creator} en [décembre 2015]{.date}.

</section>

<section class="monument">

### [Hosta]{.spatial} - [Nethé]{.coverage} Sud - groupe Nord

Sous la dénomination « Néthé Sud », nous avions simplement cité dans [@blotNouveauxVestigesProtohistoriques1975, p.122] un ensemble de 7 tumulus et 1 tertre d'habitat, découverts par nous en 1974, au niveau du col séparant le mont Néthé au Nord du mont Iramunho au Sud. Ces monuments ont été revisités par F. Meyrat, qui a découvert un nouvel ensemble de 4 tumulus plus un tertre d'habitat, que nous appellerons le *Groupe Sud*, les précédents, déjà découverts par [nous]{.creator} en [1974]{.date}, formant le *Groupe Nord* de l'ensemble dénommé *Nethé Sud*.

Cet ensemble de 8 [tumulus]{.type} et 1 [tertre d'habitat]{.type} est situé à environ 200m au Sud Sud-Est de la ferme *Bordaxarria*, dans une prairie ; les 8 tumulus se succèdent en bordure d'une haie.

-   *Tumulus 1* :

    Altitude : 545m.

    Il est situé à une vingtaine de mètres à l'Ouest de la haie. Tumulus mixte, de terre et de pierres, de forme légèrement ovale, mesurant 8m x 7m et 0,50m de haut.

-   *Tumulus 2* :

    Il est à 2m au Sud-Est du précédent, et à 20m à l'Ouest de la haie. Tumulus mixte de terre et de pierres, mesurant 7m de diamètre et 0,40m de haut.

-   *Tumulus 3* :

    Il est à 4m à l'Est Nord-Est du précédent, et à 8m seulement à l'Ouest de la haie. Tumulus mixte mesurant 8m de diamètre et 0,50m de haut.

-   *Tumulus 4* :

    Il est à 4m au Sud-Est du précédent et à 15m environ à l'Ouest de la haie. Tumulus mixte, de forme ovale mesurant 9m x 6m et 0,30m de haut.

-   *Tumulus 5* :

    Il est à 5m au du précédent et à 7 m à l'Ouest de la haie, tumulus circulaire mixte de 5m de diamètre et 0,40m de haut.

-   *Tumulus 6* :

    Tangent au Sud-Est au précédent, mixte, circulaire, mesure 6m de diamètre et 0,60m de haut.

-   *Tumulus 7* :

    Situé à 7m au Sud Sud-Est du précédent et à 8m à l'Ouest de la haie. Tumulus mixte de terre et pierres de forme ovale, mesurant 11m x 8m et 0,40m de haut.

-   *Tumulus 8* :

    Situé à 8m au Sud Sud-Est du précédent et à 8m à l'Ouest de la haie.

    Tumulus mixte de forme ovale, mesurant 11m x 8m et 0,50m de haut.

-   *Bordaxarria Nord - tertre d'habitat*

    Altitude : 547m.

    Il est situé à une cinquantaine de mètres à l'Ouest Sud-Ouest du précédent, en surélévation.

    Tertre asymétrique, mesurant 16m x 11m et 0,70m de haut.

</section>

<section class="monument">

### [Hosta]{.spatial} - [Nethé]{.coverage} Sud - groupe Sud

Sous la dénomination « Néthé Sud », nous avions simplement cité dans [@blotNouveauxVestigesProtohistoriques1975, p.122] un ensemble de 7 tumulus et 1 tertre d'habitat, découverts par nous en 1974, au niveau du col séparant le mont Néthé au Nord du mont Iramunho au Sud. Ces monuments ont été revisités par [F. Meyrat]{.creator}, qui a découvert un nouvel ensemble de 4 tumulus plus un tertre d'habitat, que nous appellerons le *Groupe Sud*, les précédents, déjà découverts par nous en 1974, formant le *Groupe Nord* de l'ensemble dénommé NETHE SUD.

Cet ensemble de 4 [tumulus]{.type} et 1 [tertre d'habitat]{.type}, se trouve au Sud-Est du précédent ensemble (groupe Nord), de l'autre côté d'une haie, dans un terrain en jachère à la végétation abondante.

-   *Tumulus 10*

    Altitude : 555m.

    Tumulus situé tout contre la clôture du terrain. Tumulus mixte, circulaire de 12m de diamètre et 0,40m de haut.

-   *Tumulus 11*

    Situé à 4m au Nord du précédent, tumulus mixte, circulaire de 12m de diamètre et 0,40m de haut.

-   *Tumulus 12*

    Situé à 10m au Nord-Ouest du précédent, tumulus mixte, circulaire de 12m de diamètre et 0,40m de haut.

-   *Tumulus 13*

    Situé à 6m au Nord-Est du précédent ; tumulus mixte, ovale, mesurant 12m x 10m et 0,50m de haut.

-   *Bordaxarria Sud - Tertre d'habitat*

    Tertre asymétrique situé à environ 200m à l'Est Sud-Est du *tumulus 10*.

    Altitude : 542m.

    Ce tertre mesure 13m x 11m et 0,90m de haut.
    
</section>

# Iholdy

<section class="monument">

### [Iholdy]{.spatial} - [Haraneko Ithurria]{.coverage} - [Dolmen]{.type}

#### Localisation

Altitude : 415m.

Situé à l'extrémité de la petite colline dont le sommet pointe à 453m, on le voit à 15m à l'Ouest du chemin de crête et tangent au Sud, à une clôture qui empiète en partie sur lui.

#### Description

Ce volumineux [tumulus]{.subject} circulaire de 8m de diamètre et 0,60m de haut est constitué de pierraille de la taille d'un pavé ou plus ; ces éléments sont éparpillés sans ordre apparent. On note une légère dépression centrale de 1,50m de diamètre, probablement trace d'une ancienne fouille clandestine et, sur le flanc Sud Sud-Est du tumulus, une dalle inclinée mesurant 0,57m x 0,50m qui pourrait être un reliquat de la chambre funéraire démolie.

Les habitants de la région n'ont pas mémoire d'un cayolar ou d'une bergerie à cet endroit.

#### Historique

Monument découvert par [F. Meyrat]{.creator} en [novembre 2014]{.date}.

</section>

<section class="monument">

### [Iholdy]{.spatial} - [Hocha Handia]{.coverage} 2 - [Tumulus]{.type} ([?]{.douteux})

#### Localisation

Altitude : 554m.

Il se trouve sur le flanc Sud-Ouest du mont Hocha Handia, à l'amorce de la montée, au Nord de la cote 549.

#### Description

Tumulus de faible relief, que nous rattacherions à la catégorie des *tertres pierreux* fréquents dans ces collines.

#### Historique

Tumulus découvert par le [groupe Ilharriak]{.creator} en [février 2013]{.date}.

</section>

<section class="monument">

### [Iholdy]{.spatial} - [Hocha Handia]{.coverage} 3 - [Tumulus]{.type} ([?]{.douteux})

#### Localisation

Altitude : 550m.

À peu de distance de *Hocha Handia 2 - Tumulus (?)*, très discret, *tertre pierreux* ou origine naturelle ?

#### Historique

Tumulus découvert par le [groupe Ilharriak]{.creator} en [février 2013]{.date}.

</section>

<section class="monument">

### [Iholdy]{.spatial} - [Hocha Handia]{.coverage} 3 - [Tumulus]{.type}

#### Localisation

Altitude : 550m

#### Description

Ce petit tumulus pierreux est du même type que ceux de cette région du Lantabat, au sens vaste du terme ; il mesure 3,5m de diamètre et 0,20m de haut.

</section>

<section class="monument">

### [Iholdy]{.spatial} - [Hocha Handia]{.coverage} 4 - [Tumulus]{.type}

#### Localisation

Altitude : 553 m.

Situé à 4m au Sud de la piste de crête, et sur une rupture de pente, de sorte que le tumulus à tendance à s'effondrer de ce côté.

#### Description

Tumulus mixte, avec de nombreuses pierres apparentes, il mesure 5,50m de diamètre et environ 0,50 de haut.

#### Historique

Tumulus découvert par [F. Meyrat]{.creator} en [novembre 2014]{.date}.

</section>

<section class="monument">

### [Iholdy]{.spatial} - [Hocha Handia]{.coverage} 5 - [Tumulus]{.type}

#### Localisation

Contigu à *Hocha Handia 4 - Tumulus*, au Nord-Est.

#### Description

De dimension inférieure au précédent, difficile à préciser environ 2m et 0,15m de haut.

#### Historique

Tumulus découvert par [F. Meyrat]{.creator} en [novembre 2014]{.date}.

</section>

<section class="monument">

### [Iholdy]{.spatial} - [Hocha Handia]{.coverage} 6 - [Tumulus]{.type}

#### Localisation

Altitude : 554m.

Ces tumulus *Hocha Handia n°6* à *10* sont situés environ 200m à l'Est, sur un vaste plateau qui précède la montée au sommet d'Hocha Handia. Ils sont alignés, en deux groupes de 3, le long ou au milieu de deux filons rocheux de calcaire blanc, parallèles, à fleur de terre et, pour le premier, (*T6*) à environ 25m au Sud Sud-Ouest du chemin de crête.

#### Description

Tumulus circulaire mixte (terre et petits blocs de calcaire blanc éparpillés visibles en surface) de 3,70m de diamètre et de très faible hauteur (0,30m environ).

#### Historique

Tumulus découvert par [F. Meyrat]{.creator} en [novembre 2014]{.date}.

</section>

<section class="monument">

### [Iholdy]{.spatial} - [Hocha Handia]{.coverage} 7 - [Tumulus]{.type}

#### Localisation

À 5m à l'Est Sud-Est de *Hocha Handia 6 - Tumulus*.

Ces tumulus *Hocha Handia n°6* à *10* sont situés environ 200m à l'Est, sur un vaste plateau qui précède la montée au sommet d'Hocha Handia. Ils sont alignés, en deux groupes de 3, le long ou au milieu de deux filons rocheux de calcaire blanc, parallèles, à fleur de terre et, pour le premier, (*T6*) à environ 25m au Sud Sud-Ouest du chemin de crête.

#### Description

Petit tumulus mixte de 2,50m de diamètre et 0,15m de haut.

#### Historique

Tumulus découvert par [F. Meyrat]{.creator} en [novembre 2014]{.date}.

</section>

<section class="monument">

### [Iholdy]{.spatial} - [Hocha Handia]{.coverage} 8 - [Tumulus]{.type}

#### Localisation

Altitude : 556m.

Situé à 18m au Sud Sud-Est de *Hocha Handia 7 - Tumulus*.

Ces tumulus *Hocha Handia n°6* à *10* sont situés environ 200m à l'Est, sur un vaste plateau qui précède la montée au sommet d'Hocha Handia. Ils sont alignés, en deux groupes de 3, le long ou au milieu de deux filons rocheux de calcaire blanc, parallèles, à fleur de terre et, pour le premier, (*T6*) à environ 25m au Sud Sud-Ouest du chemin de crête.

#### Description

Très semblable aux précédents (1,70m de diamètre et 0,15m de haut). [Monument douteux]{.douteux}.

#### Historique

Tumulus découvert par [F. Meyrat]{.creator} en [novembre 2014]{.date}.

</section>

<section class="monument">

### [Iholdy]{.spatial} - [Hocha Handia]{.coverage} 9 - [Tumulus]{.type}

#### Localisation

Altitude : 557m.

Il est situé à 37m au Nord-Est de *Hocha Handia 8 - Tumulus*.

Ces tumulus *Hocha Handia n°6* à *10* sont situés environ 200m à l'Est, sur un vaste plateau qui précède la montée au sommet d'Hocha Handia. Ils sont alignés, en deux groupes de 3, le long ou au milieu de deux filons rocheux de calcaire blanc, parallèles, à fleur de terre et, pour le premier, (*T6*) à environ 25m au Sud Sud-Ouest du chemin de crête.

#### Description

Petit tumulus pierreux, semblables aux précédents, mesurant 3,10m de diamètre, 0,15m de haut.

#### Historique

Tumulus découvert par [F. Meyrat]{.creator} en [février 2015]{.date}.

</section>

<section class="monument">

### [Iholdy]{.spatial} - [Hocha Handia]{.coverage} 10 - [Tumulus]{.type}

#### Localisation

Altitude : 557m.

À 14m au Nord-Ouest de *Hocha Handia 9 - Tumulus*.

Ces tumulus *Hocha Handia n°6* à *10* sont situés environ 200m à l'Est, sur un vaste plateau qui précède la montée au sommet d'Hocha Handia. Ils sont alignés, en deux groupes de 3, le long ou au milieu de deux filons rocheux de calcaire blanc, parallèles, à fleur de terre et, pour le premier, (*T6*) à environ 25m au Sud Sud-Ouest du chemin de crête.

#### Description

Même type de tumulus mixte, mesurant 2,70m de diamètre et 0,10m de haut.

#### Historique

Tumulus découvert par [F. Meyrat]{.creator} en [février 2015]{.date}.

</section>

<section class="monument">

### [Iholdy]{.spatial} - [Hocha Handia]{.coverage} 11 - [Tumulus]{.type}

#### Localisation

Altitude : 557m.

Il est à 9m au Nord-Ouest de Hocha Handia 10 - Tumulus

Ces tumulus *Hocha Handia n°6* à *10* sont situés environ 200m à l'Est, sur un vaste plateau qui précède la montée au sommet d'Hocha Handia. Ils sont alignés, en deux groupes de 3, le long ou au milieu de deux filons rocheux de calcaire blanc, parallèles, à fleur de terre et, pour le premier, (*T6*) à environ 25m au Sud Sud-Ouest du chemin de crête.

#### Description

Petit tumulus pierreux mixte de 2,20m de diamètre et 0,10m de haut.

#### Historique

Tumulus découvert par [F. Meyrat]{.creator} en [février 2015]{.date}.

</section>

<section class="monument">

### [Iholdy]{.spatial} - [Laparzale]{.coverage} Nord 1 et 2 - [tertres pierreux]{.type}

#### Localisation

Sur la partie Nord du plateau de Laparzale, à 150m environ à l'Est de la route, vers l'extrémité Nord du plateau.

Altitude : 347m.

#### Description

Deux tertres pierreux tangents : le plus au Sud mesure 6m de diamètre, le second tangent au Nord, 7m ; hauteur approximative des deux : 0,30m.

Ces tertres pierreux à l'aspect assez particulier, dans cette région du Lantabat, ne sont pas des tertres d'habitat, pourraient être de tas d'épierrage et même éventuellement des tumulus à vocation funéraire, au moins pour certains.

#### Historique

Tertres déjà vus en [1975]{.date} et retrouvés par [J. Blot]{.creator} en 2013.

</section>

<section class="monument">

### [Iholdy]{.spatial} - [Laparzale]{.coverage} Nord 3 - [tertres pierreux]{.type}

#### Localisation

Altitude : 347m.

Il est à environ 60m au Nord Nord-Est de *Laparzale 1 et 2 Nord - tertres pierreux*

#### Description

Tertre pierreux de 6m de diamètre et 0,40m de haut.

#### Historique

Tertre retrouvé en [2013]{.date} par [J. Blot]{.creator}.

</section>

<section class="monument">

### [Iholdy]{.spatial} - [Mendibile]{.coverage} haut - [demi-cercle]{.type}

#### Localisation

Altitude : 457m.

Il est situé à l'extrémité Est du sommet de cette colline.

#### Description

Demi-cercle délimité par une trentaine de pierres, au ras du sol, apparaissant dans le secteur Sud du cercle.

#### Historique

Monument découvert par [Balere]{.creator} en [mars 2013]{.date}.

</section>

<section class="monument">

### [Iholdy]{.spatial} - [Mendibile]{.coverage} Nord 1 - [tertre pierreux]{.type}

#### Localisation

Sur le plateau qui s'étend au Nord de la colline de ce nom. Nous y avions décrits 6 tertres pierreux en 1975 [@blotNouveauxVestigesProtohistoriques1975, p. 117]. Lors de visite en 2013 nous n'en n'avons retrouvé que 3. (La moitié Ouest du plateau ayant été transformée en prairie artificielle, le reste étant envahi par une végétation très dense...).

Altitude : 310m.

#### Description

Tertre pierreux érigé sur un terrain en légère pente vers le Nord, mesurant 7m de diamètre et 0,40m de haut.

#### Historique

Tertre retrouvé par [J. Blot]{.creator} en [2013]{.date}.

</section>

<section class="monument">

### [Iholdy]{.spatial} - [Mendibile]{.coverage} Nord 2 - [tertre pierreux]{.type}

#### Localisation

Situé à une centaine de mètres à l'Est Nord-Est de *Mendibile 1 Nord - tertre pierreux*.

Altitude : 309m.

#### Description

Tertre pierreux, érigé sur terrain en très légère pente vers le Nord, mesurant 5m et 0,40m de haut.

#### Historique

Tertre retrouvé par [J. Blot]{.creator} en [2013]{.date}.

</section>

<section class="monument">

### [Iholdy]{.spatial} - [Mendibile]{.coverage} Nord 3 - [tertre pierreux]{.type}

#### Localisation

Situé à environ 80m au Nord-Est de *Mendibile 2 Nord - tertre pierreux*.

Altitude : 308m.

#### Description

Tertre pierreux de 8m de diamètre et 0,45m de haut.

</section>

# Iroulégui

<section class="monument">

### [Iroulégui]{.spatial} - [Artxuita]{.coverage} - [Ciste]{.type} ([?]{.douteux})

#### Localisation

Altitude :412m.

Ce monument est situé sur un petit plateau habituellement encombré de végétation (fougères et ajoncs) à environ une quarantaine de mètres au Nord-Ouest du dolmen d'Artxuita qui est bien visible, à une quarantaine de mètres à l'Ouest de la route qui mène au sommet du Jarra.

#### Description

Monument douteux, visible uniquement après écobuage. Quelques dalles, au ras du sol, délimitent ce qui pourrait être une chambre funéraire mesurant environ 2m de long et 1,30m de large, orientée Nord-Est Sud-Ouest. Il existe 2 dalles principales : la dalle **a**, à l'Ouest, mesurant 1,30m de long et 0,41m de large, éversée vers l'intérieur ; la dalle **b**, à l'Est, mesurant 1,30m de long et 0,50 de large, elle aussi éversée vers l'intérieur. Deux petites dalles plantées dans le sol (**c** = 0,40m de long et **d** = 0,26m de long) complètent la limite de la chambre à l'Ouest. Trois autres : (**e** = 0,10m, **f** =0,30m et **g** = 0,37m) formeraient la limite Nord-Est de cette chambre. À noter la présence d'un important bloc pierreux au centre (**h**) d'environ 0,80m de long...

Dans le contexte pierreux environnant il est très difficile d'évoquer l'existence d'un éventuel péristalithe...

#### Historique

Ciste découvert par [I. Txintxuretta]{.creator} en [mars 2016]{.date}.

</section>

<section class="monument">

### [Iroulégui]{.spatial} - [Jarra (Erramonénéa)]{.coverage} - [Dolmen]{.type} ([?]{.douteux})

#### Localisation

Altitude : 355m.

Ce faux dolmen est situé dans une prairie, à 140m au Sud Sud-Est de la maison Erramonénéa.

#### Description

Ses différents éléments constitutifs, en [grés]{.subject} rose, peuvent faire croire aux restes bien conservés d'un dolmen, à la [dolmen]{.subject} ouverte à l'Est, et dont le montant Nord, vertical, mesurerait 1,50m de long et 1,10m de haut ainsi que 0,50m d'épaisseur ; la dalle de chevet, verticale, située à l'Ouest Nord-Ouest mesurerait 0,70m de haut et 0,45m d'épaisseur. Divers éléments rocheux, au Sud du montant Nord, et au Nord-Ouest de cet ensemble pourraient faire croire à l'autre montant de la chambre funéraire et à un reste de la dalle de couverture.

Il ne s'agit en fait que d'un seul bloc rocheux éclaté probablement par le gel, dont aucun des éléments n'est véritablement enfoncé dans le sol. Même le propriétaire des lieux pense qu'il s'agit d'un dolmen...

</section>

<section class="monument">

### [Iroulégui]{.spatial} - [Jarra (Erramonénéa)]{.coverage} - [Monolithe]{.type} ([?]{.douteux})

#### Localisation

Il est situé à 150m au Nord Nord-Est de la maison Erramonénéa, au milieu d'un bois de châtaignier, et à 50m au Nord Nord-Ouest d'une petite bergerie située au bord d'une prairie entourée d'un muret de pierres sèches.

#### Description

Ce bloc rocheux mesure 1,30m à 1,40m de haut, 1m de large à la base et 0,80m d'épaisseur. Si sa situation, isolé au sommet d'une légère éminence naturelle, laisse à penser à qu'il n'est pas venu là naturellement, sa forme, ses dimensions et le fait qu'il ne soit que posé sur le sol ne nous incite pas à le classer dans les « monolithes » au sens où nous l'entendons, à savoir comme ancienne borne pastorale...

#### Historique

Ce bloc rocheux nous avait été signalé en [1996]{.date} par [I. Gaztelu]{.creator} comme pouvant être un monolithe.

</section>

<section class="monument">

### [Iroulégui]{.spatial} - [Jarra (Erramonénéa)]{.coverage} - [Monolithe]{.type} ([?]{.douteux})

#### Localisation

À 50m au Sud Sud-Ouest de la maison Erramonénéa (en lisière d'un bois de châtaignier).

#### Description

Pierre couchée au sol, rectangulaire, mesurant 2,40m de long, 1m de large et 0,35m d'épaisseur en moyenne. Une base plus large et plus épaisse (1,20m de large et 0,70m d'épaisseur) contraste avec l'autre extrémité, de dimensions plus modestes (0,50m de large et 0,13m d'épaisseur), arrondie, plus fine, qui semble avoir été épannelée. Cette pierre était couchée dans le champ, et les engins agricoles butaient souvent dessus, de sorte que le propriétaire l'a fait extraire et transportée à quelques mètres au Nord de son emplacement initial. Cette dalle pourrait *peut-être* (avec les réserves d'usage) avoir été une borne pastorale, quand elle était érigée, si elle l'a été.

</section>

# Irrissari

<section class="monument">

### [Irrissari]{.spatial} - [Behorsaro]{.coverage} - [Tertres d'habitat]{.type}

#### Localisation

Altitude : 506m.

Les 8 tertres d'habitat sont situés au flanc Nord-Est du mont Baigoura, au lieu-dit Béhorsaro, sous le point coté 60 sur la ligne de crête. Ils sont érigés sur un terrain en pente très douce vers le Sud-Est.

#### Description

De forme ovale le plus souvent.

-   *Tertre A* : Alttitude : 506m. À quelques mètres au Nord-Ouest d'un pointement rocheux. Mesure 16m x 8m et 0,90m de haut.

-   *Tertre B* : Mesure 13m x 8m et 0,80m de haut.

-   *Tertre C* : à 2m au Nord-Ouest du précédent, et mêmes dimensions.

-   *Tertre D* : à 1m au Nord-Ouest du précédent et mêmes dimensions.

-   *Tertre E* : à 30m au Nord Nord-Est du précédent ; mesure 13m x 8m et 1,50m de haut.

-   *Tertre F* : à 25m à l'Est Sud-Est du précédent ; mesure 5m x 4m, et 0,20m de haut - Très douteux.

-   *Tertre G* : à 2m au Sud Sud-Est du précédent ; mesure 5m x 4m et 0,20m de haut - Très douteux.

-   *Tertre H* : Il est situé à environ 300m au Nord Nord-Est du *Tertre F* et au Sud d'une piste pastorale bien marquée, tangent à celle-ci. Mesure 11m x 10m et 0,90m de haut.

#### Historique

Tertres trouvés par [Blot J.]{.creator} en [1974]{.date}. (Le *Tertre F* l'a été par [Meyrat F.]{.creator} en [décembre 2013]{.date}).

</section>

<section class="monument">

### [Irrissari]{.spatial} - [Margoueta]{.coverage} 1 - [Tumulus]{.type}

#### Localisation

Altitude : 380m.

Il est tout en bordure du chemin et à l'Est de celui-ci, et à 5m au Sud d'une pierre isolée, elle aussi en bordure du chemin, mesurant 0,70m de long, 0,50m de large et 0,24m de hauteur ; elle paraît naturellement en place.

#### Description

Tumulus circulaire, mesurant 3m de diamètre et 0,20m de haut, constitué de terre avec quelques pierres visibles. Le chemin en a légèrement entamé le secteur Ouest.

#### Historique

Monument découvert par [F. Meyrat]{.creator} en [août 2015]{.date}.

</section>

# Itxassou

<section class="monument">

### [Itxassou]{.spatial} - [Âne (Col de l')]{.coverage} - [Monolithe]{.type}

#### Localisation

Altitude : 590m.

Il est à environ une centaine de mètres à l'Est Nord-Est de *Âne (Col de l') - Dolmen*, et à 3m au Nord-Ouest de la piste qui se rend à Arluxetta.

#### Description

Belle dalle de [grés]{.subject} rose gisant sur le sol, de forme grossièrement triangulaire, à grand axe orienté Nord-Est Sud-Ouest. Elle mesure 1,79m de long, 1,04m à sa base et 0,20m d'épaisseur. Tout son bord Nord-Ouest présente des traces d'épannelage.

#### Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2010]{.date}.

</section>

<section class="monument">

### [Itxassou]{.spatial} - [Âne (Col de l')]{.coverage} - [Tumulus]{.type}

Il existe à 2m au Nord Nord-Ouest de *Âne (Col de l') - Dolmen* un tumulus de terre mesurant 5m de diamètre et 0,30m de haut.

#### Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2010]{.date}.

</section>

<section class="monument">

### [Itxassou]{.spatial} - [Âne (Col de l')]{.coverage} - [Dolmen]{.type}

#### Localisation

Altitude : 585m.

Monument situé à l'extrémité Sud d'un vaste espace de gazon vert, où ne poussent pas les fougères et à la confluence de plusieurs pistes pastorales montant au flanc Est de l'Artzamendi, dont deux le contournent en son secteur Ouest.

#### Description

On note un tumulus de terre de 6m de diamètre et 0,40m de haut.

Au centre, apparaît une grande dalle de [grés]{.subject} [triasique]{.subject}, inclinée vers le Nord, orientée Est Sud-Est Ouest Nord-Ouest, mesurant 1,38m de long, 0,38m de hauteur apparente, et 0,14m d'épaisseur ; toute sa périphérie visible présente des traces d'épannelage.

Dans le secteur Sud du tumulus une dizaine de pierres, disposées en demi-cercle, semblent bien pouvoir être considérées comme les vestiges d'un péristalithe. Leurs dimensions sont variables, allant de 0,82m de long pour la plus grande à 0,42m pour la plus petite.

#### Historique

Monument découvert en [octobre 2010]{.date} par [Currutchet J.]{.creator} et [Régnier J.]{.creator}.

</section>

<section class="monument">

### [Itxassou]{.spatial} - [Âne (Col de l')]{.coverage} - [Dalle couchée]{.type} ([?]{.douteux})

#### Localisation

Altitude : 600m.

Elle se trouve à 1,60m à l'Ouest du monolithe décrit ci-dessus sous le nom de *Âne (Col de l') - Monolithe*.

#### Description

Elle mesure 1,40m dans son axe Nord-Ouest Sud-Est, et 1,10m selon l'axe Sud-Ouest Nord-Est. Son épaisseur varie de 0,12m à 0,30m suivant les endroits. Des traces d'épannelage semblent pouvoir être notées tout le long du bord Sud-Est de cette dalle.

#### Historique

Pierre découverte en [février 2011]{.date} par [F. Meyrat]{.creator}.

</section>

<section class="monument">

### [Itxassou]{.spatial} - [Apalaenea]{.coverage} - [Cromlech]{.type}

#### Localisation

A{.date}ltitude : 492m.

Il est situé à l'extrémité Nord du Plateau Vert et à 8m à l'Est d'un filon rocheux nettement visible.

#### Description

Cromlech de 7m de diamètre constitué d'une dizaine de dalles plus ou moins grandes souvent au ras du sol ; cependant, la plus grande, en secteur Ouest atteint 0,60m de long et 0,45m de haut, ainsi qu'une autre en secteur Sud-Est, mesurant 0,65m de long et 0,25m de haut. Trois autres dalles apparaissent en superficie à l'intérieur du cercle.

#### Historique

Monument découvert par [A. Martinez]{.creator} en [février 2013]{.date}.

</section>

<section class="monument">

### [Itxassou]{.spatial} - [Arimaluchenekoborda]{.coverage} - [Monolithe]{.type}

#### Localisation

Altitude : 560m.

Monolithe gisant sur la gauche du chemin qui descend vers la borde du même nom.

#### Description

Belle dalle de [grés]{.subject} rose de forme parallélépipédique, à grand axe Est-Ouest mesurant 2,40m. Sa partie la plus large mesure 1,60m, et ses deux extrémités Est et Ouest, 1,30m ; son épaisseur peut atteindre de 0,24m à 0,30m. Il semble y avoir des traces d'épannelage aux bords Ouest et Nord.

#### Historique

Monolithe découvert par [J. Blot]{.creator} en [mars 2011]{.date}.

</section>

<section class="monument">

### [Itxassou]{.spatial} - [Arluxeta]{.coverage} - [Tertres d'habitat]{.type}

#### Localisation

Altitude : 557m.

Les 14 tertres sont situés à l'extrémité du contrefort Nord-Est de l'Artzamendi qui domine (de loin...) le Pas de Roland ; ils sont au nombre de 14, répartis de part et d'autre d'un petit ruisseau orienté Sud-Ouest Nord-Est, dans le sens de la pente. Une borde en ruine entourée d'arbres est située à une quarantaine de mètres au Sud-Est.

#### Description

Ils sont érigés sur un terrain en légère pente vers le Nord-Est, leur diamètre est, en moyenne, de 10m à 11m et leur hauteur de 0,50m.

-   *TH n°1* : coordonnées ci-dessus. Il est à 13m au Sud-Ouest d'un amas de rochers bien visibles, et à l' Ouest du ruisseau, sur sa rive gauche,comme les autres tertres d'habitat jusqu'au n°8 inclus. Du TH 9 au TH 14, ils sont à l'Est de ce ruisseau. Il mesure 11m x 8m et 0,30m de haut.

-   *TH n°2* : à 15m au Sud du précédent. Mesure 10m x 7m et 0,30m de haut.

-   *TH n°3* : à 18m au Sud Sud-Ouest du précédent. Mesure 10m x 8m et 0,10m de haut.

-   *TH n°4* : à 18m au Sud Sud-Ouest du précédent. Mesure 6m x 6m et 0,30m de haut.

-   *TH n°5* : à 18m au Sud Sud-Ouest du précédent. Mesure 8m x 6m et 0,10m de haut.

-   *TH n°6* : à 2m au Sud Sud-Ouest du précédent. Mesure 11m x 6m et 0,60m de haut ; légère dépression à son sommet.

-   *TH n°7* : à 8m au Sud Sud-Ouest du précédent. Mesure 10m x 8m et 0,40m de haut ; légère dépression au sommet.

-   *TH n°8* : à 6m au Sud-Ouest du précédent. Mesure10m x 8m et 0,60m de haut. Le dernier TH à l'Est du ruisseau. Légère dépression au sommet avec 4 pierres visibles.

-   *TH n°9* : à 8m à l'Est du précédent, à l'Ouest du ruisseau. Mesure 10m x 8m et 0,30m de haut.

-   *TH n°10* : à 4m au Nord Nord-Est du précédent. Mesure 10m x 8m et 0,30m de haut.

-   *TH n°11* : tangent au précédent. Mesure 11m x 8m et 0,50m de haut ; légère dépression au sommet.

-   *TH n°12* : à 8m à l'Est Sud-Est du précédent. Mesure 9m x 8m et 0,60m de haut ; légère dépression au sommet.

-   *TH n°13* : à 4m au Nord du n° 11. Mesure 8m x 6m et 0,30m de haut.

-   *TH n°14* : à 2m au Nord Nord-Est du précédent. Mesure 11m x 8m et 0,50m de haut.

#### Historique

Cet ensemble de tertres d'habitat a été découvert par [Blot J.]{.creator} en [novembre 1974]{.date} et retrouvé et étudié par [Meyrat F.]{.creator} en [février 2014]{.date}.

</section>

<section class="monument">

### [Itxassou]{.spatial} - [Arrokagaray]{.coverage} - [Dolmen]{.type}

#### Localisation

Carte 1345 Ouest Cambo-les-Bains.

Altitude : 319m.

Il est situé au Sud du chemin qui parcourt le plateau presque au sommet du mont Arrokagarray, à 30m à l'Ouest d'un volumineux amas rocheux.

#### Description

La piste empiète sur la périphérie Nord-Ouest du tumulus pierreux qui mesure environ 10m de diamètre, et 0,80m de haut. La [chambre funéraire]{.subject}, orientée Ouest Sud-Ouest, Est Nord-Est, mesure 1,50m de long, 0,50m de large et 0,85m de haut. Elle est délimitée par deux grandes dalles de [grès]{.subject} : une au Sud, presque verticale, de 1,40m de long et 0,85m de haut ; l'autre au Nord, de mêmes dimensions semble-t-il, mais presque couchée, et en partie enfouie sous la terre. Pas de table visible.

#### Historique

Dolmen découvert en [avril 1972]{.date}. Les dalles centrales ont depuis disparu (2004), détruites par la confection d'une prairie artificielle.

</section>

<section class="monument">

### [Itxassou]{.spatial} - [Arrokagarray]{.coverage} 1 - [Tumulus]{.type}

#### Localisation

À environ 250m à l'Ouest de *Arrokagaray - Dolmen*, sur le même plateau, à environ une cinquantaine de mètres avant la dernière montée vers le sommet.

#### Description

On note un amas de volumineux blocs rocheux qui a été déposé, relativement récemment, à cet endroit, en vrac, avec un engin de terrassement. Il semble bien, toutefois, que ce dépôt ait été effectué sur un substrat tumulaire pré-existant, de 20m de diamètre et d'une hauteur difficile à apprécier maintenant, mais qui devait avoisiner 0,50m environ. Ce tumulus est formé de petits blocs rocheux de la taille approximative d'un pavé, dissimulés sous la couche herbeuse très drue.

#### Historique

Tumulus découvert en [décembre 2003]{.date}.

</section>

<section class="monument">

### [Itxassou]{.spatial} - [Arrokagaray]{.coverage} 2 - [Tumulus]{.type}

#### Localisation

Carte 1345 Ouest Cambo-les-Bains.

Altitude : 850m.

Il est sur le sommet plat du mont Arrokagaray, à environ 250m à l'Est du point culminant et à l'endroit où s'amorce la descente vers le plateau où sont érigés *Arrokagarray 1 - Tumulus* et *Arrokagarray 2 - Tumulus*.

#### Description

Tumulus mixte de forme ovale, à grand axe Est-Ouest, mesurant 22m x 16m et près de 2m de haut. Des blocs de pierre sont particulièrement visibles en son quart Sud Sud-Est.

#### Historique

Tumulus découvert en [avril 1972]{.date}.

</section>

<section class="monument">

### [Itxassou]{.spatial} - [Artzamendi]{.coverage} 1 - [Monolithe]{.type}

#### Localisation

Altitude : 367m.

Monolithe situé en rupture de pente, au bord Sud du vaste plateau qui s'étend au Sud-Est et en-dessous du sommet de l'Artzamendi. Situé sur un terrain en pente douce vers le Sud-Ouest.

#### Description

Très belle dalle de [grès]{.subject} [triasique]{.subject}, allongée sur le sol selon un axe Nord-Ouest Sud-Est, de forme rectangulaire à sommet Sud taillé en pointe, l'ensemble étant assez bien symétrique par rapport à l'axe cité. Elle mesure 3,50m de long, 1,02m dans sa plus grande largeur, et présente une épaisseur moyenne de 0,22m. On note de très nombreuses traces d'épannelage sur la quasi-totalité de son pourtour.

#### Historique

Monolithe trouvé par [Blot J.]{.creator} en [juin 2016]{.date}.

</section>

<section class="monument">

### [Itxassou]{.spatial} - [Artzamendi]{.coverage} 2 - [Monolithe]{.type}

#### Localisation

Altitude : 844m.

Situé sur le même plateau au Sud-Est et en-dessous du mont Artzamendi que *Artzamendi 1 - Monolithe*, mais de façon plus centrale, bien visible à quelques mètres au Sud d'une piste tracée par le passage répété du bétail et des hommes circulant selon un axe Est-Ouest.

Ce monolithe est situé à côté (0,15m) et à l'Est d'une ébauche de meule taillée dans du [grès]{.subject} [triasique]{.subject} (1,43m de diamètre et 0,30m d'épaisseur.).

#### Description

Monolithe de [grès]{.subject} [triasique]{.subject} parallélépipédique, couché sur le sol selon un axe Nord-Sud, pratiquement rectangulaire à la coupe.

Il mesure 2,70m de long, 0,70m de large en moyenne et 0,30m d'épaisseur en moyenne ; son pourtour présente de nombreuses traces de régularisation. Sa présence à côté de l'ébauche de meule nous incite à lui donner un âge relativement récent...

#### Historique

Monolithe trouvé par [Blot J.]{.creator} en [juin 2016]{.date}.

</section>

<section class="monument">

### [Itxassou]{.spatial} - [Artzamendi]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 914m.

On peut le voir à 4m à l'Ouest du dernier et plus petit mur érigé près de radôme.

#### Description

Petit tumulus pierreux circulaire en forme de galette aplatie, mesurant 1,90m à 2m de diamètre et 0,25m de haut. Une quarantaine de petits blocs de [grès]{.subject} [triasique]{.subject} sont visibles en surface. Monument douteux.

#### Historique

Monument découvert par [Blot J.]{.creator} en [juin 2016]{.date}.

</section>

<section class="monument">

### [Itxassou]{.spatial} - [Atharri]{.coverage} - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 371m.

Il est situé à l'amorce du versant Nord du point culminant de la colline Atharri.

#### Description

Beau tertre de forme ovale, à grand axe Est-Ouest, et dissymétrique (1m dans sa plus grande hauteur au Nord), mesurant 15m de long et une dizaine de large. Il paraît seul bien que certains reliefs soient visibles à quelque distance au Sud-Est.

#### Historique

Monument découvert par [Blot J.]{.creator} en [novembre 2011]{.date}.

</section>

<section class="monument">

### [Itxassou]{.spatial} - [Atharri]{.coverage} 1 - [Tumulus]{.type}

#### Localisation

Altitude : 355m.

Le monument est situé à environ 350m à l'Est du col de Legarre, tangent au Sud de la piste.

#### Description

Il s'agit d'un tumulus circulaire de 5,30m de diamètre, et quelques centimètres de haut, constitué de nombreuses pierres de petites dimensions, profondément enfouies dans le sol. On distingue parfaitement la quasi-totalité de sa périphérie, sauf le tiers Sud Sud-Est qui est recouvert, ainsi que la partie centrale du monument, par un amoncellement de pierrailles mobiles qui ont été déversées dessus à l'occasion d'aménagements relativement récents de la piste et de ses environs. Est-ce un tumulus dolménique ou un tumulus simple ? Au vu des dimensions nous opterions plus volontiers pour la seconde hypothèse.

#### Historique

Monument découvert par [J. Blot]{.creator} en [avril 2010]{.date}.

</section>

<section class="monument">

### [Itxassou]{.spatial} - [Atharri]{.coverage} 2 - [Tumulus]{.type}

#### Localisation

Altitude : 360m.

On le trouve à environ 80m plus à l'Est de *Atharri 1 - Tumulus*, légèrement en surplomb de la piste au moment où elle aborde le plateau, cachée, à gauche, par des touffes de touyas.

#### Description

Comme précédemment, le monument est en grande partie recouvert par des apports récents de pierraille ; néanmoins, on distingue parfaitement la moitié Nord de la périphérie du tumulus circulaire originel sous-jacent, de 4,40m de diamètre et quelques centimètres de haut. Là encore, nous optons pour un tumulus simple. À noter, juste à côté un autre important amas de pierraille dont il est impossible de dire s'il recouvre ou non un autre tumulus.

#### Historique

Monument découvert en [avril 2010]{.date} par [F. Meyrat]{.creator}.

</section>

<section class="monument">

### [Itxassou]{.spatial} - [Atharri]{.coverage} 3 - [Tumulus]{.type}

#### Localisation

Il est situé à 15m au Sud-Ouest de *Atharri - Tertre d'habitat*, au point sommital de la ligne de crète.

#### Description

Ce tumulus a été en grande partie arasé lors de la création de la prairie artificielle où il se situe actuellement ; on peut toutefois encore distinguer son relief (modeste, de quelques centimètres) et sa périphérie, donc son diamètre, soit 6m environ actuellement ; au centre se voit une légère dépression d'une trentaine de centimètres de diamètre.

#### Historique

Monument découvert par [Blot J.]{.creator} en [novembre 2011]{.date}.

</section>

<section class="monument">

### [Itxassou]{.spatial} - [Ezuretta]{.coverage} - [Cromlechs]{.type}

On peut décrire 7 cromlechs (+1...) au col d'Ezuretta situé au Sud du mont Ourezti :

-   *Ezurreta 1 - Cromlech*

    Altitude : 690m.

-   *Ezuretta 1 bis - Cromlech*

    Il est tangent au Sud-Ouest du n°1, avec lequel il a 3 témoins en commun et 6 autres pierres extérieures : ces 9 pierres semblent délimiter un cercle d'environ 1,50m de diamètre. Monument douteux ? Découvert par J. Blot en [avril 2013]{.date}.

-   *Ezuretta 2 - Cromlech*

    Il a été décrit par Blot J. [@blotInventaireMonumentsProtohistoriques2009].

    Il est situé à 4m à l'Ouest du n°1.

-   *Ezuretta 3 - Cromlech*

    Il a été décrit par Blot J. [@blotInventaireMonumentsProtohistoriques2009, p.8-9].

    Le cromlech n°3 est à 15m au Sud-Ouest du n°2 et est traversé par la piste.

Parmi les 4 cromlechs suivants, 3 ont été découverts ces dernières années par le [groupe Hilharriak]{.creator} et 1 par [J. Blot]{.creator}, sans qu'il soit bien aisé de préciser qui est l'inventeur de chaque monument, ce qui n'a d'ailleurs qu'une importance très relative !

-   *Ezuretta 4 - Cromlech*

    -   Localisation

        Situé à 2m au Sud-Ouest du n° 3 et à l'Ouest de la piste qui traverse le n°3.

    -   Description

        Huit pierres bien visibles, de calibres variés, délimitent un cercle de 4m de diamètre.

-   *Ezuretta 5 - Cromlech*

    Il est situé à 9m au Sud-Est du n°4 ; six pierres, surtout visibles dans la moitié Est, délimitent un cercle de 5,40m de diamètre.

-   *Ezuretta 6 - Cromlech*

    Situé à 15m au Sud Sud-Ouest du n°5. Il mesure 2m de diamètre et 8 pierres, au ras du sol, en délimitent la périphérie.

-   *Ezuretta 7 - Cromlech (?)*

    Il est à 6m au Sud du n°6 et 5 pierres, peu visibles délimitent un cercle de 4,70m de diamètre.

    Monument douteux.

</section>

<section class="monument">

### [Itxassou]{.spatial} - [Ezuretta]{.coverage} 2 - [Cromlech]{.type}

#### Localisation

Carte 1345 Ouest Cambo-les-Bains.

Altitude : 620m.

On le trouve au flanc Sud du mont Ourezti - lui-même au Sud du Mondarrain - et à 100m au Nord du col Ezuretta.

Il est situé sur un sol en légère pente vers le Sud, à 4m à l'Ouest Sud de *Arrokagarray 1 - Tumulus*, qui est un très beau cercle de 6m de diamètre, formé de 17 pierres bien visibles, découvert par J.M.  de Barandiaran en 1941 (ref)(Barandiaran J.M. . de,1949, p.205).

#### Description

Il semble que l'on puisse décrire 2 cercles concentriques : un cercle extérieur de 10m de diamètre, constitué de 12 pierres, et un cercle intérieur de 8m de diamètre délimité par 21 pierres. Les pierres constitutives de ce monument sont, dans l'ensemble, plus visibles dans le quart Sud-Ouest. Au centre apparaissent les sommets de 3 petites dalles.

#### Historique

Monument découvert en [octobre 1974]{.date}.

</section>

<section class="monument">

### [Itxassou]{.spatial} - [Ezuretta]{.coverage} 3 - [Cromlech]{.type}

#### Localisation

À 15m au Sud-Ouest de *Ezuretta 2 - Cromlech*, édifié sur un sol plat.

#### Description

Cromlech légèrement surélevé, mesurant 6,80m de diamètre, formé de 19 pierres ne dépassant le sol que de quelques centimètres.

#### Historique

Monument découvert en [octobre 1974]{.date}.

</section>

<section class="monument">

### [Itxassou]{.spatial} - [Iguzki]{.coverage} - [Monolithe]{.type}

#### Localisation

Au flanc Nord-Ouest du pic Iguzki. La route qui mène au col de Méatsé et aux émetteurs de l'Artzamendi, effectue un **Z** avec deux angles droits ; le monolithe est visible à environ 60m en contre-bas du deuxième virage en montant, érigé sur un terrain en pente. On notera la présence à environ 80m à l'Ouest, d'un point d'eau sous forme d'un petit ruisseau.

Altitude : 640m.

#### Description

Il s'agit d'une importante dalle de [grés]{.subject} rose, de forme grossièrement trapézoïdale et inclinée à 55° vers le Nord-Est. Sa base, enfouie dans le sol, est orientée Sud-Est Nord-Ouest. Il semble qu'elle ait pu être plus verticale dans le passé comme le suggère un bloc rocheux sur lequel elle prend appui au Sud-Est, et qui paraît s'être en partie brisé sous son poids. Les mensurations de ce monolithe sont les suivantes : 2,30m de hauteur, 2,80m à la base, épaisse elle-même de 0,80m ; l'épaisseur au sommet n'étant que de 0,52m. Il n'y a pas de traces d'épannelage. Il semble peu probable, compte tenu du contexte géologique et de ses dimensions, que cette dalle ait pu adopter de façon naturelle cette position sur la tranche. La situation de cette dalle à flanc de montagne n'est pas sans nous rappeler le *monolithe Iparla 1* ou ceux d'Athékaleun ou Gaztenbakarre (dans la Rhune).

#### Historique

Monument découvert [J. Blot]{.creator} en [1982]{.date}.

</section>

<section class="monument">

### [Itxassou]{.spatial} - [Iguzki]{.coverage} 3 - [Tumulus]{.type}

#### Localisation

Carte 1345 Ouest.

Altitude : 580m.

Il est situé à 20m au Sud-Ouest de *Ezuretta 2 - Cromlech*.

#### Description

Tumulus mixte de terre et de blocs calcaire de [grès]{.subject} local, mesurant 6,50m de diamètre et 0,40m de haut. Au centre est visible une dépression de 2m de long et un de large ; trace d'une fouille ancienne ?

#### Historique

Monument découvert en [janvier 1973]{.date}.

</section>

<section class="monument">

### [Itxassou]{.spatial} - [Iguzki]{.coverage} 4 - [Tumulus]{.type}

#### Localisation

Il est à 3m au Sud-Ouest de *Iguzki 3 - Tumulus*.

#### Description

Tumulus mixte, de terre et de pierres, de 6m à 7m de diamètre et 0,90m de haut. Il semble délimité à sa périphérie par une couronne de pierres calcaires blanches particulièrement visibles en secteur Ouest. Au centre il existe un petit amas pierreux, aux éléments assez mobiles.

#### Historique

Monument découvert en [janvier 1973]{.date}.

</section>

<section class="monument">

### [Itxassou]{.spatial} - [Iguzki]{.coverage} 5 - [Tumulus]{.type}

#### Localisation

Carte 1345 Ouest.

Altitude : 403m.

Il est sur un replat dominant à une cinquantaine de mètres à l'Est la route menant aux émetteurs de l'Artzamendi, et à 10m à l'Ouest d'un double pylône électrique ; sol légèrement en pente vers l'Ouest.

#### Description

Tumulus de terre de 5,50m de diamètre et 0,80m de haut ; il existe au centre une dépression de 2m de diamètre et 0,30m de profondeur : fouille ancienne ?

N.B. : [Nous]{.creator} avons publié ce monument, (situé sur la commune d'Itxassou), en 1973 dans [@blotNouveauxVestigesMegalithiques1973, p.10].

#### Historique

Monument découvert en [janvier 1973]{.date}.

</section>

<section class="monument">

### [Itxassou]{.spatial} - [Iguski]{.coverage} 6 - [Cromlech]{.type}

#### Localisation

Altitude : 570m.

Il se trouve sur un élargissement du terrain sur lequel passe le sentier de Grande Randonnée, lors de son contour des prairies artificielles situées à la partie inférieure du flanc Ouest du Pic Iguski.

#### Description

Monument difficile à voir (douteux ?), érigé sur terrain plat. On note un cercle de 5m de diamètre environ, en très léger relief délimité par de nombreuses pierres de dimensions variables, au ras du sol, plus abondantes dans le secteur Nord-Ouest.

#### Historique

Monument découvert par [Blot J.]{.creator} en [novembre 2011]{.date}.

</section>

<section class="monument">

### [Itxassou]{.spatial} - [Iguski]{.coverage} 7 - [Cromlech]{.type}

#### Localisation

Altitude : 589m.

Il est situé au milieu d'une petite croupe qui s'étend à gauche de la route qui monte au col de Méatsé (Artzamendi).

#### Description

Une dizaine de belles dalles de [grès]{.subject} rose, dont certaines atteignent 1,10m x 1m, délimitent approximativement un cercle de 3m à 3,50m de diamètre, dont le centre excavé traduit l'existence d'une fouille ancienne. Celle-ci a de plus bouleversé la structure du cercle, dont les témoins ont été plus ou moins déplacés de leur implantation d'origine, ou même brisés, d'où l'irrégularité du cercle.

#### Historique

Cromlech découvert par [J. Blot]{.creator} en [mars 2016]{.date}.

</section>

<section class="monument">

### [Itxassou]{.spatial} - [Iguskiegi]{.coverage} - [Monolithe]{.type}

#### Localisation

Altitude : 545m.

Cette pierre est située au col d'Iguskiegi, en bordure de piste.

#### Description

Cette importante dalle de [grés]{.subject} rose est actuellement verticale ; de forme triangulaire elle mesure 1,83m de haut, 2,30m à sa base et 0,30m d'épaisseur. Elle est orientée suivant un axe Nord-Sud.

#### Historique

Ce monument m'a été signalé par [A. Martinez Manteca]{.creator} en [février 2010]{.date}. Toutefois, mon ami [F. Meyrat]{.creator} avait déjà noté la présence de cette dalle, à cet endroit, en [2000]{.date} ; mais elle était couchée au sol, et affectait une forme plutôt rectangulaire ; on notait que l'angle supérieur gauche était séparé de la dalle par une très nette fracture, (très probablement naturelle) tout en étant resté en place. La mise (remise ?) en place, verticale de cette pierre, effectuée depuis, a laissé au sol le fragment détaché, d'où son aspect actuel. Bien qu'il s'agisse d'un travail récent, on ne peut manquer de penser que cette grande dalle, gisant à cet endroit (un très beau carrefour de pistes pastorales), avait pu être disposée là.... il y a fort longtemps, et que l'on n'a fait que « remettre les choses en place ».

</section>

<section class="monument">

### [Itxassou]{.spatial} - [Iguzkimendi]{.coverage} - [Monolithe]{.type} ([?]{.douteux})

#### Localisation

Situé en Navarre (côté espagnol), sur le flanc Sud du mont Iguzki.

Altitude : 757m.

On y accède en empruntant le sentier qui part vers le Sud-Ouest du col de Méatsé. Ce monolithe est sur un replat à une quarantaine de mètres au-dessus du sentier.

#### Description

Monolithe de [grès]{.subject} [triasique]{.subject} de forme oblongue, couché au sol, orienté Nord-Sud, avec pointe au Nord et partie renflée au Sud. Il mesure 3,70m de long, 1,07m de haut et possède une largeur variant entre 1,40m au milieu et 1,70m vers l'arrière où il est le plus épais.

Sa particularité, outre sa proximité avec la frontière, est de présenter sur sa face Ouest 4 traces verticales équidistantes et parallèles pour 3 d'entre elles. - La n°1 (la plus au Nord), est verticale, mesure 0,86m de long, est large de 0,06m ; - la n°2, plus au Sud, lui est parallèle et mesure 0,87m de long et 0,06m de large ; - la troisième, elle aussi parallèle, ne mesure que 0,54m de long et 0,05m de large. - Enfin la quatrième, légèrement oblique à droite, mesure 0,64m de long. Ces traces sont-elles le résultat d'une action humaine (laquelle ?) ou de la nature (érosion ?).

#### Historique

Ce monument nous a été signalé par le [groupe Herri Harriak]{.creator} en [décembre 2014]{.date}.

</section>

<section class="monument">

### [Itxassou]{.spatial} - [Itxassou]{.coverage} 1 - [Tumulus]{.type}

#### Localisation

Carte IGN 1345 Ouest Cambo-les-Bains.

Altitude : 85m.

On trouve 2 tumuli dans la propriété Hirriberia, sous les arbres, à l'Est de la route Cambo-Louhossoa.

#### Description

Le tumulus n°1 est formé de terre et de galets, possède un diamètre de 25m et une hauteur de 0,80m. Un passage de bulldozer l'a considérablement aplani depuis la première observation.

#### Historique

Monument découvert en [mars 1968]{.date}.

</section>

<section class="monument">

### [Itxassou]{.spatial} - [Itxassou]{.coverage} 2 - [Tumulus]{.type}

#### Localisation

Carte IGN 1345 Ouest Cambo-les-Bains.

Il est tangent au Nord-Ouest de *Itxassou 1 - Tumulus*.

#### Description

Constitué lui aussi de terre et de galets, il a été lui aussi très remanié par le passage d'un bulldozer ; il mesurait initialement 12m de diamètre et 0,70m de haut.

#### Historique

Monument découvert en [mars 1968]{.date}.

</section>

<section class="monument">

### [Itxassou]{.spatial} - [Itxassou]{.coverage} 3 - [Tumulus]{.type}

#### Localisation

Carte IGN 1345 Ouest Cambo-les-Bains.

À une cinquantaine de mètres au Nord-Ouest, dans un champ de l'autre côté de la route.

#### Description

Ce tumulus a été rasé par la préparation d'un lotissement. Nous n'avons aucune notion de ses dimensions, et n'avons pu que constater, sur le sol, une nappe circulaire de galets, d'environ une vingtaine de mètres de diamètre, et des traces de charbons de bois au centre ; pas de fragments de céramique visibles.

#### Historique

Monument découvert en [mars 1968]{.date}.

</section>

<section class="monument">

### [Itxassou]{.spatial} - [Itxassou]{.coverage} 4 - [Tumulus]{.type}

#### Localisation

Carte IGN 1345 Ouest Cambo-les-Bains.

Altitude : 60m.

Il est situé dans un pré, à 50m à l'Est de l'ancienne route qui va de Cambo à Louhossoa, en face de l'embranchement qui conduit à l'église d'Itxassou, au lieu-dit «les cinq cantons» et au Sud-Ouest de la nouvelle route.

#### Description

Tumulus terreux de 16m de diamètre et 0,80m de hauteur ; [monument douteux]{.douteux}.

#### Historique

Monument découvert en [mars 1968]{.date}.

</section>

<section class="monument">

### [Itxassou]{.spatial} - [Legarreko]{.coverage} Lepoa - [Pierre couchée]{.type}

#### Localisation

Altitude : 345m.

On peut voir cette pierre dans le jardin de la maison qui jouxte l'angle de la route au niveau du col de Legarre, à 6m au Nord du portail, repoussée contre la haie.

#### Description

Ce bloc, de gneiss semble-t-il, couché au sol, affecte la forme d'un parallélépipède rectangle de 2m de long, 0,98m de large à sa base, 0,48m au sommet qui est tronqué, et 0,45m d'épaisseur.

Cette pierre est intéressante par sa situation dans le col et le fait qu'elle semble présenter des traces d'épannelage sur toute sa face Nord.

#### Historique

Pierre découverte par [F. Meyrat]{.creator} en [avril 2010]{.date}.

</section>

<section class="monument">

### [Itxassou]{.spatial} - Les monuments du [col de Méatsé (Artzamendi)]{.coverage}

#### Localisation

Carte 1345 Ouest Cambo-les-Bains.

Altitude : 716m.

Ce col, situé entre le mont Artzamendi au Nord-Est, et le mont Iguski au Sud-Ouest, est le siège d'une importante nécropole.

#### Histoire et description sommaire

Le site de Méatsé a été découvert par [J.M. De Barandiaran]{.creator}, en [1943]{.date}, qui signale [@barandiaranCronicaPrehistoriaAlduides1949] un dolmen (*dolmen de Iuskadi*) (fouillé en 1973) et 3 cromlechs au niveau du col (*groupe Méatséko-Lepoa*), avec, au Sud-Ouest de celui-ci, les 5 cercles du *groupe Iuskadi*, et au Nord-Est, le *groupe Méatseko-Bizkarra* avec 3 cercles, dont un contenant la borne frontière n°81 en son centre. Par la suite, ce col fit l'objet, sous la pression des circonstances, de nombreuses interventions qui permirent de mieux connaître le nombre des monuments et d'en préciser les architectures.

Dès 1970, nous remarquons que le *[cromlech]{.type} n°10* a été amputé de son tiers Nord-Ouest par le passage de la route, et que des éléments de son coffre central apparaissent dans la coupe, alors que son voisin immédiat, au Sud-Ouest, le *cromlech n°9*, est toujours intact.

Nous effectuons une fouille de sauvetage sur 2 [cistes]{.subject} (numéros 4 et 7 du plan), qui ne font que préciser leur structure, et sur le *cromlech n°1*, menacés par des engins de terrassement [@blotCromlechMeatseItxassou1970]. Ce dernier cercle mesure 5,50m environ de diamètre, et il est formé d'une alternance régulière de petits amas de dalles horizontales séparées par des dalles plantées de chant et de façon radiale. Au centre un volumineux coffre, à grand axe Nord-Ouest Sud-Est, formé d'une dalle mesurant 1,30m de long et 1,10m de large reposant sur 7 montants, dont le plus important est un bloc de [grès]{.subject} de 1,50m de long, disposé horizontalement au Nord-Ouest sur le sol préalablement décapé (comme cela a été la règle pour tous les monuments de cette nécropole). Six autres dalles de modestes dimensions complètent ce coffre. À part un semis de particules carbonées, réparti dans l'ensemble du monument, il n'y avait ni mobilier ni dépôts d'ossements humains calcinés.

En 1971 et 1973, les travaux sont poursuivis par Cl. Chauchat sur le n°1, [@chauchatNecropolePrehistoriqueCol1977, p. 323], dont il achève de dégager la totalité du péristalithe ainsi que sur le *cromlech n°2* tangent au précédent qui laisse apparaître une petite partie de sa couronne et son caisson central, à grand axe Ouest Nord-Ouest Est Sud-Est, constitué d'une dalle reposant sur 4 montants verticaux, et sans autre dépôt que des particules de charbons de bois qui ont pu être datées : (Ly 881) mesure d'âge : 2380+-130 (BP ), soit en date calibrée (BC) : 800-165. Le caisson n°5 est dégagé, mesurant environ 0,90m x 0,90m, formé d'une dalle horizontale reposant sur 5 petits montants verticaux ; pas de mobilier, ni charbons de bois, ni dépôts osseux. Le *cromlech n°6* a fait l'objet d'une fouille complète : une couronne de dalles de [grès]{.subject} empilées les unes sur les autres, inclinées vers l'extérieur, réalisant une murette de pierres sèches de 4m de diamètre et 0,50m de large, entoure un caisson central. Ce dernier est formé d'un petit cercle de 1,20m de diamètre constitué de petites dalles plantées obliquement s'appuyant sur une dalle de [couverture]{.subject} horizontale. Pas de mobilier ni de dépôts d'aucune sorte.

Le *cromlech n°7*, tangent au Sud-Ouest du précédent, n'a été que très partiellement dégagé, mettant au jour une petite portion d'un péristalithe mesurant environ 3m de diamètre, constitué de dalles empilées de façon désordonnée, et un coffre central rectangulaire de1,10m de long et 0,50m de large à grand axe orienté Nord-Est Sud-Ouest, avec une dalle de [couverture]{.subject} reposant sur 4 dalles verticales ; ni mobilier, ni dépôts.

Tous ces monuments, après leur fouille, ont été laissés à l'air libre, à la vue de tous et ils ont donc été considérablement dégradés dans les mois suivants, non seulement par les intempéries, mais en servant aussi bien de foyer de camping que de dépotoirs pour les promeneurs ou l'armée en manœuvre...

Devant un tel massacre du patrimoine archéologique, nous avons décidé, avec l'accord du Directeur de Antiquités #### Historiques d'Aquitaine, de procéder à l'avenir, après toute fouille, au recouvrement total des monuments.

En 1979, nous reprenons les fouilles de sauvetage, cette fois sur le monument contenant le *caisson n°5*, que nous fouillons totalement [@blotCromlechUniteMeatse1979]. Tout autour du caisson apparaît un véritable pavage de dalles disposées horizontalement, délimité à sa périphérie par une petite murette circulaire, continue, de 6m de diamètre et 1m de largeur, formée de nombreuses petites dalles de [grès]{.subject} sans ordre apparent ou soigneusement empilées et inclinées, suivant les endroits, rappelant la disposition constatée au cromlech n°6. À l'extérieur de cette murette gisaient sur le sol, à intervalles réguliers, de grandes dalles mesurant 0,50m x 1m et paraissant avoir été initialement plantées verticalement, comme l'était encore l'une d'entre elles ; la dénomination de tumulus-cromlech n°5 nous a paru dès lors totalement justifiée.

En 1992 et 1993 nous fouillons en totalité le *Cromlech n°8*, dont la très belle architecture a été partiellement endommagée par un engin de terrassement [@blotCromlechUniteMeatse1979, p.115]. La couronne périphérique mesure 4,30m de diamètre et elle est constituée de petits amas de dallettes horizontales, séparés les uns des autres par des dalles verticales disposées de façon radiale. Au centre se trouve un coffre dont la dalle de [couverture]{.subject} repose sur 7 montants verticaux ; de petites dallettes rendent ce réceptacle « étanche », tandis que de nombreuses autres dalles sont disposées en « pelure d'oignon » autour de ce coffre, plus à titre décoratif, semble-t-il, que de soutien. Aucun mobilier ni dépôts osseux, mais de nombreux petits amas de charbons de bois disséminés dans et hors le coffre central nous ont permis d'avoir une datation au 14C : Gif 9573 : mesure d'âge : 2960+-50 BP, soit en date calibrée (BC.). :1313-1004. Lors de la fouille nous avons pu constater la présence d'un cercle tangent au Sud Sud-Est (le futur n°11).

En 1994, nous intervenons sur le *Cromlech n°12* [@blotCromlechMeatse121996a, p.167], dégradé par le passage des tous-terrains, et dont la structure est, elle aussi, très élaborée : il mesure 5m de diamètre, et sa couronne externe, ou péristalithe, est formée de 2 types d'éléments : un cercle interne de petites dalles groupées en amas distincts mais tangents les uns aux autres, et un cercle externe, discontinu, de grandes dalles disposées tangentiellement aux précédentes. Au centre, un coffre à grand axe orienté Sud-Ouest Nord-Est, constitué d'une belle dalle de [couverture]{.subject} reposant sur 4 montants ; il est complètement stérile ; aucune datation n'a pu être effectuée. Disposés sur les dalles du péristalithe, nous avons recueilli un outil, façon «chopping-tool» qui a pu servir à épanneller les dalles de ce monument, et un galet de poudingue en forme d'œuf. Nous voyons, dans ces dépôts, et tout particulièrement dans le dernier, un geste symbolique par excellence car, pour beaucoup d'ethnologues, l'œuf est un symbole de la vie future, celle qu'il recèle en lui à l'état potentiel.

En 1994 encore, une prospection électrique (étude de la résistivité des sols), est effectuée par Mr Martinaud, à la recherche de monuments rendus invisibles par les colluvions issues des deux sommets encadrant le col. Des « anomalies électriques », (auxquelles nous attribuons des lettres majuscules), vont ainsi faire l'objet de sondages et parfois de fouilles plus complètes au cours des années suivantes, afin de déterminer si il s'agit de vestiges d'origine anthropique ou d'éléments naturels.

En 1995, nous effectuons ainsi des sondages [@blotRapportSondagesArcheologiques1995] dont un au voisinage immédiat du *cromlech n°8*, sur *C 11*, qui confirme bien l'existence d'un autre cercle, et aux points **A**, **X1**, **Q**, **U**. (voir plan). Le sondage au point **U** révèle une structure de dallettes empilées en position horizontale, l'ensemble formant un arc de cercle pouvant être interprété comme la fraction d'un cercle de pierres de 7m de diamètre environ. Au point **Q**, il existe une grande dalle de [couverture]{.subject} reposant en partie sur des montants verticaux, le tout très difficile à interpréter : on peut supposer qu'il y a 1 ou peut-être 2 caissons, dont l'un possède une grande dalle de [couverture]{.subject}, et une fraction de cercle de pierres faisant partie de cet ensemble, à l'évidence d'origine anthropique.

En 1996, nous effectuons la fouille complète du *cromlech n°11* [@blotBaratzCerclePierres2002, p.81], tangent au *n°8*, qui met en évidence un cercle de 4m environ de diamètre, une architecture semblable à celle de son voisin, et aussi soignée, avec amas de dallettes horizontales séparées par des dalles verticales ; deux galets de poudingue, en forme d'œuf, ont été recueillis entre et sur les dalles de ce péristalithe. Le caisson central, rectangulaire, à grand axe Nord Nord-Ouest, Sud Sud-Ouest mesurant 1,15m x 1m, n'est formé que par une dalle de [couverture]{.subject} reposant sur 4 montants verticaux, sans dallettes ou blocs complémentaires de calage. Quelques petits dépôts de charbons de bois avaient été effectués dans et hors le coffre central ; nous les avons recueillis pour datation. Résultat : Gif 10284 : mesure d'âge : 2705+- 75 (BP), soit en date calibrée (BC) : 1041-605. Il est donc plus récent que son voisin, ce que les modifications architecturales, au point de contact, avaient clairement évoqué.

Nous effectuons aussi des sondages [@blotRapportSondagesArcheologiques1996] aux points **A1**, **C**, **U5**, **T**, **S**, **P**. Le point **U5** révèle un coffre probable (centre d'un cromlech ?), avec des charbons de bois. En **S**, on note un volumineux bloc de [grès]{.subject} de 1,10m de large, de part et d'autre duquel s'appuient deux dalles verticales, avec présence de charbons de bois... pourrait-il s'agir d'un monolithe couché (?).

En 1997, les sondages [@blotCromlechMeatseCommune1997] portent sur les points **E**, **F**, **G**, **H**, **D**, **I**, **J**, **L**, et **L**. Les points **H** et **L** représentent deux cercles très probables. Le point **E** correspond au péristalithe du caisson du cromlech n°2, péristalithe déjà partiellement dégagé par Cl. Chauchat en 1973. On note une structure en dalles de [grès]{.subject} rose, présentant des traces d'épannelage, empilées les unes sur les autres d'une façon assez désordonnée, et sans dalles verticales ; le cercle ainsi délimité a 4,50m de diamètre.

Le point **F** correspond à l'environnement du *caisson n°4*, que nous avions fouillé en 1970. On dégage ainsi un amoncellement de dalles de [grès]{.subject} rose, disposées tout autour du caisson de manière plus ou moins désordonnée, le tout correspondant parfaitement à un tumulus pierreux, dès lors appelé *Tumulus n°4*.

En 1998, nous pratiquons des sondages étendus sur les deux derniers points cités [@blotSondagesArcheologiquesAu1998] :

Le point **H** correspond bien au *cromlech n°3*, dont on voit parfaitement le péristalithe en secteur Nord Nord-Est et Sud Sud-Ouest, du même type que celui du *cromlech n°8*, c'est à dire une alternance d'amas de petites dalles horizontales, séparées par des dalles verticales en position radiale ; ce cercle mesure 5m de diamètre. Le caisson central, mesurant 1,30m x 0,90m, à grand axe Est-Ouest ; il est constitué par une dalle de [couverture]{.subject} reposant sur 4 blocs parallélépipédiques, avec de nombreux petits blocs de calage ; pas de mobilier ni de fragments osseux calcinés, seulement un léger semis de particules de charbons de bois à l'intérieur du monument, mais qui n'ont pas pu permettre de datation.

Le point **L** met en évidence l'architecture d'un autre cercle avec son petit coffre central, monument que nous dénommons *cromlech n°13*. Le péristalithe, dont le diamètre atteint 4,90m, est, là encore, formé par des assemblage de dallettes horizontales séparées par d'autres verticalement disposées ; le caisson central, mesurant 1,10m x 0,75m, à grand axe Nord-Ouest Sud-Est, présente une dalle couvercle reposant sur 4 autres dalles verticales, sans petits blocs de calage, ni dépôts osseux ; par contre il existait un semis de particules carbonées à tous les niveaux du monument et du coffre ; la datation au 14 C nous donne : Gif 11091 : mesure d'âge: 2640+- 70 ans (BP), ce qui place ce monument à une époque plus récente que *Méatsé 8* et *Méatsé 11*, mais plus ancienne que *Méatsé 2* (voir aussi le tableau des datations en fin de texte). Enfin un petit galet de poudingue, de la forme et de la taille d'un œuf, avait été disposé entre péristalithe et coffre central, en secteur Est Sud-Est.

Ce bref bilan des travaux effectués à Méatsé permet de comprendre que nous ayons pu faire une certaine approche du symbolisme sous-jacent au rite d'incinération [@blotContributionEtudeCercles1995] et [@blotMessageArchitecturesProtohistoriques2003, p.45]. Tout d'abord, il est particulièrement perceptible dans le contraste entre la modicité des vestiges visibles au-dessus de la surface du sol - ceci même immédiatement après la construction du monument - et la complexité réelle de ces architectures. Par ailleurs, celles-ci sont très riches en témoins de gestes symboliques - que nous constatons, même sans pouvoir en expliquer les motivations - tels les semis de particules carbonées dans l'ensemble du monument, les dépôts de charbons de bois dans ou hors de la [ciste]{.subject} centrale, ou les dépôts de galets en forme d'œuf sur la couronne extérieure, tout ceci coexistant avec une absence de mobilier en général, et surtout avec l'absence (ou l'extrême discrétion) de restes humains. On a évoqué la possibilité que les restes osseux aient été détruits par l'acidité du sol. Il n'en est pas toujours ainsi, car les charbons de bois neutralisent cette acidité, et nous avons par exemple trouvé des fragments de côtes, éléments particulièrement fragiles, qui avaient été protégés par un environnement de charbons de bois (Errozaté) ; inversement, il nous est souvent arrivé de ne pas trouver de restes osseux au milieu d'abondants dépôts carbonés...

Cette absence, voulue, est la preuve que ces monuments funéraires sont plus des « cénotaphes », que des sépultures vraies (le cas unique du *tumulus-cromlech Millagate 4*, à Lecumberry, contenant la **totalité** des ossements incinéré d'un défunt, reste une exception tout à fait remarquable).

</section>

<section class="monument">

### [Itxassou]{.spatial} - [Méatsekobizkarra]{.coverage} 5 - [Cromlech]{.type}

#### Localisation

Carte 1345 Ouest.

Altitude : 750m.

Trois cromlechs du groupe Méatsékobizkarra ont déjà été décrits [@barandiaranContribucionEstudioCromlechs1949, p. 202] :

-   Le *Cromlech n°1*, contenant la borne Frontière n°80 en son centre, est le plus spectaculaire.

-   Le *Cromlech n°2*, est tangent à l'Ouest Sud-Ouest du n°1.

-   Le *Cromlech n°3*, un tumulus-cromlech, est à 8m à l'Ouest du n°1.

-   Nous avons même publié en 1972 [@blotNouveauxVestigesMegalithiques1972a, p.20] un quatrième monument à 3m à l'Ouest Sud-Ouest du n°2.

-   Le *Cromlech n°5* est situé à 38m à l'Ouest du n°1, sur un sol en très légère pente.

#### Description

Cercle bien visible de 3,70m de diamètre, délimité par 8 pierres.

#### Historique

Monument découvert en [septembre 1972]{.date}.

</section>



<section class="monument">

### [Itxassou]{.spatial} - [Méatsekobizkarra]{.coverage} 2- [Monolithe]{.type}

#### Localisation

Altitude : 750m.

Le monolithe se trouve à une quinzaine de mètres au Nord de la piste qui descend vers Bidarray, à environ 130m après sa naissance de la route qui monte au sommet de l'Artzamendi (après le col de Méatsé).

#### Description

Il s'agit d'une grande dalle de [grés]{.subject} [triasique]{.subject}, gisant au sol, affectant la forme d'une pointe de lance losangique, à grand axe orienté Nord Nord-Ouest Sud Sud-Est, mesurant 4,20m de long, 1,40m de large et 0,25m d'épaisseur en moyenne. Sa surface supérieure est parfaitement lisse. Son bord Ouest présente des traces d'épannelage très nets ; on note en particulier un volumineux éclat enlevé à l'extrémité Sud de ce côté. Le bord Est se divise en un segment Sud-Est de 1,90 m de long et un autre, Nord-Est, de 2,50m de long.

#### Historique

Monument découvert par [Manteca Martinez A.]{.creator} en [mai 2010]{.date}.

</section>

<section class="monument">

### [Itxassou]{.spatial} - [Méatsékobizkarra]{.coverage} - [Pierre couchée]{.type}

#### Localisation

Altitude : 675m.

Elle est située à proximité de la BF 81.

#### Description

Pierre de forme allongée et plus ou moins pointue à ses extrémités, en partie enfouie dans le sol, selon un axe Est-Ouest. Une fois dégagée, elle mesure 2,70m de long ; sa face Sud mesure 0,77m de large - on note des traces d'épannelage très nettes à son extrémité Est et au bord inférieur de sa face Sud.

#### Historique

Pierre découverte par [Blot J.]{.creator} en [janvier 2012]{.date}.

</section>

<section class="monument">

### [Itxassou]{.spatial} - [Mendittipiko Bizkarra]{.coverage} - [Monolithe]{.type} ([?]{.douteux})

#### Localisation

Altitude : 750m.

On le voit à gauche de la piste qui se rend à Mendittipiko lepoa.

#### Description

Volumineuse dalle couchée, de forme grossièrement triangulaire, orientée selon un axe Nord-Sud. Elle mesure 3m de long, 1,92m à sa base, et 0,40m d'épaisseur. Il semble que sa partie la plus étroite, son « sommet » qui mesure 1,12m de large, présente, semble-t-il, des traces d'épannelage.

#### Historique

Dalle découverte par [J. Blot]{.creator} en [mars 2010]{.date}.

</section>

<section class="monument">

### [Itxassou]{.spatial} - [Mondarrain]{.coverage} Nord 1 - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 520m.

Il est situé sur le flanc Nord du Mondarrain et un peu au-dessus d'un petit col, à gauche et aux abords immédiats d'une piste qui monte vers le Mondarrain, partant de son flanc Est avant la maison Ithurrartia.

#### Description

Tertre asymétrique, de 4m de long, 3m de large et 1m de haut.

#### Historique

Monument découvert par [J. Blot]{.creator} en [avril 2013]{.date}.

</section>

<section class="monument">

### [Itxassou]{.spatial} - [Mondarrain]{.coverage} Nord 2 - [Tertre d'habitat]{.type}

#### Localisation

Il est situé à 38m au Nord et au-dessous de *Mondarrain Nord 1 - Tertre d'habitat*.

Altitude : 515m.

#### Description

Tertre de terre asymétrique, de forme oblonge de 6 à 8m de diamètre, moins élevé que *Mondarrain Nord 1 - Tertre d'habitat*.

#### Historique

Monument découvert par [J. Blot]{.creator} en [avril 2013]{.date}.

</section>

<section class="monument">

### [Itxassou]{.spatial} - [Mondarrain]{.coverage} Est (Groupe 1) - [Tertres d'habitat]{.type}

Cet ensemble de 6 tertres est situé au-dessus de la piste qui longe à l'horizontal le flanc Est du Mondarrain, sur un terrain en pente légère vers l'Est et irrigué par un petit riu.

-   *TH n°1* : Altitude : 600m. Tertre de terre de faible hauteur de 4m de diamètre.

-   *TH n°2* : situé à 1m à l'Est du précédent. Mesure 3,50m de diamètre ; faible hauteur.

-   *TH n°3* : situé à 8m au Nord-Ouest du précédent. Mesure 3,50m de diamètre ; de faible hauteur.

-   *TH n°4* : situé à 1m au Sud-Est du n° 2. Mêmes dimensions.

-   *TH n°5* : situé à 10m à l'Est Sud-Est du n°4. Mesure 4m de diamètre.

-   *TH n°6* : situé à 9m au Nord du précédent. Mesure 3,50m de diamètre.

</section>

<section class="monument">

### [Itxassou]{.spatial} - [Mondarrain]{.coverage} Est (Groupe 2) - [Tertres d'habitat]{.type}

-   *TH n°1* : Situé au flanc Est du Mondarrain, plus haut que la piste qui le longe à l'horizontal. Altitude : 600m. Tertre mixte (pierres et terre) de 3,50m de diamètre et de faible hauteur.

-   *TH n°2* : Situé à 7m à l'Est du précédent. Tertre mixte, sensiblement de mêmes dimensions que le précédent.

#### Historique

Tertres découverts par [Blot Jacques]{.creator} en [avril 2013]{.date}.

</section>

<section class="monument">

### [Itxassou]{.spatial} - [Mendittipiko Bizkarra]{.coverage} 2 - [Monolithe]{.type}

#### Localisation

Altitude : 869m.

Monolithe situé presque au sommet de l'Artzamendi, à gauche du dernier virage avant le sommet, où se trouve une petite aire de parking. De là part une piste pastorale vers le Sud-Ouest, sur un joli replat, et le monolithe se voit à 8m à droite de cette piste, à une quarantaine de mètres du point de départ.

#### Description

Dalle de [grès]{.subject} [triasique]{.subject} allongée sur le sol selon un axe Ouest Sud-Ouest Est Nord-Est, de forme parallélépipédique, mesurant 2,53m de long, 0,77m de large à son sommet Sud-Ouest, et 1,07m à sa base Nord-Est ; son épaisseur, appréciable à son bord Sud, est de 0,32m en moyenne.

Il semble qu'il y ait des traces d'épannelage sur ce bord Sud.

#### Historique

Monolithe découvert par [Blot J.]{.creator} en [octobre 2015]{.date}.

</section>

<section class="monument">

### [Itxassou]{.spatial} - [Mendittipiko Bizkarra]{.coverage} 3 - [Monolithe]{.type}

#### Localisation

Altitude : 850m.

Il se trouve tangent au Sud de la même piste pastorale que Mendittipiko Bizkarra 2 - Monolithe, qui se prolonge toujours vers le Sud-Ouest. Il gît à une quarantaine de mètres au Nord-Est d'un petit col où plonge cette piste.

#### Description

Dalle de [grès]{.subject} [triasique]{.subject} de forme approximativement triangulaire, à base Nord-Est.

Elle mesure 2,90m de long, 1,54m à sa base et 1,40m dans sa partie médiane. Son épaisseur est en moyenne de 0,25m. Elle présente semble-t-il des traces d'épannelage aux deux extrémités de son bord Nord-Ouest, à son sommet Sud-Ouest et à l'angle Sud-Est de sa base.

#### Historique

Monolithe découvert par [J. Blot]{.creator} en [octobre 2015]{.date}.

</section>

<section class="monument">

### [Itxassou]{.spatial} - [Méatseko Bizkarra]{.coverage} 2 - [Dalle couchée]{.type}

#### Localisation

Altitude : 734m.

Cette dalle se trouve à 41m à l'Est Nord-Est du grand cromlech ayant au centre la BF 81.

#### Description

Dalle de [grès]{.subject} [triasique]{.subject} couchée sur un sol en légère pente vers le Sud. Elle affecte la forme d'un triangle grossièrement isocèle dont le grand axe, orienté Sud-Est Nord-Ouest, mesure 1,70m de long.

La base Nord-Ouest mesure 0,93m de long, le côté Sud-Ouest : 1,80m ; le côté Nord-Est est en partie amputé vers la base. L'épaisseur moyenne de cette dalle est très régulière : aux alentours de 0,14m.

On note des traces d'épannelage tout le long de tout le bord Sud-Ouest ainsi qu'à sa jonction avec la base, ainsi qu'au sommet, à sa jonction avec le côté Nord-Est.

Cette dalle aurait pu avoir été taillée pour être, par exemple, un élément des cromlechs voisins...

#### Historique

Dalle découverte par [Blot J.]{.creator} en [août 2018]{.date}.

</section>

<section class="monument">

### [Itxassou]{.spatial} - [Ourezti]{.coverage} Nord - [Tumulus]{.type}

#### Localisation

Sur la ligne de crête du premier relief qui surgit après le col au Sud du Mondarrain.

Altitude : 680m.

#### Description

Petit tumulus de 2,60m de diamètre, de faible hauteur (0,20m...) mais présentant un agencement de pierres de calibres variés, qui ne laisse aucun doute quant à l'origine anthropique de cette construction.

#### Historique

Tumulus découvert par [Blot Jacques]{.creator} en [avril 2013]{.date}.

</section>

<section class="monument">

### [Itxassou]{.spatial} - [Ourezti]{.coverage} 1 - [Tumulus]{.type}

#### Localisation

Carte 1345 Ouest.

Altitude : 690m.

Il est situé sur le petit sommet au Nord-Est du pic Ourezti, et est tangent à la crête rocheuse qui le borde à l'Ouest.

#### Description

Tumulus pierreux de 6m de diamètre et 0,30m de haut, édifié sur un sol plat, fait de blocs de quartzite enfoncés dans le sol ; au centre, quelques dalles au ras du sol pourraient appartenir à la [chambre funéraire]{.subject}. Il semblerait que les pierres périphériques soient disposées en couronne, sans que l'on puisse parler de péristalithe.

#### Historique

Monument découvert en [mars 1971]{.date}.

</section>

<section class="monument">

### [Itxassou]{.spatial} - [Ourezti]{.coverage} 2 - [Tumulus]{.type}

#### Localisation

Il est à 8m au Nord-Est de *Ourezti 1 - Tumulus*.

#### Description

Tumulus pierreux de 6m de diamètre et 0,30m de haut présentant une légère excavation centrale dans laquelle 3 dalles, dont une atteint 1m de long, pourraient faire partie de la [chambre funéraire]{.subject}.

#### Historique

Monument découvert en [mars 1971]{.date}.

</section>

<section class="monument">

### [Itxassou]{.spatial} - [Plateau Vert]{.coverage} n°7 - [Cromlech]{.type}

#### Localisation

Altitude : 611m.

#### Description

Une vingtaine de pierres, au ras du sol, délimitent un cercle de 3m de diamètre ; il existe une pierre visible dans le centre qui présente une légère dépression.

#### Historique

Monument découvert par [J. Blot]{.creator} en [mars 2010]{.date}.

</section>

<section class="monument">

### [Itxassou]{.spatial} - [Plateau Vert]{.coverage} n°8 - [Cromlech]{.type}

#### Localisation

À 2m à l'Est de *Plateau Vert n°7 - Cromlech*.

#### Description

Une vingtaine de pierres au ras du sol délimitent un cercle de 2,50m de diamètre dont le centre est marqué par une dalle à plat, bien visible.

#### Historique

Monument découvert par [J. Blot]{.creator} en [mars 2010]{.date}.

</section>

<section class="monument">

### [Itxassou]{.spatial} - [Plateau Vert]{.coverage} n°9 - [Cromlech]{.type}

#### Localisation

À 2m à l'Est de *Plateau Vert n°8 - Cromlech*.

#### Description

Une dizaine de pierres périphériques délimitent un cercle bien visible de 4,70m de diamètre ; au centre, une structure en petites dalles forme un relief évoquant un deuxième cercle interne (?), de 2m de diamètre.

#### Historique

Monument découvert par [J. Blot]{.creator} en [mars 2010]{.date}.

</section>

<section class="monument">

### [Itxassou]{.spatial} - [Veaux (col des)]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 544m.

Il est situé juste à l'Est du parking aménagé au Col des Veaux, à la confluence des différentes routes à cet endroit.

#### Description

On note un relief de terrain à peu près circulaire de 8m de diamètre et environ 1m de haut, sur un terrain en très légère pente vers l'Ouest ; quelques pierres apparaissent à sa surface. Il est difficile de dire s'il s'agit d'un tumulus ou d'un tertre d'habitat.

#### Historique

Monument découvert par [J. Blot]{.creator} en [février 2010]{.date}.

</section>

# Jaxu

<section class="monument">

### [Jaxu]{.spatial} - [Ihitzgorri]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 555m.

Il se trouve à l'extrémité Ouest de la croupe dénommée Nabahandi ou Ihitgorri.

#### Description

Tumulus de pierres de 3m de diamètre érigé sur un filon rocheux. Sa hauteur varie entre une vingtaine de centimètres et une quarantaine, dans sa partie Nord où l'on a récemment rajouté des éléments d'épierrage, sur la structure d'origine. Il semble bien que l'on puisse distinguer un péristalithe dans sa moitié Nord.

#### Historique

Monument découvert en [avril 2012]{.date} par [P. Velche]{.creator}.

</section>

<section class="monument">

### [Jaxu]{.spatial} - [Ihitzgorri]{.coverage} - [Monolithe]{.type}

#### Localisation

Altitude : 503m.

Ce monolithe git à 0,50m au Sud Sud-Est de la piste qui parcourt la crête d'Ihitzgorri, et se trouve être la seule pierre notable des environs.

#### Description

Dalle de [grés]{.subject} local, allongée sur le sol suivant un axe Ouest Sud-Ouest Est Nord-Est, mesurant plus de 3m de long, 1,10m de large, de forme triangulaire à sommet Ouest. Cette dalle présente des traces d'épannelage tout le long deux ses deux bords.

#### Historique

Monolithe découvert par [P. Velche]{.creator} en [avril 2012]{.date}.

</section>

<section class="monument">

### [Jaxu]{.spatial} - [Ihitzgorri]{.coverage} 2 - [Tumulus]{.type}

#### Localisation

Altitude : 545m.

Il est situé à environ 140m à l'Est du tumulus *T 1* décrit dans le *Tome 5* de notre *Inventaire* [@blotInventaireMonumentsProtohistoriques2013, p.13].

#### Description

Tumulus pierreux plutôt ovale de 6m x 5m et 0,30m de haut. Paraît s'appuyer en partie sur un filon rocheux naturel ?

#### Historique

Tumulus découvert par [F. Meyrat]{.creator} en [août 2015]{.date}.

</section>

<section class="monument">

### [Jaxu]{.spatial} - [Ihitzgorri]{.coverage} 3 - [Tumulus]{.type}

#### Localisation

Altitude : 500m.

Il est situé dans un petit col immédiatement au Nord-Est et au pied de la colline Ihizgorri.

#### Description

Tumulus pierreux circulaire de 5m de diamètre et 0,30m de haut.

#### Historique

Tumulus découvert par [F. Meyrat]{.creator} en [août 2015]{.date}.

</section>

<section class="monument">

### [Jaxu]{.spatial} - [Nabahandi]{.coverage} - [Dalle taillée]{.type}, [dolmen]{.type} ([?]{.douteux})

#### Localisation

Altitude : 507m.

Dalle située vers l'extrémité Nord-Est de la petite colline du même nom.

Elle est située à une trentaine de mètres au Sud-Est de *Ihitzgorri - Monolithe*, et à une cinquantaine de mètres à l'Est du *Col de Ihizgorri 3 - Tumulus*.

#### Description

Dalle de [grés]{.subject} [triasique]{.subject}, plantée dans le sol, selon un axe Est Nord-Est Ouest Sud-Ouest (et fortement inclinée vers le Nord). Elle mesure 0,90m de long à sa base, 0,50m dans sa plus grande hauteur, et son épaisseur est en moyenne de 0,15m. Tout son pourtour est épannelé.

Dans son prolongement au Nord-Est, on note à une distance de 0,60m, une première pierre, et à 0,55m de celle-ci une seconde, toutes deux en grès rose, alors que tout l'environnement est formé de blocs et filons de calcaire blanc.

Il ne semble pas y avoir de tumulus autour de cette dalle dont la finalité pose problème : borne ? vestige d'un petit coffre dolménique démoli ?

#### Historique

Dalle découverte par [Blot J.]{.creator} en [octobre 2015]{.date}.

</section>

<section class="monument">

### [Jaxu]{.spatial} - [Sen Julian]{.coverage} - [Cromlech]{.type} ([?]{.douteux})

#### Localisation

Altitude : 479m.

Tout au sommet d'une petite colline, se dresse un calvaire un peu décentré à l'Est Sud-Est par rapport au centre d'un monument formé de deux cercles concentriques.

#### Description

Il semble que l'on puisse discerner 2 cercles concentriques constitués de pierres affleurant à peine à la surface du sol : le plus interne, de 4m de diamètre, posséderait une dizaine de pierres ; le plus externe, de 8m de diamètre est délimité par 15 pierres bien visibles au Nord Nord-Est.

#### Historique

Monument très douteux découvert par [F. Meyrat]{.creator} en [août 2015]{.date}.

</section>

# Juxue

<section class="monument">

### [Juxue]{.spatial} - [Méhalzu]{.coverage} 3 - [Tumulus]{.type}

#### Localisation

Altitude : 641m.

Il est situé à 200m environ à l'Est Nord-Est du sommet de Méhalzu.

#### Description

Tumulus circulaire de faible hauteur (0,30m), et de 4,50m de diamètre. Une trentaine de petites plaquettes calcaires délimitent sa périphérie, et sont particulièrement visibles dans sa partie Nord.

Ce qui nous semble particulièrement remarquable, c'est que ce modeste monument, qui évoque un petit tumulus à [incinération]{.subject}, a été construit sur le premier remblai de défense de l'enceinte de Méhalzu, qui lui est donc antérieur. Ce remblai à grand axe Nord-Sud barre (avec un second, plus haut et plus discret), toute la partie Est de l'enceinte de Méhalzu, la seule dont la faible pente permettrait un assaut plus aisé que sur les autres flancs, beaucoup plus pentus.

#### Historique

Tumulus découvert par [F. Meyrat]{.creator} en [juin 2015]{.date}.

</section>

<section class="monument">

### [Juxue]{.spatial} - [Oxaraniako Borda]{.coverage} Ouest - [Tumulus]{.type}

Ces monuments sont la suite de la description faite des 9 autres tumulus (numérotés de 1 à 9) au chapitre de la commune de Pagolle : *Oxaraniako Borda Est (ou Méhalzu ) - Tumulus*.

Ces 7 tumulus ont été découverts par [Meyrat F.]{.creator} en [novembre 2015]{.date}.

-   *Tumulus n°10* - Altitude : 472m.

    Tumulus mixte situé à 70m à l'Ouest du n°1 de la description des tumulus d'Oxaraniako borda Est. Il mesure 7m x 6m et 0,40m de haut.

-   *Tumulus n°11* - Altitude : 460m.

    Tumulus mixte situé à 45m au Nord-Ouest du n°10 ; il mesure 7m x 6m et 0,30m de haut.

-   *Tumulus n°12* - Altitude : 465m.

    Tumulus mixte circulaire, situé à 50m au Nord-Ouest du n°11 ; 5m de diamètre et 0,30m de haut.

-   *Tumulus n°13* - Altitude : 456m.

    Tumulus circulaire mixte, situé à 20m à l'Ouest Nord-Ouest du n°12. Diamètre de 6m et 0,40m de haut.

-   *Tumulus n°14* - Altitude : 451m.

    Tumulus circulaire mixte situé à 15m au Nord-Est du n°13 ; 5m de diamètre et 0,30m de haut.

-   *Tumulus n°15* - Altitude : 445m.

    Tumulus mixte, situé à 18m au Nord Nord-Ouest du n°14 ; mesure 9m x 6m, à grand axe Nord-Sud, et 0,30m de haut.

-   *Tumulus n°16* - Altitude : 450m.

    Tumulus mixte situé à 20m au Sud-Ouest du n°15 ; mesure 8m x6m et 0,40m de haut.

</section>

# Lacarry

<section class="monument">

### [Lacarry]{.spatial} - [Idagorria bas]{.coverage} Ouest - [Tertres d'habitat]{.type}

#### Localisation

Altitude : 965m.

Le plus net et le plus haut situé se voit en contre-bas de la route, (avant le virage vers Urrutchantzé), et les 3 autres sont à une vingtaine de mètres plus bas, au Sud-Est, en bordure du ruisseau.

#### Description

Le tertre le plus haut situé, de forme circulaire, asymétrique, mesure une quinzaine de mètres de diamètre et 1m de haut. Les 3 autres se sont effondrés au cours des temps, mais sont encore visibles.

#### Historique

Tertre découvert par [Blot J.]{.creator} en [2012]{.date}.

</section>

<section class="monument">

### [Lacarry]{.spatial} - [Olhaberria]{.coverage} Ouest 1 - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 980m.

Tertre situé entre le premier et le second relief de la crête de *Bostmendiette*.

#### Description

Tertre de terre circulaire, de 15m de diamètre et 3m de haut, asymétrique à grande pente Sud.

#### Historique

Tertre découvert pat [Blot J.]{.creator} en [1979]{.date} mais seulement cité, non décrit, sous la rubrique *Bostmendiette*.

</section>

<section class="monument">

### [Lacarry]{.spatial} - [Olhaberria]{.coverage} Centre 1 - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 980m.

Situé entre le deuxième et le troisième relief de la crête de *Bosmendiette*.

#### Description

Très semblable à *Olhaberria Ouest 1 - Tertre d'habitat* : tertre circulaire de terre de 15m de diamètre, et 2m à 3m de haut, asymétrique à grande pente Sud.

#### Historique

Tertre découvert par [Blot J.]{.creator} en [1979]{.date}, seulement cité sous la rubrique *Bostmendiette*.

</section>

<section class="monument">

### [Lacarry]{.spatial} - [Olhaberria]{.coverage} Est - [Tertres d'habitat]{.type}

#### Localisation

Altitude : 1000m.

La dizaine de tertres d'habitat est située au petit col où se trouvent les ruines du cayolar Olhaberria.

#### Description

Une dizaine de tertre, très proches les uns des autres, sont construits autour d'excavations (ou de [dolines]{.subject} ?). Ils sont circulaires, mesurent une dizaine de mètres de diamètre et un à deux de haut environ.

#### Historique

Tertres découverts par [Blot J.]{.creator} en [1979]{.date}, et seulement cités à la rubrique *Bostmendiette*.

</section>

<section class="monument">

### [Lacarry]{.spatial} - [Ugatze]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 1154m.

Il est situé sur une éminence qui domine le col d'Ugatze à l'Est.

#### Description

Tumulus de terre de 3,50m de diamètre et 0,30m de haut, partiellement amputé de son quart Nord par le passage de la piste de crête. On note, sur la coupe faite au Bulldozer 2 pierres à l'extrémité Ouest de la coupe, qui pourraient appartenir à un péristalithe...

#### Historique

Monument découvert en [mai 1989]{.date} par [Luis Millan]{.creator}.

</section>

<section class="monument">

### [Lacarry]{.spatial} - [Ugatze Lepoa]{.coverage} - [Cromlech]{.type} ou [Tumulus]{.type} ([?]{.douteux})

#### Localisation

Altitude : 1140m.

Il est en plein col, à 20m à l'Est du virage de la piste.

#### Description

Environ 13 pierres, au ras du sol délimitent un cercle de 3m de diamètre ; le secteur Nord en est démuni. Monument douteux.

#### Historique

Monument découvert par [Blot J.]{.creator} en [mai 2012]{.date}.

</section>

# Lanne

<section class="monument">

### [Lanne]{.spatial} - [Ayhautce]{.coverage} - [Cromlech]{.type}

#### Localisation

Altitude : 1227m.

Il est situé sur un replat à une trentaine de mètres au Nord de la route, au niveau d'un virage bien marqué.

#### Description

Nous avons déjà publié ce monument [@blotNouveauxVestigesMegalithiques1972a] mais il a, depuis, fait l'objet d'une fouille clandestine dont on voit la trace sous la forme d'une excavation centrale de 2m de large environ et 0,40m de profondeur.

On peut encore voir un cercle de 4,20m à 4,30m de diamètre formé d'une dizaine de pierres encore visibles, sur les 17 que nous avions notées en 1971. Peut-être certaines sont-elles enfouies sous la mousse... mais d'autres semblent bien avoir été éjectées du cercle. À noter que les 3 tertres d'habitat décrits en 1972 et dont le premier est à 20m au Nord du cromlech, n'ont subi aucune détérioration.

#### Historique

Monument découvert par [Blot J.]{.creator} en [1971]{.date}, et revisité en juillet 2016.

</section>

<section class="monument">

### [Lanne]{.spatial} - [Ayhautce]{.coverage} 4 et 5 Ouest - [Tertres d'habitat]{.type}

Nous devons signaler et ajouter un quatrième tertre d'habitat, aux 3 précédemment cités par nous en 1972 [@blotNouveauxVestigesMegalithiques1972a]. Il est le plus au Nord. Ils sont tous quatre situés à quelques mètres au Nord du précédent cromlech. Par ailleurs il existe un cinquième tertre d'habitat, *TH Ouest*, situé à environ 100m plus à l'Ouest, de l'autre côté d'un petit ruisseau.

</section>

<section class="monument">

### [Lanne]{.spatial} - [Coutchet de Planté]{.coverage} - [Tertres d'habitat]{.type} ([?]{.douteux})

#### Localisation :

Altitude : 1452m.

On note sur ce vaste pâturage de nombreuses petites éminences que l'on pourrait prendre pour de petits tertres d'habitats, ce que nous ne pensons pas (surtout si on les compare avec leurs voisins, les tertres de la Cabane de la Serre... juste en contre-bas de la route). Nous avons trouvé les semblables à Ste Engrace (Col de la Taillade). [Blot J.]{.creator}, [juillet 2016]{.date}.

</section>

<section class="monument">

### [Lanne]{.spatial} - [Couyalarou]{.coverage} - [Tertres d'habitat]{.type}

#### Localisation

Ces 3 tertres d'habitat sont situés à environ quatre cent mètres au-dessus et à l'Ouest Sud-Ouest des ruines de la cabane de Couyalarou, dont les vestiges des murs en pierres sont encore visibles.

#### Description

-   *TH 1* - Altitude : 1344m.

    Tertre de terre recouvert d'une végétation abondante, de forme ovale (12m x 10m et 0,60m de haut).

-   *TH 2* - Altitude : 1313m.

    Situé à 8m à l'Est du précédent. Tertre de forme ovale recouvert de végétation, mesurant 10m x 8m et 0,60m de hauteur.

-   *TH 3* - Altitude : 1311m.

    Situé à 6m à l'Est Sud-Est du précédent.

#### Historique

Tertres découverts par [Blot J.]{.creator} en [1978]{.date} et jamais publiés. Ils ont été revisités par [Meyrat F.]{.creator} en [août 2017]{.date}.

</section>

<section class="monument">

### [Lanne]{.spatial} - [Hournères]{.coverage} (col de) - [Cromlech]{.type}

#### Localisation

Altitude : 1475m.

Erigé dans le col lui-même, sur terrain plat.

#### Description

Cercle de 2m de diamètre délimité par 7 pierres de très faible hauteur ; il n'y a pas de tumulus mais un très faible bourrelet périphérique ; une légère dépression est visible en secteur Sud-Est. Deux témoins se détachent par leurs dimensions : en secteur Sud-Est, une pierre au ras du sol, mesurant 0,75m x 0,50m ; en secteur Sud-Ouest, une autre de 0,30m de haut et 0,60m x 0,50m.

#### Historique

Cercle découvert par [Meyrat F.]{.creator} en [décembre 2016]{.date}.

</section>

<section class="monument">

### [Lanne]{.spatial} - [Hournères]{.coverage} (ravin) - [Tertres d'habitat]{.type}

#### Localisation

Altitude : 1319m.

Les 2 tertres sont situés, sur la rive Ouest du ruisseau donnant naissance à celui empruntant le ravin de Hournères ; terrain en légère pente vers le Sud.

#### Description

-   *Tertre n°1* : le plus à l'Est, à une vingtaine de mètres à l'Ouest du ruisseau ; il mesure 5m x 5m et 0,60m de haut.

-   *Tertre n°2* : situé à 30m à l'Ouest du précédent, il mesure 6m x 5m et 0,30m de haut.

#### Historique

Monuments découverts par [Meyrat F.]{.creator} en [décembre 2016]{.date}.

</section>

<section class="monument">

### [Lanne]{.spatial} - [Isiburie]{.coverage} Nord - [Tertres d'habitat]{.type}

#### Localisation

Altitude : 1324m.

Cet ensemble de 3 tertres et plus est situé sur un terrain en légère pente vers l'Est.

#### Description

-   *Tertre n°1* : il est situé à 30m environ au Nord des ruines bien visibles d'une borde située en bordure Est de la ligne de crête ; à peu près circulaire de 9m de diamètre, il mesure 0,80m de haut.

-   *Tertre n°2* : situé à 3m au Nord du précédent, mesure 9m x 8m et environ 1m de haut.

-   *Tertre n°3* : situé à 25m environ au Nord Nord-Ouest du précédent, mesure 8m x 7m et 0,60m de haut.

-   *Autres tertres* : la végétation a rendu difficile le comptage de ces tertres situés à l'Ouest Nord-Ouest, une quarantaine de mètres plus bas sur la pente.

#### Historique

Tertres découverts par [Meyrat F.]{.creator} en [décembre 2016]{.date}.

</section>

<section class="monument">

### [Lanne]{.spatial} - [Isiburie]{.coverage} Sud - [Tertres d'habitat]{.type}

#### Localisation

Altitude : 1343m.

Situés sur la crête qui sépare les deux ruisseaux donnant naissance à l'Arrec d'Isiburie ; les 4 tertres sont peu visibles (faible hauteur) et érigés sur un terrain en légère pente vers le Sud-Est.

#### Description

-   *Tertre n°1* : mesure 6m x 4m et 0,30m de haut.

-   *Tertre n°2* : il est quasi tangent au Sud du précédent ; mesure 6m x 5m et 0,40m de haut.

-   *Tertre n°3* : Situé à 10m à l'Est du précédent ; mesure 6m x 6m et 0,20m de haut.

-   *Tertre n°4* : situé à 3m au Sud du précédent ; il mesure 6m x 6m et 0,20m de haut. Il est à une trentaine de mètres au Nord-Ouest de l'Arrec (ruisseau).

#### Historique

Tertres découverts par [Meyrat F.]{.creator} en [décembre 2016]{.date}.

</section>

<section class="monument">

### [Lanne]{.spatial} - [Lacurde (col de)]{.coverage} - [Cromlech]{.type}

#### Localisation

Altitude : 1336m.

Il est sur terrain plat, au niveau même du col, au pied du mont Iguntze.

#### Description

Sur les trois éléments repérés, nous dirons qu'un seul cercle de pierres nous paraît le plus plausible, les deux autres paraissant plus être des éléments naturels que des artefacts.

-   *Cercle n°1* : de 1,10m de diamètre, délimité par une dizaine de pierres de petit calibre, ne dépassant que de peu le sol ; l'une d'entre elles, plus importante est située au Nord et mesure 0,29m x 0,28m et 0,20m de haut. Au centre apparaissent 4 autres petites pierres.

-   Le *Cercle (?) n°2* : situé à 4m à l'Est du précédent, mesurerait 1,10m de diamètre, délimité par 5 pierres (en demi-cercle).

-   Le *Cercle (?) n°3* est à 4m au Nord-Est du n°1 et à 24m au Nord du n°2 ; il serait délimité par 4 pierres, dont une de 0,70m x 0,68m et 0,40m de haut, contrastant avec les 3 autres peu visibles.

#### Historique

Éléments découverts par [Meyrat F.]{.creator} en [octobre 2016]{.date}.

</section>

<section class="monument">

### [Lanne]{.spatial} - [Lacurde (col de)]{.coverage} - [Tertres d'habitat]{.type} ([?]{.douteux})

#### Localisation

Altitude: 1338m.

Ces trois probables tertres d'habitat sont situés dans le secteur Nord-Est du col de Lacurde, à une trentaine de mètres environ au Nord-Est de *Lacurde (col de) - Cromlech*.

#### Description

-   *TH 1* : il est situé à 2m à l'Est des ruines d'une borde, dont seuls subsistent les fondations. Tertre de terre allongé de faible hauteur (0,40m), mesurant 15m x 6m.

-   *TH 2* : situé à 2m à l'Ouest Sud-Ouest des mêmes ruines ; tertre de très faible hauteur (0,15m), mesurant 7m x 4m.

-   *TH 3* : Situé à 20m au Nord-Ouest de *TH 1* et à 9m au Nord de *TH 2* ; il mesure 0,30m de haut et 10m x 9m.

#### Historique

Ces monuments, douteux car difficiles à identifier étant donnée leur faible hauteur, ont été trouvés par [Meyrat F.]{.creator} en [Novembre 2016]{.date}.

</section>

<section class="monument">

### [Lanne]{.spatial} - [Pâturages de Montory]{.coverage} - [Tertres d'habitats]{.type}

#### Localisation

Altitude : 1097m.

L'ensemble de ces tertres (plus de 20) se trouve réparti en deux groupes distants d'une cinquantaine de mètres sur un vaste pâturage en pente douce vers le Nord, et sont entourés de part et d'autre par deux petits ruisseaux de montagne.

#### Description

-   *Le Groupe Nord* comporte au moins une quinzaine de tertres terreux de forme ovale, mesurant en moyenne 10m x 4m et 0,60m de haut.

-   *Le Groupe Sud*, riche de treize tertres ou plus (la végétation abondante rend difficile leur repérage exact), ont, pour beaucoup les mêmes dimensions que ceux du groupe sud ; toutefois certains peuvent atteindre 10m x 6m et 0,60m de haut, ou 10m x 8m et 0,80m de haut et même 15m x 4m et 0,60m de haut ; enfin l'un d'eux, le plus au Sud, atteint 22m x 8m et 0,40m de haut.

#### Historique

Ces tertres ont été identifiés par [J. Blot]{.creator} en [1978]{.date}, mais n'ont jamais été publiés, les documents ayant été égarés ; ils ont été revisités en [octobre 2016]{.date} par [F. Meyrat]{.creator}.

</section>

<section class="monument">

### [Lanne]{.spatial} - [Sulatcé]{.coverage} - [Cromlech]{.type}

#### Localisation

Altitude : 1342m.

Il est situé sur un replat qui domine un virage « en épingle à cheveu » de la route.

#### Description

Il semble bien qu'on puisse distinguer un cercle de 4,50m de diamètre, délimité par environ 6 pierres. Ce très probable monument a été remanié, (quand ?, par qui ?, pourquoi ?) de sorte que certaines pierres de la périphérie ont pu être enlevées, et que la [ciste]{.subject} centrale a été en partie mise au jour...

#### Historique

Monument découvert en [juillet 2016]{.date} par [Blot J.]{.creator}.

</section>

# Lantabat

<section class="monument">

### [Lantabat]{.spatial} - [Bertugaine]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 400m.

Il est au point culminant de la colline, au Nord d'un réservoir d'eau en ciment qui lui est tangent au Sud. La piste passe sur son sommet.

#### Description

Tumulus circulaire de terre et de pierres, de 15m de diamètre et 0,60m de haut ; il semble bien qu'un filon rocheux, orienté Sud-Nord, ait été inclus dans le monument, mais un aménagement a été fait pour rendre ce tumulus parfaitement circulaire.

#### Historique

Monument découvert par [Blot J.]{.creator} en [novembre 1975]{.date}.

</section>

<section class="monument">

### [Lantabat]{.spatial} - [Cote 395]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 395m.

Ce tumulus est érigé au sommet de la colline située au Nord-Est du col Gagnekolepoa, à 4m au Sud-Ouest de la clôture barbelée qui s'étend selon un axe Nord-Ouest Sud-Est.

#### Description

Tumulus herbeux circulaire, très érodé, de 5,30m de diamètre semble-t-il, et 0,25m de haut, sur sol plat. Il fait face au tumulus d'Haranbeltz sur la colline visible au Nord-Est.

#### Historique

Monument découvert par [J. Blot]{.creator} en [avril 2013]{.date}.

</section>

<section class="monument">

### [Lantabat]{.spatial} - [Donapétria]{.coverage} - [Tertre pierreux]{.type}

#### Localisation

Altitude : 310m.

Situé au flanc Est de la colline *Arxilako Kaskoa*, au milieu d'un vaste replat et tangent au Nord à la piste pastorale qui traverse ce plateau.

#### Description

Petit tertre pierreux circulaire (Tumulus ?) en forme de galette aplatie, mesurant 5m de diamètre et 0,40m de haut.

#### Historique

Monument découvert par [Blot J.]{.creator} en [1971]{.date}.

</section>

<section class="monument">

### [Lantabat]{.spatial} - [Gagnekolepoa]{.coverage} - [Tumulus]{.type}

#### Localisation

Au milieu du col de ce nom, à quelques mètres à l'Est de la piste qui le contourne.

Altitude : 380m.

#### Description

Vaste tumulus essentiellement pierreux, de 14m de diamètre environ et de 1m de haut.

#### Historique

Tumulus découvert par le [groupe Hilarriak]{.creator} en [mars 2013]{.date}.

Nous avons aussi noté, à quelques dizaines de mètres plus loin à l'Est, en montant vers la colline, un structure d'origine anthropique mal définissable, sur la droite de la piste...

</section>

<section class="monument">

### [Lantabat]{.spatial} - [Gagnekolepoa]{.coverage} 1 et 2 - [Tertres pierreux]{.type}

#### Localisation

Ces deux monuments, tangents, sont aussi tangents au bord Ouest de la piste.

Altitude : 385m.

#### Description

-   *Tp 1* : tertre pierreux tangent à la piste, le plus au Nord, mesure 5m de diamètre et 0,30m de haut.

-   *Tp 2* : le plus au Sud, pierreux, mesure 3m de diamètre et 0,15m de haut.

#### Historique

Tertres découverts par le [groupe Hilarriak]{.creator} en [mars 2013]{.date}.

</section>

<section class="monument">

### [Lantabat]{.spatial} - [Gagnekolepoa]{.coverage} 3 - [Tertre pierreux]{.type}

#### Localisation

Il est situé à 16m à l'Est de *Gagnekolepoa 1 et 2 - Tertres pierreux*, sur le flanc de la colline.

Altitude : 387m.

#### Description

Tertre pierreux de 3m de diamètre et 0,30m de haut

#### Historique

Tertre découvert par [J. Blot]{.creator} en [avril 2013]{.date}.

</section>

<section class="monument">

### [Lantabat]{.spatial} - [Iratze gorriko lepoa]{.coverage} Sud - [Tertre pierreux]{.type}

#### Localisation

Sur la pente qui domine au Sud le col de ce nom.

Altitude : 371m.

#### Description

Tertre pierreux de 6m de diamètre, quelques centimètres de haut, érigé sur terrain en pente légère vers le Nord.

#### Historique

Tertre vu par [J. Blot]{.creator} en [1975]{.date} et revisité par lui en [2013]{.date}.

</section>

<section class="monument">

### [Lantabat]{.spatial} - [Iratze gorriko lepoa]{.coverage} Est - [Tertre pierreux]{.type}

#### Localisation

Il est plus bas que *Iratzegorriko lepoa Sud - Tertre pierreux* et à 18m au Nord-Est de lui.

Altitude : 368m.

#### Description

Tertre pierreux sur terrain en légère pente, mesurant 6m de diamètre et 0,30m de haut.

#### Historique

Tertre vu par [J. Blot]{.creator} en [1975]{.date} et revisité par lui en [2013]{.date}.

</section>

<section class="monument">

### [Lantabat]{.spatial} - [Iratze gorriko lepoa]{.coverage} Ouest - [Tertre pierreux]{.type}

#### Localisation

À 20m à l'Ouest Nord-Ouest de *Iratze gorriko lepoa Est - Tertre pierreux* et à 18m de *Iratzegorriko lepoa Sud - Tertre pierreux*.

Altitude : 368m.

#### Description

Tertre pierreux de 6m de diamètre net quelques centimètres de haut

#### Historique

Tertre vu par [J. Blot]{.creator} en [1975]{.date} et revisité par lui en [2013]{.date}.

</section>

<section class="monument">

### [Lantabat]{.spatial} - [Laparzale]{.coverage} Sud 1 - [Tertre pierreux]{.type}

#### Localisation

Altitude : 353m.

#### Description

Tertre pierreux de 6m de diamètre et 0,30m de haut.

#### Historique

Tertre vu par [J. Blot]{.creator} en [1975]{.date} et revisité par lui en [2013]{.date}.

</section>

<section class="monument">

### [Lantabat]{.spatial} - [Laparzale]{.coverage} Sud 2 - [Tertre pierreux]{.type}

#### Localisation

Altitude : 353m.

À 20m à l'Ouest Nord-Ouest de *Laparzale Sud 1 - Tertre pierreux* et à 45m à l'Est Sud-Est du *Grand Tumulus*.

#### Description

Tertre pierreux de 4m de diamètre et quelques centimètres de haut.

#### Historique

Tertre vu par [J. Blot]{.creator} en [1975]{.date} et revisité par lui en [2013]{.date}.

</section>

<section class="monument">

### [Lantabat]{.spatial} - [Laparzale]{.coverage} Sud 3 - [Tertre pierreux]{.type}

#### Localisation

Altitude : 353m.

Situé à 20m à l'Ouest du *Grand Tumulus*.

#### Description

Tertre pierreux de 6m de diamètre et quelques centimètres de haut.

#### Historique

Tertre vu par [J. Blot]{.creator} en [1975]{.date} et revisité par lui en [2013]{.date}.

</section>

<section class="monument">

### [Lantabat]{.spatial} - [Lindugnekolepoa]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 392m.

Il est à l'extrémité Ouest de la colline Bertugaine, dominant ainsi le col Lindugnekolepoa.

#### Description

Tumulus circulaire de terre et de pierres de 14m de diamètre et 0,90m de haut.

#### Historique

Monument découvert par [Blot J.]{.creator} en [novembre 1975]{.date}.

</section>

<section class="monument">

### [Lantabat]{.spatial} - [Pagaburu]{.coverage} 1 - [Tumulus]{.type}

#### Localisation

Altitude : 437m.

Dans un petit replat qui suit la ligne de crête dénommée « Pagaburu » et qui précède la cote 440. Il se trouve au Sud-Est de la route carrossable menant aux postes d'hébergement des chasseurs. Situé près d'un poste de tir, en bordure de la piste ascendante vers la crête Pagaburru.

#### Description

Tumulus circulaire de terre et de pierraille, mesurant 7m de diamètre et 0,40m de haut ; il est parfaitement intact.

#### Historique

[Nous]{.creator} avions brièvement signalé ces 3 tumulus dans la publication suivante : [@blotTumulusBixustiaZuhamendi1976, p.115]. Nous avons revu ces monuments en avril 2015, et les décrivons ici.

</section>

<section class="monument">

### [Lantabat]{.spatial} - [Pagaburu]{.coverage} 2 - [Tumulus]{.type}

#### Localisation

Il se trouve au Sud-Est de la route carrossable menant aux postes d'hébergement des chasseurs.

Il est situé à une quarantaine de mètres au Sud-Est de *Pagaburu 1 - Tumulus*, et a été remanié en partie dans sa périphérie Sud-Ouest par la création d'un poste de tir.

#### Description

Tumulus de terre et de pierraille, qui fut circulaire, (mais remanié au Sud-Ouest), mesurant environ 7m de diamètre et 0,30m de haut.

#### Historique

[Nous]{.creator} avions brièvement signalé ces 3 tumulus dans la publication suivante : [@blotTumulusBixustiaZuhamendi1976, p.115]. Nous avons revu ces monuments en avril 2015, et les décrivons ici.

</section>

<section class="monument">

### [Lantabat]{.spatial} - [Pagaburu]{.coverage} 3 - [Tumulus]{.type}

#### Localisation

Il se trouve au Sud-Est de la route carrossable menant aux postes d'hébergement des chasseurs.

Il est à 25m au Nord de *Pagaburu 2 - Tumulus* et à 15m à l'Est de *Pagaburu 1 - Tumulus*.

#### Description

Tumulus circulaire de terre et de pierraille, bien visible, mesurant 10m de diamètre et 0,40m de haut.

#### Historique

[Nous]{.creator} avions brièvement signalé ces 3 tumulus dans la publication suivante : [@blotTumulusBixustiaZuhamendi1976, p.115]. Nous avons revu ces monuments en avril 2015, et les décrivons ici.

</section>

<section class="monument">

### [Lantabat]{.spatial} - [Pagaburu]{.coverage} 1 - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 468m.

On peut le voir longé au Nord-Ouest et au Sud-Ouest par les 2 pistes ascendantes, à quelques dizaines de mètres plus au Sud-Est du *tumulus T2*.

#### Description

Tertre de terre, asymétrique mesurant 12m de long, 8m de large et 0,40m de haut ; en grande partie recouvert par un roncier.

#### Historique

Monument découvert par [F. Meyrat]{.creator} en [avril 2015]{.date}.

</section>

<section class="monument">

### [Lantabat]{.spatial} - [Pagaburu]{.coverage} 2 - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 472m.

Il est situé à 65m au Nord-Ouest de *Pagaburu 1 - Tertre d'habitat* et, comme lui, bordé par les 2 pistes ascendantes.

#### Description

Tertre de terre, asymétrique, mesurant 8m de long, 6m de large et 0,50m de haut.

#### Historique

Monument découvert par [F. Meyrat]{.creator} en [avril 2015]{.date}.

</section>

<section class="monument">

### [Lantabat]{.spatial} - [Pentzézaharreko Ithurria]{.coverage} - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 537m.

Il est situé sur un terrain en légère pente vers l'Est, et à une quinzaine de mètres à l'Ouest d'une piste pastorale.

#### Description

Tertre dissymétrique discret, de 6m de diamètre et 0,40m de haut.

#### Historique

Monument découvert par [F. Meyrat]{.creator} en [novembre 2014]{.date}.

</section>

# Larceveau-Arreau-Cibits

<section class="monument">

### [Larceveau-Arreau-Cibits]{.spatial} - [Arapidekoborda]{.coverage} 1 - [Tumulus]{.type}

#### Localisation

Altitude : 374m.

Monument situé à mi-pente de la croupe qui se détache au Sud-Est du mont Currutcheberry, sur un terrain en très légère pente Sud-Est, à 2m au Nord-Est d'une piste pastorale. Il est à 25m au Nord-Ouest d'un abreuvoir de construction récente.

#### Description

Tumulus circulaire pierreux de 10m de diamètre environ et 0,30m de haut environ. De nombreux petits blocs [calcaires]{.subject} sont visibles dans le secteur Sud-Est. Ce monument semble plus évoquer un tertre pierreux qu'un reste de tertre d'habitat.

#### Historique

Tumlus découvert par [Meyrat F.]{.creator} en [mars 2016]{.date}.

</section>

<section class="monument">

### [Larceveau-Arreau-Cibits]{.spatial} - [Arapidekoborda]{.coverage} 2 - [Tumulus]{.type}

#### Localisation

Altitude : 368m.

Il est situé à 2m au Sud-Est de *Arapidekoborda 1 - Tumulus*.

#### Description

Tumulus circulaire pierreux de 5m de diamètre, 0,20m de haut. Un maximum de petits blocs [calcaires]{.subject} est visible dans le secteur Sud-Est. Là encore il semble qu'on soit devant un Tumulus plutôt qu'un terre d'habitat.

#### Historique

Tumulus découvert par [F. Meyrat]{.creator} en [mars 2016]{.date}.

</section>

# Larrau

<section class="monument">

### [Larrau]{.spatial} - [Abarrakiko Pekoa]{.coverage} - [Tertres d'habitat]{.type}

#### Localisation

Altitude : 1070m.

Les 5 tertres se trouvent à droite de la route qui monte vers Abarrakia, dans une prairie en pente douce.

#### Description

On peut voir éparpillés de la gauche à la droite 5 tertres d'habitat dont les dimensions varient de 6m à 8m de long et 0,80m à 1m de haut environ.

#### Historique

Tertres trouvés par [J. Blot]{.creator} en [octobre 2013]{.date}.

</section>

<section class="monument">

### [Larrau]{.spatial} - [Ardakhotchia]{.coverage} - [Cromlech]{.type}

#### Localisation

Altitude : 1109m.

On peut voir ce monument à une centaine de mètres en contre-bas et à l'Ouest de la route.

#### Description

Cercle bien visible de 3,50m de diamètre, délimité par une dizaine de pierres [calcaire]{.subject}, de la taille d'un gros pavé, ou plus, au ras du sol.

#### Historique

Cercle découvert par [J. Blot]{.creator} en [octobre 2013]{.date}.

</section>

<section class="monument">

### [Larrau]{.spatial} - [Ardakhotchia]{.coverage} (Groupe Sud-Est) - [Tertres d'habitat]{.type}

#### Localisation

Rappelons le *Groupe Nord*, déjà décrit [@blotSouleSesVestiges1979, p.3].

Le *Groupe Sud-Est* constitué de 3 tertres.

Altitude : 1067m ; le second est à 5m au Sud du précédent ; le troisième est à 30m au Sud-Ouest du second.

#### Description

Le premier et le second mesurent 10m x 10m et 0,40m de haut ; le troisième : 13m x 13m et 0,90m de haut.

#### Historique

Tertres découverts par [J. Blot]{.creator} en [octobre 2013]{.date}.

</section>

<section class="monument">

### [Larrau]{.spatial} - [Ardakhotchia]{.coverage} Est - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 1085m.

Il est près du riu, en bord Est de la prairie où se trouvent *Ardakhotchia (Groupe Sud-Est) - Tertres d'habitat* et *Ardakhotchia - Cromlech*.

#### Description

8m à 9m de diamètre et 0,60m de haut environ.

#### Historique

Monument découvert par [J. Blot]{.creator} en [octobre 2013]{.date}.

</section>

<section class="monument">

### [Larrau]{.spatial} - [Bagozabalaga]{.coverage} 2 - [Tumulus]{.type}

#### Localisation

Carte 1446 Est Tardets-Sorholus

Altitude : 1010m.

Nous avons publié les monuments de cette région [@blotSouleSesVestiges1979, p.19].

Se repérer au niveau du petit col auquel on accède en suivant la piste qui vient du cayolar Idagorria, et qui se dirige d'Est en Ouest vers la grange Halzourdi. Au niveau du col naît, au Sud, une ligne de croupes menant au bois d'Etchelu, passant par Bagozabalaga. On trouve le *tumulus n°2* à 60m environ au Sud-Ouest du col. Rappelons que le *tumulus n°1* (tumulus mixte de 7,60m de diamètre et 0,40m de haut), se trouve à une soixantaine de mètres au Nord-Est du départ de la piste qui se dirige vers Bagozabalaga.

#### Description

Tumulus pierreux de 8m de diamètre et 0,50m de haut, constitué de nombreuses pierres [calcaires]{.subject} blanches particulièrement visibles en périphérie, sans que l'on puisse pour autant parler de péristalithe. On note une dépression centrale : fouille ancienne ?

#### Historique

Monument découvert en [1980]{.date}.

</section>

<section class="monument">

### [Larrau]{.spatial} - [Bagurdineta]{.coverage} - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 1490m.

À mi-chemin de la descente vers *Bagurdineta 3 - Tumulus* et *Bagurdineta 4 - Cromlech*.

#### Description

Petit tertre de 4m à 5m de diamètre et 0,30m de haut, très remanié par la colluvion.

#### Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

</section>

<section class="monument">

### [Larrau]{.spatial} - [Betzülagagna]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 1584m.

Ce monument situé sur la piste de crête qui relie le Port de Larrau à Otxogorrigagna à l'Est, avait été décrit et publié par [nous]{.creator} en [1979]{.date} dans le [@blotSouleSesVestiges1979, p.29].

Ce tumulus de 10m de diamètre et 0,50m de haut environ, était formé de petites dalles [schiste]{.subject}uses, et présentait une légère dépression centrale. Lors d'une visite en [octobre 2017]{.date}, [F. Meyrat]{.creator} a pu constater la destruction complète de ce monument par la construction, à son sommet, d'un poste de tir à la palombe...

</section>

<section class="monument">

### [Larrau]{.spatial} - [Burkégui]{.coverage} - [Dolmen]{.type}

#### Localisation

Carte 1447 Nord Larrau.

Altitude : 1060m.

Il est situé au Nord-Ouest de l'important virage à angle droit que décrit la route qui monte de Larrau vers le pic d'Orhi ; on le trouve à environ 150m au Nord Nord-Ouest et en contre-bas du plateau où sont érigés les tertres de Burkégui que nous avons décrits [@blotCromlechUniteMeatse1979, p.27]. En partant de là, on suit une piste qui se rend à une cabane de chasse : le dolmen est situé à 25m à l'Est de cette cabane et d'une doline qui sert de dépotoir.

#### Description

Tumulus pierreux d'environ 8m de diamètre et 0,30m de haut, recouvert de fougères aigle. Au centre, une dépression de 0,90m de profondeur laisse apparaître 2 dalles épaisses en [grés]{.subject} poudingue, s'appuyant sur ses bords et légèrement inclinées vers l'extérieur. C'est tout ce qui reste d'une [chambre funéraire]{.subject}, très bouleversée par des fouilles clandestines anciennes, dont le grand axe était probablement orienté Nord-Est Sud-Ouest.

La dalle Ouest mesure 1,90m de long, 1m de large et 0,30m d'épaisseur. La dalle Sud-Est mesure 1,20m de long, 0,90m de large et 0,30m d'épaisseur. Pas de table ni de péristalithe visibles.

#### Historique

Dolmen signalé par [E. Dupré]{.creator} et vu par [nous]{.creator} en [octobre 1992]{.date}.

</section>

<section class="monument">

### [Larrau]{.spatial} - [Bagurdineta]{.coverage} 2 - [Tumulus]{.type}

#### Localisation

Carte 1447 Nord Larrau.

Altitude : 1469m.

Il est situé au Nord-Ouest du mont Odeizu-gagna, sur un petit ressaut à l'Ouest du col, et à 9m à l'Est Sud-Est du *Cromlech Bargudineta n°1* déjà publié [@blotSouleSesVestiges1979, p.16].

#### Description

Tumulus terreux de 6m de diamètre et 0,40m de haut. Au centre apparaît une dépression ovale à grand axe Nord-Sud de 4m de diamètre et 0,30m de profondeur : fouille ancienne ? Quelques rares pierres sont visibles en secteur Ouest.

#### Historique

Monument découvert en [septembre 1979]{.date}.

</section>

<section class="monument">

### [Larrau]{.spatial} - [Bagurdineta]{.coverage} 3 - [Tumulus]{.type}

#### Localisation

Sur la croupe de Bagurdineta.

altitude : 1469m

#### Description

Tumulus de 4m de diamètre et 0,15m de haut ; de nombreuses pierres apparaissent discrètement à sa surface ; péristalithe discutable.

#### Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

</section>

<section class="monument">

### [Larrau]{.spatial} - [Bagurdineta]{.coverage} 4 - [Cromlech]{.type}

#### Localisation

Altitude : 1468m.

Dans le replat du petit col et à 2m au Nord de la piste.

#### Description

Petit cercle, légèrement surélevé, formé de 8 pierres peu saillantes.

#### Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

</section>

<section class="monument">

### [Larrau]{.spatial} - [Erroimendi]{.coverage} 1 - [Cromlech]{.type}

#### Localisation

Altitude : 1360m.

Il est actuellement tout au bord de la tranchée de passage de la route qui l'a en grande partie démoli ; il n'en reste que 3 pierres visibles qui peuvent délimiter un cercle de 5m de diamètre.

#### Historique

Le *Cromlech C1* avait été décrit en [1962]{.date} par [J.M. de Barandiaran]{.creator} dans [@barandiaranProspecionesExcavacionesPrehistoricas1962, p.11], sous le nom de *Cromlech de Arhanolatze* ; à cette époque, il était intact, mesurait bien 5m de diamètre et avait 9 témoins bien visibles.

</section>

<section class="monument">

### [Larrau]{.spatial} - [Erroimendi]{.coverage} 2 - [Cromlech]{.type}

#### Localisation

Il est à 12m au Nord de *Erroimendi 1 - Cromlech* et en bordure de la tranchée de la route.

#### Description

Monument intact ; petit cercle de 2,80m de diamètre, délimité par 5 blocs de [grés]{.subject} jaune.

#### Historique

Monument trouvé par [Blot J.]{.creator} en [avril 2015]{.date}.

</section>

<section class="monument">

### [Larrau]{.spatial} - [Erroimendi]{.coverage} 3 - [Cromlech]{.type}

#### Localisation

Il est à 2m environ au Nord de *Erroimendi 2 - Cromlech*. Par contre, lui a été entamé par la tranchée routière.

#### Description

Monument fort dégradé dont il ne reste que 3 dalles visibles, ayant pu délimiter un cercle de 3m de diamètre.

</section>

<section class="monument">

### [Larrau]{.spatial} - [Erroimendi]{.coverage} 1 - [Tumulus]{.type}

#### Localisation

Altitude : 1362m.

Il est situé à environ 90m à l'Ouest Nord-Ouest de *Erroimendi 1 - Cromlech*, à 2m à l'Est du poste de tir à la palombe et à une dizaine de mètres à l'Est de *Erroimendi 2 - Tumulus*.

#### Description

Tumulus de 6,50m de diamètre et 0,40m de haut, constitué de terre et de nombreuses petites pierres visibles par endroit. Il semble qu'on puisse distinguer dans sa partie supérieure un cercle de diamètre moindre que le tumulus (environ 5,10m) délimité par une quinzaine de petits blocs de [grés]{.subject}.

#### Historique

[Nous]{.creator} avons découvert ce tumulus en [mai 1978]{.date}.

</section>

<section class="monument">

### [Larrau]{.spatial} - [Erroimendi]{.coverage} 2 - [Tumulus]{.type}

#### Localisation

Il est situé à une dizaine de mètres à l'Ouest de *Erroimendi 1 - Tumulus*.

#### Description

Tumulus bien visible, de 7m à 8m de diamètre et 0,40m de haut, dont la périphérie Sud-Est a été endommagée par les constructions successives de postes de tir à son voisinage immédiat.

#### Historique

[Nous]{.creator} avons découvert ce tumulus en [mai 1978]{.date}.

</section>

<section class="monument">

### [Larrau]{.spatial} - [Houharguieta]{.coverage} - [Pierre couchée]{.type}

#### Localisation

Altitude : 1560m.

Elle est située quasiment à l'extrémité Ouest Nord-Ouest de la crête de ce nom.

#### Description

On note, couchée sur le sol suivant un axe Nord-Sud, une dalle de [schiste]{.subject}, rectangulaire, mesurant 2,20m de long, 0,85m de large et 0,17m d'épaisseur. Elle semble présenter des traces d'épannelage à son extrémité Nord et le long de son bord Ouest, et ne semble pas être en place naturellement.

#### Historique

Pierre couchée découverte par [Meyrat F.]{.creator} en [août 2013]{.date}.

</section>

<section class="monument">

### [Larrau]{.spatial} - [Idagorria bas]{.coverage} Est 1 - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 970m.

Il est en bordure de la piste qui longe, en contre-bas la route venant de Lacarry, avant son virage vers Urrutchantzé et à proximité immédiate d'un enclos en poutrelles.

#### Description

Tertre de terre, semi circulaire, aplani, mesurant 20m de diamètre et 1,80m de haut, dominant au Nord la piste qui le contourne.

#### Historique

Tertre découvert par [Blot J.]{.creator} en [2012]{.date}.

</section>

<section class="monument">

### [Larrau]{.spatial} - [Ibarrondoa]{.coverage} Sud - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 1306m.

Il est à 20m au Sud-Ouest et en contre-bas du virage de la route qui mène à Ibarrondoa, peu avant que ne se détache la bretelle conduisant à ces installations.

#### Description

Grand tertre ovale et asymétrique, de 16m au moins dans son grand axe, et 20m de haut, face au rio (il est le symétrique du plus grand *TH* (près de la bergerie) du groupe Nord, qui en comprends au moins 8).

#### Historique

Tertre découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

</section>

<section class="monument">

### [Larrau]{.spatial} - [Kurutxetakolepoa]{.coverage} 2 - [Tumulus]{.type}

#### Localisation

Altitude : 925m.

Il est situé au sommet de la petite colline qui surplombe, au Sud, le col de Kurutxeta, à l'extrémité Nord de ce sommet, plat dans l'ensemble.

#### Description

Tumulus circulaire mixte de terre et de fragments plus ou moins gros de [calcaire]{.subject}, mesurant 7m de diamètre et 0,30m de haut.

#### Historique

Monument découvert par [Meyrat F.]{.creator} en [novembre 2017]{.date}. Nous signalons ici qu'en 1979 nous avions décrit [@blotSouleSesVestiges1979, p.17], un tumulus (*T1*). Situé dans le col lui-même. Il s'agissait en fait d'un mouvement de terrain dû aux vestiges des fondations d'un cayolar disparu, et non d'un tumulus.

</section>

<section class="monument">

### [Larrau]{.spatial} - [Kurutxetakolepoa]{.coverage} 3 - [Tumulus]{.type} ([?]{.douteux})

#### Localisation

Situé à 7m au Sud de *Kurutxetakolepoa 2 - Tumulus*.

#### Description

Relief circulaire discret, de 3,80m de diamètre et 0,10m de haut, avec légère dépression centrale.

#### Historique

Monument découvert par [Meyrat F.]{.creator} en [novembre 2017]{.date}.

</section>

<section class="monument">

### [Larrau]{.spatial} - [Kurutxetakolepoa]{.coverage} 4 - [Tumulus]{.type} ([?]{.douteux})

#### Localisation

Tangent au Sud de *Kurutxetakolepoa 3 - Tumulus (?)*, et au bord de la rupture de pente vers le Sud.

#### Description

Se présente comme une galette circulaire de 6m de diamètre, en faible relief (0,15m environ), avec une légère dépression centrale.

</section>

<section class="monument">

### [Larrau]{.spatial} - [Lapatignegagne]{.coverage} 2 - [Cromlech]{.type}

#### Localisation

À 16m environ à l'Ouest Sud-Ouest du *Tumulus n°3* déjà décrit [@blotSouleSesVestiges1979, p.17]

#### Description

Cinq pierres, au ras du sol, délimitent un cercle de 7m de diamètre : il y en a 1 au Nord, 1 à l'Ouest et 3 au Sud.

#### Historique

Cercle découvert en [octobre 2011]{.date} par [L. Millan]{.creator}, [M. Tamazyo]{.creator} et [I. Gaztelu]{.creator}.

</section>

<section class="monument">

### [Larrau]{.spatial} - [Lapatignegagne]{.coverage} 4 - [Tumulus]{.type}

#### Localisation

Altitude : 1350m.

Il est en contre-bas et au Nord-Ouest des monuments précédents, à 3m au Sud de la piste qui se rend au cayolar Arratzolaté, sur le premier plat qu'elle atteint.

#### Description

Tumulus de terre de 0,40m de haut et 6m de diamètre ; au centre existe une dépression de 1,50m de large et 0,20m de profondeur.

#### Historique

Tumulus découvert par [Blot J.]{.creator} en [août 2012]{.date}.

</section>

<section class="monument">

### [Larrau]{.spatial} - [Latsagakoborda]{.coverage} - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 874m.

#### Description

Tertre asymétrique de 12m environ et 1m de haut, sur terrain incliné.

#### Historique

Tertre découvert par [J. Blot]{.creator} en [1978]{.date}.

</section>

<section class="monument">

### [Larrau]{.spatial} - [Lepo Xipi]{.coverage} - [Cromlech]{.type}

#### Localisation

Altitude : 1450m.

Sur un replat à l'extrémité Est du sommet dénommé Pic des Escaliers, à 2m au Nord-Est de la piste.

#### Description

Un cercle de 4,80m de diamètre est délimité par une dizaine de pierres au ras du sol ; au centre se voit un petit amas d'environ une quinzaine de pierres de mêmes dimensions.

#### Historique

Monument découvert par [Blot Colette]{.creator} en [novembre 2012]{.date}.

</section>

<section class="monument">

### [Larrau]{.spatial} - [Méhatzekolepoa]{.coverage} - [Cromlech]{.type}

#### Localisation

Altitude : 1383m.

Il se trouve à 10m à l'Est du virage de la route, sur le gazon du col et à 20m au Sud-Est du *TC* [@blotSouleSesVestiges1979, p.13] maintenant détruit.

#### Description

Petit cercle 2,50m de diamètre, délimité par 5 pierres au ras du sol.

#### Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

</section>

<section class="monument">

### [Larrau]{.spatial} - [Millagate]{.coverage} 5 - [Tumulus-cromlech]{.type}

#### Localisation

Carte 1447 Nord Larrau.

Altitude : 1444m.

La nécropole de Millagate a été décrite en 1968 [@boucherNotesProspectionMegalithique1968, p.165], et nous y avons pratiqué deux fouilles de sauvetage : une en 1986 sur le *tumulus-cromlech Millagate 4* [@blotTumuluscromlechMillagateIV1990, p.49], et une sur *Millagate 5* en 1987 [@blotTumuluscromlechMillagateCompte1991, p.145].

Le monument n°5 est situé à 40m à l'Est du n°1 et à 23m à l'Est Nord-Est du n°2.

#### Description

Tumulus de 8m de diamètre et 0,20m de haut, délimité à sa périphérie par 10 pierres, au ras du sol, mais néanmoins bien visibles. On note une dépression, dans le secteur Sud, de 3m de diamètre et 0,20m de profondeur.

#### Historique

Monument découvert en [août 1979]{.date}, et fouillé en [1987]{.date}. La datation obtenue, en datation calibrée, est la suivante : 1118 - 812 BC.

</section>

<section class="monument">

### [Larrau]{.spatial} - [Murutxe]{.coverage} Sud - [Tertres d'habitat]{.type}

On en dénombre trois.

#### Localisation

Altitude : 970m.

Le premier se trouve (*TH 1*) en bord Sud de la route qui se rend vers le pic Salhagagne et Licq, au pied du relief qui précède à l'Ouest celui dénommé Murutxe.

#### Description

*TH 1* est accolé au bord de la route ; tertre de terre 7m de long, 4 de large, allongé dans le sens Est Sud-Est Ouest Nord-Ouest ; un relief à son flanc Sud suggère un deuxième tertre (*TH 2*).

Enfin un autre tertre (*TH 3*) a été probablement remanié par les engins routiers ; il est à 8m au Sud-Ouest des précédents ; de terre et de pierres il mesure 7m de long, 2,50m de large.

#### Historique

Tertres découverts par [J. Blot]{.creator} en [2012]{.date}. Signalons ici que le *Dolmen de Murutxe*, signalé par J.M. Barandiaran, [@barandiaranHombrePrehistoricoPais1953, p.235] a été détruit par les travaux routiers.

</section>

<section class="monument">

### [Larrau]{.spatial} - [Murutxe]{.coverage} Nord 1 - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 990m.

Au sommet de la colline précède celle dénommée Murutxe.

#### Description

Tertre asymétrique oblong, de terre et de quelques pierres, mesurant environ 10m de long et 0,50m de haut, a grande pente vers le Sud.

#### Historique

Tertre découvert par [Blot C.]{.creator} en [2012]{.date}.

</section>

<section class="monument">

### [Larrau]{.spatial} - [Olhaberria]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 960m.

Il est dans le col précédent la colline où se situe *Murutxe Nord 1 - Tertre d'habitat*.

#### Description

On note un relief circulaire de 10m de diamètre et 0,30m de haut, avec une légère excavation centrale de 3m de diamètre et quelques centimètres de profondeur. S'agit-il d'un tumulus ou d'un relief consécutif à une excavation ? nous pencherions pour la première hypothèse.

#### Historique

Monument découvert par [Blot J.]{.creator} en [2012]{.date}.

</section>

<section class="monument">

### [Larrau]{.spatial} - [Otxogorrigagne]{.coverage} - [Tumulus]{.type}

On en dénombre quatre.

-   *Otxogorrigane 1 - Tumulus*

    -   Localisation

        Altitude : 1400m.

        Sur la ligne de crête qui part à gauche de la route qui descend vers Larrau.

    -   Description

        Petit tumulus de 4m de diamètre environ et 0,30 de haut, fait de pierrailles essentiellement.

    -   Historique

        Monument découvert par [J. Blot]{.creator} en [mai 2011]{.date}.

-   *Otxogorrigagne 2 - Tumulus*

    -   Localisation

        Il est à une quarantaine de mètres à l'Est Sud-Est du n°1.

    -   Description

        Mesure près de 5m de diamètre et 0,40m de haut. Terre et pierrailles.

    -   Historique

        Monument découvert par J. Blot en mai 2011.

-   *Oxogorrigane 3 - Tumulus*

    -   Localisation

        Situé à une vingtaine de mètres à l'Ouest Nord-Ouest du n°1.

    -   Description

        Mesure environ 3,50m de diamètre, en pierraille et de faible hauteur.

    -   Historique

        Monument découvert par J. Blot en mai 2011.

-   *Oxogorrigagne 4 - Tumulus*

    -   Localisation

        Situé à une quarantaine de mètres à l'Ouest du précédent, de même nature et dimensions.

    -   Historique

        Monument découvert par J. Blot en mai 2011.

</section>

<section class="monument">

### [Larrau]{.spatial} - [Otxogorria]{.coverage} - [Cromlech]{.type}

#### Localisation

Altitude : 1337m.

Il se trouve à une vingtaine de mètres au Sud de *Otxogorria - Tertre d'habitat*.

#### Description

Petit cromlech fait de 7 pierres très visibles, mesurant en moyenne 0,70m x 0,25m et disposées suivant un tracé légèrement ovale de 2,30m x 2m.

#### Historique

Cromlech découvert par [Blot J.]{.creator} en [octobre 2014]{.date}.

</section>

<section class="monument">

### [Larrau]{.spatial} - [Otxogorria]{.coverage} - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 1337m.

Il se trouve sur le vaste plateau au flanc Nord du sommet de Negumendi et de son pylône relais, en contre-bas de la route qui y monte.

#### Description

Tertre de terre de faible relief (0,50m de haut), de forme oblongue (26m x 8m) érigé sur terrain presque plat.

#### Historique

Tertre découvert par [Blot J.]{.creator} en [octobre 2014]{.date}.

</section>

<section class="monument">

### [Larrau]{.spatial} - [Odécharreko lepoa]{.coverage} - [Monolithe]{.type}

#### Localisation

Altitude : 1590m.

Il gît dans un virage de la route (sur son bord Est), qui passe sous la crête de Millagate, en partant des chalets d'Iraty.

#### Description

Gros bloc parallélépipédique régulier, de [grés]{.subject} local, allongé selon un axe Nord-Sud. Il mesure 4,10m de long, 1,54m de large à sa base Sud, 1,21m à son sommet Nord. Son épaisseur varie : au côté Ouest elle est de 0,64m à son côté Est. Il ne semble pas présenter de traces d'épannelage, mais sa présence en ce lieu, sa forme et ses dimensions posent la question d'une éventuelle signification anthropique.

#### Historique

Monolithe découvert par le [groupe Hillariak]{.creator} en [2012]{.date}.

</section>

<section class="monument">

### [Larrau]{.spatial} - [Peko Olhaberria]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 1110m.

Tangent au Nord du GR 10 qui passe au virage de la route et descend vers l'Ouest, en passant par une croupe, d'abord en terrain plat ; il est à l'amorce de ce replat.

#### Description

Tumulus pierreux de 4,50m de diamètre et 0,20m de haut, constitué de blocs de [calcaire]{.subject} de la taille d'un ou deux pavés.

#### Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2013]{.date}.

</section>

<section class="monument">

### [Larrau]{.spatial} - [Peko Olhaberria]{.coverage} 1 - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 1093m.

En suivant le GR 10, sur le replat, à 70m environ à l'Ouest de *Peko Olhaberria - Tumulus*, avant l'amorce de la descente.

#### Description

Tertre de terre de 8m x 8m et 0,70m de haut environ.

#### Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2013]{.date}.

</section>

<section class="monument">

### [Larrau]{.spatial} - [Pelusagne]{.coverage} 2 - [Tumulus]{.type}

#### Localisation

Altitude : 1540m.

Il est au-dessus d'une carrière qui entame la route, au Sud, dans un virage.

#### Description

Tumulus circulaire de terre et de pierres, de 5m de diamètre et 1m de haut environ. Il semblerait qu'il y ait un péristalithe.

#### Historique

Tumulus découvert par [Meyrat F.]{.creator} en [août 2013]{.date}.

</section>

<section class="monument">

### [Larrau]{.spatial} - [Pelusagne]{.coverage} 3 - [Tumulus]{.type}

#### Localisation

À 4m à l'Est de *Pelusagne 2 - Tumulus*.

#### Description

Tumulus de terre et de pierres, mesurant 4m de diamètre et 0,40m de haut.

#### Historique

Tumulus découvert par [Meyrat F.]{.creator} en [août 2013]{.date}.

</section>

<section class="monument">

### [Larrau]{.spatial} - [Sagukidoy]{.coverage} Ouest 1 - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 970m.

Erigé sur terrain en pente légère surplombant un petit ravin. Visible de loin.

#### Description

Tertre de terre oblong d'une vingtaine de mètres de long, 5m de large et 1m de haut.

#### Historique

Tertre découvert en [1979]{.date}.

</section>

<section class="monument">

### [Larrau]{.spatial} - [Sagukidoy]{.coverage} centre - [Tertres d'habitat]{.type}

#### Localisation

Ce groupe, s'élevant plus ou moins à une dizaine de tertres de grandes dimensions, est réparti dans les environs immédiats de la grange Sagukidoy. Le relevé plus précis n'a pu encore être fait.

Altitude moyenne : 920m.

#### Historique

Tertres découverts par [Blot J.]{.creator} en [1979]{.date}.

</section>

<section class="monument">

### [Larrau]{.spatial} - [Sagukidoy]{.coverage} Est haut - [Tertres d'habitat]{.type}

-   *Tertre n°1*

    -   Localisation

        Altitude : 980m.

        On le trouve sur la droite du chemin antique qui mène à la grange Sagukidoy, au voisinage immédiat des ruines d'un très vieux cayolar.

    -   Description :

        Tertre de terre semi-circulaire de 8m environ de diamètre et 2m de haut.

-   *Tertre n°2*

    -   Localisation

        Altitude : 978m.

        Il est situé à 3m à gauche du même chemin, une dizaine de mètres plus loin.

    -   Description :

        Vaste tertre de terre, de forme oblongue, mesurant 18m de long, 6m de large et 1m de haut.

#### Historique

Tertres découverts par [Blot C.]{.creator} en [2012]{.date}.

</section>

<section class="monument">

### [Larrau]{.spatial} - [Sagukidoy]{.coverage} Est bas 1 - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 800m.

Visible de loin, de la route, sur terrain en pente, en bord de petit ravin.

#### Description

Tertre de terre d'une vingtaine de mètres de diamètre et un de haut.

#### Historique

Monument découvert par [Blot J.]{.creator} en [1979]{.date}.

</section>

<section class="monument">

### [Larrau]{.spatial} - [Salhagagne]{.coverage} 2 - [Tumulus]{.type}

#### Localisation

Altitude : 990m.

Il se trouve au Sud-Ouest du virage de la route qui va descendre vers Licq, et à quelques mètres à l'Ouest d'une piste de terre qui monte vers le sommet de Salhagagne.

#### Description

Tumulus de terre circulaire de 10m de diamètre et 0,40m de haut.

#### Historique

Tumulus découvert par [Blot J.]{.creator} en [septembre 2012]{.date}.

</section>

<section class="monument">

### [Larrau]{.spatial} - [Saratzé]{.coverage} Sud 1 - [Tertres d'habitat]{.type}

#### Localisation

Altitude : 1217m.

Ces 3 tertres sont échelonnés en bordure d'un petit riu qu'ils dominent.

#### Description

Ils mesurent en moyenne 15m de diamètre et 0,50m de haut.

#### Historique

Tertres découverts par [Blot J.]{.creator} en [octobre 2013]{.date}.

</section>

<section class="monument">

### [Larrau]{.spatial} - [Saratzé]{.coverage} Sud 2 - [Tertres d'habitat]{.type}

#### Localisation

Altitude : 1196m.

Ces deux tertres sont échelonnés sur une croupe herbeuse, étendue selon un axe Est-Ouest.

#### Description

Ils mesurent environ 16m à 18m de diamètre et 0,60m de haut.

#### Historique

Tertres découverts par [Blot J.]{.creator} en [octobre 2013]{.date}.

</section>

<section class="monument">

### [Larrau]{.spatial} - [Sarimendi]{.coverage} - [Cromlech]{.type}

#### Localisation

Altitude : 1413m.

Cromlech situé au pied du mont Sarimendi - au Sud-Est - au col Arrestelitako lepoa. Il n'est pas au col même - où débouche une piste pastorale côté Sud-Ouest - mais sur une légère éminence en surplomb de celui-ci, au Sud Sud-Est. On le voit à 3,70m à l'Est de la piste de crête, au bord de la rupture de pente.

#### Description

Il s'agit d'un cromlech qui a été très remanié par une probable fouille clandestine, dont il reste une excavation d'environ 4m de diamètre et 0,30m à 0,45m de profondeur ; en périphérie de cette dépression on note une dizaine de blocs de [calcaire]{.subject} blanc, plus ou moins bien disposés en un cercle d'environ 2,90m de diamètre, mais dont la position initiale a été, à l'évidence, modifiée par l'excavation.

#### Historique

Monument trouvé par [Ph. Velche]{.creator} en [juin 2016]{.date}.

</section>

<section class="monument">

### [Larrau]{.spatial} - [Sakia]{.coverage} 1 et 2 - [Tumulus]{.type}

#### Localisation

Altitude : 1358m.

Ce monument se trouve vers la partie haute et Sud de la longue ligne de croupe qui borde la rive droite du ruisseau (ou gorges) d'Olhadubi. Il est situé dans un joli replat herbeux à une soixantaine de mètres à l'Est des cayolars de Sakia et du très beau groupe de tertres d'habitat du même nom (Sakia-Gagnekoa).

#### Description

-   *T1* : tumulus terreux en forme de galette circulaire, mesurant 4,40m de diamètre et 0,30m de haut ; pas de pierres visibles.

-   *T2* : Tangent à lui au Sud, il semble qu'on puisse évoquer un deuxième tumulus de 4m de diamètre et 0,20m de haut, dont le tiers Est, peu visible, aurait été détérioré. Nous avions vu le même cas à Apatessaro 1 bis, confirmé par la fouille.

#### Historique

Le tumulus *T1* a été découvert par [A. Martinez]{.creator} en [septembre 2017]{.date} et le *T2* par [Blot J.]{.creator} à la fin du même mois.

</section>

<section class="monument">

### [Larrau]{.spatial} - [Ugnhurritzé]{.coverage} - [Tertres d'habitat]{.type}

altitude : 999m.

[Nous]{.creator} sommes revenus en [octobre 2014]{.date} sur ce site publié par nous en [1979]{.date} [@blotSouleSesVestiges1979, p.31], et la végétation moins abondante nous a permis de constater que le nombre de vestiges de tertres d'habitat est *plus du double* que celui précédemment indiqué : 13 TH.

</section>

<section class="monument">

### [Larrau]{.spatial} - [Uthurzeheta]{.coverage} - [Tumulus]{.type} ou [Tertre d'habitat]{.type} ([?]{.douteux})

#### Localisation

Altitude : 1340m.

Au pied du cirque montagneux qui entoure les tertres d'Uthurzeheta, à l'Ouest de ceux-ci, entre l'émergence de plusieurs petits rus.

#### Description

Vaste tertre (tumulus ?) de forme ovale mesurant de 20m à 25m de long, une quinzaine de large, et 2m à 3m de haut.

On peut, éventuellement, se poser la question d'un mouvement naturel du terrain... relativement peu probable à notre avis.

</section>

<section class="monument">

### [Larrau]{.spatial} - [Xardeka]{.coverage} 6 - [Tumulus-cromlech]{.type} ou [Dolmen]{.type}

#### Localisation

Altitude : 1515m.

Il se trouve sur le versant Nord-Ouest du col qui sépare Pelusagne de Xardeka, à une vingtaine de mètres de la naissance du petit ruisseau au point déclive du col.

#### Description

Beau tumulus de 8m de diamètre et 0,20m de haut, érigé sur terrain en pente vers le Sud-Est. Un bloc de poudingue dans le secteur Sud, de forme semi-circulaire (épannelé ?) mesurant 1m à sa base et 0,10m d'épaisseur. Ferait penser à une dalle dolménique éversée... mais par ailleurs une vingtaine de petites pierres disposées en cercle dans la région centrale, modifient de point de vue. Enfin quelques pierres à la périphérie du tumulus évoquent l'existence possible d'un péristalithe.

#### Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

</section>

# Lasse

<section class="monument">

### [Lasse]{.spatial} - [Beharria]{.coverage} - [Pierre couchée]{.type}

#### Localisation

Altitude : 774m.

Elle est sur un replat au flanc Nord du Monhoa, dans une boucle de la route et sur le Gr 10.

#### Description

Grand bloc de [quartzite]{.subject}, allongé en pain de sucre, suivant un axe sensiblement Est-Ouest. Il mesure 3,90m de long, 1,20m de large au maximum et son épaisseur est de 0,75m en moyenne. Ne présente pas de traces d'épannelage semble-t-il. Il peut s'agir d'un bloc venu par glissement de la pente au Sud, mais la présence à peu de distance de deux tertres d'habitat (*Beharria - Tertres d'habitat*) démontre, au moins, une activité pastorale ancienne.

À 5m au Nord-Est on voit un deuxième bloc, triangulaire de 2,80m dans sa plus grande longueur, 1,50m de large et 0,65m d'épaisseur, mais qui semble sans rapport avec le précédent.

#### Historique

Pierre découverte par Mme [Carde G.]{.creator} en [octobre 2011]{.date}.

</section>

<section class="monument">

### [Lasse]{.spatial} - [Beharria]{.coverage} - [Tertres d'habitat]{.type}

#### Localisation

Altitude : 640m.

Les 2 tertres sont situés à une centaine de mètres à l'Est et en contre-bas d'abreuvoirs en bord de route au niveau d'un virage, sur un petit replat.

#### Description

-   *TH N°1* : le plus bas situé, mais le plus net. Erigé sur un terrain en pente très légère, il mesure un peu plus de 7m de diamètre, et 0,80m de haut environ. On pourrait presque le confondre avec un tumulus.

-   *TH N°2* : la présence à une quinzaine de mètres au Sud-Ouest, et toujours sur terrain en pente, d'un deuxième tertre de 8 à 9m de diamètre, 0,60m de haut avec légère dépression centrale, confirme leur qualité de tertre d'habitat.

#### Historique

Tertres découverts par [Blot J.]{.creator} en [novembre 2011]{.date}.

</section>

<section class="monument">

### [Lasse]{.spatial} - [Elurméaka]{.coverage} - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 870m.

Il est situé au point culminant de modeste relief, sur la ligne de crête, à 150m environ à l'Est de *Beharria - Tertres d'habitat*.

#### Description

Tertre de terre apparemment circulaire, d'environ 1m de haut et 7,30m de diamètre ; en fait il existe un prolongement tumulaire vers le Nord-Est de 2m environ. L'ensemble est à grand axe Nord-Est Sud-Ouest, et est érigé sur un terrain en légère pente vers le Nord-Est.

#### Historique

Monument découvert par [F. Meyrat]{.creator} en [juillet 2011]{.date}.

</section>

<section class="monument">

### [Lasse]{.spatial} - [Madariakolepoa]{.coverage} - [Tertre d'habitat]{.type} ou [Tumulus cromlech]{.type} ([?]{.douteux})

#### Localisation

Altitude :860m.

Ce monument est situé à environ une quarantaine de mètres à l'Est de la bergerie du col, et un peu au Sud de la ligne de crête.

#### Description

Tertre de terre et de pierraille, très légèrement ovale, d'environ 6,50m de long, 6m de large, et 1m de haut, à grand axe Nord-Sud, érigé sur un terrain en pente douce vers l'Ouest. Il existe dans le secteur Nord-Ouest comme une couronne de pierres de volume moyen (certaines de 0,45m x 0,50m) ; vestige d'un péristalithe ?

#### Historique

Monument découvert par [F. Meyrat]{.creator} en [juillet 2011]{.date}.

</section>

<section class="monument">

### [Lasse]{.spatial} - [Mataria]{.coverage} 2 - [Cromlech]{.type} ([?]{.douteux})

#### Localisation

Altitude : 790m.

Il se trouve sur un petit replat (terrain en très légère pente vers l'Ouest) un peu avant d'arriver en haut de la montée sur Mataria, en venant du col situé au Sud-Ouest.

#### Description

Cercle de 3,80m de diamètre délimité par une quinzaine de petites pierres au ras du sol particulièrement visibles dans les deux tiers Ouest du monument.

#### Historique

Cercle découvert par [J. Blot]{.creator} en [août 2011]{.date}. Rappelons le *C n°1* [@blotNouveauxVestigesMegalithiques1972c, p.210]

</section>

<section class="monument">

### [Lasse]{.spatial} - [Mataria]{.coverage} 3 - [Cromlech]{.type} ([?]{.douteux})

#### Localisation

Altitude : 780m.

Il se trouve à une quarantaine de mètres à l'Est Sud-Est du très beau tumulus décrit antérieurement [@blotNouveauxVestigesMegalithiques1972b, p.210]

Altitude : 780m

#### Description

Un ensemble de pierres de volumes assez importants (deux ou trois gros pavés), délimite un cercle de 4m de diamètre ; ces pierres sont réparties sur une couronne d'environ 1,70m de large, laissant libre en son centre un espace circulaire de 2,30m de diamètre.

Bon nombre de ces pierres présentent une forte mobilité, ce qui laisse planer un doute sur ce monument, bien que le propriétaire des lieux l'ait toujours connu dans cet état.

#### Historique

Monument découvert par [Blot. J.]{.creator} en [août 2011]{.date}.

</section>

<section class="monument">

### [Lasse]{.spatial} - [Mataria]{.coverage} - [Dolmen]{.type}

#### Localisation

Altitude : 787m.

Il est à l'extrémité Nord-Est du plateau sommital.

#### Description

Un ensemble de dalles plantées sont visible au ras du sol, délimitant une chambre funéraire de 1,20m de long environ, et 0,88m de large, orientée Nord-Ouest Sud-Est. Six petites dalles forment la paroi Sud-Ouest ; deux autres, dont une de 0,60m de long, forment la paroi Nord-Est ; la paroi Nord-Ouest est formée de 3 fragments de dalle. Il semble qu'il existe le vestige d'un tumulus de très faible relief, actuellement de 4m de diamètre.

#### Historique

Monument découvert par [Blot. J.]{.creator} en [août 2011]{.date}.

</section>

# Lecumberry

<section class="monument">

### [Lecumberry]{.spatial} - [Apatessaro]{.coverage} 1 ter - [Cromlech]{.type}

#### Localisation

Carte 1346 Est Saint-Jean-Pied-de-Port.

Altitude : 1135m.

[Nous]{.creator} avons déjà publié la plus grande partie de cette nécropole en 1972 [@blotNouveauxVestigesMegalithiques1972b, p.74], et 1986 [@blotNecropoleProtohistoriqueApatessaro1986, p. 89]. On compte ainsi les *cromlechs 1*, *1 bis*, *2*, *6*, et *7* ; les *tumulus 3*, *4*, *5*, et *8*.

Nous avons effectué des fouilles de sauvetage sur les *cromlechs 1* et *1 bis*, [@blotCromlechsApatessaro1bis1986, p.92], et sur les *tumulus n°4*, [@blotTumulusApatessaroIV1988, p.1], *n°5* [@blotTumulusApatessaroCompte1988, p.177], et sur le *n°6* [@blotTumulusApatesaroVI1994, p.47].

On peut voir le *cromlech 1 ter* à 3m à l'Ouest du *n°1 bis*.

#### Description

Six petites pierres, au ras du sol, délimitent un cercle de 4m de diamètre.

#### Historique

Monument découvert en [juin 1983]{.date}.

</section>

<section class="monument">

### [Lecumberry]{.spatial} - [Apatessaro]{.coverage} 8 - [Tumulus]{.type}

#### Localisation

Altitude : 1200m.

Il est (difficilement) visible en bordure de la piste qui monte à Okabé, mais à quelques mètres au Sud ; isolé dans un ensemble pierreux, il se détache cependant sur un petit replat gazonné, à 3m au Sud-Est d'un gros amas de blocs de poudingue.

#### Description

Tumulus pierreux de faible hauteur (0,15m) et légèrement ovale (3,50m x 3m), constitué de blocs pierreux de modeste volume (d'un demi à un pavé).

#### Historique

Monument découvert en [juin 1983]{.date}.

</section>

<section class="monument">

### [Lecumberry]{.spatial} - [Artxilondo]{.coverage} - [Dolmen]{.type}

#### Localisation

Carte 1346 Est Saint-Jean-Pied-de-Port.

Altitude : 880m.

Il est érigé sur un petit promontoire à 80m au Sud-Ouest de la jonction d'Iraunabako-erreka avec Toscato-erreka ; il est donc sur la rive gauche du rio Artxilondo qu'il domine directement, et à 600m à l'Ouest Sud-Ouest des cabanes d'Artxilondo.

#### Description

Important tumulus de galets de [quartzite]{.subject} roulés, mesurant 11m de diamètre et 1,40m de haut. Au centre, la [chambre funéraire]{.subject} est orientée Nord-Ouest Sud-Est et mesure 3m de long, 1,20m de large, et 0,90m de profondeur ; elle est bien délimitée au Nord-Est par une grande dalle de [grés]{.subject} poudingue de 2,80m de long, 0,90m de haut et 0,30m d'épaisseur. Sur le versant Est du tumulus, immédiatement à côté de la chambre gît la dalle de [couverture]{.subject} : belle dalle de [grés]{.subject} poudingue, grossièrement rectangulaire mesurant 2,86m de long, 1,80m de large et 0,30m d'épaisseur. Ses 4 bords portent des traces d'épannelage.

#### Historique

Dolmen découvert en [octobre 1976]{.date}. Il a fait l'objet d'un sondage diagnostic en 2000 par J. Blot et D. Ebrard [@ebrardDolmenArtxilondoLecumberry2000].

</section>

<section class="monument">

### [Lecumberry]{.spatial} - [Artxilondo]{.coverage} - [tertres d'habitat]{.type}

#### Localisation

Altitude : 921m.

Tout d'abord, 6 tertres, découvert en 1974, sont échelonnés sur la rive gauche d'un petit ruisseau né au niveau du cayolar de Néquecharré (ou Négousaro), et érigés au niveau des 2 anciennes cabanes d'Artxilondo. - Ils ont depuis été remaniés ou détruits par de nouvelles constructions. Ont ainsi disparu les *tertres n°2*, *4* et *5*. Il ne semble rester que les *n°6*, *1* et *3*.

#### Description

-   *Tertre n°1*, de 11m de diamètre et 0,60m de haut.

-   *Tertre n°2* : à 7m à l'Ouest du précédent. Mesure10m de diamètre et 0,60m de haut.

-   *Tertre n°3* : à 2m à l'Ouest du précédent. Mesure 13m de diamètre et 0,70m de haut.

-   *Tertre n°4* : tangent au précédent et de mêmes dimensions.

-   *Tertre n°5* : tangent au précédent, en partie amputé de son extrémité Sud par le passage de la piste venue des vieilles cabanes, mesure10m de diamètre et 0,40m de haut environ.

-   *Tertre n°6* : à 1m à l'Ouest du précédent - lui aussi amputé par la piste -- situé à gauche du chemin qui mène aux 2 récents cayolar, et avant ce dernier ; mesure 6m et 0,40m de haut environ.

-   Le *tertre n°7* est situé en bordure du chemin qui contourne les deux récents cayolars, au Nord de ceux-ci, et donne vers la pente d'un ruisseau.

-   Le *tertre n°8* : est situé en dehors de la clôture des barbelés, à 9m au Sud-Est du n°1 ; mesure 6m x 7m et 0,50m de haut semble-t-il.

#### Historique

Ces tertres ont été trouvés par [Blot J.]{.creator} en [1974]{.date}. Sauf les 7 et 8 derniers tertres ont été trouvés pat Blot J. en [février 2014]{.date}.

</section>

<section class="monument">

### [Lecumberry]{.spatial} - [Artxilondo]{.coverage} 4 - [Tumulus]{.type}

#### Localisation

Carte 1346 Est Saint-Jean-Pied-de-Port.

Altitude : 908m.

Nous avons déjà décrit [@blotNouveauxVestigesMegalithiques1972b, p.72], dans cette région située à l'Ouest du mont Okabé, 3 cromlechs 1, 2, 3.

Par rapport aux cabanes d'Atxilondo, le tumulus 4 est situé à 4m de l'angle Nord Nord-Est du cayolar situé à droite de la route qui se rend aux cabanes précitées, au premier virage une fois franchi le pont sur Iraunabako-erreka.

#### Description

Tumulus de 4,50m de diamètre et 0,30m de haut ; 4 pierres sont visibles à sa périphérie sans que l'on puisse parler d'un tumulus-cromlech.

#### Historique

Monument découvert en [juillet 1974]{.date}.

</section>

<section class="monument">

### [Lecumberry]{.spatial} - [Artxilondo]{.coverage} 5 - [Tumulus]{.type}

#### Localisation

Il est situé à 8m à l'Ouest Nord-Ouest de *Artxilondo 4 - Tumulus*.

#### Description

Tumulus terreux d'un diamètre de 4m et 0,40m de haut ; une vingtaine de pierres apparaissent en désordre à sa surface. Il se pourrait qu'il y ait un péristalithe, mais en grande partie recouvert de terre et de végétation.

#### Historique

Monument découvert en [juillet 1974]{.date}.

</section>

<section class="monument">

### [Lecumberry]{.spatial} - [Artxilondo]{.coverage} 6 - [Tumulus]{.type}

#### Localisation

Altitude : 908m.

Il se trouve à 4m à l'Ouest du Tumulus-cromlech 5 ou à environ 25m à l'Ouest du petit cayolar.

#### Description

Tumulus de 8m de diamètre et 0,30m environ ; quelques rares pierres apparaissent par endroits.

#### Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

</section>

<section class="monument">

### [Lecumberry]{.spatial} - [Bassabero]{.coverage} 1 - [Tumulus]{.type}

#### Localisation

Carte 1346 Est Saint-Jean-Pied-de-Port.

Altitude : 1217m.

Il est érigé au point culminant de la montée qui domine Artxilondo au Nord, éminence située entre Apatessaroko-erreka et Toscato-erreka.

#### Description

Tumulus pierreux de 3m de diamètre et 0,40m de haut, formé de blocs de pierre amoncelés de la taille d'un pavé (à 150m au Nord-Est dans un petit ensellement, un léger relief pourrait évoquer un tumulus herbeux de 6m de diamètre 0,40m de haut).

#### Historique

Monument découvert en [octobre 1980]{.date}.

</section>

<section class="monument">

### [Lecumberry]{.spatial} - [Bassabero]{.coverage} 2 - [Cromlech]{.type}

#### Localisation

Carte 1346 Est Saint-Jean-Pied-de-Port.

Altitude : 1040m.

Cromlech situé au flanc de cette éminence incluse entre les 2 ruisseaux Apatessaroko-erreka et Toscato-erreka. Il est construit sur un terrain en légère pente qui domine à 20m à gauche le chemin qui mène à Neguecharre, ceci environ 500m après qu'il ait franchi le petit ravin d'Apatessaro.

#### Description

Cercle de 4m de diamètre, délimité par 15 pierres bien visibles (0,10m à 0,20m de haut). Au centre est visible, au ras du sol, une pierre de [grés]{.subject} local.

#### Historique

Monument découvert en [1979]{.date}.

</section>

<section class="monument">

### [Lecumberry]{.spatial} - [Bassabero]{.coverage} 3 - [Tumulus]{.type}

#### Localisation

Altitude : 1247m.

Situé à environ 1m au Nord d'un pointement rocheux.

#### Description

Tertre légèrement ovalaire, érigé sur terrain plat, de 8m de long et 6m environ de large, et 0,40m de haut, constitué de pierres concassées, de la taille d'une cerise ou d'un abricot.

#### Historique

Tertre découvert par [Meyrat F.]{.creator} en [août 2013]{.date}.

</section>

<section class="monument">

### [Lecumberry]{.spatial} - [Bassabero]{.coverage} 4 - [Tumulus]{.type}

#### Localisation

Il est à 10m au Nord-Ouest de *Bassabero 3 - Tumulus*.

#### Description

De même forme ovalaire et de dimensions semblables que *Bassabero 3 - Tumulus*, mais un peu plus haut (0,50m), il est constitué du même cailloutis.

#### Historique

Tumulus découvert par [Meyrat F.]{.creator} en [août 2013]{.date}.

</section>

<section class="monument">

### [Lecumberry]{.spatial} - [Bassahémela]{.coverage} - [Tumulus-cromlech]{.type} ([?]{.douteux})

#### Localisation

Altitude : 537m.

Situé à une quarantaine de mètres à l'Ouest Nord-Ouest du pointement rocheux.

#### Description

Petit tumulus terreux, de 4m de diamètre et 0,30m de haut ; il semble que 6 pierres environ puissent délimiter un péritalthe.

#### Historique

Monument découvert par [Meyrat F.]{.creator} en [avril 2014]{.date}.

</section>

<section class="monument">

### [Lecumberry]{.spatial} - [Bassahémela]{.coverage} - [Tumulus-cromlech]{.type} ([?]{.douteux})

#### Localisation

Altitude : 537m.

Situé à une quarantaine de mètres à l'Ouest Nord-Ouest du pointement rocheux.

#### Description

Petit tumulus terreux, de 4m de diamètre et 0,30m de haut ; il semble que 6 pierres environ puissent délimiter un péritalthe.

#### Historique

Monument découvert par [Meyrat F.]{.creator} en [avril 2014]{.date}.

</section>

<section class="monument">

### [Lecumberry]{.spatial} - [Bassahémela]{.coverage} - [Tumulus]{.type}

#### Localisation

Carte 1346 Est Saint-Jean-Pied-de-Port.

Altitude : 540m.

Il est situé sur un petit replat à environ 40m au Sud Sud-Est du point culminant ainsi dénommé.

#### Description

Tumulus mixte de 4m de diamètre et 0,60m de haut ; de très nombreuses pierres sont visibles.

#### Historique

Monument découvert en [mai 1972]{.date}.

</section>

<section class="monument">

### [Lecumberry]{.spatial} - [Egurgi]{.coverage}- [Tertres d'habitat]{.type}

#### Localisation

Altitude : 830m.

Ces 13 tertres sont échelonnés de part et d'autre de la route qui se rend à la frontière en passant par les anciennes installations piscicoles d'Egurgi.

#### Description

On note :

-   le *Tertre n°1*, à droite (à l'Ouest) de la route, il mesure 10m de diamètre et 0,50m de haut

-   Le *Tertre n°2* est tangent au Sud-Est et mesure 12m de diamètre et 0,70m de haut.

-   Le *Tertre n°3* est de l'autre côté de la route et a été coupé en son milieu ; il mesure 22m de diamètre et 1m de haut

-   Le *Tertre n°4* est à 7m à l'Est Nord-Est du précédent, plus sur la hauteur ; il mesure 22m de diamètre et 1m de haut.

-   Le *Tertre n°5* est à l'Est du précédent, et mesure 6m de diamètre et 0,80m de haut.

-   Les *Tertres 6, 7, 8* sont tangents les uns aux autres, échelonnés suivant un axe Ouest-Est, et de taille bien inférieure : ils mesurent entre 2 et 3 m de diamètre

-   Il en est de même pour le groupe de *Tertres 9, 10, 11, 12, 13* qui sont un peu au Sud des précédents, et ont sensiblement les mêmes dimensions.

#### Historique

Les tertres 1,2,3,4 ont été trouvés par [Blot J.]{.creator} en [1970]{.date} et les autres par [Meyrat F.]{.creator} en [août 2013]{.date}.

</section>

<section class="monument">

### [Lecumberry]{.spatial} - [Etxaaté Bizkarra]{.coverage} 3 - [Tumulus]{.type}

#### Localisation

À 40m au Nord-Est du *Tumulus 2*.

Altitude : 1199m

#### Description

Ce tumulus, au sommet d'un petit relief naturel, mesure 3,90m de diamètre, et 0,70m de haut. Il est constitué de terre et de nombreuses pierres tant à sa surface - particulièrement dans son secteur Sud - qu'à sa périphérie, sans qu'on puisse parler réellement d'un péristalithe.

#### Historique

Tumulus découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

</section>

<section class="monument">

### [Lecumberry]{.spatial} - [Etxaaté Bizkarra]{.coverage} 4 - [Tumulus-cromlech]{.type}

#### Localisation

Altitude : 1198m.

Un peu en contrebas et au Sud-Est du *Tumulus 2*.

#### Description

Tumulus érigé sur un terrain en légère pente vers l'Ouest, mesurant 3,60m de diamètre, 0,40m de haut ; on note une dépression centrale de 2,20m de diamètre, dont la périphérie est bordée par une dizaine de pierres plus ou moins apparentes.

#### Historique

Tumulus découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

</section>

<section class="monument">

### [Lecumberry]{.spatial} - [Etxaaté Bizkarra]{.coverage} - [Tertre d'habitat]{.type}

On note à 2m à l'Est de *Etxaate Bizkarra 4 - Tumulus-cromlech*, un relief qui paraît pouvoir être rattaché à celui d'un tertre d'habitat mesurant 7m de diamètre, ainsi que 3m à son sommet plat et 0,50m de haut.

#### Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

</section>

<section class="monument">

### [Lecumberry]{.spatial} - [Haltza]{.coverage} - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 782m.

Il est situé à quelques mètres au Sud-Est de la route qui monte vers St-Sauveur, sur le replat d'où se détache la bretelle qui descend vers le ravin de Gastaneguiko erreka.

#### Description

Tumulus de terre circulaire de 8m de diamètre environ, asymétrique (de 1m dans sa plus grande hauteur).

#### Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

</section>

<section class="monument">

### [Lecumberry]{.spatial} - [Harribilibila]{.coverage} Nord - [Tertres d'habitat]{.type}

#### Localisation

Altitude : 770m.

Les 2 tertres se trouvent à l'extrémité Nord Nord-Ouest du haut plateau d'Harribilibila (lieu-dit : Zalgarazareteitzaleta), sur le versant Nord d'une doline.

#### Description

-   *TH 1* : le plus au Nord-Est des deux - Relief très discret mais cependant visible ; diamètre : 5m à 6m.

-   *TH 2* se trouve à 15m au Sud-Est du précédent, de relief encore plus discret ; dimensions semblables.

#### Historique

Tertres découverts par [Blot J.]{.creator} en [avril 2013]{.date}.

</section>

<section class="monument">

### [Lecumberry]{.spatial} - [Hurlastère]{.coverage} - [Tertres d'habitat]{.type}

#### Localisation

Altitude : 510m.

Ces deux tertres sont situés au pied de la colline de ce nom qui porte la route se rendant vers les chalets d'Irati. Au niveau du col d'Haltza se détache, à droite, une piste qui descend vers le ravin où coule un ruisseau dénommé Harrigorrizolako erreka. Au dernier virage de la piste arrivée en bas de sa course, se détachent ces deux magnifiques éminences, que, jusqu'à preuve du contraire nous appellerons tertres d'habitat, leurs caractéristiques y répondant, semble-t-il, mais... sur un mode « majeur » !

#### Description

-   *Tertre n°1* : Le premier dans le virage, sur la droite, dans une prairie en pente douce vers l'Ouest.

    Tertre ovale gazonné, ne présentant aucune trace de structure particulière. Il mesure 25m x 23m et d'une hauteur de plusieurs mètres difficilement appréciable... (3m environ).

-   *Tertre n°2* : situé à environ 45m au Nord Nord-Ouest du précédent, toujours sur un terrain en pente vers l'Ouest. Tertre ovale gazonné.

</section>

<section class="monument">

### [Lecumberry]{.spatial} - [Irati Soro]{.coverage} 5 - [Cromlech]{.type}

#### Localisation

Altitude : 910m.

Difficile à voir, ce cromlech incomplet est situé en contre-bas et à une quinzaine de mètres à l'Est de la route qui vient de Sourcay, sur un terrain en pente légère.

#### Description

Cercle de 6m de diamètre, limité par 5 pierres, essentiellement groupées dans le secteur Ouest.

#### Historique

Cercle peut-être brièvement cité par [J.M. de Barandiaran]{.creator} en [1953]{.date} [@barandiaranHombrePrehistoricoPais1953, p.248] et précisé par [Blot J.]{.creator} en [2014]{.date}.

</section>

<section class="monument">

### [Lecumberry]{.spatial} - [Iraunabako erreka]{.coverage} 1 - [Cromlech]{.type}

#### Localisation

Carte 1346 Est Saint-Jean-Pied-de-Port.

Altitude : 966m.

Il est situé à 250m au Sud Sud-Ouest du col d'Irau, à une confluence de ruisseaux.

#### Description

Cercle de 6,30m de diamètre ; on compte environ 65 pierres réparties, semble-t-il en 2 ou 3 cercles concentriques, plus ou moins bien individualisés suivant les endroits. Près du centre, 2 légères dépressions évoquent la possibilité de fouilles anciennes.

#### Historique

Monument vu en [septembre 1968]{.date} ; il nous avait été signalé par [P. Boucher]{.creator}.

</section>

<section class="monument">

### [Lecumberry]{.spatial} - [Iraunabako erreka]{.coverage} 2 - [Tumulus]{.type}

#### Localisation

À 15m au Nord de *Iraunabako-erreka 1 - Cromlech*.

#### Description

Tumulus mixte, de terre et de pierres, légèrement ovale, à grand axe Est-Ouest. (5m x 2,50m) et atteignant 0,30m de haut.

#### Historique

Monument découvert en [septembre 1968]{.date}.

</section>

<section class="monument">

### [Lecumberry]{.spatial} - [Iraunabako erreka]{.coverage} 3 - [Tumulus]{.type}

#### Localisation

Il est à 150m au Sud Sud-Ouest de *Iraunabako-erreka 1 - Cromlech* et sur la rive droite d'Iraunabako-erreka.

#### Description

Tumulus mixte de 0,50m de haut, légèrement ovale à grand axe Nord-Est Sud-Ouest, (5,20m x 4,40m) et à sommet légèrement aplani au centre duquel on remarque une dépression évoquant une fouille ancienne. Il est constitué de blocs de la taille d'un pavé.

#### Historique

Monument découvert en [1968]{.date}.

</section>

<section class="monument">

### [Lecumberry]{.spatial} - [Iraunabako erreka]{.coverage} 4 - [Tumulus]{.type}

#### Localisation

Il est à 80m au Sud-Ouest de *Iraunabako-erreka 3 - Tumulus*.

#### Description

Tumulus mixte, de pierres et de terre, circulaire, arrondi en dôme, mesurant 4,50m de diamètre et 0,50m de haut.

#### Historique

Monument découvert en [septembre 1968]{.date}. Nous avons pratiqué une fouille de sauvetage en 1988. [@blotTumulusIrauIV1992, p.167]. La datation obtenue (2560 - 2057 BC.) nous indique qu'il s'agit du plus ancien monument à incinération connu en Pays Basque.

</section>

<section class="monument">

### [Lecumberry]{.spatial} - [Iraunabako erreka]{.coverage} 5 - [Cromlech]{.type}

#### Localisation

Carte 1346 Est Saint-Jean-Pied-de-Port.

Altitude : 938m.

Il est sur la rive droite du ruisseau, à 4m de la berge et à une centaine de mètres environ au Nord du défilé rocheux, au niveau de l'extrémité Est de la crête Exaaté-Bizkarra.

#### Description

Cercle de 5m de diamètre, délimité par 11 petits blocs de [grés]{.subject} poudingue, dépassant la surface du sol d'environ 0,10m à 0,30m ; au Nord, on note une dalle couchée vers l'extérieur, mesurant 1m x 0,60m.

#### Historique

Monument découvert en [septembre 1968]{.date}.

</section>

<section class="monument">

### [Lecumberry]{.spatial} - [Iraukolepoa]{.coverage} 2 - [Cromlech]{.type}

#### Localisation

Carte 1346 Est Saint-Jean-Pied-de-Port.

317,228 - 1789,730.

Altitude : 1008.

Il est à 100m au Nord Nord-Ouest du *Cromlech n°1* et à 35m à l'Ouest Nord-Ouest de la piste qui part de la route asphaltée et descend dans la vallée d'Iraunabako-erreka. Le *Cromlech n°1* a été publié [@blotNouveauxVestigesMegalithiques1972a, p. 68] ; il a été rasé par un bulldozer en 1973.

#### Description

Cercle de 5,50m de diamètre délimité par 9 blocs de poudingue qui s'élèvent de 0,20m à 0,30m au-dessus du sol, certains atteignent même 0,70m de long en secteur Ouest.

#### Historique

Monument découvert en [septembre 1972]{.date}.

</section>

<section class="monument">

### [Lecumberry]{.spatial} - [Iraukolepoa]{.coverage} 3 - [Tumulus]{.type}

#### Localisation

Altitude : 1008m.

Il est à 1m au Nord-Est de la route et à 2 m à l'Est du petit parking goudronné.

#### Description

Petit tumulus terreux de 5m de diamètre et quelques centimètres de haut. On note une légère dépression centrale de 1m de diamètre environ.

#### Historique

Tumulus découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

</section>

<section class="monument">

### [Lecumberry]{.spatial} - [Iraukolepoa]{.coverage} 4 - [Tumulus-cromlech]{.type}

#### Localisation

Il est à une quinzaine de mètres au Sud de *Iraukolepoa 3 - Tumulus*.

#### Description

Tumulus mixte de pierres et de terre de 5m de diamètre environ et 0,40m de haut, avec légère dépression centrale de 1,30m de diamètre. Les pierres périphériques sont particulièrement fournies et visibles dans le secteur Ouest.

#### Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

</section>

<section class="monument">

### [Lecumberry]{.spatial} - [Iraukolepoa]{.coverage} 5 - [Cromlech]{.type}

#### Localisation

Il est situé à 14m à l'Est de *Iraukolepoa 4 - Tumulus-cromlech*.

Altitude : 1008m

#### Description

Petit cercle de 3,50m de diamètre, délimité par 4 pierres, peu en relief, dont une centrale.

#### Historique

Cercle découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

</section>

<section class="monument">

### [Lecumberry]{.spatial} - [Iraukolepoa]{.coverage} 6 - [Cromlech]{.type}

#### Localisation

Altitude : 970m.

Situé à mi-pente, à gauche (à l'Est) de la piste qui descend du col.

#### Description

Six pierres au ras du sol délimitent un cercle très net de 2,20m de diamètre.

#### Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

</section>

<section class="monument">

### [Lecumberry]{.spatial} - [Iraukolepoa]{.coverage} 7 - [Cromlech]{.type}

#### Localisation

Altitude : 995m.

À 20m au Sud-Est et de la piste qui descend du col, et donc sur terrain en légère pente vers le Sud-Ouest.

#### Description

7 pierres délimitent un demi-cercle de 7m de diamètre ; la plus grande présente des traces nettes d'épannelage. Dans la moitié Nord gît une pierre, la plus importante, qui mesure 1,50m de long, 0,40m de large et 0,20m d'épaisseur.

#### Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

</section>

<section class="monument">

### [Lecumberry]{.spatial} - [Iraukolepoa]{.coverage} 8 - [Tumulus]{.type}

#### Localisation

À 10m au Nord-Ouest du petit parking goudronné du col d'Irau.

#### Description

Tumulus de terre et de pierres coupé à son tiers par la route. (Il en resterait les 2/3) - Il mesure 11m de diamètre et 0,80m de haut.

#### Historique

Tumulus découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

</section>

<section class="monument">

### [Lecumberry]{.spatial} - [Iraunabako erreka]{.coverage} 6 - [Dolmen]{.type}

#### Localisation

Altitude : 965m.

Il est situé sur la rive gauche du rio.

#### Description

Tumulus de 3,50m de diamètre, mixte (terre et pierres) et 0,40m de haut. Au centre, la [chambre funéraire]{.subject}, orientée Est-Ouest, mesure 1,10m de long, et 0,35m de large. Elle est parfaitement délimitée par 4 dalles qui affleurent la surface du sol : La dalle Sud mesure 0,95m de long et 0,20m de large ; La dalle Nord mesure 0,80m de long, 0,30m de large ; la dalle Ouest : 0,39m de long et 0,12m de large ; la dalle Est : 0,72m de long et 0,13m de large.

Sur le versant Sud-Ouest du tumulus gît la pierre de [couverture]{.subject} mesurant 0,90m de long, 0,40m de large et 0,20m d'épaisseur. Ce monument a fait l'objet d'une fouille clandestine relativement récente.

#### Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

</section>

<section class="monument">

### [Lecumberry]{.spatial} - [Iraunabako erreka]{.coverage} 7 - [Tumulus]{.type}

#### Localisation

À 15m au Sud-Ouest de *Iraunabako erreka 6 - Dolmen*.

#### Description

Tumulus pierreux de 5,50m de diamètre et 0,30 de haut.

#### Historique

Tumulus découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

</section>

<section class="monument">

### [Lecumberry]{.spatial} - [Iraunabako erreka]{.coverage} 8 - [Cromlech]{.type}

#### Localisation

À 6m au Sud de *Iraunabako erreka 7 - Tumulus*.

#### Description

De nombreuses grosses pierres enfouies dans le sol délimitent les ¾ d'un cercle de 3,80m de diamètre...

#### Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

</section>

<section class="monument">

### [Lecumberry]{.spatial} - [Iraunabako erreka]{.coverage} 9 - [Tertre d'habitat]{.type}

#### Localisation

À 30m au Sud Sud-Ouest de *Iraunabako-erreka 5 - Cromlech*.

Altitude : 938m

#### Description

Tertre ovalaire, asymétrique de 11m de long, 6m de large et 1,10m de haut dans sa partie la plus élevée.

#### Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

</section>

<section class="monument">

### [Lecumberry]{.spatial} - [Iraunabako erreka]{.coverage} 10 - [Tertre d'habitat]{.type}

#### Localisation

Il est à 80m au Sud Sud-Ouest de *Iraunabako-erreka 5 - Cromlech*, au pied de la colline d'Harluze.

#### Description

Tertre ovalaire asymétrique, mesurant 11m de long, 6m de large et 1,10m de haut.

#### Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

</section>

<section class="monument">

### [Lecumberry]{.spatial} - [Iraunabako erreka]{.coverage} 11 - [Tumulus-cromlech]{.type}

#### Localisation

À 20m au Nord de *Iraunabako-erreka 5 - Cromlech*.

#### Description

Petit tumulus circulaire, discret sous la végétation, mesurant 2,10m de diamètre et limité par 8 pierres périphériques.

#### Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

</section>

<section class="monument">

### [Lecumberry]{.spatial} - [Iraunabako erreka]{.coverage} 12 - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 938m.

Il est à 150m au Nord-Ouest de *Iraunabako-erreka 5 - Cromlech*, le long de la piste pastorale.

#### Description

Tertre ovale de 10m de long, 3m de large et 0,80m de haut.

#### Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

</section>

<section class="monument">

### [Lecumberry]{.spatial} - [Iraunabako erreka]{.coverage} 13 - [Cromlech]{.type}

#### Localisation

Altitude : 976m.

Il est situé à la confluence de petits rius qui vont constituer la naissance du rio Iraty, immédiatement en contre bas et au Sud du col d'Irau.

#### Description

Petit cercle discret, de 3,10m de diamètre, délimité par 8 pierres au ras du sol.

#### Historique

Monument découvert par [Blot J.]{.creator} en [mai 2016]{.date}.

À noter qu'à une centaine de mètres plus au Nord, là où le vallon se rétrécit, on peut voir une structure très semblable à *T2*, en gros galets, mais de forme imprécise (effet de la nature ou remaniement d'une structure anthropique par l'effet des eaux par exemple ?).

</section>

<section class="monument">

### [Lecumberry]{.spatial} - [Iraubazter vallon]{.coverage} - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 940m.

Situé près de la confluence du riu Iraubazter avec le rio Irati, en bordure des terres inondées, marécageuses.

#### Description

Tertre terreux asymétrique de forme classique, mesurant 4m de diamètre et 0,40m de haut.

#### Historique

Monument découvert par [Blot J.]{.creator} en [juillet 2016]{.date}.

</section>

<section class="monument">

### [Lecumberry]{.spatial} - [Iraubazter vallon]{.coverage} 1 - [Tumulus]{.type}

#### Localisation

Altitude : 941m.

Il est à 16m au Nord-Ouest de *Iraubazter vallon - Tertre d'habitat*.

#### Description

Tumulus terreux très net, circulaire en forme de galette, de 4m de diamètre et 0,40m de haut, présentant une légère excavation en son centre (fouille ancienne ?). Quelques pierres sont visibles en surface.

#### Historique

Tumulus découvert par [Blot J.]{.creator} en [juillet 2016]{.date}.

</section>

<section class="monument">

### [Lecumberry]{.spatial} - [Iraubazter vallon]{.coverage} 2 - [Tumulus]{.type}

#### Localisation

Altitude : 941m.

Il est à 4m à l'Ouest Nord-Ouest de *Iraubazter vallon 1 - Tumulus*.

#### Description

Tumulus en forme de galette aplati, de 7m de diamètre et 0,40m de haut, présentant une excavation centrale de 4m de diamètre environ et 0,30m de profondeur. De nombreuses pierres sont visibles en surface.

#### Historique

Tumulus découvert par [Blot J.]{.creator} en [juillet 2016]{.date}.

</section>

<section class="monument">

### [Lecumberry]{.spatial} - [Iraubazter vallon]{.coverage} 3 - [Tumulus]{.type}

#### Localisation

Il est situé à 8m au Nord-Ouest de *Iraubazter vallon 2 - Tumulus*.

#### Description

Petit tumulus circulaire, de forme légèrement conique, de 2,50m de diamètre et 0,40m de haut ; de nombreuses pierres apparaissent en surface.

#### Historique

Tumulus découvert par [Blot J.]{.creator} en [juillet 2016]{.date}.

</section>

<section class="monument">

### [Lecumberry]{.spatial} - [Jatsélépoa]{.coverage} - [Camp protohistorique]{.type} ([?]{.douteux})

Celui-ci domine au Nord-Est le col de ce nom et, au Nord-Ouest, la ferme Ondarneborda.

Altitude : 463m.

#### Description

Cet ensemble se répartit en un premier espace situé au plus haut, de forme ovale, entourée d'une levée de terre ayant pu supporter des palissades, auquel on accède directement par le col, et, au Nord-Est, d'une étendue quasi horizontale, où se distinguent nettement des levées de terre ayant pu faire partie d'un système de « pré-défense ».

#### Historique

Cet ensemble découvert par [Meyrat F.]{.creator} en [novembre 2013]{.date}.

</section>

<section class="monument">

### [Lecumberry]{.spatial} - [Négousaro]{.coverage} - [Tertres d'habitat]{.type} ([?]{.douteux})

#### Localisation

Altitude : 1040m.

Les 6 tertres sont à flanc de colline, sur terrain très en pente, à une quarantaine de mètres en dessous de l'ancien cayolar de J.P. Lorran [@blotArtzainakBergersBasques1989], et à 40m au-dessus et au Sud du ruisseau en contre bas.

#### Description

Quatre tertres bien visibles, légèrement ovales, se succèdent, sur une ligne horizontale orientée Sud-Est Nord-Ouest ; leurs dimensions varient de deux à trois mètres de large pour 4m de long et une hauteur de 0,50m en moyenne. À 4m au-dessus du *tertre n°3*, on note un relief discret mais bien visible, reste du tertre n°5, de dimensions semblables ; enfin, au-dessous et à 4m au Nord du *tertre n°4*, on voit les restes du *tertre n°6* (6m de long, 4m de large, 0,20m de haut). Monuments cependant tous très douteux.

#### Historique

Tertres trouvés par [Blot J.]{.creator} et [Meyrat F.]{.creator} en [août 2013]{.date}.

</section>

<section class="monument">

### [Lecumberry]{.spatial} - [Toscako erreka]{.coverage} - [Tertres d'habitat]{.type}

#### Localisation

Altitude : 900m.

Les 14 tertres d'habitat sont situés au Sud et en contre-bas de la route qui se rend à Néquécharré (ou Négousaro), un peu avant le virage qui correspond à son franchissement du ruisseau Toscako erreka. On les trouve répartis selon un axe Est-Ouest, le long d'anciennes pistes pastorales dont une se dirige vers les ruines d'un ancien cayolar, près du ruisseau, c'est à partir de ce dernier (coordonnées indiquées ci-dessus) que nous ferons la description des tertres :

#### Description

Ils sont tous de forme ovale et érigés sur un terrain en pente vers le Sud.

-   *Tertre n°1* : situé à 8m au Nord-Ouest des ruines du cayolar. Mesure 13m x 10m et 1m de haut.

-   *Tertre n°2* : situé à 75m environ au Nord-Ouest du précédent. Mesure 14m x 8m et 0,50m de haut.

-   *Tertre n°3* : situé à 50m à l'Ouest du n°1 et à 35m au Sud du n°2 (découvert par [Meyrat F.]{.creator} en [2014]{.date})

-   *Tertre n°4* : situé à 7m au Sud du n°9 et 20m à l'Est du n°1. Mesure 14m x 9m et 1m de haut.

-   *Tertre n°5* : situé à 25m au Sud du n°6 et à 7m au Nord du n°4. Mesure 10m x 8m et 0,50m de haut.

-   *Tertre n°6* : Situé à environ 20m au Sud du n°9 et 25m au Nord du n°5. Mesure 14m x 9m et 1,50m de haut.

-   *Tertre n°7* : Situé à 10m environ à l'Ouest du n°6 et 80m à l'Est du n°2. Mesure 14m x 7m et 0,80m de haut.

-   *Tertre n°8* : Situé à 10m à l'Est du n°6. Mesure 10m x 8m et 0,80m de haut.

-   *Tertre n°9* : situé à 27m à l'Est du n°10 et 20m au Nord du 6. Mesure 14m x 9m et 0,50m de haut.

-   *Tertre n°10* : Situé à 75m au Nord-Est du n°2 et 27m à l'Ouest du n°9. Mesure 8m x 7m et 0,30m de haut.

-   *Tertre n°11* : Situé à 30m à l'Est du n°9 ; mesure 13m x 7m et 0,30m de haut.

-   *Tertre n°12* : à une trentaine de mètres sous le tracé de la route qui mène à Négousaro et au Nord du 9. Tertre peu marqué.

-   *Tertre n°13* : à 20m à l'Est du n°12. - Tertre peu marqué.

-   *Tertre n°14* : à une quarantaine de mètres à l'Est du n°13, relief mieux marqué que les 2 précédents ; de nombreuses pierres y sont visibles.

#### Historique

Tertres découverts par [Bot J.]{.creator} en [1974]{.date}.

</section>

## [Lecumberry]{.spatial} - La nécropole d'[Okabé]{.coverage}

La région d'Okabé a fait essentiellement l'objet de 2 publications de P. Gombault [@gombaultTumulusEnceintesFuneraires1914, p. 65] et [@gombaultProposCromlechsOkabe1935, p.391], décrivant les cromlechs de 1 à 17 ; puis nous-mêmes [@blotNouveauxVestigesMegalithiques1972b, p.58] publions les monuments de 18 à 26. En 1978, nous pratiquons une fouille de sauvetage sur le cromlech n° 6 [@blotCromlechOkabeBasse1978, p. 1]. Datation obtenue : 767-216 BC.

Les 6 monument décrits ci-après complètent cet inventaire.

<section class="monument">

### [Lecumberry]{.spatial} - [Okabé]{.coverage} 27 - [Tumulus]{.type}

#### Localisation

Carte 1346 Est Saint-Jean-Pied-de-Port.

Altitude : 1340m.

Le *Tumulus 27* est situé à 40m à l'Est Sud-Est du n°11.

#### Description

Petit amas bien circonscrit d'un diamètre de 2m et 0,30m de haut, formé d'environ une dizaine de pierres blanchâtres, de la taille d'un gros pavé.

#### Historique

Monument découvert en [octobre 1978]{.date}.

</section>

<section class="monument">

### [Lecumberry]{.spatial} - [Okabé]{.coverage} 28 - [Tumulus-cromlech]{.type}

#### Localisation

Il est tangent au Nord Nord-Ouest du n°24.

#### Description

Tumulus terreux de 4,20m de diamètre et 0,30m de haut, délimité par 6 pierres ; une pierre est visible au centre.

#### Historique

Monument découvert en [octobre 1978]{.date}.

</section>

<section class="monument">

### [Lecumberry]{.spatial} - [Okabé]{.coverage} 29 - [Tumulus]{.type}

#### Localisation

Il est à 4m au Nord du n°24.

#### Description

Petit tumulus terreux de 2,20 de diamètre et 0,20 de haut ; 2 pierres sont visibles à sa périphérie en secteur Nord-Ouest.

#### Historique

Monument découvert en [octobre 1978]{.date}.

</section>

<section class="monument">

### [Lecumberry]{.spatial} - [Okabé]{.coverage} 30 - [Cromlech]{.type}

#### Localisation

Il est à 12m au Nord-Ouest du n°1.

#### Description

7 pierres nettement visibles sont disposées en arc de cercle qui représente en réalité la moitié Nord d'un cercle de 5m de diamètre.

#### Historique

Monument découvert en [octobre 1978]{.date}.

</section>

<section class="monument">

### [Lecumberry]{.spatial} - [Okabé]{.coverage} 31 - [Cromlech]{.type}

#### Localisation

On le trouve sur un petit replat à 100m au Sud Sud-Est du n°1.

#### Description

Cercle de 3,50m de diamètre délimité par 7 pierres, au ras du sol, mais bien visibles. Edifié sur un terrain en légère pente vers le Nord-Est.

#### Historique

Monument découvert en [octobre 1978]{.date}.

</section>

<section class="monument">

### [Lecumberry]{.spatial} - [Okabé]{.coverage} 32 - [Tumulus]{.type}

#### Localisation

Il est à 10m au Nord-Ouest de *Okabé 31 - Cromlech*.

#### Description

Petit amas pierreux circulaire, de 2m de diamètre et 0,30m de haut, en partie dissimulé sous la bruyère naine.

#### Historique

Monument découvert en [mai 1979]{.date}.

</section>

<section class="monument">

### [Lecumberry]{.spatial} - [Okabé]{.coverage} 33 - [Tumulus]{.type}

#### Localisation

Altitude : 1382m.

Il est à 6m environ au Nord-Ouest de *Okabé 13 - Cromlech*.

#### Description

Tumulus très peu marqué en hauteur, mesurant 5,30m de diamètre, constitué de nombreuses pierres de faible volume émergeant de la surface du sol.

#### Historique

Tumulus découvert par [Blot J.]{.creator} en [août 2013]{.date}.

</section>

# Macaye

<section class="monument">

### [Macaye]{.spatial} - [Bordaleku]{.coverage} - [Tertre d'habitat]{.type}

Nous rappelons ici que 3 tertres (et un monolithe) ont été publiés déjà été publiés par nous [@blotMonolithesPaysBasque1983, p.11]. Les 6 tertres ci-après décrits sont moins visibles, mais à proximité immédiate des précédents.

Pour mémoire rappelons que :

-   *TH n°1* : est à 21m au Nord du monolithe ; Mesure 10m de diamètre et 1,30m de haut.

-   *TH n°2* : à 25m au Nord du précédent ; mesure 9m de diamètre et 1,80m de haut.

-   *TH n°3* : à 7m au Nord du précédent ; Tertre de forme oblongue mesure 15m x10m et près de 2,80 de haut - On note à sa surface aplanie, quelques pierres disposées en U qui pourraient avoir délimité l'ancien habitat érigé sur ce tertre.

Les six tertres suivants ont été trouvés en [mai 2013]{.date} par [F. Meyrat]{.creator} :

-   *Tertre n°4* : Situé à 20m au Nord Nord-Ouest du précédent. Tertre (?) discret de 5m de diamètre, 0,40m de haut.

-   *Tertre n°5* : Situé à 5m au Nord Nord-Est du précédent. Lui aussi tertre (?) discret. Mesure 6m de diamètre et 0,30m de haut.

-   *Tertre n°6* : Situé au Sud-Est et en contre-bas du précédent, sur la pente ; mesure 6m de diamètre et 1m de haut.

-   *Tertre n°7* : situé au Nord Nord-Est du précédent, de l'autre côté de la piste et près d'un rocher et sur terrain en pente. Mesure7m de diamètre et 1m de haut.

-   *Tertre n°8* : à 17m au Sud-Est du précédent. Longé par la piste qui passe à son sommet. Mesure 9m x 7m et 1m de haut.

-   *Tertre n°9* : à 6m au Sud-Est du précédent et à 8m du ruisseau que franchit la piste. Mesure 6m de diamètre et 1m de haut.

</section>

<section class="monument">

### [Macaye]{.spatial} - [Harguibele]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 573m.

Il est situé à l'Est du mont Ursuya, tangent, au Sud, à la piste de crête qui s'y rend.

#### Description

Tumulus circulaire de 6m de diamètre mais de faible hauteur (0,15m à 0,25m de haut). Environ 10 pierres apparaissent à sa surface, sans que l'on puisse parler de péristalithe.

#### Historique

Monument trouvé par [F. Meyrat]{.creator} en [juin 2014]{.date}.

</section>

<section class="monument">

### [Macaye]{.spatial} - [Hiriguibel]{.coverage} - [Cromlech]{.type}

#### Localisation

Altitude : 595m.

Situé à 150m environ au Nord-Ouest du Tumulus Hiriguibel, à 5m au Nord de la piste de crête qui monte au sommet de l'Ursuya et à 15m au Sud d'une autre piste s'y rendant aussi.

#### Description

On note une quinzaine de pierres affleurant plus ou moins en demi-cercle la surface du sol. Elles sont visibles dans la moitié Nord d'un cercle de 4,30m de diamètre, matérialisé par un léger relief en forme de bourrelet.

#### Historique

Cromlech découvert par [P. Badiola]{.creator} en [janvier 2016]{.date}.

</section>

<section class="monument">

### [Macaye]{.spatial} - [Larrondoa]{.coverage} - [Tumulus]{.type}

#### Localisation

Carte 1345 Ouest Cambo-les-Bains.

Altitude : 315m.

Il est situé à l'Ouest de la piste pastorale de crête qui longe le sommet d'une croupe étendue au Nord Nord-Ouest du col Sendalepoa (au Nord de Louhossoa). Il est situé, à la cote 315, à 100m à l'Ouest Sud-Ouest de la ferme Larrondoa.

#### Description

Tumulus mixte de pierres et terre, de 16m de diamètre et 0,80m de haut.

#### Historique

Monument découvert en [mai 1973]{.date}. Il a depuis été rasé par les labours ; on pouvait voir après le premier passage de la charrue, la pierraille étalée sur une large surface circulaire, mélangée, au centre, avec des fragments de charbons de bois.

</section>

<section class="monument">

### [Macaye]{.spatial} - [Mendizabale]{.coverage} Sud - [Tumulus]{.type}

#### Localisation

Altitude : 860m

#### Description

Tumulus (douteux ?) de terre, de 5m à 6m de diamètre et 0,80m de haut. Pourrait être un mouvement naturel du terrain.

#### Historique

Monument trouvé par [Blot J.]{.creator} en [février 2013]{.date}.

</section>

<section class="monument">

### [Macaye]{.spatial} - [Ursuya]{.coverage} 8 - [Tumulus]{.type}

#### Localisation

Altitude : 575m.

Ce petit tumulus est situé tout au sommet d'une petite éminence qui domine au Nord-Ouest un petit col, lui-même au Nord-Ouest du mont Ursuya.

#### Description

Petit tumulus en forme de galette aplatie de 2,30m de diamètre et quelques centimètres de haut. Une vingtaine de petites pierres blanche ([calcaire]{.subject}) apparaissent à la surface du sol.

#### Historique

Monument découvert par [Blot J.]{.creator} en [janvier 2013]{.date}.

</section>

# Mauléon

<section class="monument">

### [Mauléon]{.spatial} - [Tibarenne]{.coverage} Est - [Tumulus]{.type} (DETRUIT)

#### Localisation

Altitude : 422m.

#### Historique

Il était situé à 10m au Nord-Ouest du premier émetteur télé ayant été érigé au sommet de la colline dénommée Tibarenne, dominant Mauléon ; il était le seul émetteur au moment de notre découverte de ce tumulus et de sa publication : [@blotNouveauxVestigesProtohistoriques1975, p.123].

Depuis, deux autres émetteurs ont été construits, ainsi qu'une route d'accès qui a complètement rasé ce magnifique tumulus qui mesurait 17m de diamètre et 1m de haut, avec une excavation centrale de 2m de large et d'une dizaine de centimètre de profondeur.

Il n'en reste maintenant qu'une très faible ondulation au niveau du tracé routier...

[1975]{.date}, [J. Blot]{.creator}.

</section>

# Mendionde

<section class="monument">

### [Mendionde]{.spatial} - [Gagnekordoki]{.coverage} - [Monolithe]{.type} ([?]{.douteux})

#### Localisation

Altitude : 591m.

Il est situé à environ 2m au Sud-Ouest de la piste qui monte au sommet de l'Ursuya et à 100m environ au Nord-Ouest du tumulus Gagnekordoki.

#### Description

Volumineux bloc de [grés]{.subject} de forme allongée, à sommet arrondi au Nord-Ouest et à large base Sud-Est. Il mesure 3,05m de long, une largeur maximale, au centre, de 1,85m. Son épaisseur est variable : au centre elle atteint 0,90m, pour diminuer à sa base (0,70m) et encore plus au sommet (0,20m). Son pourtour semble présenter quelques traces d'épannelage (?) au sommet et le long du bord Sud. Ce bloc rocheux est bien individualisé, mais semble être d'origine strictement locale : fait-il partie de l'ensemble de pierres en place dont 5 éléments apparaissent au voisinage immédiat de son bord Nord-Est ?

#### Historique

Ce possible monolithe a été signalé par [G. Laporte]{.creator} en [juin 2014]{.date}.

</section>

<section class="monument">

### [Mendionde]{.spatial} - [Gagnekordokia]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 589m.

Il est situé à environ 400m à l'Est du monument décrit précédemment à *Tumulus Harguibele* (à Macaye), il est lui aussi proche de la piste de crête se rendant au mont Ursuya (à 1,20m au Sud de celle-ci).

#### Description

On note une formation circulaire constituée d'une cinquantaine de blocs de [calcaire]{.subject} blanc, plus ou moins du volume d'un pavé, groupés sur le sol. Cette disposition qui ne paraît pas naturelle et dont la hauteur n'est que celle des pierres qui émergent du sol, nous évoque cependant la possibilité d'un « tumulus » dont nous avons déjà rencontré de tels exemplaires.

#### Historique

Monument découvert par [Meyrat F.]{.creator} en [juin 2014]{.date}.

</section>

<section class="monument">

### [Mendionde]{.spatial} - [Pagazaharreta]{.coverage} 1 - [Cromlech]{.type}

#### Localisation

Altitude : 430m.

Il est situé à environ 3m au Sud Sud-Ouest de la piste pastorale et de la pierre couchée dont la description suit.

#### Description

Cercle de 6m de diamètre, assez irrégulier, délimité par environ une quinzaine de pierres assez visibles.

#### Historique

Ce monument a été découvert en [janvier 2015]{.date} par [I. Txintxuretta]{.creator}. Il nous est apparu fort [douteux]{.douteux}.

</section>

<section class="monument">

### [Mendionde]{.spatial} - [Pagazaharreta]{.coverage} 2 - [Pierre tombée]{.type}

#### Localisation

Altitude : 430m.

Elle est à 2m au Sud Sud-Ouest de *Pagozaharreta 1 - Cromlech* et tangent à la piste pastorale.

#### Description

Bloc de [grés]{.subject} grossièrement parallélépipédique à grand axe orienté Sud-Ouest Nord-Est. Il mesure 1,80m de long, 1m de large et 0,50m d'épaisseur ; on note de nombreuses traces d'épannelage sur ses bords Sud-Ouest et aussi, moins nombreuses, au bord Nord-Ouest.

#### Historique

Ce monument a été découvert en [janvier 2015]{.date} par [I. Txintxuretta]{.creator}. Il nous est apparu fort [douteux]{.douteux}.

</section>

<section class="monument">

### [Mendionde]{.spatial} - [Pagazaharreta]{.coverage} 3 - [Tumulus]{.type} ([?]{.douteux})

#### Localisation

Il est situé juste en face de *Pagozaharreta 2 - Pierre tombée*, au Nord de la piste pastorale et tangent à celle-ci.

#### Description

Tumulus de terre d'environ 4,90m de diamètre et 0,60m de haut ; il est recouvert d'un volumineux roncier ; quelques pierres sont visibles en surface, sans ordre apparent.

#### Historique

Ce monument a été découvert en [janvier 2015]{.date} par [I. Txintxuretta]{.creator}. Il nous est apparu fort douteux.

</section>

<section class="monument">

### [Mendionde]{.spatial} - [Pagazaharreta]{.coverage} 6 - [Cromlech]{.type} ([?]{.douteux})

Monument lui aussi très douteux.

#### Localisation

Altitude : 419m

Situé entre 2 petits rius, à environ une cinquantaine de mètres à l'Ouest Nord-Ouest de *Pagozaharreta 2 - Pierre tombée*.

#### Description

Cercle (?) de 3,60m de diamètre, au nombre de pierres impossible à préciser...

</section>

<section class="monument">

### [Mendionde]{.spatial} - [Pagazaharreta]{.coverage} 7 - [Pierre dressée]{.type} ([?]{.douteux})

#### Localisation

Altitude : 415m.

Située à une quarantaine de mètres à l'Ouest Nord-Ouest de *Pagazaharetta 6 - Cromlech*.

#### Description

Bloc de [grés]{.subject} planté dans le sol, en forme de pain de sucre à sommet tronqué. Sa base mesure 0,64m de long et 0,48m de large ; il est haut de 0,78m et son sommet a 0,23m de large. Sa face Nord présente de nombreuses retouches, ainsi que ses arêtes.

#### Historique

Ce monument a été découvert en [janvier 2015]{.date} par [I. Txintxuretta]{.creator}. Il nous est apparu fort douteux.

</section>

<section class="monument">

### [Mendionde]{.spatial} - [Pagazaharreta]{.coverage} 8 - [Cromlech]{.type} ([?]{.douteux})

#### Localisation

Altitude : 415m.

Il est situé à 5m au Sud-Ouest de *Pagazaharetta 7 - Pierre dressée*.

#### Description

Cercle de 4,70m de diamètre, délimité par une dizaine de pierres peu évidentes.

#### Historique

Ce monument a été découvert en [janvier 2015]{.date} par [I. Txintxuretta]{.creator}. Il nous est apparu fort douteux.

</section>

# Mendive

<section class="monument">

### [Mendive]{.spatial} - [Apanice]{.coverage} - [Tertre d'habitat]{.type}

#### Localisation

On le trouve à 35m environ au Sud-Ouest de la route et à quelques dizaines de mètres au Nord de l'embranchement qui conduit au menhir d'Ithurroché.

#### Description

Tertre de terre de 15m de diamètre environ, à sommet discrètement arrondi et de 0,30m de haut, au Nord-Est, et jusqu' à 1m au Sud-Ouest.

#### Historique

Tertre découvert par [A. Martinez Manteca]{.creator} en [juillet 2011]{.date}.

</section>

<section class="monument">

### [Mendive]{.spatial} - [Artzainharria]{.coverage} haut - [Tertres d'habitat]{.type}

#### Localisation

Ils sont répartis sur le flanc nord d'une éminence qui fait face au pic de Béhorlégui ; et qui est séparée du *Groupe de TH* dit *du bas* par un petit col ou aboutit le chemin et où est construit le cayolar Idioineko Borda.

#### Description

Un ensemble de 11 tertres de terre est érigé sur cette pente assez marquée, séparés par des distances comprises entre 10m et 20m. Ces tertres circulaires, asymétriques, ont des diamètres de 7 à 8 m en moyenne et des hauteurs de 0,50m à 0,80m environ.

À noter, au sommet de cette éminence, à son extrémité Sud-Est, un petit tertre en bordure d'une dépression (artificielle ?).

#### Historique

Tertres découverts par [J. Blot]{.creator} en [avril 2013]{.date}.

</section>

<section class="monument">

### [Mendive]{.spatial} - [Artzainharria]{.coverage} bas - [Tertres d'habitat]{.type}

#### Localisation

De l'autre côté du col et donc au Nord-Est du cayolar Idioineko Borda.

#### Description

Quatre TH de terre, érigés sur la partie la plus élevée de cette prairie.

-   *TH 1* : le plus grand, le plus visible, mesure 17m de diamètre et 1,50m de haut.

-   *TH 2* : situé à 1m au Nord-Est ; moins net, mesure 11m de diamètre et 0,80m de haut.

-   *TH 3* : situé à 10m au Sud-Est du n°1. ; mesure 11m de diamètre et 0,50m de haut.

-   *TH 4* : situé à 10m au Sud du n°3, très visible, mesure 16m de diamètre et 1,40m de haut.

#### Historique

Tertres découverts par [Blot J.]{.creator} en [1971]{.date}, et contrôlés en [avril 2013]{.date}.

</section>

<section class="monument">

### [Mendive]{.spatial} - [Béhorlégi]{.coverage} 1 - [Cromlech]{.type}

#### Localisation

Carte 1346 est. Saint-Jean-Pied-de-Port.

Altitude 870m.

Un ensemble de 3 cromlechs et 1 tumulus est situé sur un replat à gauche de la route qui monte de Mendive au flanc sud du pic de Béhorlégi. Ce site domine un autre replat, en contre-bas et au sud, dénommé Sare-Sare.

#### Description

Le *cromlech n°1* mesure 6m de diamètre, est délimité par 12 pierres de [grés]{.subject} blanc, bien visibles ; il est érigé sur un sol en légère pente vers le sud-ouest et tangent à l'est au *n°2*.

#### Historique

Monument découvert en [avril 1973]{.date}.

</section>

<section class="monument">

### [Mendive]{.spatial} - [Béhorlégi]{.coverage} 2 - [Cromlech]{.type}

#### Localisation

Il est tangent au sud-est du cromlech *Béhorlégi 1 - Cromlech*.

#### Description

Cercle de 7,50m de diamètre, délimité par 10 pierres en grès blanc de 0,30m à 0,40m de haut.

#### Historique

Monument découvert en [avril 1973]{.date}.

</section>

<section class="monument">

### [Mendive]{.spatial} - [Béhorlégi]{.coverage} 3 - [Tumulus]{.type}

#### Localisation

À environ 80m à l'est du *Cromlech n°1*, aux deux tiers de la montée qui débute sur le replat où se trouvent les *cromlechs 1* et *2*.

#### Description

Tumulus mixte de terre et de pierres, érigé sur un sol en légère pente vers le nord-ouest ; il mesure 3,70m de diamètre et 0,40m de haut.

#### Historique

Monument découvert en [avril 1973]{.date}.

</section>

<section class="monument">

### [Mendive]{.spatial} - [Béhorlégi]{.coverage} 4 - [Cromlech]{.type}

#### Localisation

À environ 130m au sud-est du cromlech *Béhorlégi 2 - Cromlech*, il est situé sur la piste pastorale qui a un parcours légèrement ascendant quand on vient du *n°2*.

#### Description

Cercle de 6m de diamètre, délimité par 22 pierres bien visibles, atteignant 0,30m à 0,40m de haut. On distingue très nettement 4 pierres centrales.

#### Historique

Monument découvert en [avril 1973]{.date}.

</section>

<section class="monument">

### [Mendive]{.spatial} - [Burkidoy]{.coverage} Ouest - [Tumulus]{.type}

#### Localisation

Altitude : 1071m

Ce tumulus est situé au sommet d'un promontoire qui domine la D 19, à l'ouest du mont Burkidoy. Ce monument, ainsi que le suivant, n'est visible que lorsque les fougères sont à leur minimum...

#### Description

Tumulus terreux, en forme de galette aplatie, mesurant 3,50m de diamètre et 0,40m de haut. À sa périphérie Nord-Est se voit une pierre plantée dont il est difficile de dire quels sont ses rapports avec le tumulus...

#### Historique

Tumulus découvert par [J. Blot]{.creator} en [février 2020]{.date}.

</section>

<section class="monument">

### [Mendive]{.spatial} - [Burkidoy]{.coverage} Ouest - [Cromlech]{.type} ([?]{.douteux})

#### Localisation

Ce cromlech est situé à environ 5m au Nord-Est de *Burkidoy Ouest - Tumulus*.

#### Description

Cinq pierres de volume très variable, délimitent un cercle de 3m de diamètre, plus fourni dans le secteur Nord-Est. Monument douteux.

#### Historique

Monument découvert par [J. Blot]{.creator} en [février 2020]{.date}.

</section>

<section class="monument">

### [Mendive]{.spatial} - [Burkidoy]{.coverage} - [Tertres d'habitat]{.type} (nb = 4)

#### Localisation

Alt : 1291m.

À environ 300mau nord du cayolar Cihigolatzé, en suivant l'ancienne piste qui monte vers les pâturages, (au-dessus du tracé actuel, qui s'en détache environ 80m au Nord du cayolar).

#### Description

Tous ces tertres sont érigés sur un terrain en pente vers le Sud-Ouest.

-   *Tertre n°1* : Il est situé à une trentaine de mètres au Nord-Ouest des ruines bien visibles d'un très ancien cayolar, où aboutissait une bretelle de l'ancienne piste. Mesure 5m de diamètre et 0,50m de haut.

-   *Tertre n°2* : à 3m à l'Ouest du précédent, de l'autre côté de la piste, et mêmes mensurations.

-   *Tertre n°3* : à 4m au Nord Nord-Est du n°1 ; mesure 3m de diamètre et 0,30m de haut.

-   *Tertre n°4* : situé à 9m à l'Ouest Sud-Ouest du n°2 ; mesure 3m de diamètre et 0,30m de haut.

#### Historique

Tertres découverts par [Blot J.]{.creator} en [1975]{.date}.

</section>

<section class="monument">

### [Mendive]{.spatial} - [Calichaga]{.coverage} - [Cromlech]{.type}

#### Localisation

Carte 1346 est. Saint-Jean-Pied-de-Port

Altitude 489m.

Ce monument est situé sur un pâturage à 90m au Nord-Ouest du virage de la route qui va de Mendive à Ahuski, par Armiague, au moment où elle passe du flanc nord au flanc sud de la ligne de croupes baptisée *Calichaga eta Pegaretta*.

#### Description

Cercle de 7m de diamètre, délimité par 14 pierres au ras du sol, mais nettement visibles. Le monument a été détérioré par la pose d'une barrière barbelée qui le coupe selon un axe est-ouest dans son secteur nord.

#### Historique

Monument découvert en [avril 1973]{.date}.

</section>

<section class="monument">

### [Mendive]{.spatial} - [Calichaga]{.coverage} - [Tumulus]{.type}

#### Localisation

Alt : 489m.

Il se trouve au point culminant du même replat que le cromlech de ce nom et à 15m au Sud-Est de la première barrière de barbelés (qui coupe ce dernier).

#### Description

Tumulus de terre de 3,30m de diamètre et 0,30m à 0,40m de haut.

#### Historique

Monument découvert par [Blot J.]{.creator} en [août 2011]{.date}.

</section>

<section class="monument">

### [Mendive]{.spatial} - [Cihigolatzé]{.coverage} - [Tertre d'habitat]{.type}

#### Localisation

Alt : 1241m

Le tertre est situé à une trentaine de mètres à l'Ouest des ruines du cayolar, et donne sur la rive droite du ruisseau en contre-bas.

#### Description

Tertre bien visible, de 8m de diamètre environ et 0,40m de haut.

#### Historique

Monument découvert par [Blot J.]{.creator} en [avril 2014]{.date}.

</section>

<section class="monument">

### [Mendive]{.spatial} - [Egurce]{.coverage} - [Monolithe]{.type}

#### Localisation

Alt : 995m.

Il est situé dans le deuxième petit col après la montée.

#### Description

Belle dalle de [grés]{.subject} parallélépipédique gisant au sol suivant un axe Ouest-Est, mesurant 2,30m de long, 0,90m de large, 0,27m d'épaisseur et présentant des traces d'épannelage sur tout son pourtour semble-t-il.

#### Historique

Monolithe découvert par [Alfonso Martinez Manteca]{.creator} en [juillet 2011]{.date}.

</section>

<section class="monument">

### [Mendive]{.spatial} - [Egurce]{.coverage} - [Tertres d'habitat]{.type}

#### Localisation

Alt : 970m

Un premier ensemble de 6 TH se trouve au niveau d'un premier petit col après la montée ; 4 autres se trouvent avant un deuxième col, enfin 2 ou 3 autres TH sont érigés au nord du monolithe ci-après décrit dans ce 2^è^ col.

#### Description

Mêmes formes et dimensions variables que *ceux d'Egurcekobidea*.

#### Historique

Tertres découverts par [A. Martinez Manteca]{.creator} en [juillet 2011]{.date}.

</section>

<section class="monument">

### [Mendive]{.spatial} - [Egurcekolepoa]{.coverage} 1 - [Tertre d'habitat]{.type}

#### Localisation

Alt : 913m

Erigé sur la gauche du col, en montant.

#### Description

Tertre de terre d'une quinzaine de mètres de diamètre, et 3m environ dans sa plus grande hauteur, à l'Est.

#### Historique

Tertre découvert par [A. Martinez Manteca]{.creator} en [juillet 2011]{.date}.

</section>

<section class="monument">

### [Mendive]{.spatial} - [Egurcekobidea]{.coverage} - [Tertres d'habitat]{.type}

#### Localisation

Alt : 970m

Cette trentaine de tertres est échelonnée de part et d'autre d'une piste qui, partant du col d'Egurce, monte vers l'Est.

#### Description

Certains sont très marqués, d'autres beaucoup plus érodés, les dimensions varient entre 12m et 15m de diamètre, pour des hauteurs variables.

#### Historique

Tertres découverts par [A. Martinez Manteca]{.creator} en [juillet 2011]{.date}.

</section>

<section class="monument">

### [Mendive]{.spatial} - [Gahalarbe]{.coverage} 2 - [Tumulus]{.type}

#### Localisation

Même carte.

Altitude : 947m.

Nous avons publié [@blotNouveauxVestigesMegalithiques1972b, p.50], le *Cromlech n°1*, qui a été ultérieurement détruit par l'aménagement de la piste pastorale.

Altitude : 970m.

Ce *tumulus n°2* est situé à environ 100m au Nord Nord-Est du *n°1*, sur la même crête, érigé sur terrain plat.

#### Description

Tumulus ovale à grand axe Nord-Sud, (axe de la crête), mesurant 10m pour ce dernier, et 7,30m pour le plus petit, et une hauteur de 0,90m. Il s'agit d'un tumulus pierreux, fait de blocs de schiste de la taille d'un petit pavé ; il est longé par la piste pastorale à son flanc Ouest.

#### Historique

Monument découvert en [avril 1972]{.date}.

</section>

<section class="monument">

### [Mendive]{.spatial} - [Ilhareko-lepoa]{.coverage} 1 - [Tumulus]{.type}

#### Localisation

Carte 1446 ouest. Ordiarp.

Altitude : 926m.

*Ilhareko-lepoa 1* et *2* sont érigés au sommet d'une petite colline qui domine, au sud-ouest, le col de ce nom, dénomination qui avait d'ailleurs attiré notre attention (le col des morts), mais malgré plusieurs visites en ces lieux, nous n'avions rien remarqué. Ce n'est que par une belle soirée d'automne, avec l'éclairage à jour frisant du soleil couchant, que nous sont apparus les deux tumulus. Nous pensons que, compte tenu de la difficulté à les voir pour un œil non averti, la dénomination du lieu remonte à l'époque de la construction de ces monuments funéraires.

*Ilhareko-lepoa 1* est à l'extrémité sud-est de cette colline.

#### Description

Tumulus aplati en galette de 8m de diamètre et 0,40m de haut ; 4 pierres apparaissent dans le secteur est du monument.

#### Historique

Monument découvert en [octobre 1971]{.date}.

</section>

<section class="monument">

### [Mendive]{.spatial} - [Ilhareko-lepoa]{.coverage} 2 - [Tumulus]{.type}

#### Localisation

Situé à À 15m au nord-ouest de *Ilhareko-lepoa 1*.

#### Description

Petit tumulus de 5m de diamètre, aplati en galette lui aussi, de 0,40m de haut. Quelques pierres apparaissent dans le quart Nord-Ouest, et au centre.

Peut-être y aurait-il encore un troisième tumulus à 5m au sud de ce *tumulus n°2*, de 4m de diamètre et 0,50m de haut, sans que l'on puisse l'affirmer.

#### Historique

Monument découvert en [octobre 1971]{.date}.

</section>

<section class="monument">

### [Mendive]{.spatial} - [Irati-Soro]{.coverage} Nord 3 - [Tumulus]{.type}

#### Localisation

Carte 1446 ouest. Ordiarp.

Altitude : 1027m

Deux cromlechs très voisins ont été décrits [@barandiaranCronicaPrehistoria1952, p. 158], mais ont semble-t-il été détruits depuis par le passage de la route.

Ce tumulus est tout proche de la confluence des ruisseaux Ataramatze et Irati, et à droite de la route qui monte aux chalets d'Irati.

#### Description

Tumulus pierreux de 11m de diamètre et 1,50m de haut environ ; son centre présente une excavation de 5m de diamètre et 1m de profondeur, trace d'une ancienne fouille. Le versant nord-est du tumulus domine le ruisseau, le versant sud-ouest le chemin pastoral.

#### Historique

Monument découvert en [juillet 1971]{.date}.

</section>

<section class="monument">

### [Mendive]{.spatial} - [Irati-Soro]{.coverage} (N) - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 1027m

Il est situé à une quinzaine de mètres à l'Ouest du tumulus *T3*.

#### Description

Tertre de 7m de diamètre et 1m de haut.

#### Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

</section>

<section class="monument">

### [Mendive]{.spatial} - [Irati-Soro]{.coverage} Nord - [Tertres d'habitat]{.type}

#### Localisation

Altitude : 1038m

Ils sont échelonnés le long de la rive droite de l'Iratiko erreka, avant qu'il ne soit traversé par la route qui mène aux chalets d'Irati, au moment où elle amorce sa montée, après avoir laissé une bretelle vers le chalet Pedro.

#### Description

Huit tertres, dont un bon nombre sont de faible hauteur.

-   *Tertre n°1* : correspond aux coordonnées ci-dessus indiquées. Mesure 10m de diamètre, hauteur faible.

-   *Tertre n°2* : situé à 16m au Nord du précédent. Mesure 6m de diamètre.

-   *Tertre n°3* : situé à 20m au Nord Nord-Est du précédent. Mesure 10m x 8m.

-   *Tertre n°4* : situé à 10m au Nord Nord-Est du précédent. Mesure 7m de diamètre.

-   *Tertre n°5* : situé à 60m au Nord-Est du précédent. Mesure 9m de diamètre.

-   *Tertre n°6* : situé à 4m au Nord-Est du précédent. Mesure 10m x 8m de diamètre.

-   *Tertre n°7* : situé à 4m au Nord Nord-Est du précédent. Mesure 8m x 5m.

-   *Tertre n°8* : situé à 4m au Nord Nord-Est du précédent. Mesure 8m de diamètre.

#### Historique

Tertres découverts par [Blot J.]{.creator} en [1975]{.date}.

</section>

<section class="monument">

### [Mendive]{.spatial} - [Iratiko erreka]{.coverage} 1 - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 1056m

Tertre situé à une cinquantaine de mètres au sud de la route qui rejoint le plateau d'Irati aux motels, et à une centaine de mètres environ à l'Est du Tumulus (probablement dolménique) dit *T3*. Il est à 8m à l'Ouest d'un petit ru.

#### Description

Tertre terreux asymétrique, incliné vers le Sud-Est, mesurant 8m de long, 5m de large et 0,80m de haut.

#### Historique

Monument découvert par [J. Blot]{.creator} en [avril 2015]{.date}.

</section>

<section class="monument">

### [Mendive]{.spatial} - [Iratiko erreka]{.coverage} 2 - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 1054m

Il est situé à 90m environ au Sud-Ouest du précédent et à 15m à l'Ouest du petit riu déjà cité.

#### Description

Tertre asymétrique terreux de 6m de long, 4,50m de large et 0,20m de haut environ.

#### Historique

Monument découvert par [J. Blot]{.creator} en [avril 2015]{.date}.

</section>

<section class="monument">

### [Mendive]{.spatial} - [Ithurroché]{.coverage} - [Tertres d'habitat]{.type}

#### Localisation

Ces 4 tertres sont situés au Nord d'une très importante doline et ils dominent de quelques mètres la piste pastorale antique qui s'étend du nord au sud du vallon que longe, à l'est, la route actuelle.

-   Les *tertres 1* et *2*

    Altitude : 1000m. Sont sur un sol en pente vers l'Est alignés dans le sens Est-Ouest, et tangents, le 2 étant plus en altitude que le 1.

-   Les *tertres 3* et *4*

    Ils forment le deuxième groupe, à 15m au Sud du premier ; ils sont disposés de la même manière, le 3 étant plus en hauteur que le 4.

#### Description

Ils mesurent entre 7m et 9m de diamètre et 0,90m de haut.

#### Historique

Tertres découverts par [J. Blot]{.creator} en [1971]{.date}, et contrôlés en [avril 2013]{.date}.

</section>

<section class="monument">

### [Mendive]{.spatial} - [Olhazarre]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 810m

Situé dans un petit col au Nord de la route qui monte de Mendive, et à 7m au sud de l'ancienne piste de crête.

#### Description

Tumulus de terre et de pierres de 9m de diamètre et 0,40m de haut, présentant une dépression centrale de 3m de large et de 0,30m de profondeur environ. Un bloc rocheux, qui ne paraît pas en rapport avec ce monument, occupe une partie Sud-Est de la dépression centrale.

#### Historique

Monument découvert par [J. Blot]{.creator} en [mai 2011]{.date}.

</section>

<section class="monument">

### [Mendive]{.spatial} - [Saint-Sauveur-d'Irati]{.coverage} - [Tumulus]{.type}

#### Localisation

Carte 1346 est. Saint-Jean-Pied-de-Port.

Altitude 900m.

Ce tumulus, actuellement surmonté de la croix d'un calvaire, est situé à quelques mètres au Nord Nord-Est de la chapelle Saint-Sauveur.

#### Description

On note, sous le calvaire une butte plus ou moins informe qui est le reliquat d'un tumulus « préhistorique » signalé par le Cdt Rocq [@rocqEtudePeuplementPays1935, p.371]. D'après lui, lors de la réfection de la chapelle, de nombreux ossements furent trouvés dans ce tumulus ; le curé voulut donner une sépulture chrétienne à ces «païens», et la croix fut installée. On ignore tout de l'époque de cette sépulture, mais nous avons préféré la signaler ici, avec les réserves qui s'imposent.

#### Historique

Monument signalé par le [Cdt Rocq]{.creator} en [1935]{.date}.

</section>

<section class="monument">

### [Mendive]{.spatial} - [Sare-Sare]{.coverage} - [Cromlech]{.type} ([?]{.douteux})

#### Localisation

Altitude : 806m

Il est au beau milieu du replat que forme la croupe de Sare au flanc sud du pic de Béhorlégui.

#### Description

Cercle de pierre peu visible, de 9m de diamètre, délimité par 7 pierres au ras du sol ; elles sont absentes dans les deux quarts Sud-Est et Sud-Ouest. Monument douteux.

#### Historique

Monument découvert par [Meyrat F.]{.creator} en [avril 2013]{.date}.

</section>

<section class="monument">

### [Mendive]{.spatial} - [Sare-Sare]{.coverage} - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 870m.

On peut le voir au bord de la route actuelle, dont il touche le bord Sud.

#### Description

Quoiqu'en partie détérioré par la route, ce tertre de terre et de pierres affecte la forme classique du tertre circulaire dissymétrique, mesurant 12m de diamètre pour un dénivelé de 2m à 3m : il ne semble pas que nous soyons devant le résultat d'un amas dû aux engins de terrassement lors de la construction de la route.

#### Historique

Monument trouvé par [Blot J.]{.creator} en [avril 2013]{.date}.

</section>

<section class="monument">

### [Mendive]{.spatial} - [Xuberaxain]{.coverage} - [Tertres d'habitat]{.type}

#### Localisation

Il y a 8 TH se trouvant dans la prairie qui surplombe le *Dolmen de Xuberaxain Harria*.

-   *Xuberaxain TH1* - Altitude : 612m.

-   *Xuberaxain TH2* - Altitude : 608m.

-   *Xuberaxain TH3* - Altitude : 620m. Plus haut situé que les précédents, relief peu marqué

-   *Xuberaxain TH4* - Altitude : 623m. À quelques mètres au-dessus du n° 3 - relief peu marqué.

-   *Xuberaxain TH5* - Altitude : 600m. Le plus bas situé-Relief mieux marqué.

-   *Xuberaxain TH6* - Altitude : 612m. Relief peu marqué

-   *Xuberaxain TH7* - Altitude : 620m. Relief très visible - à proximité des bâtiments agricoles.

-   *Xuberaxain TH8* - Altitude : 622m. Relief très net - plus en altitude que le précédent - à proximité et au même niveau que les bâtiments agricoles.

#### Historique

-   Xuberaxain TH3, TH4 et TH5 : trouvés par [Blot J.]{.creator} [avril 2013]{.date}.

-   Xuberaxain TH7 et TH8 : Trouvés par le [groupe Hilharriak]{.creator} en [avril 2013]{.date}.

</section>

<section class="monument">

### [Mendive]{.spatial} - [Xuberaxain]{.coverage} - [Tertres d'habitat]{.type}

#### Localisation

Altitude : 630m

Ces deux tertres sont à une centaine de mètres au Nord-Est du *dolmen Xuberaxain*, sur un terrain en pente.

#### Description

-   Le *Tertre n°1*, le plus net mesure une quinzaine de mètres de diamètre. Du fait de la pente il est asymétrique, sa hauteur moyenne étant d'environ 0,70m.

-   Le *Tertre n°2* est à une trentaine de mètres à l'E du précédent. Il est plus modeste (10m de diamètre), et moins marqué en hauteur.

#### Historique

Tertres découverts par [Blot J.]{.creator} en [décembre 2011]{.date}.

</section>

# Montory

<section class="monument">

### [Montory]{.spatial} - [Erretzu]{.coverage} 1 - [Tumulus]{.type} ([?]{.douteux})

#### Localisation

Altitude : 693m.

Il est situé à l'extrémité Est Sud-Est d'un camp protohistorique.

#### Description

Situé en un point dominant de la crête, ce « tumulus » ovale, se présente sous la forme d'un amoncellement de bloc ou de dallettes de [grés]{.subject}, reposant, au Sud-Ouest sur un socle rocheux de grosses dalles naturellement en place. Les dimensions avoisinent 12m x 10m et 2m de haut environ. Depuis quelques années un petit cairn a été érigé sur son sommet.

Il est à noter que l'amoncellement de dallettes de [grés]{.subject} se poursuit vers le Nord, comme suite à un éboulement (?) sur 8m à 10m. Au vu de cet ensemble, on ne peut manquer d'évoquer la possibilité d'un rapport (mais lequel ?), de ce «tumulus» avec un stockage de pierres lié aux extractions de « lauzes » qui se faisaient au voisinage (d'où probablement le nom de « col de la LOSERE »). Nous resterons donc très prudent quant à l'époque et à la finalité de ce « tumulus ».

#### Historique

Tumulus découvert par [J. Blot]{.creator} en [1978]{.date} et publié dans [@blotSouleSesVestiges1979, p.22].

</section>

<section class="monument">

### [Montory]{.spatial} - [Erretzu]{.coverage} 2 - [Tumulus]{.type} ([?]{.douteux})

#### Localisation

Altitude : 692m.

Il est à 32m au Nord de *Erretzu 1 - Tumulus (?)*, à l'extrémité d'un filon pierreux naturel ; cependant son aspect et ses dimensions évoquent un possible artefact.

#### Description

Ce tumulus circulaire en forme de galette, mesure 3,80m de diamètre, 0,40m de haut ; il est constitué de terre et de pierres dont de nombreuses apparaissent en surface, sans qu'on puisse y distinguer un péristalithe évident.

#### Historique

Tumulus découvert par [F. Meyrat]{.creator} en [janvier 2016]{.date}.

</section>

<section class="monument">

### [Montory]{.spatial} - [Ourgaray]{.coverage} Est - [Cromlech]{.type}

#### Localisation

Altitude : 463m.

Il est à environ une cinquantaine de mètres au Nord Nord-Est de la cote 465.

Descriptrion

Sur un sol plat, au centre de la ligne de crête, cinq pierres délimitent un cromlech de 3m de diamètre ; la plus visible, au Nord, mesure 0,76m de long, 0,35m de large et 0,20m de haut.

#### Historique

Monument découvert par [F. Meyrat]{.creator} en [octobre 2015]{.date}.

</section>

<section class="monument">

### [Montory]{.spatial} - [Ourgaray]{.coverage} Ouest - [Tumulus]{.type}

#### Localisation

Altitude : 494m.

Tumulus visible au centre du plateau ; la piste pastorale est tangente à l'Est Sud-Est. Un piquet balise du sentier de randonnée, est planté dans le secteur Est, le plus saillant du tumulus.

#### Description

Tumulus terreux bien visible, de 4,20m de diamètre et 0,20m de haut, présentant une légère dépression centrale. Deux petites pierres sont visibles en surface à proximité immédiate du piquet.

#### Historique

Monument découvert par [F. Meyrat]{.creator} en [octobre 2015]{.date}.

</section>

# Musculdy

<section class="monument">

### [Musculdy]{.spatial} - [Saint-Antoine]{.coverage} - [Tumulus]{.type}

Nous avions cité dans le [@blotNouveauxVestigesProtohistoriques1975, p.122], 8 tumulus mixtes, de terre et de pierres, situés à environ 900m au Nord de la chapelle Saint-Antoine, au milieu d'un vaste plateau, en bordure de la piste pastorale de crête. Leurs dimensions varient de 2m à 5m pour une hauteur moyenne de 0,40m. Nous en décrirons ici plus précisément 4, les plus visibles actuellement ([F. Meyrat]{.creator} - [décembre 2015]{.date}).

#### Localisation

-   *T1* : Altitude : 587m.

-   *T2* : Altitude : 587m.

-   *T3* : Altitude : 587m.

-   *T4* : Altitude : 587m.

#### Description

-   *T1* : tumulus mixte légèrement oblongue, de 5 x 4m et 0,20m de haut ; situé à 6m au Nord Nord-Est de la piste.

-   *T2* : Tumulus de 6 x 4 m et 0,30m de haut. Situé à 5m au Nord Nord-Est de la piste et à 19m au Sud Sud-Ouest de T1.

-   *T3* : Tumulus oblongue de 6 x 4m et 0,30m de haut. Situé à 20m au Nord Nord-Ouest de la piste et à 25m au Nord Nord-Ouest de T2.

-   *T4* : Tumulus oblongue de 7 x 4m et 0,40m de haut. Situé à 70m au Sud Sud-Ouest de T2, et tangent au bord Nord-Ouest de la piste.

#### Historique

Tumuli vus et publiés en [1975]{.date} par [Blot J.]{.creator} et revisités en 2015 par F. Meyrat.

</section>

# Ordiarp

<section class="monument">

### [Ordiarp]{.spatial} - [Etxecortia]{.coverage} - [Pierre couchée]{.type}

#### Localisation

Altitude : 919m.

Cette pierre couchée se trouve à environ 75m au Sud Sud-Est des cayolars d'Etxecortia, dans un très vaste pâturage qui s'étend au Sud de la piste d'accès à ces cayolars.

#### Description

Elle gît dans une légère dépression circulaire, dans laquelle elle a tendance à s'enfoncer et à disparaître peu à peu ; il semble qu'elle soit en cet endroit depuis fort longtemps. Nous avons dû la dégager à la pelle pour en distinguer la périphérie avec précision. Par ailleurs, elle est la seule pierre des environs et on ne note aucun éboulement rocheux en provenance du sommet qui s'élève, à distance, plus au Sud.

En forme de triangle isocèle, cette pierre de [calcaire]{.subject} gris est orientée Nord-Sud, à sommet Sud. Son bord Est mesure 1,28m, son bord Ouest 1,34m, et sa base 0,75m. Son épaisseur varie aux alentours de 0,26m. Son bord Ouest, dans sa totalité, ainsi que sa base et la partie Sud du bord Est paraissent présenter des traces d'épannelage. Nous émettons l'hypothèse qu'il puisse s'agir d'une très ancienne borne pastorale (époque ?).

#### Historique

Pierre découverte par [J. Blot]{.creator} en [février 2017]{.date}.

</section>

<section class="monument">

### [Ordiarp]{.spatial} - [Etxecortia]{.coverage} (groupe 2) - [Tertres d'habitat]{.type}

#### Localisation

À 80m environ au Sud-Est du cayolar *actuel* de ce nom (le *groupe 1* a déjà été décrit par nous).

Altitude : 925m.

#### Description

Un ensemble de 9 tertres de tailles variables s'échelonnent le long de petites ravines ; leurs dimensions vont de 9m à 4m de diamètre et les hauteurs de 1m à quelques centimètres.

#### Historique

Tertres d'habitat découverts par [J. Blot]{.creator} en [juin 2013]{.date}.

</section>

<section class="monument">

### [Ordiarp]{.spatial} - [Etxecortia]{.coverage} (en bas) - [Tertre d'habitat]{.type}

#### Localisation

À plus basse altitude que *Etxecortia - Tertres d'habitat (groupe 2)*, ce TH est au confluent de 2 petites ravines.

Altitude : 864m.

#### Description

Tertre de terre mesurant 9m de diamètre et 0,90m de haut.

#### Historique

Tertre d'habitat découvert par [J. Blot]{.creator} en [juin 2013]{.date}.

</section>

<section class="monument">

### [Ordiarp]{.spatial} - [Ezeloua]{.coverage} 2 - [Tumulus]{.type}

Rappelons un premier tumulus (*T1*) découvert par J.M. de Barandiran.

#### Localisation

À 5om à l'Est Sud-Est des ruines du cayolar.

Altitude : 855m.

#### Description

Tumulus formé de « gravillons », oblongue à grand axe Est-Ouest, mesurant 14m x 10m avec une hauteur de 0,50m.

#### Historique

Tumulus découvert par [J. Blot]{.creator} en [juin 2013]{.date}.

</section>

<section class="monument">

### [Ordiarp]{.spatial} - [Etxecortia]{.coverage} - [Tumulus]{.type} ([?]{.douteux})

#### Localisation

Il est tangent à *Etxecortia - Tertres d'habitat (groupe 2)*, le plus bas situé et près de la route.

Altitude : 925m.

#### Description

Tumulus de 30m de haut et 7m de diamètre, avec une large excavation centrale (fouille ancienne ?).

#### Historique

Tumulus découvert par [J. Blot]{.creator} en [juin 2013]{.date}.

</section>

<section class="monument">

### [Ordiarp]{.spatial} - [Gatagorena (Col de)]{.coverage} - [Tumulus]{.type} ([?]{.douteux})

#### Localisation

Altitude : 817m.

Ce tumulus (?) se trouve au point culminant du petit plateau qui domine au Nord le col de Gatagorena ; il est à 150m environ à l'Est de la « piste des sabotiers », bien visible, et à la même distance, à l'Ouest de la piste carrossable qui mène à Etxecortia.

#### Description

« Tumulus » de 6m de diamètre et 0,30m à 0,40m de haut. Pas de pierres visibles ; nous pensons qu'il pourrait aussi s'agir d'un mouvement naturel du terrain atteignant son point culminant à cet endroit.

#### Historique

Monument découvert par [Meyrat F.]{.creator} en [février 2017]{.date}.

</section>

<section class="monument">

### [Ordiarp]{.spatial} - [Saint Grégoire]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 421m.

Ce tumulus est à 30m au Nord-Ouest de la clôture de la chapelle St Grégoire et à 9m à l'Ouest de la fin de la route qui mène à cette même chapelle.

#### Description

Tumulus de terre de forme ovale à grand axe Nord-Sud mesurant 7,60m x 5,50m et 0,40m de haut.

#### Historique

Tumulus découvert par [Blot J.]{.creator} en [avril 2015]{.date}.

</section>

# Osses

<section class="monument">

### [Osses]{.spatial} - [Elguet]{.coverage} 1 - [Tumulus]{.type}

#### Localisation

Carte 1345 Ouest Cambo-les-Bains.

Altitude : 664m.

Il est situé au versant Sud-Ouest du mont Baygoura, à environ 1000m à vol d'oiseau au Sud du sommet du pic Haltzamendi, et à 250m à l'Est de la ligne de crête.

#### Description

Modeste tumulus herbeux de 5m de diamètre et 0,50m de haut ; la piste pastorale passe en son milieu.

#### Historique

Monument découvert en [janvier 1974]{.date}.

</section>

<section class="monument">

### [Osses]{.spatial} - [Elguet]{.coverage} 2 - [Tumulus]{.type}

#### Localisation

Il est pratiquement tangent au Sud-Est de *Elguet 1 - Tumulus*.

#### Description

Tumulus de terre de 5m de diamètre et 0,40m de haut.

#### Historique

Monument découvert en [janvier 1974]{.date}.

</section>

<section class="monument">

### [Osses]{.spatial} - [Horza]{.coverage} 3 - [Tumulus]{.type}

#### Localisation

Coordonnées.

Carte 1345 Ouest Cambo-les-Bains.

Altitude : 550m.

Sur une vaste croupe, il existe au versant méridional du Baygoura, [@barandiaranProspecionesExcavacionesPrehistoricas1962, p.16], un ensemble de cromlechs, que nous avons ensuite complété [@blotNouveauxVestigesMegalithiques1972c, p. 28].

Le tumulus n°3 est tangent, à l'Est, au *Tumulus Horza 2*.

#### Description

Petit tumulus mixte de 2,50m de diamètre, et 0,30m de haut, constitué de terre et de plaquettes schisteuses délitées comme le n°2.

#### Historique

Monument découvert en [septembre 1980]{.date}.

</section>

<section class="monument">

### [Osses]{.spatial} - [Horza]{.coverage} 5 - [Cromlech]{.type}

#### Localisation

Il est tangent au Sud du précédent.

#### Description

Cromlech légèrement tumulaire, de 4m de diamètre, délimité par 7 pierres au ras du sol ; 4 autres sont visibles au centre.

#### Historique

Monument découvert en [septembre 1980]{.date}.

</section>

<section class="monument">

### [Osses]{.spatial} - [Ibidia]{.coverage} - [Tumulus]{.type} ([?]{.douteux})

#### Localisation

Carte 1345 Ouest Cambo-les-Bains.

Altitude : 527m.

Du mont Baigoura se détache, au niveau du pic Laina, une piste pastorale, suivant une ligne de crête perpendiculaire à l'axe principal, et qui se dirige vers le Sud-Est. Le tumulus est au milieu d'un petit col, à la cote 527m, à 10m à l'Ouest d'une bergerie démolie.

#### Description

Tumulus pierreux de 5m de diamètre et 0,50m de haut ; monument douteux ?

#### Historique

Monument découvert en [mars 1973]{.date}.

</section>

<section class="monument">

### [Osses]{.spatial} - [Ibidia]{.coverage} 2 - [Tumulus]{.type}

#### Localisation

Altitude 545m.

Situé en bordure de la piste qui descend le long de la croupe de ce nom, sur le flanc Est du mont Baygoura, sur un terrain en légère pente vers l'Est. Situé à l'Ouest du Tumulus Ibidia 1 qui est dans le col sous-jacent.

#### Description

Tumulus pierreux discret de 4m à 5m de diamètre et quelques centimètres de haut.

#### Historique

Monument découvert par [Meyrat F.]{.creator} en [février 2013]{.date}.

</section>

<section class="monument">

### [Osses]{.spatial} - [Iriapixta]{.coverage} - [Tertre d'habitat]{.type} ou [Tumulus]{.type} ([?]{.douteux})

#### Localisation

Altitude : 324m.

Il est situé au flanc Nord-Ouest de la colline dénommée Iriapixta.

#### Description

Très imposant tertre terreux ovale, mesurant 24m de long, 18m de large et 2,50m de haut environ, érigé sur un terrain en très légère pente vers le Nord. Un point d'eau se trouve à environ 250m au Nord-Ouest, près de l'ancienne borde. Il est difficile de préciser l'époque de construction et la finalité d'un tertre de cette importance ; nous n'en connaissons que 3 autres ayant aussi des dimensions «hors normes» mais moindre : les 2 tertres d'Hurlastère (Lecumberri) [@blotInventaireMonumentsProtohistoriques2014], et celui d'Orkhatzolhatz (Sainte-Engrace) [@blotInventaireMonumentsProtohistoriques2015, p.15] ; ils nous posent eux-aussi les mêmes problèmes diagnostiques...

#### Historique

Monument découvert par [F. Meyrat]{.creator} en [août 2015]{.date}.

</section>

<section class="monument">

### [Osses]{.spatial} - [Laina]{.coverage} - [Dalle couchée]{.type}

#### Localisation

Altitude : 821m.

Se trouve juste en bas de la pente Sud du Mont Baygoura, en bordure de la ligne de crête et de la rupture de pente côté Est.

#### Description

Dalle couchée de calcaire local, de forme rectangulaire, à grand axe Est-Ouest, mesurant 1,68m de long, 1,15m de large et de 0,24m d'épaisseur en moyenne.

Elle présente de très nettes traces d'épannelage sur tous les côtés (surtout au Sud), sauf au Nord.

#### Historique

Dalle trouvée par [Blot J.]{.creator} en [février 2013]{.date}.

</section>

<section class="monument">

### [Osses]{.spatial} - [Laina]{.coverage} 1 - [Tumulus-cromlech]{.type}

#### Localisation

Carte 1345 Ouest Cambo-les-Bains.

Altitude : 850m.

Il est visible juste après un important amas rocheux, sur un replat quand on descend du sommet du Baïgoura vers le Nord. Il est sur la ligne de crête, tout au bord du versant Est, abrupt.

#### Description

Tumulus ovale (diamètres : 6,50m x 4,50m), et de 0,90m de haut, marqué par 13 blocs périphériques de [quartzite]{.subject} local, avec, au centre, un petit amas pierreux.

#### Historique

Monument découvert en [février 1974]{.date}.

</section>

<section class="monument">

### [Osses]{.spatial} - [Laina]{.coverage} 2 - [Tumulus]{.type}

#### Localisation

Il est situé à 100m au Sud de *Laina 1 - Tumulus-cromlech*, et un peu plus en altitude, à 869m, à l'extrémité Est du sommet plat de Laina.

#### Description

Tumulus mixte où prédominent les petits blocs et les plaquettes de [schiste]{.subject}, mobiles, souvent délités ; le sommet est aplati avec légère dépression centrale. Il mesure 5m de diamètre et 0,80m de haut.

#### Historique

Monument découvert en [février 1974]{.date}.

</section>

<section class="monument">

### [Osses]{.spatial} - [Laina]{.coverage} 3 - [Cromlech]{.type}

#### Localisation

Carte 1345 Ouest Cambo-les-Bains.

On le trouve à 400m au Sud Sud-Ouest de *Laina 2 - Tumulus*.

Altitude : 780m.

Il est à l'amorce d'un petit replat, au flanc Sud Sud-Ouest du pic Laina.

#### Description

Cercle de 5m de diamètre, délimité par 11 pierres ; de nombreuses petites autres sont visibles à l'intérieur qui est légèrement surélevé.

#### Historique

Monument découvert en [février 1974]{.date}.

</section>

<section class="monument">

### [Osses]{.spatial} - [Laina]{.coverage} 4 - [Tumulus]{.type}

#### Localisation

Altitude : 869m.

Il est au bord de la rupture de pente (à l'Est), à quelques dizaines de mètres au Sud de *Laina 1 - Tumulus-cromlech*

#### Description

Tumulus de terre de 9m de diamètre et 1m de haut environ.

#### Historique

Monument découvert par [Blot J.]{.creator} en [février 2013]{.date}.

</section>

<section class="monument">

### [Osses]{.spatial} - [Laina]{.coverage} 5 - [Cromlech]{.type}

#### Localisation

Altitude : 780m.

Il se trouve à 4,60m au Sud de *Laina 3 - Cromlech*.

#### Description

Cercle délimité par une douzaine de pierres au ras du sol, plus nombreuses dans la moitié Sud (1 seule au Nord). Il semble qu'on puisse distinguer une ébauche de cercle intérieur formé de 5 pierres, plus 1 pierre centrale.

#### Historique

Monument découvert par [Meyrat F.]{.creator} en [février 2013]{.date}.

</section>

<section class="monument">

### [Osses]{.spatial} - [Orgueletegui]{.coverage} 3 - [Cromlech]{.type}

#### Localisation

Altitude : 740m.

Il est situé très exactement entre les deux cromlechs cités par J.M. de Barandiaran [@barandiaranContribucionEstudioCromlechs1949, p.199], soit donc à 1,20m à l'Est du cromlech n°1 et à 4m à l'Ouest du cromlech n°2.

#### Description

Quatre pierres (dont une au Sud-Ouest mesure 0,70m x 0,30m) délimitent un petit cercle de 2,10m de diamètre ; on en note une cinquième, à l'intérieur, dans le quart Sud-Est.

#### Historique

Ce monument trouvé par [Blot J.]{.creator} en [1974]{.date}.

</section>

<section class="monument">

### [Osses]{.spatial} - [Orgueletegui]{.coverage} - Pierre couchée

#### Localisation

Elle est située à 3,70m au Sud-Est de *Orgueletegui 3 - Cromlech*, sur un terrain en pente vers le Sud.

#### Description

Dalle de [grés]{.subject} parallélépipédique de 1,80m de long, 0,70m de large, et 0,40m d'épaisseur en moyenne, allongée selon un axe Nord-Est Sud-Ouest ; elle présente, semble-t-il des traces d'épannelage sur le bord de son extrémité Nord-Est.

#### Historique

Pierre couchée trouvée par [Meyrat F.]{.creator} en [février 2013]{.date}.

</section>

## [Osses]{.spatial} - [Horza]{.coverage} : les [cromlechs]{.type} d'Horza

La première publication de 5 cromlechs à Horza est faite par J.M. de Barandiaran [@barandiaranProspecionesExcavacionesPrehistoricas1962] ; nous avons ensuite publié [@blotNouveauxVestigesMegalithiques1972a, p.18] un cromlech supplémentaire, puis en 2008 [@blotInventaireMonumentsProtohistoriques2009, p.46] un tumulus et un autre cromlech. Lors d'une nouvelle visite sur les lieux en mai 2012, puis à nouveau en mai 2013, nous avons été dans l'impossibilité de retrouver les monuments par nous décrits ou par J.M. de Barandiaran, sauf le *n°1* de ce dernier. La végétation très drue, les intempéries... sont peut-être responsables de cet état de chose. Nous avons donc été amenés à partir du n°1, à rechercher ce qui pouvait correspondre aux publications antérieures : mais quand la distance entre 2 monuments correspondait, c'est le diamètre ou l'orientation qui différaient. Nous avons donc été contraints, pour éviter les confusions, de donner des lettres au lieu de numéros aux monuments que nous réussissions à identifier comme tels, puisque seul le *n°1* de J.M. Barandiran est bien retrouvé : ce sera le *cromlech A*.

Par contre un nouveau cromlech a été trouvé un peu plus au Sud : *Cromlech Horza Sud F*.

<section class="monument">

### [Osses]{.spatial} - [Horza]{.coverage} - [Cromlech]{.type} A

#### Localisation

Sur le replat d'Horza, à l'extrémité Sud du mont Baigura.

Altitude : 520m.

Il est de suite visible à droite de la piste qui parcours ce replat du Nord au Sud.

#### Description

D'un diamètre de 5,20m et en très léger relief, il est marqué par un grand nombre de petites pierres périphériques, ainsi qu'en l'intérieur.

#### Historique

Monument décrit en [1962]{.date} par [J.M. de Barandiaran]{.creator}.

</section>

<section class="monument">

### [Osses]{.spatial} - [Horza]{.coverage} - [Cromlech]{.type} B

#### Localisation

Il est à 5m au Sud-Ouest de *Horza - Cromlech A* (et non au Sud-Est).

#### Description

Mesure 3,20m de diamètre (et non 5m) et possède une douzaine de pierres en périphérie (et non 5).

#### Historique

Monument découvert par [Meyrat F.]{.creator} et [Blot J.]{.creator} en [mai 2013]{.date}.

</section>

<section class="monument">

### [Osses]{.spatial} - [Horza]{.coverage} - [Cromlech]{.type} C

#### Localisation

Situé à 22m au Sud de *Horza - Cromlech B*.

#### Description

Il présente un aspect nettement tumulaire (0,30m de haut) et mesure 2,70m de diamètre possédant une dizaine de pierres périphériques.

#### Historique

Monument découvert par [Meyrat F.]{.creator} et [Blot J.]{.creator} en [mai 2013]{.date}.

</section>

<section class="monument">

### [Osses]{.spatial} - [Horza]{.coverage} - [Cromlech]{.type} D

#### Localisation

À 12m au Sud de *Horza - Cromlech C*.

#### Description

Mesure 3m de diamètre avec 7 pierres périphériques.

#### Historique

Monument découvert par [Meyrat F.]{.creator} et [Blot J.]{.creator} en [mai 2013]{.date}.

</section>

<section class="monument">

### [Osses]{.spatial} - [Horza]{.coverage} - [Cromlech]{.type} E

#### Localisation

Il est à 2m au Sud de *Horza - Cromlech D*.

#### Description

Cercle de 6m de diamètre délimité par une quinzaine de pierres.

#### Historique

Monument découvert par [Meyrat F.]{.creator} et [Blot J.]{.creator} en [mai 2013]{.date}.

</section>

<section class="monument">

### [Osses]{.spatial} - [Horza]{.coverage} Sud - [Cromlech]{.type} F

#### Localisation

Altitude : 519m.

Il est à 3m à l'Ouest de la piste pastorale.

#### Description

Petit cercle de 3,20m de diamètre délimité par 8 pierres au ras du sol environ.

#### Historique

Monument découvert par découvert par [Blot J.]{.creator} en [mai 2013]{.date}.

</section>

# Ostabat

<section class="monument">

### [Ostabat]{.spatial} - [Lindungneko lepoa]{.coverage} - [Tumulus]{.type}

#### Localisation

Carte 1345 Est Iholdy.

Altitude : 350m.

Nous avons publié [@blotNouveauxVestigesProtohistoriques1975, p. 109], toute une série de monuments couronnant les croupes du Lantabat. Nous y ajoutons ce dernier, situé entre les tertres d'Ipharlaze et ceux de Lindugnekolepoa.

Il est situé à 100m à l'Est du col de ce nom qu'il domine, sur un léger replat tangent à la bordure relevée d'un petit fossé délimitant lui-même un pâturage enclos de barbelés.

#### Description

Tumulus terreux de 14m de diamètre et 0,50m de haut.

#### Historique

Monument découvert en [août 1971]{.date}.

</section>

<section class="monument">

### [Ostabat]{.spatial} - [Ipharlatze]{.coverage} 1 - [Tumulus]{.type}

#### Localisation

Altitude : 328m.

Situé au niveau du col à environ une quarantaine de mètres au Sud-Ouest de la route.

#### Description

Tumulus circulaire de terre et de terre de 14m à 15m de diamètre, et 0,80m de haut.

#### Historique

Monument découvert par [Blot J.]{.creator} en [novembre 1975]{.date}.

</section>

<section class="monument">

### [Ostabat]{.spatial} - [Ipharlatze]{.coverage} 2 - [Tumulus]{.type}

#### Localisation

Situé à une vingtaine de mètres au Sud de *Ipharlatze 1 - Tumulus*.

#### Description

Tumulus circulaire de 12m de diamètre et 0,80m de haut, de terre et de pierres.

#### Historique

Monument découvert par [Blot J.]{.creator} en [novembre 1975]{.date}.

</section>

## [Ostabat]{.spatial} - [Gagneko Ordokia]{.coverage} : les [tertres pierreux]{.type}

Nous décrirons ici un ensemble de tertres pierreux dont certains ont été découverts par [J. Blot]{.creator} en [1975]{.date}, d'autres par le [groupe Hilarriak]{.creator} en [mars 2013]{.date}, les autres enfin par J. Blot en [avril 2013]{.date}.

Commune d'Ostabat et de [Lantabat]{.spatial}. Nous les citons tous ici pour faciliter la localisation de ces tertres les uns par rapport aux autres - Toutefois **les tertres n°14, 15, 16 et 19 sont dans la commune de Lantabat**.

<section class="monument">

### [Ostabat]{.spatial} - [Gagneko Ordokia]{.coverage} 1 - [Tertre pierreux]{.type}

#### Localisation

Altitude : 401m.

Il est situé à environ 75m au Nord Nord-Est du *grand Tumulus*.

#### Description

Tertre pierreux de 4m de diamètre et quelques centimètres de haut.

#### Historique

Monument découvert par [J. Blot]{.creator}.

</section>

<section class="monument">

### [Ostabat]{.spatial} - [Gagneko Ordokia]{.coverage} 2 - [Tertre pierreux]{.type}

#### Localisation

Situé à 1m à l'Ouest Nord-Ouest de *Gagneko Ordokia 1 - Tertre pierreux*.

#### Description

Tertre pierreux mesurant 6m de diamètre ; faible hauteur.

#### Historique

Monument découvert par [J. Blot]{.creator}.

</section>

<section class="monument">

### [Ostabat]{.spatial} - [Gagneko Ordokia]{.coverage} 3 - [Tertre pierreux]{.type}

#### Localisation

Situé à 15m à l'Ouest de *Gagneko Ordokia 2 - Tertre pierreux*.

#### Description

Tertre pierreux de 3m de diamètre - Faible hauteur.

#### Historique

Monument découvert par [J. Blot]{.creator}.

</section>

<section class="monument">

### [Ostabat]{.spatial} - [Gagneko Ordokia]{.coverage} 4 - [Tertre pierreux]{.type}

#### Localisation

Situé à 6m au Nord Nord-Ouest de *Gagneko Ordokia 3 - Tertre pierreux*.

#### Description

Tertre pierreux de 3m de diamètre et de faible hauteur.

#### Historique

Monument découvert par [J. Blot]{.creator}.

</section>

<section class="monument">

### [Ostabat]{.spatial} - [Gagneko Ordokia]{.coverage} 5 - [Tertre pierreux]{.type}

#### Localisation

Tangent à l'Ouest de *Gagneko Ordokia 4 - Tertre pierreux*.

Altitude : 401m.

#### Description

Tertre pierreux de 5m de faible hauteur.

#### Historique

Monument découvert par [J. Blot]{.creator}.

</section>

<section class="monument">

### [Ostabat]{.spatial} - [Gagneko Ordokia]{.coverage} 6 - [Tertre pierreux]{.type}

#### Localisation

Altitude : 401m.

Il est situé à 33m à l'Est de *Gagneko Ordokia 1 - Tertre pierreux*.

#### Description

Tertre de pierres bien visibles, de 6,50m de diamètre et 0,30m de hauteur.

#### Historique

Monument découvert par le [groupe Hilarriak]{.creator}.

</section>

<section class="monument">

### [Ostabat]{.spatial} - [Gagneko Ordokia]{.coverage} 7 - [Tertre pierreux]{.type}

#### Localisation

Situé à 4m à l'Est de *Gagneko Ordokia 6 - Tertre pierreux*.

#### Description

Mesure 4m de diamètre et de faible hauteur.

#### Historique

Monument découvert par [J. Blot]{.creator}.

</section>

<section class="monument">

### [Ostabat]{.spatial} - [Gagneko Ordokia]{.coverage} 8 - [Tertre pierreux]{.type}

#### Localisation

Situé à 100m environ au Nord de *Gagneko Ordokia 1 - Tertre pierreux*.

Altitude : 401m.

#### Historique

Monument découvert par le [groupe Hilarriak]{.creator}.

</section>

<section class="monument">

### [Ostabat]{.spatial} - [Gagneko Ordokia]{.coverage} 9 - [Tertre pierreux]{.type}

#### Localisation

Situé à 20m au Nord de *Gagneko Ordokia 8 - Tertre pierreux*.

#### Description

Mesure 5m de diamètre ; de faible hauteur.

#### Historique

Monument découvert par le [groupe Hilarriak]{.creator}.

</section>

<section class="monument">

### [Ostabat]{.spatial} - [Gagneko Ordokia]{.coverage} 10 - [Tertre pierreux]{.type}

#### Localisation

Situé à 2m au Sud de *Gagneko Ordokia 8 - Tertre pierreux*.

#### Description

Mesure 3m de diamètre et de faible hauteur.

#### Historique

Monument découvert par [J. Blot]{.creator}.

</section>

<section class="monument">

### [Ostabat]{.spatial} - [Gagneko Ordokia]{.coverage} 11 - [Tertre pierreux]{.type}

#### Localisation

Situé à 2m au Sud de *Gagneko Ordokia 10 - Tertre pierreux*.

#### Description

Mesure 3m de diamètre ; de faible hauteur.

#### Historique

Monument découvert par [J. Blot]{.creator}.

</section>

<section class="monument">

### [Ostabat]{.spatial} - [Gagneko Ordokia]{.coverage} 12 - [Tertre pierreux]{.type}

#### Localisation

Altitude : 401m.

Il est situé à plusieurs dizaines de mètres au Nord de *Gagneko Ordokia 9 - Tertre pierreux*.

#### Description

Mesure 5,50m de diamètre et 0,40m de haut.

#### Historique

Monument découvert par [J. Blot]{.creator}.

</section>

<section class="monument">

### [Ostabat]{.spatial} - [Gagneko Ordokia]{.coverage} 13 - [Tertre pierreux]{.type}

#### Localisation

Situé à 15m à l'Ouest de *Gagneko Ordokia 12 - Tertre pierreux*.

#### Description

Mesure 4,90m de diamètre et 0,40m de haut.

#### Historique

Monument découvert par [J. Blot]{.creator}.

</section>

<section class="monument">

### [Ostabat]{.spatial} - [Gagneko Ordokia]{.coverage} 14 - [Tertre pierreux]{.type}

#### Localisation

Altitude : 397m.

Il est situé à 50m au Nord de *Gagneko Ordokia 13 - Tertre pierreux*.

#### Description

Tertre pierreux très visible, étalé sur terrain en pente vers le Nord-Ouest ; mesure 6m de diamètre.

#### Historique

Monument découvert par [J. Blot]{.creator}.

</section>

<section class="monument">

### [Ostabat]{.spatial} - [Gagneko Ordokia]{.coverage} 15 - [Tertre pierreux]{.type}

#### Localisation

Situé à 11m à l'Ouest de *Gagneko Ordokia 14 - Tertre pierreux*.

#### Description

Mesure 4,90m de diamètre et 0,40m de haut.

#### Historique

Monument découvert par [J. Blot]{.creator}.

</section>

<section class="monument">

### [Ostabat]{.spatial} - [Gagneko Ordokia]{.coverage} 16 - [Tertre pierreux]{.type}

#### Localisation

Altitude : 396m.

Situé à 13m au Nord Nord-Ouest du *n°15*.

#### Description

Mesure 3,50m de diamètre et 0,40m de haut.

#### Historique

Monument découvert par [J. Blot]{.creator}.

</section>

<section class="monument">

### [Ostabat]{.spatial} - [Gagneko Ordokia]{.coverage} 17 - [Tertre pierreux]{.type}

#### Localisation

Situé à 3m au Sud-Est de la piste.

#### Description

Tertre pierreux de 2,40m de diamètre et de faible hauteur.

#### Historique

Monument découvert par [J. Blot]{.creator}.

</section>

<section class="monument">

### [Ostabat]{.spatial} - [Gagneko Ordokia]{.coverage} 18 - [Tertre pierreux]{.type}

#### Localisation

Situé à 1m à l'Ouest de *Gagneko Ordokia 17 - Tertre pierreux*.

#### Description

Mesure 2m de diamètre, très faible hauteur.

#### Historique

Monument découvert par [J. Blot]{.creator}.

</section>

<section class="monument">

### [Ostabat]{.spatial} - [Gagneko Ordokia]{.coverage} 19 - [Tertre pierreux]{.type}

#### Localisation

Altitude : 400m.

Il est tangent à la piste, au Nord.

#### Description

Mesure 4,30m de diamètre et 0,30m de haut.

#### Historique

Monument découvert par [J. Blot]{.creator} en [2013]{.date}.

</section>

# Pagolle

<section class="monument">

### [Pagolle]{.spatial} - [Mehalzu]{.coverage} 1 - [Tumulus]{.type}

#### Localisation

Altitude : 499m.

Ce tumulus se trouve sur un terrain en très légère pente vers le Nord-Ouest, à environ 5m à l'Est de la clôture en fil barbelé matérialisant la limite entre Pagolle et Juxue.

#### Description

Tumulus de 5m de diamètre et 0,30m de haut environ ; une vingtaine de petites dalles de calcaire blanc sont visibles en surface, dont six pourraient matérialiser une [ciste]{.subject} centrale.

#### Historique

Tumulus découvert par [Blot J.]{.creator} en [octobre 2015]{.date}.

</section>

<section class="monument">

### [Pagolle]{.spatial} - [Mehalzu]{.coverage} 2 - [Tumulus]{.type}

#### Localisation

Altitude : 612m.

Tertre érigé sur un beau replat après une rude montée et avant d'aborder le sommet de Mèhalzu ; il est à 5m à l'Est de la clôture marquant la limite entre Pagolle et Juxue.

#### Description

Tertre mixte, terreux et pierreux, à la surface duquel apparaissent de nombreux blocs de [calcaire]{.subject}, de volumes variables, dont une dalle, située au Sud, dont la périphérie visible est épannelée ; elle mesure 0,89m de long et 0,25m de haut.

#### Historique

Tumulus découvert par [Blot J.]{.creator} en [octobre 2015]{.date}.

</section>

<section class="monument">

### [Pagolle]{.spatial} - [Oxaraniako borda]{.coverage} Est (ou [Mehalzu]{.coverage} ) - [Tumulus]{.type}

Ces 9 tumulus sont situés sur un terrain dans l'ensemble en légère pente vers le Nord.

#### Historique

Ces 9 tumulus ont été découverts en [avril 1974]{.date} par [Blot J.]{.creator} et cités en 1975, dans le [@blotNouveauxVestigesProtohistoriques1975, p.121], dans la rubrique *Tertres de Méhalzu*). Signalons, à cette occasion que l'ensemble des tumulus du site *Daraturagagne* [@blotNouveauxVestigesProtohistoriques1975, p.122], a été rasé par la création de nouvelles prairies.

-   *Tumulus n°1* - Altitude : 472m. Proche d'un poste de tir à la palombe. Tumulus mixte de terre et de pierres, circulaire, mesurant 9m de diamètre et 0,60m de haut

-   *Tumulus n°2* - Altitude : 469m. Il est situé à 10m au Nord Nord-Est du n° 1. Tumulus mixte mesurant 11m x 8m et 0,40m de haut, à grand axe Nord-Sud.

-   *Tumulus n°3* - Altitude : 476m. Tumulus mixte situé à 12m au Sud du n°1. Il mesure 9m x 7m et 0,40m de haut.

-   *Tumulus n°4* - Altitude : 478m. Tumulus mixte situé à 5m au Sud Sud-Est du n°3. Il mesure 7m x 6m et 0,40m de haut.

-   *Tumulus n°5* - Altitude : 471m. Tumulus mixte circulaire, situé à 12m au Nord-Ouest du n°2, mesurant 8m de diamètre et 0,30m de haut.

-   *Tumulus n°6* - Altitude : 471m. Tumulus mixte, situé à 3m au Nord-Ouest du n°5 ; il mesure 9m x 7m et 0,40m de haut, à grand axe Nord-Sud.

-   *Tumulus n°7* - Altitude : 467m. Tumulus situé à 16m au Nord-Ouest du n° 6. Il mesure 12m x 6m et 0,30m de haut., à grand axe Nord-Sud.

-   *Tumulus n°8* - Altitude : 462m. Tumulus mixte situé à 20m au Nord-Est du n°7. Il mesure 8m x 7m et 0,40m de haut.

-   *Tumulus n°9* - Altitude : 467m. Tumulus mixte situé à 70m à l'Est Sud-Est du n°8 : il mesure 9m x 8m et 0,60m de haut.

</section>

# Sainte-Engrâce

<section class="monument">

### [Sainte-Engrâce]{.spatial} - [Anhaou]{.coverage} (groupe Nord-Est) - [Tertres d'habitat]{.type}

#### Localisation

Altitude : 1266m.

Cinq tertres situés à 15m au Nord-Est du virage de la route qui passe elle aussi au Nord-Est du cayolar de ce nom.

#### Description

De ces 5 tertres, un seul est très visible, mesurant 6m x 6m et 0,50m de haut, les autres ont des dimensions plus réduites par l'érosion, mais sont cependant bien identifiables.

Rappelons ici les 7 tertres très nets déjà décrits [@blotSouleSesVestiges1979, p.33]

#### Historique

Tertres découverts par [Blot J.]{.creator} en [octobre 2013]{.date}.

</section>

<section class="monument">

### [Sainte-Engrâce]{.spatial} - [Erainze]{.coverage} - [Tertres d'habitat]{.type}

#### Localisation

Altitude : 1431m.

Ces 6 tertres sont répartis en deux groupes, accolés aux parois rocheuses qui rétrécissent le passage, au fond de ce vaste cirque où sont construits les cayolars d'Erainze. On y accède par une piste qui part du col d'Erainze, au Sud.

On distingue un **Groupe Est**, constitué de 2 tertres et un **Groupe Ouest**, de 4 autres tertres, ces deux groupes étant répartis de part et d'autre du rétrécissement déjà signalé, par où passe la piste qui mène, bien plus bas, vers Ste Engrace, par les gorges d'Ehujarro.

#### Description

-   **Groupe Est**

    -   *Tertre 1* : Celui-ci est (comme les autres) adossé à la paroi rocheuse, au Nord. Il mesure 7m de diamètre et 1,70m de haut. À son sommet, aplati, large de 2,50m, on peut distinguer une couronne de quinze pierres environ, qui ont pu servir de contention à l'habitat provisoire construit le temps de l'estive. De nombreuses dolines à proximité assurent, avec le ruisseau, l'approvisionnement en eau.

    -   *Tertre 2* : Quasi tangent, à l'Est, au précédent. Lui aussi adossé à la paroi rocheuse. Il mesure 8m de diamètre, et 1,70m de haut. Comme pour le précédent, on peut compter 8 pierres à son sommet.

-   **Groupe Ouest**

    -   *Tertre 3* : Altitude : 1431m. Lui aussi adossé à la paroi rocheuse, ce tertre mesure 10m de diamètre et 0,50m de haut ; sommet là encore couronné de quelques pierres.

    -   *Tertre 4* : Situé à 1m à l'Est du précédent. Adossé à la paroi rocheuse, il mesure 6m de diamètre et 0,50m de haut.

    -   *Tertre 5* : situé à 5m à l'Est du précédent, adossé à la paroi rocheuse, il mesure 9m de diamètre et 0,80m de haut ; de nombreuses pierres sont visibles à son sommet.

    -   *Tertre 6* : Situé tangent au précédent, à l'Est, il est de taille beaucoup plus modeste, mesurant 4m de diamètre et 0,40m de haut.

#### Historique

Le *Tertre n°1* a été découvert par [A. Martinez]{.creator} en [septembre 2017]{.date}, le *n°2* par [Blot J.]{.creator} en fin [septembre 2017]{.date} et les 4 autres par [Meyrat F.]{.creator} à la même date.

</section>

<section class="monument">

### [Sainte-Engrâce]{.spatial} - [Erainze]{.coverage} - ([?]{.douteux})

#### Localisation

Altitude : 1440m.

Situé à quelques dizaines de mètres à l'Est de *Erainze - Tertres d'habitat* (TH 1 et 2), on note ce vestige d'un habitat (non datable), constitué d'une vingtaine de blocs de calcaire blanc, émergeant du sol, et disposés suivant un plan rectangulaire ; il mesure 2,60m de long et 1,40m de large, et son ouverture est orienté vers le Sud-Ouest. Vestige identifié par [Blot Colette]{.creator} en [septembre 2017]{.date}.

</section>

<section class="monument">

### [Sainte-Engrâce]{.spatial} - [Heguitchudia]{.coverage} - [Tertres d'habitat]{.type}

#### Localisation

Altitude : 1139m.

*TH2* : Altitude : 1109m.

Cet ensemble de 7 tertres est situé sur la ligne de croupes qui borde la rive gauche des gorges d'Holzarté.

#### Description

-   *TH1* : Ce tertre - qui fait partie d'un groupe de 7 au total - est isolé, en hauteur dans le pâturage, à une cinquantaine de mètres de la piste qui passe en contre-bas au Nord-Ouest, et à 17m au Sud-Ouest des ruines du cayolar d'Héguitchudia. Tertre terreux asymétrique, érigé sur terrain en pente vers le Nord. Il mesure 8m de diamètre et 0,90m de haut environ.

-   *TH2* : Il est situé, avec les autres, à une cinquantaine de mètres à l'Est et en contre-bas du précédent, sur un terrain très en pente vers le Nord-Est, à proximité de la lisière des bois et d'un petit sentier. Il mesure 12m x 8m et 0,80m de haut.

-   *TH3* : Altitude : 1105m. Il est situé à 45m au Sud-Est du précédent, et mesure 12m x 8m et 0,50m de haut.

-   *TH4* : Altitude : 1104m. Situé à 16m à l'Est de *TH3*, il est moins visible ; il mesure12m x 6m et 0,40m de haut.

-   *TH5* et *TH6* : Altitude : 1107m. Ces deux tertres sont tangents, peu visibles, et à quelques mètres à l'Ouest Nord-Ouest de *TH4*. À eux deux ils mesurent au total 10m x 8m et 0,40m de haut, n'étant individualisés que par un discret rétrécissement.

-   *TH7* : Altitude : 1101m. Situé à 10m au Nord de *TH5* et *TH6*, il mesure 10m x 6m et 0,60m de haut.

#### Historique

Les *TH1*, *TH2*, *TH3*, *TH4*, ont été découverts en [septembre 2017]{.date} par [A. Martinez]{.creator}, les *TH5*, *TH6* et *TH7* par [Meyrat F.]{.creator} à la fin de ce même mois.

</section>

<section class="monument">

### [Sainte-Engrâce]{.spatial} - [Huchtia]{.coverage} - [Tertre d'habitat]{.type} ou [Tumulus]{.type}

#### Localisation

Altitude : 823m.

Il est à une quinzaine de mètres au Nord-Est d'un chemin pastoral.

#### Description

Tumulus terreux circulaire de 10m de diamètre, de 0,40m de haut, érigé sur un sol très légèrement en pente vers le Nord-Est.

La question peut se poser de savoir s'il s'agit d'un tertre d'habitat très érodé, ou d'un tumulus... La différence entre le diamètre et la hauteur et sa parfaite circularité nous inciterait à penser peut-être à un tumulus...

#### Historique

Monument découvert par [Blot J.]{.creator} en [novembre 2015]{.date}, ainsi que tous les autres monuments d'Huchtia.

</section>

<section class="monument">

### [Sainte-Engrâce]{.spatial} - [Huchtia]{.coverage} Sud haut - [Tertres d'habitat]{.type}

-   Hucthia Sud haut

    -   Localisation

        Altitude : 821m.

        Situé à une quarantaine de mètres au Nord-Ouest du monument précédent et tangent au Nord-Est du chemin pastoral.

    -   Description

        Tertre terreux asymétrique érigé sur terrain en légère pente vers le Nord-Est, mesurant 5m de diamètre et 0,40m de haut. On en distingue semble-t-il un autre à 10m au Sud-Est.

-   Huchtia Sud bas

    Situé à une trentaine de mètres au Nord-Ouest du précédent, il mesure 6m de diamètre et 0,40m de haut.

</section>

<section class="monument">

### [Sainte-Engrâce]{.spatial} - [Huchtia]{.coverage} Nord - [Tertres d'habitat]{.type}

-   *Huchtia Nord haut*

    -   Localisation

        Altitude : 819m.

        Il est tangent au Nord-Est du chemin pastoral.

    -   Description

        Tertre terreux asymétrique mesurant 8m de diamètre et 0,40m de haut.

-   *Huchtia Nord bas*

    Tertre situé à une vingtaine de mètres au Nord-Est du précédent, il en a sensiblement les mêmes dimensions.

</section>

<section class="monument">

### [Sainte-Engrâce]{.spatial} - [Huchtia]{.coverage} (groupe central) - [Tertres d'habitat]{.type}

#### Localisation

Altitude : 815m.

Cet ensemble de 8 petits tertres se trouve à une soixantaine de mètres plus bas et à l'Est du terre Huchtia Nord haut. Leur finalité n'est pas évidente...

</section>

<section class="monument">

### [Sainte-Engrâce]{.spatial} - [Huchtia]{.coverage} Est 1 - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 306m.

Situé à une quarantaine de mètres à l'Est du groupe précédent, ce tertre est bien visible (entre deux arbres) du fait de ses grandes dimensions (15m de diamètre et 0,80m de haut).

</section>

<section class="monument">

### [Sainte-Engrâce]{.spatial} - [Hustarte]{.coverage} - ([?]{.douteux})

#### Localisation

Altitude : 1462m.

Ce monument est situé, en terrain plat, sur le flanc Est d'un haut plateau qui s'étend au Nord du sommet dit Heylé Gagne.

#### Description

Dix-sept pierres, de la taille d'un ou deux gros pavés, délimitent un rectangle mesurant 3,20m de long et 2,60m de large, à grand axe orienté Nord-Ouest Sud-Est ; il s'agit très probablement des restes d'un habitat pastoral, d'époque indéterminée très semblable à celui que nous avons signalé à Erainze (commune de Sainte-Engrâce) [@blotInventaireMonumentsProtohistoriques2018].

#### Historique

Vestige découvert par [P. Velche]{.creator} en [septembre 2017]{.date}.

</section>

<section class="monument">

### [Sainte-Engrâce]{.spatial} - [Izeytochiloa]{.coverage} - [Tertres d'habitat]{.type}

#### Localisation

Altitude : 1183m.

Ces trois tertres se trouvent à une cinquantaine de mètres à l'Ouest Nord-Ouest du cayolar du même nom, et à une vingtaine de mètres à l'Ouest de la route, en bordure d'un petit riu qui naît à ce niveau.

#### Description

-   Le premier tertre, terreux et asymétrique, est le plus près de la route et sous le départ d'une piste herbeuse ; il mesure 8m de diamètre et 2m de haut.

-   Le second tertre est à 8m au Nord-Ouest du premier. Il mesure 8m de diamètre et 1,50m de haut ; le troisième est à 3m à l'Est du précédent et mesure 8m de diamètre et 1,80m de haut.

#### Historique

Tertres trouvés par [Blot J.]{.creator} en [novembre 2015]{.date}.

</section>

<section class="monument">

### [Sainte-Engrâce]{.spatial} - [Izeytochiloa]{.coverage} - [Plate-forme]{.type} ([?]{.douteux})

#### Localisation

Altitude : 1193m.

Ce vestige se trouve à une soixantaine de mètres à l'Ouest des tertres précédents et de la route.

#### Description

Aménagement du terrain, (en partie creusé aux dépens de la pente voisine) en une vaste plate-forme de 25m de long selon sur son axe Nord-Sud et de 6m de large (axe Est-Ouest).

Finalité difficile à préciser : plate-forme pour fabrication du charbon de bois ? poste de tir pour artillerie ? pour habitat pastoral ?

#### Historique

Découvert par [Blot J.]{.creator} en [novembre 2015]{.date}.

</section>

<section class="monument">

### [Sainte-Engrâce]{.spatial} - [Négumendi]{.coverage} - [Tertres d'habitat]{.type}

#### Localisation

Altitude : 1280m.

Une fois laissée la piste qui mène au pluviomètre, aller vers l'extrémité Ouest du plateau de ce nom, (au moment où il amorce une descente).

#### Description

On peut voir en divers endroits des reliefs dont 4 au moins nous paraissent des vestiges de Tertres d'habitat, en particulier celui auprès d'un arbre : il mesure 5m de diamètre, 0,40m de haut et présente une légère excavation en son centre.

#### Historique

Tertres trouvés par [Blot J.]{.creator} en [octobre 2013]{.date}.

</section>

<section class="monument">

### [Sainte-Engrâce]{.spatial} - [Orkhatzolhatzé]{.coverage} - [Grand tertre]{.type}

#### Localisation

Altitude : 1270m.

Ce tertre borde à l'Est une piste pastorale qui court au flanc Est du mont Negumendi, dominant la fontaine d'Orkhatzolhatzé.

#### Description

Grand tertre de terre, de forme ovale, à grand axe Nord-Ouest Sud-Est, mesurant 25m de long, 8m de large et 2,50m de haut environ. La finalité de ce tertre (ou Tumulus ?) n'est pas évidente.

#### Historique

Tertre découvert par [Blot J.]{.creator} en [octobre 2014]{.date}.

</section>

<section class="monument">

### [Sainte-Engrâce]{.spatial} - [Orkhatzolhatzé]{.coverage} - [Tertres d'habitat]{.type}

#### Localisation

Altitude : 1260m.

Sur la piste (élargie) qui mène au plateau d'Orkhatzolhatze sous-jacent.

#### Description

Deux tertres circulaires très nets bordent la piste pastorale, espacés d'une dizaine de mètres, chacun mesurant 5m à 6m de diamètre et 0,45m de haut environ.

#### Historique

Tertres découverts par [Blot J.]{.creator} en [octobre 2013]{.date}.

</section>

<section class="monument">

### [Sainte-Engrâce]{.spatial} - [Pénalargue]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 1473m.

Il est situé au sommet de la colline de ce nom.

#### Description

Nous avions publié succinctement ce tumulus en 1973 [@blotNouveauxVestigesMegalithiques1972a]. Depuis il a subi des dégradations importantes (fouilles clandestines probables). Il se présentait sous la forme d'un tumulus terreux circulaire en forme de galette aplatie, mesurant 13m de diamètre et 0,80m de haut, avec quelques rares pierres visibles en surface. Il a été, depuis, profondément remanié, et l'on peut distinguer deux traces d'excavation, l'une au Sud-Est, l'autre au Nord-Ouest, toutes deux atteignant le centre du monument.

#### Historique

Monument découvert par [Blot J.]{.creator} en [1971]{.date} et revisité en [juillet 2016]{.date}.

</section>

<section class="monument">

### [Sainte-Engrâce]{.spatial} - [Taillade (Col de la)]{.coverage} - [Tertres]{.type} ou [Tumuli]{.type} ([?]{.douteux})

#### Localisation

Altitude : 1393m.

On peut noter sur le plateau situé au Sud-Ouest de la route, en bordure de celle-ci, une dizaine de petites éminences de terre faites de main d'homme, *sur terrain plat*, échelonnées selon un axe Sud-Ouest Nord-Est ; elles mesurent environ 6m de diamètre, 0,30m à 0,40m de haut, et sont espacées de 2 à 3m en moyenne. Une autre est visible à quelques mètres à l'Ouest d'une cabane de construction récente au Sud de l'alignement ci-dessus décrit. Il est difficile de les dénommer tertres ou tumulus, au sens que nous donnons habituellement à ces termes. Elles sont très semblables à ce que nous avons signalé à Coutchet-de-Planté (Lanne).

On en retrouve aussi un grand nombre, côté Nord-Est de la route, *sur terrain en pente*, et à plusieurs niveaux. Banquettes de tir ?

#### Historique

Monuments découverts par [Blot J.]{.creator} en [juillet 2016]{.date}.

</section>

<section class="monument">

### [Sainte-Engrâce]{.spatial} - [Sohotholhatzé]{.coverage} (groupe du bas) - [Tertres d'habitat]{.type}

#### Localisation

En contre-bas de la route qui rejoint le cayolar de Sohotolhatzé à celui d'Anhaou.

Altitude : 1269m.

#### Description

Un groupe de 4 Tertres d'habitat répartis par 2 groupes de 2 tertres sur les bords des petits rius qui sillonnent la pente à cet endroit. Dimensions moyennes : 4m à 5m de diamètre et 0,60m de haut.

#### Historique

Tertres découverts par [Blot J.]{.creator} en [octobre 2013]{.date}.

</section>

<section class="monument">

### [Sainte-Engrâce]{.spatial} - [Soum de Léche]{.coverage} - [Dolmen]{.type} ([?]{.douteux})

#### Localisation

Altitude : 1680m.

On y accède en suivant le GR 10 qui part du col de la Pierre Saint-Martin pour descendre à Sainte-Engrâce. On le suit sur environ 1800m, et à environ 1800m, sur la gauche et on voit la structure à une cinquantaine de mètres en contre-bas de la piste.

#### Description

Quatre dalles de [grés]{.subject} local, friable, plus ou moins dressées et une autre au sol pourraient délimiter une [chambre funéraire]{.subject} de plus de 3m de long et 0,70m de large, orientée Est-Ouest, le tout sur un terrain en forte pente vers le Nord...

La paroi Sud de cette chambre serait formée par 2 dalles : une dalle principale la plus à l'Ouest, inclinée vers le Nord, mesurant 2,05m à sa base et 1,58m de haut avec une épaisseur moyenne de 0,36m. Elle est séparée de 0,36m de la seconde dalle de cette paroi Sud, qui elle, est inclinée vers le Sud et mesure 1,36 m à sa base, 1,10m de haut et 0,21m d'épaisseur en moyenne. La paroi Nord est matérialisée par une seule dalle, inclinée vers le Nord, mesurant 1,10m à la base, 0,75m de haut et 0,36m d'épaisseur environ ; toutefois, une deuxième dalle, qui gît à environ 2m au Nord, pourrait représenter le montant manquant, normalement situé exactement à l'Est de la précédente, et qui aurait glissé après désinsertion. Elle mesure 1,44m dans son plus grand axe Est-Ouest, 1,37m dans l'axe Nord-Sud, et son épaisseur est de 0,25m en moyenne.

Plus au Nord, dans la pente, à 32m de ces éléments, gît une dalle, mesurant 2,20m dans son grand axe Sud-0uest Nord-Est, 1,36m dans l'axe Nord-Ouest Sud-Est et 0,35m d'épaisseur. Cet élément pourrait, dans l'optique d'un dolmen, être considéré comme la dalle de couverture ayant glissé jusque-là. Toutefois, compte tenu de la très forte pente du terrain, du contexte local où se voient d'autres dalles mises à jour par l'érosion, nous considérons cette hypothèse « dolménique » comme assez peu probable...

#### Historique

Structure découverte par [P. Velche]{.creator} en [Octobre 2016]{.date}.

</section>

# Saint-Esteben

<section class="monument">

### [Saint-Esteben]{.spatial} - [Atxapuru]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 403m.

Il est situé à une cinquantaine de mètres au Nord du point sommital de la petite colline allongée en direction Nord-Sud, dénommée *Atxapuru*.

#### Description

Tumulus pierreux circulaire de 2,50m de diamètre et 0,20m de haut ; on note à sa surface une quinzaine de petits blocs calcaire blanc bien visibles.

#### Historique

Tumulus découvert par [F. Meyrat]{.creator} en [août 2015]{.date}.

</section>

# Saint-Etienne-de-Baigorri

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Aintziagakolepoa]{.coverage} 1 - [Cromlech]{.type}

#### Localisation

Altitude : 777m.

Monument situé sur une ligne de crête, en terrain plat, à 15m environ à l'Est d'un pointement rocheux et à 12m au Nord du GR 10.

#### Description

Cercle d'environ 2,20m de diamètre, légèrement surélevé semble-t-il, avec légère excavation centrale, le tout délimité par 8 pierres bien visibles, mais de dimensions très variables : les hauteurs pouvant varier de 0,05m à 0,20m et longueurs et largeurs peuvent aller de 0,25m x 0,20m à 0,90m x 0,80m...

#### Historique

Monument découvert par [F. Meyrat]{.creator} en [mai 2014]{.date}.

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Aintziagakolepoa]{.coverage} 2 - [Cromlech]{.type}

#### Localisation

Altitude : 797m.

Il est situé sur la même ligne de crête que *Aintziagakolepoa 1 - Cromlech*, mais à environ 250m plus au Nord. Il est à 3m au Nord du GR 10. Il se trouve à 7m à l'Ouest d'un pointement rocheux très visible.

#### Description

Ce cromlech à fait l'objet d'une fouille clandestine, non datable, qui a remanié sa structure initiale : ainsi, le secteur Sud-Ouest du cercle paraît démuni de témoins. On peut estimer son diamètre à 9m ; il reste une dizaine de pierres de dimensions variables, visibles autour de l'excavation de fouille, de forme ovale (2,10m x 1,60m, et 0,90m de profondeur). Certaines de ces pierres sont assez volumineuses (1,70mx 0,79m et 0,25m de haut), en secteur Nord par exemple.

#### Historique

Monument trouvé par [F. Meyrat]{.creator} en [mai 2014]{.date}.

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Astate]{.coverage} - [Cromlech tumulaire]{.type} ([?]{.douteux})

Monument situé en territoire du Baztan, à la limite avec la commune de St Etienne de Baigorri.

#### Localisation

Altitude : 959m.

Le [dolmen]{.subject} d'Astate se trouve dans le col du même nom, bien visible. Le possible Cromlech tumulaire, qui lui serait bien postérieur, se trouve lui-même construit sur le flanc Nord de ce tumulus dolménique et tangent au montant latéral Nord de la [chambre funéraire]{.subject}.

#### Description

Tumulus circulaire, mesurant 2,50m de diamètre et 0,30m de haut ; environ une vingtaine de dalles ou fragments de dalle, de dimensions très variables, paraissent en délimiter la périphérie, ainsi qu'un possible second cercle interne au précédent. Les dimensions en sont variables, atteignant pour l'une d'elles, au Nord-Est, 0,50m x 0,45m.

#### Historique

Monument découvert par [F. Meyrat]{.creator} en [juin 2014]{.date}.

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Astate]{.coverage} - [Monolithe]{.type} ([?]{.douteux})

Territoire du Baztan, décrit par nous dans notre Inventaire [@blotInventaireMonumentsProtohistoriques2009, p44].

#### Localisation

Altitude : 959m.

Il est situé à une quarantaine de mètres au Nord Nord-Est du [dolmen]{.subject} d'Astate et de la ligne frontière.

#### Description

Très sommaire : gros bloc de [grés]{.subject}, de près de 2m de long, couché au sol suivant un axe Est-Ouest, isolé en bord de piste pastorale, et ne semblant pas provenir d'un éboulis naturel.

#### Historique

Monolithe trouvé par [Meyrat F.]{.creator} en [juin 2014]{.date}.

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Baïhuntza]{.coverage} 1 - [Dolmen]{.type}

#### Localisation

Carte 1345 Ouest Cambo-les-Bains.

Altitude : 420m.

Il est, dans le mont Larla, à environ 900m à vol d'oiseau au Sud-Est du sommet d'Urchilo (cote 566), à l'extrémité Sud-Est de cette longue crête aux pâturages d'assez médiocre qualité. Le [dolmen]{.subject} est construit à 40m au Sud-Est d'une murette de pierres sèches, sur un terrain en légère pente vers l'Est.

#### Description

Tumulus pierreux formé de blocs de la dimension d'un ou deux pavés, mesurant 8m de diamètre et 1m de haut. La [chambre funéraire]{.subject}, ouverte à l'Est, est orientée Ouest Nord-Ouest, Est Sud-Est. Elle est délimitée par 3 grandes dalles de grès rose, profondément enfoncées dans le sol.

La dalle Ouest, dalle de chevet, de forme grossièrement triangulaire mesure 0,72 m à sa base, 0,38 au sommet, et 0,40m d'épaisseur. Elle présente des traces d'épannelage.

La dalle Sud, la plus grande, mesure 2,20m de long, 1,15m de haut et 0,30m d'épaisseur.

La dalle Nord mesure 1m de long, 1,40m de haut, et 0, 30m d'épaisseur.

On note dans le secteur Sud-Est une dalle isolée, inclinée vers l'extérieur, mesurant 1m de long, 0,80m de haut et 0,30m d'épaisseur ; il est difficile de dire si elle fait partie de la [chambre funéraire]{.subject} ou non.

#### Historique

[dolmen]{.subject} découvert en [février 1982]{.date}.

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Baïhuntza]{.coverage} 2 - [Dolmen]{.type}

#### Localisation

On le trouve à 80m au Sud de *Baïhuntza 1 - Dolmen*, en contre-bas, sur l'axe de la crête rocheuse, là où elle présente un petit col.

Altitude : 400m.

#### Description

Tumulus pierreux de 8m de diamètre et 0,50m de haut.

La [chambre funéraire]{.subject} est orientée Sud-Ouest Nord-Est et mesure 2m de long et 1m de large.

Elle est délimitée : au Sud-Ouest par 2 dalles identiques accolées l'une à l'autre, formant ainsi la paroi du chevet de 0,74m de large, 0,70m de haut et 0,30m d'épaisseur. Au Sud-Est une dalle de 1m de long, et 0,74m de haut est complétée par une seconde de 0,70m de long et 0,60m de haut. Au Nord-Est on trouve une dalle de 0,60 de long et autant de haut ; enfin au Nord-Ouest il n'existe qu'une très petite dalle appuyée au chevet, mesurant 0,70m de long et 0,40 de haut. Il semble qu'à 0,80m plus au Nord-Est, un bloc parallélépipédique de 1,30m de long et 0,40m d'épaisseur ait pu faire partie de cette paroi Nord-Ouest, et qu'il ait été déplacé ultérieurement. Il n'y a pas de table visible.

#### Historique

[dolmen]{.subject} découvert en [février 1982]{.date}.

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Baïhuntza]{.coverage} 3 - [Dolmen]{.type}

#### Localisation

Il est à 60m au Nord-Ouest de *Baïhuntza 1 - Dolmen* et inclus dans l'extrémité Ouest d'une murette en pierres sèches, au moment où elle naît de la paroi rocheuse. À cet endroit la murette paraît «chevaucher» un tumulus pierreux qui lui aurait donc été pré-existant.

#### Description

Tumulus pierreux de 7m de diamètre environ. Une dalle centrale, horizontale, de forme rectangulaire, pourrait être la table (ou couvercle) de ce dolmen qui pourrait être vierge. Cette dalle, à grand axe orienté Nord-Sud, mesure 1,70m de long, 1,20m de large, et 0,25m d'épaisseur ; tous ses côtés ont été épannelés. En outre la murette pourrait avoir réutilisé quelques pierres du tumulus qui paraît un peu dégarni dans sa partie Est.

#### Historique

[dolmen]{.subject} découvert en [février 1982]{.date}.

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Buztanzelhay]{.coverage} - [Cairn]{.type} ou [Cromlech]{.type} ([?]{.douteux})

#### Localisation

Altitude : 1007m.

Ce tas de pierre, ce [cairn]{.subject}, se trouve au sommet du pic de Buztanzelay.

#### Description

On note un amoncellement de petites dalles, de blocs de [grés]{.subject} issus des filons voisins, qui forment un « [cairn]{.subject} » de 4,40m de diamètre et 0,80m de haut. Ce qui a attiré notre attention, c'est qu'il semble que ce dépôt récent de dalles, ait pu être fait sur un cercle de pierres, un cromlech, sous-jacent, bien plus ancien, et dont on pourrait distinguer une partie du péristalithe dans le secteur Sud.

#### Historique

Monument découvert par [F. Meyrat]{.creator} en [juin 2014]{.date}.

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Buztanzelhay]{.coverage} - [Monolithe]{.type}

#### Localisation

À la limite des communes de Saint-Etienne de Baigorri et du Baztan.

Altitude : 1011m.

Il est, lui aussi, situé au sommet du pic de Buztanzelhay, à une trentaine de mètres au Sud Sud-Ouest de *Buztanzelhay - [cairn]{.subject} ou Cromlech (?)*, sur la ligne frontière ; la rupture de pente est à l'Est.

#### Description

Monolithe de [grés]{.subject} [triasique]{.subject}, de forme triangulaire, couché sur le sol suivant un axe Nod-Sud, à base Nord. Il mesure 2,77m de long, et 1,26m de large à sa base ; son épaisseur est variable, le bord Est étant le plus épais, environ 0,70m à 0,80m en moyenne, alors que le bord Ouest n'atteint que 0,46m environ. Les deux bords Est et Ouest semblent présenter des traces d'épannelage.

#### Historique

Monument découvert par [F. Meyrat]{.creator} en [juin 2014]{.date}.

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Buztanzelhay]{.coverage} - [Dolmen]{.type} ou [Ciste]{.type} ([?]{.douteux})

#### Localisation

Territoire du Baztan, tangent à la frontière.

Altitude : 1010m.

Il est à environ 50m environ au Nord de *Buztanzelhay - [cairn]{.subject} ou Cromlech (?)*.

#### Description

Monument douteux, situé à l'Est et à proximité immédiate d'un éboulis de pierraille, il semble qu'on puisse distinguer un tumulus pierreux de 6 à 8m de diamètre et environ 1m de haut, très irrégulier dans ses dimensions, (avec ébauche de péristalithe au Sud ?). La [chambre funéraire]{.subject} ne serait pas au centre mais décalée vers le Nord ; environ 7 dalles paraissent la délimiter : le montant Sud mesure 1,15m de long et 0,80m de haut ; la paroi Est est marquée par deux dalles : l'une 0,80m de long, l'autre de 0,60m de long, délimitant ainsi une [chambre funéraire]{.subject} de 1,55m x 1,10m, orientée Nod-Sud.

#### Historique

Monument découvert par [Meyrat F.]{.creator} en [juin 2014]{.date}.

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Buztanzelhay]{.coverage} 1 - [Cromlech]{.type}

Sur la ligne frontière entre territoire du Baztan et commune de Baigorri.

#### Localisation

Altitude : 1002m.

En commune du Baztan, quasi tangent à la ligne frontière, sur un terrain en pente légère vers le Nord Nord-Ouest., la rupture de pente est à 3m à l'Est. Il est à 17m à l'Est de *Buztanzelhay - [dolmen]{.subject} ou [ciste]{.subject} (?)*.

#### Description

Cercle assez mal identifiable du côté Sud, de 4,50m de diamètre, délimité par environ 8 à 9 pierres ; une excavation centrale de 1,60m x 1,10m semble signer une ancienne fouille clandestine.

#### Historique

Monument trouvé par [Meyrat F.]{.creator} en [juin 2014]{.date}.

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Buztanzelhay]{.coverage} 2 - [Cromlech]{.type}

#### Localisation

En territoire du Baztan, quasi tangent à la ligne frontière.

Il est à 0,80m au Nord de *Buztanzelhay 1 - Cromlech*.

#### Description

Cercle légèrement tumulaire de 2,80m de diamètre, délimité par 7 pierres ; 4 autres sont visibles au centre ; hauteur du tumulus : 0,40 à 0,50m.

#### Historique

Monument trouvé en [juin 2014]{.date} par [F. Meyrat]{.creator}.

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Buztanzelhayko lepoa]{.coverage} - [Tertres d'habitat]{.type}

#### Localisation

On trouve, sur les deux versants du col de Buztanzelay, (essentiellement sur le versant Sud-Est), et répartis de part et d'autre de la ligne frontière, un ensemble de 16 tertres d'habitat et plus. Nous ne citerons que 16 tertres d'habitat ; il en existe au moins une dizaine d'autres sur le versant Sud-Ouest du col, côté Baztan, dissimulés actuellement par les fougères...

-   *TH 1* (Baztan) : Altitude : 862m. Tertre ovale asymétrique mesurant 11m x 10m et 1,20m de haut.

-   *TH 2* (Baztan) : situé à 11m au Nord du précédent. Tertre ovale asymétrique mesurant 10m x 8m et 1 m de haut.

-   *TH 3* (Baztan) : situé à 2m à l'Ouest du précédent ; tertre ovale asymétrique de 8m x 6m et 0,40m de haut.

-   *TH 4* (St Etienne de Baigorri) : situé à 40m au Nord du précédent. Tertre ovale asymétrique mesurant 10m x 8m et 0,30m de haut.

-   *TH 5* (St Etienne de Baigorri) : situé à 35m à l'Ouest du précédent. Tertre ovale de 11m x 8m et 0,40m de haut.

-   *TH 6* (St Etienne de Baigorri) : situé à3m à l'Ouest du précédent. Tertre ovale mesurant 15m x 10m et 0,80m de haut.

-   *TH 7* (St Etienne de Baigorri) : Altitude : 841m. Situé à 2m à l'Ouest Nord-Ouest du précédent. Tertre ovale mesurant 8m x7m et 0,30m de haut.

-   *TH 8* (St Etienne de Baigorri) : Altitude : 843m. Situé à 65m au Nord-Ouest du précédent. Tertre ovale asymétrique mesurant 11m x 6m et 0,40m de haut.

-   *TH 9* (St Etienne de Baigorri). Situé à 30m au Nord-Ouest du précédent. Tertre ovale asymétrique mesurant 17m x 7m et 0,40m de haut.

-   *TH 10* (St Etienne de Baigorri) : Altitude : 854m. Situé à 150m au Nord-Ouest du précédent. Tertre ovale asymétrique mesurant 11m x 8m et 1,30m de haut. Il y a 100m environ entre ce tertre et le rocher situé au Sud Sud-Est, à la bifurcation du GR 10.

-   *TH 11* (Baztan) : Altitude : 857m. Il est situé à 30m au Sud-Ouest du rocher situé à la bifurcation du GR10. Tertre ovale asymétrique mesurant 11m x 8m et 0,60m de haut.

-   *TH 12* (Baztan) : situé à 3m au Sud-Ouest du précédent. Tertre ovale asymétrique mesurant 6m x 5m et 0,30m de haut.

-   *TH 13* (Baztan) : situé à 3m au Sud-Est du précédent. Tertre ovale asymétrique mesurant 5m x 4m et 0,20m de haut.

-   *TH 14* (Baztan) : Altitude : 841m. Situé à 80m à l'Est du précédent. Tertre ovale asymétrique mesurant 15m x 8m et 0,40m de haut.

-   *TH 15* (Baztan) : situé à 3m au Sud-Est du précédent. Tertre ovale asymétrique mesurant 17m x 8m et 0,60m de haut.

-   *TH 16* (Baztan) : Altitude : 835m. Situé à 55m au Sud-Est du n°15 et à 15m au Sud-Ouest du GR10. Tertre (?) très volumineux de 24m x 12m et 2,60m de haut.

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Elhorriko-kaskoa]{.coverage} - [Cromlech]{.type}

#### Localisation

Carte 1346 Ouest Saint-Etienne-de-Baigorri.

Altitude : 910m.

Il est situé sur un petit replat à l'extrémité Sud-Est de la crête de ce nom qui domine, au Nord-Est, le col d'Elhorietta.

#### Description

Cercle de 3m de diamètre délimité par 7 pierres bien visibles, dont certaines atteignent 0,70m de haut ; on en compte 3 au Nord, 1 à l'Est, 1 à l'Ouest, 2 au Sud-Ouest.

#### Historique

Monument découvert en [mai 1974]{.date}.

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Elhorrieta]{.coverage} - [Monolithe]{.type} ([?]{.douteux})

#### Localisation

Baztan

Altitude : 850m.

On le trouve sur le versant Sud de Nekaitz, couché dans le sens de pente (vers l'Ouest).

#### Description

Bloc de [calcaire]{.subject} parallélépipédique, pointu à son extrémité Est. Il mesure, au total, plus de 5m de long, mais est fragmenté (gel ?) en 3 morceaux dont le plus grand mesure 3,40m de long, 2,05m à sa base, et 0,50m d'épaisseur. Etant donné sa structure, il n'est pas aisé de distinguer d'éventuelles traces d'épannelage. *Monolithe douteux*.

#### Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Elhorrieta]{.coverage} (groupe Nord) - [Tertres d'habitat]{.type}

#### Localisation

Altitude : 860m.

Ils sont situés au Nord de la piste qui rejoint la BF 99 au col de Nekaitz, en venant d'Elhorrieta, à l'endroit où nait un petit riu descendant vers le Nord.

#### Description

On distingue 6 tertres d'habitat (4 à l'Est et 2 à l'Ouest), au relief atténué par la colluvion.

#### Historique

Monuments découverts par [Blot J.]{.creator} en [octobre 2011]{.date}.

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Elhorrieta]{.coverage} (groupe Sud (a) et (b)) - [Tertres d'habitat]{.type}

Baztan

Nous citons ici ces deux groupes de tertres d'habitat, car, avec le précédent, ils forment un ensemble - concernant la vie de ces bergers de la protohistoire - complémentaire de leurs nécropoles si proches.

-   *Groupe (a)*

    Altitude : 820m.

    On distingue les reliefs de plusieurs tertres d'habitat en bordure de la naissance d'un petit riu qui descend vers l'Ouest.

-   *Groupe (b)*

    Altitude : 810m.

    Même chose pour les berges de ce petit riu né plus bas.

#### Historique

Tertres d'habitat découverts par [Blot J.]{.creator} en [octobre 2011]{.date}.

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Galarzé]{.coverage} - [Tumulus]{.type}

#### Localisation

Carte 1345 Ouest Cambo-les-Bains.

Altitude : 530m.

Un ensemble d'une quinzaine de tumulus pierreux se trouve dans une fougeraie enclose d'une murette de pierres, sur un terrain en légère pente, à l'Est du pic Iparla, en contre-bas et au Nord-Ouest du col de Galarzé.

#### Description

Ces 15 tumulus sont identiques et espacés les uns des autres de 2 à 6m. Chacun d'eux se présente sous la forme d'une couronne de pierres, de 2m à 2,70m de diamètre, à l'intérieur de laquelle on trouve un remplissage de blocailles de dimensions plus modestes, le tout atteignant 0,40m de haut. Nous ignorons totalement la signification de ces constructions.

#### Historique

Cet ensemble de tumulus nous a été indiqué le [29 mars 1993]{.date} par Mr [Peyo Currutcharry]{.creator} que nous tenons à remercier ici.

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Iparla]{.coverage} 3 - [Monolithe]{.type}

#### Localisation

Altitude : 990m.

Il est situé à une cinquantaine de mètres au Sud de la BF 90, et à environ 6m à l'Ouest du GR 10, sur une pente inclinée vers l'Ouest.

#### Description

Important bloc parallélépipédique de [grés]{.subject} [triasique]{.subject}, allongé selon un axe Est-Ouest, présentant une extrémité Est plus rétrécie (0,60m) sur laquelle on voit la marque blanche et rouge du GR 10. Il mesure 4m de long, 1,40m de large et 0,45m d'épaisseur en moyenne. Il semble présenter des traces d'épannelage à son extrémité Est et sur son bord Nord, sur 1m environ, près de la pointe. Pour P. Velche, il semblerait qu'à une cinquantaine de mètres au Sud, une faille naturelle ait pu être le lieu d'extraction de ce monolithe.

#### Historique

Monument découvert par [P. Velche]{.creator} en [août 2010]{.date}.

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Ispégui]{.coverage} - [Monolithe]{.type}

#### Localisation

Altitude : 680m.

Au col d'Ispégui, à 12m environ au Nord-Est de la route goudronnée, et à une vingtaine de mètres au Nord-Est de la BF 91.

#### Description

Monolithe dalle de [grés]{.subject} [triasique]{.subject}, couché sur un sol en légère pente vers le Sud Sud-Est. De forme grossièrement parallélépipédique, il mesure 1,90m de long, 0,70m de large à son sommet le plus étroit (à l'Ouest), et son épaisseur moyenne y est de 0,24m. À l'autre extrémité, il mesure 1,04m de large et 0,06m d'épaisseur. De nombreuses traces d'épannelage sont visibles à la totalité de sa périphérie.

#### Historique

Monolithe signalé par [J. Aguirre Mauleon]{.creator} (Aranzadi), en [avril 2014]{.date}.

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Leispars]{.coverage} - [Tumulus]{.type}

#### Localisation

Carte 1346 Ouest Saint-Etienne-de-Baigorri.

Altitude : 140m.

Il se trouve à gauche de la route, au sortir de Leispars quand on se dirige vers Saint-Etienne-de-Baïgorry. Il est visible juste de l'autre côté de la murette qui le sépare de la route.

#### Description

Important tumulus de 30m de diamètre et de près de 1,50m de haut, recouvert de gazon ; la murette a amputé environ le cinquième Ouest du monument. Les propriétaires de la maison Sallaberia, de l'autre côté de la route, se rappellent avoir vu, au début du XXème siècle, de belles dalles entourer ce tumulus. Des labours les ont ensuite supprimées et ont aussi contribué à aplanir sensiblement le monument ; il ne semblerait pas que la partie centrale ait été touchée...

#### Historique

Monument découvert en [août 1972]{.date}.

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - Les monuments du [Col d'Elhorietta]{.coverage}

#### Localisation

Carte 1346 Ouest Saint-Etienne-de-Baigorri.

Altitude : 840m.

Il existe toute une nécropole, presque aussi riche que celle d'Okabé, au col d'Elorietta située dans la longue ligne de croupes qui délimite, à l'Ouest, la Vallée des Aldudes. Huit monuments ont déjà été décrits [@barandiaranCronicaPrehistoriaBaigorri1949, p.71] ; nous apportons ici un complément à cet inventaire, tout en faisant remarquer que la ligne frontière, passant par le col, certains monuments se trouvent en territoire du Baztan. [Nous]{.creator} les décrivons cependant pour l'unité de cette nécropole qui ignorait les frontières à cette époque.

L'ensemble des monuments se trouve entre les BF 101 et 102 ; nous partirons de la BF 101. Rappelons que [J.M. de Barandiaran]{.creator} décrivait en [1949]{.date} :

-   Un *cromlech n°1*, de 4m de diamètre et 13 pierres ;

-   Un *cromlech n°2*, de 4m de diamètre et 20 pierres ;

-   Un *cromlech n°3*, de m de diamètre, 11 pierres, légèrement tumulaire ;

-   Un *cromlech n°4*, de 2m de diamètre et 12 pierres ;

-   Un *cromlech n°5*, de 6 m de diamètre et 23 pierres ;

-   Un *cromlech n°6*, de 5m de diamètre et 6 pierres ;

-   Un *cromlech n°7*, de 3m de diamètre et 12 pierres ;

-   enfin un *tumulus (n° 8)*, de terre et de pierres, de 6m de diamètre, profondément excavé ([dolmen]{.subject} complètement détruit ?).

-   Le *tumulus n° 9* Se trouve à 1m au Sud de la BF 101.

    -   Description : Petit tumulus de 5m de diamètre et 0,30m de haut, à la surface duquel apparaissent une quinzaine de petites dalles sans ordre apparent ; il a été excavé et remanié dans son secteur Sud-Est.
    -   Historique : Monument découvert en [mai 1974]{.date}.

-   *Cromlech Elhorrieta 10*. [Baztan]{.spatial}.

    -   Localisation : Il est tangent au Nord-Est du n°3.
    -   Description : Cercle de 2m de diamètre, délimité par 9 petites pierres, dont 2 ou 3 seraient communes au cromlech n°3.
    -   Historique : Découvert en mai 1974.

-   *Cromlech Elhorrieta 11*. Baztan.

    -   Localisation : Tangent à l'Ouest au n°3.
    -   Description : Une dizaine de pierres délimite un petit cercle de 2m de diamètre, dont 2 à 3 seraient communes au n°3. Peut-être même y aurait-il un cromlech 11 bis à l'Ouest du n°11 et tangent au n°4.
    -   Historique : Découvert en mai 1974.

-   *Cromlech Elhorrieta 12*. Saint-Etienne-de-Baigorri.

    -   Localisation : À 5m au Sud-Ouest du n°4.
    -   Description : Cercle de 4m de diamètre délimité par 5 pierres.
    -   Historique : Découvert en mai 1974.

-   *Cromlech Elhorrieta 13*. Baztan.

    -   Localisation : À 20m au Nord Nord-Est du n°7 et à 22m au Sud-Est du n°8.
    -   Description : Cercle de 2,50m de diamètre, délimité par une quinzaine de pierres, bien visibles, blocs de grès arrondis du volume d'un gros pavé.
    -   Historique : Découvert en mai 1974.

-   *Cromlech Elhorrieta 14*. Baztan.

    -   Localisation : Tangent au Nord Nord-Ouest du n°13.
    -   Description : Petit cercle de 1,50m de diamètre, délimité par une douzaine de pierres du type galet arrondi ; 2 sont visibles au centre du cercle.
    -   Historique : Découvert en mai 1974.

-   *Cromlech Elhorrieta 15*. Baztan.

    -   Localisation : À 2m à l'Ouest Sud-Ouest du n°8.
    -   Description : Cercle de 3m délimité par une dizaine de pierres au ras du sol.
    -   Historique : Découvert en mai 1974.

-   *Cromlech Elhorrieta 16*. Baztan.

    -   Localisation : À 2m à l'Ouest Sud-Ouest du n°15.
    -   Description : Cercle de 5m de diamètre délimité par une douzaine de pierres.
    -   Historique : Découvert en mai 1974.

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Nekaitz]{.coverage} - [Tumulus]{.type} ([?]{.douteux})

#### Localisation

Baztan

Altitude : 840m.

Au milieu du petit col qui sépare Elhorrieta de Nekaitz, à l'Ouest de la BF 100.

#### Description

Petit tumulus terreux, circulaire, parfaitement régulier, de 2,30m de diamètre et 0,15m de haut. En son centre apparaît une grosse pierre.

#### Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Oilarandoy]{.coverage} - [Tumulus]{.type} ([?]{.douteux})

#### Localisation

Altitude = 921m.

Ce probable tumulus est situé en contre-bas de la chapelle, à environ 70m au Nord-Est de celle-ci sur le replat qui s'amorce après la pente ; il est à environ 10m au Sud Sud-Ouest d'un ensemble de blocs rocheux.

#### Description

Tumulus terreux circulaire, de 7m de diamètre environ, en forme de galette très aplatie (environ 0,10m de haut) ; on distingue dans sa moitié E. cinq pierres au ras du sol : (éléments d'un péristalithe ?). Monuments douteux.

#### Historique

Tumulus découvert par [F. Meyrat]{.creator} en [Août 2019]{.date}.

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Urdiako Lepoa]{.coverage} - [Cromlech]{.type}

#### Localisation

Carte 1346 Ouest Saint-Etienne-de-Baigorri.

Altitude : 960m.

Il est situé dans le col situé au Sud et en contre-bas des monuments d'Urdiakoharria (*Urdiakoharria 3 - Cromlech*...), juste à la fin de la descente.

#### Description

Cercle de 4m de diamètre, délimité par 6 pierres dont l'une atteint 0,40m de long et l'autre 0,60m. Au centre, une dalle allongée dans le sens Nord-Ouest Sud-Est pourrait faire partie de la [ciste]{.subject}.

#### Historique

Monument découvert en [juillet 1972]{.date}.

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Urdiakoharria]{.coverage} 3 - [Cromlech]{.type}

#### Localisation

Carte 1346 Ouest Saint-Etienne-de-Baigorri.

Altitude : 960m.

Nous avons publié [@blotNouveauxVestigesMegalithiques1972c, p. 206], un cromlech (n°1) et un tumulus (n°2), sur cette longue croupe orientée Est-Ouest, dominée au Sud-Ouest par le sommet Adarza, et au Nord-Est par le Munhoa.

#### Description

Ce n°3 est à 17m au Nord-Est du n°1.

Vingt pierres plantées très régulièrement en arc de cercle paraissent bien représenter les vestiges d'un cromlech de 20m de diamètre environ.

#### Historique

Monument découvert en [juillet 1972]{.date}.

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Urdiakoharria]{.coverage} 4 - [Cromlech]{.type}

#### Localisation

Il est situé à 130m à l'Ouest Sud-Ouest du n°1 [@blotNouveauxVestigesMegalithiques1972b, p. 206].

#### Description

Cercle de 3m de diamètre délimité par 6 pierres ; au centre, une dalle de forme grossièrement triangulaire, couchée sur le sol, pourrait être le couvercle de la [ciste]{.subject}.

#### Historique

Monument découvert en [juillet 1972]{.date}.

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Urdiakoharria]{.coverage} 5 - [Cromlech]{.type}

#### Localisation

À 14m au Nord-Ouest de *Urdiakoharria 3 - Cromlech*.

#### Description

Cercle de 4,40m de diamètre délimité par 11 pierres, dont une particulièrement importante, de 1m de long, dans le secteur Est Sud-Est. Au centre, 2 dalles verticales de 0,40m de long, allongées selon un axe Est-Ouest et distantes l'une de l'autre pourraient faire partie de la [ciste]{.subject} centrale.

#### Historique

Monument découvert en [juillet 1972]{.date}.

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Urdiakoharria]{.coverage} - [Dolmen]{.type}

#### Localisation

Altitude : 950m.

Il est à 12m au Sud-Est de *Urdiakoharria 5 - Cromlech*.

#### Description

Au centre d'un tumulus faiblement marqué (0,20m de haut) de 5m de diamètre environ, on note une dalle de [calcaire]{.subject} rectangulaire mesurant 1,56m de long, 1,10m de large et 0,18m d'épaisseur ; elle présente des traces très nettes d'épannelage sur tout son pourtour. Son grand axe est orienté Nord-Sud, et elle est inclinée, son extrémité Nord-Ouest s'enfonçant légèrement sous terre. On note 2 petits blocs rocheux à quelques centimètres de son angle Sud-Ouest.

#### Historique

Monument découvert par [J. Blot]{.creator} en [juillet 2011]{.date}.

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Urdiakoharria]{.coverage} 3 - [Tumulus]{.type}

#### Localisation

Altitude : 940 m.

Il est à 18mètres au Nord-Ouest de la route.

#### Description

Tumulus terreux de faible hauteur (0,30m) mesurant 7,30m de diamètre.

#### Historique

Monument découvert par [J. Blot]{.creator} en [juillet 2011]{.date}.

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Urdiakoharria]{.coverage} - [Tertre d'habitat]{.type}

#### Localisation

Il est à 10m à l'Ouest de *Urdiakoharria 4 - Cromlech*.

Altitude : 945m.

#### Description

Tertre ovale à grand axe Nord-Est Sud-Ouest, érigé sur un terrain en légère pente vers le Nord.

Il mesure 11m de long, 8,50m de large et 0,60m à 0,80m de haut.

#### Historique

Tertre découvert par [J. Blot]{.creator} en [juillet 2011]{.date}.

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Nékaitzko lepoa]{.coverage} - [Dalle couchée]{.type}

#### Localisation

Territoire du Baztan, à côté de la BF 99.

Altitude : 857m.

#### Description

Dalle de [grés]{.subject} couchée au sol suivant un axe Est Nord-Est Ouest Sud-Ouest. Affectant grossièrement une forme de stèle discoïdale, elle mesure 1,44m de long, 0,52m de large à son sommet et 0,41m de large à la base ; sa partie la plus étroite mesure 0,29m. Son épaisseur varie entre 0,24m et 0,32m. Tout son pourtour présente des traces évidentes d'épannelage.

Il est probable qu'il s'agisse d'une ancienne BF ou d'une ébauche abandonnée...

#### Historique

Dalle trouvée en [août 2014]{.date} par [F. Meyrat]{.creator}.

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Olhate]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 815m.

Ce tumulus est à 80m au Sud Sud-Ouest de la BF 95, et tangent à l'Est à la piste de crête.

#### Description

Tumulus mixte de terre et de pierres, d'environ 7m de diamètre, de faible hauteur : 0,30m. De nombreuses pierres sont visibles en surface et 7 à 8 pourraient délimiter un péristalithe... La présence d'une dalle plus importante (0,57m x 0,40m) bien visible au Nord-Est du centre pourrait évoquer un montant de [chambre funéraire]{.subject} ([ciste]{.subject} dolménique ?).

#### Historique

Monument trouvé par [F. Meyrat]{.creator} en [août 2014]{.date}.

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Munhogain]{.coverage} - [Dolmen]{.type}

#### Localisation

Altitude : 900m.

Ce monument est situé au flanc Sud Sud-Est du mont Munhogain, à 16m au Sud-Ouest de la piste qui monte au sommet.

#### Description

De ce monument, érigé sur un terrain en très faible pente vers le Sud-Est., il ne reste qu'un tumulus de très faible hauteur et une «grande» dalle, verticale, bien visible au Sud-Ouest, ainsi que 4 autres fragments de dalles constitutifs du montant Nord-Ouest, probablement la dalle de chevet, dont il ne reste que ces fragments. La grande dalle, en [calcaire]{.subject} blanc, mesure 1,40m de long à sa base et 0,96m au sommet ; elle atteint 0,50m de haut, et 0,17m d'épaisseur.

Les 4 fragments de dalle sont disposés perpendiculairement à cette grande dalle, à son extrémité Nord-Ouest ; ils mesurent respectivement 0,40m, 0,27m, et 0,26m de long.

On peut estimer que la [chambre funéraire]{.subject}, orientée Nord-Ouest Sud-Est., mesurait environ 1,50m de long et 0,87m de large.

Il semble persister les vestiges d'un péristalithe constitué de 8 pierres, visibles au ras du sol.

À noter enfin, à 20m à l'Est, un bloc de [calcaire]{.subject} bien visible (Borne ?), mesurant, à sa base 0,60m x 0,40m et 0,70m de haut.

#### Historique

Monument découvert par [Meyrat F.]{.creator} en [avril 2017]{.date}.

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Munhogain]{.coverage} - [Tumulus]{.type}

#### Localisation

Ce tumulus est tangent à *Munhogain - Dolmen*, au Nord-Ouest.

#### Description

Tumulus terreux, de forme ovale, mesurant 3,80m dans son plus grand axe Nord-Ouest Sud-Est, 3m de large et 0,30m de haut. Il semble bien que l'on puisse décrire un péristalithe, surtout bien visible à ses deux extrémités (6 pierres au Nord-Ouest et 4 au Sud-Est).

#### Historique

Tumulus découvert par [Meyrat F.]{.creator} en [avril 2017]{.date}.

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Urdoze]{.coverage} (d') - [Dolmen]{.type}

Nous tenons à signaler que ce [dolmen]{.subject} trouvé par [nous]{.creator} en [1972]{.date} et que nous avons publié dans le [@blotNouveauxVestigesMegalithiques1973, p.199] a depuis été détruit, sa [chambre funéraire]{.subject} rasée ; il n'en reste que le tumulus pierreux, de forme ovoïde, selon un grand axe Est Sud-Est Ouest Nord-Ouest, mesurant 8m x 6,80m et 0,30m de haut.

Altitude : 266m.

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Oilarandoy]{.coverage} - [Tumulus]{.type} ([?]{.douteux})

#### Localisation

Altitude : 921m.

Ce probable tumulus est situé en contre-bas de la chapelle, à environ 70m au Nord-Est de celle-ci, sur le replat qui s'amorce après la pente ; il est à environ 10m au Sud Sud-Ouest d'un ensemble de blocs rocheux...

#### Description

Tumulus terreux circulaire, de 7m de diamètre environ, en forme de galette très aplatie (environ 0,10m de haut) ; on distingue dans sa moitié Est cinq pierres au ras du sol : (éléments d'un péristalithe ?)... Monuments douteux.

#### Historique

Tumulus découvert par [F. Meyrat]{.creator} en [août 2019]{.date}.

</section>

## [Saint-Etienne-de-Baigorri]{.spatial} - Les monuments du secteur de l'[Antenne relai d'Urxilo]{.coverage}

Nous allons traiter maintenant d'un ensemble de « monuments » - ou supposés tels pour certains - dont la liste nous a été confiée par Eric Dupré en novembre 2019. Ces vestiges ont été identifiés par lui et son équipe lors des leurs recherches dans les communes de Baigorri et St Martin d'Arrossa.

Ces éléments archéologiques se trouvent répartis essentiellement sur la commune de Baigorri, en descendant de l'antenne-relai d'Urxilo vers la Nive de Baigorri et en suivant la crête de Baïhuntza. La nature du terrain, couvert naturellement, de par sa structure géologique, de très nombreux et abondants débris de dalles - lesquelles sont par endroit soulevées par la pousse des arbres - peut être source de nombreuses erreurs ; c'est pourquoi nous resterons très prudent quant aux qualifications à attribuer aux différents éléments ci-après décrits.



<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Antenne relai d'Urxilo]{.coverage} - Bai 001 - [Coffre dolménique]{.type} ([?]{.douteux})

#### Localisation

Altitude 537m.

Ce monument est érigé sur terrain plat, à environ 10m au Sud-Est du bâtiment de l'antenne relai d'Urxilo, et le chemin qui monte à l'antenne est à 3m au Nord-Est environ semble avoir coupé un éventuel tumulus, dont les pierres s éboulent sur la coupe.

#### Description

Dénommé par [E. Dupré]{.creator} « coffre dolménique ruiné ». On peut en effet décrire une [chambre funéraire]{.subject} rectangulaire d'environ 2,50m de long et 0,60m de large, délimitée, semble-t-il par 5 dalles plantées : deux dalles au nord, non jointives dont l'une, la plus à l'ouest, mesure 1,16m de haut. Le tumulus aurait un diamètre - difficile à apprécier - de 4,80m (7,50m pour E. Dupré).

Monument cependant douteux.

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Antenne relai d'Urxilo]{.coverage} - Bai 002 - [Coffre dolménique]{.type} ([?]{.douteux})

#### Localisation

Altitude 537 m.

La piste qui mène à l'antenne passe à 12 m à l'Est du « monument ».

#### Description

Coffre dolménique ruiné pour [E.Dupré]{.creator}.

La pousse d'un arbre au centre d'un « tumulus » qui aurait 8m de diamètre a soulevé un ensemble de dalles de dimensions variables pourraient évoquer une « [chambre funéraire]{.subject} » de 1,70m de long et 0,50m de large....

Monument très douteux.

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Antenne relai d'Urxilo]{.coverage} - Bai 003 ([?]{.douteux})

Ce « monument ruiné » n'a pas pu être retrouvé sur le terrain.

Altitude : 528m.

Il aurait été un [cairn]{.subject .type} de 4,50m de diamètre.

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Antenne relai d'Urxilo]{.coverage} - Bai 004 - [Coffre dolménique]{.type} ([?]{.douteux})

#### Localisation

Altitude 529m.

Ce monument est à 10m à l'ouest de *Bai 002*.

#### Description

Pour [E. Dupré]{.creator} il s'agirait d'un coffre dolménique ruiné avec un [cairn]{.subject} de 4,5m de diamètre. Il n'a cependant été vu sur le terrain qu'un amas pierreux sans tumulus et quelques dalles plantées pouvant éventuellement délimiter une [chambre funéraire]{.subject} de 1,66m de long et 0,38m de large.

Monument douteux.

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Antenne relai d'Urxilo]{.coverage} - Bai 005 - [Coffre dolménique]{.type} ([?]{.douteux})

#### Localisation

Altitude : 532m.

#### Description

"Coffre dolménique ruiné" avec [cairn]{.subject} de 9m de diamètre pour [E. Dupré]{.creator}.

Il semblerait que l'on puisse distinguer un « tumulus » pierreux de 9m de diamètre et 0,50m de haut. Un arbre a poussé, un peu décentré vers le sud, mais des dalles plantées (relevées sans doute par la pousse de l'arbre), ne semblent délimiter aucune [chambre funéraire]{.subject}.

Monument très douteux.

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Antenne relai d'Urxilo]{.coverage} - Bai 006 - [Tumulus]{.type} ([?]{.douteux})

#### Localisation

Altitude : 526m.

#### Description

[E. Dupré]{.creator} cite « un coffre dolménique ruiné » avec un [cairn]{.subject} de 6m de diamètre. Il n'a été vu que quelques dalles jonchant le sol, dont certaines soulevées par la présence d'un arbre ; il très difficile de parler d'un tumulus (vu leur petit nombre) ou d'une [chambre funéraire]{.subject}.

Monument très douteux.

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Antenne relai d'Urxilo]{.coverage} - Bai 007 - [Coffre dolménique]{.type} ([?]{.douteux})

Ce monument n'a pas pu être identifié.

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Antenne relai d'Urxilo]{.coverage} - Bai 008 - [Cromlech]{.type} ([?]{.douteux})

#### Localisation

Altitude 525m.

Le chemin qui monte à l'antenne se trouve à 6m à l'ouest.

#### Description

Présenté par [E. Dupré]{.creator} comme « coffre dolménique ruiné » avec un [cairn]{.subject} de 5m de diamètre ; on note sur le terrain une série de dalles orientées Est - Ouest, faisant partie d'un filon rocheux en place, ceci étant particulièrement visible dans le quart Nord-Ouest ; sept autres dalles paraissent plantées dans le sol, selon une disposition grossièrement circulaire, (voir dessin, dalles en hachuré), mais il est très difficile de parler de "cromlech" !

On ne note ni tumulus, ni [chambre funéraire]{.subject} évidente.

Monument très douteux.

</section>

## [Saint-Etienne-de-Baigorri]{.spatial} - Les monuments du secteur des Cabanes

Nous allons traiter maintenant d'un ensemble de « monuments » - ou supposés tels pour certains - dont la liste nous a été confiée par Eric Dupré en novembre 2019. Ces vestiges ont été identifiés par lui et son équipe lors des leurs recherches dans les communes de Baigorri et St Martin d'Arrossa.

Ces éléments archéologiques se trouvent répartis essentiellement sur la commune de Baigorri, en descendant de l'antenne-relai d'Urxilo vers la Nive de Baigorri et en suivant la crête de Baïhuntza. La nature du terrain, couvert naturellement, de par sa structure géologique, de très nombreux et abondants débris de dalles - lesquelles sont par endroit soulevées par la pousse des arbres - peut être source de nombreuses erreurs ; c'est pourquoi nous resterons très prudent quant aux qualifications à attribuer aux différents éléments ci-après décrits.

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Les Cabanes]{.coverage} - Bai 009 - [Dolmen]{.type}

#### Description

Cité comme « [dolmen]{.subject} ruiné » par [E. Dupré]{.creator} : au point GPS indiqué, la très forte pente excluait un monument, et au point culminant, à l'aplomb, il n'a été vu qu'un semis de pierraille désordonné.

Monument en définitive non vu.

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Les Cabanes]{.coverage} - Bai 010 - [Coffre dolménique]{.type} ([?]{.douteux})

#### Localisation

Altitude 467 m.

Le chemin de crête passe à environ 18m à l'ouest du monument.

#### Description

Celui-ci est érigé sur terrain plat (ou en très légère pente vers l'est ). Il est présenté par [E. Dupré]{.creator} comme « [dolmen]{.subject} ruiné » ; il semblerait en effet que l'on puisse reconnaître une [chambre funéraire]{.subject}, mesurant 1,90m de long et 1,50m de large, orientée Nord Nord-Ouest, et délimitée par 8 dalles plus ou moins horizontales actuellement mais qui semblent cependant avoir été plantées dans le sol : à noter la dalle **A**, au Nord-Est, mesurant 0,90m x 0,62m et 0,20m de haut. On ne distingue pas de tumulus, mais une ébauche de péristalithe serait plus ou moins visible dans le secteur **E** constituée de 8 pierres (?).

Monument possible, mais douteux.

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Les Cabanes]{.coverage} - Bai 011 - [Coffre dolménique]{.type}

#### Localisation

Altitude : 459 m

Il est distant d'environ 45m de *Bai 010* qui est au Nord, et sur terrain plat ou en légère pente vers l'Est Le sentier de crête pour Urxilo passe à 20m à l'ouest du monument.

#### Description

Il semble bien que l'on puisse décrire une chambrer funéraire, orientée Est Ouest mesurant environ 2m x 0,45m délimitées par 3 dalles plantées, de dimensions fort modestes, et inclinées vers le Nord.

Une dalle plus importante, couchée au sol a 1,10m l'Ouest Sud-Ouest, pourrait être un monolithe (pierre **J** sur le dessin) présentant des traces d'épannelage à son sommet et sur son bord Sud-Ouest. 1,26m de long, 0,41m de large et 0,60m d'épaisseur.

Monument très possible.

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Les Cabanes]{.coverage} - Bai 012 - ([?]{.douteux})

#### Localisation

Altitude : 461m.

#### Description

« Monument ruiné » pour [E. Dupré]{.creator}.

Sur un terrain très en pente Est Ouest, on voit une abondante pierraille, faite de très nombreuses dalles de grès gisant au sol, sans qu'il y ait pour autant une [chambre funéraire]{.subject} discernable.

« Monument » plus que douteux.

</section>

## [Saint-Etienne-de-Baigorri]{.spatial} - Les monuments du secteur du Saroi

Nous allons traiter maintenant d'un ensemble de « monuments » - ou supposés tels pour certains - dont la liste nous a été confiée par [Eric Dupré]{.creator} en novembre 2019. Ces vestiges ont été identifiés par lui et son équipe lors des leurs recherches dans les communes de Baigorri et Saint-Martin-d'Arrossa.

Ces éléments archéologiques se trouvent répartis essentiellement sur la commune de Baigorri, en descendant de l'antenne-relai d'Urxilo vers la Nive de Baigorri et en suivant la crête de Baïhuntza. La nature du terrain, couvert naturellement, de par sa structure géologique, de très nombreux et abondants débris de dalles - lesquelles sont par endroit soulevées par la pousse des arbres - peut être source de nombreuses erreurs ; c'est pourquoi nous resterons très prudent quant aux qualifications à attribuer aux différents éléments ci-après décrits.

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Saroi]{.coverage} - Bai 013 - [Tumulus]{.type} ([?]{.douteux})

#### Localisation

Altitude : 435m.

#### Description

Il s'agit d'un amas pierreux, plus ou moins circulaire visible sur un terrain plat, de 0,15m de haut et d'environ 4m à 5m de diamètre, constitué de terre et de blocs de grès de la taille d'un pavé ; il ne semble pas que l'on puisse y distinguer ni péristalithe, ni [chambre funéraire]{.subject}.

Monument douteux.

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Saroi]{.coverage} - Bai 014 - [Dolmen]{.type} ([?]{.douteux})

#### Localisation

Altitude : 433m.

#### Historique

[Nous]{.creator} avons découvert en [février 1982]{.date} ce [dolmen]{.subject} (qui semble bien avoir été par la suite, inclus dans une murette).

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Saroi]{.coverage} - Bai 015 - [amas pierreux]{.type} ([?]{.douteux})

#### Localisation

Il n'a pas pu être identifié sur le terrain.

#### Description

Amas pierreux. Très semblable à *Bai 016 - amas pierreux (?)*.

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Saroi]{.coverage} - Bai 016 - [amas pierreux]{.type} ([?]{.douteux})

#### Localisation

Altitude : 432m.

Il est situé à environ 16m à l'Est de *Bai 013 - Tumulus (?)*.

#### Description

Cité comme amas pierreux, par [E. Dupré]{.creator}, c'est en effet le cas. On ne note ni [chambre funéraire]{.subject} ni tumulus....

Monument très douteux.

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Saroi]{.coverage} - Bai 017 - [dalle plantée]{.type} - [amas pierreux]{.type} ([?]{.douteux})

#### Localisation

Altitude 437m.

#### Description

L'amas pierreux n'a pas été vu à l'endroit indiqué par les coordonnées GPS d'[E. Dupré]{.creator} (voir plus bas *Bai 017 bis*) mais une simple borne avec une croix.

Par contre, il a été noté par un important amas pierreux, véritable cône de déblais, s'étendant en pente vers le sud sur une vingtaine de mètres et 3m de haut en moyenne à sa partie la plus fournie. Il est recouvert de végétation - Est-ce l'amas pierreux cité par E. Dupré ?

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Saroi]{.coverage} - Bai 017 bis - [borne]{.type}

#### Localisation

Le sentier de crête passe à 2m à l'Est.

#### Description

Cette borne en grès mesure 0,42m de haut, 0,24m de long et 0,13m de large et porte une croix gravée à son sommet.

#### Historique

Borne découverte par [F. Meyrat]{.creator} en [octobre 2020]{.date}.

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Saroi]{.coverage} - Bai 018 - [Dolmen]{.type} ([?]{.douteux})

#### Localisation

Altitude : 420m

#### Historique

Ce monument, découvert par [nous]{.creator} en [février 1982]{.date}.

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Saroi]{.coverage} - Bai 019 - [Tumulus pierreux]{.type} ([?]{.douteux})

#### Description

[E. Dupré]{.creator} cite un « tumulus pierreux » qui n'a pas été vu.

Par contre il a été noté un amas pierreux sur un terrain très en pente vers l'Est et délimité à l'Est et à l'Ouest par une bifurcation du sentier qui vient de Baihuntza D2 à 25m plus au sud. Ce « tumulus » pierreux de forme très irrégulièrement circulaire, (de 6m environ) présente un arbre décentré vers le Nord-Est.

Monument extrêmement douteux.

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Saroi]{.coverage} - Bai 020 ([dolmen]{.type} détruit)

#### Localisation

Altitude 405m.

#### Description

[E. Dupré]{.creator} cite un [dolmen]{.subject} détruit avec un tumulus de 5m. Aucun élément correspondant n'a été vu sur le terrain. Monument très douteux.

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Saroi]{.coverage} - Bai 021 - [Dolmen]{.type}

#### Localisation

Altitude 398m.

#### Historique

Ce [dolmen]{.subject} a été trouvé par [nous]{.creator} en [février 1982]{.date}.

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Saroi]{.coverage} - Bai 022 - [Amas pierreux]{.type} sous [monolithe]{.type} ([?]{.douteux})

#### Localisation

Altitude 382m.

#### Description

[E. Dupré]{.creator} évoque « un monument sous monolithe » qui n'a pas été vu.

Monument très douteux.

</section>

## [Saint-Etienne-de-Baigorri]{.spatial} - Les monuments du secteur des Poteaux

Ce secteur n'a pas encore pu être exploré, compte tenu tout d'abord des conditions exubérantes du couvert végétal en cet automne et, ensuite, des exigences du "confinement" pour cause de Covid 19.

Nous donnons donc ici uniquement une liste fournie par [E. Dupré]{.creator}.

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Les Poteaux]{.coverage} - Bai 023 - [Coffre dolménique]{.type}

#### Description

Monument détruit

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Les Poteaux]{.coverage} - Bai 024 - [Coffre dolménique]{.type}

#### Description

Monument détruit

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Les Poteaux]{.coverage} - Bai 025 - [Coffre dolménique]{.type}

#### Description

Monument détruit

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Les Poteaux]{.coverage} - Bai 026 - [Coffre dolménique]{.type}

#### Description

Monument détruit

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Les Poteaux]{.coverage} - Bai 027 - [Coffre dolménique]{.type}

#### Description

Monument détruit

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Les Poteaux]{.coverage} - Bai 028 - [Coffre dolménique]{.type}

#### Description

Monument détruit

</section>

<section class="monument">

### [Saint-Etienne-de-Baigorri]{.spatial} - [Les Poteaux]{.coverage} - Bai 029 - [Cairn]{.type}

#### Description

[cairn]{.subject} circulaire creusé de 6m de diamètre.

</section>

# Saint-Jean-de-Luz

<section class="monument">

### [Saint-Jean-de-Luz]{.spatial} - [Zamarina]{.coverage} - [Tumulus]{.type}

#### Localisation

Carte 1245 Ouest Saint-Jean-de-Luz.

Altitude : 80m.

Il est situé à gauche du chemin qui mène à la ferme Zamarina, en face de l'ancien dépôt d'ordures de la ville, à 20m au Nord-Est de la borne IGN 84.

#### Description

Tumulus de terre de 25m de diamètre et 1,50m de haut.

#### Historique

Découvert en [janvier 1968]{.date}. Il a été depuis rasé par de nombreux labours.

</section>

# Saint-Just-Ibarre

<section class="monument">

### [Saint-Just-Ibarre]{.spatial} - [Belchou]{.coverage} 9 - [Cromlech]{.type}

#### Localisation

À 150m à l'Est Nord-Est de *Belchou 8 - Cromlech*, dans la commune d'Hosta, sur un terrain légèrement en pente vers le Nord, toujours en bordure du relèvement de la cuvette.

#### Description

Petit cercle de 4m de diamètre délimité par une vingtaine de pierres au ras du sol.

#### Historique

Monument découvert en [août 1978]{.date}.

À 100m à l'Est Nord-Est de ce monument et à 10m à l'Est d'une profonde doline peut-être pourrait-on décrire un dixième monument, un cercle de pierres, quasi tangent à la piste, de 4,10m de diamètre et formé par 7 pierres ; il nous paraît toutefois douteux.

</section>

<section class="monument">

### [Saint-Just-Ibarre]{.spatial} - [Belchou]{.coverage} 14 - [Cromlech]{.type}

#### Localisation

Altitude : 830m.

À une centaine de mètres à l'Est Nord-Est du cromlech *C 13* (voir Hosta).

À noter que le cromlech *C 8* déjà publié [@blotInventaireMonumentsProtohistoriques2009, p.38]) se trouve lui-même à quelques dizaines de mètres à l'Est

Altitude : 830m

#### Description

Cercle de 7m de diamètre, érigé sur un terrain en légère pente vers le Nord-Est, délimité par 8 pierres bien visibles, mais absentes dans le quart Nord-Ouest ; une autre plus volumineuse se voit au centre.

#### Historique

Monument découvert par [Blot J.]{.creator} en [août 2011]{.date}.

</section>

<section class="monument">

### [Saint-Just-Ibarre]{.spatial} - [Belchou]{.coverage} (groupe Nord-Est) - [Tertres d'habitat]{.type}

#### Localisation

Altitude : 860m.

Cet important groupe de 33 tertres d'habitat est situé au pied du pic de Belchou, au débouché du vallon compris entre ce dernier et l'éminence de 998m d'altitude situé à son flanc Sud-Est.

#### Description

Ces 33 tertres sont érigés, de part et d'autre d'un petit ruisseau, sur un terrain en très légère pente, soit vers le Sud-Est (tertres de 1 à 18), soit vers le Nord-Ouest (tertres de 19 à 33).

-   Premier groupe au Nord-Ouest du petit ruisseau (tertres 1 à 18) :
    -   Le *tertre n°1* est le plus au Nord du groupe et mesure 6 x3m ;
    -   le *n°2* est à 5m au Sud-Ouest (4 x 2m) ;
    -   le *n°3* est à 4m au Sud-Ouest du précédent (15 x 8m) ;
    -   le *n°4* est à 25m au Sud-Est du précédent (13 x 10m)  ;
    -   le *n°5* est à 8m Sud-Ouest (16 x 11m) ;
    -   le *n°6* est tangent (4m de diamètre) ;
    -   le *n°7* tangent aussi (10m de diamètre) ;
    -   le *n°8* à 2m au Nord-Ouest (6m de diamètre) ;
    -   le *n°9* à 6m au Sud (10m de diamètre) ;
    -   le *n°10*, tangent (5m de diamètre) ;
    -   le *n°11* tangent aussi au n°9 (9m x 3m) ;
    -   le *n°12* tangent au n°11 (9m x 5m),
    -   le *n°13* tangent au 12, (13m x 10m) ;
    -   le *n°14*, tangent au 13 (13m x 9m ) ;
    -   le *n°15* à 3m au Sud du 14 (10m x 8m) ;
    -   le *n°16* à 4m au Sud du 15 (13m x 10m) ;
    -   le *n°17* à 7m au Sud du 16 (11m de diamètre) ;
    -   le *n°18* à 3m au Sud du 17 (3m de diamètre).
-   Deuxième groupe, au Sud-Est du petit ruisseau : (tertre 19 à 33) :
    -   Le *tertre n°19* à 15m au Sud-Est du 18 (10m de diamètre) ;
    -   le *n°20*, à 24m à l'Est du 19 (12m x 9m ) ;
    -   le *n°21* à 5m au Nord du 20 (13m x 10m) ;
    -   le *n°22* à 2m à l'Est du 21 (7m x 3m ) ;
    -   le *n°23* (à 3m au Nord-Ouest du 21 (12m x 7) ;
    -   le *n°24* à 20m au Nord-Est du 23 (8m de diamètre) ;
    -   le *n°25* à 5m à l'Est du 24(6m de diamètre) ;
    -   le *n°26*, à 4m à l'Est du 25 (7m de diamètre) ;
    -   le *n°27*, à 8m au Nord du 26 (14m x 8m) ;
    -   le *n°28* à 8m au Nord du 27 (11m x 9m) ;
    -   le *n°29*, à 10m au Nord-Ouest du 28 (12m x 8m) ;
    -   le *n°30*, à 25m au Nord-Est du 29, (10m x 4m),
    -   le *n°31*, à 2m au Nord-Est du 30 (5m de diamètre) ;
    -   le *n°32*, à 7m au Nord-Est du 31 (6m de diamètre) ;
    -   le *n°33*, à 8m au Nord-Est du 32 (9m x 4m).

#### Historique

Tertres découverts par [Blot J.]{.creator} en [1986]{.date}.

</section>

# Saint-Martin-d'Arrossa

<section class="monument">

### [Saint-Martin-d'Arrossa]{.spatial} - [Dondénia]{.coverage} 4 - [Dolmen]{.type}

#### Localisation

Carte 1345 Ouest Cambo-les-Bains.

Altitude : 380m.

Nous avons décrit un ensemble de 3 dolmens [@blotNouveauxVestigesMegalithiques1972a, p. 32], situé au Sud-Est de la ferme Pagondoa.

Ce n°4 est situé à 13m au Nord-Ouest du n°2 et à 30m au Nord-Est du n°3 [@blotNouveauxVestigesMegalithiques1972a, p. 32].

#### Description

Tumulus pierreux de 9m de diamètre et 0,30m de haut. La [chambre funéraire]{.subject} semble pouvoir être matérialisée par une dalle mesurant 1,40m de long et 0,50m de large, orientée vers le Nord-Est et apparaissant à plat (légèrement inclinée vers le Sud-Est), au sommet du tumulus. Quelques petits fragments de dalles émergent du sol de part et d'autre de la table et pourraient faire partie des montants latéraux ; une dalle couchée dans le quart Sud-Est du tumulus, et mesurant 1m de long et 0,80 de large pourrait, elle aussi, en faire partie.

#### Historique

Dolmen découvert en [mars 1974]{.date}.

</section>

<section class="monument">

### [Saint-Martin-d'Arrossa]{.spatial} - [Dondénia]{.coverage} 5 - [Tumulus]{.type} ([?]{.douteux})

#### Localisation

Carte 1345 Ouest Cambo-les-Bains.

Il est situé à 13m à l'Ouest Nord-Ouest du dolmen n°3 [@blotNouveauxVestigesMegalithiques1972a, p. 32] et à 40m au Sud-Ouest de *Dondénia 4 - Dolmen*.

#### Description

Tumulus mixte de pierres et de terre, mesurant 8m de diamètre et 0,30m de haut. *Il est traversé par la piste pastorale* et quelques fragments de dalles, brisées au ras du sol, apparaissent au centre du monument : vestige d'une chambre funéraire ? est-ce un dolmen ?.

#### Historique

Monument découvert en [mars 1971]{.date}. Rappelons aussi le *dolmen de Mikelare*, à 500m au Nord Nord-Est de Dondénia [@blotNouveauxVestigesMegalithiques1972a, p. 30].

</section>

<section class="monument">

### [Saint-Martin-d'Arrossa]{.spatial} - [Dondénia]{.coverage} 6 - [Dolmen]{.type}

#### Localisation

Altitude : 380m (à 2m au Nord-Ouest de *D 4*).

#### Description

Un ensemble de six dalles de [grés]{.subject}, plantées verticalement dans le sol et ne le dépassant que de quelques centimètres, délimitent une [chambre funéraire]{.subject} de 3,70m de long et 1,40m de large, orientée Nord-Est Sud-Ouest. On note 2 dalles pour les montants Sud-Est, l'une de 1,10m de long, l'autre de 0,74m. Au Nord-Ouest, 4 dalles, deux de 0,90m de long, la troisième de 2m de long et 0,51m de haut, la quatrième de 1,06m de long.

#### Historique

Monument découvert par [Blot J.]{.creator} en [novembre 2011]{.date}.

</section>

<section class="monument">

### [Saint-Martin-d'Arrossa]{.spatial} - [Dondénia]{.coverage} - [Pierre couchée]{.type}

#### Localisation

Situé à quelques dizaines de mètres au Sud Sud-Est du dolmen *Dondénia 2* [@blotNouveauxVestigesMegalithiques1972a, p. 32].

#### Description

Beau bloc parallélépipédique de grés rose, gisant au sol, suivant un axe Est-Ouest, mesurant 1,35m de long, 0,80m de large, et 0,42m d'épaisseur en moyenne.

Il présente de nombreuses traces d'épannelage sur son flanc Sud, et sa base comme son sommet Ouest, plus étroit, semblent avoir été brisés récemment. Travail récent ?

#### Historique

Monument découvert par [Blot J.]{.creator} en [novembre 2011]{.date}.

</section>

<section class="monument">

### [Saint-Martin-d'Arrossa]{.spatial} - [Dondénia]{.coverage} - [Pierres plantées]{.type} ([?]{.douteux})

#### Localisation

Très légèrement au Sud du groupe des *dolmens de Dondénia* (2,3,4,6).

#### Description

On peut voir quelques pierres plantées, de faibles dimensions (0,76m de haut et 0,52m de large pour l'une d'elles séparée de trente centimètres de sa voisine, cette dernière mesurant 0,60m de haut et 30m de long). Ces pierres assez distantes les unes des autres sont plus ou moins alignées selon un axe Est-Ouest, et semblent faire partie d'un très ancien enclos dont les autres vestiges se voient enfouis au ras du sol. Cet enclos (?) forme un angle droit un peu après le dolmen D 3 et se dirige vers le Sud sur environ 12m.

#### Historique

Pierres vues par [Blot J.]{.creator} en [1972]{.date}.

</section>

<section class="monument">

### [Saint-Martin-d'Arrossa]{.spatial} - [Ondaya]{.coverage} - [Tumulus]{.type}

#### Localisation

Carte 1345 Ouest Cambo-les-Bains.

Altitude : 310m.

Il est situé à quelques mètres à droite de la route qui monte de Saint-Martin-d'Arossa vers Dondénia, au niveau d'un petit col situé au premier replat après la montée.

#### Description

Tumulus pierreux de 10m de diamètre et 0,60m de haut ; à son sommet une dalle couchée pourrait être le couvercle d'une [ciste]{.subject} centrale sous-jacente dont les sommets des dalles Ouest sont visibles.

#### Historique

Monument découvert en [avril 1972]{.date}.

</section>

<section class="monument">

### [Saint-Martin-d'Arrossa]{.spatial} - [Ondaya]{.coverage} 2 - [Tumulus]{.type}

#### Localisation

Altitude : 314m.

Le n° 1 [@blotInventaireMonumentsProtohistoriques2009, p.53], est situé à 20m au Sud-Ouest de la route et le n°2 est lui-même à une cinquantaine de mètres au Nord Nord-Ouest de ce n°1 (dont on notera la dalle horizontale à son sommet, mesurant 0,70m x 0,60m et 0,30m d'épaisseur apparente).

#### Description

Tumulus pierreux de 7m de diamètre et d'une trentaine de centimètres de haut. Au centre est parfaitement bien visible une structure en petites dalles verticales disposées en « pelure d'oignon ».

#### Historique

Monument découvert par [C. Blot]{.creator} en [septembre 2010]{.date}.

</section>

<section class="monument">

### [Saint-Martin-d'Arrossa]{.spatial} - [Harretchekoborda]{.coverage} 1 - [Cromlech]{.type}

#### Localisation

Altitude : 630m.

On le trouve à gauche d'un virage à angle droit du chemin, bien marqué, qui monte au flanc Nord du pic Larla, le terrain devenant plat, avant de redescendre quelques dizaines de mètres plus loin. Sur la droite de ce virage se trouve la borde *Harretchekoborda*, et à gauche s'amorce une piste se dirigeant vers le sommet de Larla. Le monument est à gauche de cette piste, à quelques dizaines de mètres de son départ.

#### Description

Sur un terrain en pente douce vers le Nord-Est, une quinzaine de pierres, au ras du sol, paraissent délimiter un cercle en léger relief de 3m de diamètre environ ; au centre, 3 pierres feraient partie d'un petit caisson...

Il pourrait y avoir deux autres cercles, difficiles à identifier (en raison probablement des phénomènes de colluvion dus à la pente) : l'un se trouvant au Nord Nord-Ouest, l'autre à l'Est Sud-Est.

#### Historique

Monument découvert par [J. Blot]{.creator} en [septembre 2010]{.date}.

</section>

<section class="monument">

### [Saint-Martin-d'Arrossa]{.spatial} - [Harretchekoborda]{.coverage} 2 - [Cromlech]{.type}

#### Localisation

À une vingtaine de mètres au Sud de *Harretchekoborda 1 - Cromlech*, tangent et à droite de la piste qui monte au Larla.

#### Description

Une quinzaine de pierres délimitent un cercle bien visible érigé sur un sol en légère pente vers le Nord-Ouest.

#### Historique

Monument découvert par [J. Blot]{.creator} en [septembre 2010]{.date}.

</section>

<section class="monument">

### [Saint-Martin-d'Arrossa]{.spatial} - [Harretchekoborda]{.coverage} 3 - [Dolmen]{.type}

#### Localisation

Altitude : 640m.

Il est un peu plus en hauteur et à l'Est de *Harretchekoborda 2 - Cromlech*, à environ 8m au Nord-Est de la piste ascendante vers le Larla.

#### Description

Monument bien dégradé, dont 7 dalles délimitent une [chambre funéraire]{.subject} à grand axe Nord-Est Sud-Ouest, mesurant environ 1,60m de long et 0,60m de large. La dalle Nord-Ouest, la plus importante, est inclinée vers l'extérieur et mesure 0,70m de long et 0,50m de haut.

Il n'y a pas de tumulus visible.

#### Historique

Monument découvert par [J. Blot]{.creator} en [septembre 2010]{.date}.

</section>

<section class="monument">

### [Saint-Martin-d'Arrossa]{.spatial} - [Larla]{.coverage} 1 - [Monolithe]{.type}

#### Localisation

Altitude : 670m.

#### Description

On trouve ce monolithe à proximité immédiate d'un filon rocheux et de nombreux autres blocs de grés. Toutefois il s'en distingue par sa forme et ses dimensions.

Il affecte, en effet, une forme de parallélépipède rectangle allongé, à grand axe Nord-Ouest Sud-Est, mesurant 3,50m de long, 0,80m dans sa plus grande largeur, 0,47m à sa base et 0,35m à sa pointe Nord-Ouest. Son épaisseur varie d'une trentaine à plus d'une quarantaine de centimètres. De nombreuses traces d'épannelage sont visibles tout le long de son bord Nord-Est, ainsi qu'à sa base, au Sud-Est, et, peut-être, à sa pointe.

Ce monolithe, ainsi que *Larla 2 - Monolithe*, pourraient avoir été mis en forme sur place, au niveau du filon d'origine et ne pas avoir été ensuite amené, pour une raison quelconque, à sa destination.

#### Historique

Monument découvert par [J. Blot]{.creator} en [septembre 2010]{.date}.

</section>

<section class="monument">

### [Saint-Martin-d'Arrossa]{.spatial} - [Larla]{.coverage} 2 - [Monolithe]{.type}

#### Localisation

À une dizaine de mètres au Sud Sud-Ouest de *Larla 1 - Monolithe*.

#### Description

Il se présente sous la forme d'une dalle de [grés]{.subject} rectangulaire, à grand axe Nord-Ouest Sud-Est, mesurant 2,50m de long et 1,65m à la base. Son épaisseur n'est que de quelques centimètres. On note enfin des traces d'épannelage très nettes à son extrémité Nord-Ouest.

#### Historique

Monument découvert par [J. Blot]{.creator} en [septembre 2010]{.date}.

</section>

<section class="monument">

### [Saint-Martin-d'Arrossa]{.spatial} - [Pikassary]{.coverage} - [Cromlech]{.type}

#### Localisation

Altitude : 470m.

Il est sur un petit plateau au flanc Nord-Est du mont Larla, à une quinzaine de mètres au Sud-Ouest d'une barre rocheuse.

#### Description

Petit cercle de 3m de diamètre délimité par une quinzaine de pierres, au ras du sol et de taille modeste (0,20m maximum) ; deux autres pierres apparaissent dans le centre légèrement surélevé.

#### Historique

Cercle découvert par le [groupe Hilharriak]{.creator} en [décembre 2012]{.date}.

</section>

<section class="monument">

### [Saint-Martin-d'Arrossa]{.spatial} - [Pikassary]{.coverage} - [Dalle couchée]{.type}

#### Localisation

Elle gît à 12m au Nord-Est de *Pikassary - Cromlech*, tout à côté de l'amas rocheux qui termine le promontoire.

#### Description

Belle dalle de [grés]{.subject} rose en forme de pain de sucre, mesurant 2,50m de long, 0,80m à sa base et 0,15m d'épaisseur. Elle présente des traces d'épannelage sur son bord Nord-Est et à son sommet Sud-Est.

#### Historique

Dalle découverte par [Blot J.]{.creator} en [janvier 2012]{.date}.

</section>

# Saint-Michel

<section class="monument">

### [Saint-Michel]{.spatial} - [Arboze]{.coverage} 1 - [Tumulus]{.type}

#### Localisation

Altitude : 525m.

Il est situé à droite de la petite route qui se détache de la « voie romaine » après le Honto et se dirige, à droite, vers les collines de Arboze (ou Arbosse) et de Coscohandi. On le trouve donc après le *dolmen d'Arboze* décrit par nous en 1972 et 1978 [@blotNouveauxVestigesMegalithiques1972c, p.162; et @blotVestigesProtohistoriquesVoie1978, p.54].

Altitude : 530m.

#### Description

Vaste tumulus circulaire de terre mesurant une trentaine de mètres de diamètre et plus de 2m de haut ; on note les vestiges d'une excavation dans le secteur Sud de sa partie supérieure, de 4m de long et 3m de large, profonde de 0,30m à 0,40m.

À la base du monument, longeant son flanc Nord-Est, on distingue très nettement les traces d'un « cheminement » pour ne pas dire d'une voie, qui se prolonge vers le Nord Nord-Ouest, dans le champ voisin... L'époque de la construction du tumulus, et sa finalité... comme celle de cette voie sont dans l'état actuel impossibles à préciser.

#### Historique

Monument découvert par [Blot J.]{.creator} en [août 2011]{.date}.

</section>

<section class="monument">

### [Saint-Michel]{.spatial} - [Arboze]{.coverage} 2 - [Tumulus]{.type}

#### Localisation

Tangent au Nord-Ouest à *Arboze 1 - Tumulus*.

#### Description

Il est de dimensions plus modestes : 15m de diamètre et 0,80m de haut environ. Il est tout aussi énigmatique que *Arboze 1 - Tumulus*.

#### Historique

Monument découvert par [Blot J.]{.creator} en [août 2011]{.date}.

</section>

<section class="monument">

### [Saint-Michel]{.spatial} - [Arnostégui]{.coverage} (Cabane d') - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 1136m.

Ce tertre est situé juste au-dessus du cours débutant du ruisseau Oillascoa à une quarantaine de mètres au Nord des bâtiments de la cabane d'Arnostégui.

#### Description

Le tertre lui-même, de forme ovale à grand axe Nord-Sud mesure 8m de long, 4m de large, et son sommet arasé ne dépasse que de peu la surface du sol ambiant. Il présente les traces d'une construction ancienne, de forme rectangulaire, actuellement matérialisée par une vingtaine de blocs de [grés]{.subject}, de volume variable profondément enfouis dans le sol, délimitant un espace de 4,30m x 2,50m.

#### Historique

Monument trouvé par [A. Martinez]{.creator} en [juillet 2019]{.date}.

</section>

<section class="monument">

### [Saint-Michel]{.spatial} - [Arnostégui]{.coverage} 4 - [Tumulus]{.type}

#### Localisation

Altitude : 1316m.

Ce monument est situé, en Navarre, mais tangent à la ligne frontière, au Sud de celle-ci, entre les BF 203 et 202 et plus près de cette dernière. Comme toujours dans les cas semblables, nous faisons la description ici de ces monuments.... existant bien avant les tracés frontaliers actuels ! Il complète la liste des 3 monuments déjà relevés dans ce secteur [@boucherNotesProspectionMegalithique1968, p.75].

#### Description

Tumulus terreux, en forme de galette circulaire de 5,50m de diamètre et 0,40m de haut, érigé sur terrain plat. Trois pierres sont visibles dans le secteur Nord-Ouest et deux dans le secteur Sud-Est, sans que l'on puisse vraiment parler de péristalithe.

#### Historique

Monument découvert par [F. Meyrat]{.creator} en [août 2018]{.date}.

</section>

<section class="monument">

### [Saint-Michel]{.spatial} - [Arnostégui]{.coverage} 5 - [Tumulus]{.type} ([?]{.douteux})

#### Localisation

En territoire Navarrais.

Altitude : 1321m.

Monument douteux, situé à 60m à l'Est Sud-Est de *Arnostégui 4 - Tumulus*, au sommet d'une petite éminence, et à une cinquantaine de mètres au Sud de la ligne frontière.

#### Description

Monument en forme de galette très aplatie de 4,50m de diamètre et 0,10m de haut, dont la partie centrale présente une légère dépression. Deux pierres sont visibles en secteur Sud.

#### Historique

Monument découvert par [F. Meyrat]{.creator} en [août 2018]{.date}.

</section>

<section class="monument">

### [Saint-Michel]{.spatial} - [Atheketa]{.coverage} - [Tertres d'habitat]{.type}

Nous citons ici ce groupe de 3 tertres d'habitat, dont la localisation peu précise dans le [@blotEtatConnaissancesArcheologiques1978], en rendait la découverte sur le terrain très difficile. Par contre la description est toujours valable.

</section>

<section class="monument">

### [Saint-Michel]{.spatial} - [Azqueta]{.coverage} - [Tertres d'habitat]{.type}

#### Localisation

Altitude : 937m.

Situés sur une éminence qui domine, à l'Ouest, la piste se rendant au cayolar Asketa.

#### Description

Ensemble de 3 tertres tangents édifiés en rupture de pente, alignés selon un axe Nord-Sud, constitués de terre, et mesurant chacun environ 12m à 14m x 9m et 0,30m de haut.

#### Historique

Tertres découverts par [Blot J.]{.creator} en [septembre 1980]{.date}.

</section>

<section class="monument">

### [Saint-Michel]{.spatial} - [Azqueta]{.coverage} 4 - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 903 m.

Il est tangent au côté Nord de la route venant d'Orisson à sa jonction avec la piste qui se rend au cayolar d'Asketa.

#### Description

Tertre « classique », terreux, demi-circulaire, en rupture de pente, mesurant 8m de diamètre et 0,50m de haut environ.

#### Historique

Tertre découvert par [C. Blot]{.creator} en [novembre 2018]{.date}.

</section>

<section class="monument">

### [Saint-Michel]{.spatial} - [Azqueta]{.coverage} 1 - [Tumulus]{.type}

#### Localisation

Altitude : 952m.

Il est situé en terrain plat, à une trentaine de mètres au Sud-Ouest de *Azqueta - Tertres d'habitat*.

#### Description

Tumulus terreux, circulaire, aplati, mesurant 10m x 10m et 0,20m à 0,30m de haut. Son centre présente une vaste dépression, mais peu profonde, s'étendant vers la partie Est du monument.

#### Historique

Tumulus découvert par [Meyrat F.]{.creator} en [octobre 2018]{.date}.

</section>

<section class="monument">

### [Saint-Michel]{.spatial} - [Azqueta]{.coverage} 2 - [Tumulus]{.type}

#### Localisation

Altitude : 956m.

Il est situé à environ une trentaine de mètres au Sud-Ouest de *Azqueta 1 - Tumulus*, légèrement plus en altitude.

#### Description

Tumulus de terre, édifié sur un terrain en très légère pente, de forme plutôt ovalaire, aplati, mesurant 10m x 8m et 0,30m de haut.

#### Historique

Tumulus découvert par [Meyrat F.]{.creator} en [octobre 2018]{.date}.

</section>

<section class="monument">

### [Saint-Michel]{.spatial} - [Azqueta (cayolar)]{.coverage} - [Tertre d'habitat]{.type} ([?]{.douteux})

#### Localisation

Altitude : 916m.

#### Description

Ces 3 éléments sont situés à l'arrière des cayolars d'Azketa, à flanc de la colline qui les surmonte à l'Ouest. Ils mesurent entre 12m et 14m x 9m. Il nous semble qu'il s'agit plus d'amoncellements de déblais consécutifs au nettoyage d'aires de traite que de véritables tertres d'habitat.

#### Historique

Eléments signalés par [Meyrat F.]{.creator} en [octobre 2018]{.date}.

</section>

<section class="monument">

### [Saint-Michel]{.spatial} - [Azqueta]{.coverage} - [Faux tumulus]{.type}

#### Localisation

Altitude : 943m.

Au sommet d'une petite éminence dominant au Sud la jonction de la piste venant du cayolar Asketa avec la route venue d'Orisson.

#### Description

Tumulus circulaire de terre et pierres de 3,50m de diamètre et 0,40m de haut. Cependant nous pensons qu'il s'agit très probablement d'un filon rocheux naturel, modelé par l'érosion des intempéries.

#### Historique

Elément découvert par [Blot J.]{.creator} en [novembre 2018]{.date}.

</section>

<section class="monument">

### [Saint-Michel]{.spatial} - [Elhursaro]{.coverage} Nord - [Tertres d'habitat]{.type}

aussi appelé les *TH de Zerkupé*.

[Nous]{.creator} avions publié en [1972]{.date}, dans le [@blotNouveauxVestigesMegalithiques1972c, p.178-180], les 6 tertres d'habitat d'Elhursaro (ou Zerkupé), sous la rubrique *Les tumulus d'Oriune (Orion)*.

À notre passage en Janvier 2018, nous avons noté la destruction du *tertre n°1* (17m x 10m et 1m de haut), lors de l'édification d'un parc à brebis en parpaings.

</section>

<section class="monument">

### [Saint-Michel]{.spatial} - [Elhursaro]{.coverage} Sud - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 945m.

Ce tertre se trouve sur un petit replat à 60m au Nord-Ouest des deux cabanes dElhursaro les plus au Sud et à l'Est du groupe ; il est à 3m au Sud-Ouest d'un pointement rocheux calcaire qui entoure sa moitié Est.

#### Description

Très beau tertre, de forme ovale, de 16m de long, 8m de large et 1,70m de haut, érigé sur un sol plat. Il n'y a aucune pierre apparente.

#### Historique

Tertre trouvé par [Blot J.]{.creator} en [février 2018]{.date}.

</section>

<section class="monument">

### [Saint-Michel]{.spatial} - [Harrixuri]{.coverage} - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 940m.

Il est sur un petit replat, à une trentaine de mètres au Nord de la route qui va au col d'Irey.

#### Description

Petit tertre asymétrique, très érodé semble-t-il, de 8m de diamètre environ et 0,35m de haut.

#### Historique

Monument découvert par [Blot J.]{.creator} en [novembre 2011]{.date}.

</section>

<section class="monument">

### [Saint-Michel]{.spatial} - [Harrietta]{.coverage} - [Pierre plantée]{.type}

#### Localisation

Altitude : 900m.

Elle est plantée au milieu du petit col où passe la piste pastorale, et à quelques mètres au Nord du *cromlech Sohandi 2* ayant fait l'objet d'une fouille en 1984 ( Blot J.).

#### Description

Bloc de calcaire, de forme parallélépipédique, orienté selon un axe Sud-Est Nord-Ouest, mesurant 0,98m de haut, 1,40m de long et 0,40m de large à la base ; il semble que l'on puisse parler de pierres de calage du côté Est.

#### Historique

Pierre découverte par [Blot J.]{.creator} en [juillet 2011]{.date}.

</section>

<section class="monument">

### [Saint-Michel]{.spatial} - [Harrietta]{.coverage} - [Tumulus]{.type} ([?]{.douteux})

#### Localisation

Altitude : 903m.

#### Description

Monument douteux se présentant sous la forme d'un tumulus d'environ 6,50m de diamètre et 0,40m de haut formé de terre et de pierres. Une dizaine de celles-ci, de volume parfois important, paraissent disposées de façon circulaire... Monument ou mouvement naturel du terrain ?

#### Historique

Elément découvert par [Blot J.]{.creator} en [août 2011]{.date}.

</section>

<section class="monument">

### [Saint-Michel]{.spatial} - [Harrietta]{.coverage} - [Tertres d'habitat]{.type}

#### Localisation

Altitude : 903m.

On note un ensemble de 4 tertres d'habitat se succédant de haut en bas à partir de l'éminence qui domine à quelques dizaines de mètres à l'Ouest Sud-Ouest la pierre plantée du col.

#### Description

-   *Le 1^er^ tertre* : Au sommet de l'éminence. Le plus imposant : allongé suivant un grand axe Nord-Ouest Sud-Est, il mesure 19m, et dans sa partie déclive, vers le Nord-Est, il a environ 3m de haut.

-   *Le 2^ème^ tertre* fait suite au précédent et lui est tangent. On peut lui attribuer 9m de long et 1m de haut à son versant Nord-Est.

-   *Le 3^ème^ tertre*, tangent au précédent, mesure 11m de long et 80 de haut.

-   *Le 4^ème^ tertre* a les mêmes caractéristiques que le précédent. Il se trouve à 10m au Sud Sud-Est du *cromlech Sohandi 2*, fouillé en 1984

#### Historique

Tertres découverts en [août 2011]{.date} par [J. Blot]{.creator}.

</section>

<section class="monument">

### [Saint-Michel]{.spatial} - [Irei (Col d')]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 751m.

Il est situé sur le plat du col d'Irei, à l'extrémité Nord de celui-ci, à côté de nombreux vestiges de travaux défensifs anciens ; toutefois il s'en distingue facilement et la confusion n'est pas possible. Il est à 4m au Sud Sud-Ouest d'une aubépine et à 21m à l'Ouest d'un pointement rocheux.

#### Description

Tumulus terreux circulaire, en forme de dôme, mesurant 4,10m de diamètre et 0,40m de haut.

#### Historique

Tumulus découvert en [février 2018]{.date} par [C. Blot]{.creator}.

</section>

<section class="monument">

### [Saint-Michel]{.spatial} - [Itsasegui]{.coverage} Sud - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 1070m.

Situé à mi-pente du versant Nord du mont Itsaségui, au bord Ouest de la naissance d'un ravin.

#### Description

Tertre de terre, très net, de 15m de diamètre et de plus de 2m de haut à son versant Nord.

#### Historique

Tertre découvert par [Blot J.]{.creator} en [août 2011]{.date}.

</section>

<section class="monument">

### [Saint-Michel]{.spatial} - [Itsasegui]{.coverage} 2 - [Tumulus]{.type}

#### Localisation

Altitude : 1161m.

Il est situé à l'extrémité Sud-Ouest du replat sommital.

#### Description

Tumulus de terre et de pierres de 5m de diamètre et 0,30m à 0,40m de haut environ. Les pierres sont plus abondantes dans la moitié Est et paraissent disposées de façon circulaire.

#### Historique

Tumulus découvert par [Blot J.]{.creator} en [2011]{.date}.

</section>

<section class="monument">

### [Saint-Michel]{.spatial} - [Itsasegui]{.coverage} Est - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 812m.

Ce monument se trouve à environ 120m à l'Est du sommet de l'Itsaségui, et domine l'horizon.

#### Description

Érigé sur un terrain en pente douce vers l'Est, ce volumineux tertre, terreux, de forme ovale, mesure 14m dans son grand axe (Nord-Sud), 12m de large et environ 1m de haut.

#### Historique

Tertre découvert par [A. Martinez]{.creator} en [juillet 2019]{.date}.

</section>

<section class="monument">

### [Saint-Michel]{.spatial} - [Jatsagune]{.coverage} 1 - [Cromlech]{.type}

#### Localisation

Altitude : 1224m.

Il est à 3m à l'Ouest du tracé du chemin de Compostelle.

#### Description

Cercle de 4,30m de diamètre, délimité par une douzaine de petites pierres, peu visibles ; on note une très légère dépression centrale.

#### Historique

Cercle découvert par le [groupe Hillariak]{.creator} en [juillet 2013]{.date}.

</section>

<section class="monument">

### [Saint-Michel]{.spatial} - [Jatsagune]{.coverage} 2 - [Cromlech]{.type}

#### Localisation

Altitude : 1226m.

Il est à 20m au Nord Nord-Est de Jatsagune 1 - Cromlech, et tangent, à l'Ouest au chemin de Compostelle.

#### Description

Petit cercle de 2m de diamètre délimité par une douzaine de pierres au ras du sol, mais bien visibles.

#### Historique

Monument découvert par [Blot J.]{.creator} en [septembre 2014]{.date}.

</section>

<section class="monument">

### [Saint-Michel]{.spatial} - [Jatsagune]{.coverage} 1 - [Tumulus]{.type}

#### Localisation

Altitude : 1234m.

Ce groupe de 3 petits tumuli tangents est à 20m à l'Ouest Sud-Ouest du poste de chasse érigé sur un tertre d'habitat *Urdanarre - Tertre d'habitat* et à 25m à l'Ouest de la « route des cîmes ».

#### Description

Petit tumulus de 3,50m de diamètre et 0,25m de haut, sans pierres apparentes.

#### Historique

Cet ensemble de 3 tumuli a été trouvé en [septembre 2014]{.date} par [F. Meyrat]{.creator}.

</section>

<section class="monument">

### [Saint-Michel]{.spatial} - [Jatsagune]{.coverage} 2 - [Tumulus]{.type}

#### Localisation

Tangent à l'Ouest de *Jatsagune 1 - Tumulus*.

#### Description

Tumulus de 3m de diamètre et 0,30m de haut, à la surface duquel apparaissent 8 pierres, au ras du sol, ne formant pas un péristalithe.

</section>

<section class="monument">

### [Saint-Michel]{.spatial} - [Jatsagune]{.coverage} 3 - [Tumulus]{.type}

#### Localisation

Tangent à l'Ouest Nord-Ouest de *Jatsagune 2 - Tumulus*.

#### Description

Tumulus de 2,50m de diamètre et 0,10 à 0,15m de haut, avec 4 pierres visibles en surface, au ras du sol.

</section>

<section class="monument">

### [Saint-Michel]{.spatial} - [Landarre]{.coverage} - [Tertre d'habitat]{.type} ([?]{.douteux})

#### Localisation

Altitude : 1082m.

Situé sur un petit replat du versant Nord du mont Itsaségui, ce « tertre » est situé à une dizaine de mètres plus en altitude que le tertre que nous avons décrit et publié en 2012 dans [@blotInventaireMonumentsProtohistoriques2012, p.19] sous le nom de *TH Itsaségui Sud* (altitude : 1070m), par rapport à d'autres tertres d'habitat identifiés plus bas au niveau du col et dénommés *Itsaégui Nord*.

#### Description

Il se présente sous la forme d'un grand tertre oblongue, à grand axe Nord-Ouest Sud-Est, mesurant 15m x 8m et d'une hauteur approximative de 1,20m. On note à ses deux extrémités deux reliefs qui en sont comme les prolongements : au Nord-Ouest une éminence de forme circulaire de 8m x 8m, et au Sud-Est une autre de 12m x 6m. À l'arrière de ces structures, on distingue nettement les traces d'excavation du sol pour les réaliser ; il pourrait s'agir de « banquettes de tir »...

#### Historique

Structure identifiée par [F. Meyrat]{.creator} en [juillet 2019]{.date}.

</section>

<section class="monument">

### [Saint-Michel]{.spatial} - [Landarre]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 933m.

Situé sur un petit replat d'un terrain en forte pente vers l'Est. Il est à 130m au Sud Sud-Est - et plus bas - que les ruines d'une cabane en pierres.

#### Description

Tumulus circulaire, en forme de galette aplatie, mesurant 6,50m de diamètre. Ce tumulus mixte, constitué de terre et de nombreux fragments de schiste, répartis sans ordre apparent, présente en son milieu une dépression circulaire - très probablement due à une fouille ancienne - mesurant 1,70m de diamètre, 0,15m de profondeur, et légèrement décentrée vers l'Ouest.

#### Historique

Monument découvert par [F. Meyrat]{.creator} en [juillet 2019]{.date}.

</section>

<section class="monument">

### [Saint-Michel]{.spatial} - [Leizahandi]{.coverage} - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 1102m.

Visible à droite de la route qui monte au cayolar Leizahandi.

#### Description

Tertre de terre mesurant 14m x 10m et 0,70m de haut.

#### Historique

Tertre découvert par [Meyrat F.]{.creator} en [octobre 2013]{.date}.

</section>

<section class="monument">

### [Saint-Michel]{.spatial} - [Leizahandi]{.coverage} - [Borne pastorale]{.type}

#### Localisation

Altitude : 1157m.

Entre la BF 209 (à l'Est) et la BF 208 (60m à l'Ouest).

#### Description

Petite borne de [calcaire]{.subject} blanc, allongée selon un axe Nord-Ouest Sud-Est, mesurant 1m de long, 0,40m de large à sa base rectiligne et 0,27m de large à son sommet arrondi.

#### Historique

Borne signalée par [Dupré E.]{.creator} en [octobre 2013]{.date}.

</section>

<section class="monument">

### [Saint-Michel]{.spatial} - [Minassaro]{.coverage} - [Tertres d'habitat]{.type}

#### Localisation

Ces 5 tertres sont répartis sur un vaste pâturage à gauche de la route qui se rend au col d'Arnostégui, au niveau où se détache la bretelle menant au cayolar Minassaro.

#### Description

Plus de 5 tertres mesurant en moyenne 5m à 6m x 5m et 0,60m de haut.

(Il existe la possibilité que certains reliefs, (en plus des 5 cités), soient d'anciens postes de tir).

#### Historique

Tertres découverts par [Blot J.]{.creator} en [octobre 2013]{.date}.

</section>

<section class="monument">

### [Saint-Michel]{.spatial} - [Orizune]{.coverage} - [Tumulus-Cromlech]{.type}

#### Localisation

Vers l'extrémité Ouest du plateau sommital, à 1060m d'altitude, et à 30m à l'Est d'un gros bloc rocheux.

Altitude : 1060m.

#### Description

Un discret péristalithe formé d'une dizaine de pierres, au ras du sol, entoure un tumulus terreux d'une dizaine de mètres de diamètre, atteignant 0,40m de haut environ.

#### Historique

Ce monument a été découvert par l'[association Hilharriak]{.creator} (Saint Sébastien) en [1999]{.date}.

</section>

<section class="monument">

### [Saint-Michel]{.spatial} - [Orizune]{.coverage} 2 - [Tumulus]{.type}

Rappelons que nous avons déjà décrit [@blotNouveauxVestigesMegalithiques1972c, p.166], un premier tumulus, à 1030m d'altitude, sur un replat dominant le col de Landarre, à une trentaine de mètres de celui-ci.

#### Localisation

Carte 1346 Est Saint-Jean-Pied-de-Port.

Altitude :1062m.

Ce monument est situé à environ 30mètres au Sud Sud-Est de la borne sommitale du mont Orizune, sur un terrain en légère pente vers le Sud-Est.

#### Description

Tumulus d'environ 4,70m de diamètre et 0,30m de haut, constitué de terre et de nombreuses dalles de [grés]{.subject}. Il ne semble pas possible de préciser l'existence d'un péristalithe.

#### Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2009]{.date}.

</section>

<section class="monument">

### [Saint-Michel]{.spatial} - [Oilaskoa]{.coverage} 1 - [Cromlech]{.type}

#### Localisation

Altitude : 1068m.

Ce monument, ainsi que *Oilaskoa 2 - Cromlech*, est situé sur un replat herbeux à environ 14m au Sud de la piste principale qui sillonne cette crête d'Ouest en Est ; il est aussi à 12m à l'Est de la piste qui se rend au cayolar Oilaskoa.

#### Description

Ce cromlech légèrement en relief peut être qualifié de légèrement tumulaire ou surélevé. Il mesure environ 4,30m de diamètre, ce dernier étant difficile à évaluer étant donnée la disposition un peu irrégulière des pierres témoins. La périphérie est cependant balisée par environ 25 éléments pierreux bien enfoncés dans le sol. On note des blocs assez volumineux (au Nord par exemple : 0,63m x 0,50m et 0,40m de haut, et au Sud un bloc de 0,69m x 0,42m et 0,25m de haut) ; certains paraissent avoir été déterrés et être devenus mobiles. Au centre sont visibles 5 pierres ou petites dalles (éléments d'une ciste ?).

#### Historique

Monument découvert par [J. Blot]{.creator} en [mars 2018]{.date}.

</section>

<section class="monument">

### [Saint-Michel]{.spatial} - [Oilaskoa]{.coverage} 2 - [Cromlech]{.type}

#### Localisation

Ce monument est situé à 10m au Sud de *Oilaskoa 1 - Cromlech*.

#### Description

On ne voit, à l'heure actuelle que la moitié Est de ce monument, qui mesure 2m de diamètre et qui est balisée par 11 pierres de dimensions très variables. Elles sont plus volumineuses et visibles à l'Est, où elles sont souvent tangentes, alors qu'elles sont plus espacées aux extrémités du demi-cercle. Deux d'entre elles, au Sud, au ras du sol, atteignent 0,83m et 0,84m de long. Deux petites pierres apparaissent au centre.

#### Historique

Monument découvert par [J. Blot]{.creator} en [mars 2018]{.date}.

</section>

<section class="monument">

### [Saint-Michel]{.spatial} - [Oilaskoa]{.coverage} Nord - [Tertre d'habitat]{.type}

#### Localisation

Situé au pied des premiers rochers d'Athéketa.

Altitude : 1048m.

#### Description

Tertre terreux asymétrique, allongé dans le sens Nord-Ouest Sud-Est, mesurant 9m de long, 5 de large environ et 1m de haut environ.

#### Historique

[J. Blot]{.creator} en [mars 2018]{.date} (Dans notre publication des TH d'Oilaskoa dans le [@blotVestigesProtohistoriquesVoie1978], nous citions 3 tertres d'habitat à l'Est Nord-Est des 15 décrits. Il semble bien que seul celui décrit ci-dessus soit valable, les 2 autres paraissant des formations naturelles).

</section>

<section class="monument">

### [Saint-Michel]{.spatial} - [Oilaskoa]{.coverage} 3 - [Cromlech]{.type}

#### Localisation

Il est tangent au Sud-Ouest au *cromlech Oilaskoa n°1* décrit dans le Tome 10 [@blotInventaireMonumentsProtohistoriques2018], de notre *Inventaire des Monuments protohistoriques en Pays Basque*. Ce C1 est situé sur un replat herbeux, à 14m au Sud de la piste principale qui sillonne la crête d'Oilaskoa d'Ouest en Est, et il est à 12m à l'Est de la piste qui se rend au cayolar d'Oilaskoa.

Altitude : 1068m.

#### Description

Huit blocs pierreux, de même nature que ceux formant le cromlech n°1 (situé à 0,30m au Nord-Est), délimitent un cercle de 1,40m de diamètre ; les dimensions sont variables, le bloc le plus important, à l'Ouest, mesurant 0,20m x 0,30m et 0,25m de haut.

#### Historique

Monument découvert par [C. Blot]{.creator} en [juillet 2018]{.date}.

On notera que depuis sa découverte, le *cromlech n°2* a été en partie recouvert par un dépôt intempestif de gravats par un engin de chantier...

</section>

<section class="monument">

### [Saint-Michel]{.spatial} - [Oillaskoa]{.coverage} (ruisseau) - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 1088m.

Ce tertre est situé sur un terrain en pente orientée vers l'Est, à environ 100m à l'Ouest du ruisseau Oilaskoa qu'il domine et à 300m au Nord du tertre dit *Cabane d'Arnostégui*.

#### Description

Important tertre terreux, de forme ovale à grand axe Nord-Sud, mesurant 10m x 8m et 0,50m de haut.

#### Historique

Tertre découvert par [A. Martinez]{.creator} en [juillet 2019]{.date}.

</section>

<section class="monument">

### [Saint-Michel]{.spatial} - [Orizune]{.coverage} Est - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 917m.

Tertre situé au flanc Est du mont Orizune (Orisson), sur un très léger replat.

#### Description

Tertre mixte de terre, dans laquelle apparaissent quelques blocs de [schiste]{.subject}, dans les secteurs Est, Nord et au centre. De forme asymétrique (sur terrain en légère pente vers l'Est), et ovale, il mesure 6m x 5m et 0,15m de haut.

#### Historique

Tertre découvert par [F. Meyrat]{.creator} en [juillet 2019]{.date}.

</section>

<section class="monument">

### [Saint-Michel]{.spatial} - [Pagoberri]{.coverage} - [cromlechs]{.type} T ( ?)

#### Localisation

Altitude : 1055m.

À une cinquantaine de mètres au Sud et en contrebas de la piste qui se rend au cayolar de Pagoberri : sur terrain en légère pente vers le Sud.

#### Description

Un ensemble de blocs en [calcaire]{.subject} blanc, de taille variable, forme une figure géométrique grossièrement trapézoïdale dans laquelle il serait peut-être possible de distinguer 3 « cercles de pierres » de diamètre allant de 3m à 3,50m .... Très douteux.

#### Historique

Ensemble découvert par [Meyrat F.]{.creator} en [octobre 2013]{.date}.

</section>

<section class="monument">

### [Saint-Michel]{.spatial} - [Pagoberri]{.coverage} - [Monolithe]{.type}

#### Localisation

Altitude : 1043m.

Plus à l'Est que les éléments précédents, ce monolithe est à 5m à l'Est des ruines d'un cayolar, et à 14m au Nord de la piste pastorale, mais toujours en contre-bas de la piste qui mène à Pagoberri. Il est très proche du tracé de la ligne frontière...

#### Description

Bloc de [calcaire]{.subject} de forme rectangulaire, couché au sol, ne présentant pas de trace d'épannelage (peut-être avait-il la forme désirée d'emblée). Il mesure 2,70m de long, 0,58m de large à sa base Nord, arrondie, 0,73m à son extrémité Sud, plus pointue et 0,44m d'épaisseur en moyenne. Il présente de fortes traces de dissolution du calcaire à sa surface supérieure.

#### Historique

Monolithe signalé par [E. Dupré]{.creator} en [octobre 2013]{.date}.

</section>

<section class="monument">

### [Saint-Michel]{.spatial} - [Pagoberri]{.coverage} 1 - [Tumulus]{.type}

#### Localisation

Altitude : 1107m.

Dans un petit cirque rocheux à 100m au Nord du cayolar Pagoberri.

#### Description

Petit tertre pierreux de 2,70m de diamètre et 0,50m de haut environ formé d'un peu plus d'une vingtaine de blocs de [calcaire]{.subject} blanc, non jointifs, mais disposés de façon circulaire. Pas de péristalithe visible.

#### Historique

Monument découvert par [Meyrat F.]{.creator} en [octobre 2013]{.date}.

</section>

<section class="monument">

### [Saint-Michel]{.spatial} - [Pagoberri]{.coverage} 2 - [Tumulus]{.type}

#### Localisation

Altitude : 1043m.

Il est à 20m à l'Est de *Pagoberri - Monolithe*.

#### Description

Tumulus pierreux circulaire, parfaitement visible, de 6m de diamètre et 0,40m de haut environ. Il est constitué de blocs en [calcaire]{.subject} blanc, de volumes variables, disposés sans ordre apparent ; il ne semble pas y avoir de péristalithe vrai.

#### Historique

Monument découvert par [Meyrat F.]{.creator} en [octobre 2013]{.date}.

</section>

<section class="monument">

### [Saint-Michel]{.spatial} - [Pagoberri]{.coverage} - [Tertres d'habitat]{.type}

Nous publions ici cet ensemble de 10 tertres d'habitat dont certains ont été trouvés par [Blot J.]{.creator} et par [Meyrat F.]{.creator} en [octobre 2013]{.date}.

#### Localisation

-   *TH 1* : Altitude : 1000m. À l'amorce d'un vaste pâturage qui s'étend vers l'Est, entre deux barres rocheuse. Tertre de 10m x 10m et 0,40m de haut.

-   *TH 2* : Situé à 10m au Nord-Est du précédent (altitude : 1002m). Adossé à la barrière rocheuse, il mesure 15m x 10m et 1,50m de haut.

-   *TH 3* : Altitude : 1002m. Situé à 15m à l'Est du précédent, et adossé à la barrière rocheuse. Tertre de 18m x 12m et 2m de haut.

-   *TH 4* : Altitude : 997m. Situé à 40m au Sud-Est du précédent. Mesure 20m x 10m et 1m de haut. Présente une excavation à son sommet.

-   *TH 5* : À 5m au Sud-Est du précédent. Mesure 7m x 5m et 0,70m de haut

-   *TH 6* : Altitude : 991m. À 60m à l'Est Sud-Est du précédent. Mesure 12m x 6m et 0,40m de haut.

-   *TH 7* : Altitude : 990m. À 40m à l'Est du précédent. Mesure 8m x 5m et 0,40m de haut.

-   *TH 8* : Altitude : 984m. À 40m à l'Est du précédent. Mesure 14m x 6m et 0,80m de haut.

-   *TH 9* : Altitude : 992m. À 140m environ au Sud-Ouest du tertre n°7. Mesure 20m x 10m et 0,90m de haut.

-   *TH 10* : Tangent à l'Ouest du précédent. Mesure 8m x 6m et 0,40m de haut

</section>

<section class="monument">

### [Saint-Michel]{.spatial} - [Sohandi]{.coverage} - [Cromlechs]{.type}

Nous avons publié les 6 cromlechs suivants (*C1*, *C7*, *C8*, *C9*, *C10*, *C11*), trouvés par [nous]{.creator} en [1977]{.date}, dans le [@blotVestigesProtohistoriquesVoie1978]. Ces monuments étaient déjà peu visibles à l'époque, les témoins n'émergeant que peu (ou pas du tout), du sol. Depuis 40 ans, les sédiments et la végétation se sont accumulés, au point que la plupart d'entre eux n'étaient plus discernables en 2018. Il a donc été nécessaire de procéder à un important travail de nettoyage - grâce à la participation très efficace de notre ami Francis Meyrat que nous remercions ici vivement - qui a permis de préciser la structure de ces différents monuments.

-   *Sohandi 1 - Cromlech*
    -   Localisation :
        
        Il est situé à environ 175m au Sud-Est de la vieille cabane d'Harrieta et à 1m à l'Est de la piste venant de celle-ci.

        Altitude : 872m.

    -   Description : 19 petits blocs de [calcaire]{.subject} de très modeste volume, délimitent un cercle (assez peu régulier) de 6,50m de diamètre. La pierre la plus volumineuse se trouve au Nord et mesure 0,75m x 0,56m et 0,20m de haut ; une autre, à l'Ouest, mesure 0,70m x 0,40m et 0,10m de haut. Les autres ont entre 0,30m et 0,10m d'implantation visible au sol...
-   *Sohandi 7 - Cromlech*
    -   Localisation : Situé à 29m au Sud Sud-Ouest de *C1*.
    -   Description : 6 pierres délimitent un cercle de 6m de diamètre. Le témoin le plus volumineux se trouve au Nord, et mesure 1,13m x 0,25m et 0,20m de haut ; une seconde, à l'Est, au ras du sol, mesure 0,90m x 0,30m. Les autres sont de volume beaucoup plus discret et peu visibles.
-   *Sohandi 8 - Cromlech*
    -   Localisation : Il est situé à 13m au Sud-Est de *C7*.

    -   Description : 
        
        Neufs petits blocs calcaires délimitent un cercle de 5,80m de diamètre.

        Une aubépine a dû être coupée, dont la souche se voit dans le quart Ouest du monument.

        Trois pierres atteignent 0,40m de long et 0,10m de haut, les autres sont bien plus petites.
-   *Sohandi 9 - Cromlech*
    -   Localisation : Il est à 6m au Sud Sud-Ouest de *C8*.
    -   Description : Ce cromlech de 6m de diamètre est délimité par 10 petits blocs [calcaire]{.subject}, dont 2, au Sud Sud-Ouest sont en commun avec *C10* avec lequel il est donc tangent. La majorité de ces témoins est groupée dans la moitié Sud du monument, et n'atteignent au mieux que 0,40m x 0,25m et 0,12m à 0,20m de haut ; par contre, au Nord Nord-Est, on note une pierre plus importante atteignant 0,90m x 0,12m et 0,24m de haut. Une aubépine a poussé dans sa moitié Sud.
-   *Sohandi 10 - Cromlech*
    -   Localisation : Ce cercle est tangent au Sud Sud-Ouest du précédent et possède 2 témoins en commun avec lui.

    -   Description : Il mesure 7m de diamètre et possède 8 pierres à peu près également réparties à sa périphérie.

        On notera aussi qu'au Sud, ce monument possède 2 autres pierres en commun avec *C 11*, avec lequel il est donc aussi tangent. Ces 2 témoins mesurent respectivement 0,53m x 0,05m et 0,14m de haut, et 0,68m x 0,09m et 0,10m de haut.
-   *Sohandi 11 - Cromlech*
    -   Localisation : Il est tangent au Sud du précédent, avec lequel il possède 2 pierres en commun.
    -   Description : Ce monument mesure 4,60m de diamètre, délimité par 7 pierres de volume variable (en moyenne 0,30m x 0,30m, plus ou moins au ras du sol) ; une exception toutefois, au Sud Sud-Est, une pierre plus allongée, au ras du sol, mais mesurant 0,90m x 40m. On peut noter au Sud Sud-Ouest une dalle plantée.

</section>

<section class="monument">

### [Saint-Michel]{.spatial} - [Sohandi]{.coverage} - [Dalle plantée]{.type}

#### Localisation

Dalle plantée à 7m au Sud Sud-Ouest de *Sohandi 11 - Cromlech*.

#### Description

Épaisse dalle de [grés]{.subject} plantée dans le sol, mesurant 0,75m à sa base et 0,80m à son sommet, ainsi que 0,16m d'épaisseur en moyenne. Elle est orientée selon un axe Sud-Ouest Nord-Est et semble présenter de discrets signes d'épannelage sur toute da périphérie (?). La signification de cette dalle, dont la présence ne paraît pas fortuite, nous échappe totalement : repère pastoral ou municipal moderne ? repère en rapport avec la nécropole à incinération ?

#### Historique

Dalle découverte par [F. Meyrat]{.creator} en [mars 2018]{.date}.

</section>

<section class="monument">

### [Saint-Michel]{.spatial} - [Urdanazburu]{.coverage} - [Tertres d'habitats]{.type}

#### Localisation

Altitude : 1100m.

Ces tertres sont situés à une trentaine de mètres l'Est de la route, sur un terrain en très légère pente vers le Sud, de suite après la très importante formation rocheuse dénommée *Urdanazburu*.

#### Description

-   *Tertre 1* : ce tertre de volume très modeste, de forme ovale, allongé selon un axe Nord-Sud, mesure 4m x 2,60m et 0,50m de haut.

Il est à noter qu'il est accompagné de deux autres formations très proches :

-   *Tertre 2 (?)* : situé à 2,20m au Nord Nord-Ouest du précédent, lui aussi de forme ovale, il mesure 1,60m x 1m et 0,20m de haut. Est-ce un tertre ?

-   *Tumulus 3 (?)* : situé à 12m à l'Ouest du précédent, et à 18m à l'Est de la route, se présente sous la forme d'un «tumulus» (?) arrondi, terreux, mesurant 3m de diamètre et 0,20m de haut, au centre duquel se détache un bloc de grès mesurant environ 0,40m x 0,35m.

#### Historique

Le *Tertre 1* a été découvert par [A. Martinez]{.creator} en juillet 2019 ; le *Tertre 2 (?)* et le *Tumulus 3 (?)* l'ont été par [J. Blot]{.creator} en [juillet 2019]{.date}.

</section>

<section class="monument">

### [Saint-Michel]{.spatial} - [Urdanarre]{.coverage} - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 1236m.

Il est sur la pente Sud du mont Urdanarre. Un poste de tir à la palombe en ciment a été construit à l'intérieur.

#### Description

Tertre dissymétrique, en terre, mesurant 15m de large et 12m dans le sens de la pente et 0,45m de haut ; une large excavation de 3,50m de diamètre a été aménagée au centre pour le poste de tir.

#### Historique

Monument découvert par le [groupe Hillariak]{.creator} en [juillet 2013]{.date}.

</section>

<section class="monument">

### [Saint-Michel]{.spatial} - [Urdanarre]{.coverage} Nord 3 - [Cromlech]{.type}

#### Localisation

Altitude : 1225m.

Ce cercle, situé au sommet d'une petite éminence, est à 14m au Sud Sud-Est du *Tumulus-cromlech 2*.

#### Description

Il semble bien qu'on puisse distinguer un cercle périphérique, d'environ 6m de diamètre, constitué d'une trentaine de dalles ou de blocs de [schiste]{.subject} de volume modeste, visibles au ras du sol, et au centre un deuxième cercle de 1,90m de diamètre, constitué d'une douzaine du même type de blocs de [schiste]{.subject}.

#### Historique

Monument identifié par [Blot J.]{.creator} en [août 2018]{.date}.

</section>

<section class="monument">

### [Saint-Michel]{.spatial} - [Urkulu]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 1303m.

Ce tumulus est situé sur terrain plat, au milieu d'un petit ensellement formant limite entre 2 grandes dolines situées au Sud Sud-Est des cabanes d'Urkulu.

#### Description

Tumulus terreux de 12m de diamètre environ, et 0,50m de haut. Pas d'éléments pierreux visibles.

#### Historique

Nous avions publié dans le [@blotVestigesProtohistoriquesVoie1978, p.75], 5 tertres d'habitat, dont 4 au bord même de la grande doline la plus proche des cabanes, (mais à une centaine de mètres au Sud Sud-Est de ces dernières, et un cinquième tertre situé au Sud Sud-Est, à environ 90m des précédents tertres. En fait et comme le notait [F. Meyrat]{.creator} en [mars 2018]{.date}, ce cinquième tertre est en vérité plutôt un tumulus.

</section>

<section class="monument">

### [Saint-Michel]{.spatial} - [Urkulu]{.coverage} BF 206 - [Tumulus]{.type}

#### Localisation

Altitude : 1412m.

Monument situé à 24m au Sud Sud-Est de la BF 206 et à 3m au Nord-Ouest d'une doline.

#### Description

Petit tumulus en forme de galette aplatie, mesurant 0,10m de haut et 2,20m de diamètre ; à sa surface apparaissent une dizaine de petits blocs de [calcaire]{.subject} blanc, prédominant dans le secteur Nord-Ouest du monument, sans que l'on puisse parler semble-t-il d'un tumulus cromlech, mais plutôt d'un tumulus mixte, constitué de terre et de pierres.

#### Historique

Monument découvert par [F. Meyrat]{.creator} en [juin 2018]{.date}.

</section>

<section class="monument">

### [Saint-Michel]{.spatial} - [Zerkupé]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 1124m.

Ce tumulus est situé sur un petit ensellement qui domine, au Nord, le camp protohistorique de *Zerhupé*, distant de 300m environ ; la Voie Romaine (ou chemin de Compostelle), est à 250m au Nord ; Château Pignon domine à 500m au Nord-Est. On note une importante doline à quelques dizaines de mètres au Nord Nord-Est.

#### Description

Cet important tumulus est érigé sur un sol plat ; il est de forme ovale, orienté suivant un axe Est-Ouest et est constitué de terre et de pierres concassées ; il mesure 10,60m de long et 5,20m de large, sa hauteur est de 0,50m au Sud et de 0,70m au Nord. Ses dimensions et sa forme pourraient aussi évoquer un tertre d'habitat ; néanmoins il est isolé, sur un sol plat, et il n'est pas impossible qu'il puisse être un éventuel tumulus funéraire, dont l'époque resterait à préciser par une fouille.

#### Historique

Monument découvert par [J. Blot]{.creator} en [juillet 2017]{.date}.

</section>

# Saint-Pée-sur-Nivelle

<section class="monument">

### [Saint-Pée-sur-Nivelle]{.spatial} - [Apeztégi]{.coverage} 2 - [Tumulus]{.type}

#### Localisation

Il se trouve à 20m à l'Ouest Nord-Ouest du tumulus Apeztegi que nous avons décrit en 1976 [@blotTumulusBixustiaZuhamendi1976, p.111].

Altitude : 150m.

#### Description

Tumulus circulaire de 7m de diamètre et d'une vingtaine de centimètres de haut, constitué de terre et de pierraille.

À signaler, à 200m au Nord-Ouest, ce qui pourrait être un troisième *Tumulus - Apeztegi 3* de 8m de diamètre et d'une trentaine de centimètres de haut, fait de pierraille ; il est traversé par une clôture en barbelés.

#### Historique

Monuments trouvés en [décembre 2010]{.date} par [J. Blot]{.creator}.

</section>

<section class="monument">

### [Saint-Pée-sur-Nivelle]{.spatial} - [Apaztégui]{.coverage} Sud - [Pierre plantée]{.type} ([?]{.douteux})

#### Localisation

Altitude : 105m. Elle se trouve sur l'axe de parcours d'une croupe au Sud et en contre-bas des *Tumulus d'Apeztegui* [@blotInventaireMonumentsProtohistoriques2011, p.21].

#### Description

Pierre plantée, mais actuellement fortement inclinée vers l'Est. Elle mesure 0,85m de haut, 0,51m à sa base et 0,37m d'épaisseur en moyenne. Elle est la seule borne plantée de ce vaste pâturage.

#### Historique

Pierre découverte par [Blot J.]{.creator} en [mars 2012]{.date}.

</section>

<section class="monument">

### [Saint-Pée-sur-Nivelle]{.spatial} - [Croix-de-Sainte-Barbe]{.coverage} - [Cromlech]{.type}

#### Localisation

Altitude : 185m.

Ce cercle de pierres se trouve à 0,50m au Sud du calvaire érigé sur cette colline.

#### Description

De nombreuses pierres (une trentaine, plus nombreuses en secteur Nord et Sud Sud-Est), souvent au ras du sol mais cependant bien visibles, délimitent un cercle de 2,50m de diamètre environ ; il semble que l'on puisse distinguer, en secteur Nord-Ouest, les ébauches de deux autres cercles.

#### Historique

Monument découvert par [J. Blot]{.creator} en [novembre 2010]{.date}.

</section>

<section class="monument">

### [Saint-Pée-sur-Nivelle]{.spatial} - [Croix-de-Sainte-Barbe]{.coverage} 2 - [Tumulus]{.type}

#### Localisation

Altitude : 150m.

Il est situé sur un replat de la ligne de crête qui prolonge à l'Ouest la colline de la Croix-de-Sainte-Barbe, à environ une soixantaine de mètres à l'Ouest Nord-Ouest d'une antenne de relais téléphonique.

#### Description

Tumulus pierreux circulaire, de 8m de diamètre et environ 0,40m de haut.

#### Historique

Découvert par [Blot J.]{.creator} en [décembre 2010]{.date}. À noter que le *Tumulus n°1 de la Croix-de_Sainte-Barbe*, déjà décrit par nous, [@blotTumulusBixustiaZuhamendi1976, p. 111], a été évité de justesse par le passage d'engins sur la piste pastorale primitive ; on peut le deviner, enfoui sous les ronces, dans un virage, au Sud de celle-ci, dans le col.

</section>

<section class="monument">

### [Saint-Pée-sur-Nivelle]{.spatial} - [Helbarron]{.coverage} - [Tumulus]{.type} ([?]{.douteux})

#### Localisation

Cet important relief se voit à quelques dizaines de mètres à droite de la route qui mène de d'Helbarron à Saint-Pée, une fois traversé Helbarron.

#### Description

On note un relief de terrain d'environ 3m de haut environ et de forme plutôt ovale que circulaire, (70m x 40m). S'agit-il d'un relief d'origine anthropique, ou d'un simple mouvement de terrain, comme nous le penserions plus volontiers ?

#### Historique

Ce relief nous a été signalé par [F. Meyrat]{.creator} en [février 2011]{.date}.

</section>

<section class="monument">

### [Saint-Pée-sur-Nivelle]{.spatial} - [Larrekokurrutzea]{.coverage} - [Tumulus]{.type} ([?]{.douteux})

#### Localisation

Carte 1245 Est Espelette.

Altitude : 160m.

La croix d'un calvaire est érigée au-dessus...Volontairement ? le tumulus ne paraît pas contemporain de ce calvaire, mais bien antérieur. L'ensemble se trouve à 15m à l'Ouest d'un réservoir d'eau en ciment.

#### Description

Tumulus terreux très aplati (peut-être par des labours anciens), de 10m de diamètre et 0,80m de haut.

#### Historique

Monument découvert en [mars 1975]{.date}. Peut-être y a-t-il 2 autres monuments, douteux, l'un de 10m de diamètre à 60m à l'Ouest Nord-Ouest, et un second à 20m au Sud-Est.

</section>

<section class="monument">

### [Saint-Pée-sur-Nivelle]{.spatial} - [Martinhaurrenborda]{.coverage} 1 - [Pierre plantée]{.type} ([?]{.douteux})

#### Localisation

Pierre située dans un col, sur un terrain en friche, sur la ligne de crête qui mène à la redoute de Bizkartzu, elle est contiguë à la parcelle Mixalen Xola.

Altitude : 195m.

#### Description

Cette borne, de section rectangulaire, à grand axe orienté Nord-Est Sud-Ouest, mesure 1,08m de haut, 1,62m de long et 0,84m de large, à sa base. Sa face Nord ainsi que son sommet présentent de nombreuse traces d'épanellage.

#### Historique

Pierre découverte par [F. Meyrat]{.creator} en [août 2011]{.date}.

</section>

<section class="monument">

### [Saint-Pée-sur-Nivelle]{.spatial} - [Martinhaurrenborda]{.coverage} 2 - [Pierre plantée]{.type} ([?]{.douteux})

#### Localisation

Borne plantée en limite de parcelle sur Mixelen Xola, à environ 30m à l'Ouest de *Martinhaurrenborda 1 - Pierre plantée (?)*.

#### Description

De section rectangulaire, elle mesure, 0,52m de long et 0,42m de large, à sa base, et 1,18m de haut. Sur elle s'appuie le grillage de clôture.

#### Historique

Borne découverte par [F. Meyrat]{.creator} en [août 2011]{.date}.

</section>

<section class="monument">

### [Saint-Pée-sur-Nivelle]{.spatial} - [Pettikenborda]{.coverage} - [Tertres d'habitat]{.type}

#### Localisation

Altitude : 85m.

Ce groupe de 7 tertres d'habitat est situé à l'Ouest de la route d'Ahetze un peu avant le hameau Orgabidea et au Sud de Pettikenborda. Ces 7 éléments sont érigés sur un terrain en assez forte pente vers le Nord-Ouest, le bas de la prairie étant drainée par 3 petits rius qui convergent en un seul ensuite. Certains reliefs voisins peuvent être interprétés comme des vestiges de tertres d'habitat peu visibles ou de simples mouvements naturels de terrain (colluvion).

#### Description

-   *Tertre n°1* : le plus visible, mesure environ 12m de diamètre et 1,80m de haut.

-   *Tertre n°2* : situé à 8m au Nord-Ouest du précédent, mesure 8m de diamètre et 0, 60m de haut.

-   *Tertre n°3* : il est tangent au n°1 (au Sud-Ouest). Mesure 8m de diamètre et 0,60m de haut.

-   *Tertre n°4* : situé à 3m au Sud-Est du n°1 et à 4m environ à l'Ouest de la route. Mesure 8m de diamètre et 0,80m de haut.

-   *Tertre n°5* : il est situé à 15m au Sud Sud-Ouest du précédent et à 8m à l'Ouest de la route. Mesure 6m de diamètre et 0,90m de haut.

-   *Tertre n°6* : Situé à 6m à l'Ouest Sud-Ouest du précédent. Mesure 6m de diamètre et 0,8m de haut.

-   *Tertre n°7* : Situé à 15m au Sud Sud-Ouest du n°5 et à 5m à l'Ouest de la route. De forme oblongue, il mesure 6 x 3 m de diamètre et 0,8m de haut.

#### Historique

Le *Tertre n°1* a été trouvé par [J. Blot]{.creator} en [octobre 1971]{.date}, ainsi que les *tertres 2* et *3* en juillet 2013 ; les *tertres 4*, *5*, *6*, *7* ont été trouvés par [F. Meyrat]{.creator} en [juillet 2013]{.date}.

</section>

<section class="monument">

### [Saint-Pée-sur-Nivelle]{.spatial} - [Péritxen]{.coverage} 1 - [Tumulus]{.type}

#### Localisation

Altitude : 150m.

Situé sur terrain dégagé très légèrement incliné vers le Nord.

#### Description

Tumulus de terre de 10m de diamètre, et 0,20m à 0,30m de haut environ.

Il semblerait qu'à quelques dizaines de mètres plus au Nord, quelques mouvements de terrain pourraient évoquer des vestiges de tertres d'habitat ?

#### Historique

Tumulus découvert par [Blot J.]{.creator} en [janvier 2011]{.date}.

</section>

<section class="monument">

### [Saint-Pée-sur-Nivelle]{.spatial} - [Péritxen]{.coverage} 2 - [Tumulus]{.type}

#### Localisation

Altitude : 155m.

On peut le distinguer, encadré par les deux chemins qui montent vers le Nord-Ouest, à 4m au Sud-Ouest de celui le plus à droite.

#### Description

Tumulus discret de 6m de diamètre environ et de 0,20m à 0,30m de haut.

#### Historique

Tumulus découvert par [J. Blot]{.creator} en [janvier 2011]{.date}.

</section>

<section class="monument">

### [Saint-Pée-sur-Nivelle]{.spatial} - [Olhaetcheberrikoborda]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 90m.

On le voit à une dizaine de mètres au Nord de la route, au sommet d'un mouvement naturel du terrain.

#### Description

Tumulus de terre, ayant été notablement aplati par la transformation du site en prairie artificielle ; nous avions vu ce monument nettement plus en relief, en 1971, quand le terrain n'était qu'une lande. Il mesure actuellement une douzaine de mètres de diamètre et une trentaine de centimètres de haut.

#### Historique

Monument découvert par [J. Blot]{.creator} en [1971]{.date} et revu en [mars 2011]{.date}.

</section>

<section class="monument">

### [Saint-Pée-sur-Nivelle]{.spatial} - [Opalazio]{.coverage} - [Tumulus]{.type}

#### Localisation

Carte 1245 Est Espelette.

Altitude : 80m.

Il est situé dans une prairie au Nord-Est de la D 305.

#### Description

Tumulus terreux de 16m de diamètre et 0,80m de haut.

#### Historique

Découvert en [mai 1972]{.date}. Il a depuis été rasé par la construction de la maison Zamaldégia.

</section>

<section class="monument">

### [Saint-Pée-sur-Nivelle]{.spatial} - [Serres]{.coverage} - [Tumulus]{.type}

#### Localisation

Carte 1245 Est Espelette.

Altitude : 65m.

Il est situé à 6m à l'Ouest du carrefour d'où descend une route vers les nouveaux lotissements d'Ascain, et la D 918.

#### Description

Un calvaire est érigé sur ce qui pourrait avoir été un tumulus antique ayant alors servi de soubassement ; ce tertre a été ensuite coupé dans sa partie Nord-Ouest par le passage de la route asphaltée et un mur de soutènement a été construit au niveau de la coupe pour éviter l'effondrement du calvaire.

On peut estimer les dimensions originelles du tumulus à 30m de diamètre et 1,90m de haut.

#### Historique

Monument découvert en [juin 1973]{.date}.

</section>

# Sare

<section class="monument">

### [Sare]{.spatial} - [Altsaan]{.coverage} 1 - [Cromlech]{.type} ([?]{.douteux})

#### Localisation

Altitude : 520m.

#### Description

Un cercle de 3m de diamètre paraît délimité par un ensemble de dalles horizontales, de dimensions variables, avec peut-être, au centre, une [ciste]{.subject} formée de 5 autres dalles, dont une plus importante, au ras du sol ou à l'horizontale.

#### Historique

Monument découvert par [J. Blot]{.creator} en [mai 2011]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Altsaan]{.coverage} 2 - [Dalle couchée]{.type}

#### Localisation

Altitude : 430m.

#### Description

Dalle de [grés]{.subject} de forme oblongue dont le grand axe Est-Ouest mesure 2,77m ; sa largeur maximum est de 0,90m et son épaisseur de 0,25m à 0,30m en moyenne. Quelques traces d'épannelage semblent pouvoir être notées aux bords Sud, Nord, et à son extrémité Ouest.

#### Historique

Dalle vue par [F. Meyrat]{.creator} en [2011]{.date} et, peut-être avant lui, par [I. Txintxuretta]{.creator}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Altsaan]{.coverage} 3 - [Dolmen]{.type}

#### Localisation

Carte 1245 Est Espelette.

Altitude : 500m.

J.M.  de Barandiaran [@barandiaranCatalogueStationsPrehistoriques1946, p. 24] avait publié le dolmen n°1 (Alt 528m), et nous même le n°2 [@blotNouveauxVestigesMegalithiques1971, p.26], sous le nom d'Altsaan Est. Tous les monuments suivants sont échelonnés le long de la crête rocheuse de ce nom qui descend vers le Sud-Est.

Altsaan 3 est situé à 15m au Sud du n°2.

#### Description

Petit tumulus pierreux, fait de fragments de dalles de [grès]{.subject} d'environ 5m de diamètre et 0,60m de haut. La [chambre funéraire]{.subject}, quadrangulaire, orientée Ouest-Est, mesure 2,10m de long, 1,10m de large et 0,40m de profondeur ; elle est délimitée par de petites dalles de [grès rose]{.subject} local. Au Sud : 3 dalles dans le prolongement les unes des autres, mais non jointives, ayant les mêmes dimensions : 0,50m de long et 0,40m de haut. La paroi Nord est constituée de 2 dalles de mêmes dimensions que les précédentes, plus ou moins inclinées vers le sol ; il en est de même pour les parois Est et Ouest. Il n'y a pas de couvercle visible.

#### Historique

Dolmen découvert en [octobre 1977]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Altsaan]{.coverage} 4 - [Dolmen]{.type}

#### Localisation

Carte 1245 Est Espelette.

Altitude : 450m.

On trouve ce monument en continuant à descendre la crête rocheuse vers l'Est, à environ 300m au Sud-Est du précédent ; il est érigé dans le dernier petit col, près d'une échancrure qui fait communiquer les deux versants de cette crête.

#### Description

Tumulus pierreux de 7m de diamètre. La [chambre funéraire]{.subject} n'est pas centrale, mais dans le quadrant Nord-Est, peut-être du fait que le tumulus pourrait avoir été remanié par la construction d'un abri pastoral dans le secteur Sud. Cette chambre, orientée Est-Ouest, mesure approximativement 2m de long et 1m de large. À l'Ouest, une dalle de chevet reste plantée et verticale de même qu'une autre au Sud ; 3 autres dalles, couchées, complètent les parois Sud, Est et Nord.

#### Historique

Dolmen découvert en [octobre 1977]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Altsaan]{.coverage} 5 - [Dolmen]{.type}

#### Localisation

Carte 1245 OT Hendaye St-Jean-de-Luz.

Altitude : 527m.

Monument situé, sur un terrain en très légère pente vers l'Est.

#### Description

Cinq dalles de grés enfoncées dans le sol délimitent une [chambre funéraire]{.subject} d'environ 1,80m de long et 0,80m de large, orientée au Sud-Est.

La dalle, au Nord-Ouest, que l'on peut considérer comme étant celle du chevet, mesure 0,75m de long et 0,35mde haut ; les deux dalles limitant la chambre à l'Ouest, mesurent, la première : 0,54m de long et 0,25m de haut, la seconde : 0,50m de long et 0,55m de haut. On note au Sud-Est qu'une des dalles atteint 0,70 m de haut et 0,45m à sa base.

Tumulus avec de nombreuses dalles ou fragments de dalles, de faible hauteur et d'environ 5m à 6m de diamètre.

#### Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2009]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Altsaan]{.coverage} 6 - [Dolmen]{.type} ([?]{.douteux})

#### Localisation

Carte 1245 OT Hendaye St-Jean-de-Luz.

Altitude : 522m.

Il est situé à environ 100m à l'Est de la voie ferrée du petit train de la Rhune, au début de la crête rocheuse d'Alsaan qui s'étend vers l'Ouest.

#### Description

Monument de forme approximativement circulaire (3m de diamètre environ), constitué de larges dalles, les unes profondément enfoncées dans le sol, les autres horizontales, en désordre apparent à sa surface. À l'évidence, il s'agit d'une construction faite de main d'homme, mais dont la finalité reste difficile à préciser dans son état actuel ; dolmen ?

#### Historique

Construction découverte par [Blot J.]{.creator} en [octobre 2009]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Altsaan]{.coverage} 7 - [Dolmen]{.type}

#### Localisation

Carte 1245 OT Hendaye St-Jean-de-Luz.

Altitude : 425m.

#### Description

La chambre funéraire, large de 0,88m, est orientée Nord-Sud, et se trouve dans un tumulus pierreux d'environ 5m de diamètre ; elle est délimitée par deux dalles parallèles : la dalle Ouest mesure 1,30m de long, 0,33m de large ; elle est prolongée par un deuxième fragment de 0,50m de long, résultat d'une probable fracture. La dalle Est mesure 0,69m de long, et 0,30 d'épaisseur.

#### Historique

Monument découvert par [J. Blot]{.creator} en [mars 2010]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Altsaan]{.coverage} 8 - [Dolmen]{.type} ([?]{.douteux})

#### Localisation

Altitude : 525m.

#### Description

Trois dalles, dont on ne voit que les sommets semblent délimiter une [chambre funéraire]{.subject} orientée Sud-Est Nord-Ouest, d'environ 2m de long et 1m de large. Les deux dalles au Nord-Est mesurent respectivement 1,30m de long et 0,35m de long ; celle au Sud-Ouest mesure 0,70m de long. Il semble qu'il y ait un léger relief tumulaire, sur lequel quelques dalles horizontales sont visibles. Monument très douteux.

#### Historique

Monument découvert par [J. Blot]{.creator} en [mars 2010]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Altsaan]{.coverage} 9 - [Dolmen]{.type}

#### Localisation

Il est très exactement tangent au Nord-Ouest du *dolmen Altsaan n°2*.

#### Description

Tumulus pierreux d'environ 5m à 6m de diamètre, au milieu duquel apparaît une importante [chambre funéraire]{.subject} orientée Sud-Ouest Nord-Est, mesurant environ 3m de long et 1,30m de large. Elle est constituée d'une dizaine de dalles dont la position et l'ordonnance ont été plus ou moins modifiées avec le temps. On peut toutefois signaler que la paroi Sud-Ouest est formée d'une dalle de 0,87m de long et 0,52m de haut, la dalle formant la paroi Nord-Est, en grande partie basculée vers l'extérieur, mesure 1,10m de long et 0,73m de large.

#### Historique

Monument découvert en [mars 2010]{.date} par notre ami [F. Meyrat]{.creator}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Altsaan]{.coverage} 10 - [Dolmen]{.type}

#### Localisation

Altitude : 405m.

Monument fort difficile à trouver. Le mieux est de partir du petit col situé à 430m d'altitude, où se trouve *Altsaan 7 - Dolmen*, un monolithe, ainsi que 2 dalles couchées. De ce col, se diriger vers le Nord-Est, une centaine de mètres plus loin et plus bas. Un long filon rocheux descendant lui aussi vers le Nord-Est, fait d'empilement de dalles épaisses, sur environ 80m de long, en arrêtant la solifluxion des terres en amont, a déterminé, à son extrémité supérieure Sud-Ouest, la formation d'un pseudo-replat, d'une vingtaine de mètres de large. C'est sur ce dernier, qui forme lui aussi un angle d'environ 30° par rapport à l'horizontale que le dolmen est érigé, ce qui reste tout à fait exceptionnel.

#### Description

On note un tumulus très érodé et remanié, de 7m à 8m de diamètre et 0,70m de haut environ ; il est fait de petites dalles disposées en «écaille de poisson» de façon très régulière, en particulier dans son secteur la mieux conservé, au Sud-Ouest. Dans la région centrale, apparaissent deux fragments de dalles, verticaux et parallèles qui peuvent matérialiser une [chambre funéraire]{.subject} de 1,50m de long et 1m de large, à grand axe orienté Sud-Est Nord-Ouest. Dans le secteur Sud-Est de ce tumulus, il semble que certaines dalles puissent être attribuées à un péristalithe ; sur son flanc Nord-Ouest, à 2,80m de la [chambre funéraire]{.subject}, gît une grande dalle parallélépipédique, de 2,60m de long, 1,50m de large et d'une épaisseur variant de 0,14m à 0,30m ; il semble qu'on puisse la considérer comme la [table de couverture]{.subject}, de même que, peut-être, deux autre fragments situés au flanc Nord-Est du tumulus et mesurant respectivement 0,80m x 0,74m et 1,02m x 0,82m.

#### Historique

Monument découvert par [I. Txintxuretta]{.creator} en [mars 2011]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Altsaan]{.coverage} 11 - [Dolmen]{.type}

#### Localisation

Altitude : 512m.

Il se trouve sur la longue crête d'Altsaan.

#### Description

Dolmen construit sur un terrain en légère pente vers l'Est. La [chambre funéraire]{.subject} qui mesure environ 2m de long et 1,50m de large, est orientée Nord-Sud... dans la mesure où nous ne confondons pas longueur et largeur, compte tenu de l'état de délabrement du monument !!

La dalle la plus importante est à l'Ouest. Elle est verticale, enfouie au ras du sol, mais sa face interne, dans la [chambre funéraire]{.subject}, est dégagée ; elle mesure 1,76m de long, 0,13m d'épaisseur et sa hauteur, (dans la chambre), est de 0,55m. - La dalle Sud, qui pourrait correspondre à la dalle de chevet, lui est perpendiculaire, verticale ; elle mesure 1,83m de long à sa base, 1,03m dans sa plus grande hauteur, et 0,18m d'épaisseur. Les autres parois ne sont pas identifiables.

Un tumulus pierreux constitué de fragments de dalles, entoure la chambre ; sa hauteur est plus marquée à L'Est (0,90m de haut) qu'à l'Ouest.

#### Historique

Monument découvert par [P. Velche]{.creator} en [juillet 2011]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Altsaan]{.coverage} 12 - [Dolmen]{.type}

#### Localisation

Altitude : 510m.

Il est quasi tangent au Sud de la piste pastorale.

#### Description

On ne distingue pas de tumulus, mais une très probable [chambre funéraire]{.subject}, orientée Est-Ouest., mesurant 2,10m de long et 1,50m de large environ. Parmi les nombreuses dalles du site, il semble que la paroi Ouest de la chambre soit représentée par une dalle inclinée, de 1,20m de long et 0,10m de haut environ ; la paroi Sud est constituée de deux dalles verticales et plantées dans le sol, mesurant chacune 1,10m de long et huit centimètre d'épaisseur - Enfin au Nord, on voit deux autres dalles, dont l'une, verticale, épaisse de 0,16m, longue de 0,56m et haute de 0,50m est, elle aussi, bien plantée dans le sol.

#### Historique

Monument découvert par [Meyrat F.]{.creator} en [octobre 2011]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Altsaan]{.coverage} 13 - [Dolmen]{.type} (ou [dalle couchée]{.type} ?)

#### Localisation

Altitude : 530m.

#### Description

Cette dalle de grés [triasique]{.subject}, de forme grossièrement parallélépipédique, est allongée suivant un grand axe Nord-Est Sud-Ouest Sud-Est. Elle mesure 3,56m de long et 2,84m de large pour une épaisseur moyenne de 0,33m ; elle paraît présenter des traces d'épannelage sur toute sa périphérie. Ce qui est remarquable, c'est la présence, le long de son bord Nord-Ouest, du sommet de 3 dalles, profondément enfoncées dans le sol ; il pourrait s'agir des montants d'une [chambre dolménique]{.subject}, dont les autres seraient cachés sous cette dalle (de couverture...).

#### Historique

Monument découvert par [Meyrat F.]{.creator} en [Janvier 2012]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Altsaan]{.coverage} 14 - [Dolmen]{.type} ([?]{.douteux})

#### Localisation

Altitude : 530m.

#### Description

Un ensemble de 5 dalles semble délimiter une chambre funéraire dolménique (ou une ciste), allongée suivant un axe Nord-Est Sud-Ouest, mesurant 0,95m de long et 0,81 de large.

La paroi Sud-Est est représentée par 2 dalles plantées dans le sol, mesurant 0,80m de long, 0,50m de haut pour l'une et 1,16m de long et 0,45m de haut pour la seconde. La paroi Nord-Ouest est formée de 2 autres dalles, dont la plus longue fait 1,06m de long et 0,68m de haut. Une seule petite dalle, elle aussi plantée dans le sol fermerait la chambre au Nord-Est. La paroi Sud-Ouest est représentée par un bloc de [grés]{.subject} de 0,82m de large sur lequel repose une autre importante dalle qui pourrait être la [dalle de couverture]{.subject} de la [chambre funéraire]{.subject}. Monument douteux.

#### Historique

{.date}Monument découvert par [Meyrat F.]{.creator} en [janvier 2012]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Altsaan]{.coverage} - [Cromlech]{.type} ([?]{.douteux})

#### Localisation

Altitude : 415 m.

Il se trouve dans le dernier petit col, à l'extrémité Est de la crête rocheuse d'Altsaan.

#### Description

On distingue nettement un grand cercle de 7m de diamètre, délimité par une trentaine de pierres, sur sol plat, contigu à une murette de pierres sèches qui empiète légèrement sur son secteur Sud-Ouest.

#### Historique

Monument découvert par [J. Blot]{.creator} en [mars 2010]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Altsaan]{.coverage} - Grande [dalle couchée]{.type} ([?]{.douteux})

#### Localisation

Il est situé dans le même col que *Altsaan 7 - Dolmen*, à une vingtaine de mètres à l'Est Sud-Est du *Dolmen n°4*.

#### Description

Grande dalle couchée de [grés]{.subject} gris, de forme triangulaire, à sommet Nord. Elle mesure 4,20m de long, 2,24m dans sa plus grande largeur, et 0,44m d'épaisseur en moyenne. Il n'y a aucune trace d'épannelage.

#### Historique

Découvert par [I. Txintxuretta]{.creator} en [2009]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Altsaan]{.coverage} - [Monolithe]{.type}

#### Localisation

Altitude : 430m.

Il est à une cinquantaine de mètres au Nord du petit col où se trouve *Altsaan 7 - Dolmen*, le long de la piste ascendante vers le Nord.

#### Description

Belle dalle de [grés]{.subject} gris, grossièrement rectangulaire couchée au sol, mesurant 2,39m de long, 1,66m de large et une vingtaine de centimètres d'épaisseur. Il est particulièrement remarquable de noter que tout le pourtour de cette dalle porte des traces évidentes d'épannelage.

#### Historique

Monument découvert par [J. Blot]{.creator} en [mars 2010]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Altsaan]{.coverage} - [Tumulus]{.type}

#### Localisation

Carte 1245 Est Espelette.

Altitude : 320m.

Il est situé sur un petit replat à 80m environ à l'Est de l'extrémité rocheuse de la crête d'Altsaan, et à 100m au Sud-Ouest d'une bergerie. Le terrain est en légère pente vers le Sud-Est.

#### Description

Tumulus mixte de terre et de pierres de 10m de diamètre et 0,20m de haut, avec une légère dépression en son centre.

#### Historique

Monument découvert en [septembre 1976]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Altsaan]{.coverage} bis - [Tumulus]{.type}

#### Localisation

Carte 1245 OT Hendaye St Jean-de-Luz.

À 1,30m à l'Ouest de *Altsaan 1 - Tumulus-cromlech*.

#### Description

Ce monument se présente comme un tumulus pierreux de faible hauteur, mesurant 3m de diamètre, constitué de dalles de formes et dimensions variées, apparaissant en désordre à la surface du monument.

#### Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2009]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Altsaan]{.coverage} 1 - [Tumulus-cromlech]{.type}

#### Localisation

Carte 1245 OT Hendaye St Jean-de-Luz.

Altitude : 515 m.

Ce monument se trouve sur un terrain en légère pente vers l'Ouest.

#### Description

On note un ensemble de dalles de dimensions variées, enfoncées dans le sol, délimitant un léger tumulus circulaire de 2,50m de diamètre, à la surface duquel apparaissent d'autre dalles.

#### Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2009]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Altsaan]{.coverage} 2 - [Tumulus-cromlech]{.type}

#### Localisation

Carte 1245 OT Hendaye St Jean-de-Luz.

Altitude : 500m.

Ce monument est situé sur une petite éminence.

#### Description

Tumulus circulaire de 2,80m de diamètre et 0,20m de haut environ, dont une trentaine de pierres marquent la périphérie.

#### Historique

Monument découvert par [J. Blot]{.creator} en [mars 2010]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Altsaan]{.coverage} 3 - [Tumulus-cromlech]{.type}

#### Localisation

Altitude : 520m.

On le trouve dans une sorte d'abri sous-roche.

#### Description

Tumulus de 3,20m de diamètre et 0,35m à 0,40m de haut, formé de terre et de pierres de dimensions variables. Le péristalithe est particulièrement bien individualisé, dans le secteur Est par une grande dalle verticale de 1,62m de long, 0,30m de haut et une quinzaine de centimètres d'épaisseur. La moitié Ouest est délimitée par un demi-cercle de pierres plus ou moins rectangulaires, presque jointives, profondément enfouies dans le sol, et dont certaines atteignent 0,45m x 0,35m.

#### Historique

Monument découvert par [J. Blot]{.creator} en [mai 2011]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Ametzia]{.coverage} - [Cromlech]{.type}

#### Localisation

Altitude : 220m.

Il est situé au pied de la colline Ametzia, qui prolonge au Sud les crêtes de Faague, au bord de la piste antique, partie de Harotzegikoboeda et qui rejoint le col Kondediagalepoa. On le trouve sur un petit replat à 50m à l'Est du franchissement par la piste d'un petit ru qui descend d'Ametzia.

#### Description

Petit cercle de 2,50m de diamètre, délimité par une dizaine de pierres, au ras du sol, plus visibles et plus nombreuses en secteur Est.

#### Historique

Monument découvert par [nous]{.creator} en [mai 2009]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Ametzia]{.coverage} 1 - [Tumulus]{.type}

#### Localisation

Carte 1245 OT Hendaye St Jean-de-Luz.

Altitude : 420m.

Il est situé à 3m à l'Ouest du *Dolmen Ametzia 1* déjà décrit par nous [@blotNouveauxVestigesMegalithiques1971, p.4].

#### Description

Tumulus circulaire pierreux de 4m de diamètre et 0,30m de haut, dont les éléments les plus importants sont visibles dans sa moitié Ouest (comme disposés en demi-cercle ?). S'agit-il d'un tumulus dolménique ruiné ou d'un tumulus simple ?

#### Historique

Monument découvert par [J. Blot]{.creator} en [novembre 2009]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Ametzia]{.coverage} 2 - [Tumulus]{.type}

#### Localisation

Carte 1245 OT Hendaye St Jean-de-Luz.

Altitude : 440m.

Il est situé sur la gauche de la piste qui monte du col au pied de la chapelle d'Olhain, au moment où elle arrive sur le plat.

#### Description

Tumulus circulaire terreux, d'environ 5m de diamètre et 0,60m de haut, dans lequel quelques pierres sont cependant bien visibles.

À 6m au Nord se voit une *pierre plantée*, bien connue des randonneurs, en bordure de piste. Elle mesure 0,95m de haut, 0,60m à sa base et 0,22m d'épaisseur. Nous ne la rattacherions pas à la catégorie des monuments protohistoriques.

#### Historique

Monument découvert par [I. Gaztelu]{.creator} en [mars 2002]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Ameztia]{.coverage} 2 - [Dolmen]{.type}

#### Localisation

Altitude : 440m.

À environ 230m au Nord Nord-Ouest du dolmen d'Ameztia publié par nous en 1971 [@blotNouveauxVestigesMegalithiques1971, p. 14]. À partir du col d'Olhain, prendre le sentier ascendant direction Sud-Ouest. Sur la partie plane, en haut de la colline, au niveau d'un petit col, se trouve une dalle verticalement plantée dans le sol. Continuer 80 pas sur le sentier en direction Nord Nord-Est, le monument est visible tangent à la piste sur son côté gauche, un terrain très légèrement en pente.

#### Description

On distingue les restes d'un petit tumulus pierreux d'environ 5m de diamètre, constitué de blocs de [grés]{.subject} certains pouvant atteindre le volume d'un gros pavé, ou même plus. On ne distingue pas de péristalithe nettement défini. Une dalle en [grés]{.subject}, orientée Sud-Ouest Nord-Est, de 1,15m de long, 0,25m de large et 0,45 de haut, est profondément enfoncée dans le sol dans la partie Sud-Est de ce tumulus. À une trentaine de centimètres au Nord, on distingue au ras du sol deux dalles brisées, contiguës, dont l'ensemble atteint 0,40m, pour ce qui en est visible et 0,15 d'épaisseur. L'ensemble de ces éléments paraît bien pouvoir être considéré comme les vestiges d'une modeste chambre funéraire.

#### Historique

Monument découvert par [A. Martinez]{.creator} en [2000]{.date}. À noter qu'il ne ressemble en rien aux descriptions que fait J.M. de Barandiaran des dolmens *Arribeltz 1* et *2* qui devraient être dans les environs...([?]{.douteux}). Nous ne les avons jamais vus.

</section>

<section class="monument">

### [Sare]{.spatial} - [Aniotzbeherekoborda]{.coverage} - [Dalle plantée]{.type} ([?]{.douteux})

#### Localisation

Altitude : 260m.

Elle est sur un petit plateau dominant le dépôt de la gare du petit train, qui est à 500m à l'Est Sud-Est, la borde est à 600m au Nord-Est et la piste montant à la redoute passe à 60m au Sud-Est.

#### Description

Dalle de grès local plantée dans le sol, légèrement inclinée vers l'Est. Elle mesure 1,90m de long, de 0,20m à 0,30m d'épaisseur et 0,52m de haut. Elle présente des traces d'épannelage à sa périphérie. Pas de tumulus visible ni d'autres dalles. Vestige d'un dolmen ?

#### Historique

Dalle découverte par [Meyrat F.]{.creator} en [juin 2012]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Airagarri]{.coverage} 2 - [Tumulus-cromlech]{.type}

#### Localisation

Carte 1245 Est Espelette.

Altitude : 540m.

Le cromlech n°1 a été décrit en 1949 [@barandiaranContribucionEstudioCromlechs1949, p.206] et 1953 [@barandiaranHombrePrehistoricoPais1953, p.250 n°85].

Il est situé sur le plateau très remanié par l'homme, à environ 70m au Sud Sud-Est de l'ancienne redoute en pierres sèches, et à 25m à l'Est de la piste qui va de cette redoute au chemin Altsaan-Ihicelhaya.

#### Description

Petit tumulus mixte de terre et dallettes de [grés]{.subject} rose, mesurant 3,50m de diamètre et 0,20m de hauteur. Douze pierres délimitent la périphérie, dont une, très visible, de 0,40m de haut.

#### Historique

Monument découvert en [février 1974]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Airagarri]{.coverage} 3 - [Cromlech]{.type}

#### Localisation

Sur le même plateau que *Airagarri 2 - Tumulus-cromlech*, et à 15m au Sud Sud-Ouest de *Airagarri 2 - Tumulus-cromlech*.

#### Description

Une dizaine de pierres délimitent un cercle de 2m de diamètre ; en secteur Ouest, l'une d'elles, mesurant 1m de long et 0,20m de haut est particulièrement visible.

#### Historique

Monument découvert en [février 1974]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Airagarri]{.coverage} 4 - [Cromlech]{.type}

#### Localisation

Altitude : 530m.

Il est situé à une dizaine de mètres au Sud-Ouest de *Airagarri 3 - Cromlech*.

découvert par nous en février 1974, ainsi que le n°2 [@blotInventaireMonumentsProtohistoriques2009, chap. 1]

Altitude : 530m

#### Description

À la périphérie d'un léger relief circulaire de 4,80mètres de diamètre, trois dalles situées dans sa moitié Nord-Ouest font partie d'un très probable péristalithe. Elles mesurent respectivement 0,50m, 0,90m et 0,80m de long ; au centre est visible une autre pierre.

#### Historique

Ce monument a été découvert par notre ami [F. Meyrat]{.creator} en [mars 2010]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Argaïne]{.coverage} - [Monolithe]{.type}

#### Localisation

Altitude : 373m.

Il gît sur le sol, tangent à l'Ouest, au sentier GR 10, au niveau d'un vaste replat marqué par un beau pâturage.

#### Description

Il s'agit d'une dalle de [grés]{.subject} rose, de forme grossièrement parallélépipédique, à grand axe orienté Nord-Sud, mesurant 2,27m dans sa plus grande dimension, 1,12m de large et dont l'épaisseur varie, suivant les côtés, de 0,10m à 0,23m. Il semble que l'on puisse noter quelques traces d'épannelage à son bord Ouest. À quelques centimètres de son extrémité Nord, existe un bloc de grés triangulaire, mesurant 1,05m à sa base, qu'il est difficile de rattacher à la dalle précédente.

Par contre, tangente à l'Est, une dalle de [grés]{.subject}, elle aussi de forme approximativement triangulaire, mesurant 1,10m de long, 0,98m de large, et 0,11m d'épaisseur (apparente) à l'Ouest, pourrait être, semble-t-il, rattachée à la dalle principale. Nous pensons, comme l'inventeur, que cette dalle pourrait en constituer le sommet « en pointe » qui se serait brisé et retourné avant de toucher le sol - dans le cas d'un monolithe dressé - (dont la base Nord aurait été enfoncé dans le sol). Des irrégularités en forme de « cannelures » sont en effet retrouvées tant au bord Ouest de la grande dalle que sur le bord Nord-Ouest de ce fragment triangulaire, suggérant qu'il s'agit bien d'un même bloc brisé. Seul un dégagement complet de l'ensemble pourrait trancher le problème.

#### Historique

Monolithe découvert par [P. Velche]{.creator} en [août 2010]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Argaïne]{.coverage} - [Tumulus]{.type}

#### Localisation

On le remarque à 12m au Sud-Est du monolithe précédent.

#### Description

Tumulus pierreux, en partie dissimulé par la végétation, de près de 6m de diamètre et 0,20m à 0,30m de haut.

#### Historique

Tumulus découvert par [J. Blot]{.creator} en [janvier 2011]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Argaïne]{.coverage} 2 - [Dolmen]{.type}

#### Localisation

Carte 1245 Est Espelette.

Altitude : 330m.

Ce dolmen n'a aucun rapport avec le dolmen du même nom décrit par J.M.  de Barandiaran au Sud Sud-Ouest des grottes de Sare. [@barandiaranCronicaPrehistoriaPirineo1951, p.240 n°267]. Il s'agit d'une simple homonymie.

*Argaine 2* est situé dans le petit vallon qui naît entre la crête d'Altsaan au Nord et Athekaleoun et Ourkilepoa au Sud. Il est visible à 100m au Sud Sud-Est de la borde Argaïne et il tangent au Nord d'un chemin rural ; il bénéficie d'une très belle vue dégagée à l'Est.

#### Description

Tumulus mixte de terre et de pierrailles, d'environ 11m de diamètre et 0,60m de haut. Une dépression centrale contient la [chambre funéraire]{.subject} orientée Sud-Est Nord-Ouest, et mesurant 2,50m de long, 1,50m de large et 1,15m de profondeur.

Il ne reste que 2 dalles limitant cette chambre : Une dalle Nord mince, mesurant 1,80m de long, 1,15m de haut et une autre, à l'Ouest, pratiquement perpendiculaire à la précédente, de 2,40m de long, et 0,50m de haut, beaucoup plus épaisse (près de 0,40m) et profondément enfoncée dans le sol. Pas de couvercle visible.

#### Historique

Dolmen découvert en [septembre 1976]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Arrosagaraykoborda]{.coverage} - [Monolithe]{.type}

#### Localisation

Altitude : 340m.

Situé à une soixantaine de mètres à l'Ouest de la borde du même nom.

#### Description

Dalle de [grés]{.subject} parallélépipédique, à sommet effilé, plantée perpendiculairement au sol. Elle mesure 1,70m de haut, 0,75m de large et 0,37m d'épaisseur à sa base. Elle présente des traces d'épannelage à son sommet et à son bord Est.

#### Historique

Monument découvert par [J.M. Lecuona]{.creator} en [2011]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Arrosagaraykoborda]{.coverage} - [Dalle plantée]{.type}

#### Localisation

À 11m à l'Est Sud-Est de *Arrosagaraykoborda - Monolithe*.

#### Description

Dalle de [grés]{.subject} épaisse de 0,37m, 1,05m de large et 1,15m de haut, orientée suivant un axe Est-Ouest. Présente des traces évidentes d'épannelage sur son bord Est.

#### Historique

Dalle découverte par [Blot J.]{.creator} en [janvier 2012]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Airagarri]{.coverage} 4 - [Cromlech]{.type}

#### Localisation

Altitude : 530m.

Il est à 2m au Sud-Est de la piste pastorale.

#### Description

Cercle assez irrégulier, (le diamètre varie entre 7,50 et 8m), délimité par 17 pierres environ de dimensions très variables. Il semble qu'il existe une légère surélévation centrale de 0,20m. À l'intérieur de ce monument apparaissent 4 pierres de dimensions notables (l'une atteint 0,80m de long) dont seule une fouille pourrait donner la signification éventuelle...

#### Historique

Ce monument découvert par [F. Meyrat]{.creator} en [2010]{.date} a de nouveau été étudié par ce même auteur en [2015]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Airagarri]{.coverage} - [Monolithe]{.type}

#### Localisation

Altitude : 559m.

Ce monolithe se trouve à 3m au Sud Sud-Ouest du cromlech *Airagarri C1* (décrit par J.M.  de Barandiaran) et à 9m à l'Ouest d'un gros bloc rocheux, repère très visible sur ce petit replat.

#### Description

Monolithe de forme générale parallélépipédique, couché au sol selon un axe Nord-Sud. Il mesure 2,30m de long, 1,20m de large à son extrémité Sud, 0,78m à son extrémité Nord, et 0,20m d'épaisseur en moyenne. Il présente des traces de régularisation sur ses bords Nord, Est et Ouest.

#### Historique

Monolithe découvert par [F. Meyrat]{.creator} en [janvier 2015]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Arrondoa]{.coverage} 1 - [Dolmen]{.type} ([?]{.douteux})

#### Localisation

Altitude : 131m.

Ce monument ([?]{.douteux}) est inclus dans le mur de clôture de la maison Arrondoa. Toutefois les bords latéraux verticaux de la dalle sont restés libres, tout en restant jointifs aux autres éléments constitutifs de la murette, construite en 1951 (témoignage de Michel et Joseph Lasaga, propriétaires des lieux).

#### Description

Le « monument » se résume actuellement à une belle dalle de grès [triasique]{.subject}, orientée Nord-Ouest Sud-Est, enfoncée dans le sol, dont tout le pourtour montre des traces d'épannelage. Ses dimensions sont les suivantes : hauteur : 1,32m ; largeur au sommet : 1,09m ; largeur maximum : 1,31m ; épaisseur : 0,18m. Cette dalle pourrait très bien être la dalle de chevet restante de la chambre funéraire d'un dolmen orientée vers le Nord-Est, dont les autres montants auraient disparu. À signaler qu'une autre dalle de 3m de long, et 0,30m à 0,40m d'épaisseur gisait à 3 ou 4m en avant et au Nord-Est de la dalle ci-dessus décrite et aurait pu être un élément constitutif d'un dolmen, sa table de [couverture]{.subject} par exemple...

Ce vestige, et a été enlevé et brisé en plusieurs morceaux en 1967.

#### Historique

C'est l'historique de cette dalle qui attire l'attention : l'abbé [J.M. de Barandiaran]{.creator} l'avait vue dans les années 1940-1950, alors qu'elle n'était pas encore incluse dans la murette actuelle. Il avait alors dit au propriétaire des lieux (le père de Joseph et Michel), qu'on avait probablement à faire à une « tombe ». Nous signalons donc cet dalle, possible vestige d'un dolmen, pour être le plus exhaustif possible... Elle nous a été signalée par [Cl. Chauchat]{.creator} en [octobre 2015]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Arrondoa]{.coverage} 2 - [Dolmen]{.type} ([?]{.douteux})

#### Localisation

Altitude : 139m.

À 80m environ au Nord Nord-Ouest de *Arrondoa 1 - Dolmen (?)*.

#### Description

On peut voir une construction très curieuse, incluse, là encore, dans une murette bordant un champ en pente assez marquée.

Il s'agit d'une sorte d'abri, ouvert vers l'Est, construit sur un sol pratiquement plat à cet endroit. Il est constitué d'une vaste dalle de [couverture]{.subject}, en grès [triasique]{.subject}, de forme grossièrement arrondie, d'environ 2,50m à 3m de diamètre et 0,55m d'épaisseur ; sa périphérie présente de nombreuses traces de retouche.

Elle repose sur deux blocs rocheux parallélépipédiques reposant sur le sol, non pas parallèlement, mais disposés de manière divergente formant un angle aigu à l'Ouest ; l'un, au Sud, mesure 1,20m de long environ, et 0,60m d'épaisseur ; l'autre au Nord, de 1,90m de long et 0,60m d'épaisseur. Au Sud, l'espace entre les deux blocs est fermé par un autre beaucoup plus modeste, de 0,60m de long.

Ces deux blocs et la dalle de [couverture]{.subject}, disposés par la main de l'homme, pourraient évoquer un abri ; mais le poids de la dalle de [couverture]{.subject}, disproportionné pour un simple abri peut aussi faire penser à un dolmen fort rustique quant à son architecture : le sol, constitué à cet endroit d'une roche plate, en place, aurait pu empêcher d'enfoncer les montants latéraux.

#### Historique

Cette construction nous a été signalée par Mr. [Michel Lasaga]{.creator}, qui l'a toujours connue ainsi.

</section>

<section class="monument">

### [Sare]{.spatial} - [Airagarri]{.coverage} 5 - [Cromlech]{.type} ([?]{.douteux})

#### Localisation

Situé à 70m au Sud Sud-Ouest de *Airagarri C4*.

#### Description

On distingue 4 dalles à au ras du sol, (3 d'entre elles mesurant 0,60m x 0,40m), qui semblent délimiter un demi-cercle de 5m de diamètre.

#### Historique

Monument découvert par [J. Blot]{.creator} en [mai 2011]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Atermin]{.coverage} - [Ciste]{.type}

#### Localisation

Altitude : 330m.

Il est situé à une centaine de mètres et en contrebas au Nord-Ouest de la borde Arrosakogaraykoborda.

#### Description

À ne pas confondre avec le *Dolmen Atermin* [@barandiaranHombrePrehistoricoPais1953, p.240]. La chambre funéraire est orientée Est-Ouest, et mesure 1,45m de long et 0,55m de large. La paroi Sud-Ouest est délimitée par une dalle verticalement plantée de 1,43m de long, 0,14m d'épaisseur, et 0,40m de haut. Au Nord-Ouest, on note une dalle perpendiculaire à la précédente ; elle mesure 0,55m de long et 0,38m de haut. La paroi Nord-Est est délimitée par une série de 5 pierres au ras du sol ; deux petites dalles ferment la [chambre]{.subject} au Sud-Est. Enfin une dalle de 1m x 0,51m occupe le fond de la chambre. Cette dernière marque le centre d'un tumulus de 5m de diamètre environ, et 0,40m de haut, constitué de terre et de pierres ; l'existence d'un péristalithe est possible...

#### Historique

Découvert en [2011]{.date} par [I. Txintxuretta]{.creator} et [X. Taberna]{.creator} (sous le nom de *ciste Atermin*).

</section>

<section class="monument">

### [Sare]{.spatial} - [Athekaleun]{.coverage} - [Dalle plantée]{.type} ([?]{.douteux})

#### Localisation

Altitude : 550m.

#### Description

Cette dalle est verticalement plantée dans un sol incliné vers le Nord-Est, près d'un pierrier issu de la crête d'Athekaleun, où se trouve le monolithe d'Athekaleun, que nous avons publié en 1983. Elle mesure environ 0,90m de haut, 0,80m à sa base, et huit centimètres d'épaisseur en moyenne.

#### Historique

Dalle découverte par [I. Gaztelu]{.creator} en [janvier 1988]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Athekaleun]{.coverage} 1 - [Dalle couchée]{.type}

#### Localisation

Altitude : 510m.

À partir de l'ensellement d'Athekaleun (altitude 530m), prendre un petit sentier qui descend vers le Nord-Ouest ; la dalle est sur le chemin.

#### Description

Cette dalle de [grés]{.subject} [triasique]{.subject} gît sur un sol en légère pente vers le Nord-Est ; elle paraît avoir glissé et repose dans sa partie Nord-Est sur quelques blocs et dalles de [grés]{.subject}, sans que l'on puisse y voir la moindre structure voulue. Elle mesure 3,50m dans son plus grand axe Nord-Ouest Sud-Est, 1,80m de large et de 0,10m à 0,25m d'épaisseur. Elle présente des traces d'épannelage, en particulier à son bord Nord-Est - s'agit-il de l'action de carriers « modernes » ?

#### Historique

Dalle trouvée par [Velche P.]{.creator} en [juillet 2011]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Athekaleun]{.coverage} 2 - [Dalle couchée]{.type} ([?]{.douteux})

#### Localisation

Altitude : 470m.

#### Description

Dalle couchée sur l'ancien chemin qui se rend d'Urkila aux Trois Fontaines. De grès local rectangulaire elle mesure 1,02m de long, 0,50m de large et 0,19m d'épaisseur. Sa forme régulière et les nombreuses traces d'épannelage sur toute sa surface nous font penser à un travail de carriers récent.

#### Historique

Dalle découverte par [Badiola P.]{.creator} en [juin 2011]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Atxurri]{.coverage} - [Dalle couchée]{.type}

#### Localisation

Altitude : 525m.

Cette dalle est incluse dans un ensemble de trois bordes en ruine ; elle est à 5m au Sud-Ouest de l'une d'elles, et à 30m au Sud-Est de la lisière du bois de conifères.

#### Description

Cette dalle forme actuellement une partie du «toit» d'une structure qui a pu servir d'abri pour animaux, mesurant 6m de long et 3 de large environ, pour 0,50m en moyenne de profondeur - creusée dans un terrain en pente vers le Nord-Est - et dont il reste des vestiges de parois en pierres sèches ou en dalles. La dalle de [grés]{.subject} [triasique]{.subject}, mesure 3,95m de long, 3,50m dans sa plus grande largeur, et 0,30m d'épaisseur. Son pourtour semble bien avoir été épannelé dans sa totalité.

Elle peut avoir été taillée pour son rôle actuel mais tout aussi être une borne antique récupérée depuis plus ou moins longtemps.

#### Historique

Dalle découverte par [Meyrat F.]{.creator} en [mars 2012]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Atxurri]{.coverage} - [Dolmen]{.type}

#### Localisation

Altitude : 430m.

Il est très près de la ligne frontière, à l'Est d'un important bois de conifères, sur un promontoire relativement plat. Il est à 2m au Nord-Ouest d'une borne marquée d'un **B** gravé à sa partie haute.

#### Description

Un léger tumulus de 5m de diamètre marque le monument, dont la périphérie semble balisée de 5 pierres (4 au Sud-Ouest et 1 à l'Est). Une chambre funéraire centrale, de 2m x 0,68m orientée Nord-Est Sud-Ouest est délimitée par 8 dalles. La paroi Sud-Est est formée de trois dalles de 0,82m, 0,18m et 0,44m de long, alignées selon l'axe Nord-Est Sud-Ouest ; elles dépassent de peu la surface (0,18m pour la dernière).

Un autre alignement de 4 dalles forme la paroi Nord-Ouest ; elles mesurent respectivement 0,38m (et 0,15m de haut), 0,24m, encore 0,24m, et 0,50m (et 0,30m de haut). L'épaisseur moyenne de toutes ces dalles est de six à huit centimètres.

#### Historique

Monument découvert par [J.M. Lecuona]{.creator} en [février 2011]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Bechinen Ardi Borda]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 265m.

Il est sur un replat à 55m à l'Est des ruines de Bechinenborda, et dominant, au Nord, la piste de transhumance antique qui se rend à Kondendiakolepoa.

#### Description

Tumulus érigé sur terrain plat, de terre et de pierres, mesurant environ 5m de diamètre et 0,40m de haut.

#### Historique

Tumulus découvert par [Badiola P.]{.creator} en [avril 2012]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Bechinen Ardi Borda]{.coverage} 1 - [Cromlech]{.type}

#### Localisation

Il est à 3,50m au Nord de Bechinen Ardi Borda - Tumulus.

#### Description

7 pierres périphériques au ras du sol délimitent un cercle de 4m de diamètre ; au centre une pierre plus importante.

#### Historique

Monument découvert par [Badiola P.]{.creator} en [mai 2012]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Bechinen Ardi Borda]{.coverage} 2 - [Cromlech]{.type} ([?]{.douteux})

#### Localisation

À 0,80m au Nord de *Bechinen Ardi Borda 1 - Cromlech*.

#### Description

5 pierres au ras du sol délimitent un cercle de 2,5m de diamètre (le quart Nord-Ouest en est démuni). Monument douteux.

#### Historique

Cercle découvert par [Badiola P.]{.creator} en [mai 2012]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Etchegaraikoborda]{.coverage} 1 - [Dolmen]{.type}

#### Localisation

Carte 1245 OT Hendaye St Jean-de-Luz.

Altitude : 320m.

Ce monument est situé sur un replat d'une croupe issue du mont Faage, (ou Fago), orientée vers l'Est. Elle fait face à la colline de la chapelle d'Olhain, qui se situe au Sud.

#### Description

Monument très dégradé. Tumulus essentiellement constitué de blocs de [grés]{.subject} et de fragments de dalles, mesurant 5,50m de diamètre et 0,10m de haut. Au centre se voit une dalle de [grés]{.subject} plantée de 0,90m de long, 0,10m d'épaisseur et 0,20m de haut, orientée Ouest-Est. À 0,66m au Nord, on distingue le sommet d'une autre pierre (0,15m de long, et 0,10m de haut) qui semble bien être tout ce qui reste, avec la dalle précédente, d'une [chambre funéraire]{.subject} orientée plein Est ; au Nord-Est, sur le tumulus, gît une grande dalle, en partie enfouie dans la terre, qui pourrait être un élément de cette chambre (la [couverture]{.subject} ?).

#### Historique

Monument découvert par [J. Blot]{.creator} en [Octobre 2009]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Etchegaraikoborda]{.coverage} 2 - [Dolmen]{.type}

#### Localisation

Ce monument est situé à une trentaine de mètres à l'Ouest de *Etchegaraikoborda 1 - Dolmen*.

#### Description

On note un tumulus pierreux de 60m de diamètre et 0,30m de haut environ, au centre duquel se voit un ensemble de 3 dalles de [grés]{.subject} gris semblant bien n'être que le seul vestige de la paroi Sud d'une [chambre funéraire]{.subject} orientée Est-Ouest, dont la longueur supposée, à s'en tenir à ces seuls éléments, pourrait atteindre environ 1m. Les dimensions de ces 3 dalles quasiment accolées les unes aux autres sont respectivement de 0,72m, 0,88m et 0,64m. Au Nord de celles-ci, et à 0,70m de distance, on note deux dalles couchées au sol, l'une de 1,14m de long et 0,30m de large, l'autre de 0,59m de long et 0,27m de large, qui pourraient avoir aussi fait partie de la chambre, ainsi que de nombreuses autres dalles ou fragments de dalles disséminés sur le tumulus.

#### Historique

Monument découvert par [J. Blot]{.creator} en [octobre 2009]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Etxalar]{.coverage} - Dolmen

#### Localisation

Altitude : 524m.

Monument situé sur la ligne frontière, (à 25m au Sud de la BF 49).

#### Description

Sur une éminence dominant l'horizon on note un tumulus de terre et de pierres de 7m de diamètre, et 0,40m de haut, avec une légère dépression centrale, correspondant à la [chambre funéraire]{.subject} orientée Est-Ouest. Ses dimensions sont proches de celles du montant Nord toujours en place, penchée vers le Nord, qui mesure 2m de long, 0,73m de haut et 1,03m d'épaisseur.

#### Historique

Monument découvert par [Blot J.]{.creator} en [mars 2012]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Faageko erreka]{.coverage} 1 Sud - [Cromlech]{.type}

#### Localisation

Carte IGN 1245 OT Hendaye - Saint-Jean-de-Luz.

Altitude : 260m.

Ce monument se trouve dans une belle fougeraie parsemée de chênes têtards, à quelques dizaines de mètres au Sud de l'antique piste de transhumance reliant Sare à Vera.

#### Description

Une vingtaine de pierres de [grés]{.subject} gris d'un volume pouvant atteindre 1m x 0,55m délimitent un cercle bien visible de 5,50m de diamètre. Quelques pierres sont aussi visibles à l'intérieur du cercle.

#### Historique

Monument découvert en [2000]{.date} par [A. Martinez Manteca]{.creator}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Faageko erreka]{.coverage} 2 Sud - [Cromlech]{.type}

#### Localisation

À une douzaine de mètres à l'Ouest Nord-Ouest de *Faageko erreka 1 Sud - Cromlech*.

#### Description

Une quinzaine de pierres enfoncées dans le sol, peu visibles, délimitent un cercle de 3,20m de diamètre, excavé en son centre (fouille ancienne ?) où apparaissent deux petites dalles.

#### Historique

Monument découvert par [L. Millian]{.creator} en [2000]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Faageko erreka]{.coverage} 3 Nord - [Cromlech]{.type}

#### Localisation

Altitude : 271 m.

Ce monument se trouve lui aussi dans une belle fougeraie, surplombant la précédente. On y trouve le dolmen de Faague et ce cromlech est à une distance de 10m au Sud Sud-Ouest de ce dolmen.

#### Description

Une dizaine de pierres, dépassant de peu la surface du sol, délimitent un cercle d'environ 3m de diamètre.

#### Historique

Monument trouvé en [2000]{.date} par [A. Martinez Manteca]{.creator}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Faageko erreka]{.coverage} 4 Nord - [Cromlech]{.type}

#### Localisation

À une vingtaine de mètres au Sud Sud-Est du *Dolmen de Faague*.

#### Description

Onze pierres de volume assez important (certaines atteignent 0,50m à 0,60m de long), délimitent un cercle bien visible de 3m de diamètre.

#### Historique

Monument découvert en [octobre 2009]{.date} par [J. Blot]{.creator}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Faague]{.coverage} - [Dolmen]{.type}

#### Localisation

Altitude : 270m.

Sur un replat transformé en une belle fougeraie, non plantée d'arbres, au flanc Sud du petit sommet dénommé Faague sur les cartes IGN (552m d'altitude). Ce replat fait face au mont Ibantelly, de l'autre côté de la D 406 reliant Sare à Vera.

#### Description

On distingue facilement, sur ce replat dénudé, ce qui reste de ce très probable monument. Les éléments les plus visibles sont les deux dalles qui apparaissent sur le côté Est de cet ensemble pierreux. Une grande dalle en [grés]{.subject} légèrement inclinée vers le Nord-Ouest et orientée Sud-Ouest Nord-Est, mesure 1,40m de long ; son épaisseur est de 0,34m à son extrémité Sud-Ouest et de 0,45m à l'autre extrémité. Sa hauteur est de 0,80m.

Une seconde dalle, bien visible se dresse à 0,47m au Sud-Ouest de la précédente, et perpendiculaire à l'orientation de cette dernière. Rectangulaire, plane sur ses deux faces, elle mesure 0,88m de haut, 0,15m d'épaisseur en moyenne et 0,47m de large. Elle présente des signes d'épannelage à son bord Nord-Ouest.

Il semblerait qu'une troisième dalle, faisant partie de la structure initiale de la [chambre funéraire]{.subject}, ait été brisée presque au ras du sol, n'ayant plus de visible que sa base à environ 0,80m au Nord-Ouest de la précédente et presque dans son prolongement. Elle mesure quelques centimètres de haut, 0,70m de long, et de 0,25m à 0,19m de large à ses extrémités.

On peut enfin se poser la question du rôle et de la position initiale d'un bloc rocheux parallélépipédique, situé à une quarantaine de centimètres au Nord-Ouest de la seconde dalle

décrite, basculé vers le Sud-Est, et dont la base, n'est plus insérée dans le sol ; il mesure 0,95m de haut, 0,50m de large en moyenne et 0,35m d'épaisseur ; il présente des traces d'épannelage à son sommet.

Ces structures sont visibles dans ce qui a pu être initialement un tumulus pierreux. Toutefois il nous semble que celui-ci a fait l'objet de remaniements profonds (ayant pu affecter aussi la [chambre funéraire]{.subject}). On voit en effet très nettement qu'il existe une structure carrée ([?]{.douteux}) d'environ 5m x 5m, faite de blocs plus ou moins tangents et volumineux., dont les 3 dalles précédemment décrites seraient parties constitutives au Sud-Est et au Sud-Ouest. De nombreux autres blocs sont visibles, en désordre, à l'intérieur de cette structure. Tout ceci n'est pas sans nous rappeler le dolmen *Generalen Tomba*, dans les Aldudes, qui a lui aussi subi ce genre de transformation (pour en faire un modeste abri pastoral).

#### Historique

Ce monument a été découvert par [A. Martinez]{.creator} en [2000]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Faague]{.coverage} - [Monolithe]{.type}

#### Localisation

Altitude : 280m.

En bordure de la vieille piste pastorale qui, partie de la D 506, 1500m avant la borne frontière 36, se dirige en montant vers le Nord-Ouest, pour rejoindre un très beau col, à 600m, où se trouve la BF 32. Arrivée à l'aplomb des rochers de Faague, mais, bien en-dessous, à 270m d'altitude, cette piste traverse une très belle fougeraie, dénuée d'arbres, dont elle en longe la lisière Sud sur une centaine de mètres. Cette fougeraie est délimitée à l'Est et à l'Ouest par deux ravins parcourus par chacun par un ruisseau.

Le monolithe se trouve en bordure de piste, côté Nord, à l'extrémité Ouest de la fougeraie, à une trentaine de mètres après que le terrain ait amorcé sa déclivité vers l'Ouest.

Il est situé, à 300m à l'Ouest de *Faague - Dolmen*.

#### Description

Ce monolithe affecte la forme, classique en Pays basque, d'un pain de sucre orienté Nord-Sud, à base Nord. Ce bloc de [grés]{.subject} mesure 2,90m de long, 1,70m dans sa partie moyenne, la plus large. Son épaisseur varie de 0,50m à sa base à 0,37m au sommet.

Il semble bien qu'il y ait des traces d'épannelage à sa base, et tout le long du côté Ouest jusqu'à la pointe, ceci ayant eu pour effet de donner une certaine symétrie avec le côté opposé, aux formes naturelles.

On remarquera la situation classique de ce monolithe, en bord de piste pastorale, près de points d'eaux et au milieu de pâturages ; nous avons déjà émis l'hypothèse que les monolithes (ou *Muga* en basque = borne), puissent, en effet, être des bornes ayant servi de points de ralliement pour régler les différends pouvant survenir entre pasteurs, dès les temps protohistoriques, quant à l'exploitation des pâturages et des points d'eaux. De plus, il est particulièrement curieux de noter que ce monolithe se trouve exactement à l'aplomb de la borne 36, et donc des *Tables de Lizuniaga*, elles-mêmes toujours consacrées aux accords pastoraux.

Comme le faisait remarquer J.M. de Barandiaran, ces accords, compte tenu de la richesse des environs en monuments protohistoriques, peuvent fort bien remonter à ces lointaines époques. Par ailleurs, il n'est pas certain que ce fond de vallon, étroit, humide, boueux dans le passé - mais où passe l'actuelle D. 506 - ait été très favorable dans la protohistoire, aux déplacements des troupeaux ou aux réunions de pasteurs, ni que le monolithe originel, dont les Tables ont repris le rôle ensuite, ait été à cet emplacement dès le début.

C'est pourquoi -- *mais ce n'est qu'une hypothèse* - ce monolithe de Faague, placé au bord d'une belle piste pastorale - reliant elle aussi Sare à Vera - au milieu de pâturages dégagés, pourrait - avec quelques probabilités - être considéré comme le véritable ancêtre des Tables de Lizuniaga...

Rappelons qu'il n'y avait, à ce jour, que 2 monolithes connus dans la Rhune, que nous avons déjà publiés : *Gastenbakarre* [@blotMonolithesPaysBasque1983, p. 1] et *Athekaleun* [@blotMonolithesPaysBasque1983, p. 2].

#### Historique

Ce monument a été découvert par [nous]{.creator} en [mars 2009]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Fago]{.coverage} - [Monolithe]{.type}

#### Localisation

Altitude : 605m.

Il se trouve à 25m au Sud-Est de la BF 29, sur terrain plat.

#### Description

Belle dalle de [grés]{.subject} rose en forme de pain de sucre, gisant au sol selon un axe Nord Nord-Est, Sud Sud-Ouest. D'environ une dizaine de centimètres d'épaisseur, elle mesure 2,64m de long et 1,36m dans sa plus grande largeur. Elle semble présenter des traces d'épannelage à son sommet Sud-Ouest et sur un renflement de son bord Ouest.

#### Historique

Monolithe découvert par [P. Badiola]{.creator} en [janvier 2011]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Fago]{.coverage} - [Cromlech]{.type} ([?]{.douteux})

#### Localisation

On le trouve à environ 5m au Sud-Est de l'angle Sud-Est d'une grande borde en ruine, elle-même située à une centaine de mètres au Sud de la BF 29.

#### Description

On ne distingue qu'un demi-cercle (3,40m de diamètre environ) constitué d'une dizaine de pierres, visibles au ras du sol ; ce dernier ayant été remanié aux alentours, il est difficile d'être plus précis.

#### Historique

Elément trouvé par [J. Blot]{.creator} en [mars 2011]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Fago]{.coverage} 3 - [Dalle couchée]{.type}

#### Localisation

Nous avons déjà cité, le long de cette ligne frontière, et dans les alentours de la BF 29, les *dalle couchées Fago n°1* et *n°2* [@blotInventaireMonumentsProtohistoriques2013] et le *monolithe Fago* [@blotInventaireMonumentsProtohistoriques2011]. Voici une 3^ème^ dalle couchée, sans que l'on puisse en préciser la finalité ni l'époque.

Altitude : 515m.

Elle est située à 10m au Nord-Est de l'ancienne BF 30, couchée au sol, elle-même aussi à 10m au Nord-Est de la nouvelle BF 30, détruite, en morceaux sur le sol.

#### Description

Dalle de [grés]{.subject} [triasique]{.subject}, de forme lancéolée, allongée sur le sol selon un axe Nord-Est Sud-Ouest, mesurant 1,12m à sa base, 1,78m de long, et une vingtaine de centimètres d'épaisseur en moyenne. Tout son pourtour montre des traces évidentes d'épannelage.

#### Historique

Dalle signalée par [P. Badiola]{.creator} en [juin 2014]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Fago]{.coverage} 1 - [Dalle couchée]{.type}

#### Localisation

Altitude : 600m.

Elle est à une quarantaine de mètres à l'Est du *monolithe de Fago*.

#### Description

Dalle de [grés]{.subject} local, mesurant 1m de long et 0,35m de large. Elle présente des traces d'épannelage sur tout son pourtour.

#### Historique

Dalle découverte par [Badiola P.]{.creator} en [avril 2012]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Fago]{.coverage} 29 - [Dalle couchée]{.type} ([?]{.douteux})

#### Localisation

Altitude : 605m.

#### Description

Cette dalle est plantée selon un axe Sud-Ouest Nord-Est, mais très inclinée vers le Nord. Sa longueur est de 1,30m, sa largeur de 0,53m et son épaisseur de 0,15m. Ses bords Sud-Ouest et Nord-Ouest présentent des traces évidentes d'épannelage.

#### Historique

Dalle découverte par [Badiola P.]{.creator} en [Mai 2012]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Fago]{.coverage} 2 - [Dolmen]{.type} ([?]{.douteux})

#### Localisation

Altitude : 545m.

Ce probable dolmen est tangent au Sud de la piste 4x4, un peu avant qu'elle n'atteigne le col de Fago.

#### Description

On note, contre un buisson d'ajoncs, quelques pierres d'assez importantes dimensions, qui ont roulé là (naturellement ? action anthropique ?) ; elles dissimulent en partie une dalle de [grés]{.subject} verticale, solidement plantée dans le sol - un tumulus probable - selon un axe Est-Ouest. De forme trapézoïdale, cette dalle verticalement plantée, mesure plus d'un mètre à sa base, 0,76m de haut, et son épaisseur varie de 7 à 10 centimètres.

#### Historique

Monument trouvé par [J. Blot]{.creator} en [mars 2011]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Fago]{.coverage} - [Dolmen]{.type}

#### Localisation

Altitude : 561m.

Il est pratiquement au sommet de ce plateau allongé vers l'Est qui se trouve au Nord-Est du mont Fago. De là, il domine l'horizon.

#### Description

On note tout d'abord un important tumulus pierreux de 8m de diamètre et 0,40m de haut ; les structures centrales sont difficiles à préciser : dans la partie Nord Nord-Est du tumulus on note une dépression de 1m de large environ et d'une dizaine de centimètres de profondeur (fouille ancienne ?) et dans la partie Sud-Ouest de ce même tumulus apparaît discrètement ce qui pourrait être le sommet d'une longue dalle enfouie dans le sol (plus d'un mètre de long semble-t-il). Est-ce une partie de la [chambre funéraire]{.subject}, mais qui ne serait pas très centrale... ?

#### Historique

Monument découvert par [J. Blot]{.creator} en [octobre 2009]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Fago]{.coverage} 1 et 2 - [Tertres d'habitat]{.type}

#### Localisation

Altitude : 410m.

Se trouvent au col de Fago, de part et d'autre de la piste du même nom.

#### Description

-   *TH n°1* : tangent à la piste, à l'Ouest, il mesure 4m de diamètre, 0,70m de haut.

-   *TH n°2* : situé à 10m à l'Ouest du précédent, de l'autre côté de la piste. Mesure 4m de diamètre et 0,40m de haut.

#### Historique

Tertres signalés par [Badiola P.]{.creator} en [octobre 2011]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Fago]{.coverage} - [Tumulus]{.type} ([?]{.douteux})

#### Localisation

Altitude : 405m.

À l'extrémité Sud d'un plateau herbeux, à 3m ou 4m à l'Ouest d'une barre rocheuse.

#### Description

Monument très douteux. On distingue cependant un relief tumulaire circulaire d'environ 6m de diamètre et 0,30m de haut, à la périphérie duquel on peut compter 5 à 6 pierres, ainsi que deux autres au centre ([ciste]{.subject} centrale ?).

#### Historique

Monument douteux découvert par [J. Blot]{.creator} en [octobre 2009]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Gastenbakarre]{.coverage} 5 - [Cromlech]{.type} ([?]{.douteux})

#### Localisation

Carte IGN 1245 OT. Hendaye Saint-Jean-de-Luz.

Il est situé sur le replat juste au-dessus de *Gastenbakarre n°5 - Dolmen*.

Altitude : 351m.

#### Description

À la périphérie d'un très léger relief circulaire de 6m de diamètre, il semble que 3 pierres, disposées dans le secteur Nord fassent partie d'un péristalithe (monument très douteux).

#### Historique

Monument découvert par [J. Blot]{.creator} en [mars 2010]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Gastenbakarre]{.coverage} 2 - [Dalle couchée]{.type}

#### Localisation

Altitude : 275m.

Cette dalle se voit en bordure Sud-Ouest de la piste qui passe devant le grand monolithe planté, plus au Nord, et qui se dirige vers le Sud.

#### Description

Grosse dalle de [grés]{.subject} de forme approximativement triangulaire, allongée selon un axe Ouest Sud-Ouest, Est Nord-Est, à base épaisse au Nord-Est (0,57m), en bordure de piste et dont l'extrémité Sud-Ouest est plus mince et plus effilée. Elle mesure 3,10m de long, 1,14m dans sa plus grande largeur, et semble présenter des traces d'épannelage tout le long de son bord Sud-Est.

#### Historique

Dalle découverte par [Duvert M.]{.creator} en [décembre 2011]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Gastenbakarre]{.coverage} 3 - [Dolmen]{.type}

#### Localisation

Carte 1245 Est Espelette.

Altitude : 269m.

Nous avons décrit [@blotNouveauxVestigesMegalithiques1971, p.6] deux dolmens situés en un petit col au flanc Nord-Est de Larraun, à environ 1000m à vol d'oiseau au Nord du col de Saint-Ignace. Le n°1 a été complètement saccagé depuis par des « chercheurs de trésor » !

Il est à environ 150m au Sud-Est du n°2, et à 3m au Nord-Est du chemin qui monte vers Altsaan.

#### Description

On note un tumulus mixte de terre et de pierres de 11m de diamètre et 0,70m de haut. Au centre une [chambre funéraire]{.subject} bien visible, orientée plein Est, mesure 2m de long, 0,90m de large et 0,45m de profondeur. Elle est totalement vidée de son contenu puisque le fond en est visible, tapissé d'un petit dallage de plaquettes de [grés]{.subject}, chose assez rare dans le cas de ces monuments dans notre région.

Quatre dalles délimitent par ailleurs cette chambre :

Au Nord, une belle dalle, légèrement inclinée vers l'intérieur, mesure 1,70m de long et 0,45m de haut ; elle est prolongée par une seconde, de dimensions plus restreintes : 0,70m de long et 0,65m de haut.

À l'Ouest, une dalle mesurant 0,90m de long et 0,30m de haut, est perpendiculaire à la précédente.

À l'Est, on note une dalle bien plus petite, de 0,50m de long et 0,45m de haut.

Il n'y a pas de couvercle visible.

#### Historique

Dolmen découvert en [avril 1974]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Gastenbakarre]{.coverage} 4 - [Dolmen]{.type}

#### Localisation

Carte IGN 1245 OT. Hendaye Saint-Jean-de-Luz

Altitude : 271m.

Il est situé au départ de la piste qui se rend, vers l'Ouest, au ruisseau Gastenbakarreko erreka, et à une quinzaine de mètres au Nord-Est de sa jonction avec la piste ascendante vers Altsaan.

#### Description

Il existe un très discret tumulus de 5m de diamètre environ au milieu duquel apparaissent 4 dalles de [grés]{.subject} rose, très inclinées qui délimitent une [chambre funéraire]{.subject} de 2,25m de long et 0,57m de large environ, orientée Nord-Ouest Sud-Est. Les dimensions de ces dalles sont, au Nord de 0,60m et 1m de long ; au Sud de 0,83m et 0,44m de long. Il existe une cinquième dalle couchée au Sud Sud-Est de la chambre, probablement un de ses éléments constitutifs.

#### Historique

Monument découvert par [J. Blot]{.creator} en [mars 2010]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Gastenbakarre]{.coverage} 5 - [Dolmen]{.type}

#### Localisation

Carte IGN 1245 OT. Hendaye Saint-Jean-de-Luz.

Altitude : 340 m.

Il est situé à 20m au Nord-Est de la piste ascendante, sur un terrain en très légère pente vers le Sud-Est.

#### Description

On note trois dalles dont les sommets apparaissent au ras du sol, délimitant une [chambre funéraire]{.subject} de 1,40m de long et 0,80m de large, orientée Est-Ouest. Les dimensions des dalles : dalle Nord : 0,70m de long, 0,16m de haut ; dalle Sud : en deux fragments, l'un de 0,67m de long, l'autre de 0,20m de long ; la dalle de chevet, à l'Ouest, mesure 0,52m de long.

On ne note pas de tumulus visible.

#### Historique

Monument découvert par [J. Blot]{.creator} en [mars 2010]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Gastenbakarre]{.coverage} 6 - [Dolmen]{.type} ([?]{.douteux})

#### Localisation

Carte IGN 1245 OT. Hendaye Saint-Jean-de-Luz.

Situé à 10m à l'Est de *Gastenbakarre n°5 - Dolmen*.

#### Description

On note deux dalles (qui ne sont en fait qu'une seule, fragmentée en deux), orientées Nord-Sud, l'une mesurant 0,78m de long, l'autre 0,84m, et une hauteur de 0,30m et 0,20m. Il n'y a pas de tumulus visible.

#### Historique

Monument découvert par [J. Blot]{.creator} en [mars 2010]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Gastenbakarre]{.coverage} 7 - [Dolmen]{.type} ([?]{.douteux})

#### Localisation

Altitude : 290m.

Il est situé à environ 80m au Nord-Ouest des ruines d'un cayolar et à 20m au Sud-Ouest du ruisseau.

#### Description

On peut voir 2 dalles verticales, de [grés]{.subject} [triasique]{.subject}, érigées selon un axe Est-Ouest, au centre d'un probable tumulus pierreux dont il est difficile d'apprécier les dimensions sur ce terrain en légère pente vers le Sud. La dalle Nord mesure 1,37m à la base et 0,59m de haut ; son épaisseur atteint 0,14m. Celle au Sud mesure 1,02m à sa base, 0,50m de haut et son épaisseur est de 0,14m.

#### Historique

Elément trouvé par [J. Blot]{.creator} en [avril 2011]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Gastenbakarre]{.coverage} 8 - [Dolmen]{.type}

#### Localisation

Altitude : 300m.

Situé à peu de distance à l'Ouest de *Gastenbakarre n°7 - Dolmen ([?]{.douteux})*, sur un terrain en très légère pente vers l'Est.

#### Description

La [chambre funéraire]{.subject}, qui mesure 2,50m de long et 1,20m de large, est orientée selon un axe Est Nord-Est, Ouest Sud-Ouest. Elle est délimitée, au Nord, par une grande dalle de 2m de long, 0,33m de haut et 0,10m d'épaisseur en moyenne, dont tout le bord supérieur présente des traces d'épannelage ; au Sud-Ouest, la paroi est formée par deux autre dalles de 0,67m et 0,84m de long, doublées à l'extérieur par une troisième de 0,74m de long. Toutes ces dalles sont au ras du sol.

#### Historique

Monument découvert par [Meyrat F.]{.creator} en [nombre 2012]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Gastenbakarre]{.coverage} 2 - [Monolithe]{.type}

#### Localisation

Altitude : 330m.

Monolithe situé sur un petit replat, dominant la rive droite du ruisseau Gastenbakarrekoerreka, (à environ 80m).

#### Description

Belle dalle plate de [grés]{.subject} [triasique]{.subject}, gisant selon un axe sensiblement Est-Ouest, qui est celui de la légère pente du sol, et de son plus grand axe, soit une longueur de 3,20m. Elle mesure 1,55m de large et son épaisseur moyenne est d'une vingtaine de centimètres.

#### Historique

Ce monolithe nous a été signalé par [L. Millan]{.creator} en [mars 2011]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Gastenbakarre]{.coverage} 3 - [Monolithe]{.type}

#### Localisation

Altitude : 335m.

#### Description

Belle dalle de [grés]{.subject} [triasique]{.subject} allongée selon un axe Nord-Est Sud-Ouest, mesurant 2,80m de long, 1m à sa base Nord-Est, 0,68m à son sommet, et 0,20m d'épaisseur en moyenne. Elle présente de nombreuses traces d'épannelage sur 3 de ses côtés (sauf le coté Sud-Ouest).

#### Historique

Monolithe découvert par [Ph. Velche]{.creator} en [mai 2011]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Gastenbakarre]{.coverage} - [Pierre couchée]{.type} ([?]{.douteux})

#### Localisation

Altitude : 275m.

#### Description

Cette dalle gît sur le flanc Est de Larraoun, à l'Ouest de la piste pastorale empierrée ci-dessus évoquée, à laquelle sa base est tangente.

Dalle de [grés]{.subject} local, de forme parallélépipédique, allongée selon un axe Ouest-Est et à sommet arrondi. Elle mesure 2,10m de long, 1,15m de large à sa base, 0,57m à la corde de l'arc du sommet ; son épaisseur varie de 28 à 32 centimètres. Il semble bien que le sommet arrondi et tout le bord Nord de cette dalle présentent des traces d'épannelage.

Simple dalle naturelle ou monolithe vrai ayant subi une action anthropique ?

#### Historique

Elle nous a été signalée par [M. Duvert]{.creator} en [octobre 2010]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Gastenbakarre]{.coverage} - [Tumulus]{.type}

#### Localisation

Carte IGN 1245 OT. Hendaye Saint-Jean-de-Luz.

Altitude : 275m.

On trouve ce tumulus sur un replat, en montant à une soixantaine de mètres au Nord-Ouest du *Dolmen Gastenbakarre n°3*.

#### Description

Tumulus de terre et de pierres de 6m de diamètre environ, et 0,45m de hauteur. On note une dalle qui affleure le sol à sa périphérie en secteur Est, et les sommets de quelques autres affleurant très discrètement au centre posent la question d'un éventuel tumulus dolménique.

#### Historique

Monument découvert par [J. Blot]{.creator} en [mars 2010]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Gastenbakarreko erreka]{.coverage} - [Monolithe]{.type}

#### Localisation

Carte IGN 1245 OT. Hendaye Saint-Jean-de-Luz.

Il est situé à 20m à l'Ouest du *Dolmen Gastenbakarreko erreka n°2*.

#### Description

Volumineux bloc de pierre en [grés]{.subject} gris, grossièrement rectangulaire, couché au sol, selon un axe Nord-Est Sud-Ouest. Ses dimensions : 2,30m de long, 1m de large, et une trentaine de centimètres d'épaisseur. Il est particulièrement remarquable de noter que ce monolithe présente des traces très nettes d'épannelage sur tout son pourtour.

#### Historique

Monument découvert par [J. Blot]{.creator} en [octobre 2009]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Gastenbakarreko erreka]{.coverage} 1 et 2 - [Cromlechs]{.type}

#### Localisation

Carte IGN 1245 OT. Hendaye Saint-Jean-de-Luz.

Altitude : 285 m.

Monuments peu visibles, on les trouve à quelques mètres au Sud du ruisseau Gastenbakarreko erreka qui s'étale dans le gazon ; ils sont à peu près à équidistance des ruines d'un cayolar ruine, au Nord, et du monolithe dalle, bien connu et bien visible, dressé au Sud, à 80m environ.

#### Description

-   *Cromlech n°1* : Une dizaine de pierres, surtout visible dans sa moitié Sud, enfouies dans le sol, délimitent un cercle de 2m de diamètre.

-   *Cromlech n°2* : il est situé à quelques centimètres au Nord-Est du précédent. Huit pierres périphériques délimitent un cercle de 2,60m de diamètre.

#### Historique

Monuments découverts par [J. Blot]{.creator} en [octobre 2009]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Gastenbakarreko Erreka]{.coverage} 4 - [Dolmen]{.type}

#### Localisation

Altitude : 245m.

Ce monument est situé sur un petit replat à une vingtaine de mètres au Sud du ruisseau Gastenbakarreko erreka.

#### Description

La [chambre funéraire]{.subject}, orientée selon un axe Est-Ouest, se voit au milieu des restes d'un tumulus pierreux, peu élevé, de 4m à 5m de diamètre ; elle mesure environ 3m de long et 1m de large. On distingue 2 montants au Nord : une grande dalle verticale de 1,80m de long à sa base, de 0,26m d'épaisseur et 0,90m de haut, et, dans son prolongement Ouest, une autre dalle, verticale, elle aussi, de 0,95m de long et 0,85m de haut. Au Sud, deux autres montants sont visibles : le premier, vertical, mesure 1,15m de long à sa base, 0,50m de haut et 0,16m d'épaisseur ; le second a basculé vers l'intérieur de la [chambre funéraire]{.subject}, et mesure 1m de long, 0,25m de haut et 0,25m d'épaisseur. De nombreuses autres dalles sont visibles sans qu'il soit possible de leur attribuer un rôle éventuel.

#### Historique

Monument découvert par [Meyrat F.]{.creator} en [octobre 2011]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Gorostiarria]{.coverage} - [Monolithe]{.type}

#### Localisation

Carte IGN 1245 OT. Hendaye Saint-Jean-de-Luz.

Altitude : 513m.

Il se trouve au Sud de la crête d'Altsaan, au niveau de la station de croisement du train à crémaillère, sur une aire gazonnée sous une pinède. C'est la seule pierre importante de ce site, et elle se trouve à une soixantaine de mètres au Nord-Est de la voie ferrée.

#### Description

Epaisse dalle rectangulaire de [grés]{.subject} rose, dont le bord Nord-Ouest a été brisé assez récemment semble-t-il. Son extrémité Sud Sud-Est, arrondie, présente de très nettes traces d'épannelage. Elle mesure 2,44m de long, 1,60m de large et 0,35m d'épaisseur.

#### Historique

Monolithe découvert par [J. Blot]{.creator} en [mars 2010]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Gorostiarria]{.coverage} - [Dolmen]{.type}

#### Localisation

Carte 1245 Ouest Saint-Jean-de-Luz.

Altitude : 549m.

Il est situé à 30m à l'Est du petit bois marqué 541 sur la carte IGN, et à 30m au Nord des rails du funiculaire, sur un terrain en légère pente vers le Sud. De ce col on a une très belle vue vers les montagnes à l'Est.

#### Description

Tumulus terreux recouvert d'herbe, de 9m de diamètre et 0,40m de haut. Cinq pierres à sa périphérie évoquent un possible péristalithe. Le tumulus paraît plus marqué dans sa partie Sud du fait de la légère inclinaison du terrain.

La [chambre funéraire]{.subject}, orientée Est Sud-Est, mesure environ 1m de long, 0,70m de large et 0,25m de haut ; elle est délimitée par 2 modestes dalles en grès rose local : - au Sud-Ouest, une dalle de 1m de long, 0,22m d'épaisseur et 0,25m de haut ; - une autre, au Nord-Est, de 0,35m de long et 0,25m de haut. Il n'y a pas de couvercle visible.

#### Historique

Dolmen découvert en [octobre 1975]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Ibantelli]{.coverage} Sud - [Dolmen]{.type}

#### Localisation

Carte IGN 1245 OT Hendaye - Saint-Jean-de-Luz.

Altitude : 480m.

Il est érigé à l'extrémité d'une prairie en pente douce vers l'Est, à environ 200m au Nord-Est de la BF n°43.

#### Description

Il se présente sous la forme d'un tumulus terreux et pierreux de 7m à 8m de diamètre et 0,40m de haut. Une légère excavation centrale (fouille ancienne ?), laisse apparaître une portion de dalle qui pourrait faire partie d'une [chambre funéraire]{.subject} orientée Ouest-Est.

#### Historique

Monument découvert par [I. Txintxurreta]{.creator} en [2004]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Ibantelli]{.coverage} - [Monolithe]{.type}

#### Localisation

Altitude : 600m.

Monolithe situé à 80m au Nord de la BF 39, en bordure de piste, et en lisière d'un bois de conifères, sur terrain plat.

#### Description

Important bloc de [grés]{.subject} [triasique]{.subject} couché au sol, de forme triangulaire, à grand axe Est-Ouest, mesurant 3,60m de long, 1,04m à sa base, épais de 0,40m à sa base et 0,24m au sommet. Son séjour à cet endroit doit remonter à fort longtemps, cette roche présentant des signes d'un délitement avancé. On ne note pas de traces d'épannelage, la forme naturelle ayant pu convenir d'emblée.

#### Historique

Monolithe découvert par [Velches P.]{.creator} en [mai 2011]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Ibantelli]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 470m.

Il est situé sur un vaste plateau, à 8m au Nord-Ouest de la BF 43.

#### Description

Tumulus de terre de 6m de diamètre, et 0,20m de haut ; la piste passe en son milieu.

#### Historique

Tumulus découvert par [Meyrat F.]{.creator} en [mars 2012]{.date} (et [L. Millian]{.creator} en [2004]{.date}).

</section>

<section class="monument">

### [Sare]{.spatial} - [Ibantelli]{.coverage} Sud 2 - [Dolmen]{.type}

#### Localisation

Altitude 611m.

Ce monument est érigé à environ 200 m à l'Est du sommet du mont Ibantelli ; le col de Lizarietta se voit en contre bas au Sud. Il apparaît au milieu d'un vaste éboulis dû au délitement par les intempéries d'un filon de grès. Le terrain à cet endroit est en légère pente vers le Sud-Ouest.

#### Description

Ce monument n'est pas évident. Si la réalité d'un monument funéraire à inhumation ne fait que peu de doutes, il reste difficile à décrire dans son état actuel.

Il semblerait que l'on puisse voire une [chambre funéraire]{.subject} orientée Sud-Ouest Nord-Est mesurant environ 2,70m ([?]{.douteux}) de long et 1,50m de large, délimitée par 4 dalles et 2 autres dalles de [couverture]{.subject}, ayant légèrement glissé dans le sens de la pente. La partie la plus évidente de cette [chambre funéraire]{.subject} est formée par la jonction de 2 dalles, à angle droit, dans l'angle Nord de cette chambre, dalles de grès de volume modeste n'émergeant du sol que d'une dizaine de centimètres ; la plus au Sud-Ouest (dalle 1), mesure 0,65m de long et 0,11m d'épaisseur ; celle qui lui est perpendiculaire au Sud-Ouest (dalle 2) mesure 1,12m de long et 0,16m d'épaisseur. Une troisième dalle (dalle 3), bien modeste, est visible un peu décalée vers le Nord-Est et mesure 0,70m de long et 0,16m d'épaisseur. La dernière visible (dalle 4) formant la limite extérieure Sud-Est de cette [chambre funéraire]{.subject}, est sensiblement perpendiculaire à la paroi Nord-Est et mesure environ 1,80m de long.

Les autres constituants des montants de ce dolmen (ou ciste dolmenique ?) ne sont pas visibles, cachés dans le sol, ou sous les dalles de [couverture]{.subject}, ou disparus....

Les 2 dalles de couverture : la plus au Nord-Est (dalle A) mesure 1,57m de long et 0,84m de large, avec une épaisseur de 0,42m ; la seconde (dalle B), plus au Sud-Ouest, mesure 1,70m de long, 0,92m de large et 0,15m d'épaisseur.

Un tumulus d'environ 5m de diamètre nous paraît peu probable.

#### Historique

Monument découvert par [I. Txintxuretta]{.creator} en [septembre 2019]{.date} et revu par [F. Meyrat]{.creator} en [décembre 2019]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Ibantelli]{.coverage} Nord - [Monolithe]{.type}

#### Localisation

Altitude 638m.

Ce monolithe couché se trouve dans un col herbeux séparant au nord le sommet de l'Ibantelli d'une importante barre rocheuse au sud (alt. 655m).. Ce col est à la fois un pâturage et un lieu de passage.

#### Description

Belle dalle de grès [triasique]{.subject} en forme de pain de sucre légèrement rétréci en son milieu, à sommet Nord Nord-Est, couchée sur un sol en légère pente vers le nord ; elle mesure 4,23m de long, 1,75m dans sa plus grande largeur et sa base 1,16m. Son épaisseur moyenne est de 0,45m. Enfin cette dalle paraît bien présenter des signes d'épannelage sur tout son pourtour.

#### Historique

Monolithe découvert en [janvier 2020]{.date} par [F. Meyrat]{.creator}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Ibantelli]{.coverage} 348 - [Tumulus]{.type} ([?]{.douteux})

#### Localisation

Altitude : 348m.

Ce « monument » est situé à l'extrémité Est d'une longue croupe qui s'étend au flanc Est de l'Ibantelli et domine directement, au Nord, la D 306 à son arrivée au col de Lizarrieta.

#### Description

Un assemblage d'une trentaine de pierres est situé au Sud-Est du poste de chasse n°20, et tangent à ce dernier ; ces éléments pierreux, apparaissant au ras du sol et semblent affecter une disposition plus ou moins circulaire une surface de 2m de diamètre environ, (ils prédominent dans le secteur Nord-Ouest), sans aucun relief. Il n'est donc pas possible de parler d'un tumulus, mais peut-être d'évoquer un ancien tumulus pierreux qui aurait été nivelé par l'activité humaine toute proche... À noter, à 2m à l'Est Sud-Ouest, les vestiges d'un ancien foyer dont les pierres disposées en **U** sont encore bien visibles.

#### Historique

Ce « cas particulier » a été découvert par [F. Meyrat]{.creator} en [mars 2019]{.date}. Nous le publions avec les plus extrêmes réserves.

</section>

<section class="monument">

### [Sare]{.spatial} - [Iratzeburua]{.coverage} - Grande [dalle couchée]{.type} ([?]{.douteux})

#### Localisation

Situé à une cinquantaine de mètres au Nord-Est de *Sayberri - Cromlechs (?)* Altitude : 340m.

#### Description

Monolithe de [grés]{.subject} local, couché sur le sol, en forme de pain de sucre, mesurant 4,40m de long, 1,60m de large et 0,25m d'épaisseur en moyenne. Il ne présente pas, semble-t-il de traces d'épannelage (?).

#### Historique

Monolithe découvert par [J. Blot]{.creator} en [mai 2009]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Ithunarria]{.coverage} - [Monolithe]{.type}

#### Localisation

Altitude : 441m.

Cette pierre plantée se dresse sur la colline qui domine, à l'Ouest, le col d'Olhayn.

#### Description

Elle est située dans une sorte de couloir rocheux, orientée Nord-Sud, long d'une soixantaine de mètres, délimité par deux barres rocheuses Est et Ouest formées de volumineux amas de poudingue. Cette pierre est plus proche de la paroi Ouest (3m) que de la paroi Est (11m).

Ce bloc de [grés]{.subject}, en forme de dalle parallélépipédique, d'épaisseur moyenne d'environ 0,20m, est orienté suivant un axe Nord-Sud. Il présente un bord Sud haut de 0,95m et un bord Nord de 0,60m ; cette dissymétrie aboutissant à un sommet incliné vers le Nord. La base est large de 0,55m, et l'ensemble va en se rétrécissant légèrement vers le haut (0,40m).

Cette dalle a été volontairement plantée là, comme en témoignent de nombreux blocs rocheux de calage à sa base.

#### Historique

Cette dalle est signalée par [M. Duvert]{.creator} (communication personnelle).

Identification de cette dalle sur le terrain et photos par [F. Meyrat]{.creator} en [novembre 2019]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Kondendiagakolepoa]{.coverage} - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 315m.

#### Description

Tertre terreux de 5m de diamètre et 0,40m de haut.

#### Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Larria]{.coverage} 4 - [Dolmen]{.type}

#### Localisation

Carte 1245 Est Espelette.

Situés au Nord-Ouest du mont Sayberry, les *Dolmens Larria 1*, *2* et *3* ont été décrits initialement en 1946 [@barandiaranCatalogueStationsPrehistoriques1946, p.34] et le *Dolmen Larria 1 Nord* l'a été en 1967 [@chauchatSeptDolmensNouveaux1966, p.83].

Le dolmen Larria 4 est situé à 40m à l'Est Sud-Est du *n°3*.

#### Description

Tumulus pierreux tout à fait semblable à celui du n°3, formé de petits blocs de [grés]{.subject} rose. Il mesure 8m de diamètre et 0,40m de haut, et il est adossé, au Sud-Ouest, à une crête rocheuse. Ce tumulus, bien qu'érigé au milieu de nombreux blocs de grès épars autour de lui et sur un terrain en très légère pente, est cependant bien identifiable. Il n'y a pas de [chambre funéraire]{.subject} visible, ni de dépression centrale ; peut-être s'agit-il d'un monument vierge ?.

#### Historique

Monument découvert en [septembre 1970]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Lezia]{.coverage} - [Dolmen]{.type}

#### Localisation

Carte 1245 Est Espelette.

Altitude : 210m.

Il est situé sur une petite colline qui domine immédiatement, au Sud, la route qui arrive aux grottes de Sare, à environ 300m à vol d'oiseau au Nord Nord-Ouest du parking de celles-ci.

Il est construit à 20m au Nord d'un amoncellement de gros blocs de poudingue, visible de loin.

#### Description

Tumulus pierreux de 10m de diamètre et 0,60m de haut, formé de blocs de [grés]{.subject} rose local de la taille d'un pavé. De la [chambre funéraire]{.subject}, orientée Ouest-Est, et qui devait initialement mesurer environ 2m x 1,10m, il ne reste que deux belles dalles de grès rose en place, apparaissant dans une profonde dépression centrale. Le montant Nord mesure 1,70m de long, 0,70m de haut et 0,20m d'épaisseur ; il est incliné vers le Nord et présente des traces d'épannelage à son bord supérieur. Le montant Sud, aux bords arrondis, ne présente pas de traces de régularisation ; il est légèrement plus long que le précédent : environ 2m, et 0,30m de haut avec une épaisseur de 0,25m, et très fortement incliné vers le Nord. Ceci est dû à la pression exercée sur sa face externe par le tumulus pierreux qui la recouvre entièrement, et qui est lui-même édifié sur un terrain en pente vers le Nord. On note sur le bord Sud-Est du tumulus deux gros fragments de dalle, dont un partiellement enfoui ; ils pourraient représenter tout ou partie d'un montant ou de la table de [couverture]{.subject}. De même, il existe à 7m à l'Est Nord-Est du tumulus un autre important fragment de dalle, dont un bord est épannelé ; est-ce une partie d'un montant ?

#### Historique

Il nous a été signalé en [mai 1999]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Lezia]{.coverage} - [Dalle plantée]{.type}

#### Localisation

Altitude : 300m.

#### Description

Elle est verticalement plantée dans le sol, mesure 1,38m à sa base, 1,10m de haut, et 0,25m d'épaisseur. Il semblerait qu'on puisse distinguer, à sa base, un tumulus pierreux de 6m de diamètre.

#### Historique

Dalle découverte par [Blot J.]{.creator} en [mars 2012]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Lizuniaga]{.coverage} - [Tumulus-cromlechs]{.type}

Nous décrivons ici ces deux monuments, qui, bien que ne faisant pas partie de la commune de Sare, mais de celle de Vera, complètent, avec ceux du col de Kondedikolepoa, le contexte archéologique de l'antique voie de transhumance reliant Sare à Vera. Voir aussi mon article [@blotMonolitheFaagueTable2009, p.23].

#### Localisation

Altitude : 240m.

Ces deux monuments se trouvent très près de la frontière, à quelques mètres au Sud-Est de la BF 35, à environ 200m au Nord-Ouest de la venta-restaurant, et à droite du virage qu'effectue le chemin empierré qui, partant de cette venta, se rend à un ensemble de maisons proches.

#### Description

-   Le *premier* tumulus-cromlech mesure environ 4,70m de diamètre et 0,40m de haut ; au centre existe une dépression peu profonde de 2m de diamètre ; de nombreuses pierres blanches apparaissent à la périphérie de celle-ci, et sur le tumulus lui-même.

-   Il semble bien qu'il existe un *deuxième* monument de même type, tangent à l'Est du précédent, de même hauteur et de 3m de diamètre environ.

#### Historique

Monuments découverts, le premier par [I. Gaztelu]{.creator} en [mars 2001]{.date}, le second par [A. Martinez]{.creator} en [2009]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Lizuniaga]{.coverage} 3 - [Tumulus]{.type}

#### Localisation

Altitude : 360m.

Dans un petit col, sur un replat à 5m au Sud-Est de la piste.

#### Description

Petit tumulus pierreux de 3,50m de diamètre et 0,30m de haut.

#### Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Lizuniaga]{.coverage} 4 - [Tumulus]{.type}

#### Localisation

Altitude : 304m.

Ce monument est sur un replat, à une centaine de mètres au Sud Sud-Est de la BF 34, et à 9m à l'Est Sud-Est de la piste qui monte vers Larraun.

#### Description

Tumulus pierreux de 3,20m de diamètre, en forme de galette circulaire aplatie, constitué de blocs de calcaire blanc qui affleurent la surface du sol, de la taille d'un poing ou d'un pavé.

#### Historique

Monument découvert par [P. Badiola]{.creator} en [mars 2015]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Nabalatz]{.coverage} 2 - [Tumulus]{.type}

#### Localisation

Altitude : 505m.

Il est situé dans le premier col à l'Est de celui où se trouve *Nabalatz T1* (et la BF 50), sur terrain plat, à 50m à l'Est du point déclive de son col, et tangent à la piste ascendante.

#### Description

Tumulus de terre de 4m de diamètre et 0,40m de haut, avec une légère dépression centrale.

#### Historique

Monument découvert par [Blot J.]{.creator} en [mars 2012]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Nabalatz]{.coverage} - [Dalle plantée]{.type}

#### Localisation

Altitude : 490m.

Elle est sur la droite d'une piste ascendante peu marquée qui rejoint directement la route forestière des Palombières à la ligne de crête, des cote 465 à 505.

#### Description

Dalle de schiste [grés]{.subject}eux de forme triangulaire plantée dans le sol, recouverte de mousse, inclinée vers l'Est, mesurant 1,30m de haut, 1m à sa base et 0,10m d'épaisseur en moyenne. On note que le gel a détaché un fragment de la partie inférieure de cette dalle, mais qui lui est cependant resté parallèle ; fragment 0,60m de haut et 0,47m de large.

#### Historique

Dalle découverte par [Colette Blot]{.creator} en [mars 2012]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Nabarlaz]{.coverage} (ou Nayalaz) - [Tumulus]{.type}

#### Localisation

Carte 1245 Est Espelette.

Altitude : 430m.

Il est situé dans le col de ce nom, à 14m au Sud-Est de la BF 50, donc en territoire navarrais.

#### Description

Tumulus de terre, à sommet aplati, de 5m de diamètre et 0,60m de haut.

#### Historique

Monument découvert en [mars 1970]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Olhayn]{.coverage} - [Tumulus]{.type}

#### Localisation

Même carte.

Altitude : 350m.

Il est situé au milieu d'un col, sur un terrain en très légère pente, à l'Ouest du mont du même nom, et de la chapelle en ruine édifiée à son sommet.

#### Description

Tumulus pierreux, formé de petits galets et de débris de plaquettes de [grés]{.subject} rose local, il mesure 6m de diamètre et 0,40m de haut.

#### Historique

Monument découvert en [novembre 1974]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Palombières]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 465m.

Situé sur un replat, à un croisement de pistes forestières et pastorales.

#### Description

Tumulus de terre mesurant environ 8m de diamètre et 0,80m à 1m de haut - il présente une petite dépression au voisinage de son sommet.

#### Historique

Monument découvert par [J. Eloségui]{.creator} et « re-découvert » par [Blot J.]{.creator} en [mai 1984]{.date}. Nous publions ici ce tertre, car tous les chercheurs ou amateurs de ces monuments n'ont pas obligatoirement le temps ou la possibilité de lire l'ensemble des publications faites sur ce thème...

</section>

<section class="monument">

### [Sare]{.spatial} - [Sayberri]{.coverage} - [Cromlechs]{.type} ([?]{.douteux})

#### Localisation

Altitude : 480m.

Situés très exactement au milieu du col de Sayberri, au pied du mont du même nom, (506 m altitude) - à 1 kilomètre au Sud-Est des grottes de Sare.

#### Description

On note une dizaine de blocs rocheux, délimitant un premier cercle approximatif de 3,50m de diamètre. À 4m au Nord, peut-être existe-t-il un deuxième cercle de 3,50m de diamètre lui aussi, délimité par une quinzaine de pierres de moindre volume, au ras du sol. Enfin, à 5m à l'Ouest de ce second vestige, il pourrait y avoir deux cercles tangents, mesurant chacun 3m de diamètre et aux éléments là encore difficilement visibles.

#### Historique

Ces monuments, douteux, nous ont été signalés par [J. Régnier]{.creator} en [mai 2009]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Sayberri]{.coverage} 1 - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 485m.

Situé à 5m environ au Sud des ruines d'un cayolar.

#### Description

Tertre de terre (ou plate-forme ?) asymétrique, à pente orientée vers le Sud, mesurant 7m x 12m. La finalité n'est pas évidente...

#### Historique

Monument découvert par [Blot J.]{.creator} en [décembre 2013]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Sualar]{.coverage} 1 - [Pierre couchée]{.type}

#### Localisation

Altitude : 520m.

Elle se trouve à quelques mètres à droite de la piste qui monte de Gaztenbakarre au plateau d'Altsaan.

#### Description

Bloc de [grés]{.subject} local, de forme parallélépipédique, régulière, orientée selon un axe Sud-Est Nord-Ouest, mesurant 2,50m de long, 0,80m de large et d'une épaisseur de 0,40m. Il semble bien que des traces d'épannelage puissent être notées réparties en de nombreux endroits sur la périphérie de cette pierre.

#### Historique

Pierre découverte par [P. Badiola]{.creator} en [mai 2011]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Sualar]{.coverage} 2 - [Pierre couchée]{.type} ([?]{.douteux})

#### Localisation

Altitude : 515m.

Il est à une quinzaine de mètres au Nord-Est de la piste pastorale qui monte de Gaztenbakarre.

#### Description

Bloc de [grés]{.subject} local de forme parallélépipédique, allongé dans le sens de la pente (vers le Nord-Est). Il mesure 2,10m de long, 0,62m à son extrémité Nord-Est, 0,70m à son extrémité Sud-Ouest et 0,85m dans sa plus grande largeur. Son épaisseur est d'une vingtaine de centimètres.

Il n'y a pas de traces d'épannelage visibles. Monument douteux.

#### Historique

Pierre découverte par [Blot J.]{.creator} en [mai 2011]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Tomba]{.coverage} - [Cromlech]{.type} ([?]{.douteux})

#### Localisation

Altitude : 476m.

#### Description

Petit cercle de 1m de diamètre, légèrement surélevé, constitué d'une quinzaine de pierres particulièrement visibles dans la partie Ouest du cercle.

#### Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2012]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Tomba]{.coverage} 1 - Tumulus ([?]{.douteux})

#### Localisation

Altitude : 470m.

Il est à 2m au Nord de la piste (herbeuse) qui se rend vers le *Dolmen de Tomba*, vers la partie haute d'une petite colline et à 15m au Sud du pointement rocheux qui en marque le sommet.

#### Description

Tumulus circulaire, constitué de pierres enfouies dans le sol, qui devait former un tumulus en galette aplatie de 4m de diamètre environ et 0,20m de haut. ; une dépression circulaire de 1m de diamètre, remplie de pierres, (reste d'une probable fouille clandestine) se voit au centre de ce tumulus dont seule la partie Est reste encore la plus visible.

#### Historique

Tumulus découvert par [Blot J.]{.creator} en [octobre 2012]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Tomba]{.coverage} 2 - Tumulus ([?]{.douteux})

#### Localisation

Altitude : 471m.

Ce tumulus se trouve à une quinzaine de mètres au Nord-Ouest de *Tomba 1 - Tumulus (?)*.

#### Description

Tumulus pierreux là encore très détérioré par une probable fouille ancienne : de sa forme circulaire primitive il ne reste en effet qu'un bourrelet pierreux de 1m de large environ, qui représente la partie Est de ce tumulus. Dans la dépression centrale due à la fouille on distingue 3 élément de quartzite qui pourraient faire partie d'une ciste centrale mesurant 0,45m x 0,40m.

#### Historique

Tumulus découvert par [Blot J.]{.creator} en [novembre 2012]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Trois Fontaines]{.coverage} 1 - [Tumulus]{.type} ou [Tertre d'habitat]{.type} ([?]{.douteux})

#### Localisation

Altitude : 500m.

Il est à 30m au Nord-Est de la lisière du petit bois des Trois fontaines, à 150m à l'Ouest de *Urkila 3 - Tumulus*, et à 80m environ à l'Ouest de la voie ferrée ascendante. Deux autres tumulus se trouvent plus à l'Ouest, sur la commune d'Ascain.

#### Description

Tumulus de terre et de pierres, de faible hauteur (0,20m), érigé sur terrain plat, d'un diamètre de 8m environ. Au centre une dépression de 3m de large et 0,70m de profondeur, en partie comblée de grosses pierres. On note une dalle plantée à 2m au Nord-Est.

#### Historique

Tumulus découvert par [Meyrat F.]{.creator} en [avril 2012]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Trois Fontaines]{.coverage} 1 - [Dalle plantée]{.type}

#### Localisation

À 2m au Nord-Est du tumulus *Trois Fontaines n°1*.

#### Description

Dalle de [grés]{.subject} local, parallélépipédique, verticalement plantée dans le sol, mesurant 0,84m de haut, 9 centimètres d'épaisseur, 0,30m à la base et 0,27m dans sa partie haute.

#### Historique

Dalle trouvée par [Meyrat F.]{.creator} en [avril 2012]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Trois Fontaines]{.coverage} 1 - [Dolmen]{.type}

#### Localisation

Altitude : 540m.

#### Description

Trois dalles sont alignées, plantées verticalement dans un tumulus pierreux de 0,15m de haut environ. Elles mesurent 0,60m de long chacune et 0,20m de haut ; d'autres apparaissent plus ou moins enfouies à l'extrémité Sud-Ouest de cet alignement ; enfin une autre dalle, au Nord-Est, elle aussi verticalement plantée, est à angle droit avec les précédentes, (contre un arbre poussant à cet endroit), et semble déterminer, avec elles, les parois d'une [chambre funéraire]{.subject} à grand axe Nord-Ouest Sud-Est ([?]{.douteux}).

#### Historique

Monument trouvé pat [Meyrat F.]{.creator} en [avril 2012]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Trois Fontaines]{.coverage} 2 - [Dolmen]{.type}

#### Localisation

Il se trouve à 6m à l'Ouest Nord-Ouest de *Trois Fontaines 1 - Dolmen*.

#### Description

Un ensemble de 4 dalles délimite une [chambre funéraire]{.subject} à grand axe Nord-Ouest Sud-Est de 2,80m de long et 1m de large environ.

Les dalles sont toutes couchées, éversées. Celles du côté Nord-Est mesurent respectivement 0,90m et 0,66m ; celles du côté Sud-Ouest, mesurent 0,65m et 0,95m de long. Un arbre pousse à l'extrémité Nord-Ouest du monument.

#### Historique

Monument découvert par [Meyrat F.]{.creator} en [avril 2012]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Uhardiko Etxola]{.coverage} - [Pierre couchée]{.type}

#### Localisation

Altitude : 345m.

Elle est située à 1,50m au Nord de la piste et à 3m au Nord des ruines d'un ancien habitat.

#### Description

Dalle de [grés]{.subject} grossièrement parallélépipédique, allongée selon un axe Ouest Nord-Ouest, Est Sud-Est. Elle mesure 2,12m de long, 0,59m de large et 0,36m d'épaisseur en moyenne. Son pourtour présente de nombreuses traces d'épannelage.

#### Historique

Pierre découverte par [Badiola P.]{.creator} en [décembre 2011]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Urio]{.coverage} - [Tumulus]{.type} ([?]{.douteux})

#### Localisation

Situé dans une fougeraie qui domine le rio Urio, ce petit tumulus pierreux mesure 2,45m de diamètre et 0,20m de haut avec un centre légèrement excavé.

#### Description

Il semblerait qu'il puisse faire partie d'une structure rectangulaire mesurant 7m x 3,50m, balisée par de gros galets.

À 16m de distance, au Sud-Ouest, il y aurait peut-être un cercle de 5m de diamètre constitué de 16 pierres.

#### Historique

Ces deux monuments - douteux - nous ont été signalés par [Duvert M.]{.creator} en [octobre 2011]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Urkila]{.coverage} 1 - [Monolithe]{.type}

#### Localisation

Altitude : 555m.

Cette dalle gît sur la gauche de la piste pastorale qui se rend à Urkilepoa, à environ 150m du site où se croisent les funiculaires.

#### Description

Dalle de [grés]{.subject} rose, mesurant 3,10m de long, 1,70m de large et 0,30m à 0,40m d'épaisseur. Quelques traces d'épannelage sont visibles à son extrémité Est, visiblement arrondie.

#### Historique

Monolithe découvert par [Blot J.]{.creator} en [juin 1982]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Urkila]{.coverage} 2 - [Monolithe]{.type}

#### Localisation

Altitude : 625m.

Elle est isolée sur un sol en pente marquée vers le Nord et à une cinquantaine de mètres à l'Est de la voie ferrée qui monte à Larraun.

#### Description

Beau bloc de [grés]{.subject} presque rectangulaire, dont les 4 bords sont parfaitement épannelés (travail récent ?). Il mesure 2,85m de long, 1,12m dans sa plus grande largeur, 0,96m dans la plus petite, et 0,26m d'épaisseur.

#### Historique

Monolithe découvert par [P. Badiola]{.creator} en [décembre 2011]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Urkila]{.coverage} 3 - [Tumulus]{.type}

#### Localisation

Altitude : 543m.

Il est situé à quelques mètres à l'Est de la voie ferrée qui monte à Larrun.

#### Description

Il est très semblable aux tumulus 1 et 2 que nous avons déjà décrits [@blotInventaireMonumentsProtohistoriques2010, p. 31] : tumulus de terre, à sommet aplani, de 5m à 6m de diamètre et plus haut à l'Est qu'à l'Ouest, cette dissymétrie évoque un tertre d'habitat, ce que nous ne pensons pas qu'il soit.

#### Historique

Tumulus découvert par [Meyrat F.]{.creator} en [janvier 2012]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Urkila]{.coverage} n°1 - [Tumulus]{.type} ([?]{.douteux})

#### Localisation

Altitude : 590m.

#### Description

« Tumulus » situé à une quinzaine de mètres à l'Est de la voie ferrée du train à crémaillère, formé de terre et de pierres. Sensiblement circulaire, il est érigé sur un terrain en pente légère, incliné vers le Nord. Mesure 6m de diamètre et 1,50m de haut ; il a plus l'aspect d'un tertre d'habitat (que nous ne pensons pas qu'il soit), que d'un tumulus vrai ou dolménique ; tas de cailloux de construction de la voie ferrée ?

#### Historique

Découvert par [J. Blot]{.creator} en [mars 2010]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Urkila]{.coverage} n°2 - [Tumulus]{.type} ([?]{.douteux})

#### Localisation

Altitude : 650m.

#### Description

Il est situé dans la concavité de la voie ferrée, à 30m à l'Ouest de celle-ci, sur terrain en légère pente. Ce tumulus de terre et de pierres, ayant sensiblement la même forme que le précédent, mesure 5m de diamètre et 1m de haut environ ; il nous inspire les mêmes commentaires que le « tumulus » précédent, quant à sa nature....

#### Historique

Découvert par [I. Gaztelu]{.creator} en [janvier 1988]{.date} (dénommé dolmen).

</section>

<section class="monument">

### [Sare]{.spatial} - [Urkila]{.coverage} - [Dalle plantée]{.type} ([?]{.douteux})

#### Localisation

Altitude : 540m.

Elle est à l'Ouest de la piste qui descend d'Athekaleun, et à 150m au Nord Nord-Est de *Urkila n°1 - Tumulus (?)*.

#### Description

Dalle verticalement plantée dans le sol, de 0,80m de haut, 1,30m à sa base et 0,10m d'épaisseur.

#### Historique

Découverte par [J. Blot]{.creator} en [mars 2010]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Uzkaïne]{.coverage} - [Pierre plantée]{.type} ([?]{.douteux})

Nous avons décrit dans le **Tome 3** de notre Inventaire [@blotInventaireMonumentsProtohistoriques2011, p.25], un probable tumulus situé à la confluence des 3 communes de Sare, Saint-Pée et Ascain, sur lequel est érigée une borne ; celle-ci mesure 1,15m de haut, 0,57m de long et 0,46m de large à sa base ; bien que la date 1955 soit gravée à son sommet, avec les traits de délimitation des trois communes, nous émettons l'hypothèse qu'elle soit bien plus ancienne que cette date... ce qui n'exclut nullement l'existence d'un tumulus sous-jacent.

Altitude : 223m.

</section>

<section class="monument">

### [Sare]{.spatial} - [Uzkaïne]{.coverage} - [Tumulus]{.type} ([?]{.douteux})

#### Localisation

Altitude : 223m.

Ce probable tumulus se situe au point de rencontre des limites des trois communes de Sare, Saint-Pée-sur-Nivelle, et Ascain.

#### Description

Une borne plantée en son milieu (une croix est gravée à son sommet ainsi que la date : 1995), matérialise ces délimitations administratives. Ce tumulus de terre et de pierres, mesure 8m de diamètre et environ une quinzaine de centimètres de haut. Nous ne pensons pas qu'il s'agisse des traces des travaux de terrassement nécessités pour planter cette borne, compte tenu de ses dimensions et de sa structure ; par ailleurs il est sur un sol plat, dans un emplacement dominant, au bord de la piste pastorale, en ce lieu privilégié qui a même été choisi pour y ériger un calvaire, à 5m au Sud-Ouest.

#### Historique

Monument découvert par [J. Blot]{.creator} en [décembre 2010]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Uzkizelaikoharri]{.coverage} - [Monolithe]{.type}

#### Localisation

Altitude : 510m.

Il se trouve au pied d'une colline, sur un terrain en pente douce vers le Sud.

#### Description

Dalle de [grés]{.subject} de forme parallélépipédique allongée au sol suivant un axe Nord-Sud, mesurant 1,46m de long, 0,55m de large à sa base Sud, 0,32m à son sommet Nord, plus étroit ; son épaisseur varie de 8 à 18 centimètres. Elle présente des traces d'épannelage sur tout son pourtour.

#### Historique

Monolithe découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Uzkizelaikoharri]{.coverage} - [« Rempart » mégalithique]{.type}

#### Localisation

Altitude : 553m.

Cette colline surplombe la piste qui monte au col d'Olhain.

#### Description

On note, barrant le sommet ci-dessus un « alignement » de dalles « mégalithiques » au sens littéral du terme (certaines atteignent plus de 2m de haut, 2m de large et 0,80m d'épaisseur), c'est pourquoi nous le signalons ici. Ces dalles ne sont pas fichées en terre mais simplement posées et calées sur d'autres dalles ou blocs, ce qui leur donne un aspect incliné, soit vers l'amont soit vers l'aval de la pente. Certaines gisent au sol, de toute façon elles ne forment pas un « obstacle » continu.

#### Historique

Ce remarquable ensemble a été noté par [Louis de Buffières]{.creator} en [février 1992]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Xabaloa]{.coverage} - [Dolmen]{.type}

#### Localisation

Carte 1245 Est Espelette.

Altitude : 170m.

Nous ne voulons pas terminer ce chapitre sur Sare sans évoquer un monument actuellement disparu, et que nous avons eu beaucoup de mal à localiser.

Ce monument, était érigé sur une petite colline située au flanc Nord-Est du mont Ibantelli, qui domine, au Sud, le ruisseau Xabaloa ; il était à 100m à l'Ouest au-dessus de la route qui mène à Etchalar, sur la gauche du chemin rural reliant cette route à Xabaloa. Ce monument a été découvert en [1944]{.date} [@barandiaranCronicaPrehistoriaPirineo1951a, p. 243].

#### Description

Tumulus pierreux de 6m de diamètre et 0,60m de haut, au centre duquel 2 dalles verticales laissaient entrevoir la forme rectangulaire de la [chambre funéraire]{.subject}, environ 1,50m x 1m et 1m de profondeur, orientée Est-Ouest. La dalle du côté Sud était marquée d'une croix gravée sur la face externe, tandis que l'autre avait un rhombe. Ces dalles ont été enlevées pour la construction de la maison Mailuen Bordaberri. Il existe, très proche, sur cette colline, une borde neuve appelée *Axuria*.

</section>

<section class="monument">

### [Sare]{.spatial} - [Xantakoenea]{.coverage} - [Monolithe]{.type}

#### Localisation

Altitude : 190m.

Il gît sur le bord du petit chemin qui part de Xantakoenea (altitude : 162m) et rejoint plus haut le GR 10 ; il ne paraît pas « en place » mais « en cours de déplacement ».

#### Description

Bloc de [grés]{.subject} allongé à grand axe Sud-Ouest Nord-Est (extrémité la plus étroite au Sud-Ouest), mesurant 3,15m de long et 0,90m de large au maximum. Son épaisseur varie de 0,45m à son extrémité Nord-Est, à 0,16m au Sud-Ouest. Il semble y avoir quelques traces d'épannelage aux deux extrémités.

#### Historique

Monolithe découvert par [Meyrat F.]{.creator} en [mai 2011]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Xarita]{.coverage} 1 - [Tumulus]{.type}

#### Description

Petit tumulus circulaire de 4m de diamètre et 0,40m de haut, formé de terre et de pierres ; il paraît indépendant des éléments environnants.

#### Historique

Tumulus découvert par [Blot C.]{.creator} en [2013]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Xarita]{.coverage} - [Monolithe]{.type}

#### Localisation

Altitude : 405m.

Il faut emprunter la piste bien visible qui part sur la gauche de la route, à 250m environ du col de Lizarrieta (en direction de Sare). Le monolithe gît sur le bord Est de la piste à environ 900m du point de départ. Cette piste, étroite en général, longe le flanc Est de l'Ibantelli, et ce monolithe est situé à un élargissement particulièrement notable du terrain, bien plat à cet endroit.

#### Description

Il se présente sous la forme d'une belle dalle de [grés]{.subject} [triasique]{.subject}, couchée au sol, de forme triangulaire à sommet Sud Sud-Ouest, mesurant 2,24m de long, 0,98m à la base, et 0,94m en son milieu. Son épaisseur est de 0,35m à 0,40m à son bord Est, de 0,20m à sa base, qui présente des traces de délitement probablement naturel ; l'épaisseur du bord Ouest est difficilement appréciable, étant enfoui dans le sol. Il ne semble pas y avoir de traces d'épannelage ; toutefois cette dalle isolée, loin de tout éboulement pierreux ou de tout autre fragment rocheux, attire l'attention en cet emplacement.

#### Historique

Monolithe découvert par [Luis Millan]{.creator} en [janvier 1990]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Xarita]{.coverage} 3 - [Dolmen]{.type}

#### Localisation

Il est situé sur une croupe du flanc Sud de l'Ibantelli, et domine (à une centaine de mètres au Nord) l'ancien sentier minier horizontal qui court sur ce flanc. Ce monument a été érigé sur un terrain en pente vers le Sud, rendu horizontal par d'importants travaux de soutènement (apport de grosses pierres et de terre).

#### Description

On ne peut pas parler d'un tumulus, mais d'une aire circulaire horizontale de 7m de diamètre, dont la demi-circonférence, côté Sud, est soutenue par de nombreuses pierres de volume important (certaines de 0,90m x 0,60m) en deux ou 3 assises suivant les endroits. Ce soutènement se voit aussi, dans une moindre mesure, à l'Est comme à l'Ouest. Au Nord, seules quelques rares pierres balisent la circonférence.

Au centre de ce cercle apparaît la partie supérieure d'une belle dalle de 1,93m de long, 0,29m de large (à l'Ouest) et 0,15m (à l'Est) ; elle n'émerge du sol que de 0,15m. Elle semble bien former la paroi Sud d'une [chambre funéraire]{.subject} orientée Est-Ouest qui aurait eu 1m de large.

On note, à 2m au Nord-Ouest de cette dalle, une autre dalle couchée de 1,04m x 0,56 et 0,10m d'épaisseur qui aurait pu faire partie de la paroi Nord de la chambre ou de la [couverture]{.subject}.

#### Historique

Monument découvert par [L. Millan]{.creator} en [1990]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Xarita]{.coverage} 6 - [Dolmen]{.type} ([?]{.douteux})

#### Localisation

Altitude : 313m.

Ce monument est difficile à atteindre ; il faut tout d'abord emprunter la piste précédemment décrite à propos du *monolithe Xarita*, et c'est environ à 1000m du point de départ que l'on distingue, plus ou moins facilement suivant la végétation, un petit sentier qui se dirige vers la droite et le bas, en suivant une ligne de crête. Ce sentier aboutit à un petit ensellement où seules les pierres de ces 2 monuments (*Xarita 6* et *Xarita 7*) sont visibles.

#### Description

Ce monument, si cela en est un, se résume à une volumineuse dalle de grès [triasique]{.subject}, couchée sur le sol, et présentant, à son bord Ouest une série de pierres enfouies dans le sol, pouvant faire partie d'une [chambre funéraire]{.subject}. Toutefois, nous ne pouvons pas éliminer totalement l'hypothèse que cette dalle fasse même partie de *Xarita 7 - Dolmen (?)*.

La dalle de grès, de forme grossièrement rectangulaire, mesure 1,90m dans son plus grand axe Est-Ouest, 1,35m dans l'axe Nord-Sud, et en moyenne 0,37m d'épaisseur. À son extrémité Nord elle repose sur, semble-t-il, une deuxième dalle de moindre taille, qui pourrait n'être qu'un fragment séparé d'elle-même (délité par le gel ?). Cette deuxième dalle, épaisse en moyenne de 0,19m, mesure 1,13m de long et 0,88m de large.

La « [chambre funéraire]{.subject} », rectangulaire, orientée Est-Ouest, aurait aux environs de 3m de long et 1,50m de large et serait délimitée par une quinzaine de blocs ou de dalles de grès de volume variable, mais modéré, bien enfouis dans le sol. Ils sont surtout bien visibles en secteur Nord.

#### Historique

Monument découvert par [I. Txintxurreta]{.creator} le [19 janvier 2019]{.date}. (Monument douteux).

</section>

<section class="monument">

### [Sare]{.spatial} - [Xarita]{.coverage} 7- [Dolmen]{.type} ([?]{.douteux})

#### Localisation

Il est situé à 4m au Nord de *Xarita 6 - Dolmen (?)*.

#### Description

Monument se présentant sous la forme d'une série de 4 dalles de [grés]{.subject} [triasique]{.subject} enfoncées dans le sol selon un axe Est-Ouest (et plus ou moins éversées vers le Sud) et pouvant former la paroi Sud d'une [chambre funéraire]{.subject} de près de 5m de long et 1m de large, orientée Est-Ouest. Il semblerait, de plus, que cette longue [chambre funéraire]{.subject} soit divisée en deux par une dalle médiane, l'une à l'Est, de 2,10m de long et l'autre à l'Ouest, de 2,50m de long.

*La paroi Sud de la chambre* funéraire est constituée de 4 dalles. En allant de l'Est vers l'Ouest :

-   la première, presque verticale, et rectangulaire, mesure 0,67m x 0,48m et 0,14m d'épaisseur.

-   la seconde, nettement plus inclinée, mesure 1,67m x 0,80m et 0,20m d'épaisseur. ; elle est séparée de la première par 0,17m.

-   la troisième, complètement couchée sur le sol, mesure 1,18m x 0,96m et 0,16m d'épaisseur et est distante de 0,40 de la seconde.

-   la quatrième dalle, elle aussi couchée sur le sol, est nettement plus modeste et ne mesure que 0,75m x 0,57m et 0,12m d'épaisseur ; la distance entre elle et la troisième dalle est de 0,46m.

*La paroi Est de la chambre* pourrait être marquée par 2 dalles ; la première, couchée au sol, dans le prolongement de la paroi Sud, mais perpendiculaire à elle, mesurant 0,92m x 0,37m et 0,12m d'épaisseur. Une seconde dalle, rectangulaire, est située à l'extrémité Nord de la précédente et mesure 0,58m x 0,44m.

*Une dalle médiane*, semble indiquer une séparation de la [chambre funéraire]{.subject} en 2 parties et mesure 0,84m x 0,22m, et 0,10m d'épaisseur.

On ne distingue pas d'éléments pouvant figurer les parois Nord et Ouest de la [chambre funéraire]{.subject}.

On remarquera que toutes ces dalles, par leur présence et leur disposition évoquent une action anthropique, (de même que la volumineuse dalle de *Xarita 6*, qui ne paraît pas être venue là par l'effet du hasard ou d'une quelconque solifluxion, cet ensellement étant en effet dominé à l'Ouest et à l'Est par deux éminences de terrain).

#### Historique

Monument découvert par [I. Txintxurreta]{.creator} le [19 janvier 2019]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Xominen]{.coverage} 5 - [Dolmen]{.type}

#### Localisation

Altitude : 260m.

Il se trouve à 3m à l'Ouest du chemin qui part de la route goudronnée desservant le nouveau quartier construit au-dessus et au Sud du col de Saint-Ignace.

#### Description

On peut noter 4 dalles de [grés]{.subject} local dont 2 quasi verticales formant la paroi Sud d'une [chambre funéraire]{.subject} mesurant environ 1,90m x 0,90m, à grand axe Est Sud-Est, Ouest Nord-Ouest.

La plus grande dalle, au Sud-Est mesure 0,60m de long, autant de haut et 0,10m d'épaisseur ; tangente à elle, une seconde dalle la prolonge à l'Ouest, (0,50m de long, 0,24m de haut, 0,10m d'épaisseur). Deux autres dalles plus ou moins couchées au sol compètent la chambre au Nord-Est et au Nord-Ouest. Il semble qu'on puisse distinguer la présence d'un tumulus de faible hauteur, de 5m de diamètre environ, érigé sur un terrain en légère pente vers l'Est.

#### Historique

Monument découvert par [P. Badiola]{.creator} en [mai 2011]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Xominen]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 230m.

Il est sur le plateau où se trouvaient les *dolmens 1*, *2*, *3*, *4* et *5* détruits depuis [@blotNouveauxVestigesMegalithiques1971, p. 10]. On le voit à l'angle que forment la route goudronnée (privée) et une murette de séparation (à 2m à l'Est de la route et à 2m au de la murette).

#### Description

Tumulus de terre de 5m de diamètre, mais de faible hauteur (0,25m environ), présentant en son centre une dépression de 1m de diamètre et de quelques centimètres de profondeur.

On peut noter 3 pierres aux flancs du tumulus et 3 autres à sa périphérie.

#### Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Xominen]{.coverage} 1 et 2 - [Cromlechs]{.type} ([?]{.douteux})

Le premier se trouve à 2m au Nord de *T1*, et le second à 5m à l'Ouest de *T1* ; seraient délimités par quelques pierres au ras du sol - Douteux - ([Blot J.]{.creator}, [octobre 2011]{.date}).

</section>

<section class="monument">

### [Sare]{.spatial} - [Zuhamendi]{.coverage} 5 - [Tumulus]{.type} ([?]{.douteux})

#### Localisation

Altitude : 214m.

Ce probable tumulus est situé à une bonne centaine de mètres au Sud du *Tumulus de Bixustia*, (lui-même fort dégradé maintenant et coupé en deux par une haie). Il est en position dominante et à une vingtaine de mètres à l'Ouest d'une autre haie qui domine le chemin d'accès à ce secteur.

#### Description

Monument actuellement peu visible, ayant été en partie arasé par les labours de confection d'une prairie artificielle. Il mesure environ 11m de diamètre et une trentaine de centimètres de haut.

#### Historique

Ce monument nous a été signalé par [Martinez-Manteca A.]{.creator} en [novembre 2010]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Zuhamendi]{.coverage} 6 - [Tumulus]{.type} ([?]{.douteux})

#### Localisation

Altitude : 200m.

Ce probable tumulus est situé en terrain plat, et sur la ligne de crête de la prairie - dans la même prairie artificielle que celle où se trouve le tumulus *Zuhamendi 3* que nous avons fouillé en 1975.

#### Description

Il se présente sous la forme d'un léger relief circulaire de 6m de diamètre et d'une dizaine de centimètres de haut. Quelques pierres apparaissent en surface.

#### Historique

Tumulus trouvé par [J. Blot]{.creator} en [décembre 2010]{.date}.

</section>

## [Sare]{.spatial} - Les [cromlechs]{.type} de [Gastenbakarre]{.coverage}

<section class="monument">

### [Sare]{.spatial} - [Gastenbakarre]{.coverage} 1 - [Cromlech]{.type}

#### Localisation

Altitude : 265m.

#### Description

Une petite murette de dalles superposées, particulièrement visible dans le secteur Sud Sud-Est, délimite un cercle de 5m de diamètre dont le centre paraît avoir été légèrement excavé (fouille ancienne ?).

#### Historique

Monument découvert par [J. Blot]{.creator} en [mai 2009]{.date}.

Nous avons noté trois autres cercles au Sud-Est de ce dolmen, sur le replat déjà cité, au Nord-Ouest de la cote 271.

</section>

<section class="monument">

### [Sare]{.spatial} - [Gastenbakarre]{.coverage} 2 - [Cromlech]{.type}

Il est situé à 20m à l'Est Sud-Est de *Gastenbakarre n°4 - Dolmen*.

Quinze à vingt petites dalles de [grés]{.subject}, au ras du sol, délimitent un cercle de 2,70m de diamètre. Elles sont plus abondantes et plus grandes dans le secteur Sud-Ouest du cercle.

</section>

<section class="monument">

### [Sare]{.spatial} - [Gastenbakarre]{.coverage} 3 - [Cromlech]{.type}

Situé à 18m au Sud-Est du précédent. Une vingtaine de petites dalles blanches bien visibles, profondément enfoncées dans le sol, délimitent un cercle de 3,20m de diamètre. Elles sont plus abondantes dans le secteur Ouest du cercle. Une dalle rectangulaire, plus importante que les autres (0,46m x 0,46m) apparaît dans le secteur Sud-Est.

</section>

<section class="monument">

### [Sare]{.spatial} - [Gastenbakarre]{.coverage} 4 - [Cromlech]{.type}

Le plus au Sud-Est de ces trois derniers cercles, à 15mètres au Sud-Est du précédent.

Altitude : 265m.

Il mesure 3m de diamètre, et est délimité par une quarantaine de petites dalles de [grés]{.subject} gris, au ras du sol. En secteur Ouest du cercle, là où elles sont plus nombreuses, l'une d'elles est un peu plus saillante que les autres (0,20m de haut).

#### Historique

Un de ces quatre cercles nous a été signalé par [L. Millan San Emeterio]{.creator}, en [mars 2009]{.date} ; les trois autres par [J. Blot]{.creator} en [mai 2009]{.date}.

</section>

## [Sare]{.spatial} - Les 3 dolmens de [Gaztenbakarreko erreka]{.coverage}

Ces trois dolmens se trouvent en contre-bas de la piste qui part du menhir de Gaztenbakarre, en direction du Sud-Ouest et à environ 100m de celui-ci.

<section class="monument">

### [Sare]{.spatial} - [Gaztenbakarreko erreka]{.coverage} 1 - [Dolmen]{.type}

#### Localisation

Le plus proche de la piste, à 40m environ à l'Est sur un léger replat.

Altitude : 265m.

#### Description

Tumulus pierreux de dalles et de blocs de [grés]{.subject}, de 5m de diamètre environ, très peu élevé. De la [chambre funéraire]{.subject}, orientée Est-Ouest, il ne reste que deux dalles. La première, presque verticale, est une dalle trapézoïdale présentant des traces d'épannelage, de 0,94m de haut, mesurant 1,40m à la base, 0,40m au sommet, et 0,25m d'épaisseur à sa base. Le seconde qui lui est perpendiculaire à son extrémité Ouest, est de dimensions plus modestes : 0,70m à la base, 0,45m de haut et 0,10m d'épaisseur, légèrement inclinée en dedans.

</section>

<section class="monument">

### [Sare]{.spatial} - [Gaztenbakarreko erreka]{.coverage} 2 - [Dolmen]{.type}

#### Localisation

Altitude : 250m.

Situé à 40m environ à l'Est de *Gaztenbakarreko erreka 1 - Dolmen*, et légèrement plus bas (altitude 254m).

#### Description

Tumulus pierreux de 6m de diamètre et de faible hauteur, au centre duquel se voit une seule dalle légèrement inclinée, de forme rectangulaire et présentant des traces d'épannelage. Elle mesure 0,98m à sa base, 0,80m de haut et 0,16m d'épaisseur. L'axe de cette dalle est Nord-Sud ; il ne reste en place aucune autre dalle de la [chambre funéraire]{.subject}, orientée Est-Ouest, exceptée, gisant sur le flanc Sud-Est du tumulus une belle dalle de 1,20m de long et 0,90m de large.

</section>

<section class="monument">

### [Sare]{.spatial} - [Gaztenbakarreko erreka]{.coverage} 3 - [Dolmen]{.type}

#### Localisation

Il est situé à 10m à l'Est de *Gaztenbakarreko erreka 2 - Dolmen* et légèrement plus haut (255 m d'altitude).

#### Description

Tumulus bien visible de 5,70m de diamètre, formé de gros blocs de [grés]{.subject} gris. De la [chambre funéraire]{.subject} orientée Est Nord-Est, Ouest Sud-Ouest, il ne reste en place qu'une dalle presque verticale, de forme grossièrement rectangulaire, mesurant 0,75m à sa base, 0,80m de haut et 0,13m d'épaisseur. À côté d'elle, au Sud, gît un bloc de [grés]{.subject} qui a pu faire partie des parois de la chambre.

#### Historique

Ces 3 monuments ont été découverts par [I. Txintxuretta]{.creator} en [2009]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Gaztenbakarreko bidea]{.coverage} - [Dolmen]{.type}

#### Localisation

Il est situé à environ 200m au Sud du menhir, à une vingtaine de mètres en contre-bas de la piste.

Altitude : 265m.

#### Description

Erigé sur un terrain en légère pente, on note un tumulus pierreux de 5 m de diamètre et de faible hauteur, délimité par un très probable péristalithe.

De la [chambre funéraire]{.subject}, orientée Est Nord-Est, Ouest Sud-Ouest, il ne reste que la dalle Ouest, rectangulaire, mesurant 1,20m à sa base et 0,47m de haut, légèrement inclinée vers l'Est.

#### Historique

Monument découvert par [J. Blot]{.creator} en [mai 2009]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Iratzeburua]{.coverage} - [Dolmen]{.type}

#### Localisation

Il est situé à une centaine de mètres au-delà et au-dessus de la bergerie en ruine que longe la piste.

Altitude : 295m.

#### Description

Erigé sur un terrain en pente, on note un tumulus de faible hauteur d'un diamètre d'environ 6m constitué de terre et de quelques pierres peu visibles. Au centre, la [chambre funéraire]{.subject}, orientée Nord-Est Sud-Ouest, est nettement délimitée par un ensemble de dalles émergeant de quelques centimètres au-dessus du sol. Au Sud, une dalle légèrement inclinée vers le Nord, mesurant 0,47m à sa base, 0,40m au sommet, 0,38m de haut et 0,11m d'épaisseur. À l'Est, et perpendiculaire à la précédente, une seule dalle de 0,63m de long, 0,20m de haut et 0,6m d'épaisseur. Enfin à l'Ouest, deux dalles : l'une de 0,95m de long, 0,20m de haut et 0,10m d'épaisseur, l'autre, la chevauchant de quelques centimètres, mesure 0,60m de long, 0,32m de haut et 0,8m d'épaisseur.

#### Historique

Ce monument a été découvert par [I. Txintxuretta]{.creator} en [2009]{.date}.

</section>

<section class="monument">

### [Sare]{.spatial} - [Iratzeburua]{.coverage} - [Tumulus-cromlech]{.type}

#### Localisation

Situé à une centaine de mètres au Sud Sud-Ouest de *Iratzeburua - Dolmen*.

Altitude : 330m.

#### Description

Sur une très légère éminence, on note un cercle de 5m de diamètre, avec une légère dépression centrale, délimité par une couronne de nombreuse petites dalles de [grés]{.subject} plus ou moins superposées.

#### Historique

Monument re-découvert et précisé par [J. Blot]{.creator} en [mai 2009]{.date}.

</section>

## [Sare]{.spatial} - Cromlechs et tumulus de [Kondendiagako lepoa]{.coverage}

Nous ne voulons pas attribuer indûment à la commune de Sare ce qui revient à celle de Vera. Si nous traitons ici de ces monuments, c'est, d'une part, à cause de leur extrême proximité avec la ligne frontière, d'autre part du fait qu'ils se trouvent à l'arrivée, à ce col, d'une importante et antique piste pastorale reliant Sare à Vera.

#### Localisation

Au col Kondendiaga - où se trouve la BF 32 - situé juste au-dessus du col de Lizuniaga.

Altitude : 315m.

#### Description

Tous ces monuments apparaissent sur un sol gazonné, parfaitement dégagé de toutes autres pierres par ailleurs.

<section class="monument">

### [Sare]{.spatial} - [Kondendiagako lepoa]{.coverage} 1 - [Cromlech]{.type}

#### Localisation

Il est situé à 10m au Sud Sud-Ouest de la « Venta Negra », et à 30m au Nord Nord-Est de la BF 32.

#### Description

On note une légère surélévation de terrain en forme de couronne de 3m de diamètre, à la surface de laquelle apparaissent une vingtaine de pierres, au ras du sol mais bien visibles. Quelques pierres marquent le centre du cercle. On allume malheureusement parfois des foyers au centre de ce monument...

</section>

<section class="monument">

### [Sare]{.spatial} - [Kondendiagako lepoa]{.coverage} 2 - [Cromlech]{.type}

#### Description

Tangent Sud du précédent. Cercle de 3m de diamètre, délimité par quelques pierres apparaissant sur une légère surélévation en couronne surtout marquée en secteur Est ; on note une pierre centrale.

</section>

<section class="monument">

### [Sare]{.spatial} - [Kondendiagako lepoa]{.coverage} 3 - [Cromlech]{.type}

#### Description

Monument douteux. On note cependant, à 1m à l'Ouest Sud-Ouest du *n°1*, une dizaine de pierres qui paraissent délimiter un cercle de 2m de diamètre.

</section>

<section class="monument">

### [Sare]{.spatial} - [Kondendiagako lepoa]{.coverage} - [Tumulus]{.type}

#### Localisation

Situé à 6m à l'Ouest Nord-Ouest de *Kondendiagako lepoa 1 - Cromlech*.

#### Description

On remarque une quinzaine de pierres groupées en un amas circulaire dont la surface apparaît au-dessus du sol. Hélas situé sur une zone de grand passage, il a été fortement détérioré.

#### Historique

Monuments découverts par [J. Blot]{.creator} en [mai 2009]{.date}.

</section>

# Sauguis-Saint-Etienne

<section class="monument">

### [Sauguis-Saint-Etienne]{.spatial} - [Zera]{.coverage} 6 - [Tumulus]{.type}

#### Localisation

Altitude : 640m.

Il se trouve sur un replat à mi-pente du flanc Nord du mont Zera, dominant le col de Saraxague. Il est situé au milieu de deux pistes pastorales qui passent à 2m de lui.

#### Description

Tumulus terreux érigé sur sol plat, mesurant 10m de diamètre et 0,70m de haut.

#### Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

</section>

# Suhescun

<section class="monument">

### [Suhescun]{.spatial} - [Elhortegia]{.coverage} 1 - [Tumulus]{.type}

#### Localisation

Altitude : 388m.

Tumulus situé à 150m environ au Nord-Ouest du Col des Palombières, sur le premier replat avant la rude ascension de la pente suivante. Il est situé en terrain très légèrement en pente vers le Sud-Est, à 7m au Sud-Ouest de la piste ascendante et à 50m environ au Nord de celle qui traverse d'Est en Ouest ce même replat.

#### Description

Tumulus terreux circulaire de 4,40m de diamètre et 0,15m de haut, à la surface duquel apparaissent de nombreuses pierres de grés blanc, laissant entrevoir la possibilité d'un péristalithe, surtout dans sa moitié Sud.

#### Historique

Monument signalé par [nous]{.creator} [@blotTumulusBixustiaZuhamendi1976, p.117] et revu en [avril 2015]{.date}. Nous avions signalé dans cette publication deux autres tumulus «disposés en triangle» séparés d'une vingtaine de mètres ; après contrôle sur le terrain, nous apportons le rectificatif suivant : les deux autres éléments se trouvent alignés au Sud de *T1*, mais leur qualité de « tumulus » semble douteuse. Néanmoins nous en dirons quelques mots.

-   *Elhortegia T2 (?)* : situé à 17m au Sud du tumulus précédent, il se présente sous la forme d'un relief ovale, de terre et de pierraille, à grand axe Nord-Sud, mesurant 11m de long, 6 de large et 0,40m de haut.

-   *Elhortegia T3 (?)* : situé à 5m au Sud du précédent et à 10m au Nord de la piste Est-Ouest déjà signalée. Constitué de terre et de pierraille, il affecte lui aussi une forme ovale, oblongue, mesure 12m de long, 5m de large et 0,40m de haut.

Devons-nous les considérer comme des tas d'épierrage (peu probable à nos yeux) ou comme des « tumulus » s'étant éboulés dans le sens de la pente ?

</section>

<section class="monument">

### [Suhescun]{.spatial} - [Harribeltza]{.coverage} 2 - [Tumulus]{.type} ([?]{.douteux})

#### Localisation

Un monument dénommé *T1* a été découvert par le groupe Hilharriak en février 2013, mais nous n'avons rien vu à l'endroit signalé.

Il se trouve vers l'extrémité Ouest du grand plateau inclus dans le périmètre du camp protohistorique d'Harribeltza.

#### Description

Tumulus oblongue, érigé sur terrain plat, constitué de terre et de pierraille, à grand axe Nord-Sud ; il mesure 6,60m de long et 5m de large et environ 0,80m de haut. Monument légèrement douteux.

#### Historique

Monument découvert par le [groupe Hilharriak]{.creator} en [février 2013]{.date}.

</section>

<section class="monument">

### [Suhescun]{.spatial} - [Harribeltza]{.coverage} 3 - [Tumulus]{.type}

#### Localisation

Il est à 2m au Nord Nord-Ouest de *Harribeltza 2 - Tumulus (?)*.

#### Description

Petit tumulus de terre et de pierraille, bien circulaire, mesurant 2,40m de diamètre et 0,35m à 0,40m de haut.

#### Historique

Tumulus découvert par [J. Blot]{.creator} en [octobre 2014]{.date}.

</section>

<section class="monument">

### [Suhescun]{.spatial} - [Hirur Haïtzeta lepoa]{.coverage} - [Cromlech]{.type} ([?]{.douteux})

#### Localisation

Altitude : 412m.

Au sommet d'un petit replat dominant au Nord le col d'Hirur Haizeta, à proximité immédiate de postes de tir à la palombe.

#### Description

Cercle de 4m de diamètre, délimité par une vingtaine de petites pierres de la taille d'un poing, dont le sommet affleure à peine la surface du sol. Une très légère dépression occupe la totalité du cercle. Monument douteux ?

#### Historique

Monument découvert par [J. Blot]{.creator} en [avril 2015]{.date}.

</section>

<section class="monument">

### [Suhescun]{.spatial} - [Hocheko Ithurria]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 419m.

Il se voit à droite de la route qui de Suhescun mène au col d'Istixaretta au niveau du réservoir d'eau dénommé « Hocheko Ithurria » : là se détache une bretelle qui mène au col « Haltzeko lepoa ». Le tumulus se trouve à 20m au Sud de la bifurcation.

#### Description

Tumulus très visible de 5m de diamètre et 0,40m de haut ; érigé sur un terrain en très légère pente vers le Nord, il est constitué de terre et de pierraille.

#### Historique

Monument découvert par [J. Blot]{.creator} en [octobre 2014]{.date}.

</section>

<section class="monument">

### [Suhescun]{.spatial} - [Istilxarreta]{.coverage} - [Tumulus]{.type} ([?]{.douteux})

#### Localisation

Altitude : 540m.

Sur un replat au milieu de la ligne de crête qui monte du col d'Istilxaretta vers Hocha Handia.

#### Description

Elle est rendue très difficile par l'abondance de la végétation (ronces etc.) qui dissimule les détails. Néanmoins il semble qu'on puisse « deviner » un tumulus de terre et de pierres de 7m de diamètre environ, avec une dépression centrale (excavation ?). Il ne semble pas s'agir d'un mouvement naturel du terrain. Il pourrait exister un deuxième élément, sensiblement de mêmes dimensions, tangent au Sud-Ouest, mais lui aussi dissimulé par la végétation... Comme d'habitude, nous préférons signaler un élément douteux que d'en omettre un qui pourrait éventuellement s'avérer valable ultérieurement.

#### Historique

Monument découvert par [J. Blot]{.creator} en [octobre 2014]{.date}.

</section>

<section class="monument">

### [Suhescun]{.spatial} - [Sen Julian]{.coverage} - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 411m.

Il est érigé au flanc Nord-Ouest de la colline de Sen Julian, à une dizaine de mètres à l'Est de la naissance d'un petit riu et une centaine de mètres d'un petit bosquet de chênes.

#### Description

Tertre asymétrique sur terrain en pente vers le Nord Nord-Est, de 10m de large, d'une douzaine de long, et 1m de haut environ.

#### Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2015]{.date}.

</section>

<section class="monument">

### [Suhescun]{.spatial} - [Urdamendi Lepoa]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 383m.

Il est situé à 10m au Sud du chemin de crête.

#### Description

Tumulus circulaire mixte, de terre et de pierres de 8m de diamètre et 0,40m de haut, fortement dissimulé dans les broussailles.

#### Historique

Tumulus découvert par [F. Meyrat]{.creator} en [novembre 2014]{.date}.

</section>

# Tardets-Sorholus

<section class="monument">

### [Tardets-Sorholus]{.spatial} - [Juge]{.coverage} 1 - [Tumulus]{.type}

#### Localisation

Altitude : 573m.

Sur la ligne de crête de la longue croupe allongée d'Ouest en Est, au flanc Est du sommet de La Madeleine. Il est à environ 100m à l'Est de la maison « Juge ».

#### Description

Tumulus (ancien dolmen ?) de terre et de pierres abondantes, de 18m de diamètre environ et, en moyenne, de 1m de haut érigé sur terrain plat ; au centre on peut voir une légère dépression circulaire de 2,40m de diamètre. Tout au bord et au Nord de celle-ci est construit sur ce monument, un important poste de tir à la palombe.

#### Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

Ajoutons ici que le *Dolmen de Bordetta* [@blotSouleSesVestiges1979, p.26], situé sur cette même ligne de crête, plus à l'Est, a été rasé par les travaux agricoles. Il n'en reste que la silhouette du tumulus qui faisait 26m de diamètre et 1,50m de haut.

Altitude : 563m

</section>

<section class="monument">

### [Tardets-Sorholus]{.spatial} - [Juge]{.coverage} 2 - [Tumulus]{.type} ([?]{.douteux})

#### Localisation

Altitude : 562m.

Situé au Sud-Est du précédent, à proximité immédiate d'une clôture de barbelés.

#### Description

Petit tumulus terreux de 4m de diamètre et 0,50m de haut.

#### Historique

Tumulus découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

</section>

<section class="monument">

### [Tardets-Sorholus]{.spatial} - [Juge]{.coverage} 3 - [Tumulus]{.type} ([?]{.douteux})

#### Localisation

À 7m au Nord de *Juge 2 - Tumulus (?)*.

#### Description

Constitué de terre et de pierraille, il mesure 7m de diamètre et 0,80m de haut.

#### Historique

Monument découvert par [Blot J.]{.creator} en [octobre 2011]{.date}.

</section>

# Uhart-Cize

<section class="monument">

### [Uhart-Cize]{.spatial} - [Phagalepoa]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 885m.

Ce tumulus est situé sur sol plat au sommet d'une petite éminence qui domine, à l'Ouest, la « voie romaine », à environ 400m au Sud du refuge d'Orisson.

#### Description

Tumulus circulaire mesurant 8m de diamètre et environ 0,90m de haut. Il n'y a pas d'éléments pierreux visibles ; on note une légère dépression à son sommet.

Il est entouré d'une légère dépression circulaire, de 1,60m de diamètre en moyenne, qui pourrait correspondre au volume de terre recueillie pour son élaboration.

#### Historique

Monument découvert par [F. Meyrat]{.creator} en [décembre 2017]{.date}.

</section>

<section class="monument">

### [Uhart-Cize]{.spatial} - [Othatzen borda]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 922m.

Il est situé à environ 800m plus au Sud que *Phagalepoa - Tumulus*, sur la même ligne de croupes orientées Sud-Nord, et toujours à l'Ouest de la « voie romaine ». On le voit à l'extrémité Nord d'une petite éminence, juste avant sa rupture de pente vers le Nord.

#### Description

Une légère surélévation du terrain au sommet de cette éminence semble bien individualiser un relief tumulaire circulaire de 6m de diamètre et 0,50m de haut. [Monument cependant douteux]{.douteux}.

#### Historique

A été découvert par [F. Meyrat]{.creator} en [décembre 2017]{.date}.

</section>

# Uhart-Mixe

<section class="monument">

### [Uhart-Mixe]{.spatial} - [Beneditenia]{.coverage} - [Dolmen]{.type} (Faux dolmen) ([?]{.douteux})

#### Localisation

Altitude : 110m.

Situé au point où le sentier de GR 65 se détache de la petite route qui mène à Orsanco, ce « monument » est constitué de 3 dalles de schiste plantées en forme de pseudo-dolmen, sans tumulus visible.

#### Historique

Monument signalé par [P. Velche]{.creator} en [juin 2011]{.date} ; après enquête, il s'avère que ces dalles ont été plantées en 1996 par le curé en charge des villages avoisinants, de même que le pseudo menhir érigé derrière la chapelle de Soyarce.

</section>

<section class="monument">

### [Uhart-Mixe]{.spatial} - [Soyarce]{.coverage} 3 - [Tumulus]{.type}

#### Localisation

Altitude : 284m.

Il est situé à 20m au Nord-Ouest de *Soyarce 3 - Tumulus*.

[@blotNouveauxVestigesProtohistoriques1975], tangent à la berge Sud du chemin de Soyarce, et immédiatement à l'Ouest d'un abreuvoir en ciment.

#### Description

Tumulus de terre et de pierres de 9m à 10m de diamètre environ et 0,40m de haut.

#### Historique

Monument découvert par [Blot J.]{.creator} en [mars 1976]{.date} ; non publié depuis.

</section>

<section class="monument">

### [Uhart-Mixe]{.spatial} - [Soyarce]{.coverage} 4 - [Tumulus]{.type} ([?]{.douteux})

#### Localisation

Altitude : 276m.

Situé à quelques dizaines de mètres au Nord-Ouest du chemin qui descend de Soyarce vers « Gibraltar ».

#### Description

Tumulus érigé sur un terrain en légère pente vers le Sud-Est, de 6m à 7m de diamètre, et de 0,30m à 0,40m de haut, formé de terre et de nombreuses pierres. Sa hauteur irrégulière, le volume des pierres nous feraient penser à un possible (?) tas d'épierrage.

#### Historique

Monument trouvé par le [groupe Hilharriak]{.creator} en [décembre 2011]{.date}.

</section>

<section class="monument">

### [Uhart-Mixe]{.spatial} - [Soyarce]{.coverage} 5 - [Tumulus]{.type}

#### Localisation

Altitude : 285m.

Il est situé à 20m au Sud-Est de *Soyarce 3 - Tumulus* [@blotNouveauxVestigesProtohistoriques1975].

#### Description

Tumulus pierreux très net mais de très faible hauteur, étalé en forme de galette circulaire de 4,60m de diamètre.

#### Historique

Monument découvert par [Blot J.]{.creator} en [janvier 2012]{.date}.

</section>

<section class="monument">

### [Uhart-Mixe]{.spatial} - [Soyarce]{.coverage} 6 - [Tumulus pierreux]{.type} ([?]{.douteux})

#### Localisation

Altitude : 270m.

Il se trouve tangent au bord Est du chemin qui vient de la chapelle de Soyarce. Sur un terrain en légère pente vers le Sud, il mesure 7m à 8m de diamètre, 0,30m de haut, constitué de nombreuses petites dalles du matériau local (marne).

#### Historique

Monument trouvé par [Blot J.]{.creator} en [janvier 2012]{.date}.

</section>

# Urepel

<section class="monument">

### [Urepel]{.spatial} - [Caminarte]{.coverage} 1 - [Tumulus]{.type}

#### Localisation

Carte 1346 Ouest Saint-Etienne-de-Baïgorri.

Altitude : 930m.

Toujours situé, comme les monuments précédents, en forêt d'Hayra, il est à 17m au Sud de la route qui va au col de Teillary, à 500m au Nord-Ouest de ce dernier, et en bordure du chemin de crête qui vient du Sud.

#### Description

Un amoncellement de blocs de grès et de schiste détermine un tumulus de 5m de diamètre et 0,40m de haut : au centre une légère dépression à grand axe Nord-Sud (fouille ancienne ?).

#### Historique

Monument découvert en [août 1978]{.date}.

</section>

<section class="monument">

### [Urepel]{.spatial} - [Caminarte]{.coverage} 2 - [Tumulus]{.type}

#### Localisation

À 1,50m au Sud-Est de *Caminarte 1 - Tumulus*.

#### Description

Tumulus de 6m de diamètre, plus plat que *Caminarte 1 - Tumulus*, sa hauteur est difficile à évaluer : 0,20m à 0,30m de haut. On distingue bien un petit massif pierreux central, et il y aurait probablement un péristalithe, mais mal individualisé en l'état actuel.

#### Historique

Monument découvert en [août 1978]{.date}.

</section>

<section class="monument">

### [Urepel]{.spatial} - [Caminarte]{.coverage} 3 - [Tumulus]{.type}

#### Localisation

Altitude : 920m.

Ce très discret monument se trouve devant un poste de tir à la palombe, à 5m à l'Est des deux autres *Tumulus n°1* et *n°2*, découverts par nous et déjà publiés [@blotInventaireMonumentsProtohistoriques2009, p.54].

#### Description

Une quinzaine de petits blocs de schiste disposés quasiment en spirale dans le sol, forment comme une galette de 1,40m de diamètre, bien visible, mais dépassant à peine la surface.

#### Historique

Monument découvert par [J. Blot]{.creator} en [mai 2010]{.date}.

</section>

<section class="monument">

### [Urepel]{.spatial} - [Errola]{.coverage} - [Tertre d'habitat]{.type}

#### Localisation

Altitude : 790m.

Il est situé sur un petit replat, immédiatement à l'Ouest d'un virage de la piste qui monte vers le sommet du mont Errola en venant du col de Méharostégui.

#### Description

Tertre de 8m de diamètre, asymétrique, avec un sommet plat d'environ 5m de large et un versant abrupt à l'Ouest.

#### Historique

Monument découvert par [Blot J.]{.creator} en [décembre 2011]{.date}.

</section>

<section class="monument">

### [Urepel]{.spatial} - [Errola]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 830m.

On le trouve à l'extrémité Sud d'un replat au flanc Sud du mont Errola.

#### Description

Beau tumulus de 8m de diamètre et plus d'un mètre de haut, constitué de terre et de pierres. Au sommet se voit une légère dépression, allongée suivant un axe Nord-Sud, de 1m de long, 0,80m de large et quelques centimètres de profondeur. Tumulus dolménique ?...

#### Historique

Monument découvert par [Meyrat F.]{.creator} en [décembre 2011]{.date}.

</section>

<section class="monument">

### [Urepel]{.spatial} - [Harriondoko Kaskoa]{.coverage} - [Tertres d'habitat]{.type}

#### Localisation

Altitude : 890m.

6 tertres d'habitat sont situés dans un petit col à environ 500m au Sud-Ouest du col d'Hauzay.

#### Description

On peut décrire 6 tertres d'habitat, établis sur un terrain en légère pente vers le Nord.

-   Le *tertre n°1* le plus à l'Est se trouve à environ 200m au Sud-Ouest du petit col à l'arrivée de la route venant d'Hauzay ; de forme ovale comme tous les autres, il mesure 6m x 5m et 0,40m de haut.

-   Le *tertre n°2* est à 26m au Sud-Ouest du précédent, il mesure 7m x 6m et 0,30 de haut.

-   Le *n°3* est à 14 m au Sud-Ouest du *n°2* ;

-   le *n°4* à 15m à l'Ouest du *n°2* et on peut voir les vestiges de 2 autres tertres accolés à lui, côté Est.

-   Le *n°5* est à 5m à l'Est du précédent, et lui aussi possède les restes de 2 ou 3 tertres à son flanc Nord-Ouest.

-   Enfin le *tertre n°6* est à 7m au Nord du précédent et mesure 7m x 7m.

#### Historique

Tertres découverts par [J. Blot]{.creator} en [mai 2010]{.date}.

</section>

<section class="monument">

### [Urepel]{.spatial} - [Hauzay]{.coverage} - [Dolmen]{.type}

#### Localisation

Carte 1346 Ouest Saint-Etienne-de-Baïgorri.

Altitude : 990m.

Il est situé sur cette longue ligne de croupes orientée Nord-Sud, qui s'étend du mont Otsaxar au Nord, au Lindus au Sud. Il est à environ 150m au Sud du col d'Hauzay, et en bordure de la piste pastorale antique dominant la route récemment tracée, toute proche.

#### Description

Tumulus pierreux d'un diamètre de 5,30m environ, et 0,40m de haut. Il semblerait que l'on puisse y distinguer un péristalithe, difficile cependant à affirmer dans le contexte pierreux de ce tumulus. La [chambre funéraire]{.subject}, orientée Sud Sud-Est mesure 1,40m de long, 0,90m de large et 0,60m de profondeur. De cette chambre, il ne reste que 2 dalles plantées dont une, à l'Ouest, mesure 1,40m de long et 0,60m de haut ; à l'Est gît la grande dalle de couverture, mesurant 1,40m de long et 0,90m de large.

#### Historique

Dolmen découvert en [août 1972]{.date}.

</section>

<section class="monument">

### [Urepel]{.spatial} - [Hortz Zorrotz]{.coverage} - [Dolmen]{.type}

#### Localisation

Carte 1346 Ouest Saint-Etienne-de-Baïgorri.

Altitude : 1010m.

Il est situé en bordure de la piste pastorale qui vient du cirque rocheux d'Hortz-Zorrotz, et des monuments que celui-ci recèle à 150m au Nord (voir cromlechs et tumulus décrits à la commune de Banca).

#### Description

Tumulus pierreux de 11m de diamètre et 0,50m de haut, recouvert de mousse. La [chambre funéraire]{.subject}, orientée Sud Sud-Est, mesure 1,44m de long, 0,90m de large et 0,30m de profondeur ; elle est délimitée par 2 dalles perpendiculaires espacées de 0,50m, ayant toutes deux les mêmes mensurations : 0,90m de long, 0,30m de haut et 0,30m d'épaisseur.

#### Historique

Dolmen découvert en [août 1972]{.date}.

</section>

<section class="monument">

### [Urepel]{.spatial} - [Hortz Zorrotz]{.coverage} 2 - [Dolmen]{.type}

#### Localisation

Altitude : 1025m.

Ce dolmen est tangent et à droite de la piste qui monte du col d'Hortz Zorrotz vers Urtaray et qui longe des postes de tir à la palombe.

#### Description

Tumulus formé d'un amoncellement de blocs rocheux couverts de mousse, (très similaire à *Iraztei 3 - Dolmen*), érigé sur un terrain en légère pente vers le Nord-Ouest. Tumulus de forme ovale, allongé dans le sens de la pente, mesurant 7m x 5m et 0,40m de haut.

Au centre une dalle plantée verticalement, selon un axe Nord-Ouest Sud-Est, au milieu d'une légère dépression, marque la [chambre funéraire]{.subject} ; cette dalle mesure 0,80m de long, 0,24m de haut et 0,20m d'épaisseur.

#### Historique

Dolmen trouvé par [Blot J.]{.creator} en [juin 2012]{.date}.

</section>

<section class="monument">

### [Urepel]{.spatial} - [Hortz Zorrotz]{.coverage} - [Monolithe]{.type}

#### Localisation

Altitude : 1010m.

#### Description

Belle dalle de [calcaire]{.subject} blanc, de forme rectangulaire dans l'ensemble, mesurant 3,18m de long et 1,51m dans sa plus grande largeur, et 0,15 à 0,20m d'épaisseur en moyenne. Ce qui la rend particulièrement remarquable c'est l'existence d'un épannelage sur tout son pourtour, et le fait qu'il semble nettement qu'on ait voulu donner à cette dalle un aspect « anthropomorphe » en faisant apparaître deux reliefs, à gauche et surtout à droite, évoquant des épaules de part et d'autre d'une « tête » très schématique, mais bien individualisée. Un trait de cassure incomplet dû au gel ou à l'homme, a presque détaché la « tête » du corps. Il serait intéressant de retourner cette dalle pour vérifier l'existence ou non de gravures sur l'autre face.

#### Historique

Monolithe découvert par [I. Txintxuretta]{.creator} en [août 2010]{.date}.

</section>

<section class="monument">

### [Urepel]{.spatial} - [Hortz Zorrotz]{.coverage} - [Tertres d'habitat]{.type}

#### Localisation

Altitude : 980m.

Trois tertres d'habitat se trouvent au flanc Nord de la barre rocheuse d'Hortz Zorrotz, quasiment accolés à elle. Ils sont aussi au Sud-Ouest et au-dessus d'un vaste replat qui domine le col d'Hauzay, au Sud.

#### Description

-   Le *TH n°1*, le plus à l'Ouest des trois, se présente sous la forme d'un important tumulus ovale, de terre et de pierres assez volumineuses, érigé sur un terrain en pente vers le Nord-Ouest, mesurant 8m de long, 7 de large et 2m de haut environ ; le sommet est plat. Il s'agit d'un TH de type bas-navarrais assez démonstratif.

-   Le *TH n°2* est à 10m au Sud-Est et lui est semblable en tous points.

-   Le *TH n°3* est 8m au Sud-Est du précédent, mais est moins net tant dans sa forme que dans ses dimensions.

#### Historique

Monuments découverts par [Blot Colette]{.creator} et [Meyrat F.]{.creator} en [septembre 2012]{.date}.

</section>

<section class="monument">

### [Urepel]{.spatial} - [Ichterbégui]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 863m.

Ce monument se trouve à une centaine de mètres à l'Ouest de la borde dénommée Biperren Borda, sur un petit replat existant sur une prairie en pente au flanc Nord du mont Itcherbégui. Ce tumulus est situé à 14m au Nord Nord-Est d'un muret qui s'étend d'Est en Ouest et à une centaine de mètres, toujours au Nord Nord-Est, d'une banquette de tir située dans une prairie, à proximité de sa périphérie marquée par des arbres. Cette montagne d'Itcherbégui est très riche en éléments de défense se rapportant très probablement à l'époque napoléonienne.

#### Description

Petit tumulus de terre, en forme de dôme circulaire mesurant 4m de diamètre et 0,40m de haut. Pas de pierres visibles.

#### Historique

Monument découvert par [F. Meyrat]{.creator} en [avril 2019]{.date}.

</section>

<section class="monument">

### [Urepel]{.spatial} - [Iraztei]{.coverage} - [Cromlech]{.type}

#### Localisation

Altitude : 960m (le point culminant IGN, 962m, se trouve à 8m à l'Ouest Nord-Ouest). Enfin il est à 80m au Sud de *Iraztei 3 - Dolmen*.

#### Description

Érigé sur terrain plat, on note un cercle de 8m de diamètre, délimité par des pierres de calibre variable - certaines volumineuses - plantées ou enfouies dans le sol. Les plus volumineuses sont dans la moitié Sud du monument, et 4 autres dans une légère dépression centrale mesurant 5,40m de diamètre et 5 centimètres de profondeur.

Dans le quart Nord-Ouest du cercle apparaissent de nombreuses pierres de calibre plus réduit.

#### Historique

Monument découvert par [L. Millan]{.creator} en [juin 2012]{.date}.

</section>

<section class="monument">

### [Urepel]{.spatial} - [Iraztei]{.coverage} 3 - [Dolmen]{.type}

#### Localisation

Altitude : 960m.

Ce dolmen se trouve à 20m au Sud Sud-Ouest du *Dolmen n°2*, lequel se trouve à 23m au Sud Sud-Ouest du *n°1*, ces deux derniers monuments ayant été décrits par J.M. de Barandiaran [@barandiaranCronicaPrehistoriaAlduides1949, p.7].

#### Description

On note un tumulus pierreux de 6m de diamètre et 0,60m de haut, érigé sur terrain plat, formé de blocs de schiste gréseux de dimensions variables. Au centre se voit une pierre rectangulaire de calibre plus important, presque couchée à la surface du tumulus, selon un axe Nord Sud, l'extrémité Sud étant enfoncée dans le monument. Elle mesure 0,80m de long, 0,71m de large et 0,40m d'épaisseur en moyenne. Vestige d'un élément de la chambre funéraire ou de son couvercle ?

#### Historique

Monument découvert par [J. Blot]{.creator} en [mai 2010]{.date}.

Rappelons ici pour mémoire, et toujours dans la commune d'Urepel, l'ensemble formé par :

-   les *deux cromlechs d'Hortz-Zorrotz*: *n°1* et *n°2* [@blotNouveauxVestigesMegalithiques1972c, p.192],

et

-   les *deux tumulus* du même nom [@blotInventaireMonumentsProtohistoriques2009, p.22].

    À propos de ces tumulus, précisons que :

    -   Le *Tumulus n°3 d'Hortz-Zorrotz*, à 4m au Sud Sud-Est du *Cromlech n°2*, présente en son centre un petit amas pierreux d'environ 8 pierres - de calcaire local - et que sa périphérie pourrait être délimitée par une douzaine de pierres, peu visibles, disposées en cercle.

    -   Le *Tumulus pierreux n°4 d'Hortz-Zorrotz*, à 2m au Sud-Ouest du *Cromlech n°2* mesure une vingtaine de centimètres de haut, et est constitué d'une quinzaine de pierres (calcaire local) dont certaines paraissent délimiter une ébauche de cercle périphérique, surtout visible dans sa moitié Nord-Est.

</section>

<section class="monument">

### [Urepel]{.spatial} - [Lezetako Kaskoa]{.coverage} - [Cromlech]{.type} ([?]{.douteux})

#### Localisation

Altitude : 600m.

Il est situé à 5m au Nord du chemin empierré qui conduit de Bihurrietabuztanénéa au mines d'or, à son arrivée sur le plateau. Ce chemin se détache lui-même sur la gauche de la route venant de la colline Kutxaxarreta.

#### Description

Sur un terrain sensiblement plat, on note près d'une quinzaine de pierres, de calibres variés, plantées dans le sol, et qui paraissent délimiter un cercle de 3m de diamètre. Monument douteux...

#### Historique

Monument découvert par [M. Txoperena]{.creator} et signalé à [L. Millan]{.creator} en [novembre 2011]{.date}.

</section>

<section class="monument">

### [Urepel]{.spatial} - [Lezetako Kaskoa]{.coverage} - [Tumulus-cromlech]{.type} ([?]{.douteux})

#### Localisation

Il est à 3m au Sud-Est de *Lezetako Kaskoa - Cromlech*.

#### Description

Tumulus de 3m de diamètre ; une profonde excavation est visible en son centre, mesurant 1,40m de large. Une dizaine de pierres à la périphérie de celle-ci peuvent faire penser à un péristalithe (?). Monument douteux.

#### Historique

Monument découvert par [M. Txoperena]{.creator} et signalé à [L. Millan]{.creator} en [novembre 2011]{.date}.

</section>

<section class="monument">

### [Urepel]{.spatial} - [Mizpira]{.coverage} 2 - [Tumulus]{.type}

#### Localisation

Carte 1346 Ouest Saint-Etienne-de-Baïgorri.

Altitude : 830m.

Ces monuments sont situés sur la ligne de croupe, bien plus au Nord que les précédents. Nous avions décrit [@blotNouveauxVestigesMegalithiques1972c, p.190], un premier tumulus dans la commune des Aldudes ; deux autres tumulus, tangents, sont situés à 180m environ au Sud Sud-Est du précédent, à 830m d'altitude, dans un petit col au carrefour de plusieurs pistes pastorales.

#### Description

Petit tumulus mixte de terre et de petites pierres, aplati en galette, de 6m de diamètre et 0,50m de haut.

#### Historique

Ce monument a été découvert en [août 1972]{.date}. Il a été rasé depuis par le passage d'une route.

</section>

<section class="monument">

### [Urepel]{.spatial} - [Mizpira]{.coverage} 3 - [Tumulus]{.type}

#### Localisation

Il est tangent au Sud de *Mizpira 2 - Tumulus*.

#### Description

Petit tumulus mixte de terre et de petites pierres, aplati en galette, de 6m de diamètre et 0,50m de haut.

#### Historique

Ce monument a été découvert en [août 1972]{.date}. Il a été rasé depuis par le passage d'une route.

</section>

<section class="monument">

### [Urepel]{.spatial} - [Patarramunho]{.coverage} - [Tertres d'habitat]{.type}

#### Localisation

Sur le flanc Ouest du mont Patarramunho, à une soixantaine de mètres au Nord de la borde Zurguinaren borda, sur un terrain en pente. On trouve deux groupes de deux tertres d'habitat.

#### Description

-   Groupe du haut : *Tertres d'habitat A* et *B*.

    -   *TH A* :

        Altitude : 850m.

        Tertre de terre, dissymétrique, érigé sur un terrain en pente vers le Nord, allongé dans le sens Est-Ouest, mesurant 6m x 4m et 0,50m de haut.

    -   *TH (?) B* : situé à 4m à l'Est du précédent ; beaucoup plus modeste, ne mesure que 3m de diamètre et 0,30m de haut ; tertre douteux.

-   Groupe du bas : *Tertres d'habitat C* et *D*.

    Ce groupe du bas est à une centaine de mètres plus bas que le groupe précédent, et dans une zone boisée.

    -   *TH C* :

        Altitude : 830m.

        Ce tertre d'habitat, constitué de terre, érigé sur terrain en pente, affecte la forme dissymétrique classique, mesure 6m x 4m et 0,60m de haut.

    -   *TH D* :

        Altitude : 812m.

        Tertre de terre, de même forme que *C*, érigé sur terrain en pente, situé à 25m à l'Est Sud-Est de lui mais un peu plus en hauteur, mesure 6m x 4m et 0,50m de haut.

</section>

<section class="monument">

### [Urepel]{.spatial} - [Urtaray]{.coverage} 1 - [Tumulus]{.type}

#### Localisation

Altitude : 1120m.

Monument érigé sur la ligne de crête.

#### Description

Petit tumulus pierreux de 30 à 40 centimètres de haut, constitué d'un amas central recouvert par une dalle de 0,84m de long, 0,54m de large et 0,20m d'épaisseur, à base large et arrondie vers l'Est et plus étroite vers l'Ouest. Cette dalle est entourée d'une vingtaine de pierres, de calibre plus modeste, enfouies dans le sol mais cependant mobiles. La périphérie de tumulus est marquée par huit pierres (péristalithe ?) dépassant de peu la surface du sol, certaines atteignant cependant 0,50m à 0,60m de long.

#### Historique

Monument découvert par [J. Blot]{.creator} en [juillet 2010]{.date}.

</section>

<section class="monument">

### [Urepel]{.spatial} - [Urtaray]{.coverage} 2 - [Tumulus]{.type} ([?]{.douteux})

#### Localisation

Altitude : 1140m.

Monument érigé sur la ligne de crête.

#### Description

Petit tumulus pierreux, apparaissant sous la forme d'un léger relief circulaire d'environ 4m de diamètre et quelques centimètres de haut. Au centre se voient une douzaine de petites pierres, tandis qu'à la périphérie, à l'Est et surtout à l'Ouest, on peut en voir quelques autres.

#### Historique

Monument découvert par [J. Blot]{.creator} en [juillet 2010]{.date}.

</section>

# Urrugne

<section class="monument">

### [Urrugne]{.spatial} - [Aire Leku]{.coverage} 1 - [Tumulus-cromlech]{.type}

#### Localisation

Altitude : 290m.

Très visible à une vingtaine de mètres au Nord de la piste principale, au milieu du plateau.

#### Description

Tumulus pierreux de 7m de diamètre et 0,40m de haut constitué d'assez volumineux blocs de grés. On note un péristalithe très net, particulièrement visible dans sa moitié Nord-Ouest, où il semble y avoir une alternance de 3 grandes pierres avec d'autres plus petites, environ tous les 2m.

#### Historique

Monument découvert par [J. Blot]{.creator} en [octobre 2009]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Aire Leku]{.coverage} 1 - [Cromlech]{.type} [(?)]{.douteux}

#### Localisation

Altitude : 290m.

Il est à tangent au Nord de la piste principale, et se trouve à une trentaine de mètres au Sud-Est de *Aire Leku 1 - Tumulus-cromlech*.

#### Description

Le cercle, de 3m de diamètre est délimité par huit pierres au ras du sol.

#### Historique

Monument, douteux, découvert par [J. Blot]{.creator} en [octobre 2009]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Aire Leku]{.coverage} 2 - [Cromlech]{.type}

#### Localisation

Altitude : 285m.

Il est situé à l'extrémité Est du plateau, à environ une trentaine de mètres à l'Ouest d'un cayolar en ruines.

#### Description

Cromlech très légèrement surélevé, de 5m de diamètre, délimité par environ 7 pierres, visibles surtout dans la moitié Est de la périphérie ; le centre est marqué par deux autres pierres.

#### Historique

Monument découvert par [J. Blot]{.creator} en [octobre 2009]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Aire Leku]{.coverage} 2 - [Tumulus-cromlech]{.type}

#### Localisation

Altitude : 290m.

Il est situé à 5m au Nord de la piste principale et à quelques dizaines de mètres au Sud-Ouest de *Aire Leku 1 - Tumulus-cromlech*.

#### Description

Petit tumulus pierreux de 0,30m à 0,40m de haut, en grande partie recouvert de broussailles, entouré d'un bourrelet de terre circulaire de 5,20m de diamètre où apparaissent de nombreuses pierres.

#### Historique

Monument découvert par [J. Blot]{.creator} en [avril 2010]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Aire Leku]{.coverage} 3 - [Cromlech]{.type}

#### Localisation

Il est tangent à l'Ouest de *Aire Leku 2 - Tumulus-cromlech*.

#### Description

Petit cercle de 1,50m de diamètre, souligné par un petit bourrelet de terre dans lequel apparaissent de nombreuses petites pierres.

#### Historique

Monument découvert par [J. Blot]{.creator} en [avril 2010]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Aire Leku]{.coverage} 4 - [Cromlech]{.type}

#### Localisation

Monument tangent au Sud-Ouest de *Aire Leku 2 - Tumulus-cromlech* et au Sud de *Aire Leku 3 - Cromlech*.

#### Description

Une quinzaine de pierres au ras du sol délimitent un cercle de 1,90m de diamètre, au centre duquel on note une légère dépression.

#### Historique

Monument découvert par [J. Blot]{.creator} en [avril 2010]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Aire Leku Lepoa]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 250m.

Il est situé dans le petit col séparant le Plateau d'Aire-Leku de la colline Muxugorrigane, tangent au Sud de la piste qui les relie.

#### Description

Tumulus pierreux de 3m à 4m de diamètre, et d'environ 0,40m de haut.

#### Historique

Monument découvert par [J. Blot]{.creator} en [octobre 2009]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Aire Leku]{.coverage} - [Pierre plantée]{.type} [(?)]{.douteux}

#### Localisation

Altitude : 285m.

Elle se trouve à flanc Nord du plateau, à l'amorce de son inclinaison vers le Nord.

#### Description

Pierre ou dalle épaisse en forme de tronc de cône, plantée dans le sol, mesurant 1,20m de haut, 0,80m à sa base, et 0,40m en moyenne d'épaisseur. Pas de traces évidentes d'épannelage.

#### Historique

Pierre découverte par [J. Blot]{.creator} en [octobre 2009]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Aire Leku]{.coverage} Nord - [Dalle plantée]{.type} [(?)]{.douteux}

#### Localisation

Altitude : 200m.

On le trouve à environ 2m au Nord-Ouest de l'ancienne piste pastorale qui descend directement vers Urtubienborda.

#### Description

Dalle de grés de forme lancéolée à pointe supérieure, plantée dans le sol mais inclinée vers le Sud-Ouest. On ne distingue pas de traces d'épannelage.

#### Historique

Dalle découverte par [I. Txintxuretta]{.creator} en [2009]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Aire Ona]{.coverage} - [Monolithe]{.type}

#### Localisation

Altitude : 75m.

Ce monolithe se trouve dans un champ, près de la clôture le séparant de la route, à quelques mètres à l'Est de la bifurcation routière au niveau du camping Aire Ona.

#### Description

Monolithe planté dans le sol, en forme de pain de sucre mesurant 0,60m à sa base la plus large, et 1,80m de haut. Il ne semble pas présenter de traces d'épannelage ni d'inscriptions.

#### Historique

Monolithe découvert par [I. Txintxuretta]{.creator} en [2005]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Anduretako Erreka]{.coverage} - [Pierre plantée]{.type}

#### Localisation

Altitude : 227m.

Il est sur un terrain en pente, au flanc Nord-Est de Mugi, dominant le cours du ruisseau Anduretako erreka.

#### Description

Bloc de grés verticalement planté, de forme rectangulaire, mesurant 1,20m de haut, dont la base, épaisse, rectangulaire à la coupe mesure 0,67m x 0,55m ; le sommet est beaucoup plus mince (0,13m) et lui donne un aspect triangulaire, vue de profil.

#### Historique

Pierre découverte en [mars 2012]{.date} par [D. Ibarzola]{.creator}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Bartzeleku haut]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 390m.

Il est situé à l'extrémité Nord-Est de la croupe du même nom, dans un petit col, avant la remontée du terrain vers le Nord.

#### Description

Petit tumulus de terre et de pierres de 3m de diamètre et 0,30m de haut.

#### Historique

Monument découvert par [Blot J.]{.creator} en [mars 2012]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Erentzu]{.coverage} 3 - [Dolmen]{.type}

#### Localisation

Carte 1245 OT. Hendaye-St Jean-de-Luz.

Altitude : 420 m.

Erentzu n°3 est situé à 50m à l'Est Nord-Est de la BF 15, sur un terrain en pente douce vers l'Est, mais qui présente un très léger replat à cet endroit.

#### Description

Il ne reste de visible, semble-t-il, qu'une belle dalle de grés enfoncée dans le sol mesurant 1,20m de long, 0,40m de haut, et 8 centimètres d'épaisseur en moyenne. Elle est légèrement inclinée vers le Nord, et orientée Est Nord-Est. Tout son pourtour libre présente des traces d'épanelage. Une épaisse végétation de robustes genêts rend difficile l'appréciation d'éventuels autres éléments architecturaux. On ne note pas la présence d'un tumulus, lequel peut avoir été éliminé par solifluxion. Cette dalle unique paraît cependant pouvoir être considérée, par ses caractéristiques, comme les restes d'un petit dolmen de montagne.

#### Historique

Ce vestige a été découvert par [I. Gaztelu]{.creator} en [1989]{.date}. Les deux autres dolmens d'Erentzu, ont été trouvés, le *n°1*, par Barandiaran J.M., et le *n°2* par Blot J.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Barzeleku]{.coverage} 2 - [Dolmen]{.type}

#### Localisation

Altitude : 290m.

Au col situé au flanc Sud de la colline Mokua, prendre un sentier à droite qui mène à un petit replat orienté vers l'Ouest.

#### Description

On note, au sol, une dalle de grés rose rectangulaire, mesurant 1,70m de long pour environ 0,75m de large et 0,20m d'épaisseur. Elle est orientée Est-Ouest, et à son extrémité Nord-Ouest, on note une dalle plantée de 0,71m de long, 0,21m d'épaisseur et 0,23m de haut ; de même qu'apparaît une autre dalle à son autre extrémité Sud-Ouest, de dimensions difficilement appréciables (enfouie).

#### Historique

Monument remarqué depuis longtemps par [F. Iturria]{.creator} (ONF) qui nous l'a signalé en [septembre 2010]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Barzeleku]{.coverage} - [Monolithe]{.type} [(?)]{.douteux}

#### Localisation

Altitude : 295m.

À quelques mètres à l'Ouest de *Barzeleku 2 - Dolmen*, ce monolithe (ou simple rocher ?), attire l'attention par sa position dominante sur le col et sur tout l'horizon à l'Ouest.

#### Description

Il mesure 2,80m de haut, 2,30m à sa base, et environ 0,90m d'épaisseur. Il semble posséder des pierres de calage à sa base, côté Ouest. On note qu'un volumineux bloc a été séparé du flanc Sud, par un phénomène de gel semble-t-il. Il ne semble pas y avoir de traces d'épannelage.

#### Historique

Elément noté par [J. Blot]{.creator} en [septembre 2010]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Biskartxu]{.coverage} - [Tumulus]{.type} [(?)]{.douteux}

#### Localisation

Altitude : 380m.

Il se trouve à l'extrémité d'un petit replat, à une vingtaine de mètres au Nord-Est du GR 10, après que celui-ci ait contourné par le Nord la colline de Mandale, en venant d'Ibardin.

#### Description

Il s'agit d'une structure constituée d'une quarantaine de pierres enfoncées dans le sol, affectant la forme d'une galette circulaire de 3m de diamètre dont 3 pierres jointives pourraient concrétiser le centre. Cet ensemble, bien que douteux, est très semblable au *tumulus Apatessaro 8* [@blotInventaireMonumentsProtohistoriques2009, p.39], ou au *tumulus Erreta 1* [@blotInventaireMonumentsProtohistoriques2009, p.36] ou encore au *tumulus Caminarte 3*.

#### Historique

Tumulus découvert pat [Blot J.]{.creator} en [novembre 2010]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Descarga (Grand)]{.coverage} 2 - [Dolmen]{.type} [(?)]{.douteux}

#### Localisation

Commune d'[Ascain]{.spatial} (enclave dans la commune d'Urrugne)

Altitude : 271m.

#### Description

Une quarantaine de blocs pierreux éparpillés sans ordre apparent, pourraient être les restes d'un tumulus pierreux ayant 4,50m de diamètre environ. (Ce sont les seules pierres visibles dans l'environnement immédiat).

Au centre de ce groupement pierreux, se remarque une dalle de grés local, de forme grossièrement rectangulaire, mesurant 1,27m x 1,20m, à grand axe Nord-Sud. Son bord Ouest est épais (0,39m) et la dalle va en s'amincissant vers son bord Est, qui ne fait qu'un à trois centimètres d'épaisseur. À une vingtaine de centimètres au Nord du bord Nord existe un bloc pierreux de 0,86m x 0,35m ; de même au Sud, à 0,80m, il est une deuxième pierre de 0,92m x 0,58m. L'ensemble de tous ces éléments fait soupçonner un probable monument (dolmen ?) que seule une exploration un peu plus précise permettra de confirmer.

#### Historique

Cet ensemble pierreux avait été remarqué depuis longtemps par de nombreux promeneurs et par nous-mêmes, sans avoir jamais été décrit.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Erentzu]{.coverage} 4 - [Dolmen]{.type} [(?)]{.douteux}

#### Localisation

Altitude : 350m.

Il est situé au flanc Nord du mont Erentzu, sur un terrain en légère pente.

#### Description

On ne distingue, à l'heure actuelle, qu'une belle dalle de grés rose local, verticalement plantée dans le sol, dont le sommet semble avoir été volontairement retaillé ; elle mesure 1,30m de long à sa base, 1m de haut et 0,20m d'épaisseur. De nombreux fragments de dalles sont visibles aux alentours ; il est possible que des phénomènes de colluvion, au cours des siècles, aient modifié la nature du sol qui a pu être plus horizontal dans le passé, comme cela semble avoir été le cas pour nombre de dolmens existant au flanc Nord du Xoldokocelay.

#### Historique

Ce monument nous a été signalé par [F. Iturria]{.creator} (ONF) en [septembre 2010]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Erentzu]{.coverage} 5 - [Dolmen]{.type}

#### Localisation

Altitude : 390m.

#### Description

Au milieu d'un tumulus pierreux de 8m de diamètre et 0,40m de haut, on peut distinguer un ensemble de dalles, dépassant de peu le sol, qui semblent bien délimiter une chambre funéraire, à grand axe Nord-Sud, d'environ 2,30m de long et 1,20m de large. Une grande dalle de 1,94m de long délimite la paroi Est, tandis que, à l'Ouest, la paroi est matérialisée par les sommets de 3 dalles (d'environ 0,35m chacun). Au Nord et au Sud, d'autres pierres pourraient faire partie de la chambre.

#### Historique

Monument découvert par [J. Blot]{.creator} en [janvier 2011]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Erentzu]{.coverage} - [Dalle couchée]{.type} ? Filon naturel ?

#### Localisation

Altitude : 343m.

Dalle située à une dizaine de mètres au Nord-Ouest de la piste qui se dirige vers la frontière, du ruisseau qui la longe, et d'une pierre gravée d'un lauburru daté de 1894.

#### Description

Il s'agit, en apparence, d'une dalle de grés rose de 3,60m de long et 1,35m de large et 0,12m d'épaisseur en moyenne, allongée selon un axe Nord-Ouest Sud-Est. Toutefois, un décapage plus poussé de cette dalle (Meyrat F., juin 2011) a montré qu'elle se prolongeait sous l'humus, bien au-delà, en particulier à son flanc Nord-Est et à son extrémité Sud-Est (qui lui donne alors une longueur de 4,30m). Il nous semblerait peut-être plus valable de parler de filon naturel...

#### Historique

Dalle découverte par [Blot J.]{.creator} en [janvier 2011]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Erentzu]{.coverage} 3 - [Dolmen]{.type}

#### Localisation

Altitude : 420m.

Il est situé à 50m environ de la BF 15 (entre la 15 et la 16), à 4m au Nord du sentier frontière, et sur un terrain en déclivité vers l'Est.

#### Description

Ce monument a déjà été décrit par nous dans notre *Inventaire* [@blotInventaireMonumentsProtohistoriques2010, p.33], mais de récents travaux de défrichage, signalés par F. Ithurria en juin 2011, ont permis de mieux apprécier ce qui reste de ce monument qui était enfoui sous une abondante végétation de robustes genêts. Outre la grande dalle de grés local déjà décrite, (orientée Nord-Est Sud-Ouest, qui mesure 1,33m de long, de 0,08m à 0,15m d'épaisseur, et 0,35m de haut), on peut maintenant distinguer une autre dalle de dimensions plus modestes, orientée Sud-Ouest Nord-Est, qui lui est *presque* perpendiculaire (à son extrémité Nord-Ouest). Elle mesure 0,66m de long, 0,11m d'épaisseur et 0,36m de haut, matérialisant ainsi une chambre funéraire orientée Nord-Ouest Sud-Est, mesurant environ 1,80m de long et 0,80m à 1m de large.

#### Historique

Monument découvert par [I. Gaztelu]{.creator} en [1989]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Erentzu]{.coverage} 6 - [Dolmen]{.type}

#### Localisation

Altitude : 345m.

On le trouve à quelques dizaines de mètres au Sud-Ouest du «parking» anciennement utilisé par les carriers d'Erentzu et ses environs, à 25m environ au Nord de *Erentzu 4 - Dolmen (?)*.

#### Description

On note tout d'abord, sur un terrain en légère pente vers le Nord, une belle dalle de grés rose, implantée selon un axe Nord-Sud haut (légèrement inclinée vers l'Est), mesurant 1,30m à sa base, 0,60m de haut et d'une vingtaine de centimètres d'épaisseur. Elle se situe dans un tumulus pierreux, d'environ 5,50m de diamètre et O,30m de haut. Il semble bien que l'on puisse distinguer, à la périphérie Nord de ce tumulus, une dizaine de pierres du péristalithe..

#### Historique

Monument découvert par [P. Badiola]{.creator} en [mai 2011]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Erentzu]{.coverage} 7 - [Dolmen]{.type} [(?)]{.douteux}

#### Localisation

Altitude : 370m.

On le trouve dans le même secteur que *Errentzu 6 - Dolmen*, plus en hauteur, à environ 80m au Sud Sud-Est de *Erentzu 4 - Dolmen (?)*.

#### Description

Au centre d'un tumulus constitué essentiellement de petites dalles de grés rose, mesurant 5,60m de diamètre et 0,40m de haut, apparaît le sommet d'une dalle verticale, orientée Est-Ouest, d'environ 1,50m de long, 0,50m de haut et une vingtaine de centimètres d'épaisseur. Nous faisons quelques réserves quant à l'authenticité de ce dolmen, en raison de son tumulus (uniquement dalles et diamètre plutôt réduit), et du contexte immédiat (une exploitation de carriers...).

#### Historique

Monument découvert par [P. Badiola]{.creator} en [mai 2012]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Erentzu]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 420m.

Il est situé à 14m à l'Ouest Nord-Ouest de la BF 15. À noter qu'il existe un second tumulus en territoire navarrais, à environ 12m au Sud-Est de la BF 15, de l'autre côté du barbelé frontière (information I. Gastelu).

#### Description

Tumulus pierreux, en forme de galette circulaire de 4m de diamètre et 0,30m de haut, présentant une excavation centrale.

#### Historique

Tumulus découvert par [Blot J.]{.creator} en [février 2012]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Galbario]{.coverage} 3 - [Dolmen]{.type}

#### Localisation

Carte 1245 Ouest Saint-Jean-de-Luz.

Altitude : 224m.

Les *dolmens Galbario 1* et *2* ont été décrits en 1966, [@chauchatSeptDolmensNouveaux1966, p.6]. Altitude : 190m. ; le n°2 se trouve à 200m au Sud du n°1.

Le n°3 se trouve à environ 400m à l'Est Nord-Est de la Croix du Calvaire, sur une petite éminence bien délimitée et bien visible.

#### Description

Tumulus mixte de terre et de pierres, de 8m de diamètre et 0,80m de haut. La chambre funéraire rectangulaire, visible au centre, mesure 2,40m de long et 0,60m de large et est orientée Est-Ouest. Près d'une trentaine de petites dalles de grès local en marquent les limites. La paroi Sud est particulièrement bien délimitée par 9 d'entre-elles, de quelques centimètres de haut, mais restées bien verticales ; il n'y a pas de couvercle visible.

#### Historique

Dolmen découvert en [mars 1970]{.date}. Totalement détruit par les travaux de passage du gazéoduc.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Galbario]{.coverage} 4 - [Dolmen]{.type}

Ont déjà été publiés les *dolmens Galbario 1* et *2* [@chauchatSeptDolmensNouveaux1966, p.100].

#### Localisation

Carte 1245 OT. Hendaye-St Jean-de-Luz.

Altitude : 250m.

Ce monument est situé à l'Ouest du chemin qui relie le sommet du Mont du Calvaire au petit col situé à son flanc Sud, d'où part la piste qui mène au Mont Xoldokogaina. Il est érigé sur un petit replat, et à 4m au Nord d'un pylône d'une ligne à haute tension.

#### Description

Sur un terrain en très légère pente vers le Sud, on distingue un tumulus de quelques centimètres de haut et de 7,50m de diamètre, constitué de terres et de pierres. Au centre apparaît une dalle, de 0,88m de long et 0,23m d'épaisseur, orientée Nord-Sud et inclinée vers l'Est. À l'extrémité Nord-Est de celle-ci, on distingue une autre dalle, horizontale, en grande partie cachée sous l'humus, et qui pourrait mesurer environ 0,57m de large. Ces deux éléments semblent pouvoir être rattachés à la chambre funéraire d'un petit dolmen de montagne.

#### Historique

Monument découvert par [I. Txintxurreta]{.creator}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Galbario]{.coverage} - [Tumulus]{.type}

#### Localisation

Carte 1245 OT. Hendaye-St Jean-de-Luz.

Altitude : 270m.

On le trouve sur terrain plat, à une centaine de mètres au Nord Nord-Ouest de *Galbario 4 - Dolmen*, à l'extrémité du plateau où est érigée la croix du calvaire, à environ une centaine de mètres au Sud-Ouest de celle-ci.

#### Description

Tumulus circulaire de 8m de diamètre et 0,40m de haut, essentiellement constitué de pierraille et de terre. La végétation, dense au centre, semble cacher une dépression qui pourrait être la trace d'une fouille ancienne ou le reste d'une chambre funéraire dolménique dont les dalles auraient disparu.

#### Historique

Monument découvert en [1998]{.date} par [A. Martinez Manteca]{.creator}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Herboure]{.coverage} - [Monolithe]{.type}

#### Localisation

Altitude : 230m.

On trouve cette pierre dans un tournant du chemin récemment tracé par l'ONF : prendre la première route goudronnée à gauche, après l'ancienne douane d'Herboure, et ensuite le chemin de terre (barrière), qui démarre, à gauche, de suite après le franchissement d'un petit ruisseau.

#### Description

Bloc de grés triasique, parallélépipédique, mesurant 2,80m de long et 1,03m dans sa plus grande largeur. Il présente une extrémité arrondie (avec probables traces d'épannelage), d'environ 0,60m de large, l'autre extrémité, rectangulaire n'a que 0,54m de large. L'un des deux côtés les plus longs est naturellement rectiligne, l'autre plus sinueux, ne rompt pas l'harmonie de l'ensemble, mais on note à son niveau des traces très nettes de l'engin mécanisé qui l'a enlevé de son lieu d'origine, peu éloigné toutefois. La patine marron est absente sur une partie de la face supérieure plane de cette pierre : était-elle partiellement enfouie avant son déplacement ?

#### Historique

Monolithe signalé par [F. Ithurria]{.creator} en [avril 2011]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Ibardineko Erreka]{.coverage} - [Tumuli]{.type} et [tertres d'habitat]{.type}

Ce site a été découvert par [I. Txintxuretta]{.creator} en [avril 2005]{.date}, qui signale 6 tertres d'habitat au lieu dénommé : *Aire Leku Zelaia*... Comme le nom *Aire Leku* est déjà attribué à un autre site, nous l'avons appelé : *Ibardineko erreka*, du fait des ruisseaux de ce nom qui encadrent ce vaste pâturage. Nous l'avons revisité en [décembre 2011]{.date}, et avons noté 9 tertres d'habitat et 9 tumuli. Les dimensions et le nombre de tumuli nous évoquent la nécropole à tumuli d'Olhette, toute proche, que nous avons publié en 2008 [@blotInventaireMonumentsProtohistoriques2009, p. 4].

-   *Ibardineko Erreka 1 - Tumulus*

    Altitude : 58m.

    Tumulus de terre et de pierres, sur sol horizontal, mesurant 15m de diamètre et 0,40m de haut, à une quinzaine de mètres à l'Ouest de la piste (I. Txintxuretta : *Tertre d'habitat 1*).

-   *Ibardineko Erreka 2 - Tumulus*

    Tumulus de terre et de pierres situé à 15m à l'Est Nord-Est de *T1*, sur sol horizontal. Il mesure 10m de diamètre et 0,30m de haut (I. Txintxuretta : *Tertre d'habitat 2*).

-   *Ibardineko Erreka 3 - Tumulus*

    Altitude : 59m.

    Tumulus de terre et quelques pierres visibles, érigé sur sol horizontal, mesurant 10m de diamètre et 0,40m de haut ([Blot J.]{.creator}).

-   *Ibardineko Erreka 4 - Tumulus*

    Altitude : 60m.

    Tumulus constitué de terre et de très nombreuses pierres, mesurant 8m de diamètre et 0,30m de haut, sur terrain plat (Blot J.).

-   *Ibardineko Erreka 5 - Tumulus*

    Altitude : 60m.

    Tumulus de 12m de diamètre et 0,50m de haut, de terre et de pierres, sur terrain plat (Blot J.)

-   *Ibardineko Erreka 6 - Tumulus*

    Situé, sur terrain plat, à 4m à l'Est du précédent, il mesure 12m de diamètre et 0,35m de haut. Constitué de terre et de pierres (Blot J.).

-   *Ibardineko Erreka 7 - Tumulus*

    Situé à 12m au Sud de T5, sur terrain plat. Constitué de terre et de pierres, il mesure 12m de diamètre et 0,30m de haut (Blot J.)

-   *Ibardineko Erreka 8 - Tumulus (?)*

    Très petit tumulus ovale, bien visible cependant.

    Altitude : 70m.

    Constitué de terre essentiellement, il mesure 4m x 3m et 0,40m de haut (Blot J.).

-   *Ibardineko Erreka 9 - Tumulus*

    Altitude : 60m.

    Constitué de terre et de pierres, érigé sur terrain plat. Mesure 8m de diamètre et 0,30m de haut (I. Txintxuretta : *Tertre d'habitat 4*).

    Peut-être y aurait-il un dixième tumulus très discret, à 15m au Nord du *Tertre d'habitat n°9*, mesurant 5m de diamètre et de quelques centimètres de haut (Blot J.).

-   *Ibardineko Erreka 1 - Tertre d'habitat*

    Altitude : 58m.

    Situé à 40m au Nord-Ouest et au-dessus d'un ruisseau, et sur terrain en pente.

    Tertre de terre et quelques pierres, mesurant 8 à 9m de diamètre, 0,30m de haut, de forme asymétrique.

-   *Ibardineko Erreka 2 - Tertre d'habitat*

    Altitude : 60m.

    Érigé en rupture de pente, au-dessus et au Nord-Ouest du ruisseau, ce tertre, asymétrique, de terres et de nombreuses pierres, mesure 10m de diamètre et 0,50m de haut.

-   *Ibardineko Erreka 3 - Tertre d'habitat*

    Situé à une quinzaine de mètres à l'Est Nord-Est du *TH n°2*, sur terrain en pente, il est plus discret, asymétrique, et mesure 9m de diamètre et 0,30m de haut.

-   *Ibardineko Erreka 4 - Tertre d'habitat*

    Situé à 5m à l'Est Nord-Est du *TH n°3*, sur terrain en pente, ce tertre asymétrique mesure 8m de diamètre et 0,20m de haut (Blot J.).

-   *Ibardineko Erreka 5 - Tertre d'habitat*

    Altitude : 55m.

    Situé à 30m au Nord-Ouest du *TH n° 2*.

    Tertre asymétrique, discret, érigé sur terrain en pente, mesurant 5m de diamètre et 0,30m de haut (Blot J.).

-   *Ibardineko Erreka 6 - Tertre d'habitat*

    Altitude : 60m.

    Il est situé à 70m environ au Sud Sud-Ouest de *TH n°2*.

    Asymétrique sur terrain en pente, fait de terre et de pierres nombreuses, il mesure 8m x 8m (Blot J.).

-   *Ibardineko Erreka 7 - Tertre d'habitat*

    Altitude : 60m.

    Il est tangent à l'Ouest à la piste et à 20m à l'Ouest du *tumulus n°5*.

    Tertre essentiellement de terre mesurant 10m de diamètre et 0,40m de haut (Blot J.).

-   *Ibardineko Erreka 8 - Tertre d'habitat*

    Altitude : 60m.

    Tertre asymétrique de terre et de pierres, mesurant 8m de diamètre et quelques centimètres de haut (Blot J.).

-   *Ibardineko Erreka 9 - Tertre d'habitat*

    Altitude : 60m.

    Situé à l'Est Nord-Est du *TH n°8* ; asymétrique, il mesure 8m de diamètre et 0,40m de haut (I. Txintxuretta).

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Ibardin]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 335m.

Il est situé sur un petit replat et tangent, au Nord-Ouest, à une piste qui descend des ventas d'Ibardin, situées au Sud-Ouest.

#### Description

Tumulus de terre et pierraille, d'environ 8m de diamètre, 0,30m de haut ; on note une très faible dépression centrale, et la périphérie Sud-Est a été entamée par le passage de la piste.

#### Historique

Monument découvert par [Blot J.]{.creator} en [décembre 2011]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Larrauntiki]{.coverage} 1 - [Tumulus-cromlech]{.type}

#### Localisation

Carte 1245 Ouest Saint-Jean-de-Luz.

Altitude : 660m.

Urrugne. (Vera de Bidassoa).

Il est situé, ainsi que *Larrauntiki 2 - Tumulus-cromlech* et *Larrauntiki 3 - Tumulus*, dans le col de Zizkouitz, entre Larraun et Larrauntiki, à 10m au Sud Sud-Est de la BF 22, donc en territoire de Vera de Bidassoa.

#### Description

Petit tumulus aplati de 0,30m de haut et d'un diamètre de 4m, délimité par une quinzaine de pierres dont 11 sont particulièrement visibles dans la moitié Sud. On note une petite dalle dans le secteur Sud-Ouest, à l'intérieur du monument : fait-elle partie d'une ciste ?

#### Historique

Monument découvert en [septembre 1975]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Larrauntiki]{.coverage} 2 - [Tumulus-cromlech]{.type}

#### Localisation

Urrugne. (Vera de Bidassoa).

À 8m à l'Ouest Sud-Ouest de la BF 22.

#### Description

Tumulus aplati de 5m de diamètre et 0,30m de haut. Une quinzaine de pierres bien visibles en marquent la périphérie. On note une légère dépression centrale où apparaissent 4 petites dalles : ciste ?

#### Historique

Monument découvert en [septembre 1975]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Larrauntiki]{.coverage} 3 - [Tumulus]{.type}

#### Localisation

Urrugne. (Vera de Bidassoa).

Il est à 60m à l'Ouest Sud-Ouest de *Larrauntiki 2 - Tumulus-cromlech*, au bord Sud de la crête, un peu plus en altitude que les précédents, à l'altitude de 662m.

#### Description

Tumulus mixte mesurant 7m de diamètre et 0,30m de haut, à prédominance pierreuse, affectant une forme de couronne du fait d'une légère dépression centrale de 2m de diamètre.

#### Historique

Monument découvert en [septembre 1975]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Lezante]{.coverage} 3 - [Dolmen]{.type}

#### Localisation

Carte 1245 OT. Hendaye-St Jean-de-Luz.

Altitude : 240m.

Ce monument se trouve à environ 200m à l'Ouest de la station filtre du Lezante, sur un replat herbeux au flanc Nord du massif du Xoldokogagna. C'est à ce niveau que la route aboutit, dans un virage à cette station filtre. On rappellera qu'il a déjà été décrit 2 dolmens à quelques dizaines de mètres au Sud-Ouest de cette station [@chauchatSeptDolmensNouveaux1966, p.110].

#### Description

On note un très léger tumulus de terre, d'environ 5m de diamètre et 0,30m de haut actuellement. Au centre sont visibles deux dalles de grés rose parallèles, très inclinées, pratiquement couchées au ras du sol, vers le Nord. La dalle la plus au Nord mesure 0,65m de long et 0,20m de large ; elle est distante d'environ 0,70m de la seconde qui mesure 1,17m de long et 0,30m de large. Ces deux dalles sont tout ce qui reste d'une chambre funéraire orientée Ouest-Est. Il semble exister un péristalithe d'une dizaine de pierres autour du tumulus.

#### Historique

Monument trouvé par [Blot J.]{.creator} en [décembre 2009]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Mandale]{.coverage} 1 à 5 - [Cromlechs]{.type}

#### Localisation

Carte 1245 Ouest Saint-Jean-de-Luz.

Altitude : 500m.

[J.M. de Barandiaran]{.creator} a publié en [1953]{.date} [@barandiaranHombrePrehistoricoPais1953, p.251] et décrit en 1962 [@barandiaranProspecionesExcavacionesPrehistoricas1962, p.18] sur ce petit replat qui domine à l'Ouest le col d'Ibardin, un groupe de 5 cromlechs : - le n°1 : 6m de diamètre et 19 pierres périphériques ; - le n°2 : 5m et 13 pierres ; - le n°3 : 4m et 10 pierres ; - le n°4 : 7m et 11 pierres ; - le n°5 : 8m et 4 pierres ;

Le matériau employé est la quartzite blanche abondante dans l'environnement du site. Pour certains de ces monuments, nous n'avons pas retrouvé, sur le terrain, les mêmes dimensions, ni le même nombre de pierres, ni les mêmes orientations ; enfin il nous a paru possible de proposer deux monuments supplémentaires.

-   *Le cromlech n°1,* le plus au Nord, mesure, suivant les axes, entre 5,70m et 6m de diamètre, avec 19 pierres périphériques. Deux d'entre elles sont particulièrement remarquables : l'une, marquée **A** sur le schéma, plantée au Nord-Est, en forme de pain de sucre, mesure 0,70m de haut et 0,50m à sa base ; l'autre, marquée **B**, au Sud, allongée, mesure 1,65m de long et 0,25m d'épaisseur.

-   *Le cromlech n°2* est situé à 9m au Sud Sud-Est du précédent. Il mesure 6m de diamètre avec 20 pierres périphériques. On retrouve, en plus de dimensions et d'un nombre de pierres similaires, un autre point commun avec le n°1 : les pierres **A** au Nord-Est et **B** au Sud, ont sensiblement les mêmes positions, formes et dimensions.

-   *Le cromlech n°3* est à 4,30m à l'Ouest Sud-Ouest du n°1. Il mesure 3,50 à 4m de diamètre et 7 pierres périphériques, au ras du sol, peu visibles.

-   *Le cromlech n°4* est à 21m au Sud-Ouest du n°1. Il mesure 5,50m de diamètre avec 16 pierres périphériques ; la pierre marquée **A**, au Sud-Est, mesure 0,70 à sa base et 0,70m de haut.

-   *Le cromlech n°5* est à 2m au Sud du précédent. On ne voit actuellement que 3 pierres qui paraissent décrire un arc de cercle ayant 6,50m de diamètre ; la plus volumineuse, au Nord Nord-Est, marquée **A**, mesure 0,70m à sa base et 0,30m de haut.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Mandale]{.coverage} 6 - [Cromlech]{.type}

#### Localisation

À 12m au Sud-Ouest du n°2, et à 3m au Sud Sud-Ouest d'un volumineux conifère, le plus à l'Ouest du groupe d'arbres à cet endroit.

#### Description

Cercle très discret de 3m de diamètre, délimité par 5 pierres, profondément enfouies dans le sol et dont seuls les sommets sont visibles. Comme au col de Méatsé, dans l'Artzamendi (Itxassou), on peut évoquer des colluvions importantes venant, ici, du sommet de Mandale à l'Ouest et qui ont d'ailleurs pu recouvrir totalement d'autres monuments sur ce site ; une prospection géophysique, comme celle qui a été pratiquée au col de Méatsé, pourrait répondre à la question.

#### Historique

Monument découvert en [juin 1968]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Mandale]{.coverage} 7 - [Cromlech]{.type}

#### Localisation

À 4,50m au Sud-Est du n°1.

#### Description

Cercle de 7,50m de diamètre, délimité par 5 pierres. On note, au Nord, une très volumineuse dalle de 1,50m de long, 1,30m de large et 0,15m d'épaisseur, à grand axe Nord-Ouest Sud-Est, en grande partie recouverte de mousse. Cette dernière dissimule presque aussi en totalité les 4 autres blocs de quartzite, dans la moitié Sud du monument.

#### Historique

Monument découvert en [juin 1968]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Manuelenborda]{.coverage} - [Tertres d'habitat]{.type}

#### Localisation

Altitude : 212m.

Ces trois tertres d'habitat (peut-être 4), sont situés dans une fougeraie à gauche de la piste qui monte à Arlépoa.

#### Description

Ces trois tertres sont érigés sur terrain en pente vers le Nord-Est et présentent la dissymétrie habituelle à ce type de monument.

-   *Le tertre n°1*, (le plus bas situé), est à 2m de la piste ; mesure environ 6m x 4m et 0,50m de haut.

-   *Le tertre n°2*, à 1m de la piste, est 6m au Sud-Ouest et mesure environ 5m x 3m et 0,40m de haut. À 1m à l'Est se voit un relief discret qui pourrait correspondre à un éventuel 4^ème^ tertre.

-   *Le tertre n°3*, tangent à la piste et le plus haut situé est à 12m au Sud-Ouest ; il mesure environ 4m x 3m et 0,40m de haut, paraissant en partie effondré.

#### Historique

Tertres découverts par [Domeka Ibarzola]{.creator} en [2014]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Mokua]{.coverage} 1 - [Monolithe]{.type}

#### Localisation

Altitude : 343m.

Il est semble-t-il issu du filon rocheux sur lequel il repose, bien individualisé.

#### Description

Bloc de grés parallélépipédique irrégulier, mesurant 2,07m de long, couché sur le sol selon un axe Est Nord-Est Ouest Sud-Ouest, présentant une base Ouest Sud-Ouest d'une épaisseur de 0,44m et un sommet Est Nord-Est lui aussi de 0,30m d'épaisseur. Au centre il atteint 0,50m d'épaisseur. Il semblerait que ce monolithe ait subi des traces de régularisation, en particulier sur son bord Nord.

#### Historique

Monolithe découvert par [I. Txintxurreta]{.creator} en [décembre 2015]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Mokua]{.coverage} 2 - [Monolithe]{.type} (dit aussi «Vierge Marie»)

#### Localisation

Altitude : 200m.

Ce monolithe se trouve à 6m à 7m au Sud-Ouest d'un petit sanctuaire dédié à la Vierge Marie, et à 6m à l'Ouest de la piste de crête.

#### Description

Petit bloc de grés couché sur le sol selon un axe Nord Nord-Ouest Sud Sud-Est, de forme parallélépipédique allongée, mesurant 1,60m de long pour une largeur au centre de 0,38 m et une épaisseur de 0,38m.

Sa largeur à son extrémité Nord-Ouest est de 0,18m et son épaisseur de 0,44m.

À son extrémité Sud-Est, sa largeur est de 0,24m et son épaisseur de 0,30m.

Pas de traces d'épannelage visible... Borne pastorale ?

#### Historique

Monolithe découvert par [I. Txintxurreta]{.creator} en [décembre 2015]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Mokua]{.coverage} 3 - [Monolithe]{.type}

#### Localisation

Altitude : 344m.

Monolithe couché au sol à 8m au Sud-Est de la piste de crête.

#### Description

Dalle de grés de forme triangulaire de 1,90m de long dans son plus grand axe Nord-Est Sud-Ouest. Sa base Sud-Ouest mesure 1,55m et présente une épaisseur de variant entre 0,24m et 0,32m. Cette épaisseur semble aller en décroissant vers le sommet. Celui-ci est séparé du monolithe par une cassure de 0,72m de long, survenue après une probable chute... ce qui impliquerait qu'elle ait été plantée. De plus, cette dalle triangulaire paraît avoir été épannelée sur tout son pourtour.

#### Historique

Monolithe découvert par [F. Meyrat]{.creator} en [janvier 2016]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Mokua]{.coverage} 2 - [Dolmen]{.type}

#### Localisation

Altitude : 230m.

On le trouve sur un replat de la croupe du flanc Nord du mont Mokua.

#### Description

Au milieu d'un tumulus de 15m de diamètre environ, de terre et de pierres, et d'une hauteur variant de 1m à l'Est à près de 1,50m à l'Ouest, on note deux dalles verticales de grés triasique, les restes de la chambre funéraire, à grand axe Nord Nord-Est Sud Sud-Ouest. Ses dimensions avoisinaient 2,50m de long et 1,20m de large environ ; il n'en reste que le montant Nord-Ouest, une dalle longue de 1,80m, haute de 0,30m à 0,50m et épaisse d'une dizaine de centimètres.

Au Sud-Ouest, à 1m de distance environ et en oblique par rapport à la précédente, (elle est orientée Nord-Sud), la deuxième dalle, légèrement inclinée vers le Nord-Est, est longue de 1,40m, haute de 0,70m en moyenne, et épaisse de 0,10m environ.

#### Historique

Monument trouvé par [F. Iturria]{.creator} en [avril 2011]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Mugi]{.coverage} 3 - [Dolmen]{.type}

#### Localisation

Carte 1245 OT. Hendaye-St Jean-de-Luz.

Altitude : 300 m.

#### Description

Un tumulus de 6m de diamètre se signale à l'attention par une hauteur de quelques centimètres à peine et aussi par la qualité de l'herbe qui y pousse, d'aspect différent du gazon environnant. Au milieu de ce tumulus de terre et de pierres on peut distinguer une belle dalle de 1,10m de long, 0,30m de large, enfoncée dans le sol et complètement couchée ; une autre dalle, plus petite, de 0,22m de long, elle aussi enfoncée dans le sol, est visible à une quarantaine de centimètres au Nord-Ouest. Ce sont les seuls vestiges visibles d'une chambre funéraire orientée Nord-Est Sud-Ouest.

(Signalons pour mémoire deux dalles épaisses plantées dans le sol ainsi que quelques autres, à courte distance, dont l'interprétation n'est pas aisée)

#### Historique

Monument découvert par [J. Blot]{.creator} en [janvier 2010]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Muxugorrigaine]{.coverage} Nord - [Pierre couchée]{.type} [(?)]{.douteux}

#### Localisation

Altitude : 269m.

#### Description

Bloc de grés de forme triangulaire, allongé selon un axe Nord-Ouest Sud-Est, à sommet Nord-Ouest. Il atteint 1,82m de long, et de 0,60m à 0,70m de large à sa base.

Son épaisseur moyenne est de 0,30m. On note près de son sommet, côté Ouest, des traces d'épannelage (?). Il n'y a pas d'autres pierres dans son environnement immédiat. Ce bloc est remarquable lui aussi, comme *Muxugorrigaine Sud - Dalle couchée (?)*, par sa position et ses caractéristiques.

#### Historique

Monument découvert par [J. Blot]{.creator} en [janvier 2010]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Muxugorrigaine]{.coverage} Nord 2 - [Tumulus]{.type}

#### Localisation

Carte 1245 OT. Hendaye-St Jean-de-Luz.

Altitude : 270m.

Il est situé à une centaine de mètres au Sud-Est du *tumulus Muxugorrigaine Nord 1*, et il est tangent, à l'Ouest, à une piste pastorale qui s'est détachée, à quelques dizaines de mètres en amont, de celle qui mène de *Muxugorrigaine Sud* à *Muxugorrigaine Nord 1*.

#### Description

Sur un terrain en très légère pente, on note un tumulus pierreux de 5m de diamètre et une vingtaine de centimètres de haut. Quelques pierres au centre, sont plus volumineuses que les autres et font penser à un éventuel remaniement du tumulus (petit abri pastoral ?) plus qu'à un tas d'épierrage, inattendu dans le contexte environnant.

#### Historique

Monument découvert par [J. Blot]{.creator} en [janvier 2010]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Muxugorrigaine]{.coverage} Sud - [Dalle couchée]{.type} [(?)]{.douteux}

#### Localisation

Altitude : 276m.

Elle est tangente à la piste pastorale et à 9m au Sud-Ouest du *Tumulus-cromlech Muxugorrigaine Sud*.

#### Description

Dalle de grés allongée sur le sol, selon un axe Nord-Sud, en forme de pain de sucre à sommet Sud. Elle mesure 1,45m de long, 0,50m à sa base et 0,33m à son sommet ; son épaisseur est de 0,17m en moyenne. Elle présente des traces évidentes d'épannelage sur tout son pourtour visible ; si elle n'a pas les dimensions habituelles des monolithes, elle est cependant remarquable par ses caractéristiques de localisation et de sa régularisation.

#### Historique

Dalle découverte par [I. Gaztelu]{.creator} en [novembre 1987]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Mugi]{.coverage} - [Cromlech]{.type}

#### Localisation

Altitude : 319m.

Ce monument est situé sur un petit replat de ce terrain en pente générale vers le Nord.

#### Description

Une dizaine de pierres, peu visibles, au ras du sol et de taille variable, délimitent un cercle de 2,80m de diamètre.

#### Historique

Cercle découvert par [I. Txintxurreta]{.creator} en [décembre 2015]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Mugi]{.coverage} 1 - [Tumulus]{.type}

#### Localisation

Altitude : 314m.

Ce tumulus se trouve à 30m au Nord de *Mugi - Cromlech*, et à 40m au Sud de *Mugi 4 - Dolmen*.

#### Description

Tumulus terreux, circulaire, en forme de galette aplatie, de 6m de diamètre et 0,30m de haut.

#### Historique

Monument découvert par [Blot J.]{.creator} en [janvier 2016]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Mugi]{.coverage} 4 - [Dolmen]{.type}

#### Localisation

Altitude : 301m.

Ce monument est situé à 40m au Sud de *Mugi 1 - Tumulus*, sur un léger replat, et bordé sur son flanc Ouest par une petite ravine qui lui a entamé une faible partie de son tumulus.

#### Description

Tumulus circulaire mixte, de terre et de pierres, en forme de galette aplatie, mesurant 10m de diamètre et 0,40m de haut environ. Au centre sont bien visibles, quoiqu'au ras du sol, 3 dalles ou fragment de dalles, profondément enfouies, orientées Est-Ouest, et faisant partie de la chambre funéraire, dont les parois se sont inclinées vers le Nord.

Le fragment de dalle le plus au Nord mesure 0,60m de long ; la seconde dalle, qui lui est parallèle, mesure 0,90m de long, et la troisième, elle aussi parallèle à la précédente, mesure 0,73m de long ; leur épaisseur à toutes trois est de 0,08m en moyenne.

#### Historique

Monument découvert par [I. Txinturreta]{.creator} en [décembre 2015]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Mugi]{.coverage} 2 - [Tumulus]{.type}

#### Localisation

Altitude : 306m.

Il est situé à 10m à l'Ouest de *Mugi 4 - Dolmen* et il est lui aussi longé sur son flanc Ouest par une autre petite ravine.

#### Description

Tumulus pierreux de 4,50m de diamètre et 0,40m de haut ; de nombreux blocs de pierre de taille variable sont visibles en surface, sans aucun ordre apparent.

#### Historique

Monument découvert par [Blot J.]{.creator} en [janvier 2016]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Munhoa]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 340m.

Il se trouve au pied du mont Munhoa, sur un replat dominant un petit col, à 4m à l'Ouest des ruines d'une bergerie recouverte d'un abondant roncier.

#### Description

Tumulus essentiellement pierreux, fait de petits fragments de dalles ou d'éléments nettement plus volumineux, le tout particulièrement bien visible dans la moitié Est.

Ce tumulus mesure 4m de diamètre, et a 0,40m de haut.

#### Historique

Tumulus découvert par [Duvert M.]{.creator} en [décembre 2011]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Oneaga]{.coverage} 2 - [Tumulus]{.type}

#### Localisation

Altitude : 334m.

Ce tumulus est au Nord-Est d'un enclos à bétail métallique.

#### Description

Tumulus mixte de terre et de pierres, de forme conique, mesurant 5,90m de diamètre et 0,45m de haut. Était dans les années passées entièrement recouvert d'ajoncs.

#### Historique

Tumulus découvert par [Meyrat F.]{.creator} en [janvier 2016]{.date}. Nous rappelons qu'il existe un tumulus (*Onéaga T1*), découvert par nous en 2009 et publié dans [@blotInventaireMonumentsProtohistoriques2010].

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Oneaga]{.coverage} - [Ciste]{.type}

#### Localisation

Altitude : 334m.

#### Description

Ciste rectangulaire à grand axe orienté Nord-Ouest Sud-Est, délimitée par deux dalles enfouies dans le sol dont seuls les sommets affleurent de quelques millimètres au-dessus de la surface. La dalle Sud-Ouest mesure 0,60m de long et est séparée de 0,48m de la dalle Nord-Est qui lui est parallèle et qui mesure 0,54m de long. Une troisième portion de dalle est visible, 0,18m dans l'angle Sud qui complète ainsi cette chambre funéraire. Il n'est pas possible de dire, dans l'état actuel, s'il s'agit de la ciste d'un cromlech, d'un tumulus plus ou moins arasé, ou si elle est isolée...

#### Historique

Ciste découverte par le [groupe Hilhariak]{.creator} en [décembre 2015]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Oneaga]{.coverage} (ou Gainxobordakolepoa) - [Cromlechs]{.type}

#### Localisation

Dans le petit col, au Sud-Est du mont Oneaga, où passe la piste qui se rend au dolmen du même nom.

Altitude : 300m.

#### Description

Ces 5 cromlechs sont très difficiles à voir et seul *le n°5* est absolument certain.

-   *Cromlech n°1* : Trois chênes sont alignés selon un axe Nord-Sud sur la gauche de la piste quittant le chemin empierré et traversant le gazon du col (l'arbre du milieu est mort et en partie abattu). Le cromlech se trouve immédiatement au Nord du 3^ème^ arbre, ^soit^ à environ 30m de la naissance de la piste. Cercle de 8m de diamètre, délimité par une dizaine de pierres, au ras du sol. Une pierre est visible au centre.

-   *Cromlech n°2* : Il est pratiquement tangent, au Nord du précédent. Cercle de 6 m de diamètre, délimité par une quinzaine de pierres, toujours au ras du sol.

-   *Cromlech n°3* : Situé à 1m du précédent. Cercle de 3,50m de diamètre, délimité par une dizaine de pierres au ras du sol.

-   *Cromlech n°4* : Situé à 3m au Nord du n°2. Un cercle de 5,20m de diamètre serait délimité par environ 8 pierres, toujours au ras du sol.

-   *Cromlech n°5* : On le trouve à l'Est de la piste se rendant au dolmen ; il est à environ 30m à l'Ouest du *cromlech n°2*.

S'il n'y a que 5 pierres pour délimiter un cercle de 6,50m de diamètre, au moins sont-elles bien visibles : tout d'abord, dans le secteur Nord, on note 3 dalles dépassant légèrement la surface du gazon. Les deux plus longues mesurent 0,76m de long et sont séparées par une troisième plus petite de 0,18m de long ; la quatrième dalle, de 0,40m de long, se trouve en secteur Est, et la cinquième, au Sud.

Nous n'avons pas noté de sixième cromlech, quoique qu'il nous ait été signalé... de même qu'un tumulus.

#### Historique

Les *cromlechs 1*, *2*, *3*, *4* ont été trouvés par [L. Millan]{.creator} ; le *5* par [A. Martinez]{.creator} ; le *6* par A. Martinez et L. Millan - [juillet 1997]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Oneaga]{.coverage} 6 - [Cromlech]{.type}

#### Localisation

Carte 1245 OT. Hendaye-St Jean-de-Luz.

Altitude : 330m.

Il se trouve à 6m au Nord du *dolmen Oneaga 1* que nous avons décrit en 1972 [@blotNouveauxVestigesMegalithiques1972a, p. 12].

#### Description

Petit cercle de 3m de diamètre, délimité par 4 pierres profondément enfoncées dans le sol, dont deux atteignent 0,75 m de long. Quatre autres pierres complètent le cercle mais leur mobilité les rend douteuses.

#### Historique

Monument découvert par [J. Blot]{.creator} en [1971]{.date} et confirmé en [2010]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Oneaga]{.coverage} 7 - [Cromlech]{.type}

#### Localisation

Carte 1245 OT. Hendaye-St Jean-de-Luz.

Altitude : 330m.

Il est situé à l'extrémité Sud-Est du sommet de la première petite colline au Sud-Est du mont Oneaga, et qui délimite avec lui le col du même nom.

#### Description

On note un cercle délimité par plus d'une vingtaine de pierres, bien visibles, et en plus grand nombre dans le quart Sud-Est ; il existe une légère dépression centrale.

#### Historique

Monument découvert par [J. Blot]{.creator} en [février 2010]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Oneaga]{.coverage} - [Tumulus]{.type}

#### Localisation

Carte 1245 OT. Hendaye-St Jean-de-Luz.

Altitude : 350m.

On le trouve à l'extrémité Est du sommet de la petite colline (cote 351), située juste à l'Est du mont Oneaga.

#### Description

Tumulus constitué de terres et de pierres, mesurant environ 5m de diamètre et 1m de haut.

#### Historique

Monument découvert par [J. Blot]{.creator} en [février 2010]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Oneaga]{.coverage} 2 - [Dolmen]{.type}

#### Localisation

Carte 1245 OT. Hendaye-St Jean-de-Luz.

Altitude : 382m.

Ce dolmen se trouve exactement au point le plus élevé du mont Oneaga, avec une vue admirable à 360° sur les lointains.

#### Description

On note un tumulus de 8m de diamètre, bien délimité, d'environ une trentaine de centimètres de haut, couvert d'une herbe fine. Quelques rares pierres apparaissent par endroits.

Au centre se trouve une grande dalle, de forme grossièrement triangulaire, dont la base Sud enfoncée dans le sol mesure 1,25m de long ; sa largeur (sa hauteur quand elle était verticale) atteint 1,25m et son épaisseur *apparente* est de 0,10m. C'est tout ce qui reste de la chambre funéraire, orientée probablement Est-Ouest.

#### Historique

Monument découvert par [J. Blot]{.creator} en [janvier 2010]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Oneaga]{.coverage} 3 - [Dolmen]{.type}

#### Localisation

Altitude : 300m.

Il se trouve sur le flanc Nord Nord-Est du mont Onéaga.

Monument assez difficile à trouver, au milieu d'une abondante végétation.

#### Description

On remarque 5 dalles qui émergent du sol, suivant un axe Nord-Sud, et dont l'épaisseur moyenne est de 0,20m à 0,25m. On peut considérer que la paroi Est du monument est formée par un alignement de 4 dalles. La plus au Nord mesure 0,60m de haut et 0,45m à sa base ; la suivante, en allant vers le Sud, mesure 1m de haut et 0,45m à sa base ; la troisième : 0,35m de haut et 0,60m à sa base ; enfin la dernière mesure 0,37m de haut et 0,45m à sa base. De la paroi Ouest, il ne subsiste qu'une dalle de 0,25m de haut et de 0,45m à la base.

#### Historique

Ce monument nous a été signalé par [F. Iturria]{.creator} (ONF), en [septembre 2010]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Subisia]{.coverage} - [Tumulus]{.type}

#### Localisation

Carte 1245 Ouest Saint-Jean-de-Luz.

Altitude : 550m.

Il est situé à une vingtaine de mètres au Sud du petit col, au pied de Soubisia, en bordure de la piste pastorale.

#### Description

Tumulus mixte de terre et de pierres de 6m de diamètre et 0,30m de haut ; monument douteux ?

#### Historique

Monument découvert en [octobre 1969]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Urbisi]{.coverage} 3 - [Dolmen]{.type} [(?)]{.douteux}

#### Localisation

Carte 1245 OT. Hendaye-St Jean-de-Luz.

Altitude : 352 m.

On le trouve à l'extrémité d'un petit replat qui domine l'horizon, à l'Ouest de la principale piste pastorale qui gravit le mont Xoldokogagna en venant de la station filtre du Lezante.

#### Description

Au milieu d'un probable tumulus pierreux peu appréciable, on note un ensemble de 3 dalles de grés rose. Une première, presque rectangulaire, horizontale, mesure 0,90m x 0,80m, la seconde, à l'Ouest, mesure 0,90m de long et 0,45m de large. La troisième, au Nord, mesure 1,40m de long et 0,50m de large. Dolmen douteux.

#### Historique

Monument découvert par [A. Martinez]{.creator} en [2009]{.date}.

-   Rappelons que le *dolmen Urbisi 1* a été publié en 1966 [@chauchatSeptDolmensNouveaux1966, p.108].

    Altitude : 370m.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Urbisi]{.coverage} 2 - [Dolmen]{.type}

#### Localisation

Carte 1245 OT. Hendaye-St Jean-de-Luz.

Altitude : 432m.

Il est situé à une dizaine de mètres à l'Est de la piste de transhumance qui monte du Lezante, sur un terrain en légère pente vers le Nord, mais il semble que ce soit un glissement de terrain venu du Sud qui donne cette impression.

#### Description

Il ne reste que 2 dalles plantées dans le sol, qui devaient délimiter une chambre funéraire mesurant environ 1,80m de long et 0,80 de large, orientée Est Sud-Est, Ouest Nord-Ouest.

La dalle Nord mesure 1,44m de long, 0,76m de haut et 0,12m d'épaisseur. Elle présente des signes d'épannelage très net à sa partie supérieure. La dalle Est, qui lui est perpendiculaire, en est distante de 0,50m ; elle mesure 0,38m de large et 0,68m de haut. On ne note pas de relief tumulaire.

#### Historique

Monument découvert par [J. Blot]{.creator} en [1976]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Urbisi]{.coverage} - [Monolithe]{.type}

#### Localisation

Carte 1245 OT. Hendaye-St Jean-de-Luz.

Altitude : 447m.

Il se trouve à une dizaine de mètres à l'Ouest de la piste pastorale qui monte du Lezante, sur un léger replat de terrain.

#### Description

Il se présente sous la forme d'une belle dalle de grés gris, couchée au sol, orientée Nord-Est Sud-Ouest, dont l'extrémité Nord-Est, taillée en pointe, présente des traces d'épannelage ; sa base est rectangulaire. Il mesure 3m de long, 1,40m de large et 0,40m d'épaisseur en moyenne. Il n'y a pas d'autres pierres visibles dans les alentours.

#### Historique

Monolithe découvert par [J. Blot]{.creator} en [janvier 2010]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Usatuita]{.coverage} 2 - [Dolmen]{.type}

#### Localisation

Carte 1245 Ouest Saint-Jean-de-Luz.

Altitude : 260m.

Le *dolmen n°1* a été publié en 1966 [@chauchatSeptDolmensNouveaux1966, p.10].

Le n°2 est à environ 9m au Sud du n°1 et situé, comme le précédent en bordure de la piste pastorale, sur un terrain en très légère pente vers le Nord, ce qui donne l'impression qu'il est plus épais dans sa moitié Sud.

#### Description

Tumulus pierreux ovale de 7m x 6m de diamètre et 0,60m de haut dans lequel une grande dalle de grès rose, inclinée vers le Nord, forme la limite Sud de ce qui reste de la chambre funéraire que l'on pourrait estimer avoir eu 2,50m de long, 1,70m de large, et qui orientée Est-Ouest semble-t-il.

#### Historique

Dolmen découvert en [janvier 1970]{.date}.

NB : à 3m au Sud Sud-Est du n°1, on peut distinguer, à jour frisant, une sorte de bourrelet circulaire qui pourrait être le vestige d'un tumulus terreux de 6m de diamètre environ ; au centre, une dalle de grès rose de 0,50m de long et 0,70m de haut pourrait être... une simple borne, ou l'unique témoin des montants d'une chambre funéraire disparue.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Urbisi]{.coverage} 4 - [Dolmen]{.type}

#### Localisation

Altitude : 370m.

Sur un replat important, au milieu de la montée qui, du Lezante, va au mont Xoldokogagna. Il est situé à environ 100m au Nord-Est d'une grande bergerie détruite.

#### Description

On note un tumulus circulaire de faible hauteur fait de petites pierres et de dallettes, mesurant environ 5m de diamètre, érigé sur un terrain en très légère pente vers le Nord-Est. La chambre funéraire qui mesurerait probablement 1,40m de long et 0,90m de large est marquée par une légère dépression orientée Nord Nord-Est Ouest Sud-Ouest, dont seule demeure, à sa partie Nord-Est, une petite dalle verticale à sommet arrondi.

#### Historique

Monument découvert par [J. Blot]{.creator} en [mars 2011]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Urbisi]{.coverage} 2 - [Monolithe]{.type}

#### Localisation

Altitude : 460m.

Il est situé à gauche de la piste qui monte du Lezante.

#### Description

Grande dalle de grés triasique gisant sur un sol en pente douce vers le Nord-Est, de forme grossièrement parallélépipédique ; elle mesure 4,43m dans son plus grand axe (orienté Est-Ouest), 2m dans sa plus grande largeur et d'une épaisseur apparente, suivant les endroits, variant de 0,10 à 0,15 m. Des traces d'épannelage apparaissent en de nombreux endroits du bord Nord, et sur la partie Est du bord Sud.

#### Historique

Monument découvert par [P. Badiola]{.creator} en [février 2011]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Urbisi]{.coverage} - [Cromlech]{.type} [(?)]{.douteux}

#### Localisation

Altitude : 370m.

Sur un grand replat de la colline d'Urbisi, et à 100m environ au Nord-Est des ruines d'un grand cayolar ; il est aussi à 5m au Sud-Ouest de *Urbisi 4 - Dolmen*.

#### Description

Une trentaine de dalles et de pierres plus ou moins grandes, souvent très modestes, délimitent un cercle de 12m de diamètre ; les dalles les plus visibles, plus ou moins verticales, se trouvent dans le secteur Sud-Est du cercle bien que dans un certain désordre ; les autres, au ras du sol, ont nécessité un décapage énergique pour être visibles...

#### Historique

Monument découvert par [I. Txintxuretta]{.creator} en [novembre 2011]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Urbisi]{.coverage} 5 - [Dalle couchée]{.type} [(?)]{.douteux}

#### Localisation

Altitude : 472m.

Quand le sentier venant du Lezante atteint son sommet, continuer encore 200m environ, puis aller en direction Sud pour trouver cette dalle à une quarantaine de mètres.

#### Description

Dalle de [grés]{.subject} [triasique]{.subject}, gisant au sol, de forme losangique, mesurant 3,47m dans son plus grand axe, orienté Nord-Est Sud-Ouest, et 1,10m de large. Lorsque nous avons voulu apprécier son épaisseur, nous avons constaté (F. Meyrat) qu'une grande partie du bord Nord-Ouest ne faisait qu'un avec une grande dalle sous-jacente. On note tout le long du bord libre, au Sud-Est, des traces de taille, et semble-t-il aussi des traces de barre à mine au Nord ; il semble enfin que la dalle qui gît à quelques centimètres au Nord-Ouest (1,45m x 0,90m) soit un éclat enlevé à ce même «monolithe». Au vu de l'ensemble de ces constatations, nous éliminons cette importante masse de grés de la catégorie des « monolithes ».

#### Historique

Dalle découverte par [A. Martinez]{.creator} en [avril 2011]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Urbisi]{.coverage} 5 - [Dolmen]{.type}

#### Localisation

Altitude : 390m.

Il est sur ce vaste plateau où se trouvent déjà *Urbisi 2 - Dolmen*... La grande borde en ruine est à une centaine de mètres au Nord Nord-Est, et la piste qui monte vers le sommet au Sud se trouve à 35m à l'Est.

#### Description

Érigé sur un terrain en légère pente vers le Nord, ce monument, sans tumulus visible, ne se distingue que par sa chambre funéraire, orientée Nord-Sud, délimitée par une dalle à l'Ouest, mesurant 1,30m de long, épaisse de 0,14m et haute de 0,18m. Une deuxième dalle, à l'Est, séparée de la précédente de 0,40m à 0,60m représente l'autre élément de cette chambre ; elle mesure 0,70m de long, 0,12m d'épaisseur et 0,14m à 0,22m de haut. Deux autres dalles posées à proximité immédiate, au Nord et au Sud, pourraient avoir fait partie de cette chambre. Compte tenu de ces dimensions, le terme de *ciste* nous paraît plus approprié que celui de dolmen !

#### Historique

Monument découvert par [Badiola P.]{.creator} en [mai 2012]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Urbisi]{.coverage} 6 - [Dolmen]{.type} [(?)]{.douteux}

#### Localisation

Altitude : 455m.

Il se trouve, comme tous les monuments suivants, au milieu d'une abondante végétation d'ajoncs, qui a nécessité un gros travail de dégagement (F. Meyrat) pour étude. (Compte tenu de leur aspect, il serait plus approprié d'appeler ces monuments cistes que dolmens...).

#### Description

Dolmen (?) érigé sur un terrain en pente légère vers l'Est, au milieu d'un tumulus mixte de terre et de pierres de 6m de diamètre, peu marqué ; on voit la chambre funéraire, à grand axe Sud-Est Nord-Ouest, qui mesurerait 2m x 1,10m, délimitée au Sud-Est par une dalle plantée de 0,74m de haut et 0,85m de long ; au Nord et à l'Est par 3 autres dalles couchées au sol. Monument douteux.

#### Historique

Monument découvert par [D. Ibarloza]{.creator} en [mai 2012]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Urbisi]{.coverage} 7 - [Dolmen]{.type}

#### Localisation

Il est à 12m au Sud Sud-Ouest de *Urbisi 6 - Dolmen (?)*.

#### Description

Tumulus mixte peu marqué, de terre et de pierres, de 6m de diamètre et 0,20m de haut, érigé sur terrain en légère pente vers l'Est. La chambre funéraire, à grand axe Est-Ouest, mesure 2,20m x 1m. Elle est délimitée par 2 dalles verticales au Sud, séparées d'une cinquantaine de centimètres ; leur longueur varie entre 0,88m et 0,59m pour une hauteur de 0,50m ; au Nord : 3 dalles verticales dont l'une atteint 0,74m de long et 0,39m de haut.

#### Historique

Monument découvert par [D. Ibarloza]{.creator} en [mai 2012]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Urbisi]{.coverage} 8 - [Dolmen]{.type} [(?)]{.douteux}

#### Description

Pas de tumulus visible. Six dalles délimiteraient une chambre funéraire de 2,20m x 1,10m à grand axe Ouest Nord-Ouest Est Sud-Est, dont quatre couchées au sol et deux dalles verticales : l'une au Sud-Est, l'autre au Nord-Ouest. Monument douteux.

#### Historique

Monument découvert par [D. Ibarloza]{.creator} en [mai 2012]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Urbisi]{.coverage} 9 - [Dolmen]{.type}

#### Localisation

Il est à 7m au Sud Sud-Ouest de *Urbisi 7 - Dolmen*.

#### Description

Tumulus de 6m de diamètre de faible hauteur, érigé sur un terrain en légère pente vers l'Est. La chambre funéraire, à grand axe Est-Ouest, mesure 2,40m x 1,40m. Elle est délimitée par plusieurs dalles dont 5 plantées plus ou moins verticalement, qui se voient au Sud, au Nord, et à l'Est. Cette dernière de 0,47m de long et 0,86m de haut.

#### Historique

Monument découvert par [D. Ibarloza]{.creator} en [mai 2012]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Urbisi]{.coverage} 10 - [Dolmen]{.type}

#### Localisation

Altitude : 450m.

Il est à 20m au Sud Sud-Est de *Urbisi 8 - Dolmen (?)*.

#### Description

Sur un terrain en légère pente vers l'Est est érigé un tumulus de 6m de diamètre environ, de faible hauteur (entre 0,20m et 0,40m) ; une quinzaine de dalles délimitent une chambre funéraire de 2m x 0,80m à grand axe Est-Ouest. Une dizaine de celles-ci sont plantées, plus ou moins verticales, dont une, au Sud mesure 0,97m, 0,47 de haut et 0,11m d'épaisseur. La paroi Est est marquée par plusieurs dalles plantées, et proches les unes des autres.

#### Historique

Monument découvert par [D. Ibarloza]{.creator} en [mai 2012]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Urbisi]{.coverage} 11 - [Dolmen]{.type}

#### Localisation

On le voit à 3m au Nord-Ouest de *Urbisi 8 - Dolmen (?)*.

#### Description

Sur un terrain en légère pente vers l'Est (et sans tumulus apparent), 4 dalles plantées dans le sol délimitent une chambre funéraire de 1,90m x 1,10m environ, à grand axe Nord-Sud. La plus grande dalle, au Nord, mesure 1,47m de long, 0,22m de haut et 0,14m d'épaisseur.

#### Historique

Monument découvert par [Meyrat F.]{.creator} en [juillet 2012]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Urbisi]{.coverage} 12 et 13 - [Dolmens]{.type}

#### Localisation

Ils sont à 35m à l'Ouest Sud-Ouest de *Urbisi 10 - Dolmen* et à 8m au Sud-Est de *Urbisi 14 - Dolmen*.

#### Description

Nous décrivons ensemble ces deux monument, tangents, qu'on aurait pu prendre pour un monument à double chambre funéraire. En fait elles sont distinctes.

-   *Le dolmen D 12*, le plus au Nord, possède une chambre funéraire rectangulaire, bien visible sur trois de ses côtés mesurant 2,50m x 1,30m, à grand axe Sud-Est Nord-Ouest, délimitée par 8 dalles plantées verticalement, de longueurs et hauteurs variables (de 0,88m à 0,57m de long et atteignant au maximum 0,34m de haut).

-   *Le dolmen D 13*, tangent au Sud au précédent, présente une chambre funéraire moins bien conservée, moins géométrique, mesurant environ 2m x 1m, à grand axe Sud-Est Nord-Ouest ; elle est délimitée par 6 dalles plus ou moins inclinées, l'une d'elles, au Sud, atteignant 0,54m de haut.

#### Historique

Monuments découverts par [Meyrat F.]{.creator} en [juillet 2012]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Urbisi]{.coverage} 14 - [Dolmen]{.type}

#### Localisation

Altitude : 460m

Le monument est à une vingtaine de mètres à l'Est du chemin de crête et à 8m au Sud-Est de *Urbisi 12 et 13 - Dolmens*.

#### Description

Sur un tumulus de 4m environ de diamètre avec de nombreuses pierres (au Sud), on voit que 4 dalles plantées délimitent essentiellement une chambre funéraire de 2,10m x 0,90m à grand axe Nord-Est Sud-Ouest. Les deux dalles les plus notables mesurent respectivement : l'une, 0,86m de long, 0,11m d'épaisseur et 0,22m de haut ; l'autre : 0,58m de long, 0,10m d'épaisseur et 0,40m de haut. La limite Est serait marquée par une dalle verticale de 0,49m de long, 0,11m de large et 0,11m de haut.

#### Historique

Monument découvert par [Badiola P.]{.creator} en [mai 2012]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Urbisi]{.coverage} 3 - [Monolithe]{.type}

#### Localisation

Altitude : 465m.

Ce monolithe est situé à environ 30m au Sud Sud-Est de *Urbisi 5 - Dalle couchée (?)*, à l'amorce d'une pente assez marquée qui descend vers le Sud-Est.

#### Description

Grande dalle de grés triasique, de forme grossièrement rectangulaire, à grand axe orienté Sud-Est Nord-Ouest ; elle mesure 5,50m de long, 3m de large et 0,35m à 0,40m d'épaisseur. Elle présente de nombreuses traces d'épannelage, en particulier à son extrémité arrondie Sud-Est.

#### Historique

Dalle découverte par [F. Meyrat]{.creator} en [avril 2011]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Urbisi]{.coverage} 6 - [Pierre couchée]{.type} [(?)]{.douteux}

#### Localisation

Altitude : 470m.

Cette pierre gît sur un sol en pente douce vers le Nord-Est, à environ 30m à gauche de la piste ascendante.

#### Description

Elle affecte la forme d'un parallélépipède rectangle à grand axe Est Nord-Est Ouest Sud-Ouest, et mesure 1,70m de long, 0,85m de large et son extrémité Ouest est plus étroite (0,33m) ; son épaisseur moyenne est d'environ 0,40m. Pas de traces d'épannelage visibles semble-t-il.

#### Historique

Pierre découverte par [P. Badiola]{.creator} en [février 2011]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Urbisi]{.coverage} 7 - [Pierre plantée]{.type}

#### Localisation

Altitude : 390m.

Pierre plantée isolée, à flanc de colline en pente vers le Nord.

#### Description

Pierre plantée verticalement, mesurant 1,03m de haut, 0,80m à sa base, et 0,34m d'épaisseur. Pas de trace d'épannelage visible. On distingue très nettement une structure de soutient faite de pierres entassées à la base de cette dalle, côté Nord.

#### Historique

Pierre découverte par [I. Txintxuretta]{.creator} en [novembre 2011]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Urbisi]{.coverage} 8 - [Pierre plantée]{.type}

#### Localisation

Altitude : 365m.

Dalle plantée comme *Urbisi 7 - Pierre plantée*, au flanc Nord de la colline, sur terrain en pente.

#### Description

Dalle de grès, rectangulaire, de 0,90m de haut, 0,50m à sa base et 0,20m d'épaisseur. Elle est inclinée vers le Nord et deux pierres de soutient y ont été enfouies à sa base.

#### Historique

Pierre découverte par [I. Txintxuretta]{.creator} en [novembre 2011]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Urbisiko erreka]{.coverage} - [Ciste]{.type}

#### Localisation

Altitude : 256m.

#### Description

Cette très modeste ciste est érigée sur un terrain en pente vers l'Ouest. Trois dalles limitent une chambre funéraire, 1m de long et 0,60m de large, orientée selon un axe Nord Nord-Ouest Sud Sud-Est. La dalle au Sud-Est mesure 0,47m à sa base, 0,27m de haut et 0,19m d'épaisseur ; celle au Sud-Ouest mesure 0,90m de long, et celle au Nord-Est, 0,40m.

#### Historique

Monument découvert par [I. Txintxuretta]{.creator} en [novembre 2011]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Urbisiko erreka]{.coverage} - [Cromlech]{.type}

#### Localisation

Altitude : 270m.

Situé sur un replat de la dimension du monument.

#### Description

Entre 15 et 20 pierres, au ras du sol (très plat), délimitent un cercle de 8m de diamètre.

#### Historique

Monument découvert par [Meyrat F.]{.creator} en [janvier 2011]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Urbisiko erreka]{.coverage} 1 - [Dolmen]{.type}

#### Localisation

Altitude : 300m.

#### Description

Tumulus de terre et de pierres de 6m de diamètre et 0,30m de haut. Au centre, une chambre funéraire d'environ 1m de long et 0,60m de large, orientée, semble-t-il, Nord-Sud et essentiellement délimitée au Nord par une dalle de 0,60m x 0,30m et à l'Ouest, par une autre de 0,6m x 0,15m et 9 centimètres de haut. Trois autres petites dalles délimitent le côté Est.

#### Historique

Monument découvert par [I. Txintxuretta]{.creator} en [novembre 2011]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Urbisiko erreka]{.coverage} 2 - [Dolmen]{.type}

#### Localisation

Altitude : 300m.

Il se trouve à une quarantaine de mètres au Nord-Ouest de *Urbisiko erreka 1 - Dolmen*.

#### Description

Tumulus essentiellement pierreux de 8m de diamètre, et d'une hauteur variant entre 0,30m et 1m. Au centre, une chambre funéraire de 1m de long et 0,70m de large, orientée Nord-Ouest Sud-Est, et délimitée par 5 dalles de taille variable. Au Sud-Ouest, une dalle de 0,50m de long et 0,13m d'épaisseur, au ras du sol ; au Sud-Est, une autre dalle de 0,40m de long, 0,10m d'épaisseur et 0,58m de haut ; trois autres dalles au Nord-Ouest et au Nord-Est, dont une de 0,40m de hauteur, complètent cette chambre.

#### Historique

Monument découvert par [I. Txintxuretta]{.creator} en [novembre 2011]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Urbisiko erreka]{.coverage} 3 - [Dolmen]{.type}

#### Localisation

Altitude : 241m.

On trouve ce monument sur une petite éminence, à gauche du chemin qui va d'Usatuita au Lézante.

#### Description

Tumulus de 12m de diamètre, de terre et de pierres, érigé sur un mouvement naturel de terrain, en pente vers le Nord, ce qui lui donne une hauteur plus importante au Nord (1,50m) qu'au Sud (0,40m). Il semble qu'il existe un péristalithe d'une douzaine de pierres.

Au centre du tumulus reste de la chambre funéraire une grande dalle de grés local, orientée selon un axe Ouest Nord-Ouest Est Sud-Est, et inclinée vers le Nord, mesurant 2,20m de long, 0,90m dans sa plus grande largeur, et 0,20m d'épaisseur en moyenne ; deux autres dalles de taille plus petite, au Nord, pourraient évoquer soit des restes de la chambre funéraire, soit faire partie du tumulus...

À la périphérie Ouest de ce dernier on voit une dalle en partie enfouie dans le sol, mesurant 1,80m de long et 1,70m de large ; dalle de couverture ? montant arraché ?

#### Historique

Monument découvert par [I. Txintxuretta]{.creator} en [novembre 2011]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Urbisiko erreka]{.coverage} - [Pierre dressée]{.type}

#### Localisation

Altitude : 300m.

Elle se trouve à environ 100m à l'Ouest du tumulus 5, sur un petit plateau d'une centaine de mètres de long, où l'on trouve aussi *Urbisiko erreka 1 - Dolmen* et *Urbisiko erreka 2 - Dolmen*.

#### Description

Bloc de grés trapu, posé ou planté dans le sol, mesurant 1,40m à la base, 0,60m de large et 1,04m de haut. Sa périphérie ne présente aucune trace d'épannelage. Il peut tout aussi bien s'agir d'un bloc d'éboulis...

#### Historique

Pierre découverte par [I. Txintxuretta]{.creator} en [novembre 2011]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Urbisiko erreka]{.coverage} 1 - [Tumulus]{.type}

#### Localisation

Altitude : 290m.

Il est situé au-dessus de *Urbisiko erreka - Cromlech*.

#### Description

Tumulus de terre et de nombreuses pierres, de 3m de diamètre environ et 0,30m de haut.

#### Historique

Monument découvert par [I. Txintxuretta]{.creator} en [novembre 2011]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Urbisiko erreka]{.coverage} 2 - [Tumulus-cromlech]{.type}, [Cromlech]{.type} ([?]{.douteux})

#### Localisation

Altitude : 255m.

La ciste se trouve à 25m au Sud, et il est lui-même à 40m au-dessus et au Nord de *D 3*.

#### Description

Tumulus de 6m de diamètre et 0,60m de haut ; on voit une vingtaine de pierres disposées de manière plus ou moins circulaire : tumulus-cromlech ou cromlech ou (?) monument douteux.

#### Historique

Monument découvert par [F. Meyrat]{.creator} en [janvier 2012]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Urbisiko erreka Zutarria]{.coverage} - [Dalle couchée]{.type}, [Monolithe]{.type}, [Dolmen]{.type} [(?)]{.douteux}

#### Localisation

Altitude : 365m.

Sur un petit replat incliné vers l'Ouest.

#### Description

Belle dalle de grés rose, gisant selon un axe Est-Ouest, de forme rectangulaire, mesurant 1,90m de long, 0,94m de large et 0,32m d'épaisseur. Il semble qu'il y ait des traces d'épannelage sur les bords Nord et Est. Cette dalle n'est pas à plat sur le sol mais repose sur la périphérie Ouest d'un tumulus (naturel ou non ?), mesurant environ 3,70m de diamètre et 0,50m de haut. S'agit-il donc d'un monolithe, d'une simple dalle couchée, ou d'une table dolménique par exemple ?

#### Historique

Dalle découverte par [I. Txintxuretta]{.creator} en [novembre 2011]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Xoldokocelay]{.coverage} 3 - [Dolmen]{.type}

#### Localisation

Carte 1245 OT. Hendaye-St Jean-de-Luz.

Altitude : 365m.

Ce dolmen se trouve sur un replat, au flanc Nord du mont Xoldokogagna, à environ 150m à l'Est Nord-Est de *Xoldokogaina 2 - Dolmen*, lui-même situé à une trentaine de mètres au Nord de *Xoldokogaina 1 - Dolmen*.

#### Localisation et description de ces deux dolmens dans [@chauchatSeptDolmensNouveaux1966, p.106].

#### Description

On note un important tumulus pierreux, de 8m de diamètre et 0,80m de haut, formé de nombreux blocs de grés de tailles variables, au centre duquel une dépression d'environ 4,20m de long et 1,20m de large représente ce qui reste d'une chambre funéraire, où apparaissent encore de nombreuses dalles de [grés]{.subject}, dont l'aspect et le désordre montrent que ce monument a fait l'objet de fouilles sauvages dans le passé.

Il semble, en fait, qu'on puisse distinguer deux chambres funéraires distinctes, dans le prolongement l'une de l'autre, alignées suivant un axe Nord-Est Sud-Ouest, séparées par une dalle verticale de 0,73m de long, 0,50m de haut et 0,12m d'épaisseur.

La chambre funéraire située à l'extrémité Sud-Ouest mesure environ 2m de long et paraît avoir été délimitée à l'arrière par une dalle de chevet triangulaire, qui gît actuellement sur le sol ; celle-ci mesure 0,70m à sa base et 0,97m de long, (ou de haut, quand elle était verticale). La paroi latérale, au Nord-Ouest est délimitée par 3 dalles verticalement enfoncées dans le sol, dont les 2 plus importantes, dans le prolongement l'une de l'autre, mesurent respectivement 0,95m et 0,92m de long, et 0,15m d'épaisseur.

La deuxième chambre, dans le prolongement de la première, mesure environ 2m de long et en est séparée par la dalle verticale déjà citée. Sa paroi Nord-Ouest est délimitée par 4 dalles de taille variable ; trois autres dalles de taille plus modeste matérialisent la paroi Sud-Est.

Il est particulièrement intéressant de noter l'existence de ces deux chambres funéraires dans le prolongement l'une de l'autre ; c'est la première fois que nous voyons ceci en pays Basque Nord ; il nous est déjà arrivé de voir deux chambres funéraires, mais contiguës latéralement, comme pour le dolmen de Berdaritz (commune des Aldudes) ou celui d'Abrakou, *Abrakou - Dolmen*.

#### Historique

Monument découvert par [Goyo Mercader]{.creator} en [1998]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Xoldokocelay]{.coverage} 11 - [Dolmen]{.type}

#### Localisation

Altitude : 440m.

Ce monument se trouve à 20m au Nord Nord-Ouest de *Xoldokocelai 10 - Dolmen*.

(commune de [Biriatou]{.spatial}).

#### Description

Chambre funéraire de 1,90m de long, 1,08m de large, orientée Nord-Sud, érigée sur un terrain en légère pente vers le Nord. Elle est délimitée par un ensemble de dalles, en grande partie enfoui sous une épaisse végétation : 2 à l'Est, 1 au Nord, 3 à l'Ouest et une, semble-t- il, au Sud, totalement cachée.

#### Historique

Monument découvert en [avril 2010]{.date} par [F. Meyrat]{.creator}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Xoldokocelay]{.coverage} 11 bis - [Dolmen]{.type}

#### Localisation

Altitude : 440m.

Il est à quelques dizaines de centimètres au Nord de Xoldokocelai 11 - Dolmen.

#### Description

Une dizaine de dalles plus ou moins en désordre paraissent délimiter une chambre funéraire rectangulaire, à grand axe Nord-Sud.

#### Historique

Monument découvert par [F. Meyrat]{.creator} en [février 2010]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Xoldokocelay]{.coverage} 14 - [Dolmen]{.type}

#### Localisation

Altitude : 380m.

Il est situé sur un petit replat à quelque distance de la rive droite d'un petit ruisseau.

#### Description

La chambre funéraire, rectangulaire à grand axe orienté Nord-Est Sud-Ouest, mesure 2,30m de long et 1,50m de large. Elle est délimitée par une douzaine de dalles de modestes dimensions. La paroi Nord est délimitée par deux (ou trois ?) dalles dont l'une mesure 0,47m de long et 0,47m de haut, l'autre 0,45m de long et 0,40m de haut. À l'Ouest, on note deux dalles debout et une couchée, aux dimensions semblables aux précédentes ; la paroi Sud est essentiellement marquée par quatre dalles dont les longueurs varient de 0,30m à 0,50m et les hauteurs de 0,10m à 0,30m. Enfin la paroi Est est délimitée par au moins trois dalles, dont les longueurs varient de 0,40m à 0,53m et la hauteur de 0,20m à 0,40m. Cette chambre est inscrite dans un tumulus d'environ 4m de diamètre, essentiellement pierreux, fait de petites dalles de grés local.

#### Historique

Monument découvert par [P. Badiola]{.creator} en [février 2011]{.date}, et dégagé par F. Meyrat.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Xoldokocelay]{.coverage} 15 - [Dolmen]{.type}

#### Localisation

Altitude : 355m.

Situé à 20m à l'Ouest et au-dessus de Xoldokocelay 2 - Tumulus.

#### Description

Tumulus très peu marqué avec peut-être une ébauche de péristalithe. Au centre, la chambre funéraire, orientée Sud-Est Nord-Ouest, semble essentiellement délimitée au Nord-Est par une grande dalle inclinée vers l'Est, mesurant 1,53m dans sa plus grande dimension, 1,15m de large et 0,35m d'épaisseur. Six à sept autres petites dalles complèteraient cette chambre, dont une dalle au Sud-Est, verticalement plantée, mesurant 0,50m à sa base et 0,30m de haut.

#### Historique

Monument découvert par [I. Txintxuretta]{.creator} en [novembre 2011]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Xoldokocelay]{.coverage} 2 - [Tumulus]{.type}

#### Localisation

Altitude : 350m.

Sur un petit replat à droite d'un sentier qui monte après la traversée d'un petit riu. Il est à 20m à l'Est et en contre-bas de *Xoldokocelay 15 - Dolmen*.

#### Description

Tumulus de faible hauteur, fait de terre et de pierres, mesurant 5m de diamètre environ. Au centre, un ensemble de 5 pierres, dont les dimensions avoisinent 0,30m à 0,40m de long, pourrait représenter la ciste centrale. Il semble qu'on puisse distinguer une ébauche de péristalithe dans le secteur Nord-Est du tumulus.

#### Historique

Monument découvert par [I. Txintxuretta]{.creator} en [novembre 2011]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Xoldokocelay]{.coverage} 3 - [Tumulus]{.type}

#### Localisation

Altitude : 460m.

Il est érigé sur un terrain en pente légère vers le Nord et à 250m environ au Nord de la borne géodésique du mont Xoldokogana.

#### Description

Tumulus de terre et quelques pierres, de 6m de diamètre et 0,50m de haut en moyenne. Quelques dalles couchées apparaissent à sa périphérie sans qu'on puisse parler de péristalithe.

#### Historique

Tumulus découvert par [Meyrat F.]{.creator} en [avril 2012]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Xoldokogaina]{.coverage} - [Dalle plantée]{.type} [(?)]{.douteux}

#### Localisation

Altitude : 460m.

Dalle située sur la pente Nord du mont Xoldokogaina, à une centaine de mètre environ au-dessous de *Xoldokogaina 1 - Dolmen*. Cette dalle est érigée sur un « tumulus » d'environ 3,60m de diamètre que traverse une piste pastorale orientée Est-Ouest. La dalle, plantée selon un axe Ouest-Est, est légèrement inclinée vers le Nord.

#### Description

Elle est en grés local, mesure 0,78m de haut ; sa base, solidement implantée mesure 0,40m de long et l'épaisseur varie de 0,17m à la base à 0,9m au sommet. On distingue sur ce dernier, taillé en pointe, une croix gravée en profondeur, au 2 branches égales (7 centimètres chacune), et orientées selon les 4 points cardinaux ; enfin tout le bord Ouest de la dalle présente des traces évidentes d'épannelage. Le « tumulus », qui se fond dans son secteur Sud avec la piste, est, par ailleurs, nettement visible. Enfin, le sol au pied de la dalle, côté Nord, est creusé, très probablement par les ovins, ce qui a favorisé, avec la pente, l'inclinaison de la dalle. Borne pastorale ? vestige de dolmen ? (Peu probable).

#### Historique

Dalle découverte par [Blot J.]{.creator} en [octobre 2010]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Xoldokogaina]{.coverage} 1 - [Dolmen]{.type}

#### Localisation

Altitude : 472m.

Il est situé à 18m au Nord de la piste qui parcours le sommet du plateau et va ensuite vers le Lezante.

#### Description

Une chambre funéraire de 1,60m de long et 0,90m de large orientée Ouest-Est.

Est délimitée au Sud par 3 dalles dont l'une encore dressée, mesure 0,57m à sa base, 0,81m de haut et 0,13m d'épaisseur ; deux autres dalles complètent cette paroi, dont l'une mesure 0,60m de long, et 0,37m de haut. La paroi Nord est représentée par une dalle rectangulaire couchée et en partie enfouie dans le sol, dont les mensurations apparentes sont de 1,08m de long et 0,68m de large. On ne note pas de tumulus.

#### Historique

Monument découvert par [J. Blot]{.creator} en [avril 2010]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Xoldokogaina]{.coverage} 2 - [Dolmen]{.type} [(?)]{.douteux}

#### Localisation

Altitude : 471m.

Il est situé à environ 150m au Sud-Est de *Xoldokogaina 1 - Dolmen*.

#### Description

Une dalle de grés rose, presque horizontale semble être le couvercle du monument : elle mesure 1,65m de long, 1,17m de large, et 0,22m d'épaisseur ; elle est orientée Sud-Ouest Nord-Est. La paroi du côté Nord-Ouest est formée par une dalle plantée de 1m de long et 0,35m de haut, très inclinée vers l'extérieur, et d'une deuxième, beaucoup plus petite sous la dalle horizontale elle-même. Peut-être y-a-t-il une ébauche de tumulus ? Il *ne semble pas* que cet ensemble fasse partie des remaniements du terrain dus aux carriers...

#### Historique

Monument (douteux) découvert par [F. Meyrat]{.creator} en [avril 2010]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Xoldokogaina]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 471m.

Il est situé à peu près au milieu entre *Xoldokogaina 2 - Dolmen (?)* et *Xoldokogaina 1 - Dolmen*.

#### Description

Petit tumulus circulaire de terre et de pierres de 2,90m de diamètre et 0,30m de haut, qui paraît, là encore, tout à fait indépendant de tous les remaniements dus aux carriers.

#### Historique

Monument découvert par [F. Meyrat]{.creator} en [avril 2010]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Xoldokogaina]{.coverage} - [Dalle couchée]{.type} [(?)]{.douteux}

#### Localisation

Altitude : 460m.

#### Description

Cette dalle de grés local, de forme rectangulaire, repose sur une élévation de terre, de 0,60m de haut, elle mesure 1,78m de long, 0,47m à 0,50m de large. Elle est brisée dans son tiers Sud-Est et un gros fragment est absent dans le quart Nord-Est. Des traces d'épannelage sont visibles sur ses bords Nord et Sud.

#### Historique

Dalle découverte par [Badiola P.]{.creator} en [mai 2012]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Xuanen Borda]{.coverage} 1 - [Tumulus]{.type}

#### Localisation

Altitude : 159m.

Il est érigé dans un élargissement de la vallée, sur le bord gauche, c'est à dire Nord d'une bretelle que détache la piste qui monte vers Arlepoa, à l'endroit où cette bretelle vient de traverser le Larrungo erreka et chemine sur sa rive droite.

#### Description

Tumulus ovale, érigé sur terrain plat, à grand axe Nord-Est Sud-Ouest, mesurant 6m x 5m et plus d'1m de haut.

Il est constitué essentiellement d'un amas pierreux, recouvert d'une abondante végétation ; on voit quelques éléments rocheux plus volumineux à la périphérie en particulier dans les quadrants Sud-Ouest et Nord-Est : éléments d'un péristalithe ? S'agit-il d'un tumulus dolménique ?

#### Historique

Monument découvert par [Blot J.]{.creator} en [avril 2014]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Xuanen Borda]{.coverage} 2 - [Tumulus]{.type}

#### Localisation

À 15m au Sud Sud-Est de *Xuanen Borda 1 - Tumulus* ; il est de l'autre côté de la piste qui vient de traverser le rio, c'est à dire au Sud de celle-ci, et sur son bord droit en montant vers Xuanen borda.

#### Description

Tumulus circulaire, plus modeste que *Xuanen Borda 1 - Tumulus*, mesurant environ 2m de diamètre et 0,55m de haut. Il est essentiellement constitué de pierres, et semblerait être entouré d'un péristalithe dont 6 pierres particulièrement visibles.

#### Historique

Monument découvert par [Blot J.]{.creator} en [avril 2014]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Xapatainbaita]{.coverage} - [Camp protohistorique]{.type}

#### Localisation

Il est situé à droite de la route qui relie Ciboure à Olhette en passant par Béthanie, au niveau d'un virage, dans une descente. Des lignes à haute tension passent juste au-dessus.

Altitude : 104m.

Avant la construction des habitations actuellement visibles, on remarquait d'importantes levées de terre caractéristiques d'un camp protohistorique, dont il ne reste que des traces actuellement.

#### Historique

Ce camp a été découvert par [Blot J.]{.creator} en [1976]{.date}, et ne semble pas avoir été décrit par F. Gaudeul.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Ziburumendi]{.coverage} - [Monolithe]{.type}

#### Localisation

Altitude : 353m.

Il est situé au sommet de la piste qui monte directement du col du Grand Descarga vers Ziburumendi, sur le premier replat qui domine un col situé à l'Ouest. Il est à 2m à l'Ouest de cette piste.

#### Description

Bloc de grés triasique en forme de parallélépipède rectangle, couché au sol selon un axe Est Nord-Est Ouest Sud-Ouest, et présentant deux extrémités effilées. Il mesure 2,67m de long, 0,41m dans sa plus grande largeur, et 0,18m à ses deux extrémités. Son épaisseur moyenne est de 0,55m. On note de nombreuses traces d'épannelage : à ses deux extrémités, sur sa face supérieure, et sa face Nord-Ouest.

#### Historique

Monolithe découvert par [A. Martinez]{.creator} en [2014]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Ziburukomendia]{.coverage} - [Dolmen]{.type}

#### Localisation

Carte 1245 Ouest Saint-Jean-de-Luz.

Altitude : 310m.

Il est situé sur un replat au flanc Sud-Ouest du mont appelé « Montagne de Ciboure », en surplomb du col du Grand Descarga qui la sépare de Larraun (La Rhune).

#### Description

Tumulus mixte de terre et de pierres (petits blocs de poudingue), mesurant 6m de diamètre et 0,40m de haut. Au centre une chambre funéraire rectangulaire à grand axe Sud Sud-Ouest, Nord Nord-Est ; elle est délimitée par 4 dalles de grès triasique perpendiculaires les unes aux autres : à l'Est, une dalle 0,90m de long et 0,50m de haut, à l'Ouest une dalle de 1,20m de long et 0,60m de haut ; enfin au Nord comme au Sud une dalle de 0,50m de long et 0,30m de haut. Il n'y a pas de couvercle visible.

#### Historique

Dolmen découvert en [mars 1973]{.date}. Ce monument a été complètement rasé lors de la plantation de conifères effectuée en 1974.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Zizkuitz]{.coverage} - [Dalle couchée]{.type} [(?)]{.douteux}

#### Localisation

Altitude : 643m.

Cette dalle se trouve en bordure Sud-Est de la piste qui, au flanc Ouest de Larrun, se rend à Zizkuitz.

#### Description

De forme parallélépipédique, elle mesure 1,50m de long, 0,60m de large et pourrait présenter des traces d'épannelage à son bord Sud-Ouest.

#### Historique

Dalle découverte par [Badiola P.]{.creator} en [mai 2012]{.date}.

</section>

<section class="monument">

### [Urrugne]{.spatial} - [Zubizia]{.coverage} - [Pierre plantée]{.type} [(?)]{.douteux}

#### Localisation

Altitude : 540m.

Elle se trouve sur terrain en pente à environ 200m à l'Est et au-dessus de la BF 21.

#### Description

Pierre de grés de forme triangulaire (sur ses faces Est et Ouest), haute de 1,15m et épaisse de 0,50m. À son sommet une petite encoche. Elle est dans un contexte de carrier.

#### Historique

Pierre trouvée par [Badiola P.]{.creator} en [avril 2012]{.date}.

</section>

<div id="biblio">
<h6 class="unnumbered" id="bibliographie">Bibliographie</h6>
<div id="refs">
    
</div>
</div>