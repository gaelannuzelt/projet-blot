<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){  
    $('#TOC ul>li').click(function () {  
        $('#TOC ul>li').not(this).find('ul').hide();  
        $(this).find('ul').toggle();  
    });  
}); 
</script>

<script type="text/javascript">
function toggle(id) {
    var n = document.getElementById(id);
    n.style.display =  (n.style.display != 'none' ? 'none' : '' );
    }
</script>

<script type="text/javascript">

var links = document.querySelectorAll('div.csl-entry a');
for (var i = 0, linksLength = links.length; i < linksLength; i++) {
    if (!links[i].target) {
        
            links[i].target = '_blank';
    
    }
}
</script>
