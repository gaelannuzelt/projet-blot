<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" doctype-system="about:legacy-compat" encoding="UTF-8" indent="yes" />
    <xsl:template match="/">
        <html lang="fr">

        <head>
            <meta charset="utf-8" />
            <title>Tableau des notices Blot</title>
            <link rel="icon" type="image/png" href="UPPA_logo-rose.png" />
            <link rel="stylesheet" href="styleChercheur.css" />
        </head>

        <body>

            <table border="1">
                <tr>
                    <th>#</th>
                    <th>title</th>
                    <th>spatial</th>
                    <th>coverage</th>
                    <th>type</th>
                    <th>date</th>
                    <th>creator</th>
                    <th>subject</th>
                    <th>douteux</th>
                    <th>ref biblio</th>
                    <th>description</th>
                </tr>
                <xsl:variable name="sep" select="', '"/>
                <xsl:for-each select="/notices/notice">
                    <xsl:sort select="title" order="ascending" />
                    <tr>
                        <td style="font-family:monospace;font-size:0.9em;">
                            <xsl:value-of select="position()" />
                        </td>
                        <td>
                            <xsl:value-of select="title" />
                        </td>
                        <td>
                            
                            <xsl:for-each select="spatial">
                                <xsl:if test="position() > 1"><xsl:copy-of select="$sep"/></xsl:if>
                                <xsl:value-of select="."/>
                            </xsl:for-each>
                        </td>
                        <td>
                            
                            <xsl:for-each select="coverage">
                                <xsl:if test="position() > 1"><xsl:copy-of select="$sep"/></xsl:if>
                                <xsl:value-of select="."/>
                            </xsl:for-each>
                        </td>
                        <td>
               
                            <xsl:for-each select="type">
                                <xsl:if test="position() > 1"><xsl:copy-of select="$sep"/></xsl:if>
                                <xsl:value-of select="."/>
                            </xsl:for-each>
                        </td>
                        <td>
                            <xsl:for-each select="date">
                                <xsl:if test="position() > 1"><xsl:copy-of select="$sep"/></xsl:if>
                                <xsl:value-of select="."/>
                            </xsl:for-each>
                        </td>
                        <td>
                            
                            <xsl:for-each select="creator">
                                <xsl:if test="position() > 1"><xsl:copy-of select="$sep"/></xsl:if>
                                <xsl:value-of select="."/>
                            </xsl:for-each>
                        </td>
                        <td>
                            
                            <xsl:for-each select="subject">
                                <xsl:if test="position() > 1"><xsl:copy-of select="$sep"/></xsl:if>
                                <xsl:value-of select="."/>
                            </xsl:for-each>
                        </td>
                        <td>
                            
                            <xsl:for-each select="douteux">
                                <xsl:if test="position() > 1"><xsl:copy-of select="$sep"/></xsl:if>
                                <xsl:value-of select="."/>
                            </xsl:for-each>
                        </td>
                        <td>
                            
                            <xsl:for-each select="ref">
                                <xsl:if test="position() > 1"><xsl:copy-of select="$sep"/></xsl:if>
                                <xsl:value-of select="."/>
                            </xsl:for-each>
                        </td>
                        <td>
                            <xsl:value-of select="description" />
                        </td>


                    </tr>
                </xsl:for-each>
            </table>
        
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>