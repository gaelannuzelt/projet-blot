# Mauléon

### [Mauléon]{.spatial} - [Tibarenne]{.coverage} Est - [Tumulus]{.type} (DETRUIT)

Localisation

Altitude : 422m.

Historique

Il était situé à 10m au Nord-Ouest du premier émetteur télé ayant été érigé au sommet de la colline dénommée Tibarenne, dominant Mauléon ; il était le seul émetteur au moment de notre découverte de ce tumulus et de sa publication : [@blotNouveauxVestigesProtohistoriques1975, p.123].

Depuis, deux autres émetteurs ont été construits, ainsi qu'une route d'accès qui a complètement rasé ce magnifique tumulus qui mesurait 17m de diamètre et 1m de haut, avec une excavation centrale de 2m de large et d'une dizaine de centimètre de profondeur.

Il n'en reste maintenant qu'une très faible ondulation au niveau du tracé routier...

[1975]{.date}, [J. Blot]{.creator}.
