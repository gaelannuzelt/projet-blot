# Saint-Esteben

### [Saint-Esteben]{.spatial} - [Atxapuru]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 403m.

Il est situé à une cinquantaine de mètres au Nord du point sommital de la petite colline allongée en direction Nord-Sud, dénommée *Atxapuru*.

Description

Tumulus pierreux circulaire de 2,50m de diamètre et 0,20m de haut ; on note à sa surface une quinzaine de petits blocs calcaire blanc bien visibles.

Historique

Tumulus découvert par [F. Meyrat]{.creator} en [août 2015]{.date}.
