# Saint-Jean-de-Luz

### [Saint-Jean-de-Luz]{.spatial} - [Zamarina]{.coverage} - [Tumulus]{.type}

Localisation

Carte 1245 Ouest Saint-Jean-de-Luz.

Altitude : 80m.

Il est situé à gauche du chemin qui mène à la ferme Zamarina, en face de l'ancien dépôt d'ordures de la ville, à 20m au Nord-Est de la borne IGN 84.

Description

Tumulus de terre de 25m de diamètre et 1,50m de haut.

Historique

Découvert en [janvier 1968]{.date}. Il a été depuis rasé par de nombreux labours.
