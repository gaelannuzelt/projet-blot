# Ahaxe

### [Ahaxe]{.spatial} - [Bilgotza Mendebalde]{.coverage} 1 - [Tumulus-cromlech]{.type}

#### Localisation

Altitude : 720m.

On le trouve à 500 mètres à l'Ouest-Nord-Ouest de *Bilgotza Mendebalde 2 - Cromlech* et *Bilgotza Mendebalde 3 - Cromlech*, sur une petite éminence naturelle, à quelques mètres au Sud de la piste.

#### Description

On distingue nettement un tumulus d'environ une dizaine de mètres de diamètre, et 0,40 à 050m de hauteur; sa périphérie semble bien être délimitée par un péristalithe constitué de nombreuses pierres plus ou moins bien visibles. À environ 1m à l'Est-Sud-Est du centre géométrique, on peut voir 3 pierres de volume différent, la plus volumineuse affecte grossièrement la forme d'un **T** ; elle mesure 1,40m de long et 1,10m au niveau de sa « barre transversale ». Ce qui est très curieux et que nous avons découvert lors de notre passage (le 27 décembre 2010) c'est une grande structure rectangulaire qui prend appui sur cette grosse pierre dans son angle Sud-Ouest. Le tracé est constitué par de nombreux petits blocs de pierre bien visibles au ras du sol, qui délimitent un rectangle de 7,20m de long et 3m de large, orienté Nord-Nord-Est Sud-Sud-Ouest. Le sol est parfaitement plat à l'intérieur de cette surface qui ne ressemble en rien aux vestiges d'une chambre funéraire ; nous n'avons, à l'heure actuelle, aucune interprétation à proposer.

#### Historique

Tumulus-cromlech découvert par [A. Martinez Manteca]{.creator} le [12 décembre 2010]{.date}.

### [Ahaxe]{.spatial} - [Bilgotza Mendebalde]{.coverage} 2 - [Cromlech]{.type}

#### Localisation

Il est à 12m au Sud-Ouest de *Bilgotza Mendebalde 1 - Tumulus-cromlech*, sur terrain plat.

#### Description

Cercle de 10m de diamètre environ, délimité par une vingtaine de pierres dont seule la douzaine du secteur sud sont très nettement visibles.

#### Historique

Monument découvert le [12 décembre 2010]{.date} par [A. Martinez Manteca]{.creator}.

### [Ahaxe]{.spatial} - [Bilgotza Mendebalde]{.coverage} 3 - [Cromlech]{.type}

#### Localisation

Situé à 5m au Sud-Est de *Bilgotza Mendebalde 2 - Cromlech*.

#### Description

Cinq pierres, au ras du sol, semblent bien faire partie d'un cercle de 6 mètres de diamètre environ.

#### Historique

Monument découvert par [A. Martinez Manteca]{.creator} le [12 décembre 2010]{.date}.

### [Ahaxe]{.spatial} - [Bilgotza Mendebalde]{.coverage} 4 - [Cromlech]{.type}

#### Localisation

Il est tangent au Nord-Nord-Ouest, au tumulus-cromlech du même nom, *Bilgotza Mendebalde 1 - Tumulus-cromlech*.

#### Description

Une vingtaine de pierres, au ras du sol, délimitent un cercle de près de 6mètres de diamètre. Il semblerait que deux pierres particulièrement visibles appartenant au péristalithe du tumulus-cromlech, soient aussi communes à ce cromlech.

#### Historique

Monument découvert par [Blot, J.]{.creator} le [27 décembre 2010]{.date}.

### [Ahaxe]{.spatial} - [Bilgotza Mendebalde]{.coverage} - [Tumulus]{.type}

#### Localisation

Altitude : 715m.

Il est situé à 6m au Nord-Est de la piste.

#### Description

Petit tumulus, peu visible, de terre et de petites pierre mesurant 3m de diamètre et 0,30m de haut.

#### Historique

Tumulus découvert en [février 2011]{.date} par [A. Martnez]{.creator}.

### [Ahaxe]{.spatial} - [Bilgotza Mendebalde]{.coverage} 2 - [Tumulus]{.type}

#### Localisation

Coordonnées : situé à 15m au Sud de *Bilgotza Mendebalde - Tumulus*, de l'autre côté de la piste.

#### Description

Petit tumulus peu visible, de terre et de petites pierres, mesurant 2,10m de diamètre et quelques centimètres de haut.

#### Historique

Tumulus découvert par le [groupe Hilharriak]{.creator} en [février 2011]{.date}.

### [Ahaxe]{.spatial} - [Bilgotza Mendebalde]{.coverage} 3 - [Tumulus]{.type}

#### Localisation

À 23m à l'Ouest de *Bilgotza Mendebalde - Tumulus*, en plein milieu de la piste

#### Description

Petit tumulus de faible hauteur, mesurant prés de 3m de diamètre, constitué de terre et de pierres.

#### Historique

Tumulus découvert par [Blot J.]{.creator} en [mars 2012]{.date}.

### [Ahaxe]{.spatial} - [Buluntza]{.coverage} 1 - [Tumulus]{.type}

Ce monument est à différencier du tertre d'habitat du même nom, qui se trouve à 60m à l'Ouest-Nord-Ouest du *dolmen de Buluntza*.

#### Localisation

Altitude : 724m.

Il est situé au sommet du ressaut qui domine la descente menant au col de Buluntza quand on vient de Bilgotza, et à une centaine de mètres au Sud-Ouest des monuments de Bilgotza Mendebalde.

#### Description

Tumulus pierreux en forme de galette aplatie, d'environ 2,50m de diamètre et 0,30m de haut. Une quinzaine de pierres, certaines atteignant 0,30m de long, sont visibles ; peut-être y aurait-il une ébauche de péristalithe (?).

#### Historique

Monument découvert par [J. Blot]{.creator} le [27 décembre 2010]{.date}.

### [Ahaxe]{.spatial} - [Buluntza]{.coverage} 2 - [Tumulus]{.type}

#### Localisation

Carte IGN 1346 Est Saint-Jean-Pied-de-Port.

Altitude : 672m.

On peut apercevoir ce tumulus à une soixantaine de mètres à l'Ouest-Nord-Ouest du *dolmen de Buluntza*.

#### Description

Tumulus ovalaire d'environ une quinzaine de mètres de long et 0,80m de haut. Ces caractéristiques nous évoquent un tertre d'habitat ou un tumulus funéraire dolménique dont la [chambre]{.subject} serait détruite.

#### Historique

Monument découvert par [J. Blot]{.creator} en [1975]{.date} et publié dans [@blotArcheologieMontagneBasque1993a, p.164].

### [Ahaxe]{.spatial} - [Burguista]{.coverage} - [Dolmen]{.type}

#### Localisation

Altitude : 530m.

Ce monument est situé sur une croupe orientée Sud-Nord, qui se détache du massif dénommé Handiague, au Sud. Il est situé dans un petit ensellement d'où l'on a une vue magnifique sur le début de la vallée de l'Hergaray, à l'Ouest.

#### Description

Il s'agit d'un dolmen de montagne, aux dimensions modestes, ce qui s'explique aisément si l'on considère sa difficulté d'accès, comparée aux dolmens de Buluntza, Armiague etc. On note tout d'abord un tumulus pierreux de 9m de diamètre et environ 0,50m de hauteur.

Les éléments constitutifs en sont des blocs de [grès triasique]{.subject} ou de poudingue, de la taille d'un pavé, récoltés dans les environs immédiats qui sont riches en pointements rocheux.

Une dépression centrale, circulaire, de 4m de diamètre et 0,30m de profondeur environ, signe d'une fouille clandestine ancienne, laisse apparaître une importante dalle de [grès triasique rose]{.subject}, elle aussi de provenance proche. On peut très probablement la considérer comme la [dalle de couverture]{.subject} d'une [chambre funéraire]{.subject} dont les montants ne sont pas visibles dans l'état actuel. Cette dalle de grès, orientée dans le sens EO., affecte grossièrement la forme d'un parallélépipède allongé. Elle mesure 2,28m de long. Sa base Est, arrondi, mesure 1,13m de large. Dans sa partie la plus étroite, cette dalle mesure 1,23m et dans sa partie la plus large 1,34 m. Son sommet Ouest, rétréci ne mesure plus que 0,50m.

Son épaisseur variable est maximum à l'extrémité Ouest (0,33m) et bien moindre sur les côtés ou à sa base Est (entre 0,11m et 0,18m). Il semblerait que l'on puisse noter quelques traces d'épannelage (?) de part et d'autre de la région Ouest et peut être à la partie médiane du bord Sud.

#### Historique

Ce monument nous a été signalé par [C. Chauchat]{.creator} en [mai 2012]{.date}. À notre visite en mars 2020, nous avons constaté que ce monument avait fait l'objet d'une intervention sommaire, dont l'auteur, *véritable amateur*, n'a même pas respecté la règle de base de remise en état des lieux (abandon sur place des terres enlevées, des bâches de protection etc.)

### [Ahaxe]{.spatial} - [Handiague]{.coverage} 1 - [Cromlech]{.type}

#### Localisation

Altitude : 620m.

Il est situé à l'extrême Ouest de la longue crête d'Handiague, sur un terrain en pente très douce vers le Nord-Est. Terrain encombré d'une végétation drue (fougères, touyas), à environ 5m au Nord-Est de la piste qui parcourt la crête.

#### Description

Cercle de 3,30m de diamètre, délimité par 16 blocs bien visibles, dont 14 de poudingue et 2 de [grés]{.subject} local ; au centre trois autres dalles de grés et un petit bloc de poudingue sont visibles sans qu'on puisse dire qu'ils forment une [ciste]{.subject}.

#### Historique

Monument découvert par [I. Gaztelu]{.creator}, [A. Martinez]{.creator} et [L. Millan]{.creator} en [janvier 2013]{.date}.

### [Ahaxe]{.spatial} - [Handiague]{.coverage} 6 - [Cromlech]{.type}

#### Localisation

Altitude : 600m.

Situé plus au Nord que *Handiague 1 - Cromlech*, sur cette même crête, mais un peu plus bas.

#### Description

Avant son dégagement (F. Meyrat), ce monument était peu visible, encombré de végétation avec un houx poussant en son milieu. Il est adossé, à l'Ouest, à un important bloc rocheux. Cromlech légèrement tumulaire (0,20m de haut), il semble présenter deux cercles : un cercle central fait de 8 pierres et ayant 2,20m de diamètre, et un extérieur de 3,30m de diamètre et 9 pierres. Toutes ces pierres ont des volumes variables, allant du pavé au volume de 2 ballons de football.

#### Historique

Monument découvert par le [groupe Hillariak]{.creator} en [janvier 2013]{.date}.
