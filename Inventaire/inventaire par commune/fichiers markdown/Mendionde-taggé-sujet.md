# Mendionde

### [Mendionde]{.spatial} - [Gagnekordoki]{.coverage} - [Monolithe]{.type} ([?]{.douteux})

Localisation

Altitude : 591m.

Il est situé à environ 2m au Sud-Ouest de la piste qui monte au sommet de l'Ursuya et à 100m environ au Nord-Ouest du tumulus Gagnekordoki.

Description

Volumineux bloc de [grés]{.subject} de forme allongée, à sommet arrondi au Nord-Ouest et à large base Sud-Est. Il mesure 3,05m de long, une largeur maximale, au centre, de 1,85m. Son épaisseur est variable : au centre elle atteint 0,90m, pour diminuer à sa base (0,70m) et encore plus au sommet (0,20m). Son pourtour semble présenter quelques traces d'épannelage (?) au sommet et le long du bord Sud. Ce bloc rocheux est bien individualisé, mais semble être d'origine strictement locale : fait-il partie de l'ensemble de pierres en place dont 5 éléments apparaissent au voisinage immédiat de son bord Nord-Est ?

Historique

Ce possible monolithe a été signalé par [G. Laporte]{.creator} en [juin 2014]{.date}.

### [Mendionde]{.spatial} - [Gagnekordokia]{.coverage} - [Tumulus]{.type}

Localisation

Altitude : 589m.

Il est situé à environ 400m à l'Est du monument décrit précédemment à *Tumulus Harguibele* (à Macaye), il est lui aussi proche de la piste de crête se rendant au mont Ursuya (à 1,20m au Sud de celle-ci).

Description

On note une formation circulaire constituée d'une cinquantaine de blocs de [calcaire]{.subject} blanc, plus ou moins du volume d'un pavé, groupés sur le sol. Cette disposition qui ne paraît pas naturelle et dont la hauteur n'est que celle des pierres qui émergent du sol, nous évoque cependant la possibilité d'un « tumulus » dont nous avons déjà rencontré de tels exemplaires.

Historique

Monument découvert par [Meyrat F.]{.creator} en [juin 2014]{.date}.

### [Mendionde]{.spatial} - [Pagazaharreta]{.coverage} 1 - [Cromlech]{.type}

Localisation

Altitude : 430m.

Il est situé à environ 3m au Sud Sud-Ouest de la piste pastorale et de la pierre couchée dont la description suit.

Description

Cercle de 6m de diamètre, assez irrégulier, délimité par environ une quinzaine de pierres assez visibles.

Historique

Ce monument a été découvert en [janvier 2015]{.date} par [I. Txintxuretta]{.creator}. Il nous est apparu fort [douteux]{.douteux}.

### [Mendionde]{.spatial} - [Pagazaharreta]{.coverage} 2 - [Pierre tombée]{.type}

Localisation

Altitude : 430m.

Elle est à 2m au Sud Sud-Ouest de *Pagozaharreta 1 - Cromlech* et tangent à la piste pastorale.

Description

Bloc de [grés]{.subject} grossièrement parallélépipédique à grand axe orienté Sud-Ouest Nord-Est. Il mesure 1,80m de long, 1m de large et 0,50m d'épaisseur ; on note de nombreuses traces d'épannelage sur ses bords Sud-Ouest et aussi, moins nombreuses, au bord Nord-Ouest.

Historique

Ce monument a été découvert en [janvier 2015]{.date} par [I. Txintxuretta]{.creator}. Il nous est apparu fort [douteux]{.douteux}.

### [Mendionde]{.spatial} - [Pagazaharreta]{.coverage} 3 - [Tumulus]{.type} ([?]{.douteux})

Localisation

Il est situé juste en face de *Pagozaharreta 2 - Pierre tombée*, au Nord de la piste pastorale et tangent à celle-ci.

Description

Tumulus de terre d'environ 4,90m de diamètre et 0,60m de haut ; il est recouvert d'un volumineux roncier ; quelques pierres sont visibles en surface, sans ordre apparent.

Historique

Ce monument a été découvert en [janvier 2015]{.date} par [I. Txintxuretta]{.creator}. Il nous est apparu fort douteux.

### [Mendionde]{.spatial} - [Pagazaharreta]{.coverage} 6 - [Cromlech]{.type} ([?]{.douteux})

Monument lui aussi très douteux.

Localisation

Altitude : 419m

Situé entre 2 petits rius, à environ une cinquantaine de mètres à l'Ouest Nord-Ouest de *Pagozaharreta 2 - Pierre tombée*.

Description

Cercle (?) de 3,60m de diamètre, au nombre de pierres impossible à préciser...

### [Mendionde]{.spatial} - [Pagazaharreta]{.coverage} 7 - [Pierre dressée]{.type} ([?]{.douteux})

Localisation

Altitude : 415m.

Située à une quarantaine de mètres à l'Ouest Nord-Ouest de *Pagazaharetta 6 - Cromlech*.

Description

Bloc de [grés]{.subject} planté dans le sol, en forme de pain de sucre à sommet tronqué. Sa base mesure 0,64m de long et 0,48m de large ; il est haut de 0,78m et son sommet a 0,23m de large. Sa face Nord présente de nombreuses retouches, ainsi que ses arêtes.

Historique

Ce monument a été découvert en [janvier 2015]{.date} par [I. Txintxuretta]{.creator}. Il nous est apparu fort douteux.

### [Mendionde]{.spatial} - [Pagazaharreta]{.coverage} 8 - [Cromlech]{.type} ([?]{.douteux})

Localisation

Altitude : 415m.

Il est situé à 5m au Sud-Ouest de *Pagazaharetta 7 - Pierre dressée*.

Description

Cercle de 4,70m de diamètre, délimité par une dizaine de pierres peu évidentes.

Historique

Ce monument a été découvert en [janvier 2015]{.date} par [I. Txintxuretta]{.creator}. Il nous est apparu fort douteux.
