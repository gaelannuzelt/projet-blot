# Notes sur Mendive-toc.md

- Petite commune
- 30 fiches de monument(s)

## Ajout de la balise `<article></article>`

## Ajout de balises `<monument></monument>`

## Ajout de balises `<sub></sub>`

## Classes créées (20)

### `{.meta}`--- 
- Localisation
- Description
- Historique

### `{.monument}`
- menhir d'Ithurroché
- groupe de TH dit «du bas»
- cayolar Idioineko Borda
- cayolar
- cromlech n°1
- cromlech Béhorlégi 1
- les cromlechs 1 et 2
- cromlech Béhorlégi 2
- n°2
- cayolar Cihigolatzé
- ruines bien visibles d'un très ancien cayolar
- ruines du cayolar
- le cromlech de ce nom
- monolithe ci-après décrit
- ceux d'Egurcekobidea
- Deux cromlechs
- Ce tumulus n°2
- n°1
- Ilhareko-lepoa 1 et 2
- Ilhareko-lepoa 1
- Tumulus (probablement dolménique) dit T3
- précédent
- le dolmen de Xuberaxain Harria
- dolmen Xuberaxain
- la chapelle Saint-Sauveur
- la chapelle
- 

### `{.type}`
- cromlechs
- Monolithe
- tertre
- Tertre
- tertres
- Tertres
- Tertre asymétrique terreux
- tertres circulaires
- Tertre de terre
- tertres de terre
- tertre de terre et de pierres
- Tertre terreux
- TH
- TH de terre
- tumulus
- Tumulus
- Tumulus aplati
- Tumulus de terre
- Tumulus de terre et de pierres
- Tumulus mixte
- Tumulus ovale
- tumulus pierreux
- tumulus «préhistorique»
- Petit tumulus

### `{.personne}`
- J. Blot
- Blot J.
- Alfonso Martinez Manteca
- A. Martinez Manteca
- le Cdt Rocq
- Meyrat F.
- le groupe Hilharriak

### `{.date}`
- 1935
- 1971
- juillet 1971
- octobre 1971
- avril 1973
- 1975
- mai 2011
- juillet 2011
- août 2011
- décembre 2011
- avril 2013
- avril 2014
- avril 2015

### `{.toponyme}`
- pic de Béhorlégi
- Sare-Sare
- les pâturages
- Calichaga eta Pegaretta
- col d'Egurce
- ruisseaux Ataramatze et Irati
- chalets d'Irati
- Iratiko erreka
- chalet Pedro
- le col de ce nom
- le col des morts
- le plateau d'Irati
- un petit ru
- petit ru déjà cité
- la chapelle Saint-Sauveur
- la chapelle
- la piste pastorale antique
- la route actuelle

### `{.commune}`
- Saint-Jean-Pied-de-Port
- Mendive
- Odiarp
- Ahuski
- Armiague
- 

### `{.ref}`--- 
- Blot J. 1972 c, p.50
- Barandiaran JM. de. 1952, p. 158
- «Etude sur le peuplement du Pays Basque et les persistances archaïques dans la civilisation et la langue basque» *- Bulletin des Sciences, Lettres et Arts de Bayonne,* 1935, p.371.
### `{.sub}`

---

### `{.nb}`
- 11
- Quatre
- quatre
- 

### `{.altitude}`
- 870 m
- 

### `{.distance}`
- 35 mètres
- quelques dizaines de mètres
- entre 10 et 20 m
- 80m
- une trentaine de mètres
- 15 mètres
- une quinzaine de mètres
- 

### `{.hauteur}`
- 0,30m
- 1m
- de 0,50 à 0,80 m environ
- 0,30 à 0,40m
- 0,40
- variables
- 

### `{.hauteurnote}`---
- 
- 

### `{.longueur}`
### `{.largeur}`
### `{.epaisseur}`---
### `{.diametre}`
- 15m
- de 7 à 8 m en moyenne
- 7,50m
- 3,70m
- 6m
- 7 m
- 3,30m
- entre 12 et 15 mètres
- entre 7 et 9 mètres
- 

### `{.card}`
- est
- N
- nord
- N NE
- nord nord-est
- NE
- NO
- nord-ouest
- O
- O.
- O SO
- S
- SO
- SE
- sud
- sud-est
- sud-ouest
- flanc nord
- flanc ouest
- flanc sud
- axe nord-sud
- axe O-E
- quart nord-ouest
- secteur est
- le bord sud

### `{.carte}`
- 1346 est
- Même carte
- 

### `{.}`

---

## CSS
### Pour les classes

```css

.meta{
    color: purple;
    margin-left: 1em;
    letter-spacing: 4px;
}
.monument{
    color: MediumPurple;
    text-decoration: underline dotted;
}
.sub{
    color: MediumPurple;
    font-style: italic;
}
.type{
    color: PaleVioletRed;
}
.personne{
    color: DarkGoldenRod;
}
.date{
    color: MediumSeaGreen;
    font-family: 'Georgia';
    font-size: 0.8em;
}
.toponyme{
    color: coral;
    font-style: italic;
}
.commune{
    color: crimson;
}
.ref{
    color: olive;
    font-style: italic;
    font-family: monospace;
}
.nb{
    font-family: monospace;
}
.altitude{
    color: darkcyan;
    font-family: 'Georgia';
    font-size: 0.8em;
}
.distance{
    color: grey;
}
.hauteur{
    color: Chocolate;
}
.hauteurnote{
    color: Chocolate;
}
.longueur{
    color: Chocolate;
}
.largeur{
    color: Chocolate;
}
.diametre{
    color: Chocolate;
}
.card{
    font-style: italic;orange
}
.carte{
    color: olive;
    font-family: 'Georgia';
    font-size: 0.8em;
}
h4 ~ p{
    margin-left: 2em;
    margin-top: 0em;
    margin-bottom: 0em;
}
ul{
    margin-left: 1em;
    list-style-type: '÷ ';
}
ul ul{
    margin-left: -1em;
    list-style-type: '› ';
}
.citation{
    color: olive;
    font-family: 'Georgia';
    font-size: 0.8em;
}
.references{
    width: 70%;
    margin: 0 auto;
    line-height: 110%;
    font-size: 0.9em;
}

```

### Modif de la feuille pandoc

```css

---

a {
    color: MediumPurple;
    text-decoration: none;
}
a:hover {
    color: deeppink; 
}
---

 html, body {
    margin: 0;
    margin-bottom: 4em;
    padding: 0;
}
body {
    background-color: MistyRose;
    color: #333;
    font-family: 'Garamond', 'Caladea', 'DejaVu', 'Georgia', 'Times New Roman', 'Times', serif;
}
article {
    width: 70%;
    font-size: 1em;
    margin: 0 auto;
    line-height: 110%;
}

---

h1{
    font-family: 'Caladea', 'Raleway', 'Lato', 'Liberation sans', 'Helvetica', sans-serif;
    color: crimson;
    letter-spacing: 3px;
    margin-top: 2em;
}
h2{
    width: 70%;
    font-size: 1.25em;
    margin: 0 auto;
    margin-top: 2em;
    line-height: 110%;
    font-family: 'Lucida Calligraphy', 'Century gothic', 'Raleway', 'Lato', 'Liberation sans', 'Helvetica', sans-serif;
    color: olive;
}
h3{
    font-size: 1em;
    font-weight: 600;
    margin-top: 2em;
    margin-bottom: -0.25em;
    letter-spacing: 2px;
    text-decoration: underline;
    font-family: 'Tahoma', 'OCR A', 'Alef';
    color: MediumPurple;
}
h4{
    font-size: 0.7em;
    margin-bottom: 0.2em;
}
h4, h5, h6 {
    font-family: 'Century gothic', 'Raleway', 'Lato', 'Liberation sans', 'Helvetica', sans-serif;
    color: MediumPurple;
}

```

### Non modifié

```css

* {
    box-sizing: border-box;
}

---

hr {
    border: none;
    border-bottom: 1px solid #999;
    width: 80%;
}
html, body {
    margin: 0;
    margin-bottom:5em;
    padding: 0;
}

---

/* Better display on printing */
@media print {
    article {
      width: 90%;
      font-size: 12pt;
      margin: 0 auto;
      line-height: 125%;
    }
}
article p {
    hyphens: auto;
    text-align: justify;
}

---

img {
    max-width: 100%;
    height: auto;
}
blockquote {
    font-size: 80%;
    color: rgba(120, 120, 120, 1);
    margin: 2% 5%;
    line-height: 120%;
}
table {
    border-collapse: collapse;
    width: 100%;
    font-size: 70%;
    font-family: 'Raleway', 'Lato', 'Liberation sans', 'Helvetica', sans-serif;
}
th, td {
    padding: 4px 20px;
    border-bottom: 1px solid #333;
}

---

/* Generic styles */
pre.sourceCode {
    color: #657b83;
    background-color: #fdf6e3;
    box-shadow: 0px 0px 20px 0px rgba(0, 0, 0, .05);
    padding: 2px;
    overflow: auto;
}

/* All classes, taken from skylighting lib */
.sourceCode .kw { color: #859900; font-weight: bold; } /* Keyword */
.sourceCode .dt { color: #b58900; } /* Datatype */
.sourceCode .df { color: #d33682; } /* Decimal values*/
.sourceCode .bn { color: #d33682; } /* base-n-integers */
.sourceCode .fl { color: #d33682; } /* Floats */
.sourceCode .ch { color: #2aa198; } /* Character */
.sourceCode .st { color: #2aa198; } /* String */
.sourceCode .vs { color: #2aa198; } /* Verbatim String */
.sourceCode .ss { color: #2aa198; } /* Special String */
.sourceCode .co { color: #586e75; font-style: italic; } /* Comment */
.sourceCode .ot { font-weight: bold; } /* Other token */
.sourceCode .al { color: #d33682; background-color: #073642; } /* Alert */
.sourceCode .fu { color: #268bd2; } /* Function name */
.sourceCode .re {} /* Region marker */
.sourceCode .er { color: #dc322f; font-weight: bold; } /* Error */
.sourceCode .cn { color: #2aa198; } /* Constant */
.sourceCode .sc { color: #dc322f; } /* Special character */
.sourceCode .im { color: #6c71c4; font-weight: bold; } /* Import statement */
.sourceCode .do { color: #dc322f; } /* Documentation string */
.sourceCode .an { color: #2aa198; } /* Annotation */
.sourceCode .cv { color: #6c71c4; } /* Comment var */
.sourceCode .va { color: #268bd2; } /* Variable */
.sourceCode .cf { color: #859900; } /* Control Flow (if, else, return) */
.sourceCode .op {} /* Operator */
.sourceCode .bu { color: #b58900; } /* Builtin function/class/identifier */
.sourceCode .ex { color: #268bd2; } /* Extension */
.sourceCode .pp { color: #cb4b16; } /* Preprocessor, like #import in C++ */
.sourceCode .at { color: #dc322f; } /* Attribute */
.sourceCode .in { color: #586e75; } /* Information */
.sourceCode .wa { color: #cb4b16; } /* Warning */

```

---

## Script Pandoc (sources .docx d'un dossier --> .md))

```sh
$> find . -name "*.docx" | while read i; do pandoc -f docx -t markdown -s --wrap=none "$i" -o ../fichiers\ markdown/"${i%.*}.md"; done
```

## Script Pandoc (fichiers .md balisés d'un dossier --> .html)

```sh
$> find . -name "*.md" | while read i; do pandoc -o ./Sortie/"${i%.*}.html" -s --toc -H ./Data/InventaireBlot.css --bibliography=./Data/BiblioInventaire.bib --csl=./Data/sciences-po-ecole-doctorale-author-date.csl "$i"; done
```

## Xquery (.html --> .xml)

```xquery
for $i in //*:h3
let $altitude := $i//*:span[@class="altitude"]/text()
return <altitude>{$altitude}</altitude>

```

