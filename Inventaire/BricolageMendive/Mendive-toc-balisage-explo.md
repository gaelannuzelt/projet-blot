---
documentclass: article
lang: fr-FR
...

<article>

-   [Mendive](#mendive)
    -   [Apanice-Tertre d'habitat](#apanice-tertre-dhabitat)
    -   [Artzainharria haut-Tertres d'habitat (nb = 11)](#artzainharria-haut-tertres-dhabitat-nb-11)
    -   [Artzainharria bas-Tertres d'habitat (nb = 4)](#artzainharria-bas-tertres-dhabitat-nb-4)
    -   [Béhorlégi 1-Cromlech](#béhorlégi-1-cromlech)
    -   [Béhorlégi 2-Cromlech](#béhorlégi-2-cromlech)
    -   [Béhorlégi 3-Tumulus](#béhorlégi-3-tumulus)
    -   [Béhorlégi 4-Cromlech](#béhorlégi-4-cromlech)
    -   [Burkidoy-Tertres d'habitat (nb = 4)](#burkidoy-tertres-dhabitat-nb-4)
    -   [Calichaga-Cromlech](#calichaga-cromlech)
    -   [Calichaga-Tumulus](#calichaga-tumulus)
    -   [Cihigolatzé-Tertre d'habitat](#cihigolatzé-tertre-dhabitat)
    -   [Egurce-Monolithe](#egurce-monolithe)
    -   [Egurce-Tertres d'habitat (groupe du haut nb = 10 environ)](#egurce-tertres-dhabitat-groupe-du-haut-nb-10-environ)
    -   [Egurcekolepoa 1-Tertre d'habitat](#egurcekolepoa-1-tertre-dhabitat)
    -   [Egurcekobidea-Tertres d'habitat (nb = 30 environ)](#egurcekobidea-tertres-dhabitat-nb-30-environ)
    -   [Gahalarbe 2-Tumulus](#gahalarbe-2-tumulus)
    -   [Ilhareko-lepoa 1-Tumulus](#ilhareko-lepoa-1-tumulus)
    -   [Ilhareko-lepoa 2-Tumulus](#ilhareko-lepoa-2-tumulus)
    -   [Irati-Soro Nord 3-Tumulus](#irati-soro-nord-3-tumulus)
    -   [Irati-Soro (N)-Tertre d'habitat](#irati-soro-n-tertre-dhabitat)
    -   [Irati- Soro N-Tertres d'habitat (nb = 8)](#irati--soro-n-tertres-dhabitat-nb-8)
    -   [Irratiko erreka 1-Tertre d'habitat](#irratiko-erreka-1-tertre-dhabitat)
    -   [Iratiko erreka 2-Tertre d'habitat](#iratiko-erreka-2-tertre-dhabitat)
    -   [Ithurroché-Tertres d'habitat (nb = 4)](#ithurroché-tertres-dhabitat-nb-4)
    -   [Olhazarre-Tumulus](#olhazarre-tumulus)
    -   [Saint-Sauveur-d'Irati-Tumulus](#saint-sauveur-dirati-tumulus)
    -   [Sare-Sare-Cromlech (?)](#sare-sare-cromlech)
    -   [Sare-Sare-Tertre d'habitat](#sare-sare-tertre-dhabitat)
    -   [Xuberaxain-Tertres d'habitat (nb = 8)](#xuberaxain-tertres-dhabitat-nb-8)
    -   [Xuberaxain-Tertres d'habitat (nb = 2)](#xuberaxain-tertres-dhabitat-nb-2)

# Mendive : Inventaire des monuments

### Apanice-Tertre d'habitat 
<!--nom proposé : "Mendive - Apanice - Tertre d'habitat"-->
<!--nom proposé : "Mendive - Tertre d'habitat à Apanice"-->

#### [Localisation]{.meta}
On le trouve à [35 mètres]{.distance} environ au [SO]{.card} de la route et à [quelques dizaines de mètres]{.distance} au [N]{.card} de l'embranchement qui conduit au [menhir d'Ithurroché]{.monument}.

#### [Description]{.meta}
[Tertre de terre]{.type} de [15m]{.diametre} de diamètre environ, à sommet discrètement arrondi et de [0,30m]{.hauteur} de haut, au [NE]{.card .hauteurnote}, et jusqu'à [1m]{.hauteur} au [SO]{.card .hauteurnote}.

#### [Historique]{.meta}
[Tertre]{.monument} découvert par [A. Martinez Manteca]{.personne} en [juillet 2011]{.date}.


### Artzainharria haut-Tertres d'habitat (nb = 11) 
<!--nom proposé : "Mendive - Artzainharria haut - 11 Tertres d'habitat"-->
<!--nom proposé : "Mendive - 11 Tertres d'habitat à Artzainharria haut"-->

#### [Localisation]{.meta}
Ils sont répartis sur le [flanc nord]{.card} d'une éminence qui fait face au [pic de Béhorlégui]{.toponyme} ; et qui est séparée du [groupe de TH dit «du bas»]{.monument} par un petit col ou aboutit le chemin et où est construit le [cayolar Idioineko Borda]{.monument}.

#### [Description]{.meta}
Un ensemble de [11]{.nb} [tertres de terre]{.type} est érigé sur cette pente assez marquée, séparés par des distances comprises [entre 10 et 20 m]{.distance}. Ces [tertres circulaires]{.type}, asymétriques, ont des diamètres [de 7 à 8 m en moyenne]{.diametre} et des hauteurs [de 0,50 à 0,80 m environ]{.hauteur}.

A noter, au sommet de cette éminence, à son extrémité [SE]{.card}, *un petit [tertre]{.type} en bordure d'une dépression* (artificielle ?).

#### [Historique]{.meta}
[Tertres]{.monument} découverts par [J. Blot]{.personne} en [avril 2013]{.date}.


### Artzainharria bas-Tertres d'habitat (nb = 4)  
<!--nom proposé : "Mendive - Artzainharria bas - 4 Tertres d'habitat"-->
<!--nom proposé : "Mendive - 4 Tertres d'habitat à Artzainharria bas"-->

#### [Localisation]{.meta}

De l'autre côté du col et donc au [NE]{.card} du [cayolar Idioineko Borda]{.monument}

#### [Description]{.meta}

[Quatre]{.nb} [TH de terre]{.type}, érigés sur la partie la plus élevée de cette prairie.

- [TH 1]{.sub} : le plus grand, le plus visible, mesure [17m]{.diametre} de diamètre et [1,50m]{.hauteur} de haut.
- [TH 2]{.sub} : situé à [1m]{.distance} au [NE]{.card} ; moins net, mesure [11m]{.diametre} de diamètre et [0,80m]{.hauteur} de haut.
- [TH 3]{.sub} : situé à [10m]{.distance} au [SE]{.card} du [n°1]{.monument} ; mesure [11m]{.diametre} de diamètre et [0,50m]{.hauteur} de haut.
- [TH 4]{.sub} : situé à [10m]{.distance} au [S]{.card} du [n°3]{.monument}, très visible, mesure [16m]{.diametre} de diamètre et [1,40m]{.hauteur} de haut.

#### [Historique]{.meta}

[Tertres]{.monument} découverts par [Blot J.]{.personne} en [1971]{.date}, et contrôlés en [avril 2013]{.date}.



    
### Béhorlégi 1-Cromlech
<!--nom proposé : "Mendive - Béhorlégi - 1 : Cromlech"-->
<!--nom proposé : ""-->

#### [Localisation]{.meta}
Carte [1346 est]{.carte}. [Saint-Jean-Pied-de-Port]{.commune}.

Altitude : [870 m]{.altitude}

Un ensemble de [3]{.nb} [cromlechs]{.type} et [1]{.nb} [tumulus]{.type} est situé sur un replat à gauche de la route qui monte de [Mendive]{.commune} au [flanc sud]{.card} du [pic de Béhorlégi]{.toponyme}. Ce site domine un autre replat, en contre-bas et au [sud]{.card}, dénommé [Sare-Sare]{.toponyme}.

#### [Description]{.meta}
Le [cromlech n°1]{.monument} mesure [6m]{.diametre} de diamètre, est délimité par 12 pierres de grès blanc, bien visibles ; il est érigé sur un sol en légère pente vers le [sud-ouest]{.card} et tangent à l'[est]{.card} au [n°2]{.monument}.

#### [Historique]{.meta}
[Monument]{.monument} découvert en [avril 1973]{.date}.



    
### Béhorlégi 2-Cromlech
<!--nom proposé : "Mendive - Béhorlégi - 2 : Cromlech"-->
<!--nom proposé : ""--> 

#### [Localisation]{.meta}
Il est tangent au [sud-est]{.card} du [cromlech Béhorlégi 1]{.monument}.

#### [Description]{.meta}
Cercle de [7,50m]{.diametre} de diamètre, délimité par 10 pierres en grès blanc de [0,30 à 0,40m]{.hauteur} de haut.

#### [Historique]{.meta}
[Monument]{.monument} découvert en [avril 1973]{.date}.



    
### Béhorlégi 3-Tumulus
<!--nom proposé : "Mendive - Béhorlégi - 3 : Tumulus"-->
<!--nom proposé : ""-->

#### [Localisation]{.meta}
A environ [80m]{.distance} à l'[est]{.card} du [cromlech n°1]{.monument}, aux deux tiers de la montée qui débute sur le replat où se trouvent [les cromlechs 1 et 2]{.monument}.

#### [Description]{.meta}
[Tumulus mixte]{.type} de terre et de pierres, érigé sur un sol en légère pente vers le [nord-ouest]{.card} ; il mesure [3,70m]{.diametre} de diamètre et [0,40]{.hauteur} de haut.

#### [Historique]{.meta}
[Monument]{.monument} découvert en [avril 1973]{.date}.



    
### Béhorlégi 4-Cromlech
<!--nom proposé : "Mendive - Béhorlégi - 4 : Cromlech"-->
<!--nom proposé : ""-->

#### [Localisation]{.meta}
A environ [130m]{.distance} au [sud-est]{.card} du [cromlech Béhorlégi 2]{.monument}, il est situé sur la piste pastorale qui a un parcours légèrement ascendant quand on vient du [n°2]{.monument}.

#### [Description]{.meta}
Cercle de [6m]{.diametre} de diamètre, délimité par 22 pierres bien visibles, atteignant [0,30 à 0,40m]{.hauteur} de haut. On distingue très nettement 4 pierres centrales.

#### [Historique]{.meta}
[Monument]{.monument} découvert en [avril 1973]{.date}.



    
### Burkidoy-Tertres d'habitat (nb = 4)
<!--nom proposé : "Mendive - Burkidoy - 4 Tertres d'habitat"-->
<!--nom proposé : ""-->

#### [Localisation]{.meta}

Altitude : [1291 m]{.altitude}

A environ [300 m]{.distance} au [nord]{.card} du [cayolar Cihigolatzé]{.monument}, en suivant l'ancienne piste qui monte vers [les pâturages]{.toponyme}, (au-dessus du tracé actuel, qui s'en détache environ [80 m]{.distance} au [N]{.card} du [cayolar]{.monument}).

#### [Description]{.meta}

Ces [quatre]{.nb} [tertres]{.type} sont érigés sur un terrain en pente vers le [SO]{.card}.

- [Tertre n°1 :]{.sub} Il est situé à [une trentaine de mètres]{.distance} au [NO]{.card} des [ruines bien visibles d'un très ancien cayolar]{.monument}, où aboutissait une bretelle de l'ancienne piste. Mesure [5m]{.diametre} de diamètre et [0,50m]{.hauteur} de haut.
- [Tertre n°2 :]{.sub} à [3m]{.distance} à l'[O]{.card} du [précédent]{.monument}, de l'autre côté de la piste, et mêmes mensurations.
- [Tertre n° 3 :]{.sub} à [4m]{.distance} au [N NE]{.card} du [n°1]{.monument} ; mesure [3m]{.diametre} de diamètre et [0,30m]{.hauteur} de haut.
- [Tertre n° 4 :]{.sub} situé à [9m]{.distance} à l'[O SO]{.card} du [n°2]{.monument} ; mesure [3m]{.diametre} de diamètre et [0,30m]{.hauteur} de haut.

#### [Historique]{.meta}

[Tertres]{.monument} découverts par [Blot J.]{.personne} en [1975]{.date}.



    
### Calichaga-Cromlech
<!--nom proposé : "Mendive - Calichaga - Cromlech"-->
<!--nom proposé : ""-->

#### [Localisation]{.meta}

Carte [1346 est]{.carte}. [Saint-Jean-Pied-de-Port]{.commune}

Altitude : [489 m]{.altitude}

[Ce monument]{.monument} est situé sur un pâturage à [90m]{.distance} au [nord-ouest]{.card} du virage de la route qui va de [Mendive]{.commune} à [Ahuski]{.commune}, par [Armiague]{.commune}, au moment où elle passe du flanc nord au flanc sud de la ligne de croupes baptisée «[Calichaga eta Pegaretta]{.toponyme}».

#### [Description]{.meta}

Cercle de [7 m]{.diametre} de diamètre, délimité par 14 pierres au ras du sol, mais nettement visibles. [Le monument]{.monument} a été détérioré par la pose d'une barrière barbelée qui le coupe selon un axe est-ouest dans son secteur nord.

#### [Historique]{.meta}

[Monument]{.monument} découvert en [avril 1973]{.date}.



    
### Calichaga-Tumulus
<!--nom proposé : "Mendive - Calichaga - Tumulus"-->
<!--nom proposé : ""-->

#### [Localisation]{.meta}

Altitude : [489 m]{.altitude}

Il se trouve au point culminant du même replat que [le cromlech de ce nom]{.monument} et à [15 mètres]{.distance} au [SE]{.card} de la première barrière de barbelés (qui coupe ce dernier).

#### [Description]{.meta}

[Tumulus de terre]{.type} de [3,30m]{.diametre} de diamètre et [0,30 à 0,40m]{.hauteur} de haut.

#### [Historique]{.meta}

[Monument]{.monument} découvert par [Blot J.]{.personne} en [août 2011]{.date}.



    
### Cihigolatzé-Tertre d'habitat
<!--nom proposé : "Mendive - Cihigolatzé - Tertre d'habitat"-->
<!--nom proposé : ""-->

#### [Localisation]{.meta}

Altitude : [1241 m]{.altitude}

Le [tertre]{.type} est situé à [une trentaine de mètres]{.distance} à l'[O]{.card} des [ruines du cayolar]{.monument}, et donne sur la rive droite du ruisseau en contre-bas.

#### [Description]{.meta}

[Tertre]{.type} bien visible, de [8m]{.diametre} de diamètre environ et [0,40m]{.hauteur} de haut.

#### [Historique]{.meta}

[Monument]{.monument} découvert par [Blot J.]{.personne} en [avril 2014]{.date}



    
### Egurce-Monolithe
<!--nom proposé : "Mendive - Egurce - Monolithe"-->
<!--nom proposé : ""-->

#### [Localisation]{.meta}

Altitude : [995 m]{.altitude}

Il est situé dans le deuxième petit col après la montée.

#### [Description]{.meta}

Belle dalle de grés parallélépipédique gisant au sol suivant un [axe O-E]{.card}, mesurant [2,30m]{.longueur} de long, [0,90m]{.largeur} de large, [027m]{.epaisseur} d'épaisseur et présentant des traces d'épannelage sur tout son pourtour semble-t-il.

#### [Historique]{.meta}

[Monolithe]{.monument} découvert par [Alfonso Martinez Manteca]{.personne} en [juillet 2011]{.date}.



    
### Egurce-Tertres d'habitat (groupe du haut nb = 10 environ)
<!--nom proposé : "Mendive - Egurce - 10 Tertres d'habitat - groupe du haut"-->
<!--nom proposé : ""-->

#### [Localisation]{.meta}

Altitude : [970 m]{.altitude}

[Un premier ensemble de 6]{.sub} [TH]{.type} se trouve au niveau d'un premier petit col après la montée ; [4 autres]{.sub} se trouvent avant un deuxième col, enfin [2 ou 3 autres]{.sub} [TH]{.type} sont érigés au [nord]{.card} du [monolithe ci-après décrit]{.monument} dans ce 2^ème^ col.

#### [Description]{.meta}

Mêmes formes et dimensions variables que [ceux d'Egurcekobidea]{.monument}.

#### [Historique]{.meta}

[Tertres]{.monument} découverts par [A. Martinez Manteca]{.personne} en [juillet 2011]{.date}.



    
### Egurcekolepoa 1-Tertre d'habitat
<!--nom proposé : "Mendive - Egurcekolepoa 1 - Tertre d'habitat"-->
<!--nom proposé : ""-->

#### [Localisation]{.meta}

Altitude : [913 m]{.altitude}

Erigé sur la gauche du col, en montant.

#### [Description]{.meta}

[Tertre de terre]{.type} d'[une quinzaine de mètres]{.diametre} de diamètre, et [3 mètres]{.hauteur} environ dans sa plus grande hauteur, à l'[Est]{.card}.

#### [Historique]{.meta}

[Tertre]{.monument} découvert par [A. Martinez Manteca]{.personne} en [juillet 2011]{.date}.



    
### Egurcekobidea-Tertres d'habitat (nb = 30 environ)
<!--nom proposé : "Mendive - Egurcekobidea - 30 Tertres d'habitat"-->
<!--nom proposé : ""-->

#### [Localisation]{.meta}

Altitude : [970 m]{.altitude}

Cette [trentaine]{.nb} de [tertres]{.type} est échelonnée de part et d'autre d'une piste qui, partant du [col d'Egurce]{.toponyme}, monte vers l'[Est]{.card}.

#### [Description]{.meta}

Certains sont très marqués, d'autres beaucoup plus érodés, les dimensions varient [entre 12 et 15 mètres]{.diametre} de diamètre, pour des hauteurs [variables]{.hauteur}.

#### [Historique]{.meta}

[Tertres]{.type} découverts par [A. Martinez Manteca]{.personne} en [juillet 2011]{.date}.



    
### Gahalarbe 2-Tumulus
<!--nom proposé : "Mendive - Gahalarbe - 2 : Tumulus"-->
<!--nom proposé : ""-->

#### [Localisation]{.meta}

[Même carte]{.carte}.

Altitude : [947 m]{.altitude}

Nous avons publié [@blotNouveauxVestigesMegalithiques1972, p.50], le [cromlech n° 1]{.monument}, qui a été ultérieurement détruit par l'aménagement de la piste pastorale ; ses coordonnées étaient : 320,473 -- 1791,767, Altitude : [970 m]{.altitude}.

[Ce tumulus n°2]{.monument} est situé à environ [100m]{.distance} au [nord nord-est]{.card} du [n°1]{.monument}, sur la même crête, érigé sur terrain plat.

#### [Description]{.meta}

[Tumulus ovale]{.type} à grand [axe nord-sud]{.card}, (axe de la crête), mesurant [10m]{.diametre} pour ce dernier, et [7,30m]{.diametre} pour le plus petit, et une hauteur de [0,90m]{.hauteur}. Il s'agit d'un [tumulus pierreux]{.type}, fait de blocs de schiste de la taille d'un petit pavé ; il est longé par la piste pastorale à son [flanc ouest]{.card}.

#### [Historique]{.meta}

[Monument]{.monument} découvert en [avril 1972]{.date}.



    
### Ilhareko-lepoa 1-Tumulus
<!--nom proposé : "Mendive - Ilhareko-lepoa - 1 : Tumulus"-->
<!--nom proposé : ""-->

#### [Localisation]{.meta}

Carte [1446 ouest]{.carte}. [Ordiarp]{.commune}.

Altitude : [926 m]{.altitude}

[Ilhareko-lepoa 1 et 2]{.monument} sont érigés au sommet d'une petite colline qui domine, au [sud-ouest]{.card}, [le col de ce nom]{.toponyme}, dénomination qui avait d'ailleurs attiré notre attention ([le col des morts]{.toponyme}), mais malgré plusieurs visites en ces lieux, nous n'avions rien remarqué. Ce n'est que par une belle soirée d'automne, avec l'éclairage à jour frisant du soleil couchant, que nous sont apparus les deux [tumulus]{.type}. Nous pensons que, compte tenu de la difficulté à les voir pour un œil non averti, la dénomination du lieu remonte à l'époque de la construction de [ces monuments funéraires]{.monument}.

[Ilhareko-lepoa 1]{.monument} est à l'extrémité [sud-est]{.card} de cette colline.

#### [Description]{.meta}

[Tumulus aplati]{.type} en galette de [8m]{.diametre} de diamètre et [0,40m]{.hauteur} de haut ; 4 pierres apparaissent dans le [secteur est]{.card} du monument.

#### [Historique]{.meta}

[Monument]{.monument} découvert en [octobre 1971]{.date}.



    
### Ilhareko-lepoa 2-Tumulus
<!--nom proposé : "Mendive - Ilhareko-lepoa - 2 : Tumulus"-->
<!--nom proposé : ""-->

#### [Localisation]{.meta}

Situé à [15 m]{.distance} au [nord-ouest]{.card} de [Ilhareko-lepoa 1]{.monument}.

#### [Description]{.meta}

[Petit tumulus]{.type} de [5 m]{.diametre} de diamètre, aplati en galette lui aussi, de [0,40m]{.hauteur} de haut. Quelques pierres apparaissent dans le [quart nord-ouest]{.card}, et au centre.

- Peut-être y aurait-il encore un [troisième tumulus]{.sub} à [5m]{.distance} au [sud]{.card} de [ce tumulus n°2]{.monument}, de [4m]{.diametre} de diamètre et [0,50m]{.hauteur} de haut, sans que l'on puisse l'affirmer.

#### [Historique]{.meta}

[Monument]{.monument} découvert en [octobre 1971]{.date}.



    
### Irati-Soro Nord 3-Tumulus
<!--nom proposé : "Mendive - Irati-Soro Nord - 3 : Tumulus"-->
<!--nom proposé : ""-->

#### [Localisation]{.meta}

Carte [1446 ouest]{.carte}. [Ordiarp]{.commune}.

Altitude : [1027 m]{.altitude}

[Deux cromlechs]{.monument} très voisins ont été décrits [dans @barandiaranCronicaPrehistoria1952, p. 158], mais ont semble-t-il été détruits depuis par le passage de la route.

Ce [tumulus]{.type} est tout proche de la confluence des [ruisseaux Ataramatze et Irati]{.toponyme}, et à droite de la route qui monte aux [chalets d'Irati]{.toponyme}.

#### [Description]{.meta}

[Tumulus pierreux]{.type} de [11m]{.diametre} de diamètre et [1,50]{.hauteur} de haut environ ; son centre présente une excavation de 5m de diamètre et 1m de profondeur, trace d'une ancienne fouille. Le [versant nord-est]{.card} du [tumulus]{.monument} domine le ruisseau, le [versant sud-ouest]{.card} le chemin pastoral.

#### [Historique]{.meta}

[Monument]{.monument} découvert en [juillet 1971]{.date}.



    
### Irati-Soro (N)-Tertre d'habitat
<!--nom proposé : "Mendive - Irati-Soro Nord - Tertre d'habitat"-->
<!--nom proposé : ""-->

#### [Localisation]{.meta}

Altitude : [1027 m]{.altitude}

Il est situé à [une quinzaine de mètres]{.distance} à l'[O.]{.card} du [tumulus T3]{.monument}.

#### [Description]{.meta}

[Tertre]{.type} de [7m]{.diametre} de diamètre et [1m]{.hauteur} de haut.

#### [Historique]{.meta}

[Monument]{.monument} découvert par [Blot J.]{.personne} en [octobre 2011]{.date}.



    
### Irati- Soro N-Tertres d'habitat (nb = 8)
<!--nom proposé : "Mendive - Irati-Soro Nord - 8 Tertres d'habitat"-->
<!--nom proposé : ""-->

#### [Localisation]{.meta}

Altitude : [1038 m]{.altitude}

Ils sont échelonnés le long de la rive droite de l'[Iratiko erreka]{.toponyme}, avant qu'il ne soit traversé par la route qui mène aux [chalets d'Irati]{.toponyme}, au moment où elle amorce sa montée, après avoir laissé une bretelle vers le [chalet Pedro]{.toponyme}.

#### [Description]{.meta}

[Huit]{.nb} [tertres]{.type}, dont un bon nombre sont de [faible hauteur]{.hauteur}.

- [Tertre n°1]{.sub} : correspond aux coordonnées ci-dessus indiquées. Mesure [10m]{.diametre} de diamètre, [hauteur faible]{.hauteur}.
- [Tertre n°2]{.sub} : situé à [16m]{.distance} au [N]{.card} du [précédent]{.monument}. Mesure [6m]{.diametre} de diamètre.
- [Tertre n°3]{.sub} : situé à [20m]{.distance} au [N NE]{.card} du [précédent]{.monument}. Mesure [10m]{.longueur} x [8m]{.largeur}.
- [Tertre n°4]{.sub} : situé à [10m]{.distance} au [N NE]{.card} du [précédent]{.monument}. Mesure [7m]{.diametre} de diamètre.
- [Tertre n°5]{.sub} : situé à [60m]{.distance} au [NE]{.card} du [précédent]{.monument}. Mesure [9m]{.diametre} de diamètre.
- [Tertre n°6]{.sub} : situé à [4m]{.distance} au [NE]{.card} du [précédent]{.monument}. Mesure [10m]{.longueur} x [8m]{.largeur}.
- [Tertre n°7]{.sub} : situé à [4m]{.distance} au [N NE]{.card} du [précédent]{.monument}. Mesure [8m]{.longueur} x [5m]{.largeur}.
- [Tertre n°8]{.sub} : situé à [4m]{.distance} au [N NE]{.card} du [précédent]{.monument}. Mesure [8m]{.diametre} de diamètre.

#### [Historique]{.meta}

[Tertres]{.type} découverts par [Blot J.]{.personne} en [1975]{.date}.



    
### Irratiko erreka 1-Tertre d'habitat
<!--nom proposé : "Mendive - Irratiko erreka - 1 : Tertre d'habitat"-->
<!--nom proposé : ""-->

#### [Localisation]{.meta}

Altitude : [1056 m]{.altitude}

[Tertre]{.type} situé à [une cinquantaine de mètres]{.distance} au [sud]{.card} de la route qui rejoint [le plateau d'Irati]{.toponyme} aux môtels, et à [une centaine de mètres]{.distance} environ à l'[Est]{.card} du [Tumulus (probablement dolménique) dit T3]{.monument}. Il est à [8m]{.distance} à l'[O.]{.card} d'[un petit ru]{.toponyme}.

#### [Description]{.meta}

[Tertre terreux]{.type} asymétrique, incliné vers le [SE]{.card}, mesurant [8m]{.longueur} de long, [5m]{.largeur} de large et [0,80m]{.hauteur} de haut.

#### [Historique]{.meta}

[Monument]{.monument} découvert par [J. Blot]{.personne} en [avril 2015]{.date}.



    
### Iratiko erreka 2-Tertre d'habitat
<!--nom proposé : "Mendive - Iratiko erreka - 2 : Tertre d'habitat"-->
<!--nom proposé : ""-->

#### [Localisation]{.meta}

Altitude : [1054 m]{.altitude}

Il est situé à [90 mètres]{.distance} environ au [SO.]{.card} du [précédent]{.monument} et à [15m]{.distance} à l'[Ouest]{.card} du [petit ru déjà cité]{.toponyme}.

#### [Description]{.meta}

[Tertre asymétrique terreux]{.type} de [6m]{.longueur} de long, [4,50m]{.largeur} de large et [0,20]{.hauteur} de haut environ.

#### [Historique]{.meta}

[Monument]{.monument} découvert par [Blot J.]{.personne} en [avril 2015]{.date}.



    
### Ithurroché-Tertres d'habitat (nb = 4)
<!--nom proposé : "Mendive - Ithurroché - 4 Tertres d'habitat"-->
<!--nom proposé : ""-->

#### [Localisation]{.meta}

Ces [4]{.nb} [tertres]{.type} sont situés au [N.]{.card} d'une très importante doline et ils dominent de quelques mètres [la piste pastorale antique]{.toponyme} qui s'étend [du nord au sud]{.card} du vallon que longe, à l'[est]{.card}, [la route actuelle]{.toponyme}.

- [Les tertres 1 et 2]{.sub} :
  Altitude : [1000 m]{.altitude}. Sont sur un sol en pente vers l'[E]{.card}. alignés dans le sens [EO.]{.card}, et tangents, [le 2]{.sub} étant plus en altitude que [le 1]{.sub}.
- [Les tertres 3 et 4]{.sub} forment le deuxième groupe, à 15m au [S.]{.card} du [premier]{.monument} ; ils sont disposés de la même manière, [le 3]{.sub} étant plus en hauteur que [le 4]{.sub}.

#### [Description]{.meta}

[Ils]{.monument} mesurent [entre 7 et 9 mètres]{.diametre} de diamètre et [0,90m]{.hauteur} de haut.

#### [Historique]{.meta}

[Tertres]{.monument} découverts par [J. Blot]{.personne} en [1971]{.date}, et contrôlés en [avril 2013]{.date}.



    
### Olhazarre-Tumulus
<!--nom proposé : "Mendive - Olhazarre - Tumulus"-->
<!--nom proposé : ""-->

#### [Localisation]{.meta}

Altitude : [810 m]{.altitude}

Situé dans un petit col au [N]{.card} de la route qui monte de [Mendive]{.commune}, et à [7 mètres]{.distance} au [sud]{.card} de l'ancienne piste de crête.

#### [Description]{.meta}

[Tumulus de terre et de pierres]{.type} de [9m]{.diametre} de diamètre et [0,40m]{.hauteur} de haut, présentant une dépression centrale de 3m de large et de 0,30m de profondeur environ. Un bloc rocheux, qui ne paraît pas en rapport avec ce monument, occupe une partie [SE]{.card} de la dépression centrale.

#### [Historique]{.meta}

[Monument]{.monument} découvert par [J. Blot]{.personne} en [mai 2011]{.date}.




### Saint-Sauveur-d'Irati-Tumulus
<!--nom proposé : "Mendive - Saint-Sauveur d'Irati - Tumulus"-->
<!--nom proposé : ""-->

#### [Localisation]{.meta}

Carte [1346 est]{.carte}. [Saint-Jean-Pied-de-Port]{.commune}.

Altitude : [900 m]{.altitude}

Ce [tumulus]{.type}, actuellement surmonté de [la croix d'un calvaire]{.monument}, est situé à [quelques mètres]{.distance} au [nord nord-est]{.card} de [la chapelle Saint-Sauveur]{.monument}.

#### [Description]{.meta}

On note, sous le calvaire une butte plus ou moins informe qui est le reliquat d'un [tumulus «préhistorique»]{.type} signalé par [le Cdt Rocq]{.personne} dans [@rocqEtudePeuplementPays1935, p.371]. D'après lui, lors de la réfection de [la chapelle]{.monument}, de nombreux ossements furent trouvés dans [ce tumulus]{.monument} ; le curé voulut donner une sépulture chrétienne à ces «païens», et la croix fut installée. On ignore tout de l'époque de cette sépulture, mais nous avons préféré la signaler ici, avec les réserves qui s'imposent.

#### [Historique]{.meta}

[Monument]{.monument} signalé par [le Cdt Rocq]{.personne} en [1935]{.date}.



    
### Sare-Sare-Cromlech (?)
<!--nom proposé : "Mendive - Sare-Sare - Cromlech douteux"-->
<!--nom proposé : ""-->

#### [Localisation]{.meta}

Altitude : [806 m]{.altitude}

Il est au beau milieu du replat que forme la croupe de [Sare-Sare]{.toponyme} au [flanc sud]{.card} du [pic de Béhorlégui]{.toponyme}.

#### [Description]{.meta}

Cercle de pierre peu visible, de [9m]{.diametre} de diamètre, délimité par 7 pierres au ras du sol ; elles sont absentes dans les deux quarts [SE]{.card} et [SO]{.card}. Monument douteux.

#### [Historique]{.meta}

[Monument]{.monument} découvert par [Meyrat F.]{.personne} en [avril 2013]{.date}.



    
### Sare-Sare-Tertre d'habitat
<!--nom proposé : "Mendive - Sare-Sare - Tertre d'habitat"-->
<!--nom proposé : ""-->

#### [Localisation]{.meta}

Altitude : [870 m]{.altitude}

On peut le voir au bord de [la route actuelle]{.toponyme}, dont il touche [le bord sud]{.card}.

#### [Description]{.meta}

Quoiqu'en partie détérioré par la route, ce [tertre de terre et de pierres]{.type} affecte la forme classique du [tertre circulaire dissymétrique]{.type}, mesurant [12 m]{.diametre} de diamètre pour un dénivelé de 2 à 3 m : il ne semble pas que nous soyons devant le résultat d'un amas dû aux engins de terrassement lors de la construction de la route.

#### [Historique]{.meta}

[Monument]{.monument} trouvé par [Blot J.]{.personne} en [avril 2013]{.date}.



    
### Xuberaxain-Tertres d'habitat (nb = 8)
<!--nom proposé : "Mendive - Xuberaxain - 8 Tertres d'habitat"-->
<!--nom proposé : ""-->

#### [Localisation]{.meta}

Il y a [8]{.nb} [TH]{.type} se trouvant dans la prairie qui surplombe [le dolmen de Xuberaxain Harria]{.monument}.

- [Xuberaxain TH 1]{.sub} -- Altitude : [612 m]{.altitude}
- [Xuberaxain TH 2]{.sub} -- Altitude : [608 m]{.altitude}
- [Xuberaxain TH 3]{.sub} -- Altitude : [620 m]{.altitude}
    - Plus haut situé que [les précédents]{.monument}, relief peu marqué
- [Xuberaxain TH 4]{.sub} -- Altitude : [623 m]{.altitude}
    - A quelques mètres au-dessus du [n° 3]{.monument} -- relief peu marqué.
- [Xuberaxain TH 5]{.sub} -- Altitude : [600 m]{.altitude}
    - Le plus bas situé -- Relief mieux marqué
- [Xuberaxain TH 6]{.sub} -- Altitude : [612 m]{.altitude}
    - Relief peu marqué
- [Xuberaxain TH 7]{.sub} -- Altitude : [620 m]{.altitude}
    - Relief très visible -- à proximité des bâtiments agricoles
- [Xuberaxain TH 8]{.sub} --Altitude : [622 m]{.altitude}
    - Relief très net -- plus en altitude que [le précédent]{.monument} -- à proximité et au même niveau que les bâtiments agricoles

#### [Historique]{.meta}

[Xuberaxain TH3]{.monument}, [TH 4]{.monument} et [TH5]{.monument} : trouvés par [Blot J.]{.personne} en [avril 2013]{.date}.

[Xuberaxain TH 7]{.monument} et [TH8]{.monument} : Trouvés par [le groupe Hilharriak]{.personne} en [avril 2013]{.date}.



    
### Xuberaxain-Tertres d'habitat (nb = 2)
<!--nom proposé : "Mendive - Xuberaxain - 2 Tertres d'habitat"-->
<!--nom proposé : ""-->

#### [Localisation]{.meta}

Altitude : [630 m]{.altitude}

Ces [deux]{.nb} [tertres]{.type} sont à [une centaine de mètres]{.distance} au [NE]{.card} du [dolmen Xuberaxain]{.monument}, sur un terrain en pente.

#### [Description]{.meta}

- [Le tertre n°1]{.sub}, le plus net mesure [une quinzaine de mètres]{.diametre} de diamètre. Du fait de la pente il est asymétrique, sa hauteur moyenne étant d'environ [0,70m]{.hauteur}.
- [Le tertre n°2]{.sub} est à [une trentaine de mètres]{.distance} à l'[E]{.card} du [précédent]{.monument}. Il est plus modeste ([10m]{.diametre} de diamètre), et [moins marqué]{.hauteur} en hauteur.

#### [Historique]{.meta}

[Tertres]{.monument} découverts par [Blot J.]{.personne} en [décembre 2011]{.date}.


</article>

## Bibliographie
