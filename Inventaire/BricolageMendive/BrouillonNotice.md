- Nom : Xquery

- Commune (Pactols-Lieux) : tableur
- type de monument (PACTOLS-Sujet) : tableur
- période (PACTOLS-Chronologie) : tableur
- PACTOLS-Peuple : tableur

- réference Fouille DRAC : xquery + tableur
- référence HAL : xquery + openrefine
- référence biblio ? : xquery + openrefine

- lieu-dit : xquery

- inventeur : xquery
- date de découverte : xquery

- monuments liés : xquery + tableur


- sous-ensemble
- 



<title>$for(h1)$$h1$ - $endfor$</title>


<title>$if(title-prefix)$$title-prefix$ ÔÇô $endif$$pagetitle$</title>