for $i in //monument
let $nom := $i/h3/text()

let $alt := for $a in $i//*:span[@class="altitude"]
return <alt>{$a/text()}</alt>

let $date := for $b in $i//*:span[@class="date"]
return <date>{$b/text()}</date>

let $pers := for $c in $i//*:span[@class="personne"]
return <personne>{$c/text()}</personne>

let $type := for $d in $i//*:span[@class="type"]
return <type>{$d/text()}</type>

let $sub := for $e in $i//*:span[@class="sub"]
return <sub>{$e/text()}</sub>

let $monument := for $f in $i//*:span[@class="monument"]
return <monument>{$f/text()}</monument>

let $topo := for $g in $i//*:span[@class="toponyme"]
return <toponyme>{$g/text()}</toponyme>

let $commune := for $h in $i//*:span[@class="commune"]
return <commune>{$h/text()}</commune>

let $nb := for $j in $i//*:span[@class="nb"]
return <nb>{$j/text()}</nb>

let $distance := for $k in $i//*:span[@class="distance"]
return <distance>{$k/text()}</distance>

let $hauteur := for $l in $i//*:span[@class="hauteur"]
return <hauteur>{$l/text()}</hauteur>

let $longueur := for $m in $i//*:span[@class="longueur"]
return <longueur>{$m/text()}</longueur>

let $largeur := for $n in $i//*:span[@class="largeur"]
return <largeur>{$n/text()}</largeur>

let $diametre := for $o in $i//*:span[@class="diametre"]
return <diametre>{$o/text()}</diametre>

let $card := for $p in $i//*:span[@class="card"]
return <card>{$p/text()}</card>

let $carte := for $q in $i//*:span[@class="carte"]
return <carte>{$q/text()}</carte>

return <notice><nom>{$nom}</nom>{$commune}{$carte}{$alt}{$nb}{$type}{$distance}{$topo}{$card}{$diametre}{$longueur}{$largeur}{$hauteur}{$pers}{$date}{$sub}{$monument}</notice>
